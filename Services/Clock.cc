#include <string.h>
#include <time.h>
#include <Services/Clock.h>
#include <usvgwidgets/USVGText.h>
// -------------------------------------------------------------------------
Clock::~Clock(){
	connPoll.disconnect();
	delete tm;
}
// -------------------------------------------------------------------------
void Clock::gen_str()
{
	gchar timestr[64];
	struct timeval tv;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, tm);
	strftime(timestr, 64, fmt1.c_str(), tm);
	widget1->setText(Glib::locale_to_utf8(timestr));
	strftime(timestr, 64, fmt2.c_str(), tm);
	widget2->setText(Glib::locale_to_utf8(timestr));
	strftime(timestr, 64, fmt3.c_str(), tm);
	widget3->setText(Glib::locale_to_utf8(timestr));
}
// -------------------------------------------------------------------------
bool Clock::timer_callback()
{
	GDK_THREADS_ENTER ();
	gen_str();
	GDK_THREADS_LEAVE ();
	return true;
}
// -------------------------------------------------------------------------
Clock::Clock(USVGText *w1, USVGText *w2, USVGText *w3) :
	widget1(w1),
	widget2(w2),
	widget3(w3),
	fmt1("%H:%M:%S"),
	fmt2("%d %b"),
	fmt3("%Y")
{
	tm = new struct tm;

	gen_str();
	connPoll = Glib::signal_timeout().connect(sigc::mem_fun(*this, &Clock::timer_callback), 1000);
}
// -------------------------------------------------------------------------
/*
void Clock::set_format1(const std::string frmt){
	fmt1 = frmt;
}

void Clock::set_format2(const std::string frmt){
	fmt2 = frmt;
}

void Clock::set_format3(const std::string frmt){
	fmt3 = frmt;
}
*/
// -------------------------------------------------------------------------
