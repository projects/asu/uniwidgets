#include <Services/ClockConfig.h>
#include <Services/FailureDialog.h>
// -------------------------------------------------------------------------
using namespace std;
// -------------------------------------------------------------------------
extern void unset_focus(Gtk::Widget *w);
extern void add_frame(Gtk::Dialog *w);
// -------------------------------------------------------------------------
const char* setclock_messages[2] = {"", "Не удалось выставить время"};
// -------------------------------------------------------------------------
ClockConfig::ClockConfig(Glib::RefPtr<Gnome::Glade::Xml> gxml, FailureDialog *fwin, Gtk::Dialog *d) :
	dialog(d),
	fw(fwin)
{
	gxml->get_widget("clock_config", w);
	add_frame(w);

	gxml->get_widget("spin_hours", sh);
	unset_focus(sh);
	gxml->get_widget("spin_minutes", sm);
	unset_focus(sm);
	gxml->get_widget("spin_seconds", ss);
	unset_focus(ss);
	gxml->get_widget("spin_years", sy);
	unset_focus(sy);
	gxml->get_widget("spin_months", smo);
	unset_focus(smo);
	gxml->get_widget("spin_days", sd);
	unset_focus(sd);

	Gtk::Button *apply, *cancel;
	gxml->get_widget("clock_apply", apply);
	gxml->get_widget("clock_cancel", cancel);
	apply->signal_clicked().connect(sigc::mem_fun(*this, &ClockConfig::apply));
	cancel->signal_clicked().connect(sigc::mem_fun(*this, &ClockConfig::cancel));
}
// -------------------------------------------------------------------------
void ClockConfig::apply()
{

	stringstream out;
	out.width(2);
	out.fill('0');
	out << setfill('0') << setw(2) << sh->get_value_as_int()  << " ";
	out << setfill('0') << setw(2) << sm->get_value_as_int()  << " ";
	out << setfill('0') << setw(2) << ss->get_value_as_int()  << " ";
	out << setfill('0') << setw(2) << sd->get_value_as_int()  << " ";
	out << setfill('0') << setw(2) << smo->get_value_as_int() << " ";
	out << setfill('0') << setw(2) << sy->get_value_as_int();

	string command = "/usr/bin/ctl-setclock.sh " + out.str();

	int result = system(command.c_str());
	cerr << "set time: result = " << result << endl;

	bool ok = fw->show(command.c_str(), 1, setclock_messages);
	if(ok)
		w->hide();
}
// -------------------------------------------------------------------------
void ClockConfig::cancel()
{
	w->hide();
}
// -------------------------------------------------------------------------
void ClockConfig::show()
{
	struct tm t;
	struct timeval tv;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &t);

	sh->set_value(t.tm_hour);
	sm->set_value(t.tm_min);
	ss->set_value(t.tm_sec);
	sy->set_value(t.tm_year + 1900);
	smo->set_value(t.tm_mon + 1);
	sd->set_value(t.tm_mday);

	if(dialog->is_visible())
		dialog->hide();
	w->show();
}
// -------------------------------------------------------------------------
