#include <Services/FailureDialog.h>
// -------------------------------------------------------------------------
using namespace std;
// -------------------------------------------------------------------------
extern void add_frame(Gtk::Dialog *w);
// -------------------------------------------------------------------------
FailureDialog::FailureDialog(Glib::RefPtr<Gnome::Glade::Xml> gxml)
{
	Gtk::Button *btn;

	gxml->get_widget("failure_dialog", failure_dialog);
	add_frame(failure_dialog);

	gxml->get_widget("failure_message", failure_message);
	gxml->get_widget("failure_cancel", btn);

	btn->signal_clicked().connect(sigc::mem_fun(*this, &FailureDialog::hide));
}
// -------------------------------------------------------------------------
void FailureDialog::show_simple (Glib::ustring message)
{
		failure_message->set_text(message);
		failure_dialog->show();
}
// -------------------------------------------------------------------------
bool FailureDialog::show(const char *command, int max_code, const char *messages[], bool show_always)
{
	int result = system(command);

	if(result < 0)
	{
		failure_message->set_text("Negative error messages are still not processed");
		failure_dialog->show();
		return false;
	}

	if(result || show_always) {
		if(result > max_code)
			result = max_code;

		/* Hack; normal i18n is required instead */
		failure_message->set_text(Glib::locale_to_utf8(messages[show_always ? result : result - 1]));
		failure_dialog->show();
	}

	return result == 0;
}
// -------------------------------------------------------------------------
void FailureDialog::hide()
{
	failure_dialog->hide();
}
// -------------------------------------------------------------------------
