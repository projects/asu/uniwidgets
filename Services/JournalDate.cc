#include <Services/JournalDate.h>
#include <Services/FailureDialog.h>
// -------------------------------------------------------------------------
using namespace std;
// -------------------------------------------------------------------------
extern void unset_focus(Gtk::Widget *w);
extern void add_frame(Gtk::Dialog *w);
extern const char* flash_messages[2];
// -------------------------------------------------------------------------
JournalDate::JournalDate(Glib::RefPtr<Gnome::Glade::Xml> gxml, FailureDialog *fwin, Gtk::Dialog *d) :
	dialog(d),
	fw(fwin)
{
	gxml->get_widget("journal_date", w);
	add_frame(w);

	gxml->get_widget("spin_years1", sy1);
	unset_focus(sy1);
	gxml->get_widget("spin_months1", smo1);
	unset_focus(smo1);
	gxml->get_widget("spin_days1", sd1);
	unset_focus(sd1);
	gxml->get_widget("spin_years2", sy2);
	unset_focus(sy2);
	gxml->get_widget("spin_months2", smo2);
	unset_focus(smo2);
	gxml->get_widget("spin_days2", sd2);
	unset_focus(sd2);

	Gtk::Button *apply, *cancel;
	gxml->get_widget("date_apply", apply);
	gxml->get_widget("date_cancel", cancel);
	apply->signal_clicked().connect(sigc::mem_fun(*this, &JournalDate::apply));
	cancel->signal_clicked().connect(sigc::mem_fun(*this, &JournalDate::cancel));
}
// -------------------------------------------------------------------------
void JournalDate::apply()
{

	stringstream out;
	out.width(2);
	out.fill('0');
	out << setfill('0') << setw(2) << sy1->get_value_as_int() << "-";
	out << setfill('0') << setw(2) << smo1->get_value_as_int() << "-";
	out << setfill('0') << setw(2) << sd1->get_value_as_int()  << " ";

	out << setfill('0') << setw(2) << sy2->get_value_as_int() << "-";
	out << setfill('0') << setw(2) << smo2->get_value_as_int() << "-";
	out << setfill('0') << setw(2) << sd2->get_value_as_int();

	string command = "/usr/bin/ctl-flash.sh --savedump " + out.str();

	bool ok = fw->show(command.c_str(), 1, flash_messages, true);
	if(ok)
		w->hide();
}
// -------------------------------------------------------------------------
void JournalDate::cancel()
{
	w->hide();
}
// -------------------------------------------------------------------------
void JournalDate::show()
{
	struct tm t;
	struct timeval tv;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &t);

	sy1->set_value(t.tm_year + 1900);
	smo1->set_value(t.tm_mon + 1);
	sd1->set_value(t.tm_mday);
	sy2->set_value(t.tm_year + 1900);
	smo2->set_value(t.tm_mon + 1);
	sd2->set_value(t.tm_mday);

	if(dialog->is_visible())
		dialog->hide();
	w->show();
}
// -------------------------------------------------------------------------
