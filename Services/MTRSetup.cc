#include <sstream>
#include <iostream>
#include <unistd.h>
#include <UniXML.h>
#include <extensions/MTR.h>
// #include "YauzaConfiguration.h"
#include <usvgwidgets/USVGButton.h>
#include <include/Services/MTRSetup.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace UniSetTypes;
using Glib::ustring;
// -------------------------------------------------------------------------
extern void unset_focus(Gtk::Widget *w);
extern void add_frame(Gtk::Dialog *w);
// -------------------------------------------------------------------------
MRMReCreator::MRMReCreator():
	mb_(NULL),
	plugged_(false)
{ }
// -------------------------------------------------------------------------
bool MRMReCreator::checkPlugged()
{
	plugged_ = (access(dev_.c_str(), F_OK | R_OK | W_OK) == 0);
	return plugged_;
}
// -------------------------------------------------------------------------
void MRMReCreator::reCreate()
{
	try {
		mb_ = new ModbusRTUMaster(dev_, false);
		mb_->setTimeout(timeout_);
		mb_->setSpeed(speed_);
	}
	catch(ModbusRTU::mbException& ex) {
		cerr << "(mtr-setup): " << ex << endl;
	}
	catch(SystemError& err) {
		cerr << "(mtr-setup): " << err << endl;
	}
	catch(Exception& ex) {
		cerr << "(mtr-setup): " << ex << endl;
	}
	catch(...) {
		cerr << "(mtr-setup): catch(...)" << endl;
	}
}
// -------------------------------------------------------------------------
void MRMReCreator::changeDev(const Glib::ustring dev)
{
	dev_ = dev;

	if (mb_) { 
		delete mb_;
		mb_ = NULL;
	}

	if (checkPlugged())
		reCreate();
}
// -------------------------------------------------------------------------
void MRMReCreator::check()
{
	const bool was_plugged = plugged_;
	checkPlugged();

	if (was_plugged && !plugged_ && mb_) {
		delete mb_;
		mb_ = NULL;
	}

	if (plugged_ && !was_plugged)
		reCreate();
}
// -------------------------------------------------------------------------
void MTRSetup::hide()
{
	abortation();
	MTR_Setup->hide();
}
// -------------------------------------------------------------------------
const ustring plugged_text   = "Состояние устройства: <span weight=\"bold\" size=\"larger\" color=\"green\">подключено</span>";
const ustring unplugged_text = "Состояние устройства: <span weight=\"bold\" size=\"larger\" color=\"red\">отключено</span>";
const ustring unknown_text   = "Состояние устройства: <span weight=\"bold\" size=\"larger\" color=\"gray\">неизвестно</span>";
// -------------------------------------------------------------------------
bool MTRSetup::check(bool deprecated)
{
	using ModbusRTU::SlaveFunctionCode;
	detected = false;

	mrmc.check();
	ModbusRTUMaster* mb = mrmc.getMRM();

	frame_autodetect->set_sensitive(mb != NULL);
	start_autodetect->set_sensitive(mb != NULL);

	if(!mb)
	{
		label_state->set_markup(unplugged_text);
		box_actions->set_sensitive(detected);
		return true;
	}

	try {
		mb->setSpeed(combo_speed->get_active_text());

		const int addr = spin_addr->get_value_as_int(); 
		const int reg  = spin_reg ->get_value_as_int();
		const SlaveFunctionCode fc =
			SlaveFunctionCode(combo_fn->get_active_row_number() + 1);
		ModbusHelpers::autodetectSlave(mb, addr, addr, reg, fc);

		detected = true;
	}
	catch(TimeOut) {
	}
	catch(...) {
		cerr << "(mtr-setup): unknown_error" << endl;
	}

	box_actions->set_sensitive(detected);

	if (detected)
	{
		label_state->set_markup(plugged_text);
	}
	else
	{
		label_state->set_markup(unknown_text);
	}
	return true;
}
// -------------------------------------------------------------------------
void MTRSetup::show()
{
	dialog->hide();
	start_check();
	MTR_Setup->show();
}
// -------------------------------------------------------------------------
void MTRSetup::start_check()
{
	check_dev = Glib::signal_timeout().connect(sigc::bind<bool>(sigc::mem_fun(*this, &MTRSetup::check), false), 2000);
}
// -------------------------------------------------------------------------
void MTRSetup::stop_check()
{
	check_dev.disconnect();
}
// -------------------------------------------------------------------------
void MTRSetup::begin_changed()
{
	spin_end->set_range(spin_beg->get_value(), 255.0);
}
// -------------------------------------------------------------------------
void MTRSetup::dev_changed()
{
	Glib::ustring new_dev = combo_dev->get_active_text();
	//TODO: does this check really needed?
	if(new_dev != current_device) {
		mrmc.changeDev(new_dev);
		const bool created = mrmc.getMRM();
		current_device = new_dev;
		frame_autodetect->set_sensitive(created);
		start_autodetect->set_sensitive();
	}
}
// -------------------------------------------------------------------------
void MTRSetup::speed_changed()
{
	mrmc.setSpeed(combo_speed->get_active_text());
}
// -------------------------------------------------------------------------
void MTRSetup::timeout_changed()
{
	mrmc.setTimeout(spin_timeout->get_value_as_int());
}
// -------------------------------------------------------------------------
void MTRSetup::toggle_settings_cb()
{
	settings_visible = !settings_visible;

	settings_visible ? frame_settings->show() : frame_settings->hide();
	settings_visible ? frame_autodetect->show() : frame_autodetect->hide();
	toggle_settings->set_label( (settings_visible ? "Скрыть параметры" : "Параметры...") );
}
// -------------------------------------------------------------------------
void select_item(Gtk::ComboBox *combo, Glib::ustring item)
{
	Glib::RefPtr<Gtk::TreeModel> mdl = combo->get_model();
	Gtk::TreeModel::Children cld = mdl->children();

	for(Gtk::TreeNodeChildren::iterator it = cld.begin(); it != cld.end(); it++) {
		Gtk::TreeModel::Row row = *it;
		Glib::ustring v;
		row.get_value(0, v);
		if(v == item) {
			combo->set_active(row);
			break;
		}
	}
}
// -------------------------------------------------------------------------
void MTRSetup::set_normal_state(bool state)
{
	state ? mtr_abort->hide() : mtr_abort->show();
	state ? autodetect_progress->hide() : autodetect_progress->show();
	state ? start_autodetect->show() : start_autodetect->hide();

	frame_settings->set_sensitive(state);
	frame_prog->set_sensitive(state);
	radio_guessspeed->set_sensitive(state);
}
// -------------------------------------------------------------------------
static char *speeds[] = {"9600", "14400", "19200", "38400", "57600", "115200"};
// -------------------------------------------------------------------------
void MTRSetup::autodetect_start()
{
	stop_button_pressed = false;
	using ModbusRTU::SlaveFunctionCode;
	stop_check();

	set_normal_state(false);

	const int beg_addr = spin_beg ->get_value_as_int();
	const int end_addr = spin_end ->get_value_as_int();
	const int reg      = spin_reg ->get_value_as_int();

	const SlaveFunctionCode fn =
		SlaveFunctionCode(combo_fn->get_active_row_number() + 1);

	ModbusRTU::ModbusAddr detected_addr = 0;
	std::string detected_speed;
	detected = false;

	label_state->set_markup(unknown_text);

	ModbusRTUMaster* mb = mrmc.getMRM();

	autodetect_progress->set_fraction(0.0);
	if (radio_guessspeed->get_active()) 
	{
		std::list<string>::iterator sit = speedlist.begin();
		int num=0;
		for(;sit!=speedlist.end();sit++)
		{
			mb->setSpeed(*sit);
			for(int i = beg_addr; i <= end_addr; i++)
			{
				const double new_fraction = (float)num/speedlist.size() + (float)(i-beg_addr)/(end_addr-beg_addr)/speedlist.size();
				autodetect_progress->set_fraction(new_fraction);
				while(Gtk::Main::events_pending())
					Gtk::Main::iteration();
				if (stop_button_pressed) 
				{
					stop_button_pressed = false;
					return;
				}
				try
				{
					detected_addr = ModbusHelpers::autodetectSlave(mb, i, i, reg ,fn);
					detected = true;
					detected_speed = *sit;
					break;
				}
				catch(UniSetTypes::TimeOut)
				{
					cerr << "catch timeout" << endl;
				}
				catch( ... )
				{
					cerr << "catch ..." << endl;
				}
			}
			num++;
			if (detected)
				break;
		}
		const int max_speed_id = sizeof(speeds) / sizeof(char*);
		for(int speed_id = 0; speed_id < max_speed_id; speed_id++) {
			mb->setSpeed(speeds[speed_id]);
			for(int i = beg_addr; i <= end_addr; i++) {
				const double new_fraction =
					float(speed_id) / (sizeof(speeds) / sizeof(char*)) +
					float(i - beg_addr) / end_addr / (sizeof(speeds) / sizeof(char*));
				autodetect_progress->set_fraction(new_fraction);

				while(Gtk::Main::events_pending())
					Gtk::Main::iteration();
				if (stop_button_pressed) {
					stop_button_pressed = false;
					return;
				}

				try {
					detected_addr = ModbusHelpers::autodetectSlave(mb, i, i, reg ,fn);
					detected = true;
					detected_speed = speeds[speed_id];
					break;
				}
				catch(UniSetTypes::TimeOut)
				{
					cerr << "catch timeout" << endl;
				}
			}
			if (detected)
				break;
		}

	}

	else {
		for(int i = beg_addr; i <= end_addr; i++) {
			autodetect_progress->set_fraction((float)(i - beg_addr)/(end_addr - beg_addr));
			while(Gtk::Main::events_pending()) Gtk::Main::iteration();
			try {
				detected_addr = ModbusHelpers::autodetectSlave(mb, i, i, reg, fn);
				detected = true;
			}
			catch( UniSetTypes::TimeOut& ) {
				cerr << "catch timeout" << endl;
			}
			catch( ... ) {
				cerr << "catch ..." << endl;
			}

			if (detected)
				break;
		}
	}

	if(detected) {
		spin_addr->set_value(detected_addr);
		if(radio_guessspeed->get_active())
			select_item(combo_speed, detected_speed);
	}
	else {
		fail_mess->set_text("Устройство не найдено.");
		detection_failed->show_all();
		detection_failed->run();
		detection_failed->hide();
	}

	start_check();
	set_normal_state(true);
}
// -------------------------------------------------------------------------
void MTRSetup::prog_clicked(Glib::ustring fname)
{
	stringstream command;
	const string filename = path + fname;

	if(access(filename.c_str(), R_OK) != 0) {
		const ustring error_message =  
			ustring("Файл конфигурации не найден! (") + filename + ")";
		fail_mess->set_text(error_message);
		detection_failed->show_all();
		detection_failed->run();
		detection_failed->hide();
		return;
	}

	int result = 1;
	if( mrmc.plugged() )
		result = MTR::update_configuration( mrmc.getMRM(), spin_addr->get_value_as_int(), filename, 0 ) ? 0 : 1;

	if(result != 0) {
		fail_mess->set_text("Сбой программирования устройства!");
		detection_failed->show_all();
		detection_failed->run();
		detection_failed->hide();
	}
	else {
		fail_mess->set_text("Устройство перепрограммировано");
		detection_failed->show_all();
		detection_failed->run();
		detection_failed->hide();
	}

}
// -------------------------------------------------------------------------
void MTRSetup::abortation()
{
	stop_button_pressed = true;
	update_progress.disconnect();
	check_dev.disconnect();
	set_normal_state(true);
}
// -------------------------------------------------------------------------
void combo_postconfig(Gtk::ComboBoxEntry *combo)
{
	Gtk::Entry *entry = combo->get_entry();
	entry->set_editable(false);
	entry->unset_flags(Gtk::CAN_FOCUS);
}
// -------------------------------------------------------------------------
MTRSetup::MTRSetup(Glib::RefPtr<Gnome::Glade::Xml> gxml, Gtk::Dialog *d, Glib::ustring mtr_prog_path) :
	dialog(d),
	path(mtr_prog_path),
	port_ok(false),
	settings_visible(false),
	detected(false)
{
	Gtk::Button *btn;

	gxml->get_widget("MTR_setup", MTR_Setup);
	add_frame(MTR_Setup);

	gxml->get_widget("frame_settings", frame_settings);
	gxml->get_widget("frame_autodetect", frame_autodetect);
	gxml->get_widget("frame_prog", frame_prog);
	gxml->get_widget("box_actions", box_actions);

	gxml->get_widget("combo_dev", combo_dev);
	combo_postconfig(combo_dev);
	combo_dev->set_active(0);
	gxml->get_widget("combo_speed", combo_speed);
	combo_postconfig(combo_speed);
	Gtk::TreeView*treeview;
	treeview = manage(new class Gtk::TreeView(combo_speed->get_model()));
	Gtk::TreeModel::Children children = combo_speed->get_model()->children();
	for(Gtk::TreeModel::Children::iterator iter = children.begin(); iter != children.end(); ++iter)
	{
		combo_speed->set_active(iter);
		speedlist.push_back(combo_speed->get_active_text());
	}


	gxml->get_widget("spin_addr", spin_addr);
	unset_focus(spin_addr);
	gxml->get_widget("spin_timeout", spin_timeout);
	unset_focus(spin_timeout);

	gxml->get_widget("combo_fn", combo_fn);
	combo_postconfig(combo_fn);
//	combo_fn->set_active(2);
	select_item(combo_fn, "0x03");
	
	gxml->get_widget("spin_beg", spin_beg);
	unset_focus(spin_beg);
	spin_beg->signal_value_changed().connect(sigc::mem_fun(*this, &MTRSetup::begin_changed));
	gxml->get_widget("spin_end", spin_end);
	unset_focus(spin_end);

	gxml->get_widget("spin_reg", spin_reg);
	unset_focus(spin_reg);
	gxml->get_widget("autodetect_progress", autodetect_progress);
	gxml->get_widget("label_state", label_state);

	gxml->get_widget("mtr_abort", mtr_abort);
	mtr_abort->signal_clicked().connect(sigc::mem_fun(*this, &MTRSetup::abortation));

//	gxml->get_widget("radio_speed", radio_speed);
//	radio_speed->signal_toggled().connect(sigc::mem_fun(*this, &MTRSetup::speed_toggled));
	gxml->get_widget("radio_guessspeed", radio_guessspeed);
//	radio_guessspeed->signal_toggled().connect(sigc::mem_fun(*this, &MTRSetup::guessspeed_toggled));

	gxml->get_widget("label_beg", label_beg);
	gxml->get_widget("label_end", label_end);

	gxml->get_widget("mtr_settings", toggle_settings);
	toggle_settings->signal_clicked().connect(sigc::mem_fun(*this, &MTRSetup::toggle_settings_cb));

	gxml->get_widget("mtr_quit", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*this, &MTRSetup::hide));

	gxml->get_widget("start_autodetect", start_autodetect);
	start_autodetect->signal_clicked().connect(sigc::mem_fun(*this, &MTRSetup::autodetect_start));

	detection_failed = new Gtk::Dialog("", *MTR_Setup, true);
	add_frame(detection_failed);
	Gtk::VBox *thing = detection_failed->get_vbox();
	fail_mess = new Gtk::Label("");
	fail_mess->show();
	thing->pack_start(*fail_mess);

	btn = new Gtk::Button("Закрыть");
	detection_failed->add_action_widget(*btn, Gtk::RESPONSE_OK);

	xmlNode *actions = conf->getNode("MTR");
	if(actions == NULL) {
		cout << "Отсутствует секция \"MTR\"!" << endl;
		return;
	}

	UniXML_iterator it(actions);
	std::string v;

	v = it.getProp("device");
	if(!v.empty())
		select_item(combo_dev, v);

	v = it.getProp("speed");
	if(!v.empty())
		select_item(combo_speed, v);

	v = it.getProp("address");
	if(!v.empty())
		spin_addr->set_value(uni_atoi(v));

	v = it.getProp("timeout");
	if(!v.empty())
		spin_timeout->set_value(uni_atoi(v));

//	current_device = combo_dev->get_active_text();
//	port_ok = create_mb(mb, current_device, combo_speed->get_active_text(),
//	                        spin_timeout->get_value_as_int());
//	cerr << "mb created" << port_ok << endl;
//
	mrmc.changeDev(combo_dev->get_active_text());
	mrmc.setSpeed(combo_speed->get_active_text());
	mrmc.setTimeout(spin_timeout->get_value_as_int());
	mrmc.check();

	const bool ok = mrmc.plugged();
	frame_autodetect->set_sensitive(ok);
	start_autodetect->set_sensitive(ok);

	if(!it.goChildren()) {
		cout << "Не задан список вариантов программирования устройств MTR!" << endl;
		return;
	}

	Gtk::HBox *thing1;
	unsigned int i = 0;
	for(; it.getCurrent(); it.goNext()) {
		if(i == 0) {
			thing1 = new Gtk::HBox(true, 10);
			box_actions->pack_start(*thing1);
			thing1->show();
		}

		btn = new USVGButton(it.getProp("text"));
		thing1->pack_start(*btn, true, true, 0);

		btn->signal_clicked().connect(sigc::bind<Glib::ustring>(sigc::mem_fun(*this, &MTRSetup::prog_clicked),
		                                                        it.getProp("file")));
		btn->set_size_request(-1, 50);
		btn->show();

		if(++i > 4)
			i = 0;
	}
}
// -------------------------------------------------------------------------
