#include <Services/MainWindow.h>
#include <Services/ScreenPacker.h>
#include <Services/Miscellaneous.h>
#include <Services/MTRSetup.h>
#include <Services/JournalDate.h>
#include <Services/ClockConfig.h>
#include <Services/FailureDialog.h>
#include <Services/Clock.h>
#include <usvgwidgets/USVGText.h>
#include "types.h"
// -------------------------------------------------------------------------
#define NOTEBOOK_PREFIX "nb_"
#define NOTEBOOK_PREFIX_SIZE 3
// -------------------------------------------------------------------------
using namespace UniSetTypes;
using namespace UniWidgets;
using namespace std;
// using namespace Yauza;
// -------------------------------------------------------------------------
extern UPrinterInterface* printer;
extern ConnectorRef connector_;
extern const char* flash_messages[2];
// -------------------------------------------------------------------------
Glib::ustring getSaveDir()
{
	Glib::ustring savedir = "/tmp";

	xmlNode* node = conf->getNode("GUI");

	if(node)
	{
		UniXML_iterator it(node);
		savedir = it.getProp("SaveDir");
	}
	return savedir;
}
// -------------------------------------------------------------------------
/*Структура для обработчика сохранения отчетов*/
struct m_report
{
	FailureDialog *fd;
	const gchar *mes;
};
// -------------------------------------------------------------------------
/* Обработчик результата команды сохранения отчета из web-журнала */
void notify_status_cb(GObject* object, GParamSpec* pspec, m_report *d)
{
	using Glib::ustring;
	WebKitDownload* download = WEBKIT_DOWNLOAD(object);
	switch (webkit_download_get_status(download)) {
		case WEBKIT_DOWNLOAD_STATUS_FINISHED:
			(d->fd)->show_simple(Glib::locale_to_utf8("Отчет успешно сохранён:\n"+string(d->mes)));
			delete d;
			break;
		case WEBKIT_DOWNLOAD_STATUS_ERROR:
			(d->fd)->show_simple(Glib::locale_to_utf8("Ошибка сохранения отчета:\n"+string(d->mes)));
			delete d;
			break;
		case WEBKIT_DOWNLOAD_STATUS_CANCELLED:
// 			g_assert_not_reached();
			delete d;
			break;
		default:
		break;
	}
}
// -------------------------------------------------------------------------
/* Обработчик запроса на сохранения данных из web-журнала */
bool download_requested_cb(WebKitWebView *web_view, WebKitDownload
*download,FailureDialog *fd)
{
	using Glib::ustring;
	/*Создание имени файла отчета*/
	struct tm t;
	struct timeval tv;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &t);

	Glib::ustring dir = getSaveDir();
	const ustring filename =
		ustring::compose("%1.%2_%3.%4.%5",
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_mday ),
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_mon  + 1),
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_hour + 1),
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_min  + 1),
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_sec  + 1));
	ustring source=dir+"/"+webkit_download_get_suggested_filename(download)+"_"+filename;
	gchar *destination = NULL;
	destination = g_filename_to_uri(source.c_str(),NULL, NULL);
	if(destination)
	{
		m_report *d = new m_report();
		d->fd = fd;
		d->mes = destination;
		webkit_download_set_destination_uri(download,destination);
// 		g_free (destination);
		g_signal_connect(download, "notify::status",
                     G_CALLBACK(notify_status_cb), d);

		return true;
	}
	else
		return false;
}
// -------------------------------------------------------------------------
MainWindow::MainWindow (string gladedir, string guifile, string svgdir, string mtr_prog_path, string weblog_addr, std::list<std::string> screens) :
	full_state(false),
	is_pressed(false),
	page(1)
{
	Gtk::Window splash;
	splash.modify_bg(Gtk::STATE_NORMAL, Gdk::Color("#000000"));

	Gtk::Alignment alignment(0.5,0.5,0,0);
	Gtk::ProgressBar bar;
	alignment.add(bar);
	splash.add(alignment);
	splash.set_default_size(1280,1024);
	splash.show_all();

	while(Gtk::Main::events_pending())
		Gtk::Main::iteration();
	UPostcreate<IndicatorTwoState>::reg(&widget_connect);
	UPostcreate<IndicatorFourState>::reg(&widget_connect);
	UPostcreate<GDG>::reg(&widget_connect);
	UPostcreate<VDG>::reg(&widget_connect);
	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(gladedir + guifile);
	/*Получить connector для опроса датчиков*/
	UProxyWidget *pw;
	gxml->get_widget("uproxywidget1", pw);
	connector_ = pw->get_connector();
	gxml->get_widget("window1", w);
	Gtk::Notebook *n;
	gxml->get_widget("ulocknotebook1", n);
	gxml->get_widget("ulocknotebook1", lock_notebook);
	gxml->get_widget("journalbook", jb);
	jb->set_current_page(1);
	jb->signal_switch_page().connect(sigc::mem_fun(*this, &MainWindow::page_switched));
	UJournal *uj;
	gxml->get_widget("ujournal1", uj);
	gxml->get_widget("uprinterinterface1", printer);
	sigc::slot< bool,Glib::ustring > slot = sigc::mem_fun(*printer, &UPrinterInterface::print_string);
	uj->print_.connect(slot);//Печать
	USVGText *lt, *ld, *ly;
	gxml->get_widget("label_time", lt);
	gxml->get_widget("label_date", ld);
	gxml->get_widget("label_year", ly);
	clock = new Clock(lt, ld, ly);
	gxml->get_widget("config_window", dialog);
	add_frame(dialog);
	fw = new FailureDialog(gxml);
	cc = new ClockConfig(gxml, fw, dialog);
	jd = new JournalDate(gxml, fw, dialog);
	ms = new MTRSetup(gxml, dialog, mtr_prog_path);
	Gtk::EventBox *eb;
	gxml->get_widget("eb1", eb);
	eb->signal_button_press_event().connect(sigc::mem_fun(*this, &MainWindow::pressed));
	eb->signal_button_release_event().connect(sigc::mem_fun(*this, &MainWindow::released));
	gxml->get_widget("eb2", eb);
	eb->signal_button_press_event().connect(sigc::mem_fun(*this, &MainWindow::pressed));
	eb->signal_button_release_event().connect(sigc::mem_fun(*this, &MainWindow::released));
	gxml->get_widget("eb3", eb);
	eb->signal_button_press_event().connect(sigc::mem_fun(*this, &MainWindow::pressed));
	eb->signal_button_release_event().connect(sigc::mem_fun(*this, &MainWindow::released));
	Gtk::Button *btn;
	gxml->get_widget("service_menu", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::show_conf_menu));
	gxml->get_widget("cancel_button", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::hide_conf_menu));
	gxml->get_widget("set_datetime", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*cc, &ClockConfig::show));
	gxml->get_widget("flash_op", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::save_to_flash));
	gxml->get_widget("prog_mtr", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*ms, &MTRSetup::show));
	gxml->get_widget("poweroff", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::poweroff));
	gxml->get_widget("reboot", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::reboot));
	gxml->get_widget("dump_journal", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*jd, &JournalDate::show));
	gxml->get_widget("snapshot", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::make_shot));
	gxml->get_widget("make_shot", btn);
	btn->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::make_shot));
	gxml->get_widget("checkbutton1", check_button);
	check_button->signal_toggled().connect(sigc::mem_fun(*this, &MainWindow::switch_lock));
	lock_notebook->enable_lock(!check_button->get_active());

	init_tabs(n, gladedir, svgdir, weblog_addr, bar, screens);

	w->signal_key_press_event().connect( sigc::mem_fun(*this, &MainWindow::on_key_event) );
	
	bar.set_fraction(1);
	splash.hide();
}
// -------------------------------------------------------------------------
void MainWindow::init_tabs(Gtk::Notebook *_n, string &_gladedir, string &_svgdir, string &_weblog_addr, Gtk::ProgressBar &_bar, std::list<std::string> &_screens)
{
	ScreenPacker packer(_n, _gladedir, _svgdir);

	float step = float(1)/float(_screens.size());
	int i=0;
	for(std::list<std::string >::iterator it = _screens.begin();it!=_screens.end();it++,i++)
	{
		/*    Проверка на наличие префикса NOTEBOOK_PREFIX("nb_" по-умолчанию).
		 * Данный префикс означает, что загружается glade файл с Gtk::Notebook
		 * в качестве основного экрана вместо стандартного Gtk::Fixed.
		 */
		Glib::ustring::size_type pos = it->find(NOTEBOOK_PREFIX);
		if( pos != Glib::ustring::npos && !pos )
			packer.append_notebook(it->substr(NOTEBOOK_PREFIX_SIZE));
		else
			packer.append(*it);

		_bar.set_fraction(i * step);
		while(Gtk::Main::events_pending())
			Gtk::Main::iteration();
	}

	WebKitWebView* webview;
	webview = WEBKIT_WEB_VIEW(webkit_web_view_new());
	webkit_web_view_load_uri(WEBKIT_WEB_VIEW(webview), _weblog_addr.c_str());
	Gtk::Widget* browser = Glib::wrap(GTK_WIDGET(webview));

	g_signal_connect(webview, "download-requested", G_CALLBACK
		(download_requested_cb), fw);
	packer.append_widget(browser,"database");
	browser->show();
}
// -------------------------------------------------------------------------
void MainWindow::switch_lock()
{
	lock_notebook->enable_lock(!check_button->get_active());
}
// -------------------------------------------------------------------------
void MainWindow::make_shot()
{
	using Glib::ustring;

	Glib::RefPtr<Gdk::Window> wnd = w->get_window();
	int wd, h;
	wnd->get_size(wd, h);

	/* If the dialog window was shown we first should close it then redraw the main window and be sure
	   that the process is completed to avoid artifacts (aka black rectangle at the place of the hidden window */
	if(dialog->is_visible()) {
		dialog->hide();
		wnd->invalidate_rect(Gdk::Rectangle(0, 0, wd, h), true);
		wnd->process_updates(true);
	}

	Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create((Glib::RefPtr<Gdk::Drawable>)wnd, 0, 0, wd, h);

	struct tm t;
	struct timeval tv;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &t);

	const ustring dir = getSaveDir();
	const ustring filename =
		ustring::compose("/shot%1.%2_%3.%4.%5.png",
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_mday ),
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_mon  + 1),
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_hour + 1),
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_min  + 1),
		ustring::format(std::setw(2), std::setfill(L'0'), t.tm_sec  + 1));

	try {
		pixbuf->save(dir + filename, "png");
		fw->show_simple(Glib::locale_to_utf8("Снимок экрана успешно сохранён:\n") + dir + filename);
	}
	catch (const Glib::FileError& err) {
		fw->show_simple(Glib::locale_to_utf8("Ошибка сохранения снимка экрана: \n") + err.what());
	}
	catch (const Gdk::PixbufError& err) {
		fw->show_simple(Glib::locale_to_utf8("Ошибка сохранения снимка экрана: \n") + err.what());
	}
}
// -------------------------------------------------------------------------
void MainWindow::page_switched(GtkNotebookPage *pg, guint cur_pg)
{
	if(cur_pg == 0) /* This page should never be selected */
		jb->set_current_page(page);
	else
		page = cur_pg;
}
// -------------------------------------------------------------------------
void MainWindow::show_conf_menu()
{
	dialog->show();
}
// -------------------------------------------------------------------------
void MainWindow::hide_conf_menu()
{
	dialog->hide();
}
// -------------------------------------------------------------------------
bool MainWindow::pressed(GdkEventButton *eb)
{
	is_pressed = true;
	return true;
}
// -------------------------------------------------------------------------
bool MainWindow::released(GdkEventButton *eb)
{
	if(is_pressed) {
		is_pressed = false;
		/* Here we should check if the cursor still is above the button. But this may have little sense for the touchscreen */
		cc->show();
	}
	return true;
}
// -------------------------------------------------------------------------
bool MainWindow::on_key_event(GdkEventKey *e)
{
//	cerr << "key pressed:" << e->keyval<<endl;
	if ( e->keyval == 102)
	{
		full_state ? w->fullscreen() :w->unfullscreen();
		full_state = !full_state;
	}
	return 0;
}
// -------------------------------------------------------------------------
void MainWindow::save_to_flash()
{
	std::string savedir = "/tmp";

	xmlNode* node = conf->getNode("GUI");

	if(node)
	{
		UniXML_iterator it(node);
		savedir = it.getProp("SaveDir");
	}

	string command = "/usr/bin/ctl-flash.sh --savebackup " + savedir;
	bool ok = fw->show(command.c_str(), 1, flash_messages, true);
	if(ok)
		dialog->hide();
}
// -------------------------------------------------------------------------
const char* reboot_messages[1] = {"Перезагрузка не удалась",};
// -------------------------------------------------------------------------
void MainWindow::reboot()
{
	bool ok = fw->show("/usr/bin/ctl-system.sh reboot", 1, reboot_messages);
	if(ok)
		dialog->hide();
}
// -------------------------------------------------------------------------
const char* poweroff_messages[1] = {"Выключение не удалось",};
// -------------------------------------------------------------------------
void MainWindow::poweroff()
{
	bool ok = fw->show("/usr/bin/ctl-system.sh poweroff", 1, poweroff_messages);
	if(ok)
		dialog->hide();
}
// -------------------------------------------------------------------------
