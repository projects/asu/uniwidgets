#include <Services/Miscellaneous.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace UniSetTypes;
using namespace UniWidgets;
// -------------------------------------------------------------------------
UniWidgets::UPrinterInterface* printer; /* Указатель на виджети принтер */
ConnectorRef connector_; /* Указатель на коннектор */
const char* flash_messages[2] = {"Данные сохранены на флэш-карту", "Ошибка сохранения данных"};
Gtk::Menu* popup_menu_; /* Меню печать для индикаторов */
Gtk::Menu* popup_custom_menu_; /* Меню печать для индикаторов экрана УИПС */
Glib::RefPtr<Gtk::UIManager> menu_uimanager_ref_;
Glib::RefPtr<Gtk::ActionGroup> menu_action_group_;
// -------------------------------------------------------------------------
void unset_focus(Gtk::Widget *w)
{
	w->unset_flags(Gtk::CAN_FOCUS);
}
// -------------------------------------------------------------------------
void add_frame(Gtk::Dialog *w)
{
	Gtk::VBox *thing = w->get_vbox();
	w->remove();

	Gtk::Frame *fr = new Gtk::Frame();
	Gtk::Alignment *al = new Gtk::Alignment();
	al->set_padding(5, 5, 5, 5);
	fr->add(*al);
	fr->show_all();
	al->add(*thing);

	w->add(*fr);
//	w->signal_realize().connect(sigc::bind<Gtk::Widget*>(sigc::ptr_fun(&unset_pm), w));
}
// -------------------------------------------------------------------------
Glib::ustring make_string( std::vector<UniSetTypes::ObjectId> &ids )
{
	/*Составление описания и значения индикатора*/
	Glib::ustring marked_up_form_text;
	Glib::ustring sensorName;
	long sensorValue=0;
	time_t raw_time;
	struct tm * timeinfo;
	gchar timestr[64];

	std::vector<UniSetTypes::ObjectId>::iterator it = ids.begin();
	for(;it != ids.end();++it)
	{
		xmlNode* node = conf->getXMLObjectNode(*it);
		if(!node)
			return Glib::ustring("");
		const char* text = (const char*)::xmlGetProp((xmlNode*)node, (const xmlChar*)"textname");
	
		if (text == NULL)
			return Glib::ustring("");
		
		sensorName = Glib::ustring(text);
		sensorValue = connector_->get_value(*it,UniSetTypes::DefaultObjectId);
	
		time(&raw_time);
		timeinfo = localtime(&raw_time);
		strftime(timestr, 64, "%H:%M:%S", timeinfo);

		marked_up_form_text+=Glib::ustring::compose("%1 %2 - %3\n"
							,Glib::ustring::format(std::left,timestr)
							,Glib::ustring::format(std::setw(10),std::right,sensorValue)
							,Glib::ustring::format(std::left,sensorName));
	}
	return marked_up_form_text;
}
// -------------------------------------------------------------------------
void on_print_indicator(IndicatorFourState* indicator)
{
	if(indicator)
	{
		std::vector<UniSetTypes::ObjectId> ids_;
		ids_.push_back(indicator->get_value_ai());
		const Glib::ustring str_ = make_string(ids_);
		if(printer && str_ != "")
			printer->print_string(str_);
	}
}
// -------------------------------------------------------------------------
void on_print_indicator(IndicatorTwoState* indicator)
{
	if(indicator)
	{
		std::vector<UniSetTypes::ObjectId> ids_;
		ids_.push_back(indicator->get_value_ai());
		const Glib::ustring str_ = make_string(ids_);
		if(printer && str_ != "")
		{
			printer->print_string(str_);
		}
	}
}
// -------------------------------------------------------------------------
void on_print_indicator(GDG* gdg)
{
	if(gdg)
	{
		std::vector<UniSetTypes::ObjectId> ids_;
		ids_.push_back(gdg->get_indicator_u_ai());
		ids_.push_back(gdg->get_indicator_p_ai());
		ids_.push_back(gdg->get_indicator_f_ai());
		ids_.push_back(gdg->get_indicator_i_ai());
		const Glib::ustring str_ = make_string(ids_);
		if(printer && str_ != "")
			printer->print_string(str_);
	}
}
// -------------------------------------------------------------------------
void on_print_indicator(VDG* vdg)
{
	if(vdg)
	{
		std::vector<UniSetTypes::ObjectId> ids_;
		ids_.push_back(vdg->get_indicator_u_ai());
		ids_.push_back(vdg->get_indicator_p_ai());
		ids_.push_back(vdg->get_indicator_f_ai());
		const Glib::ustring str_ = make_string(ids_);
		if(printer && str_ != "")
			printer->print_string(str_);
	}
}
// -------------------------------------------------------------------------
template<typename W>
void make_object_menu(W* w)
{
	/* Create actions */
	void (*ptr)(W* w);
	ptr = &on_print_indicator;
	menu_action_group_ = Gtk::ActionGroup::create();
	menu_action_group_->add(Gtk::Action::create("Print", "Печать"),
						sigc::bind(ptr, w));

	/* Create menu content */
	Glib::ustring ui_info;
	ui_info.append(
	  "<ui>"
	  "  <popup name='PopupMenu'>"
	  "    <menuitem action='Print'/>"
	  "    <separator/>");
	ui_info.append(
	  "  </popup>"
	  "</ui>");

	/* Create popup menu object */
	menu_uimanager_ref_ = Gtk::UIManager::create();
	menu_uimanager_ref_->insert_action_group(menu_action_group_);

	menu_uimanager_ref_->add_ui_from_string(ui_info);
	popup_menu_ = dynamic_cast<Gtk::Menu*>(
	menu_uimanager_ref_->get_widget("/PopupMenu"));
}
// -------------------------------------------------------------------------
template<typename W>
bool on_print_press_event(GdkEventButton* event, W* w)
{
	if ((event->type == GDK_BUTTON_RELEASE))// && (event->button == 1))
	{
		/* Make menu for selected object */
		make_object_menu(w);

		/* Show object popup menu */
		popup_menu_->popup(event->button, event->time);
		return true;
	}
	return false;
}
// -------------------------------------------------------------------------
template<typename W>
void widget_connect(W* widget)
{
	widget->button_release_conn_ = widget->signal_button_release_event().connect(sigc::bind(&on_print_press_event<W>, widget));
}
// -------------------------------------------------------------------------
void switch_to(GtkWidget *w, gpointer user_data)
{
	gint page;
	g_object_get(G_OBJECT(w), "page", &page, NULL);
	Gtk::Notebook* nbook = Glib::wrap( GTK_NOTEBOOK(user_data) );
	nbook->set_current_page( page );
}
// -------------------------------------------------------------------------
template void widget_connect(IndicatorTwoState* widget);
template void widget_connect(IndicatorFourState* widget);
template void widget_connect(GDG* widget);
template void widget_connect(VDG* widget);
// -------------------------------------------------------------------------
