#include <Services/ScreenPacker.h>
#include <Services/Miscellaneous.h>

using namespace std;
using namespace UniSetTypes;
using namespace UniWidgets;
// -------------------------------------------------------------------------
#define SHADOW_WIDTH 3
#define SHADOW_HEIGHT 47
#define SHADOW_XOFFSET 1097
#define SHADOW_YOFFSET 3
#define SEPARATION 8
// -------------------------------------------------------------------------
extern ConnectorRef connector_;
// -------------------------------------------------------------------------
ScreenPacker::ScreenPacker(Gtk::Notebook* n, const Glib::ustring& path, const Glib::ustring& svgdir) :
										nbook (n),
										gladedir(path),
										svgdir_(svgdir),
										scrnum(0)
{
}
// -------------------------------------------------------------------------
void ScreenPacker::append_widget(Gtk::Widget *w, const Glib::ustring& tabname)
{
	Glib::RefPtr<Gnome::Glade::Xml> tabsxml = Gnome::Glade::Xml::create(gladedir + "tabs.glade", string("tab_") + tabname);
	Gtk::Widget *tab;
	tabsxml->get_widget(string("tab_") + tabname, tab);

	nbook->append_page(*w, *tab);
}
// -------------------------------------------------------------------------
void ScreenPacker::append(const Glib::ustring& tabname)
{
	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(gladedir + tabname + ".glade", tabname +"_fixed1");
	Gtk::Fixed* screen;
	gxml->get_widget(tabname + "_fixed1", screen);

	Gtk::Widget *shadow = new USVGImage(svgdir_ + "gui/shadow.svg");
	shadow->show();
	shadow->set_size_request(SHADOW_WIDTH, SHADOW_HEIGHT);
	screen->put(*shadow, SHADOW_XOFFSET, (SHADOW_HEIGHT + SEPARATION) * (scrnum++) + SHADOW_YOFFSET);
	
	Glib::RefPtr<Gnome::Glade::Xml> tabsxml = Gnome::Glade::Xml::create(gladedir + "tabs.glade", string("tab_") + tabname);
	Gtk::Widget *tab;
	tabsxml->get_widget(string("tab_") + tabname, tab);

	nbook->append_page(*screen, *tab);
}
// -------------------------------------------------------------------------
void ScreenPacker::append_notebook(const Glib::ustring& tabname)
{
	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(gladedir + tabname + ".glade", tabname +"_notebook1");
	Gtk::Notebook* sub_book;
	gxml->get_widget(tabname + "_notebook1", sub_book);
	
	Gtk::Fixed* first;
	Gtk::Fixed* last;
	gxml->get_widget(tabname + "_fixed1", first);
	gxml->get_widget(tabname + "_signals_fixed1", last);
	Gtk::Widget *shadow_1 = new USVGImage(svgdir_ + "gui/shadow.svg");
	Gtk::Widget *shadow_2 = new USVGImage(svgdir_ + "gui/shadow.svg");
	shadow_1->show();
	shadow_1->set_size_request(SHADOW_WIDTH, SHADOW_HEIGHT);
	shadow_2->show();
	shadow_2->set_size_request(SHADOW_WIDTH, SHADOW_HEIGHT);
	first->put(*shadow_1, SHADOW_XOFFSET, (SHADOW_HEIGHT + SEPARATION) * scrnum + SHADOW_YOFFSET);
	last->put(*shadow_2, SHADOW_XOFFSET, (SHADOW_HEIGHT + SEPARATION) * (scrnum++) + SHADOW_YOFFSET);


	Glib::RefPtr<Gnome::Glade::Xml> tabsxml = Gnome::Glade::Xml::create(gladedir + "tabs.glade", string("tab_") + tabname);
	Gtk::Widget *tab;
	tabsxml->get_widget(string("tab_") + tabname, tab);
	nbook->append_page(*sub_book, *tab);
	glade_xml_signal_connect_data(gxml->gobj(), 
			"switch_page",
			G_CALLBACK(switch_to),
			sub_book->gobj());

}
// -------------------------------------------------------------------------
