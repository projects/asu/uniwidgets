#include <iostream>
#include <sstream>
#include <getopt.h>
#include <glade/glade.h>
#include <libglademm.h>
#include <Configuration.h>
#include <include/Services/MainWindow.h>

// --------------------------------------------------------------------------
using namespace UniSetTypes;
using namespace std;
// --------------------------------------------------------------------------

static struct option longopts[] = {
	{ "help", no_argument, 0, 'h' },
	{ "pages", required_argument, 0, 'p' },
	{ "svg-dir", required_argument, 0, 's' },
	{ "guifile", required_argument, 0, 'u' },
	{ "glade-dir", required_argument, 0, 'g' },
	{ "confile", required_argument, 0, 'c' },
	{ "gtkthemedir", required_argument, 0, 't' },
	{ "weblog-addr", required_argument, 0, 'w' },
	{ NULL, 0, 0, 0 }
};

std::list<std::string> screens;

static void short_usage()
{
	printf("[-h|--help]			    	- Print this message.\n");
	printf("[-p|--pages] page1,page2,...,pageN	- Pages or name of glade-files.\n");
	printf("[-s|--svg-dir] /path/to/svg_dir/	- Path to directory with svg-files.\n");
	printf("[-u|--guifile] gui.glade		- Name of gui file.\n");
	printf("[-g|--glade-dir] /path/to/glade-dir/	- Path to directory with glade-files.\n");
	printf("[-c|--confile] /path/to/configure.xml	- Path to configure.xml.\n");
	printf("[-t|--gtkthemedir] /path/to/gtkrc/	- Gtkrc theme directory.\n");
	printf("[-w|--weblog-addr] http://localhost     - Weblog addres of server.\n");
}
// --------------------------------------------------------------------------
int main(int argc, char** argv)
{
	try
	{
		if( argc > 1 && !strcmp(argv[1],"--help") )
		{
			short_usage();
			return 0;
		}

		Gtk::Main Kit(argc, (char**&)argv);

		if (!g_thread_supported())
			g_thread_init(NULL);

		string confile;
		string theme;
		string guifile;
		string gladedir;
		string svgdir;
		string weblog_addr;

		int optindex = 0;
		int opt;

		while( (opt = getopt_long(argc, argv, "hc:g:p:s:t:u:w:",longopts,&optindex)) != -1 ) 
		{
			switch (opt) 
			{
				case 'h':
					short_usage();
				return 0;

				case 'c':	
					confile = string(optarg);
					conf = new Configuration(argc, argv, confile);
				break;

				case 'g':	
					gladedir = string(optarg);
				break;

				case 'p':	
					screens = explode_str(string(optarg),',');
				break;

				case 's':	
					svgdir = string(optarg);
				break;

				case 't':	
					theme = string(optarg);
					if ( !theme.empty() )
						gtk_rc_parse( (theme + "/gtkrc").c_str() );
				break;

				case 'u':	
					guifile  = string(optarg);
				break;

				case 'w':	
					weblog_addr  = string(optarg);
				break;

// 				case '?':
// 				default:
// // 					printf("? argumnet\n");
// // 					return 0;
			}
		}
		if(confile == "" || guifile == "" || gladedir == "")
		{
			short_usage();
			return 1;
		}

		MainWindow mw(gladedir, guifile, svgdir, conf->getDataDir(), weblog_addr, screens);
		Kit.run(*mw.w);
	}
	catch(Exception& ex)
	{
		cerr << "(GUI-Starter::main): " << ex << endl;
	}
	catch(...)
	{
		cerr << "(GUI-Starter::main): catch ..." << endl;
	}

	return 0;
}
