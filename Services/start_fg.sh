#!/bin/sh

export LIBGLADE_MODULE_PATH=../plugins/libglade/.libs

ulimit -Sc 1000000

uniset-start.sh -f ./uniwidgets-gui-starter --confile ~/Projects/yauza/conf/configure.xml \
					    --svg-dir ~/Projects/yauza/src/GUI/19/svg/ \
					    --guifile gui.glade \
					    --glade-dir ~/Projects/yauza/src/GUI/19/ \
					    --pages lubr_oil,cool_gdg,fuel,pneumo,boiler,waters,overall,shaft,ses,ses2,ged,fire_air_cond,nb_ksu,drainage \
					    --weblog-addr http://localhost:8000/ \
					    --gtkthemedir ~/Projects/yauza/src/GUI/theme/
#--dlog-add-levels info,warn,crit \
#--unideb-add-levels info,warn,crit \
#--cbid 51  \