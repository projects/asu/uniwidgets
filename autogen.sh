#! /bin/sh

# If needed, run autoreconf -fiv manually and commit all files
CXXFLAGS=
LDFLAGS=
PROJECT_PATH=/srv/$USER/Projects
UNIWIDGETS_PATH=$PROJECT_PATH/uniwidgets
UWUNIWIDGETS_PATH=$UNIWIDGETS_PATH/uniwidgets

TryExport()
{
	EXPORTPATH=$(readlink -f $1)
	test -r $1/$2.include && . $1/$2.include
}

# if local arg, build with libraries from git tree
if [ "$1" = "local" ] ; then
	shift
# 	TryExport ../uniset libUniSet
#	TryExport ../oscillograph-gtkmm liboscillograph-gtkmm
fi

# We run just autoreconf, updates all needed
autoreconf -fiv

# run configure in anyway
./configure --enable-maintainer-mode --prefix=/usr $*

