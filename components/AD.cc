#include <types.h>
#include "AD.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
static const double LENGHT_ARROW = 0.43;
static const double ARROW_ROOT = 0.5;
static const double FAT_ARROW = 0.9;
static const double X_DIGITVAL = 0.45;
static const double Y_DIGITVAL = 0.72;
static const double X_LABEL = 0.36;
static const double Y_LABEL = 0.92;
static const double LENGHT_SCALEMARK = 0.87;
static const double LENGHT_SMALL_SCALEMARK = 0.93;
static const double RADIUS = 0.45;
static const double SCALE_RANGE = 3*3.1415/2;
static const double SCALE_SHIFT = 3.1415/4;
// -------------------------------------------------------------------------
#define AD_NAME_DEVICE    "name-device"
#define AD_MIN_VAL    "min-value"
#define AD_MAX_VAL    "max-value"
#define AD_STEP_VAL    "step-value"
#define AD_LAG      "lag"
// -------------------------------------------------------------------------
#define INIT_AD_PROPERTIES() \
  name_device(*this, AD_NAME_DEVICE, "dev")  \
  ,min_value(*this, AD_MIN_VAL, 0) \
  ,max_value(*this, AD_MAX_VAL, 100) \
  ,step_value(*this, AD_STEP_VAL, 10) \
  ,lag(*this, AD_LAG , 6)
// -------------------------------------------------------------------------
//Поиск пересечений с имеющимися секциями
class FindCrossColorSect: public binary_function< sects*, pair<int, int>, bool>
{
public:
  bool operator() ( sects* sector, pair<int, int> sect ) const
  {
    if(sect.first >= sector->endBorder && sect.second >= sector->endBorder)
      return false;
    else if(sect.first <= sector->startBorder && sect.second <= sector->startBorder)
      return false;
    return true;
  }
};
// -------------------------------------------------------------------------
//Поиск одинаковых секций
class FindSameColorSect: public binary_function< sects*, pair<int, int>, bool>
{
public:
  bool operator() ( sects* sector, pair<int, int> sect ) const
  {
    if(sector->startBorder == sect.first && sector->endBorder == sect.second)
      return true;
    else if(sector->endBorder == sect.first && sector->startBorder == sect.second)//перевернутый отрезок
      return true;
    return false;
  }
};
// -------------------------------------------------------------------------
void AD::on_settings_changed()
{
  facial=get_name_device();
  devName=get_name_device();
  minp=get_min_value();
  maxp=get_max_value();
  step=get_step_value();
  inerc=get_lag();
}
// -------------------------------------------------------------------------
void AD::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<AD>;

  connect_property_changed(AD_NAME_DEVICE, sigc::mem_fun(*this, &AD::on_settings_changed) );
  connect_property_changed(AD_MIN_VAL, sigc::mem_fun(*this, &AD::on_settings_changed) );
  connect_property_changed(AD_MAX_VAL, sigc::mem_fun(*this, &AD::on_settings_changed) );
  connect_property_changed(AD_STEP_VAL, sigc::mem_fun(*this, &AD::on_settings_changed) );
  connect_property_changed(AD_LAG, sigc::mem_fun(*this, &AD::on_settings_changed) );

  this->set_size_request((int)width,(int)height);

  GoDynamics();
}
// -------------------------------------------------------------------------
AD::AD(Gtk::DrawingArea::BaseObjectType* gobject) :
  Gtk::DrawingArea(gobject),
  value(0),
  x_pos(0),
  y_pos(0),
  facial("device"),
  devName("device"),
  minp(0),maxp(100),step(20),
  squareside(0),
  width(200),
  height(200),
  redrawTime(60),
  inerc(3),
  gg(true),
  redr(true),
  glo(false),
  secCnt(0),
  pFont("Liberation Sans"),
  SMALL_SCALEMARKS_NUMBER(5),  SCALEMARKS_FONTSIZE(14),
  DIGITVAL_FONTSIZE(24),  LABEL_FONTSIZE(16),
  SCALEMARK_WIDTH(3), COLORLINE_WIDTH(16),
  INIT_AD_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
AD::AD():
  Glib::ObjectBase("ad"),
  value(0),
  x_pos(0),
  y_pos(0),
  facial("device"),
  devName("device"),
  minp(0),maxp(100),step(20),
  squareside(0),
  width(200),
  height(200),
  redrawTime(60),
  inerc(3),
  gg(true),
  redr(true),
  glo(false),
  secCnt(0),
  pFont("Liberation Sans"),
  SMALL_SCALEMARKS_NUMBER(5),  SCALEMARKS_FONTSIZE(14),
  DIGITVAL_FONTSIZE(24),  LABEL_FONTSIZE(16),
  SCALEMARK_WIDTH(3), COLORLINE_WIDTH(16),
  INIT_AD_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
AD::~AD()
{
}
// -------------------------------------------------------------------------
void AD::on_size_allocate(Gtk::Allocation& allocation)
{
  Gtk::DrawingArea::on_size_allocate(allocation);
}
// -------------------------------------------------------------------------
bool AD::on_expose_event(GdkEventExpose* event)
{
  Glib::RefPtr<Gdk::Window> window = get_window();
  if(window)
  {
    Gtk::Allocation allocation = get_allocation();
    width = allocation.get_width();
    height = allocation.get_height();

    int temp=0;
    temp = allocation.get_x();
//     if(x_pos != temp)
//     {
//       x_pos = temp;
//       redr = true;
//     }
//         
//     temp = allocation.get_y();
//     if(y_pos != temp)
//     {
//       y_pos = temp;
//       redr = true;
//     }

    temp = height < width ? height : width;
    if(squareside != temp)
    {
      squareside = temp;
      redr = true;
    }

    sIndex=squareside/250; /*!!!!!!!!!!*/
  
    Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();
// minp - минимальное значение отображаемого параметра
// maxp - максимальное значение оного
// step - шаг нанесения больших рисок (чисел) на шкалу
// пример : от -600 до 600, шаг 200
// на шкале отобразятся:  -600 -400 -200  0  200  400  600

    cr->rectangle(x_pos,y_pos,squareside,squareside);
    cr->clip();

//    попытка улучшить производительность
    if(glo)
    {
      if(redr)
      {
        cr->push_group();
        ScaleInit(cr);//рисуем шкалу
        scalePatt = cr->pop_group();
        
        cr->push_group();
        GlassDrawing(cr);
        BlickDrawing(cr);
        glassPatt = cr->pop_group();
        redr = false;
//         cout<<devName<<": redr"<<endl;
      }
      cr->save();
      cr->set_source(scalePatt);
      cr->paint();
      cr->stroke();
      cr->restore();
    }
    else
      ScaleInit(cr);//рисуем шкалу

    //инерционность стрелки прибора
    dValue=(value-prevValue)/inerc;
    aValue=prevValue+dValue;
    prevValue=aValue;
    
    //реализация процесса зашкаливания в двух направлениях
    // в оригинале max & min делились на номинал
    overshoot=false;

    if(aValue>maxp)
    {
      aValue = gg ? maxp*1.007 : maxp*1.002;
      prevValue=aValue;
      gg=!gg;
      overshoot=true;
    }
    else if(aValue<minp)
    {
      if(gg)
        aValue=minp*1.002;
      else 
        aValue = minp ? minp*1.007 : -0.1;

      prevValue=aValue;
      gg=!gg;
      overshoot=true;
    }

    ArrowDrawing(cr);

    if(glo)
    {
      cr->save();
      cr->set_source(glassPatt);
      cr->paint();
      cr->stroke();
      cr->restore();
    }
    else
    {
      GlassDrawing(cr);
      BlickDrawing(cr);
    }
  }
  return Gtk::DrawingArea::on_expose_event(event);
}
//--------------------------------------------------------------------------------
void AD::ArrowDrawing(Cairo::RefPtr<Cairo::Context>& cr)
{
  float lengtArrow = LENGHT_ARROW*squareside;//длина стрелки
  float phia = SCALE_RANGE * ( (aValue-minp)/(maxp-minp) );
  if(phia<0) 
    phia=0;
  float center = ARROW_ROOT*squareside; //начало стрелки
  float x1_new = center + x_pos;
  float y1_new = center + y_pos;
  float x2_new = center - lengtArrow*cos( phia - SCALE_SHIFT ) + x_pos;
  float y2_new = center - lengtArrow*sin( phia - SCALE_SHIFT ) + y_pos;

  cr->set_line_cap(Cairo::LINE_CAP_ROUND);

  // рисуем cтрелку
  cr->set_line_width(1);
  cr->move_to( x1_new, y1_new );
  cr->line_to( x2_new, y2_new );
  cr->stroke(); //тонкая часть

  float x2_n = center - FAT_ARROW*lengtArrow*cos( phia - SCALE_SHIFT ) + x_pos;
  float y2_n = center - FAT_ARROW*lengtArrow*sin( phia - SCALE_SHIFT ) + y_pos;

  cr->set_line_width(3);
  cr->move_to( x1_new, y1_new );
  cr->line_to( x2_n, y2_n );
  cr->stroke();  //толстая

  /*Выводим числовое значениек датчика*/
//   char digitValue[10];
//   sprintf(digitValue,"%02d",value);
  Glib::ustring digitValue = Glib::ustring::format(std::setfill(L'0'), std::setw(2), value );

  if(overshoot)
    cr->set_source_rgba(1, 0, 0, 1);   // red

  cr->set_font_size( DIGITVAL_FONTSIZE*sIndex );
  cr->move_to( X_DIGITVAL*squareside + x_pos, Y_DIGITVAL*squareside + y_pos );
  cr->show_text(digitValue);
  cr->stroke();
}
//------------------------------------------------------------------------------
void AD::ScaleInit(Cairo::RefPtr<Cairo::Context>& cr)
{//рисуем шкалу
    //всю поверхность красим в черное
    cr->set_source_rgba(0, 0, 0, 1);//black - the colour of the pen
    cr->paint();
    cr->stroke();
    
    //заливаем белым прямоугольник
    cr->rectangle( squareside/30 + x_pos, squareside/30 + y_pos,
            squareside - 2*squareside/30, squareside - 2*squareside/30 );
    cr->clip();
    cr->set_source_rgba(1, 1, 1, 1);
    cr->paint();
    cr->stroke();

    //Рисуем тень рамки
    cr->set_source_rgba(0, 0, 0, 0.3);    
    cr->set_line_width(6);
    cr->move_to( squareside/30 + x_pos, squareside + y_pos );
    cr->line_to( squareside/30 + x_pos, squareside/30 + y_pos );
    cr->line_to( squareside + x_pos, squareside/30 + y_pos );
    cr->stroke();
  
    //пишем какое-нить слово
    cr->set_source_rgba(0, 0, 0, 1);   // black
    cr->set_font_size( LABEL_FONTSIZE*sIndex );  
    cr->move_to( X_LABEL*squareside + x_pos, Y_LABEL*squareside + y_pos );
    cr->show_text(facial);
    cr->stroke();

    //красим сектора
    ColorizeSectors(cr);

    //начинаем рисовать риски
    static const int tstep[]={10,5,6,4,3,7,2,8,9};
    int i, bigMax=5;
    double phi, lc=LENGHT_SCALEMARK;//lc - длина риски шкалы
  
    if ( maxp*minp < 0 ) 
      bigMax=6;
      
    for ( i = 0; i < 9 && ( step != (maxp-minp)/bigMax ); i++ )
      bigMax = tstep[i];

    int small = 1,big = bigMax;// big - счетчик больших черточек small - счетчик маленьких черточек
        
    float otstoys = RADIUS*squareside;//радиус шкалы
    float center = ARROW_ROOT*squareside;
        // далее идет расчет координат концов рисок (в зависимости от угла j (phi))
        // через каждые пять маленьких рисок (lc=0.95) идет большая (lc=0.9)

    cr->set_source_rgba(0, 0, 0, 1);
    float x1s,y1s,x2s,y2s;
    for( phi = 0; phi <= SCALE_RANGE; phi += SCALE_RANGE/(SMALL_SCALEMARKS_NUMBER*bigMax) )
    {
      x1s = ( ( center - otstoys*cos( phi - SCALE_SHIFT ) ) + x_pos );
      y1s = ( ( center - otstoys*sin( phi - SCALE_SHIFT ) ) + y_pos );
      x2s = ( ( center - (otstoys*lc)*cos( phi - SCALE_SHIFT ) ) + x_pos );
      y2s = ( ( center - (otstoys*lc)*sin( phi - SCALE_SHIFT ) ) + y_pos );
  
      cr->set_line_width(SCALEMARK_WIDTH);
      cr->move_to(x1s,y1s);
      cr->line_to(x2s,y2s);
      cr->stroke();

      if( lc == LENGHT_SCALEMARK )
      {
//         char szBuf[25];
//         sprintf( szBuf,"%d",(int)( maxp-(big--)*step ) );
// 
// //        cr->select_font_face("Arial 16");
//         cr->move_to( x2s - 14*sIndex*sin(phi) + 4,
//                y2s + 10*sIndex*sin(phi) + 7*sIndex );
//         cr->set_font_size( sIndex*SCALEMARKS_FONTSIZE );
//         cr->show_text(szBuf);
//         cr->stroke();
//         char szBuf[25];
//         sprintf( szBuf,"%d",(int)( maxp-(big--)*step ) );
        Glib::ustring szBuf = Glib::ustring::compose("%1",(int)( maxp-(big--)*step ));

//        cr->select_font_face("Arial 16");
        if(phi==0)
          cr->move_to(x2s,y2s);
        else
        {
          if(szBuf == "0")
          {
            cr->move_to( x2s-54*sIndex*sin(phi*0.15)+15*sIndex,
                y2s+16*sIndex*sin(phi*0.66)-3);
          }
          else
            cr->move_to( x2s-54*sIndex*sin(phi*0.15)+8*sIndex,
                y2s+16*sIndex*sin(phi*0.66)-3);
        }
        cr->set_font_size(sIndex*SCALEMARKS_FONTSIZE);
        cr->show_text(szBuf);
        cr->stroke();
      }

      lc=LENGHT_SMALL_SCALEMARK;

      if( small == SMALL_SCALEMARKS_NUMBER && phi != 0)
      {
        small=0;
        lc=LENGHT_SCALEMARK;
      }
         small++;
    }

    cr->rectangle(x_pos,y_pos,squareside,squareside);
    cr->clip();
}
//------------------------------------------------------------------------------
void AD::ColorizeSectors(Cairo::RefPtr<Cairo::Context>& cr)
{
  float theta1,theta2;

  float otstoys = RADIUS*squareside;//радиус шкалы
  float center = ARROW_ROOT*squareside;

  sectListIterator it;
  for(it = sectList.begin(); it != sectList.end(); ++it)
  {
    theta1 = SCALE_RANGE*(( (float)(*it)->startBorder - minp )/( maxp - minp ) );
    theta2 = SCALE_RANGE*(( (float)(*it)->endBorder - 
                (float)(*it)->startBorder )/( maxp - minp ) );
    cr->set_source_rgba((*it)->red, (*it)->green, 
              (*it)->blue, (*it)->alfa);
        
    if((*it)->solid)
    {
      cr->set_line_width(0);
      cr->arc( center + x_pos, center + y_pos,
         otstoys, M_PI + theta1 - SCALE_SHIFT, M_PI + theta2 + theta1 - SCALE_SHIFT );
      cr->line_to( center + x_pos, center + y_pos );
      cr->close_path();
      cr->fill_preserve();
    }
    else
    {
      cr->set_line_width(COLORLINE_WIDTH*sIndex);
      cr->arc(center+x_pos, center+y_pos, 
         otstoys-8*sIndex, M_PI+theta1 - SCALE_SHIFT, M_PI+theta2+theta1 - SCALE_SHIFT);
    }
    cr->stroke();
  }
}
//------------------------------------------------------------------------------
void AD::GlassDrawing(Cairo::RefPtr<Cairo::Context>& cr)
{
/*  cr->rectangle(x_pos,y_pos,squareside,squareside);
  cr->clip();
*/
  //штука, которая закрывает основание стрелки
  /*cr->arc( squareside + x_pos, squareside + y_pos, squareside*0.3, -(M_PI/2), M_PI );
  cr->close_path();
  cr->fill_preserve();
  cr->stroke();*/
        float centerxy=ARROW_ROOT*squareside; 
  cr->set_source_rgba(0, 0, 0, 0.2);
  cr->arc(centerxy+x_pos+1,centerxy+y_pos+1,
        (int)squareside*0.1,0,2*M_PI);
  cr->fill_preserve();
  cr->stroke();    

  cr->set_source_rgba(1, 1, 1, 1);
  cr->arc(centerxy+x_pos,centerxy+y_pos,
        (int)squareside*0.09,0,2*M_PI);
  cr->fill_preserve();
  cr->stroke();    

  cr->set_source_rgba(0.3, 0.3, 0.3, 1);
  cr->arc(centerxy+x_pos,centerxy+y_pos,
        (int)squareside*0.01,0,2*M_PI);
  cr->fill_preserve();
  cr->stroke();
}
//------------------------------------------------------------------------------
void AD::BlickDrawing(Cairo::RefPtr<Cairo::Context>& cr)
{
/*
  radgrad = Cairo::RadialGradient::create(0.1*squareside+x_pos,0.2*squareside+y_pos,0.05*squareside,
                        0.3*squareside+x_pos,0.3*squareside+y_pos,0.4*squareside);
  radgrad->add_color_stop_rgba(0, 1, 1, 1, 0.8);
  radgrad->add_color_stop_rgba(0.3, 1, 1, 1, 0.2);                              
  radgrad->add_color_stop_rgba(1, 1, 1, 1, 0);

  cr->save();
  cr->set_source(radgrad);
  cr->paint();
  cr->stroke();
  cr->restore();
*/
}
//------------------------------------------------------------------------------
void AD::setFacialString(string newFacial)
{
  facial = newFacial;
  redr = true;
}
//------------------------------------------------------------------------------
void AD::setValue(int val)
{
  value = val;
  GoDynamics();
  
}
//------------------------------------------------------------------------------
void AD::setScale(int newMin, int newMax, float newStep)
{
  minp = newMin;
  maxp = newMax;
  step = newStep;
  redr = true;
}
//------------------------------------------------------------------------------
void AD::setScaleMarksNumber(int num)
{
  SMALL_SCALEMARKS_NUMBER = num;
  redr = true;
}
//------------------------------------------------------------------------------
bool AD::Redraw(void)
{
  this->queue_draw();

  if( abs(value-aValue) <= 0.004*(maxp-minp))
    return false;

    return true;
}
//------------------------------------------------------------------------------
void AD::GoDynamics(void)
{
#if GTK_VERSION_GE(2,20)
  if( !this->get_realized() )
#else
  if( !this->is_realized() )
#endif
    return;
  tmrConn.disconnect();
  tmrConn = Glib::signal_timeout().connect(sigc::mem_fun(*this, &AD::Redraw), redrawTime);
}
//------------------------------------------------------------------------------
void AD::setInerc(float inerciya)
{
  if( inerciya < 1.1 )
    inerc = 1.1;
  else
    inerc = inerciya>15 ? 15 : inerciya;
}
//------------------------------------------------------------------------------
void AD::setRedrawTime(int rTime)
{
  if( rTime < 20 )
    redrawTime = 20;
  else
    redrawTime = rTime>1000 ? 1000 : rTime;
}
//------------------------------------------------------------------------------
void AD::addSect(int startBorderN,int endBorderN,
      double redN,double greenN, double blueN,double alfaN,bool solidN)
{
  /*Создание новой пары значений для передачи в алгоритмы проверок.
    Предполагается, что сектор обозначается как интервал "start end ...",где
    start < end. Например: 0 60 ..., -60 20 ... и т.п.*/
  pair<int, int> new_sect(startBorderN,endBorderN);

  sectListIterator it =
    find_if(sectList.begin(), sectList.end(), bind2nd(FindSameColorSect(), new_sect));

  if( it != sectList.end() )
  {
    //Обновляем существующий
    (*it)->startBorder = startBorderN;
    (*it)->endBorder = endBorderN;  
    (*it)->red = redN;
    (*it)->green = greenN;
    (*it)->blue = blueN;
    (*it)->alfa = alfaN;
    (*it)->solid = solidN;  
    return;
  }

  /*Проверка на пересечение секторов, если пересечение с имеющимся в списке сектором есть
    ,то новый сектор не рисуется*/
  sectListIterator it_cross =
    find_if(sectList.begin(), sectList.end(), bind2nd(FindCrossColorSect(), new_sect));

  if(it_cross != sectList.end())
    return;

  //Добавляем новый сектор
  sects *sect = new sects();
  sect->startBorder = startBorderN;
  sect->endBorder = endBorderN;  
  sect->red = redN;
  sect->green = greenN;
  sect->blue = blueN;
  sect->alfa = alfaN;
  sect->solid = solidN;

  sectList.push_back(sect);
/*  sectList[secCnt].startBorder = startBorderN;
  sectList[secCnt].endBorder = endBorderN;  
  sectList[secCnt].red = redN;
  sectList[secCnt].green = greenN;
  sectList[secCnt].blue = blueN;
  sectList[secCnt].alfa = alfaN;
  sectList[secCnt++].solid = solidN;*/
  redr = true;
}
//------------------------------------------------------------------------------
void AD::delLastSect()
{
  secCnt--;
  redr = true;
}
//------------------------------------------------------------------------------
void AD::cleanSect()
{
  sectList.clear();
  redr = true;
}
//------------------------------------------------------------------------------
void AD::setMarksFontSize(int num)
{
  SCALEMARKS_FONTSIZE = num;
  redr = true;
}
//------------------------------------------------------------------------------
void AD::setDigitFontSize(int num)
{
  DIGITVAL_FONTSIZE = num;
  redr = true;
}
//------------------------------------------------------------------------------
void AD::setLabelFontSize(int num)
{
  LABEL_FONTSIZE = num;
  redr = true;
}
//------------------------------------------------------------------------------
void AD::setScaleMarkWidth(int pix)
{
  SCALEMARK_WIDTH = pix;
  redr = true;
}
//------------------------------------------------------------------------------
void AD::setColorLineWidth(int pix)
{
  COLORLINE_WIDTH = pix;
  redr = true;
}
//------------------------------------------------------------------------------
