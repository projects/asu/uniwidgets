#ifndef _AD_H
#define _AD_H
// -------------------------------------------------------------------------
/*! \author Pavel Ponamarev */
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <iomanip>
#include <plugins.h>
#include <UDefaultFunctions.h>
#include <SensorProp.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*! \brief Вспомогательная структура.
 *  \par
 *  Cтруктура инициализации цветного сектора.
*/
struct sects
{  
  int startBorder;        /*!< начало сектора */
  int endBorder;          /*!< конец сектора */
  double red;          /*!< уровень красного */
  double green;          /*!< зеленого */
  double blue;          /*!< синего */
  double alfa;          /*!< альфа-канал (непрозрачность) */
  bool solid;          /*!< флаг заливки всего сектора */
};
/*!
 * \brief Аналоговый индикатор.
 * \par
 * Компонент отображения аналогового индикатора. Позволяет выводить значение
 * аналогового датчика в виде стрелочного индикатора.
 * Данный вид индикатора отображается в виде квадратного циферблата со стрелкой
 * по центру и шкалой в виде разомкнутого снизу круга.
*/
class AD : public Gtk::DrawingArea
{

public:

  AD();
  explicit AD(Gtk::DrawingArea::BaseObjectType* gobject);
  virtual ~AD();

  void setValue(int val);          /*!< передача прибору нового значения */
  void setScale(int minp, int maxp, float step);    /*!< установка шкалы (default 0,100,20) */
  void setScaleMarksNumber(int num);      /*!< количество маленьких рисок между большими */

  void setFacialString(std::string newFacial);    /*!< строка на лицевой панели прибора (default Facial) */
  void setInerc(float inerciya);        /*!< инерционность прибора (default 3) */

  void setRedrawTime(int rTime);        /*!< период перерисовки в мс. (default 60), влияет на инерционность */
  void addSect(int startBorder,int endBorder,
  double red,double green,
  double blue,double alfa, bool solid);   /*! добавление цветного сектора */
  void cleanSect();          /*! очистка всех секторов */
  void delLastSect();
  void setMarksFontSize(int num);        /*!< размер шрифта шкалы */
  void setDigitFontSize(int num);        /*!< размер шрифта числового значения */
  void setLabelFontSize(int num);        /*!< размер шрифта надписи */
  void setScaleMarkWidth(int pix);      /*!< толщина рисок */
  void setColorLineWidth(int pix);      /*!< толщина цветной линии */
  void setGLO(bool _glo){glo = _glo;}      /*!< функция выставления переменной оптимизации отрисовки - glo*/

protected:

  void GoDynamics(void);          /*!< фунции перерисовки */  
  bool Redraw(void);
  bool on_expose_event(GdkEventExpose* event);
  void on_settings_changed();
  void on_size_allocate(Gtk::Allocation& allocation);

  
  virtual void ScaleInit(Cairo::RefPtr<Cairo::Context>& cr);  /*! инициализация шкалы */
  virtual void ColorizeSectors(Cairo::RefPtr<Cairo::Context>& cr);/*! раскраска секторов */
  virtual void ArrowDrawing(Cairo::RefPtr<Cairo::Context>& cr);  /*! рисование стрелочки */
  virtual void GlassDrawing(Cairo::RefPtr<Cairo::Context>& cr);  /*! рисование верхнего слоя (стеклышка) */
  virtual void BlickDrawing(Cairo::RefPtr<Cairo::Context>& cr);  /*! рисование блика на стеклышке */

  int value;            /*!< реальное значение */
  float prevValue,dValue,aValue;        /*!< значения для динамики, aValue - показываемое стрелкой значение */
  float x_pos,y_pos;          /*!< позиция виджета */
  std::string facial, devName, myFont;      /*!< строка на лицевой панели, название девайса, шрифт */

  int minp,maxp;            /*!< max & min значения на шкале */
  float step;            /*!< шаг шкалы */
  
  float sIndex;            /*!< индех пропорциональности */
  float squareside,width,height;        /*!< сторона квадрата - минимальное из ширины и высоты виджета */

  int redrawTime;            /*!< время перерисовки, влияет на инерционность */
  float inerc;            /*!< инерционность стрелки ( <1-безынерционная, max 15, default 3) */
  bool gg,overshoot,redr,glo;        /*!< флаги инерционности, перегрузки, перерисовки и графической оптимизации соответственно */

  sigc::connection tmrConn;        /*!< соединение с сигналом таймера */


  Cairo::RefPtr< Cairo::Pattern > scalePatt, glassPatt;

  Cairo::RefPtr< Cairo::RadialGradient> radgrad;

  
  int secCnt;            /*!< счетчик цветных секторов */

  std::list<sects* > sectList;          /*!< хранилище инициализаторов цветных секторов */
  typedef std::list<sects* >::iterator sectListIterator;    /*!< итератор хранилища цветных секторов */

  Pango::FontDescription pFont;

  int SMALL_SCALEMARKS_NUMBER;
  int SCALEMARKS_FONTSIZE;
  int DIGITVAL_FONTSIZE;
  int LABEL_FONTSIZE ;
  int SCALEMARK_WIDTH ;
  int COLORLINE_WIDTH ;
private:
  void constructor();
//  AD(std::string facial, bool glo_);      /*!< параметры: строка на лицевой панели и флаг графической оптимизации */


  DISALLOW_COPY_AND_ASSIGN(AD);
  ADD_PROPERTY( name_device, Glib::ustring )    /*!< свойство: строка на лицевой панели */
  ADD_PROPERTY( min_value, long )        /*!< свойство: минимум шкалы */
  ADD_PROPERTY( max_value, long )        /*!< свойство: максимум шкалы */
  ADD_PROPERTY( step_value, long )      /*!< свойство: шаг шкалы */
  ADD_PROPERTY( lag, long )        /*!< свойство: инерционность */
};

}
#endif
