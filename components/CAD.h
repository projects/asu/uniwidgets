#ifndef _CAD_H
#define _CAD_H
// -------------------------------------------------------------------------
/*! \author Pavel Ponamarev */
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <iomanip>
#include <UDefaultFunctions.h>
#include <SensorProp.h>
#include <global_macros.h>
#include "AD.h"
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Аналоговый индикатор.
 * \par
 * Компонент отображения аналогового индикатора. Позволяет выводить значение
 * аналогового датчика в виде стрелочного индикатора.
 * Данный вид индикатора отображается в виде круглого циферблата со стрелкой
 * по центру и шкалой в виде разомкнутого снизу круга.
*/
class CAD : public AD
{

public:

  CAD();
  explicit CAD(AD::BaseObjectType* gobject);
  virtual ~CAD();

protected:

  virtual void ScaleInit(Cairo::RefPtr<Cairo::Context>& cr);    /*!< инициализация шкалы */
  virtual void ColorizeSectors(Cairo::RefPtr<Cairo::Context>& cr);  /*!< раскраска секторов */
  virtual void ArrowDrawing(Cairo::RefPtr<Cairo::Context>& cr);    /*!< рисование стрелочки */
  virtual void GlassDrawing(Cairo::RefPtr<Cairo::Context>& cr);    /*!< рисование верхнего слоя (стеклышка) */
  virtual void BlickDrawing(Cairo::RefPtr<Cairo::Context>& cr);    /*!< рисование блика на стеклышке */

private:
  void constructor();
  DISALLOW_COPY_AND_ASSIGN(CAD);
};

}
#endif
