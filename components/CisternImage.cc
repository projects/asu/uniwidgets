#include <iostream>
#include "CisternImage.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace Glib;
// -------------------------------------------------------------------------
#define CISTERNIMAGE_BACKGROUND_IMAGE_PATH    "background-image-path"
#define CISTERNIMAGE_FILLING_IMAGE_PATH      "filling-image-path"
#define CISTERNIMAGE_SCALE_IMAGE_PATH      "scale-image-path"
#define CISTERNIMAGE_SCALE_ON_OR_OFF      "scale-switch"
// -------------------------------------------------------------------------
#define INIT_CISTERNIMAGE_PROPERTIES() \
  background_image_path( *this, CISTERNIMAGE_BACKGROUND_IMAGE_PATH, "" ) \
  ,filling_image_path( *this, CISTERNIMAGE_FILLING_IMAGE_PATH, "" ) \
  ,scale_image_path( *this, CISTERNIMAGE_SCALE_IMAGE_PATH, "" ) \
  ,scale_switch( *this, CISTERNIMAGE_SCALE_ON_OR_OFF, false)
// -------------------------------------------------------------------------
const int CisternImage::MaxValue = 100;
const int CisternImage::MinValue = 0;
// -------------------------------------------------------------------------
void CisternImage::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<CisternImage>;

  on_path_changed(background_image_,
      &background_image_path,
      Glib::ustring(CISTERNIMAGE_BACKGROUND_IMAGE_PATH));

  on_path_changed(filling_image_,
      &filling_image_path,
      Glib::ustring(CISTERNIMAGE_FILLING_IMAGE_PATH));

  on_path_changed(scale_image_,
      &scale_image_path,
      Glib::ustring(CISTERNIMAGE_SCALE_IMAGE_PATH));

  on_path_changed(background_off_image_,
      &background_image_path,
      Glib::ustring(CISTERNIMAGE_BACKGROUND_IMAGE_PATH));

  on_path_changed(filling_off_image_,
      &filling_image_path,
      Glib::ustring(CISTERNIMAGE_FILLING_IMAGE_PATH));
  /* The value is percent of the filling image path to draw.
   * This is able to take values from 0 to 100 */
  value_ = MinValue;
}
// -------------------------------------------------------------------------
CisternImage::CisternImage() :
  Glib::ObjectBase("cisternimage")
  ,INIT_CISTERNIMAGE_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
CisternImage::CisternImage(SimpleImage::BaseObjectType* gobject) :
  SimpleImage(gobject)
  ,INIT_CISTERNIMAGE_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
CisternImage::~CisternImage()
{
}
// -------------------------------------------------------------------------
bool CisternImage::is_blinking()
{
  return false;
}
// -------------------------------------------------------------------------
bool CisternImage::on_expose_event(GdkEventExpose* event)
{
  Gtk::EventBox::on_expose_event(event);

  draw(background_image_);

  draw_value(filling_image_);

  if(get_scale_switch())
    draw(scale_image_);

  return true;
}
// -------------------------------------------------------------------------
void CisternImage::set_value(long value)
{
  /* Check the value for a range 0 - 100 */
  if (value > MaxValue)
    value = MaxValue;
  else if (value < MinValue)
    value = MinValue;

  value_ = value;
  queue_draw();
}
// -------------------------------------------------------------------------
int CisternImage::value_to_offset(const Gtk::Allocation allocation)
{
  int ret;

  ret = allocation.get_y() + ((float(MaxValue) - value_) / float(MaxValue)) * float(allocation.get_height());

  return ret;
}
// -------------------------------------------------------------------------
void CisternImage::draw_value(Glib::RefPtr<Gdk::Pixbuf> image)
{
  /* FIXME: Part of this code is the same as SimpleImage::draw() */
  Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
  const Gtk::Allocation alloc = get_allocation();

  Gdk::Cairo::rectangle(cr,alloc);
  cr->clip();

  if (!image)
  {
    cr->set_line_width(1);
    Gdk::Cairo::rectangle(cr, alloc);
    cr->stroke();
    image = missing_image_;
  }

  RefPtr<Gdk::Pixbuf> resized_image = image->scale_simple(
      alloc.get_width(),
      alloc.get_height(),
      Gdk::INTERP_NEAREST);

  Gdk::Cairo::set_source_pixbuf(cr,
      resized_image,
      alloc.get_x(),
      alloc.get_y());

  cr->rectangle(alloc.get_x(),
      value_to_offset(alloc),
      alloc.get_width(),
      alloc.get_height());

  cr->clip();

  cr->paint();
}
// -------------------------------------------------------------------------
