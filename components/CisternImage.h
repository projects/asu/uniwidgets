#ifndef _CISTERNIMAGE_H
#define _CISTERNIMAGE_H
// -------------------------------------------------------------------------
#include <components/SimpleImage.h>
#include <plugins.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Компонент виджета цистерна.
 * \par
 * Компонент отображения картинки цистерны.
 * Отображается рамка цистерны т.е. её границы и отрисовывается
 * наполнитель цистерны в зависимости от текущего значения датчика.
*/
class CisternImage : public SimpleImage
{
public:
  CisternImage();
  explicit CisternImage(SimpleImage::BaseObjectType* gobject);
  virtual ~CisternImage();

  /* Methods */
  void set_value(long value);  /*!< Выставление значения уровня в цистерне */
  virtual void start_blink() {};
  virtual void stop_blink() {};
  virtual bool is_blinking();

protected:
  /* Event handlers */
  virtual bool on_expose_event(GdkEventExpose* event);
  Glib::RefPtr<Gdk::Pixbuf> background_off_image_;
  Glib::RefPtr<Gdk::Pixbuf> background_image_;

  Glib::RefPtr<Gdk::Pixbuf> filling_image_;
  Glib::RefPtr<Gdk::Pixbuf> filling_off_image_;
  void draw_value(Glib::RefPtr<Gdk::Pixbuf> image);  /*!< Отрисовка текущего уровня */
  int value_to_offset(const Gtk::Allocation allocation);  /*!< вычисление точки, из которой будет отрисовываться картинка уровня*/

  /* Variables */
  int value_;            /*!< Значение уровня */
  Glib::RefPtr<Gdk::Pixbuf> scale_image_;

  /* Constants */
  static const int MaxValue;        /*!< максимальный уровень цистерны(по-умолчанию 100) */
  static const int MinValue;        /*!< минимальный уровень цистерны(по-умолчанию 0) */

private:
  /* Methods */
  void constructor();          /*!< функция вызывающаяся в конструкторе конструкторе */
  DISALLOW_COPY_AND_ASSIGN(CisternImage);
  /* Properties */
  ADD_PROPERTY( background_image_path, Glib::ustring )  /*!< свойство: картинка для рамки цистерны */
  ADD_PROPERTY( filling_image_path, Glib::ustring )  /*!< свойство: картинка для заполнителя цистерны */ 
  ADD_PROPERTY( scale_image_path, Glib::ustring )    /*!< свойство: картинка для шкалы цистерны */ 
  ADD_PROPERTY( scale_switch, bool )      /*!< свойство: выключатель отображения шкалы */
};

}

#endif
