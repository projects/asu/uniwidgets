#include <iostream>
#include <iomanip>
#include <types.h>
#include "CisternImageBlink.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
using Glib::ustring;
using namespace Glib;
// -------------------------------------------------------------------------
#define CISTERNIMAGEBLINK_STATE                         "state"
#define CISTERNIMAGEBLINK_IMAGE_WARN_PATH    "image-warn-path"
#define CISTERNIMAGEBLINK_IMAGE_ALARM_PATH    "image-alarm-path"
#define CISTERNIMAGEBLINK_HIGH_ALARM_IMAGE_PATH    "high-alarm-image-path"
#define CISTERNIMAGEBLINK_LOW_ALARM_IMAGE_PATH    "low-alarm-image-path"
#define CISTERNIMAGE_THRESHOLD_HIGH      "threshold-high"
// -------------------------------------------------------------------------
#define INIT_CISTERNIMAGEBLINK_PROPERTIES() \
  property_state_(*this, CISTERNIMAGEBLINK_STATE, true) \
  ,image_warn_path( *this, CISTERNIMAGEBLINK_IMAGE_WARN_PATH, "" ) \
  ,image_alarm_path( *this, CISTERNIMAGEBLINK_IMAGE_ALARM_PATH, "" ) \
  ,high_alarm_image_path( *this, CISTERNIMAGEBLINK_HIGH_ALARM_IMAGE_PATH, "" ) \
  ,low_alarm_image_path( *this, CISTERNIMAGEBLINK_LOW_ALARM_IMAGE_PATH, "" ) \
  ,threshold_high( *this, CISTERNIMAGE_THRESHOLD_HIGH, 100 )
// -------------------------------------------------------------------------
Blinker CisternImageBlink::blinker;
// -------------------------------------------------------------------------
void CisternImageBlink::constructor()
{
  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<CisternImageBlink>;

  states_.insert(pair<long,Glib::RefPtr<Gdk::Pixbuf> >(mOFF, get_cache().get_pixbuf_from_cache( get_background_image_path()) ));
  states_.insert(pair<long,Glib::RefPtr<Gdk::Pixbuf> >(mWARNING, get_cache().get_pixbuf_from_cache( get_image_warn_path()) ));
  states_.insert(pair<long,Glib::RefPtr<Gdk::Pixbuf> >(mALARM, get_cache().get_pixbuf_from_cache( get_image_alarm_path()) ));

  on_path_changed(background_warn_image_, &image_warn_path, ustring(CISTERNIMAGEBLINK_IMAGE_WARN_PATH));
  on_path_changed(background_alarm_image_, &image_alarm_path, ustring(CISTERNIMAGEBLINK_IMAGE_ALARM_PATH));
  on_path_changed(filling_alarm_image_, &high_alarm_image_path, ustring(CISTERNIMAGEBLINK_HIGH_ALARM_IMAGE_PATH));
  on_path_changed(filling_alarm_image_, &high_alarm_image_path, ustring(CISTERNIMAGEBLINK_HIGH_ALARM_IMAGE_PATH));
  on_path_changed(states_[mWARNING], &image_warn_path, ustring(CISTERNIMAGEBLINK_IMAGE_WARN_PATH));
  on_path_changed(states_[mALARM], &image_alarm_path, ustring(CISTERNIMAGEBLINK_IMAGE_ALARM_PATH));
}
// -------------------------------------------------------------------------
CisternImageBlink::CisternImageBlink() :
  Glib::ObjectBase("cisternimageblink")
    ,is_blinking_(false)
    ,sleep_blinking_(false)
  ,INIT_CISTERNIMAGEBLINK_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
CisternImageBlink::CisternImageBlink(CisternImage::BaseObjectType* gobject) :
  CisternImage(gobject)
    ,is_blinking_(false)
    ,sleep_blinking_(false)
  ,INIT_CISTERNIMAGEBLINK_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
CisternImageBlink::~CisternImageBlink()
{
}
// -------------------------------------------------------------------------
void CisternImageBlink::on_set_state(const long mode,const long level)
{
  if( mode == mOFF)
  {
    background_current_image_ = background_off_image_;
    filling_current_image_ = filling_off_image_;
  }
  else
  {
    background_current_image_ = states_[mode];
    filling_current_image_ = filling_alarm_image_;
  }
  queue_draw();
}
// -------------------------------------------------------------------------
bool CisternImageBlink::on_expose_event(GdkEventExpose* event)
{
  Gtk::EventBox::on_expose_event(event);

  if ( get_property_state_() )
  {
    background_image_ = background_current_image_;
    filling_image_ = filling_current_image_;
  }
  else
  {
    background_image_ = background_off_image_;
    filling_image_ = filling_off_image_;
  }

  draw(background_image_);

  /*Если значение превышает верхний порог, то рисуем снизу нормальный уровень, а сверху аварийный*/
  if( value_ > get_threshold_high() )
  {
    int ret = get_allocation().get_y() + ((float(MaxValue) - get_threshold_high()) / float(MaxValue)) * float(get_allocation().get_height());

    draw_value_y(filling_off_image_,get_allocation().get_height(),ret);

    ret = ceil(((float(value_) - float(get_threshold_high())) / float(MaxValue)) * float(get_allocation().get_height()));

    draw_value_y(filling_image_,ret,value_to_offset(get_allocation()));
  }
  else
    draw_value(filling_image_);

  if(get_scale_switch())
    draw(scale_image_);

  return true;
}
// -------------------------------------------------------------------------
void CisternImageBlink::start_blink()
{
  if(is_blinking())
    return;

  if(!is_mapped())
  {
    is_blinking_ = true;
    sleep_blinking_ = true;
    return;
  }

  blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].
    connect(sigc::mem_fun(this, &CisternImageBlink::blink));

  is_blinking_ = true;
}
// -------------------------------------------------------------------------
void CisternImageBlink::stop_blink()
{
  if(!is_blinking())
    return;

  if(!is_mapped())
  {
    sleep_blinking_ = false;
  }

  blink_connection_.disconnect();
  if ( get_property_state_()  != true)
  {
    set_property_state_( true );
    queue_draw();
  }
  is_blinking_ = false;
}
// -------------------------------------------------------------------------
void CisternImageBlink::blink(bool state,int time)
{
  bool prev =  get_property_state_();
  set_property_state_( state );

  if (prev !=  get_property_state_() )
    queue_draw();
}
// -------------------------------------------------------------------------
bool CisternImageBlink::is_blinking()
{
  return is_blinking_;
}
// -------------------------------------------------------------------------
void CisternImageBlink::draw_value_y(Glib::RefPtr<Gdk::Pixbuf> image, const long y1_ ,const long y2_)
{
  /* FIXME: Part of this code is the same as SimpleImage::draw() */
  Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
  const Gtk::Allocation alloc = get_allocation();

  Gdk::Cairo::rectangle(cr,alloc);
  cr->clip();

  if (!image)
  {
    cr->set_line_width(1);
    Gdk::Cairo::rectangle(cr, alloc);
    cr->stroke();
    image = missing_image_;
  }

  RefPtr<Gdk::Pixbuf> resized_image = image->scale_simple(
      alloc.get_width(),
      alloc.get_height(),
      Gdk::INTERP_NEAREST);

  Gdk::Cairo::set_source_pixbuf(cr,
      resized_image,
      alloc.get_x(),
      alloc.get_y());

  cr->rectangle(alloc.get_x(),
      y2_,
      alloc.get_width(),
      y1_);

  cr->clip();

  cr->paint();
}
// -------------------------------------------------------------------------
void CisternImageBlink::on_map()
{
  Gtk::EventBox::on_map();
  if(sleep_blinking_)
  {
    is_blinking_ = false;
    start_blink();
    sleep_blinking_ = false;
  }
}
// -------------------------------------------------------------------------
void CisternImageBlink::on_unmap()
{
  Gtk::EventBox::on_unmap();
  if(is_blinking())
  {
    stop_blink();
    sleep_blinking_ = true;
    is_blinking_ = true;
  }
}
// -------------------------------------------------------------------------
