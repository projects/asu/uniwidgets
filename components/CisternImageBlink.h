#ifndef _CISTERNIMAGEBLINK_H
#define _CISTERNIMAGEBLINK_H
// -------------------------------------------------------------------------
/*! \author Ilya Polshikov <ilyap@etersoft.ru> */
// -------------------------------------------------------------------------
#include <unordered_map>
#include <gtkmm.h>
#include <gdkmm.h>
#include <SBlinker.h>
#include <plugins.h>
#include <global_macros.h>
#include <components/CisternImage.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Компонент виджета цистерна.
 * \par
 * Компонент отображения картинки цистерны со встроенным механизмом мигания.
 * Мигание применяется для обозначения сработавшей АПС по уровню в цистерне.
 * Мигать может как весь уровень т.е. весь заполнитель цистерны так и только
 * та часть заполнителя, которая вляется превышением некого порогового уровня.
*/
class CisternImageBlink : public CisternImage
{
public:
  CisternImageBlink();
  explicit CisternImageBlink(Gtk::EventBox::BaseObjectType* gobject);
  virtual ~CisternImageBlink();

  /* Methods */
  void start_blink();              /*!< запустить мигание виджета */
  void stop_blink();              /*!< остановить мигание виджета */
  void on_set_state(const long mode, const long level=0);      /*!< установить текущее состояние, выставить новый mode */
  bool is_blinking();              /*!< проверка на мигание */
  virtual bool can_blinking(){return true;} 

protected:
  /* Event handlers */
  virtual bool on_expose_event(GdkEventExpose* event);
  void on_map();
  void on_unmap();

private:
  /* Variables */
  Glib::RefPtr<Gdk::Pixbuf> background_current_image_;
  Glib::RefPtr<Gdk::Pixbuf> filling_current_image_;

  Glib::RefPtr<Gdk::Pixbuf> background_warn_image_;
  Glib::RefPtr<Gdk::Pixbuf> background_alarm_image_;

  Glib::RefPtr<Gdk::Pixbuf> filling_alarm_image_;
  std::unordered_map<long , Glib::RefPtr<Gdk::Pixbuf> > states_;

  static Blinker blinker;
  sigc::connection blink_connection_;
  bool is_blinking_;
  bool sleep_blinking_;

  void constructor();
  void blink(bool state,int time = DEFAULT_BLINK_TIME);            /*!< функция мигания виджета */
  virtual void draw_value_y(Glib::RefPtr<Gdk::Pixbuf> image, const long y1_ ,const long y2_);  /*!< отрисовка уровня в цистерну */
  DISALLOW_COPY_AND_ASSIGN(CisternImageBlink);
  ADD_PROPERTY( property_state_, bool )
  ADD_PROPERTY( image_warn_path, Glib::ustring )              /*!< свойство: картинка "WARNING" для рамки цистерны */
  ADD_PROPERTY( image_alarm_path, Glib::ustring )              /*!< свойство: картинка "ALARM" для рамки цистерны */
  ADD_PROPERTY( high_alarm_image_path, Glib::ustring )            /*!< свойство: картинка "ALARM" для наполнителя при верхнем уровне в цистерне */
  ADD_PROPERTY( low_alarm_image_path, Glib::ustring )            /*!< свойство: картинка "ALARM" для наполнителя при нижнем уровне в цистерне */ 
  ADD_PROPERTY( threshold_high, long )                /*!< свойство: числовое значение порога срабатывания уровня в цистерне(в пределах 0-100%) */
};

}
#endif
