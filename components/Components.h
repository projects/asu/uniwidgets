#ifndef _COMPONENTS_H
#define _COMPONENTS_H

#include <components/SimpleView.h>
#include <components/ImageBlink.h>
#include <components/Image.h>
#include <components/CisternImage.h>
#include <components/CisternImageBlink.h>
#include <components/SimpleText.h>
#include <components/Text.h>
#include <components/TextBlink.h>
#include <components/AD.h>
#include <components/EAD.h>
#include <components/CAD.h>
#endif
