#include <types.h>
#include "EAD.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
static const double LENGHT_ARROW = 0.75;
static const double ARROW_ROOT = 0.88;
static const double FAT_ARROW = 0.9;
static const double X_DIGITVAL = 0.07;
static const double Y_DIGITVAL = 0.17;
static const double X_LABEL = 0.50;
static const double Y_LABEL = 0.63;
static const double LENGHT_SCALEMARK = 0.9;
static const double LENGHT_SMALL_SCALEMARK = 0.95;
static const double RADIUS = 0.78;
static const double SCALE_RANGE = 3.1415/2;
static const double SCALE_SHIFT = 0;
//  static const int SCALEMARK_WIDTH = 3;
//  static const int COLORLINE_WIDTH = 14;
// -------------------------------------------------------------------------
void EAD::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<EAD>;

  COLORLINE_WIDTH-=4;
  setScale(0,100,20);
  this->set_size_request(200,200);
}
// -------------------------------------------------------------------------
EAD::EAD(AD::BaseObjectType* gobject) :
  AD(gobject)
{
  constructor();
}
// -------------------------------------------------------------------------
EAD::EAD():
  Glib::ObjectBase("ead"),
  AD()
{
  constructor();
}
// -------------------------------------------------------------------------
EAD::~EAD()
{
}
// -------------------------------------------------------------------------
void EAD::ArrowDrawing(Cairo::RefPtr<Cairo::Context>& cr)
{
  float lengtArrow = LENGHT_ARROW*squareside;
  float phia = SCALE_RANGE * ( (aValue-minp)/(maxp-minp) );
  if(phia<0) 
    phia=0;
  float center = ARROW_ROOT*squareside;
  float x1_new = center + x_pos;
  float y1_new = center + y_pos;
  float x2_new = center - lengtArrow*cos( phia - SCALE_SHIFT ) + x_pos;
  float y2_new = center - lengtArrow*sin( phia - SCALE_SHIFT ) + y_pos;
    
  cr->set_line_cap(Cairo::LINE_CAP_ROUND);

  cr->set_line_width(1);
  cr->move_to( x1_new, y1_new );
  cr->line_to( x2_new, y2_new );
  cr->stroke();
      
  float x2_n = center - FAT_ARROW*lengtArrow*cos( phia - SCALE_SHIFT ) + x_pos;
  float y2_n = center - FAT_ARROW*lengtArrow*sin( phia - SCALE_SHIFT ) + y_pos;
      
  cr->set_line_width(3);
  cr->move_to( x1_new, y1_new );
  cr->line_to( x2_n, y2_n );
  cr->stroke();
  
    
  char digitValue[10];
  sprintf(digitValue,"%02d",value);
    
  if(overshoot)
    cr->set_source_rgba(1, 0, 0, 1);   // red

//  const string myFont="DSCrystal Bold 17";
//  cr->select_font_face(Pango::FontDescription("DSCrystal"));
//  cr->select_font_face(myFont,CAIRO_FONT_SLANT_NORMAL,CAIRO_FONT_WEIGHT_NORMAL);

  cr->set_font_size( DIGITVAL_FONTSIZE*sIndex );
  cr->move_to( X_DIGITVAL*squareside + x_pos, Y_DIGITVAL*squareside + y_pos );
  cr->show_text(digitValue);
  cr->stroke();    

//  move(label,(int)(X_DIGITVAL*squareside),(int)(Y_DIGITVAL*squareside));
//  label.set_size_request(20,20);
//  pFont.set_size(30);//DIGITVAL_FONTSIZE*sIndex);
//  label.modify_font(pFont);

//  label.set_text(digitValue);

/*
    led.setWidgetSize(60*sIndex,26*sIndex);
    led.setWidgetFont("Arial 24");
    this->put(led,squareside/30,squareside/30);
    led.setValue(value);
*/

}
// -------------------------------------------------------------------------
void EAD::ScaleInit(Cairo::RefPtr<Cairo::Context>& cr)
{
  cr->set_source_rgba(0, 0, 0, 1);//black - the colour of the pen
  cr->paint();
  cr->stroke();
    
  cr->rectangle( squareside/30 + x_pos, squareside/30 + y_pos,
          squareside - 2*squareside/30, squareside - 2*squareside/30 );
  cr->clip();
  cr->set_source_rgba(1, 1, 1, 1);
  cr->paint();
  cr->stroke();

  //shadow
  cr->set_source_rgba(0, 0, 0, 0.3);    
  cr->set_line_width(6);
  cr->move_to( squareside/30 + x_pos, squareside + y_pos );
  cr->line_to( squareside/30 + x_pos, squareside/30 + y_pos );
  cr->line_to( squareside + x_pos, squareside/30 + y_pos );
  cr->stroke();

  cr->set_source_rgba(0, 0, 0, 1);   // black
  cr->set_font_size( LABEL_FONTSIZE*sIndex );  
  cr->move_to( X_LABEL*squareside + x_pos, Y_LABEL*squareside + y_pos );
  cr->show_text(facial);
  cr->stroke();    

  ColorizeSectors(cr);

  static const int tstep[]={10,5,6,4,3,7,2,8,9};
  int i, bigMax=5;
  double phi, lc=LENGHT_SCALEMARK;

  if ( maxp*minp < 0 ) 
    bigMax=6;
    
  for ( i = 0; i < 9 && ( step != (maxp-minp)/bigMax ); i++ )
    bigMax = tstep[i];

  int small = 1,big = bigMax;

  float otstoys = RADIUS*squareside;
  float center = ARROW_ROOT*squareside;

  cr->set_source_rgba(0, 0, 0, 1);    
  float x1s,y1s,x2s,y2s;
  for( phi = 0; phi <= SCALE_RANGE; phi += SCALE_RANGE/(SMALL_SCALEMARKS_NUMBER*bigMax) )
  {
    x1s = ( ( center - otstoys*cos( phi - SCALE_SHIFT ) ) + x_pos );
    y1s = ( ( center - otstoys*sin( phi - SCALE_SHIFT ) ) + y_pos );
    x2s = ( ( center - (otstoys*lc)*cos( phi - SCALE_SHIFT ) ) + x_pos );
    y2s = ( ( center - (otstoys*lc)*sin( phi - SCALE_SHIFT ) ) + y_pos );

    cr->set_line_width(SCALEMARK_WIDTH);
    cr->move_to(x1s,y1s);
  cr->line_to(x2s,y2s);
  cr->stroke();

    if( lc == LENGHT_SCALEMARK )
    {
      char szBuf[25];
      sprintf( szBuf,"%d",(int)( maxp-(big--)*step ) );

//        cr->select_font_face("Arial 16");
      cr->move_to( x2s - 14*sIndex*sin(phi) + 4,
            y2s + 10*sIndex*sin(phi) + 7*sIndex );
      cr->set_font_size( sIndex*SCALEMARKS_FONTSIZE );
      cr->show_text(szBuf);
      cr->stroke();    
    }
    
    lc=LENGHT_SMALL_SCALEMARK;

    if( small == SMALL_SCALEMARKS_NUMBER && phi != 0)
    {
      small=0; 
      lc=LENGHT_SCALEMARK;
    }
    small++;
  }
}
// -------------------------------------------------------------------------
void EAD::ColorizeSectors(Cairo::RefPtr<Cairo::Context>& cr)
{
  float theta1,theta2;

  float otstoys = RADIUS*squareside;
  float center = ARROW_ROOT*squareside;

  sectListIterator it;
  for(it = sectList.begin(); it != sectList.end(); ++it)
  {
    theta1 = SCALE_RANGE*(( (float)(*it)->startBorder - minp )/( maxp - minp ) );
    theta2 = SCALE_RANGE*(( (float)(*it)->endBorder - 
                (float)(*it)->startBorder )/( maxp - minp ) );
    cr->set_source_rgba((*it)->red, (*it)->green, 
              (*it)->blue, (*it)->alfa);
        
    if((*it)->solid)
    {
      cr->set_line_width(0);
      cr->arc( center + x_pos, center + y_pos,
         otstoys, M_PI + theta1 - SCALE_SHIFT, M_PI + theta2 + theta1 - SCALE_SHIFT );
      cr->line_to( center + x_pos, center + y_pos );
      cr->close_path();
      cr->fill_preserve();
    }
    else
    {
      cr->set_line_width(COLORLINE_WIDTH*sIndex);
      cr->arc(center+x_pos, center+y_pos, 
         otstoys-8*sIndex, M_PI+theta1 - SCALE_SHIFT, M_PI+theta2+theta1 - SCALE_SHIFT);
    }
    cr->stroke();
  }  
}
// -------------------------------------------------------------------------
void EAD::GlassDrawing(Cairo::RefPtr<Cairo::Context>& cr)
{
/*  cr->rectangle(x_pos,y_pos,squareside,squareside);
  cr->clip();
*/
  cr->arc( squareside + x_pos, squareside + y_pos, squareside*0.3, -(M_PI/2), M_PI );
  cr->close_path();
  cr->fill_preserve();
  cr->stroke();

}
// -------------------------------------------------------------------------
