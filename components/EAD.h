#ifndef _EAD_H
#define _EAD_H
// -------------------------------------------------------------------------
/*! \author Pavel Ponamarev */
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <iomanip>
#include <components/AD.h>
#include <UDefaultFunctions.h>
#include <SensorProp.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Аналоговый индикатор.
 * \par
 * Компонент отображения аналогового индикатора. Позволяет выводить значение
 * аналогового датчика в виде стрелочного индикатора.
 * Данный вид индикатора отображается в виде квадратного циферблата со стрелкой
 * начинающейся в правом нижнем углу и шкалой слева.
*/
class EAD : public AD
{

public:

  EAD();
  explicit EAD(AD::BaseObjectType* gobject);
  virtual ~EAD();

protected:

  virtual void ScaleInit(Cairo::RefPtr<Cairo::Context>& cr);    /*!< инициализация шкалы */
  virtual void ColorizeSectors(Cairo::RefPtr<Cairo::Context>& cr);  /*!< раскраска секторов */
  virtual void ArrowDrawing(Cairo::RefPtr<Cairo::Context>& cr);    /*!< рисование стрелочки */
  virtual void GlassDrawing(Cairo::RefPtr<Cairo::Context>& cr);    /*!< рисование верхнего слоя (стеклышка) */

private:
  void constructor();
};

}
#endif
