#include <iostream>
#include <gtkmm.h>
#include "Image.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
// -------------------------------------------------------------------------
#define IMAGE_IMAGE_PATH    "image-path"
// -------------------------------------------------------------------------
#define INIT_IMAGE_PROPERTIES() \
  image_path(*this, IMAGE_IMAGE_PATH, "" )
// -------------------------------------------------------------------------
void Image::constructor()
{
  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<Image>;
  on_path_changed(image_ref_, &image_path, Glib::ustring(IMAGE_IMAGE_PATH));
}
// -------------------------------------------------------------------------
Image::Image() :
  Glib::ObjectBase("image")
  ,INIT_IMAGE_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
Image::Image(SimpleImage::BaseObjectType* gobject) :
  SimpleImage(gobject)
  ,INIT_IMAGE_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
Image::Image(Glib::ustring svgfile) :
  Glib::ObjectBase("image")
  ,INIT_IMAGE_PROPERTIES()
{
  set_image_path(svgfile);

  constructor();
}
// -------------------------------------------------------------------------
Image::~Image()
{
}
// -------------------------------------------------------------------------
void Image::on_realize()
{
  SimpleImage::on_realize();
}

void Image::on_size_request(Gtk::Requisition* req)
{
    SimpleImage::on_size_request(req);
    *req = Gtk::Requisition();
    if( image_ref_ != NULL )
    {
      req->width = image_ref_->get_width();
      req->height = image_ref_->get_height();
    }
//     else
//     {
//       req->width = -1;
//       req->height = -1;
//     }
//     queue_draw();
}
// -------------------------------------------------------------------------
void Image::on_size_allocate(Gtk::Allocation& alloc)
{
  SimpleImage::on_size_allocate(alloc);
  if(!image_ref_)
    return;
  if(image_ref_->get_width() != alloc.get_width() || image_ref_->get_height() != alloc.get_height())
  {
    set_size_request(alloc.get_width(),alloc.get_height());
    on_path_changed(image_ref_, &image_path, Glib::ustring(IMAGE_IMAGE_PATH));
  }
}

bool Image::on_expose_event(GdkEventExpose* event)
{
  draw(image_ref_);
  Gtk::EventBox::on_expose_event(event);
  return true;
}
// -------------------------------------------------------------------------
void Image::on_show()
{
  SimpleImage::on_show();
}
// -------------------------------------------------------------------------
void Image::on_hide()
{
  SimpleImage::on_hide();
}
// -------------------------------------------------------------------------
