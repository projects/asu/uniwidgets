#ifndef _IMAGE_H
#define _IMAGE_H
// -------------------------------------------------------------------------
#include <components/SimpleImage.h>
#include <plugins.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Картинка.
 * \par
 * Компонент отображения картинки.
*/
class Image : public SimpleImage
{
public:
  Image();
  Image(Glib::ustring svgimage);
  explicit Image(SimpleImage::BaseObjectType* gobject);
  virtual ~Image();

    virtual void on_size_request(Gtk::Requisition* requisition);
    virtual void on_size_allocate(Gtk::Allocation& alloc);

protected:
  /* Variables */
  Glib::RefPtr<Gdk::Pixbuf> image_ref_;      /*!< отображаемая картинка */

  /* Event handlers */
  virtual void on_show();
  virtual void on_hide();
  virtual bool on_expose_event(GdkEventExpose* event);
  virtual void on_realize();

private:
  void constructor();
  DISALLOW_COPY_AND_ASSIGN(Image);
  /* Properties */
  ADD_PROPERTY( image_path, Glib::ustring )
};

}

#endif
