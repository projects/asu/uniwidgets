#include <iostream>
#include <gtkmm.h>
#include <gdkmm.h>
#include "ImageBlink.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace Glib;
// -------------------------------------------------------------------------
#define IMAGEBLINK_IMAGE_PATH    "image2-path"
#define BLINKTIME      "blinktime"
// -------------------------------------------------------------------------
#define IMAGEBLINK_PROPERTY_INIT() \
  image2_path(*this, IMAGEBLINK_IMAGE_PATH, "" ) \
  ,blinktime(*this, BLINKTIME, 500)
// -------------------------------------------------------------------------
Blinker ImageBlink::blinker;
// -------------------------------------------------------------------------
void ImageBlink::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<ImageBlink>;

  on_path_changed(image2_ref_, &image2_path, ustring(IMAGEBLINK_IMAGE_PATH));
  connect_property_changed("blinktime", sigc::mem_fun(*this, &ImageBlink::configure_blinktime));
}
// -------------------------------------------------------------------------
ImageBlink::ImageBlink() :
  Glib::ObjectBase("imageblink")
    ,is_blinking_(false)
    ,sleep_blinking_(false)
    ,state_(true)
  ,IMAGEBLINK_PROPERTY_INIT()
{
  constructor();
}
// -------------------------------------------------------------------------
ImageBlink::ImageBlink(Image::BaseObjectType* gobject) :
  Image(gobject)
    ,is_blinking_(false)
    ,sleep_blinking_(false)
    ,state_(true)
  ,IMAGEBLINK_PROPERTY_INIT()
{
  constructor();
}
// -------------------------------------------------------------------------
ImageBlink::~ImageBlink()
{
}
// -------------------------------------------------------------------------
void ImageBlink::start_blink()
{
  if (is_blinking())
    return;

  if(!is_mapped())
  {
    is_blinking_ = true;
    sleep_blinking_ = true;
    return;
  }

  is_blinking_ = true;

  blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].
    connect(sigc::mem_fun(this, &ImageBlink::blink));
}
// -------------------------------------------------------------------------
void ImageBlink::stop_blink()
{
  if (!is_blinking())
    return;

  if(!is_mapped())
  {
    sleep_blinking_ = false;
  }

  blink_connection_.disconnect();

  if (state_ != true)
  {
    state_ = true;
    queue_draw();
  }

  is_blinking_ = false;
}
// -------------------------------------------------------------------------
void ImageBlink::blink(bool state, int time)
{
  bool prev = state_;

  state_ = state;

  if (prev != state_)
    queue_draw();
}
// -------------------------------------------------------------------------
bool ImageBlink::is_blinking()
{
  return is_blinking_;
}
// -------------------------------------------------------------------------
bool ImageBlink::on_expose_event(GdkEventExpose* event)
{
  Gtk::EventBox::on_expose_event(event);

  if (state_)
    draw(image2_ref_);
  else
    draw(image_ref_);

  return true;
}
// -------------------------------------------------------------------------
bool ImageBlink::set_state(bool st)
{
  state_ = st;
  queue_draw();
  return state_;
}
// -------------------------------------------------------------------------
void ImageBlink::configure_blinktime()
{
  blinker.set_blink_time(get_blinktime());
}
// -------------------------------------------------------------------------
void ImageBlink::on_map()
{
  Gtk::EventBox::on_map();
  if(sleep_blinking_)
  {
    is_blinking_ = false;
    start_blink();
    sleep_blinking_ = false;
  }
}
// -------------------------------------------------------------------------
void ImageBlink::on_unmap()
{
  Gtk::EventBox::on_unmap();
  if(is_blinking())
  {
    stop_blink();
    sleep_blinking_ = true;
    is_blinking_ = true;
  }
}
// -------------------------------------------------------------------------
void ImageBlink::on_size_allocate(Gtk::Allocation& alloc)
{
  SimpleImage::on_size_allocate(alloc);
  if(image2_ref_ == NULL)
    return;
  if(image2_ref_->get_width() != alloc.get_width() || image2_ref_->get_height() != alloc.get_height())
  {
    set_size_request(alloc.get_width(),alloc.get_height());
    on_path_changed(image2_ref_, &image2_path, ustring(IMAGEBLINK_IMAGE_PATH));
  }
}
// -------------------------------------------------------------------------
