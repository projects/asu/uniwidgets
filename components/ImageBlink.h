
#ifndef _IMAGEBLINK_H
#define _IMAGEBLINK_H
// -------------------------------------------------------------------------
#include <SBlinker.h>
#include <components/Image.h>
#include <plugins.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Картинка мигающая.
 * \par
 * Компонент отображения картинки с возможностью мигания.
*/
class ImageBlink : public Image
{
public:
  ImageBlink();
  explicit ImageBlink(Image::BaseObjectType* gobject);
  virtual ~ImageBlink();

  /* Methods */
  virtual void start_blink();          /*!< включить мигание */
  virtual void stop_blink();          /*!< выключить мигание */
  virtual bool is_blinking();          /*!< проверка мигания миджета */
//  virtual void set_blink_(bool value){blink_.set_value(value);}  /*!< установка типа картинки(с миганием или без мигания) */
  virtual bool set_state(bool st);        /*!< установка состояния виджета(включенное или выключенное состояние)*/
  virtual bool can_blinking(){return true;}
  virtual void on_size_allocate(Gtk::Allocation& alloc);

protected:
  /* Event handlers */
  virtual bool on_expose_event(GdkEventExpose*);
  void on_map();
  void on_unmap();

private:
  /* Variables */
  static Blinker blinker;
  Glib::RefPtr<Gdk::Pixbuf> image2_ref_;
  sigc::connection blink_connection_;
  bool is_blinking_;
  bool sleep_blinking_;
  /* FIXME: state_ is indicator of showing image (first or second).
   * This is not realy bool value !!! */
  bool state_;

  /* Methods */
  void constructor();
  void blink(bool state,int time = DEFAULT_BLINK_TIME);

  // Функция для
  void configure_blinktime();

  DISALLOW_COPY_AND_ASSIGN(ImageBlink);
  /* Properties */
  ADD_PROPERTY( image2_path, Glib::ustring )
  //Задаёт частоту мигания картинки
  ADD_PROPERTY( blinktime, int )
};

}

#endif
