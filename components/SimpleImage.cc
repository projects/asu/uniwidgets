#include <iostream>
#include "SimpleImage.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace Glib;
// -------------------------------------------------------------------------
void SimpleImage::constructor()
{
    GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
    goclass->set_property = &custom_set_property_callback_object<SimpleImage>;
  /* Эта строчка нужна для инициализации режима
   * видимости картинки. Без нее картинка будет не видимой */
  set_visible_window(false);
  missing_image_ = render_icon(Gtk::Stock::MISSING_IMAGE, Gtk::ICON_SIZE_MENU);
}
// -------------------------------------------------------------------------
SimpleImage::SimpleImage() :
  Glib::ObjectBase("simpleimage")
  ,is_glade(false)
{
  constructor();
}
// -------------------------------------------------------------------------
SimpleImage::SimpleImage(SimpleView::BaseObjectType* gobject) :
  SimpleView(gobject)
  ,is_glade(true)
{
  constructor();
}
// -------------------------------------------------------------------------
SimpleImage::~SimpleImage()
{
}
// -------------------------------------------------------------------------
void SimpleImage::on_path_changed(RefPtr<Gdk::Pixbuf>& image, Property< Glib::ustring >* new_path, ustring property_name)
{
  sigc::slot<void, RefPtr<Gdk::Pixbuf>*,
    const Property< Glib::ustring >* > slot_handler =
      sigc::mem_fun(*this, &SimpleImage::load_pixbuf);

  sigc::slot<void> image_path_changed =
    sigc::bind(sigc::bind(slot_handler, new_path), &image);

  sigc::slot<void> redraw =
    sigc::mem_fun(*this, &SimpleImage::queue_draw);

  image_path_changed();
  connect_property_changed(property_name, image_path_changed);
  connect_property_changed(property_name, redraw);
}
// -------------------------------------------------------------------------
void SimpleImage::draw(RefPtr<Gdk::Pixbuf>& image)
{
  Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
  const Gtk::Allocation alloc = get_allocation();

  Gdk::Cairo::rectangle(cr,alloc);
  cr->clip();

  if (!image)
  {
    cr->set_line_width(1);
    Gdk::Cairo::rectangle(cr, alloc);
    cr->stroke();
    image = missing_image_;
  }
  RefPtr<Gdk::Pixbuf> resized_image = image->scale_simple(
      alloc.get_width(),
      alloc.get_height(),
      Gdk::INTERP_NEAREST);

  Gdk::Cairo::set_source_pixbuf(cr,
      resized_image,
      alloc.get_x(),
      alloc.get_y());

  cr->paint();
#if 0 
  if(!is_sensitive())
    cr->paint_with_alpha(0.4);
  else
    cr->paint();
#endif 
}
// -------------------------------------------------------------------------
void SimpleImage::load_pixbuf(RefPtr<Gdk::Pixbuf>* image_ptr, const Property< Glib::ustring >* property_path)
{
  Glib::ustring path;
  path = (*property_path).get_value();

  /* Check path to empty */
  if (path.length() == 0)
    return;

  int w=0,h=0;
  get_size_request(w,h);
  /* Loading pixbuf from the path */
  try
  {
    *image_ptr = get_cache().get_pixbuf_from_cache(path,w,h);
  }
  catch (...)
  {
    cerr << get_name() << ": file doesn't exist - " << path << endl;
  }
}
// -------------------------------------------------------------------------
