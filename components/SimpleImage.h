
#ifndef _SIMPLEIMAGE_H
#define _SIMPLEIMAGE_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <UPixbufCache.h>
#include <global_macros.h>
#include <components/SimpleView.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Базовый класс для виджетов картинок.
 * \par
 * Компонент служит базовым классом для виджетов картинок таких как Image и ImageBlink.
 * Внутри класса определены базовые методы и переменные для виджетов типа картинка.
*/
class SimpleImage : public SimpleView
{
public:
  SimpleImage();
  explicit SimpleImage(SimpleView::BaseObjectType* gobject);
  virtual ~SimpleImage();

protected:
  /* Variables */
  Glib::RefPtr<Gdk::Pixbuf> missing_image_;        /*!< картинка,которая выводится при отсутствии основной картинки */

  /* Methods */
  void on_path_changed(Glib::RefPtr<Gdk::Pixbuf>& image,
      Glib::Property<Glib::ustring>* new_path,
      Glib::ustring property_name);        /*!< метод обрабатывающий изменение свойства картинки виджета */

  void draw(Glib::RefPtr<Gdk::Pixbuf>& image);        /*!< отрисовка картинки */
  UPixbufCache& get_cache(){ return pixbufcache;}            /*!< вернуть кэш для картинок */

private:
  /* Variables */
  UPixbufCache pixbufcache;
  bool is_glade;
  /* Methods */
  void constructor();

  void load_pixbuf(Glib::RefPtr<Gdk::Pixbuf>* image_ptr,
      const Glib::Property<Glib::ustring>* property_path);
  DISALLOW_COPY_AND_ASSIGN(SimpleImage);
};

}

#endif
