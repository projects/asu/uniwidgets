#include <iostream>
#include <gtkmm.h>
#include <components/SimpleText.h>
#include <ThemeLoader.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
// -------------------------------------------------------------------------
#define SIMPLETEXT_TEXT        "text"
#define SIMPLETEXT_DROP_SHADOW      "drop-shadow"
#define SIMPLETEXT_TRANSPARENCY      "tranparency"
#define SIMPLETEXT_ALIGNMENT      "alignment"
// -------------------------------------------------------------------------
#define INIT_SIMPLETEXT_PROPERTIES() \
  property_text_(*this,SIMPLETEXT_TEXT,"Default Text") \
  ,property_drop_shadow_(*this, SIMPLETEXT_DROP_SHADOW, false) \
  ,property_transparency_(*this, SIMPLETEXT_TRANSPARENCY, 1) \
  ,property_alignment_(*this, SIMPLETEXT_ALIGNMENT,Pango::ALIGN_RIGHT)
// -------------------------------------------------------------------------
void SimpleText::constructor()
{
    GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
    goclass->set_property = &custom_set_property_callback_object<SimpleText>;

  set_flags(Gtk::NO_WINDOW);
  set_redraw_on_allocate(true);
  layout = create_pango_layout("");
  current_color = Gdk::Color("black");
}
// -------------------------------------------------------------------------
SimpleText::SimpleText() :
  Glib::ObjectBase("simpletext")
  ,SimpleView()
  ,INIT_SIMPLETEXT_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
SimpleText::SimpleText(SimpleView::BaseObjectType* gobject) :
  SimpleView(gobject)
  ,INIT_SIMPLETEXT_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
SimpleText::~SimpleText()
{
}
// -------------------------------------------------------------------------
bool SimpleText::on_expose_event(GdkEventExpose* event)
{
  SimpleView::on_expose_event(event);
  text_draw();
  return true;
}
// -------------------------------------------------------------------------
void SimpleText::on_realize()
{
  SimpleView::on_realize();

  connect_property_changed("text", sigc::mem_fun(*this, &SimpleText::on_text_changed));
  connect_property_changed("drop-shadow", sigc::mem_fun(*this, &SimpleText::on_dropshadow_changed));
  connect_property_changed("transparancy", sigc::mem_fun(*this, &SimpleText::on_transparency_changed));

  layout->set_text(property_text_);
  transparency = get_property_transparency_();
}
// -------------------------------------------------------------------------
void SimpleText::on_transparency_changed()
{
  transparency = property_transparency_;
  queue_draw();
}
// -------------------------------------------------------------------------
void SimpleText::on_dropshadow_changed()
{
  queue_resize();
}
// -------------------------------------------------------------------------
void SimpleText::on_text_changed()
{
  layout->set_text(property_text_);
  queue_resize();
}
// -------------------------------------------------------------------------
void SimpleText::text_draw()
{
  Gtk::Allocation alloc = get_allocation();
  cr = get_window()->create_cairo_context();

  Gdk::Cairo::rectangle(cr, alloc);
  cr->clip();

  int w,h;
  layout->set_alignment( property_alignment_ );
  layout->get_pixel_size(w,h);
//   set_size_request(w,h);

//   if(is_sensitive())
//   {

  if (property_drop_shadow_)
  {
    cr->set_source_rgb(0,0,0);
    cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2+1, alloc.get_y()+(alloc.get_height()-h)/2+1 );
    layout->add_to_cairo_context(cr);
    cr->fill();
  }

  /* Glade can set color to NULL so we need to check it.
    May be later we will find another solution
  */
  if (current_color.gobj() == NULL)
    current_color = Gdk::Color("black");

  cr->set_source_rgba( current_color.get_red_p(),
            current_color.get_green_p(),
            current_color.get_blue_p(),
            transparency );

  cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2, alloc.get_y()+(alloc.get_height()-h)/2 );
  layout->add_to_cairo_context(cr);
  cr->fill();
//   }else
//   {
//     cr->set_source_rgb(1,
//                        1,
//                        1 );
//     
//     cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2+1, alloc.get_y()+(alloc.get_height()-h)/2+1 );
//     layout->add_to_cairo_context(cr);
//     cr->fill();
//     
//     cr->set_source_rgba( 0,
//                          0,
//                          0,
//                          0.4 );
//     
//     cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2, alloc.get_y()+(alloc.get_height()-h)/2 );
//     layout->add_to_cairo_context(cr);
//     cr->fill();
//   }
}
// -------------------------------------------------------------------------
