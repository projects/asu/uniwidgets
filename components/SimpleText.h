#ifndef _SIMPLETEXT_H
#define _SIMPLETEXT_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <plugins.h>
#include <global_macros.h>
#include <components/SimpleView.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Базовый класс для виджета Текст.
 * \par
  * Компонент служит базовым классом для текстовых виджетов таких как Text и TextBlink.
  * Внутри класса определены базовые методы и переменные для виджетов типа текст.
*/
class SimpleText : public SimpleView
{

public:
  SimpleText();
  explicit SimpleText(SimpleView::BaseObjectType* gobject);
  virtual ~SimpleText();

  /* Methods */
  virtual void text_draw();				/*!< отрисовка текста */

  int get_text_width(){return layout->get_width();}
  int get_text_height(){return layout->get_height();}
  void get_pixel_size(int &w,int &h){layout->get_pixel_size(w, h);}

protected:
  /* Event handlers */
  virtual bool on_expose_event(GdkEventExpose* event);
  virtual void on_realize();
  virtual void on_transparency_changed();
  virtual void on_dropshadow_changed();
  virtual void on_text_changed();
  /* Variables */
  Glib::RefPtr<Pango::Layout> layout;
  Cairo::RefPtr<Cairo::Context> cr;

  /* Variables */
  Gdk::Color current_color;   /*!< текущий цвет виджета */
  double transparency;         /*!< прозрачность виджета */

private:
  /* Methods */
  void constructor();
  DISALLOW_COPY_AND_ASSIGN(SimpleText);
  ADD_PROPERTY( property_text_, Glib::ustring )
  ADD_PROPERTY( property_drop_shadow_, bool )
  ADD_PROPERTY( property_transparency_, double )
  ADD_PROPERTY( property_alignment_, Pango::Alignment )

};

}
#endif
