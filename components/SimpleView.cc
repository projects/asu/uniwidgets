#include <iostream>
#include <gtkmm.h>
#include <ThemeLoader.h>
#include "SimpleView.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
// -------------------------------------------------------------------------
SimpleView::SimpleView() :
                Glib::ObjectBase("simpleview")
                ,Gtk::EventBox()
                ,show_(*this, "show", false) \
                ,blink_(*this, "blink", false) \
                ,mode(*this, "mode", 0) \
                ,priority(*this, "priority", 0)
{
  // DO SMTH
}
// -------------------------------------------------------------------------
SimpleView::SimpleView(SimpleView::BaseObjectType* gobject) :
    Gtk::EventBox(gobject)
    ,show_(*this, "show", false) \
    ,blink_(*this, "blink", false) \
    ,mode(*this, "mode", 0) \
    ,priority(*this, "priority", 0)
{
  // DO SMTH
}
// -------------------------------------------------------------------------
SimpleView::~SimpleView()
{
  // DO SMTH
}