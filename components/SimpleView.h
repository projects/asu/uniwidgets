#ifndef _SIMPLEVIEW_H
#define _SIMPLEVIEW_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <plugins.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Отображение.
 * \par
 * Компонент отображения. Базовый класс для таких компонент как картинка, текст...
 * Так как с точки зрения отображения картинки и текст являются однотипными объектами, базовый класс
 * позволяет использовать эти объекты в логике без спецификации типа. В базовом классе определен
 * минимальный набор виртуальных функция, которые формируют интерфейс и свойства. Функции blink(мигание)
 * пустые так как только мигающие объекты определяют этот процесс...
*/
class SimpleView : public Gtk::EventBox
{
public:
  SimpleView();
  explicit SimpleView(Gtk::EventBox::BaseObjectType* gobject);
  virtual ~SimpleView();

  /* Methods */
  virtual void start_blink() {;}
  virtual void stop_blink() {;}
  virtual bool is_blinking(){return false;}
  virtual bool can_blinking(){return false;}  //Определяем способен ли компонент мигать

private:
  DISALLOW_COPY_AND_ASSIGN(SimpleView);
  /* Properties */
  ADD_PROPERTY( show_, bool )
  ADD_PROPERTY( blink_, bool )
  ADD_PROPERTY( mode, long )
  ADD_PROPERTY( priority, long )
};

}

#endif
