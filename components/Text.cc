#include <gtkmm.h>
#include <iostream>
#include <ThemeLoader.h>
#include "Text.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
// -------------------------------------------------------------------------
#define TEXT_THEME                      "theme"
#define TEXT_USE_THEME              "use-theme"
#define TEXT_TEXT                          "text"
#define TEXT_DROP_SHADOW      "drop-shadow"
#define TEXT_TRANSPARENCY     "tranparency"
#define TEXT_ALIGNMENT            "alignment"
#define TEXT_FONT_NAME          "font-name"
#define TEXT_FONT_ABS_FONT_SIZE    "abs-font-size"
#define TEXT_FONT_COLOR       "font-color"
// -------------------------------------------------------------------------
#define INIT_TEXT_PROPERTIES() \
   property_theme_(*this, TEXT_THEME, "text_normal") \
  ,property_use_theme_(*this, TEXT_USE_THEME, false) \
  ,property_font_name_(*this, TEXT_FONT_NAME, "Liberation Sans 16") \
  ,property_abs_font_size_(*this, TEXT_FONT_ABS_FONT_SIZE, -1) \
  ,property_font_color_(*this, TEXT_FONT_COLOR, Gdk::Color("grey"))
// -------------------------------------------------------------------------
void Text::constructor()
{
    GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
    goclass->set_property = &custom_set_property_callback_object<Text>;
}
// -------------------------------------------------------------------------
Text::Text() :
  Glib::ObjectBase("text")
  ,INIT_TEXT_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
Text::Text(SimpleText::BaseObjectType* gobject) :
  SimpleText(gobject)
  ,INIT_TEXT_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
Text::~Text()
{
}
// -------------------------------------------------------------------------
// bool Text::on_expose_event(GdkEventExpose* event)
// {
//   SimpleText::on_expose_event(event);
//   text_draw();
//   return true;
// }
// -------------------------------------------------------------------------
void Text::on_realize()
{
  SimpleText::on_realize();

  on_text_changed();
  connect_property_changed("theme", sigc::mem_fun(*this, &Text::on_theme_changed));
  connect_property_changed("font-name", sigc::mem_fun(*this, &Text::on_font_name_changed));
  connect_property_changed("font-color", sigc::mem_fun(*this, &Text::on_use_theme_changed));
  connect_property_changed("use-theme", sigc::mem_fun(*this, &Text::on_use_theme_changed));
  connect_property_changed("abs-font-size", sigc::mem_fun(*this, &Text::on_font_name_changed));
  connect_property_changed("drop-shadow", sigc::mem_fun(*this, &Text::on_use_theme_changed));

  on_use_theme_changed();
}
// -------------------------------------------------------------------------
void Text::on_size_request(Gtk::Requisition* req)
{
  SimpleText::on_size_request(req);
  *req = Gtk::Requisition();
  layout->get_pixel_size(req->width, req->height);
  if ( property_drop_shadow_ )
  {
    req->width += 1;
    req->height += 1;
  }
}
// -------------------------------------------------------------------------
void Text::on_size_allocate(Gtk::Allocation& alloc)
{
  SimpleText::on_size_allocate(alloc);
}
// -------------------------------------------------------------------------
void Text::load_property_settings()
{
  off_color = property_font_color_;
  Pango::FontDescription fd( get_property_font_name_() );
  if (get_property_abs_font_size_() > 0)
    fd.set_absolute_size( get_property_abs_font_size_() * Pango::SCALE);
  layout->set_font_description( fd );
  current_color = off_color;
}
// -------------------------------------------------------------------------
void Text::on_text_changed()
{
  layout->set_text(property_text_);
  queue_resize();
}
// -------------------------------------------------------------------------
void Text::load_theme_settings()
{
  ThemeLoader tl(property_theme_);

  current_color = tl.get_color("color");
  layout->set_font_description( Pango::FontDescription( tl.get_string("font") ) );

}
// -------------------------------------------------------------------------
void Text::on_theme_changed()
{
  if ( property_use_theme_ )
  {
    load_theme_settings();
    queue_resize();
  }
}
// -------------------------------------------------------------------------
void Text::on_use_theme_changed()
{
  if ( property_use_theme_ )
    load_theme_settings();
  else
    load_property_settings();

  queue_resize();
}
// -------------------------------------------------------------------------
void Text::on_font_name_changed()
{
  if ( property_use_theme_ ) return;
  
  Pango::FontDescription fd(property_font_name_);
  if ( property_abs_font_size_ > 0)
    fd.set_absolute_size( property_abs_font_size_ * Pango::SCALE );
  layout->set_font_description(fd);
  queue_resize();
}
// -------------------------------------------------------------------------
void Text::on_font_color_changed()
{
  if ( property_use_theme_ ) return;
  queue_draw();
}
// -------------------------------------------------------------------------
// void Text::on_transparency_changed()
// {
//   transparency = property_transparency_;
//   queue_draw();
// }
// -------------------------------------------------------------------------
// void Text::text_draw()
// {
//   Gtk::Allocation alloc = get_allocation();
//   cr = get_window()->create_cairo_context();
// 
//   int w,h;
//   layout->set_alignment( property_alignment_ );
//   layout->get_pixel_size(w,h);
//   set_size_request(w,h);
// 
//   if (property_drop_shadow_)
//   {
//     cr->set_source_rgb(0,0,0);
//     cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2+1, alloc.get_y()+(alloc.get_height()-h)/2+1 );
//     layout->add_to_cairo_context(cr);
//     cr->fill();
//   }
// 
//   /* Glade can set color to NULL so we need to check it.
//     May be later we will find another solution
//   */
//   if (current_color.gobj() == NULL)
//     current_color = Gdk::Color("black");
// 
//   cr->set_source_rgba( current_color.get_red_p(),
//             current_color.get_green_p(),
//             current_color.get_blue_p(),
//             transparency );
// 
//   cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2, alloc.get_y()+(alloc.get_height()-h)/2 );
//   layout->add_to_cairo_context(cr);
//   cr->fill();
// }
