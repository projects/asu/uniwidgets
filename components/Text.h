#ifndef _TEXT_H
#define _TEXT_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <plugins.h>
#include <global_macros.h>
#include <components/SimpleText.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Текст.
 * \par
 * Компонент предназначен для отображения текста. Используется для отображения текста сигналов.
*/
class Text : public SimpleText
{
public:
  Text();
  explicit Text(SimpleText::BaseObjectType* gobject);
  virtual ~Text();

  /* Methods */
  void on_font_name_changed();			          /*!< обработчик сигнала об изменении свойства имени шрифт виджета */
  void on_font_color_changed();			          /*!< обработчик сигнала об изменении свойства цвет виджета  */
  void on_use_theme_changed();			        /*!< обработчик сигнала об изменении свойства использования темы виджета */
  virtual void on_theme_changed();		      /*!< обработчик сигнала об изменении свойства тема виджета */
  virtual void load_theme_settings();		      /*!< загрузка настроек из темы */
  virtual void load_property_settings();		  /*!< загрузка настроек из свойств виджета */

  virtual void on_size_request(Gtk::Requisition* requisition);
  virtual void on_realize();
  virtual void on_size_allocate(Gtk::Allocation& alloc);
//  virtual void text_draw();       /*!< отрисовка текста */

protected:
  /* Variables */
  Gdk::Color off_color;           /*!< цвет текста выключенного состояния виджета */

  /* Methods */
  virtual void on_text_changed();
//   void on_map();

private:
  /* Methods */
  void constructor();
  /* Variables */
  DISALLOW_COPY_AND_ASSIGN(Text);
  /* Properties */
  ADD_PROPERTY( property_theme_, Glib::ustring )
  ADD_PROPERTY( property_use_theme_, bool )
  ADD_PROPERTY( property_font_name_, Glib::ustring )
  ADD_PROPERTY( property_abs_font_size_, gint )
  ADD_PROPERTY( property_font_color_, Gdk::Color )
};

}
#endif
