#include <iostream>
#include <gtkmm.h>
#include <ThemeLoader.h>
#include "TextBlink.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
// -------------------------------------------------------------------------
#define TEXTBLINK_STATE      "state"
#define TEXTBLINK_ON_TRANSPARENCY    "on-tranparency"
#define TEXTBLINK_ON_FONT_COLOR    "on-font-color"
#define TEXTBLINK_ON_FONT_NAME    "on-font-name"
#define TEXTBLINK_FONT_ON_ABS_FONT_SIZE  "on-abs-font-size"
// -------------------------------------------------------------------------
#define INIT_TEXTBLINK_PROPERTIES() \
                                  property_state_(*this, TEXTBLINK_STATE, true) \
                                  ,property_on_transparency_(*this, TEXTBLINK_ON_TRANSPARENCY, 1) \
                                  ,property_on_font_color_(*this, TEXTBLINK_ON_FONT_COLOR, Gdk::Color("yellow")) \
                                  ,property_on_font_name_(*this, TEXTBLINK_ON_FONT_NAME, "Libertation Sans 16") \
                                  ,property_on_abs_font_size_(*this, TEXTBLINK_FONT_ON_ABS_FONT_SIZE, -1)
// -------------------------------------------------------------------------
  Blinker TextBlink::blinker;
// -------------------------------------------------------------------------
  void TextBlink::constructor()
{
    GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
    goclass->set_property = &custom_set_property_callback_object<TextBlink>;

  connect_property_changed("state", sigc::mem_fun(*this, &TextBlink::on_state_changed));
  connect_property_changed("on-font-name", sigc::mem_fun(*this, &TextBlink::on_font_name_changed));
  connect_property_changed("on-font-color", sigc::mem_fun(*this, &TextBlink::load_property_settings));
  connect_property_changed("on-transparency", sigc::mem_fun(*this, &TextBlink::on_transparency_changed));
  connect_property_changed("on-abs-font-size", sigc::mem_fun(*this, &TextBlink::on_font_name_changed));
}
// -------------------------------------------------------------------------
TextBlink::TextBlink() :
  Glib::ObjectBase("textblink")
  ,blinking_(false)
  ,sleep_blinking_(false)
  ,INIT_TEXTBLINK_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
TextBlink::TextBlink(Text::BaseObjectType* gobject) :
  Text(gobject)
  ,blinking_(false)
  ,sleep_blinking_(false)
  ,INIT_TEXTBLINK_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
TextBlink::~TextBlink()
{
}
// -------------------------------------------------------------------------
void TextBlink::on_realize()
{
  Text::on_realize();

  on_state_changed();
}
// -------------------------------------------------------------------------
void TextBlink::load_property_settings()
{
  on_color = property_on_font_color_;
  off_color = property_font_color_;
  Pango::FontDescription fd( get_property_font_name_() );
  if (get_property_abs_font_size_() > 0)
    fd.set_absolute_size( get_property_abs_font_size_() * Pango::SCALE);
  layout->set_font_description( fd );
  current_color = get_property_state_() ? on_color : off_color;
}
// -------------------------------------------------------------------------
void TextBlink::load_theme_settings()
{
  ThemeLoader tl(property_theme_);

  on_color = tl.get_color("on_color");
  off_color = tl.get_color("off_color");
  Pango::FontDescription fd( tl.get_string("font") );
  if (property_on_abs_font_size_ > 0)
    fd.set_absolute_size( property_on_abs_font_size_ * Pango::SCALE );
  layout->set_font_description( fd );
  current_color = property_state_ ? on_color : off_color;
}
// -------------------------------------------------------------------------
bool TextBlink::is_blinking()
{
  return blinking_;
}
// -------------------------------------------------------------------------
void TextBlink::start_blink()
{
  if (blinking_)
    return;

  if(!is_mapped())
  {
    blinking_ = true;
    sleep_blinking_ = true;
    return;
  }

  blinking_ = true;
  blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].
      connect(sigc::mem_fun(this, &TextBlink::blink));
}
// -------------------------------------------------------------------------
void TextBlink::stop_blink()
{
  if (!blinking_)
    return;

  if(!is_mapped())
  {
    sleep_blinking_ = false;
  }

  blinking_ = false;
  blink_connection_.disconnect();
  //TODO:use sensor_state_ from signals
  set_property_state_(true);
}
// -------------------------------------------------------------------------
void TextBlink::blink( bool blink_state ,int time)
{
  set_property_state_(blink_state);
}
// -------------------------------------------------------------------------
void TextBlink::on_state_changed()
{
  current_color = get_property_state_() ? on_color : off_color;
  transparency = get_property_state_() ? get_property_on_transparency_() : get_property_transparency_();
  property_drop_shadow_.set_value(get_property_state_());
  queue_resize();
}
// -------------------------------------------------------------------------
void TextBlink::on_map()
{
  Gtk::EventBox::on_map();
  if(sleep_blinking_)
  {
    blinking_ = false;
    start_blink();
    sleep_blinking_ = false;
  }
}
// -------------------------------------------------------------------------
void TextBlink::on_unmap()
{
  Gtk::EventBox::on_unmap();
  if(is_blinking())
  {
    stop_blink();
    sleep_blinking_ = true;
    blinking_ = true;
  }
}
// -------------------------------------------------------------------------
