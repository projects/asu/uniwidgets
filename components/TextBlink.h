#ifndef _TEXTBLINK_H
#define _TEXTBLINK_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <SBlinker.h>
#include <global_macros.h>
#include <components/Text.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Текст мигающий.
 * \par
 * Компонент предназначен для отображения текста с возможностью мигания.
 * Используется для отображения текста сигналов.
*/
class TextBlink : public Text
{
public:
  TextBlink();
  explicit TextBlink(Text::BaseObjectType* gobject);
  virtual ~TextBlink();

  /* Methods */
  virtual void start_blink();            /*!< включить мигание */
  virtual void stop_blink();            /*!< выключить мигание */
  virtual bool is_blinking();            /*!< проверка мигания */

  void on_state_changed();            /*!< обработчик события изменения состояния виджета */

  virtual void load_property_settings();          /*!< загрузка настроек из свойств виджета */
  virtual void load_theme_settings();          /*!< загрузка настроек из темы */
  virtual void on_realize();
  virtual bool can_blinking(){return true;}

protected:
  /* Variables */
  Gdk::Color on_color;              /*!< цвет текста включенного состояния виджета */

  bool blinking_;
  bool sleep_blinking_;
  void on_map();
  void on_unmap();

  /* Event handlers */
  sigc::connection blink_connection_;
  static Blinker blinker;              /*!< класс реализующий свойство мигание виджета */
private:
  /* Methods */
  void constructor();
  virtual void blink( bool blink_state ,int time = DEFAULT_BLINK_TIME); /*!< функция реализующая процесс мигания */
  DISALLOW_COPY_AND_ASSIGN(TextBlink);
  /* Properties */
  ADD_PROPERTY( property_state_, bool )
  ADD_PROPERTY( property_on_transparency_, double )
  ADD_PROPERTY( property_on_font_color_, Gdk::Color )
  ADD_PROPERTY( property_on_font_name_, Glib::ustring )
  ADD_PROPERTY( property_on_abs_font_size_, gint )
};

}

#endif
