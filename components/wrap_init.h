#ifndef COMPONENTS_WRAP_INIT_H_INCLUDED
#define COMPONENTS_WRAP_INIT_H_INCLUDED

namespace UniWidgets
{

void wrap_init_components();

}

#endif /* !COMPONENTS_WRAP_INIT_H_INCLUDED */
