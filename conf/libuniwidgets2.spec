%def_enable doc
%def_enable main
%def_enable uniset2
%def_enable webkit

%define oname uniwidgets2
Name: libuniwidgets2
Version: 2.0.0
Release: eter8
Summary: Collection of GTK widgets for use with UniSet2

License: GPL
Group: Development/C++
Url: http://etersoft.ru

Source: %name-%version.tar

# Automatically added by buildreq on Mon Nov 24 2014
# optimized out: fontconfig fontconfig-devel gcc-c++ glib2-devel libatk-devel libatkmm-devel libcairo-devel libcairomm-devel libcommoncpp2-devel libfreetype-devel libgdk-pixbuf libgdk-pixbuf-devel libgio-devel libglade-devel libglibmm-devel libgtk+2-devel libgtkmm2-devel libjavascriptcoregtk2-devel libomniORB-devel libpango-devel libpangomm-devel libsigc++2-devel libsoup-devel libstdc++-devel libxml2-devel pkg-config
BuildRequires: libglademm-devel libgladeui-devel

%if_enabled doc
BuildRequires: doxygen
%endif

%if_enabled uniset2
BuildRequires: libuniset2-devel libuniset2-extension-common-devel
%endif

%if_enabled main
BuildRequires: doxygen librsvg-devel
%if_enabled webkit
BuildRequires:libwebkitgtk2-devel
# libwebkitgtk2 versions > 1.8.1 may crash while working with http servers that are using javascript
Requires: libwebkitgtk2
%endif
%endif

Requires: libglademm >= 2.4

%set_verify_elf_method textrel=strict,rpath=strict,unresolved=strict
%set_verify_elf_method rpath=relaxed,fhs=relaxed

%description
Collection of GTK widgets for use with UniSet

%if_enabled main
%package doc
Group: Development/C++
Summary: Documentations for developing with UniWidgets
Requires: %name = %version-%release
BuildArch: noarch

%description doc
Documentations for developing with UniWidgets

%package devel
Summary: Devel package for %name
Group: Development/C++
Requires: %name = %version-%release

%description devel
Devel package for %name

%package glade-plugin
Summary: Plugin for Glade-3 adds %name support
Group: Development/C++
Requires: %name = %version-%release
Requires: glade3 => 3.4.5

%description glade-plugin
Glade-3 plugin for editing interface %name
%endif

%if_enabled uniset2
%package uniset2-plugin
Summary: Plugin for UniSet2 adds %name support
Group: Development/C++
Requires: %name = %version-%release
Requires: libuniset2 = %{get_SVR libuniset2}
Release: %{release}_%{get_version libuniset2}.%{get_release libuniset2}

%description uniset2-plugin
UniSet2 plugin for editing interface %name
%endif

%prep
%setup

%build
%autoreconf
%if_enabled doc
%configure --disable-static  %{subst_enable uniset2} %{subst_enable webkit}
%else
%configure --disable-docs --disable-static  %{subst_enable uniset2} %{subst_enable webkit}
%endif
%make_build

%install
%makeinstall_std
%if_enabled main
rm -f %buildroot%_libdir/glade3/modules/*.la
rm -f %buildroot%_libdir/libglade/2.0/*.la
rm -f %buildroot%_libdir/*.la
%endif

%if_enabled main
%files
%_libdir/libUniWidgets2.so.*
%_libdir/libglade/
%_datadir/%oname/svg/
#%_bindir/uniwidgets-gui-starter

%if_enabled doc
%files doc
%_docdir/%oname
%endif

%files devel
%_libdir/libUniWidgets2.so
%_includedir/%oname
%_includedir/%oname/Services/
%_pkgconfigdir/*.pc

%files glade-plugin
%_libdir/glade3/modules/*
%_datadir/glade3/
%endif

%if_enabled uniset2
%files uniset2-plugin
%_libdir/libUniWidgets2-uniset2.so*
%endif

%changelog
* Tue Oct 04 2016 Vinogradov Aleksey <uzum@server> 2.0.0-eter8
- (22220-builder): build on commit 9a823b48f4b60408907faced4f17652aa6813f43

* Tue Oct 04 2016 Vinogradov Aleksey <uzum@server> 2.0.0-eter7
- (22220-builder): build on commit fbead95dd4f8ecfc6e8107bd18ad063b1e95ed4f

* Wed Sep 28 2016 Vinogradov Aleksey <uzum@server> 2.0.0-eter6
- (22220-builder): build on commit 54943a358e3417a9d1cfd4af0ddf2b4c21a0f434

* Wed Sep 28 2016 Vinogradov Aleksey <uzum@server> 2.0.0-eter5
- (22220-builder): build on commit 92bc9b833b5edf18a4bcfc56dacf9f4b915dff28

* Wed Sep 21 2016 Vinogradov Aleksey <uzum@server> 2.0.0-eter4
- (22220-builder): build on commit 924a8794d5fe3397b72e932df3999ee8df121d26

* Wed Sep 14 2016 Vinogradov Aleksey <uzum@server> 2.0.0-eter3
- (22220-builder): build on commit b3823d9766d3d6b87c55b69ca7a2161adafe98cd

* Tue Sep 13 2016 Vinogradov Aleksei <uzum@server> 2.0.0-eter2
- (uzum): build for new uniset (>=2.5-alt14)

* Mon Sep 12 2016 Vinogradov Aleksei <uzum@server> 2.0.0-eter1
- (uzum): test build 2.0.0 version

* Wed Sep 07 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter7.6
- rebuild for new uniset

* Wed Sep 07 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter7.5
- rebuild for new uniset

* Fri Sep 02 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter7.4
- minor fixes 

* Thu Sep 01 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter7.3
- fixes for new uniset (>=2.5-alt4)

* Fri Aug 26 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter7.2
- rebuild for new uniset

* Thu Aug 25 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter7.1
- rebuild for new uniset

* Sun Aug 21 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter7
- rebuild for new uniset

* Sun Aug 21 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter6.1
- up version

* Sun Aug 21 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter5.1
- rebuild for new uniset

* Thu Aug 11 2016 Vinogradov Aleksey <uzum@server> 1.2.2-eter6
- get_info() modified

* Tue Aug 09 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter5
- rebuild for new uniset

* Mon Aug 08 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter4.1
- devel test build 

* Wed Aug 03 2016 Vinogradov Aleksey <uzum@server> 1.2.2-eter4
- error in confirm corrected

* Wed Aug 03 2016 Vinogradov Aleksey <uzum@server> 1.2.2-eter3
- test build

* Wed Aug 03 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.2-eter2
- update requires

* Tue Jul 26 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.1-eter25
- update requires

* Mon Jul 25 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.1-eter24
- rebuild for new uniset

* Sun Jul 24 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.1-eter23
- up build

* Sun Jul 24 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.1-eter22.1
- test build

* Sun Jul 24 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.1-eter22
- up build

* Wed Jul 06 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter21
- user slots added in getInfo

* Wed Jul 06 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter20
- Journal optimization

* Thu Apr 21 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter19
- build whithout UDKeyboard

* Tue Apr 19 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter18
- build with new uniset2

* Fri Mar 11 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter16
- USVGButton textname alignment added

* Sun Feb 21 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter14
- waitReady added

* Sun Feb 08 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter13
- UIndicatorContainer on_size_allocate corrected

* Thu Jan 21 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter12
- build (USVGButton changes)

* Mon Jan 18 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter11
- container size corrected

* Mon Jan 18 2016 Pavel Vainerman <pv@etersoft.ru> 1.2.1-eter11.1
- up version build

* Mon Jan 18 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.1-eter11
- rebuild for libuniset

* Mon Jan 18 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.1-eter10.1
- test rebuild for new uniset

* Sat Jan 16 2016 Vinogradov Aleksey <uzum@server> 1.2.1-eter10
- build for new uniset2 with bit message corrections

* Fri Jan 15 2016 Pavel Vainerman <pv@altlinux.ru> 1.2.1-eter9
- build for new uniset2

* Mon Dec 21 2015 Vinogradov Aleksey <uzum@server> 1.2.1-eter7
- build for new uniset2

* Wed Dec 16 2015 Vinogradov Aleksey <uzum@server> 1.2.1-eter5
- build for libuniset2 >= 2.2-alt12

* Tue Dec 15 2015 Vinogradov Aleksey <uzum@server> 1.2.1-eter4
- signals get_info output corrected

* Mon Dec 14 2015 Vinogradov Aleksey <uzum@server> 1.2.1-eter3
- blink with confirm in container corrected

* Sat Dec 12 2015 Vinogradov Aleksey <uzum@server> 1.2.1-eter1
- SensorProp optimization

* Thu Dec 10 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter21
- сделан getInfo() для UProxyWidget

* Wed Dec 02 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter20
- UVoid::set_state(bool) base realisation corrected

* Tue Nov 24 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter19
USVGButton color properties

* Tue Nov 10 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter16
- uniset2 requires updated

* Mon Oct 05 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter15
- UValueIndicator interface corrected + UIndicatoContainer confirm logic corrected

* Sat Oct 03 2015 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter14
- rebuild for new libuniset

* Fri Sep 25 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter13
- UIndicatorContainer blink logic corrected

* Thu Sep 24 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter12
- confirmsignal emit() method corrected

* Wed Sep 23 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter11
- USVGBar2 corrections

* Tue Sep 22 2015 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter10
- new UJournal..

* Tue Sep 22 2015 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter9
- rebuild for new libuniset

* Mon Aug 31 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter8.14
- corrected build with fonts for ujournal

* Tue Aug 25 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter8.13
- editor modified

* Mon Jul 20 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter8.11
- Font for ujournal

* Mon Jun 22 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter8.10
- USVGText.h corrections

* Thu Jun 18 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter8.9
- some more h-file corrections

* Thu Jun 18 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter8.8
- oscil h-file corrections

* Wed Jun 17 2015 Vinogradov Aleksey <uzum@server> 1.2.0-eter8.7
- build with Oscillograph changes

* Tue Jun 16 2015 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter8.6
- rebuild for new libuniset (2.1-alt7.7)

* Sun Jun 07 2015 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter8.4
- rebuild for new libuniset (2.1-alt7.1) 

* Thu Jun 04 2015 Pavel Vainerman <pv@etersoft.ru> 1.2.0-eter8.3
- rebuild for new libuniset (2.1-alt6)

* Mon May 11 2015 Pavel Vainerman <pv@etersoft.ru> 1.2.0-eter8.1
- rebuild for new libuniset (2.0-alt31)

* Sat May 09 2015 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter8
- global optimization N1 (map --> unordered_map (c++11)) 

* Sat May 09 2015 Pavel Vainerman <pv@etersoft.ru> 1.2.0-eter7.1
- rebuild for new libuniset

* Wed Jan 21 2015 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter7
- merge current changes from 1.0

* Sun Jan 18 2015 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter6
- changes for new libuniset (2.0-alt11)

* Mon Nov 24 2014 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter5
- changes for new libuniset (2.0-alt10)
 
* Thu Jul 24 2014 Pavel Vainerman <pv@altlinux.ru> 1.2.0-eter3.1
- rebuild

* Thu May 15 2014 Vinogradov Aleksey <uzum@server> 1.0.1-eter53.3.2
- (p5): Local build on commit fb54befe3fcdeb1f27cc7a480fd8c5bf550f4afd

* Tue May 06 2014 Vinogradov Aleksey <uzum@server> 1.0.1-eter53.3.1
- (p5): Local build on commit 94538bf798553ef756a9ee4562eccdb8c8a6966e

* Thu Apr 03 2014 Ilya Polshikov <ilyap@etersoft.ru> 1.2.0-eter3
- Build with UniSet2 on commit 1143f2622bd4cd4c1f14216153b8e48d5684e553

* Wed Apr 02 2014 Ilya Polshikov <ilyap@etersoft.ru> 1.2.0-eter2
- Build after merge on commit 3361a34f244a61b4e4823c756fb2a30fa4f72872

* Thu Mar 20 2014 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter53.3
- (yauza): Local build on commit cbf5f840f50589aa7162abfbc0add5e4994b8ffb

* Tue Mar 11 2014 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter53.2
- Local build on commit ca199aceaa55e238223322e1bdd8a163ee69e88a

* Wed Feb 12 2014 Aleksey Surov <alex@server.setri.ru> 1.0.1-eter52.1
- (mb22030): Local build on commit 117f759f36a26e56a63dcd544fa100117fd9e16f

* Mon Feb 10 2014 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter52
- New build on commit 178de768d4185c5f6f8aef7823f165e852bde8a1

* Fri Jan 31 2014 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter51
- (pavelnote): Local build on commit 24af771abbb3e7ec5febc990a2b740f6035a7e45

* Thu Dec 26 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter50
- Fix lots of widgets. Some code optimization.
- New build on commit 3ffc7789c3c71aa1414cfe099503c4c34a0d2ce1

* Tue Dec 24 2013 Aleksey Surov <alex@server.setri.ru> 1.0.1-eter50
- (mb22030): Local build on commit 7a2c4965264bbfb33b3e54ac2e58bd2de95b64ee

* Mon Sep 23 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter49
- New build in office.
- Fix require for uniset.

* Mon Sep 09 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.10
- Local build on commit 6bf73102e9b942a97c2c52dad5716374fed3aee0

* Sun Sep 08 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.9
- Local build on commit 602b5711af7e8b2ea9b71fc4135877a216a9bd59

* Thu Sep 05 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.8
- Local build on commit 31c5759935cdc1a03822e79dd96ff291652036b1

* Wed Sep 04 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.7
- Local build on commit 274c7cc946534e6712e2441ecf2b40cb0e7285a1

* Sun Sep 01 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.6
- Local build on commit 5731800b60e4896b164e6e934f4b78e66fd1fa2c

* Sat Aug 31 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.5
- Local build on commit 9dee6d8ebd674bcfe5b789a339e481d598d939eb

* Fri Aug 30 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.4
- Local build on commit 7fa0d681b254ed12babc34446b57aed7f77349bb

* Thu Aug 29 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.3
- Local build on commit f9fadefa03539f23d5cef34f38e09b1c7fa51dcc

* Thu Aug 29 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.2
- Local build on commit 371d0e6d56b9c12d78d317a986d734d8e1834826

* Thu Aug 29 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter48.1
- Local build for yauza

* Fri Jun 14 2013 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter48
- (builder): Local build on commit 2fd04a426edd2177cb0ca234cecdd52f5a3de52c

* Tue Jan 29 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter47
- Local build on commit 8de52eae96b242289b26b3dc63463fe8b2730451

* Mon Jan 28 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter46
- Local build on commit 574bd35c289960265edd2d7529eaa9164031572a
- Added support Gtk::Builder

* Fri Jan 25 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter45
- Local build on commit bd1eea4b177cc6a37fd00518cd83f1e4ff3eb46b

* Tue Jan 22 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter44
- Local build on commit 57192077579649d02bae0f409e0fcd63b9a7d379

* Thu Jan 17 2013 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter43
- Local build on commit a7e55a8f60afbfd6222c3fe29587322de504e4b8
- Add docs

* Thu Jan 17 2013 Aleksey Surov <alex@server.setri.ru> 1.0.1-eter42.3
- (mb22030): Local build on commit 0c54961653346e3fb97cc5232a510584b03c023b

* Thu Jan 17 2013 Aleksey Surov <alex@server.setri.ru> 1.0.1-eter42.2
- (mb22030): Local build on commit ff650bcabfa0962afc27bba68ee7ffdc12b7d91f

* Thu Jan 17 2013 Aleksey Surov <alex@server.setri.ru> 1.0.1-eter42.1
- (mb22030): Local build on commit ff650bcabfa0962afc27bba68ee7ffdc12b7d91f

* Mon Dec 03 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter42
- Local build on commit d8ee4562aa602d79c8507f81b97b6fc6cfef68ea

* Thu Nov 29 2012 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter41
- (yauza): Local build on commit 2b106da50a0ab2f2ea7089407daf91521e6f5818

* Fri Nov 23 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter40
- Local build on commit 47ce3f60ea0dc1247b63a8cdc43be9502ab2c9ac

* Fri Nov 23 2012 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter39
- (yauza): Local build on commit 6f49297efa59fe0607399d88ad647fca7d49df8b

* Thu Nov 22 2012 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter38
- (yauza): Local build on commit c106e808c676f78a5b3807de83651eba37ea3284

* Thu Nov 22 2012 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter37
- (yauza): Local build on commit 65e22f1d40a5d5914e36a0d20b61c33f175d9552

* Thu Nov 22 2012 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter36
- (yauza): Local build on commit 0445d0844b6ed68d18ebbaf64558c0db95782938

* Wed Nov 14 2012 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter35
- (yauza): Local build on commit 0001dfdd8c634c7ecaddfef4adbe58effc9f7c67

* Thu Nov 01 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter34
- Local build on commit 2ce308c65d1767e7b378ce70ac6b38f80844401b

* Thu Nov 01 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter33
- Local build on commit 23a0f8535c12abe131ba769e1c127d4f573f22cc

* Fri Oct 26 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter32.4
- (inote): Local build on commit 00f31923019bcb57f659b2ae58ff051038262f7a

* Fri Oct 26 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter32.3
- (inote): Local build on commit 99ddaf660e192b36be234cae2fff0668994c26d4

* Wed Oct 24 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter32.2
- (inote): Local build on commit 6132754b97f2cc7277cc95458ff2b54a559b6398

* Wed Oct 24 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter32.1
- (inote): Local build on commit d99fa13798853660811e7a7296cf42b54118a210

* Wed Oct 17 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter32
- Local build on commit 72ba0b024ca92566cc2f9450318592e4d90e73f2

* Tue Oct 09 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter31
- Local build on commit 3cbb2e567097295121fd4410a41de7501d2c467f

* Wed Oct 03 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter30
- Local build on commit 0cb58bdc1218e733dc18513853a37229faf5390b

* Fri Sep 28 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter29
- Local build on commit 6cffc19dede967f1e0d810cf6b8197723a83d33f

* Fri Sep 14 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter28
- Local build on commit 2fb54773335176da4c52e06dc8a6720cd9439012

* Mon Sep 03 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter27
- (inote): Local build on commit 82df80938602514c97ae144f95dfdd85d9a8e46e

* Fri Aug 31 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter26
- (inote): Local build on commit 371fb07843883d799d74b2de3acb9bc3475f5133

* Wed Aug 29 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter25
- (inote): Local build on commit dee65187bd50b402eb63131a97476caaba80d67d

* Fri Aug 17 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter24
- (inote): Local build on commit b6adf2940075413ed5679269ce822d38c5fd0e02

* Wed Aug 15 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter23
- (inote): Local build on commit 0bbd33fc6e493b7cd3eda5599c88b03d37b2cd7c

* Mon Aug 13 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter22
- (inote): Local build on commit 50a637687be1728f169b14be18313b2001c0662e

* Tue Aug 07 2012 Vinogradov Aleksey <uzum@server> 1.0.1-eter13.4
- Local build on commit f2f2e686a3cf70797cc9b6270b8aa418c2112219

* Wed Aug 01 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter21
- (inote): Local build on commit 7a5cc64368ff5a9a80a1f47026606ab274cbe294

* Tue Jul 31 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter20
- (inote): Local build on commit 99827e6343477eae7a4d178d62c5870f6cead55e

* Tue Jul 31 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter19
- (inote): Local build on commit 70bb64718c39f64041b942374b8adf607b598d2c

* Mon Jul 30 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter18
- (inote): Local build on commit c7aa0fcde753a23570c096cfc389575ff0ac6c0d

* Wed Jul 25 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter17
- (inote): Local build on commit 88872ef009a178c137ac912fd505a891153b8312

* Thu Jul 19 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter16
- (inote): Local build on commit 899dbe416f4f233f6c79e8d422538355235913f3

* Tue Jul 17 2012 Alex Surov <alex@server.setri.ru> 1.0.1-eter13.3
- Local build on commit 521437c81c279ef762dd08ec6755de59757b39ad

* Wed May 30 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter15
- New build 1.0.1-eter15 on commit 2ea89b31c924dba1e26d4d4459c64388c32afece

* Fri May 11 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter14
- New build 1.0.1-eter14 on commit 8d65e247

* Thu Mar 15 2012 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter13
- Local build on commit 2aacc8d51c5b77b0bbda42934a1a823ac185d7ad

* Thu Mar 15 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter12
- new build on commit 315f6da459f26f3862566d3a1212523fb7067803

* Wed Mar 14 2012 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter11
- Local build on commit 6179b2ede357fcada98fe189668ea8b1f7a3900a

* Mon Mar 12 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter10
- new build for p6 on commit 06160200f36cc3bacaeba31c4aed1108cb19f760

* Mon Mar 12 2012 Pavel Turchinsky <pavel_t@etersoft.ru> 1.0.1-eter9
- Local build on commit 9756c7c2b0ac771e25a623311de1d37785685c89

* Sun Mar 11 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter8
- new build on commit 616fce

* Fri Mar 09 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter7
- new build on commit 0493262cf054c81772820ef54750b802a58632ab

* Mon Jan 23 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter6
- new build

* Thu Jan 19 2012 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter5
- new build

* Thu Nov 03 2011 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter2
- new build

* Wed Nov 02 2011 Ilya Polshikov <ilyap@etersoft.ru> 1.0.1-eter1
- new build

* Mon Apr 18 2011 Evgeny Sinelnikov <sin@altlinux.ru> 1.0.0-eter1
- build with changed soname to 1.0.0
+ new version builds for project yauza
+ previous version continues for project standpm

* Wed Jan 26 2011 Pavel Vainerman <pv@altlinux.ru> 0.4.2-eter11
- fixes for build in Sisyphus

* Wed Oct 27 2010 Ilya Shpigor <elly@altlinux.org> 0.4.2-eter10
- fix bug with PrinterInterface.h

* Thu Oct 21 2010 Ilya Shpigor <elly@altlinux.org> 0.4.2-eter9
- change the UButton interface

* Mon Oct 11 2010 Ilya Shpigor <elly@altlinux.org> 0.4.2-eter8
- add "invert" property to UButton

* Thu Sep 30 2010 Ivan Donchevskiy <yv@etersoft.ru> 0.4.2-eter7
- new build

* Thu Sep 23 2010 Ivan Donchevskiy <yv@etersoft.ru> 0.4.2-eter6
- new build for standpm

* Fri Aug 27 2010 Ilya Shpigor <elly@altlinux.org> 0.4.2-eter5
- fixes in the USVGBar2 widget for standpm
- add set method for USpinButton widget
- add set method for UValueIndicator widget

* Mon Aug 23 2010 Ilya Shpigor <elly@altlinux.org> 0.4.2-eter4
- fixes in the USVGBar2 and UValueIndicator widgets for standpm

* Tue Jul 06 2010 Ilya Shpigor <elly@altlinux.org> 0.4.2-eter3
- fix vertical and horizontal USVGBar2 drawing

* Tue Jun 01 2010 Vitaly Lipatov <lav@altlinux.ru> 0.4.2-eter2
- cleanup spec, set ExclusiveArch i586

* Wed Mar 24 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4.2-eter1
- USVGBar2 optimization

* Tue Mar 23 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4.1-eter1
- fixed bug with printer
- USVGBar2 rewritten

* Mon Mar 15 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter18
- new builde for uniset

* Tue Mar 09 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter17
- fixed Printer Support

* Fri Mar 05 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter16
- fixed Printer Support

* Fri Feb 26 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter15
- fixes in locking

* Wed Feb 10 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter14
- fixed USVGImage Constructor

* Tue Feb 02 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter13
- build for new uniset

* Thu Jan 28 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter12
- fixed scrolling of Journals

* Wed Jan 27 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter11
- fixed autoconfirming
- fixed screen locking

* Thu Jan 21 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter10
- fixed UValueIndicator

* Thu Jan 14 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter9
- fixed USVGImage

* Thu Jan 14 2010 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter8
- rebuilding

* Mon Dec 28 2009 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter7
- fixed SensorProp - -1 = LocalNode
- added locking interface

* Tue Dec 22 2009 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter6
- fixed confirm signal

* Tue Dec 08 2009 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter5
- resize of usvgindicaotr plus for mb745

* Wed Nov 11 2009 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter4
- fixed autoconfirm

* Wed Nov 11 2009 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter3
- fixed confirming

* Tue Nov 10 2009 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter2
- some changes in UniSetConnector
- fix in UIndicatorContainer
- fix pkg

* Mon Nov 02 2009 Larik Ishkulov <gentro@etersoft.ru> 0.4-eter1
- added Connector interface
- add disconnect processing
- changed some properties' names

* Wed Oct 14 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter18
- fixed UIndicatorContainer locking

* Wed Oct 07 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter17
- fixed journal bug

* Wed Oct 07 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter16
- journal fixes

* Tue Oct 06 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter15
- test build

* Thu Sep 17 2009 Vitaly Lipatov <lav@altlinux.ru> 0.3-eter13
- cleanup code, fix to build with new uniset 0.97
- overload set_sens_id/set_node_id with string sisters
- fix UNISET_WIDGETS for autogen local mode
- fix getPropUtf8 / locale_to_utf8 using

* Thu Sep 10 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter12
- build for new UniSet

* Wed Aug 26 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter11
- printer test with PrinterInterface

* Mon Aug 24 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter10
- lock_screen property added in UIndicatorCOntainer

* Fri Aug 21 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter9
- Fixed journals and screen blocking

* Thu Aug 20 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter8
- added variant of screen blocking

* Mon Aug 17 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter7
- UAPSJournal features added, fixed glade catalog for Journals.

* Fri Aug 14 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter6
- UJournal features added

* Thu Aug 13 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter5
- temporary build for old uniset

* Wed Aug 05 2009 Vitaly Lipatov <lav@altlinux.ru> 0.3-eter4
- checked build with latest uniset

* Mon Aug 03 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter3
- New features for USVGButton we need in mb745

* Thu Jul 30 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter2
- new properties for USVGIndicatorBig, renamed child properties

* Sat Jul 25 2009 Larik Ishkulov <gentro@etersoft.ru> 0.3-eter1
- restructurisation, normal UIndicatorContainer

* Tue Jul 14 2009 Vitaly Lipatov <lav@altlinux.ru> 0.2-eter7
- update buildreqs, cleanup spec, rename spec to libuniwidgets.spec

* Sun Apr 05 2009 Pavel Vainerman <pv@altlinux.ru> 0.2-eter6
- new build

* Sun Apr 05 2009 Pavel Vainerman <pv@altlinux.ru> 0.2-eter5
- new build

* Sun Apr 05 2009 Pavel Vainerman <pv@altlinux.ru> 0.2-eter4
- new build

* Sun Apr 05 2009 Pavel Vainerman <pv@altlinux.ru> 0.2-eter3
- new build

* Sun Apr 05 2009 Pavel Vainerman <pv@altlinux.ru> 0.2-setri1
 new build

* Sun Apr 05 2009 Pavel Vainerman <pv@altlinux.ru> 0.2-alt4
- new build

* Thu Jan 29 2009 Larik Ishkulov <gentro@etersoft.ru> 0.1-alt1
- new build

* Mon May 05 2008 Larik Ishkulov
-Test
