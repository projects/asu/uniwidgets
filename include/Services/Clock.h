#ifndef _CLOCK_H
#define _CLOCK_H
// -------------------------------------------------------------------------
/*! \page page_USG Универсальный пускатель графики
	\section digital_clock Цифровые часы
\par
	Класс реализующий отображение цифровых часов(описание класса Clock).
*/
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <sys/time.h>
#include <sigc++/sigc++.h>
// -------------------------------------------------------------------------
/*!
* \brief Цифровые часы.
* \par
* Класс реализующий отображение цифровых часов на главном окне приложения.
*/
class USVGText;
class Clock {
private:
	USVGText *widget1, *widget2, *widget3;
	gint timer_id;
	std::string fmt1, fmt2, fmt3;
	struct tm *tm;
	sigc::connection connPoll;
	int tz_corr;

	void gen_str();
	bool timer_callback();
public:
//	void set_format1(const std::string fmt);
//	void set_format2(const std::string fmt);
//	void set_format3(const std::string fmt);
	/*! конструктор.
	    \param w1 часы.
	    \param w2 минуты.
	    \param w3 секунды.
	*/
	Clock(USVGText *w1, USVGText *w2, USVGText *w3);
	~Clock();

};

#endif /* _CLOCK_H */
