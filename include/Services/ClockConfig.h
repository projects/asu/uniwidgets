#ifndef _CLOCKCONFIG_H
#define _CLOCKCONFIG_H
// -------------------------------------------------------------------------
/*! \page page_USG Универсальный пускатель графики
	\section clockconfig Окно настройки системного времени
\par
	Класс для окна установки системного времени(описание класса ClockConfig).
*/
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <glibmm.h>
#include <libglademm.h>
#include <iostream>
#include <iomanip>
#include <sys/time.h>
// -------------------------------------------------------------------------
class FailureDialog;
/*!
 * \brief Окно настройки системного времени.
 * \par
 * Класс для окна установки системного времени.
*/
class ClockConfig
{
	public:
		/*! конструктор.
		    \param gxml glade-файл с окном настройки времени.
		    \param fw окно ошибок.
		    \param d окно меню.
		 */
		ClockConfig(Glib::RefPtr<Gnome::Glade::Xml> gxml, FailureDialog *fw, Gtk::Dialog *d);
		~ClockConfig(){};

		/*! отобразить окно */
		void show();

	private:
		Gtk::Dialog *w, *dialog;
		Gtk::SpinButton *sh, *sm, *ss, *sy, *smo, *sd;

		FailureDialog *fw;

		/* применить настройки */
		void apply();

		/* закрыть окно не применяя изменения */
		void cancel();
};

#endif /* _CLOCKCONFIG_H */
