#ifndef _FAILUREDIALOG_H
#define _FAILUREDIALOG_H
// -------------------------------------------------------------------------
/*! \page page_USG Универсальный пускатель графики
	\section failuredialog Окно вывода ошибок
\par
	Класс реализующий выполнение скриптовой команды
и в зависимости от кода возврата выдает сообщение о результате(описание класса FailureDialog).
*/
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <glibmm.h>
#include <libglademm.h>
#include <iostream>
#include <ctime>
// -------------------------------------------------------------------------
/*!
 * \brief Окно вывода ошибок.
 * \par
 * Класс реализующий выполнение скриптовой команды и в зависимости от кода возврата
 * выдает сообщение о результате.
*/
class FailureDialog
{
	        public:
                /*! конструктор.
		    \param gxml файл содержащий описание окна.
		*/
                FailureDialog(Glib::RefPtr<Gnome::Glade::Xml> gxml);

                ~FailureDialog(){};

                /*! Выполняет консольную команду и показывает окно с результатом.
		    \param command выполняемая комманда.
		    \param max_code количество возможных сообщений.
		    \param messages список возможных сообщений.
		    \param show_always отображает окно с результатом, даже если комманда выполнена без ошибок.
		    \return возвращает \c true если код возврата 0.
		 */
		bool show (const char *command, int max_code, const char *messages[], bool show_always = false);

		/*! Отображает окно с сообщением.
		    \param message текст сообщения.
		 */
		void show_simple (Glib::ustring message);

		private:
		Gtk::Dialog *failure_dialog;
		Gtk::Label *failure_message;
		/* закрыть окно */
		void hide();
};

#endif /* _FAILUREDIALOG_H */