#ifndef _JOURNALDATE_H
#define _JOURNALDATE_H
// -------------------------------------------------------------------------
/*! \page page_USG Универсальный пускатель графики
	\section journaldate Окно сохранения журнала за период
\par
	Класс для окна cохранения журнала на флэш карту.
В окне выбирается промежуток времени, за который нужно сохранить 
соответствующие записи из журнала(описание класса JournalDate).
*/
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <glibmm.h>
#include <libglademm.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/time.h>
// -------------------------------------------------------------------------
class FailureDialog;
/*!
 * \brief Окно сохранения журнала за период.
 * \par
 * Класс для окна cохранения журнала на флэш карту. В окне выбирается промежуток
 * времени, за который нужно сохранить соответствующие записи из журнала.
*/
class JournalDate
{
	public:
		/*! конструктор.
		    \param gxml glade-файл с окном выбора дат.
		    \param fw окно ошибок.
		    \param d окно меню.
		 */
		JournalDate(Glib::RefPtr<Gnome::Glade::Xml> gxml, FailureDialog *fw, Gtk::Dialog *d);
		~JournalDate(){};

		/*! Отобразить окно */
		void show();

	private:
		Gtk::Dialog *w, *dialog;
		Gtk::SpinButton *sy1, *smo1, *sd1, *sy2, *smo2, *sd2;

		FailureDialog *fw;

		/* применить выбранные даты и сохранить журнал */
		void apply();

		/* закрыть окно не применяя изменения */
		void cancel();
};

#endif /* _JOURNALDATE_H */