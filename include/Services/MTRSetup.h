#ifndef _MTRSETUP_H
#define _MTRSETUP_H
// -------------------------------------------------------------------------
/*! \page page_USG Универсальный пускатель графики
	\section mtrsetup Перепрошивка устройства MTR
\par
	Класс реализующий окно перепрошивки МТР и сам процесс прошивки(описание класса MTRSetup).
*/
// -------------------------------------------------------------------------
#include <fstream>
#include <gtkmm.h>
#include <modbus/ModbusRTUMaster.h>
#include <modbus/ModbusHelpers.h>
#include <extensions/MTR.h>
// -------------------------------------------------------------------------
/*!
 * \brief Настройка процесса обмена с устройством.
 * \par
 * Класс реализующий процесс обмена с устройством mtr.
*/
class MRMReCreator
{
public:
	MRMReCreator();
	/*! выполнить проверку подключения устройства */
	void check();

	ModbusRTUMaster* getMRM()
	{ return mb_; }
	/*! вернуть true если устройство подключено */
	bool plugged()
	{ return plugged_; }
	/*! проверить на существование объекта подключения(ModbusRTUMaster) */
	bool MRMCreated() const
	{ return mb_ != NULL; }
	/*! задать путь к устройству */
	void changeDev(const Glib::ustring dev);
	/*! задать скорость обмена с устройством */
	void setSpeed(const Glib::ustring speed)
	{ speed_ = speed; }
	/*! задать таймаут приема/посылки данных */
	void setTimeout(int timeout)
	{ timeout_ = timeout; }

private:
	Glib::ustring dev_;
	Glib::ustring speed_;
	int timeout_;

	bool plugged_;
	ModbusRTUMaster *mb_;

	bool checkPlugged();
	void reCreate();
};
/*!
 * \brief Перепрошивка устройства MTR.
 * \par
 * Класс реализующий окно перепрошивки МТР.
*/
class MTRSetup {

private:
	Gtk::Dialog *MTR_Setup, *dialog;
	Gtk::Frame *frame_settings, *frame_autodetect, *frame_prog;
	Gtk::VButtonBox *box_actions;
	Gtk::ComboBoxEntry *combo_dev, *combo_speed, *combo_fn;
	Gtk::SpinButton *spin_addr, *spin_timeout, *spin_beg, *spin_end, *spin_reg;
	Gtk::ProgressBar *autodetect_progress;
	Gtk::Button *mtr_abort, *toggle_settings, *start_autodetect;
	Gtk::Label *label_state, *label_beg, *label_end, *ok_mess, *fail_mess;
	Gtk::CheckButton *radio_speed, *radio_guessspeed;
	Gtk::Dialog *detection_failed;

	Glib::ustring current_device, path;

	void hide();
	bool check(bool unknown_state);
	void start_check();
	void stop_check();
	void toggle_settings_cb();
//	void speed_toggled();
//	void guessspeed_toggled();
	void begin_changed();
	void dev_changed();
	void speed_changed();
	void timeout_changed();
	void autodetect_start();
	void prog_clicked(Glib::ustring filename);
	void abortation();
	void set_normal_state(bool state);
//	bool update_p_cb(unsigned int addr, unsigned int beg_addr, unsigned int end_addr, unsigned int reg, ModbusRTU::SlaveFunctionCode fn, bool guessspeed, bool speed);

	MRMReCreator mrmc;
//	ModbusRTUMaster *mb;
	sigc::connection update_progress, check_dev;

	std::list<std::string> speedlist;
	bool stop_button_pressed;
	bool port_ok;
	bool settings_visible;
	bool detected;
//	unsigned int iii, ad;

public:
	/*! конструктор.
		    \param gxml glade-файл с описанием окна.
		    \param d путь к директории с glade-файлами.
		    \param mtr_prog_path путь к директории с прошивками для устройств mtr.
	*/
	MTRSetup(Glib::RefPtr<Gnome::Glade::Xml> gxml, Gtk::Dialog *d, Glib::ustring mtr_prog_path);
	~MTRSetup(){};

	void show();
};

#endif /* _MTRSETUP_H */
