#ifndef _MAINWINDOW_H
#define _MAINWINDOW_H
// -------------------------------------------------------------------------
/*! \page page_USG Универсальный пускатель графики
	\section mainwindow Главное окно графики.
\par
	Основная связующая часть УПГ. Здесь все элементы собираются в один
главный экран и производятся все инициализации и дополнительные привязки для
виджетов.
*/
// -------------------------------------------------------------------------
#include <usvgwidgets/USVGImage.h>
#include <uwidgets/ULockNotebook.h>
#include <uwidgets/UJournal.h>
#include <uwidgets/UProxyWidget.h>
#include <ctime>
#include <uwidgets/UPrinterInterface.h>
#include <typical/IndicatorTwoState.h>
#include <typical/IndicatorFourState.h>
#include <typical/GDG.h>
#include <typical/VDG.h>
#include <objects/SimpleObject.h>
#include <uniwidgets/UPostcreate.h>
// #include "YauzaConfiguration.h"
// #include "YauzaTypes.h"
// #include "YauzaMessages.h"
#include <webkit/webkit.h>
#include <iomanip>
#include <UniXML.h>
#include <gtkmm/comboboxentrytext.h>
#include <glibmm/regex.h>
// -------------------------------------------------------------------------
class MTRSetup;
class JournalDate;
class ClockConfig;
class FailureDialog;
class Clock;
/*!
 * \brief Главное окно графики.
 * \par
 * Основная связующая часть УПГ. Здесь все элементы собираются в один
 * главный экран и производятся все инициализации и дополнительные привязки для
 * виджетов(описание класса MainWindow).
*/
class MainWindow
{
	public:
		MainWindow(string gladedir, string guifile, string svgdir, string mtr_prog_path, string weblog_addr, std::list<std::string> screens);
		~MainWindow() {delete w;};
		/*! главное окно */
		Gtk::Window *w;

	protected:
		/*! инициализация вкладок.
		    \param n виджет окно с вкладками.
		    \param _gladedir путь к директории с glade-файлами.
		    \param _svgdir путь к директории с svg-файлами.
		    \param _weblog_addr url-адрес web-журнала.
		    \param _bar индикатор прогресса.
		    \param _screens список названий вкладок.
		*/
		virtual void init_tabs(Gtk::Notebook *n, string &_gladedir, string &_svgdir, string &_weblog_addr, Gtk::ProgressBar &_bar, std::list<std::string> &_screens);

	private:
		//Gtk::Menu menu;
		Gtk::Notebook *jb;
		bool full_state, is_pressed;

		/* использовалось при отладке */
		bool on_key_event(GdkEventKey *e);
		/* использовалось при отладке */
		bool pressed(GdkEventButton *eb);
		/* использовалось при отладке */
		bool released(GdkEventButton *eb);
		/* показать меню */
		void show_conf_menu();
		/* скрыть меню */
		void hide_conf_menu();
		/* перезагрузить систему */
		void reboot();
		/* выключить систему */
		void poweroff();
		/* обработка переключений закладок для NoteBook содержащего Journal и APSJournal(Нужно для того чтобы убрать первою вкладку) */
		void page_switched(GtkNotebookPage *pg, guint cur_pg);
		/* сделать скриншот */
		void make_shot();
		/* сохранить на флеш сделанные скриншоты */
		void save_to_flash();
		/* Вкл./Откл. блокировку автоматического переключения вкладок */
		void switch_lock();

		guint page;
		Clock *clock;
		ClockConfig *cc;
		JournalDate *jd;
		FailureDialog *fw;
		MTRSetup *ms;
		Gtk::Dialog *dialog;
		Gtk::Label *failure_message;
		Gtk::CheckButton* check_button;
		ULockNotebook* lock_notebook;
};

#endif /* _MAINWINDOW_H */
