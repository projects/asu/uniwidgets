#ifndef _MISCELLANOUS_H
#define _MISCELLANOUS_H
// -------------------------------------------------------------------------
/*! \page page_USG Универсальный пускатель графики
	\section miscellaneous Различные вспомогательные функции и переменные
\par
	Файл с различными дополнительными общими функциями и переменными.
*/
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <glibmm.h>
#include <libglademm.h>
#include <glade/glade.h>
#include <iostream>
#include <iomanip>
#include <UniXML.h>
#include <Configuration.h>
#include <uwidgets/UPrinterInterface.h>
#include <typical/IndicatorTwoState.h>
#include <typical/IndicatorFourState.h>
#include <typical/GDG.h>
#include <typical/VDG.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace UniSetTypes;
using namespace UniWidgets;
// -------------------------------------------------------------------------
void add_frame(Gtk::Dialog *w);
void unset_focus(Gtk::Widget *w);

Glib::ustring make_string( std::vector<UniSetTypes::ObjectId> &ids );
void on_print_indicator(IndicatorFourState* indicator);
void on_print_indicator(IndicatorTwoState* indicator);
void on_print_indicator(GDG* gdg);
void on_print_indicator(VDG* vdg);
void switch_to(GtkWidget *w, gpointer user_data);

template<typename W> void make_object_menu(W* w);
template<typename W> bool on_print_press_event(GdkEventButton* event, W* w);
template<typename W> void widget_connect(W* widget);

#endif /* _MISCELLANOUS_H */
