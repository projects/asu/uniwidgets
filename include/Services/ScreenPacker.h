#ifndef _SCREENPACKER_H
#define _SCREENPACKER_H
// -------------------------------------------------------------------------
/*! \page page_USG Универсальный пускатель графики
	\section screenpacker Заполнение вкладок ULockNotebook
\par
	Класс реализующий заполненине вкладок ULockNotebook. А также назначение
обработчиков сигналов для отдельных групп вижетов(описание класса ScreenPacker).
*/
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <glibmm.h>
#include <libglademm.h>
#include <glade/glade.h>
#include <iostream>
#include <iomanip>
#include <UniXML.h>
// #include "YauzaConfiguration.h"
// #include "YauzaTypes.h"
// #include "YauzaMessages.h"
#include <uwidgets/UProxyWidget.h>
#include <typical/IndicatorFourState.h>
#include <typical/IndicatorTwoState.h>
#include <typical/GDG.h>
#include <typical/VDG.h>
#include <uwidgets/UPrinterInterface.h>
#include <usvgwidgets/USVGImage.h>
#include <usvgwidgets/USVGText.h>
// -------------------------------------------------------------------------
/*!
 * \brief Заполнение вкладок ULockNotebook.
 * \par
 * Класс реализующий заполненине вкладок ULockNotebook. А также назначение
 * обработчиков сигналов для отдельных групп вижетов.
*/
class ScreenPacker
{
#warning ScreenPacker - very unsafe class!
public:
	/*! конструктор.
		    \param n виджет окно с вкладками.
		    \param path путь к директории с glade-файлами.
		    \param svgdir путь к директории с svg-файлами.
	*/
	ScreenPacker( Gtk::Notebook* n, const Glib::ustring& path, const Glib::ustring& svgdir);
	/*! добавить отдельный виджет на вкладку */
	void append_widget(Gtk::Widget *w, const Glib::ustring& tabname);
	/*! добавить контейнер с виджетами на вкладку */
	virtual void append(const Glib::ustring& tabname);
	/*! добавить Notebook на вкладку */
	virtual void append_notebook(const Glib::ustring& tabname);
private:
	Gtk::Notebook* nbook;
	std::string gladedir;
	std::string svgdir_;
	int scrnum;
	std::vector<Gtk::TargetEntry> listTargets;
};

#endif /* _SCREENPACKER_H */