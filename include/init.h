#ifndef _UNIWIDGETS_INIT_H
#define _UNIWIDGETS_INIT_H

namespace UniWidgets
{

void init(int argc, char** argv);

}

#endif
