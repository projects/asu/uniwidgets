#ifndef _PORTS_H
#define _PORTS_H

#include <glibmm.h>
#include <ctime>

/* sprintf for Glib::ustring. Use Glib::ustring::compose if possible. */
size_t usprintf(Glib::ustring &ustr, const Glib::ustring fmt, ...);

void get_gmtime_str(Glib::ustring& dtimeStr, Glib::ustring& ttimeStr, time_t sec = time(NULL));
void get_time_str(Glib::ustring& dtimeStr, Glib::ustring& ttimeStr, time_t sec = time(NULL));

#endif
