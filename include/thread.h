#ifndef _THREAD_H
#define _THREAD_H

#include <sigc++/sigc++.h>
#include <glib.h>
#include <gdk/gdk.h>

// -------------------------------------------------------------------------
class GtkThreadBase
{
	public:
		virtual ~GtkThreadBase()
		{
			g_mutex_clear(&mutex);
			g_cond_clear(&cond);
		}
		
	protected:
		GtkThreadBase()
		{
			g_cond_init(&cond);
			g_mutex_init(&mutex);
			g_mutex_lock(&mutex);
		}
		void ready()
		{
			g_mutex_lock(&mutex);
			g_cond_signal(&cond);
			g_mutex_unlock(&mutex);
		}
		void wait()
		{
			g_cond_wait(&cond, &mutex);
			g_mutex_unlock(&mutex);
		}
		
	private:
		GCond cond;
		GMutex mutex;
};

#define ARG(num) T_arg ## num* arg ## num
#define CONST_ARG(num) const T_arg ## num& _arg ## num
#define CONST_CAST_ARG(num) data.arg ## num = const_cast<T_arg ## num*>(&_arg ## num)

template<class T_arg1>
class GtkThread1 : public GtkThreadBase
{
	public:
		typedef GtkThread1<T_arg1> ClassType;
		typedef sigc::slot<void, T_arg1> SlotType;
		virtual ~GtkThread1() {}
		static void execute(const SlotType& _slot, CONST_ARG(1))
		{
			ClassType data;
			data.slot = _slot;
			CONST_CAST_ARG(1);
			gdk_threads_add_idle(ClassType::gtk_thread_execute, &data);
			data.wait();
		}
		
	protected:
		GtkThread1() {}
		static gboolean gtk_thread_execute(gpointer user_data)
		{
			ClassType* data = reinterpret_cast<ClassType*>(user_data);
			if(data)
			{
				data->slot(*data->arg1);
				data->ready();
			}
			return G_SOURCE_REMOVE;
		}
		
	private:
		SlotType slot;
		ARG(1);
};

template<class T_arg1, class T_arg2>
class GtkThread2 : public GtkThreadBase
{
	public:
		typedef GtkThread2<T_arg1, T_arg2> ClassType;
		typedef sigc::slot<void, T_arg1, T_arg2> SlotType;
		virtual ~GtkThread2() {}
		static void execute(const SlotType& _slot, CONST_ARG(1), CONST_ARG(2))
		{
			ClassType data;
			data.slot = _slot;
			CONST_CAST_ARG(1);
			CONST_CAST_ARG(2);
			gdk_threads_add_idle(ClassType::gtk_thread_execute, &data);
			data.wait();
		}
		
	protected:
		GtkThread2() {}
		static gboolean gtk_thread_execute(gpointer user_data)
		{
			ClassType* data = reinterpret_cast<ClassType*>(user_data);
			if(data)
			{
				data->slot(*data->arg1, *data->arg2);
				data->ready();
			}
			return G_SOURCE_REMOVE;
		}
		
	private:
		SlotType slot;
		ARG(1);
		ARG(2);
};

template<class T_arg1, class T_arg2, class T_arg3>
class GtkThread3 : public GtkThreadBase
{
	public:
		typedef GtkThread3<T_arg1, T_arg2, T_arg3> ClassType;
		typedef sigc::slot<void, T_arg1, T_arg2, T_arg3> SlotType;
		virtual ~GtkThread3() {}
		static void execute(const SlotType& _slot, CONST_ARG(1), CONST_ARG(2), CONST_ARG(3))
		{
			ClassType data;
			data.slot = _slot;
			CONST_CAST_ARG(1);
			CONST_CAST_ARG(2);
			CONST_CAST_ARG(3);
			gdk_threads_add_idle(ClassType::gtk_thread_execute, &data);
			data.wait();
		}
		
	protected:
		GtkThread3() {}
		static gboolean gtk_thread_execute(gpointer user_data)
		{
			ClassType* data = reinterpret_cast<ClassType*>(user_data);
			if(data)
			{
				data->slot(*data->arg1, *data->arg2, *data->arg3);
				data->ready();
			}
			return G_SOURCE_REMOVE;
		}
		
	private:
		SlotType slot;
		ARG(1);
		ARG(2);
		ARG(3);
};
#endif
