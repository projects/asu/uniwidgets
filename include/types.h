#ifndef _TYPES_H_
#define _TYPES_H_
// -------------------------------------------------------------------------
#include <cstring>
#include <string>
#include <vector>
#include <glibmm.h>
#include <gtkmm/enums.h>
#include <assert.h>
#include <libxml/tree.h>
// -------------------------------------------------------------------------
/*! \namespace UniWidgetsTypes
 *  \brief Пространство имен содержащее различные константы.
*/
namespace UniWidgetsTypes
{
	enum ChildrenTypes
	{
		typeFirst = 0,
		typeLogic = 0,
		typeView = 1,// Тип для картинок или текста по-умолчанию
		typeIndicator = 2,
		typeCistern = 3,
		typeText = 4,// Тип для текста при использовании картинок и текста вместе.Например, смотри StateMultiLogic и TypicalGDGControl. Текст там используется
				    // как дополнительное числовое обозначение состояния виджета к картинке.
		typeButtom = 5,
		/* typeObject must have the maximum type number ! */
		typeObject = 10,
		typeLast
	};

	enum DiscreteValues
	{
		discreteOff = 0,
		discreteOn = 1
	};
	/*! Возможные режимы работы объекта
	*  (общее для дизеля, генератора, автомата, СЭС)
	*/
	enum ObjectMode
	{
		mOFF			= 0,	/*!< режим отключён */
		mON			 = 1,	/*!< режим включён */
		mPROTECTION	 = 2,		/*!< режим защиты */
		mINIT			 = 3,		/*!< режим инициализации */
		mTRANSITIVE	 = 4,		/*!< переходный процесс */
		mSLEEP			= 5,		/*!< режим выключенное управление */
		mWARNING		= 6,	/*!< неполадка */
		mALARM			= 7,		/*!< авария */
		mREADY			= 8,		/*!< готов к пуску */
		mRunning		= 9,	/*!< запущен */
		mUNKNOWN		= 10,	/*!< неопределнное состояние */
		minMode = mOFF,
		maxMode = 18 			/*!< Самое большое значение ,используемое для состояния(искусственное) */
	};

	enum TypicalStateMode
	{
		mBACKGROUND = -1,
		mRESERV = 15
	};

	enum TypicalFourStateMode
	{
		mWARNING_HIGH = 11
		,mWARNING_LOW = 12
		,mALARM_HIGH = 13
		,mALARM_LOW = 14
	};

	//! Возможные режимы управления
	enum TypicalGDGControlMode
	{
		mINDEF = 0,		/*!< неопределённое значение */
		mHAND = 1,		/*!< ручное управление */
		mCPU = 2,		/*!< управление с ЦПУ */
		mAUTO = 3		/*!< автоматическое управление*/
	};

	enum MessageType
	{
		msgINFO = 0,
		msgWARNING = 1,
		msgALARM = 2,
		msgATTENTION = 3
	};

	/*! Values of the link sensor */
	enum LinkValues
	{
		linkOn = 0,		/*!< device is connected */
		linkOff = 1		/*!< device is disconnected */
	};

	enum UWState
	{
		uwsUnknown = 10, 	/*!< неопределённое */
		uwsOFF = 0,  		/*!< отображает выключенное состояние (т.е. сигнал не сработал) */
		uwsON = 1,   		/*!< отображает включенное состояние (т.е. сигнал сработал) */
		uwsWARNING = 6, 	/*!< сработал предупредительный сигнал */
		uwsALARM = 7,   	/*!< сработал аварийный сигнал */
		uwsWaitConfirm = 11 	/*!< ожидает квитирования */
	};

	enum RTV_mode
	{
		Workmain  = 1,		/*!< Работает основной РТВ */
		Workcommon  = 3		/*!< Работает резервный РТВ */
	};

	enum RTVerror
	{
		rtvSWITCH  = 1,		/*!< сигнал переключения РТВ */
		rtvALARM  = 2		/*!< переключение РТВ не удалось */
	};

	enum ThresholdType
	{
		mWARNING_type=0,
		mALARM_type
	};

	enum ArrowDeviceType
	{
		AD_type=0,
		EAD_type,
		CAD_type
	};
  
	enum LampCommand
	{
		lmpOFF      = 0,    /*!< выключить */
		lmpON       = 1,    /*!< включить */
		lmpBLINK    = 2,    /*!< мигать */
		lmpBLINK2   = 3,    /*!< мигать */
		lmpBLINK3   = 4     /*!< мигать */
	};

	class Exception:
		public std::exception
	{
		public:

			Exception(const std::string& txt): text(txt.c_str()) {}
			Exception(): text("Exception") {}
			virtual ~Exception() noexcept(true) {}

			friend std::ostream& operator<<(std::ostream& os, const Exception& ex )
			{
				os << ex.text;
				return os;
			}

			virtual const char* what() const noexcept override
			{
				return text.c_str();
			}

		protected:
			const std::string text;
	};

	static int _argc = { 0 };
	static const char* const* _argv;
	
	typedef long ObjectId;
	const ObjectId DefaultObjectId = -1;
	
	struct ObjectInfo
	{
		long min;      /*!< минимальное калиброванное значение */
		long max;      /*!< максимальное калиброванное значение */
		short precision;  /*!< точность */
	};
	
	bool file_exist( const std::string& filename );
	void uniwidgets_init(int argc, const char* const* argv);
	
	xmlNode* xmlFindNode( xmlNode* node, const std::string& searchnode, const std::string& name = "" );
	xmlNode* xmlExtFindNode( xmlNode* node, int depth, int width, const std::string& searchnode, const std::string& name = "", bool top = true );
	std::string xmlGetProp(const xmlNode* node, const std::string& name);
	std::string getArgParam( const std::string& name, const std::string& defval = "" );
}
// -------------------------------------------------------------------------
#endif // Types_H_
// -------------------------------------------------------------------------
