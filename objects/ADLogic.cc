#include <types.h>
#include <components/AD.h>
#include "ADLogic.h"
#include "SimpleObject.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define ADLOGIC_VALUE_AI		"value-ai"
#define ADLOGIC_NODE			"node"
// -------------------------------------------------------------------------
#define INIT_ADLOGIC_PROPERTIES() \
	value_ai(*this, ADLOGIC_VALUE_AI, UniWidgetsTypes::DefaultObjectId)\
	,node(*this, ADLOGIC_NODE , UniWidgetsTypes::DefaultObjectId)
// -------------------------------------------------------------------------
void ADLogic::constructor()
{

	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback_object<ADLogic>;

	connect_property_changed(ADLOGIC_VALUE_AI, sigc::mem_fun(*this, &ADLogic::set_sensor_handler));
	connect_property_changed(ADLOGIC_NODE, sigc::mem_fun(*this, &ADLogic::set_sensor_handler));
}
// -------------------------------------------------------------------------
ADLogic::ADLogic() :
	Glib::ObjectBase("adlogic")
	,INIT_ADLOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
ADLogic::ADLogic(Gtk::EventBox::BaseObjectType* gobject) :
		AbstractLogic(gobject)
		,INIT_ADLOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
ADLogic::~ADLogic()
{
}
// -------------------------------------------------------------------------
void ADLogic::connect()
{
	set_current_value();
}
// -------------------------------------------------------------------------
void ADLogic::set_current_value()
{
	assert(object_ != NULL);

// 	if(!is_initialized())
// 		return;
	if( get_value_ai() == UniWidgetsTypes::DefaultObjectId )
		return;

	float value = object_->get_analog_value(get_value_ai(), get_node());

	if(current_ad != NULL)
		current_ad->setValue(value);

}
// -------------------------------------------------------------------------
void ADLogic::on_init()
{
	assert(object_ != NULL);
	init_ad();
	set_sensor_handler();
}
// -------------------------------------------------------------------------
void ADLogic::init_ad()
{
	/*Virtual,rename*/
	assert(object_ != NULL);

	std::vector<Gtk::Widget*> widgets(
			object_->get_children(typeIndicator));
	if( widgets.empty() )
		return;

	AD* ad = dynamic_cast<AD*>(widgets[0]);
	
	if ( ad !=NULL )
	{
		current_ad = ad;
	}else
		cerr << get_name() << ": widget doesn't load" << endl;
}
// -------------------------------------------------------------------------
void ADLogic::set_sensor_handler()
{
	assert(object_ != NULL);
	object_->set_analog_sensor_handler(
			sigc::mem_fun(this, &ADLogic::sensor_handler),
			get_value_ai(),
			get_node());
	set_current_value();
}
// -------------------------------------------------------------------------
void ADLogic::sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, float value)
{
	set_value(value);
}
// -------------------------------------------------------------------------
void ADLogic::set_value(const double value)
{
	assert(current_ad != NULL);
	current_ad->setValue(value);
}
// -------------------------------------------------------------------------
