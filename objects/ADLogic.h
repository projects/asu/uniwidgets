#ifndef _ADLOGIC_H
#define _ADLOGIC_H
// -------------------------------------------------------------------------
#include "types.h"
#include <objects/AbstractLogic.h>
#include <global_macros.h>
#include <USignals.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class AD;
/*!
 * \brief Логика аналогового индикатора.
 * \par
 * Логика для выставления значения на аналоговом индикаторе.
 * Логике через свойства передается id датчика, изменение значения которого 
 * отслеживается и выводится на экран через соответствующий компонент аналогового индикатора.
*/
class ADLogic : public AbstractLogic
{
public:
	ADLogic();
	explicit ADLogic(Gtk::EventBox::BaseObjectType* gobject);
	virtual ~ADLogic();

	virtual void connect();

protected:
	/* Methods */
	virtual void on_init();		/*!< инициализация логики */

private:
	/* Variables */
	AD *current_ad;
	/* Methods */
	void constructor();
	virtual void init_ad();
	virtual void set_current_value();
	/*! выставление заданного уровня в цистерне */
	void set_value(const double value);
	/*! привязка сигнала к обработчику изменения значения датчика */
	virtual void set_sensor_handler();
	/*! обработчик изменения значения датчика */
	virtual void sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, float value);

	DISALLOW_COPY_AND_ASSIGN(ADLogic);
	ADD_PROPERTY( value_ai, UniWidgetsTypes::ObjectId )
	ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )
};

}
#endif
