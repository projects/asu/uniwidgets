#include <types.h>
#include "AbstractLogic.h"
// -------------------------------------------------------------------------
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace std;
// -------------------------------------------------------------------------
AbstractLogic::AbstractLogic() :
	Glib::ObjectBase("abstractlogic")
{
}
// -------------------------------------------------------------------------
AbstractLogic::AbstractLogic(Gtk::EventBox::BaseObjectType* gobject) :
	Gtk::EventBox(gobject)
{
}
// -------------------------------------------------------------------------
AbstractLogic::~AbstractLogic()
{
}
// -------------------------------------------------------------------------
void AbstractLogic::init(SimpleObject* object)
{
	if (object == NULL)
		return;

	set_object(object);
	on_init();
}
// -------------------------------------------------------------------------
void AbstractLogic::set_object(SimpleObject* object)
{
	object_ = object;
}
// -------------------------------------------------------------------------
bool AbstractLogic::on_handle_error_message( const long value )
{
	/*Обработка ошибочных значений и вывод в
	логи, отображение на виджете неопределенного состояния*/
	if(value <= maxMode && value >= minMode)
	{
		/*Проверку прошла*/
		return true;
	}
	else
	{
		/*Не прошла проверку, вызов обработчика ошибок*/
		handle_error(value);
		return false;
	}
}
// -------------------------------------------------------------------------
void AbstractLogic::handle_error(const long value)
{
	cerr<<get_name()<<" :"<<value<<" value isn't valid "<<endl;
}
// -------------------------------------------------------------------------
