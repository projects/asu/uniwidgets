#ifndef _ABSTRACTLOGIC_H
#define _ABSTRACTLOGIC_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <global_macros.h>
#include <objects/SimpleObject.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
// class SimpleObject;
/*!
 * \brief Базовый класс логики.
 * \par
 * Базовый класс для всех объектов логик. Здесь реализована связь с контейнером SimpleObject.
 * Контейнер содержит информацию об содержащихся в нем компонентах(картинках, тексте и т.д.),
 * других логиках и позволяет получить ссылки на эти объекты. С помощью этих ссылок логика может
 * управлять различными компонентами т.е. внешним видом виджета. Основная задача логики - это
 * взаимодействие с датчиками, их опрос или слежение за изменением состояния и выставление.
*/
class AbstractLogic : public Gtk::EventBox
{
public:
	AbstractLogic();
	explicit AbstractLogic(Gtk::EventBox::BaseObjectType* gobject);
	virtual ~AbstractLogic();

	/* Methods */
	void init(SimpleObject* object);				/*!< инициализация логики, получение ссылки на контейнер SimpleObject */
	virtual void connect() {}					/*!< обработчик события присоединения к процессу SharedMemory */

protected:
	/* Variables */
	SimpleObject* object_;						/*!< ссылка на контейнер SimpleObject */

	/* Methods */
	virtual void set_object(SimpleObject* object);			/*!< установить ссылку на контейнер SimpleObject */
	virtual void on_init() = 0;					/*!< метод вызывается при инициализации в init и для каждой логики должна быть свой реализация */
	virtual bool on_handle_error_message(const long value);		/*!< обработчик значения сигнала от датчика */
	virtual void handle_error(const long value);			/*!< обработчик ошибочных значений от датчика */

private:
	DISALLOW_COPY_AND_ASSIGN(AbstractLogic);
};

}
#endif
