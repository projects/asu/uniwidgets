
#include <components/CisternImage.h>
#include <types.h>
#include <ConfirmSignal.h>
#include "CisternLogic.h"
#include "SimpleObject.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define CISTERNLOGIC_VALUE_AI			"value-ai"
#define CISTERNLOGIC_NODE			"node"
// -------------------------------------------------------------------------
#define INIT_CISTERNLOGIC_PROPERTIES() \
	value_ai(*this, CISTERNLOGIC_VALUE_AI, UniWidgetsTypes::DefaultObjectId)\
	,node(*this, CISTERNLOGIC_NODE , UniWidgetsTypes::DefaultObjectId)
// -------------------------------------------------------------------------
void CisternLogic::constructor()
{

	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback_object<CisternLogic>;

	current_cistern_ = NULL;
}
// -------------------------------------------------------------------------
CisternLogic::CisternLogic() :
	Glib::ObjectBase("cisternlogic")
	,INIT_CISTERNLOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
CisternLogic::CisternLogic(Gtk::EventBox::BaseObjectType* gobject) :
	AbstractLogic(gobject)
	,INIT_CISTERNLOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
CisternLogic::~CisternLogic()
{
}
// -------------------------------------------------------------------------
void CisternLogic::connect()
{
	// Set Initial State
	set_current_value();
}
// -------------------------------------------------------------------------
void CisternLogic::on_init()
{
	init_value();
	set_value(0);
	set_sensor_handler();
}
// -------------------------------------------------------------------------
void CisternLogic::set_current_value()
{
	assert(object_ != NULL);
	/*Если датчик установлен по-умолчанию UniWidgetsTypes::DefaultObjectId
	,то выходим без проверки его состояния*/
	if(get_value_ai() == UniWidgetsTypes::DefaultObjectId)
		return;

	long value;
	value = object_->get_value_from_sm(get_value_ai(),get_node());
	//Set value indicator
	set_value(value);
}
// -------------------------------------------------------------------------
void CisternLogic::init_value()
{
	assert(object_ != NULL);
	std::vector<Gtk::Widget*> widgets(
			object_->get_children(typeCistern));

	CisternImage* cistern = dynamic_cast<CisternImage*>(widgets[0]);
	
	if ( cistern !=NULL )
	{
		current_cistern_=cistern;
	}else
	cerr << get_name() << ": widget doesn't load" << endl;
}
// -------------------------------------------------------------------------
void CisternLogic::set_sensor_handler()
{
	assert(object_ != NULL);

	object_->set_analog_sensor_handler(
			sigc::mem_fun(this, &CisternLogic::sensor_handler),
			get_value_ai(),
			get_node());
}
// -------------------------------------------------------------------------
void CisternLogic::sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value)
{
	set_value(value);
}
// -------------------------------------------------------------------------
void CisternLogic::set_value(const long value)
{
	assert(current_cistern_ != NULL);
	current_cistern_->set_value(value);
}
// -------------------------------------------------------------------------
