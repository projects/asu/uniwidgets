#ifndef _CISTERNLOGIC_H
#define _CISTERNLOGIC_H
// -------------------------------------------------------------------------
#include "types.h"
#include <objects/AbstractLogic.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class CisternImage;
/*!
 * \brief Логика для цистерны.
 * \par
 * Логика для выставления уровня в компоненте цистерна в зависимости от значения
 * отслеживаемого датчика.
*/
class CisternLogic : public AbstractLogic
{
public:
	CisternLogic();
	explicit CisternLogic(Gtk::EventBox::BaseObjectType* gobject);
	virtual ~CisternLogic();

	virtual void connect();
protected:
	/* Methods */
	virtual void on_init();						/*!< инициализация логики */

private:
	/* Variables */
	CisternImage* current_cistern_;					/*!< ссылку на компонент цистерна */

	/* Methods */
	void constructor();
	void init_value();						/*!< инициализация ссылки на компонент цистерна путем запроса у контейнера виджета типа typeCistern */
	void set_value(const long value);				/*!< выставление заданного уровня в цистерне */
	void set_current_value();					/*!< выставление текущего уровня в цистерне */

	/* Handlers */
	void sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value);
	void set_sensor_handler();

	DISALLOW_COPY_AND_ASSIGN(CisternLogic);
	/* Properties */
	ADD_PROPERTY( value_ai, UniWidgetsTypes::ObjectId )
	ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )
};

}
#endif
