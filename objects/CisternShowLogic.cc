#include <iomanip>
#include <types.h>
#include "CisternShowLogic.h"
#include "SimpleObject.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
using Glib::ustring;
// -------------------------------------------------------------------------
void CisternShowLogic::constructor()
{

	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<CisternShowLogic>;

	current_cistern_ = NULL;
}
// -------------------------------------------------------------------------
CisternShowLogic::CisternShowLogic() :
	Glib::ObjectBase("cisternshowlogic")
{
	constructor();
}
// -------------------------------------------------------------------------
CisternShowLogic::CisternShowLogic(Gtk::EventBox::BaseObjectType* gobject) :
	QueueLogic(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
void CisternShowLogic::on_init()
{
	assert(object_ != NULL);

	std::vector<Gtk::Widget*> widgets(
			object_->get_children(typeCistern));

	if( widgets.empty() )
		return;
	CisternImageBlink *cistern = dynamic_cast<CisternImageBlink*>(widgets[0]);
	if(cistern != NULL)
	{
		current_cistern_ = cistern;
		/*Значение по-умолчанию*/
		set_mode(mOFF);
	}
}
// -------------------------------------------------------------------------
void CisternShowLogic::set_mode(long mode)
{
	current_cistern_->on_set_state(mode);
}
// -------------------------------------------------------------------------
void CisternShowLogic::on_set_state_mode(const long mode, const int type,bool blink)
{
	on_set_mode(mode,type,blink);
}
// -------------------------------------------------------------------------
void CisternShowLogic::set_state(bool state)
{
  current_cistern_->set_property_state_(state);
}
// -------------------------------------------------------------------------
void CisternShowLogic::off_set_state_mode(const long mode, const int type)
{
	off_set_mode(mode,type);
}
// -------------------------------------------------------------------------
void CisternShowLogic::stop_blink()
{
	current_cistern_->stop_blink();
}
// -------------------------------------------------------------------------
bool CisternShowLogic::is_blinking()
{
	return current_cistern_ -> is_blinking();
}
// -------------------------------------------------------------------------
void CisternShowLogic::start_blink()
{
	current_cistern_->start_blink();
}
// -------------------------------------------------------------------------
long CisternShowLogic::get_state_obj()
{
	if(current_cistern_ == NULL)
		return uwsUnknown;

	if(current_cistern_ -> is_blinking())
		return uwsWaitConfirm;
	else
	{
		long mode = current_mode_;
		if( mode == mWARNING_HIGH or mode == mWARNING_LOW )
		{
			/*Значение соответствует порогам
			индикатора для одинакового состояния,
			но для разных типов порогов(верхний и нижний)*/
			return uwsWARNING;
		}
		else if( mode == mALARM_HIGH or mode == mALARM_LOW )
		{
			return uwsALARM;
		}
		else if( mode == mWARNING )
		{
			return uwsWARNING;
		}
		else if( mode == mALARM )
		{
			return uwsALARM;
		}
		else if( mode != mOFF)
		{
			/*Любые значения отличные от "0" и от выше перечисленных
			  воспринимаются как "включенное состояние"*/
			return uwsON;
		}
		else
		{
			/*В конце проверок остается только состояние соответствующее "0"*/
			return uwsOFF;
		}
	}
}
// -------------------------------------------------------------------------
