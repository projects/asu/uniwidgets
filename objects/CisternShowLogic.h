#ifndef _CISTERNSHOWLOGIC_H
#define _CISTERNSHOWLOGIC_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <gdkmm.h>
#include <objects/QueueLogic.h>
#include <SBlinker.h>
#include <USignals.h>
#include <ConfirmSignal.h>
#include <components/CisternImageBlink.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class CisternImageBlink;
/*!
 * \brief Логика для цистерны.
 * \par
 * Логика для выставления порогового уровня в компоненте цистерна в зависимости от значения
 * отслеживаемого датчика. Если для цистерны существуют АПС сигналы уровней
 * (верхних и/или нижних) цистерна может мигать и менять цвет при наличии сработавшего
 * порога.
*/
class CisternShowLogic : public QueueLogic
{
public:
  CisternShowLogic();
  explicit CisternShowLogic(Gtk::EventBox::BaseObjectType* gobject);
  ~CisternShowLogic() {}

	/*! функция выставляет новый режим в зависимости от его типа и приоритета.
	    Самый высокий приоритет обрабатывается в первую очередь, если приоритет одинаковый -
	    то обработка по порядку поступления.
	*/
	void on_set_state_mode(const long mode, const int type,bool blink=true);
	void off_set_state_mode(const long mode, const int type);			/*!< снятия режима */
	virtual void set_mode(long mode);						/*!< выставить режим для цистерны */
	virtual void set_state(bool state);						/*!< выставить состояние для цистерны (включен/выключен режим сработавшей АПС) */
	virtual void stop_blink();							/*!< выключить мигание цистерной */
	virtual void start_blink();							/*!< включить мигание цистерной */
	virtual bool is_blinking();							/*!< проверка цистерны на мигание */
	virtual long get_state_obj();							/*!< получить текущее состояние виджета */
protected:
	/* Methods */
	virtual void on_init();								/*!< инициализация логики */
private:
	/* Variables */
	CisternImageBlink *current_cistern_;

  DISALLOW_COPY_AND_ASSIGN(CisternShowLogic);
	/* Methods */
	void constructor();
};

}
#endif
