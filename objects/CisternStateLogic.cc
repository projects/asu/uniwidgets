#include <types.h>
#include <ConfirmSignal.h>
#include "CisternStateLogic.h"
#include "SimpleObject.h"
#include "CisternShowLogic.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define CISTERNSTATELOGIC_STATE_DI		"state-di"
#define CISTERNSTATELOGIC_STATE_OBJ_AI		"state-obj-ai"
#define CISTERNSTATELOGIC_MODE			"mode"
#define CISTERNSTATELOGIC_THRESHOLD		"threshold"
#define CISTERNSTATELOGIC_NODE			"node"
// -------------------------------------------------------------------------
#define INIT_CISTERNSTATELOGIC_PROPERTIES() \
	state_di(*this, CISTERNSTATELOGIC_STATE_DI, UniWidgetsTypes::DefaultObjectId)\
	,state_obj_ai(*this, CISTERNSTATELOGIC_STATE_OBJ_AI, 0)\
	,mode(*this, CISTERNSTATELOGIC_MODE, -1)\
	,threshold(*this, CISTERNSTATELOGIC_THRESHOLD, 0)\
	,node(*this, CISTERNSTATELOGIC_NODE , UniWidgetsTypes::DefaultObjectId)
// -------------------------------------------------------------------------
void CisternStateLogic::constructor()
{

	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback_object<CisternStateLogic>;

	current_cistern_ = NULL;
	current_value_=0;
}
// -------------------------------------------------------------------------
CisternStateLogic::CisternStateLogic() :
	Glib::ObjectBase("cisternstatelogic")
	,INIT_CISTERNSTATELOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
CisternStateLogic::CisternStateLogic(Gtk::EventBox::BaseObjectType* gobject) :
	AbstractLogic(gobject)
	,INIT_CISTERNSTATELOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
CisternStateLogic::~CisternStateLogic()
{
}
// -------------------------------------------------------------------------
void CisternStateLogic::connect()
{
	// Set Initial State
	set_current_state();
}
// -------------------------------------------------------------------------
void CisternStateLogic::on_init()
{
	init_state();
	set_sensor_handler();
	set_message_handler();
}
// -------------------------------------------------------------------------
void CisternStateLogic::set_current_state()
{
	assert(object_ != NULL);
	/*Если датчик установлен по-умолчанию UniWidgetsTypes::DefaultObjectId
	,то выходим без проверки его состояния*/
	if(get_state_di() == UniWidgetsTypes::DefaultObjectId)
		return;
	
	long value = object_->get_value_from_sm(get_state_di(),get_node());
	long mode = (get_mode() == -1) ? value : get_mode();

	if(value == mOFF)
	{
		current_cistern_->on_set_state_mode(mOFF,get_threshold(),false);
		current_value_ = value;
	}
	else
	{
		current_cistern_->on_set_state_mode(mode,get_threshold(),false);
		current_value_ = mode;
	}
	set_state_object();
}
// -------------------------------------------------------------------------
void CisternStateLogic::init_state()
{
	assert(object_ != NULL);
	std::vector<Gtk::Widget*> widgets(
			object_->get_children(typeLogic));

  CisternShowLogic* cistern = dynamic_cast<CisternShowLogic*>(widgets[0]);
	
	if ( cistern !=NULL )
	{
		current_cistern_=cistern;
	}
	else
		cerr << get_name() << ": widget doesn't load" << endl;
}
// -------------------------------------------------------------------------
void CisternStateLogic::set_message_handler()
{
	assert(object_ != NULL);

	if( ! object_->get_connector() )
		return;

	UMessages::MessageId id(get_state_di(), get_node(), 1);

	UMessages::Message msg =
			object_->get_connector()->signals().get_message(id);
	if( ! msg.valid() )
		return;
	/* Check to warning and alarm messages */
	if ( msg.getMessageType() != msgWARNING &&
		msg.getMessageType() != msgALARM )
		return;
	object_->set_message_handler(
			sigc::mem_fun(this, &CisternStateLogic::message_handler),msg.getMessageId());
}
// -------------------------------------------------------------------------
void CisternStateLogic::set_sensor_handler()
{
	assert(object_ != NULL);

	object_->set_sensor_handler(
			sigc::mem_fun(this, &CisternStateLogic::sensor_handler),
			get_state_di(),
			get_node());
	set_current_state();
}
// -------------------------------------------------------------------------
void CisternStateLogic::set_confirm_handler(UMessages::MessageId id)
{
	assert(object_ != NULL);

	sigc::slot<void, UMessages::MessageId, time_t> slot = sigc::mem_fun(this, &CisternStateLogic::confirm_handler);
	object_->set_confirm_handler(
			slot,
			id);
}
// -------------------------------------------------------------------------
void CisternStateLogic::confirm_handler(UMessages::MessageId id, time_t sec)
{
	long mode = (get_mode() == -1) ? id.get_long_value() : get_mode();
	current_cistern_->confirm(mode,get_threshold());
	set_state_object();
}
// -------------------------------------------------------------------------
void CisternStateLogic::message_handler(UMessages::MessageId id, Glib::ustring msg)
{
	if ( id._id != get_state_di() )
		return;
	if(current_cistern_!=NULL)
	{
 		set_confirm_handler(id);
	}
}
// -------------------------------------------------------------------------
void CisternStateLogic::sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value)
{
	set_state(value);
}
// -------------------------------------------------------------------------
void CisternStateLogic::set_state(const long value)
{
	assert(current_cistern_ != NULL);
	if(on_handle_error_message(value))
	{
		current_cistern_->off_set_state_mode(current_value_,get_threshold());
	}
	else
		return;
	if(value == mOFF)
	{
		current_cistern_->on_set_state_mode(value,get_threshold());
	}
	else
	{
		mode = (get_mode() == -1) ? value : get_mode();
		current_cistern_->on_set_state_mode(mode,get_threshold());
		if(!current_cistern_->is_blinking())
			current_cistern_->start_blink();
	}
	current_value_ = mode;
	set_state_object();
}
// -------------------------------------------------------------------------
void CisternStateLogic::set_state_object()
{
	if(get_state_obj_ai() != UniWidgetsTypes::DefaultObjectId)
	{
		long state_obj = current_cistern_ -> get_state_obj();
		/*Установить новое состояние датчика через set_value*/
		object_->set_value_obj(state_obj,get_state_obj_ai(),get_node());
	}
}
// -------------------------------------------------------------------------
