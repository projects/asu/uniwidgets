#ifndef _CISTERNSTATELOGIC_H
#define _CISTERNSTATELOGIC_H
// -------------------------------------------------------------------------
#include "types.h"
#include <objects/AbstractLogic.h>
#include <USignals.h>
#include <plugins.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class CisternShowLogic;
/*!
 * \brief Логика для порогового уровня цистерны.
 * \par
 * Логика для выставления режима в компоненте цистерна в зависимости от значения
 * отслеживаемого датчика. Основной задачей логики является отслеживание состояния
 * заданного датчика и при смене его значения выставлять соответствующий режим для цистерны
 * и снимать текущее режим. Для логики в свойствах задается режим "mode" соответствующий
 * аварии или предупреждению,а свойство threshold задает тип логики - верхний или нижний порог,
 * где 1 - верхний порог, 0 - нижний.
*/
class CisternStateLogic : public AbstractLogic
{
public:
	CisternStateLogic();
	explicit CisternStateLogic(Gtk::EventBox::BaseObjectType* gobject);
	virtual ~CisternStateLogic();

	virtual void connect();
protected:
	/* Methods */
	virtual void on_init();				/*!< инициализация логики */

private:
	/* Variables */
  CisternShowLogic* current_cistern_;		/*!< ссылку на логику отвечющую за отображение состояния цистерны */
	long current_value_;				/*!< текущий режим */

	/* Methods */
	void constructor();
	void init_state();				/*!< инициализация указателя на логику отображения цистерны CisternBlinkLogic */
	void set_state(const long value);		/*!< выставить новый режим и снять текущий */
	void set_current_state();			/*!< выставить текущий режим */

	void set_message_handler();
	void set_confirm_handler(UMessages::MessageId id);
	void set_sensor_handler();

	/* Handlers */
	void confirm_handler(UMessages::MessageId id, time_t sec);
	void message_handler(UMessages::MessageId id, Glib::ustring msg);
	void sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value);

	void set_state_object();

	DISALLOW_COPY_AND_ASSIGN(CisternStateLogic);
	/* Properties */
	ADD_PROPERTY( state_di, UniWidgetsTypes::ObjectId )
	ADD_PROPERTY( state_obj_ai, UniWidgetsTypes::ObjectId )
	ADD_PROPERTY( mode, UniWidgetsTypes::ObjectId )
	ADD_PROPERTY( threshold, UniWidgetsTypes::ObjectId )
	ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )
};

}

#endif
