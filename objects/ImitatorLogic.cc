#include <types.h>
#include "ImitatorLogic.h"
#include "SimpleObject.h"
#include "ShowLogic.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void ImitatorLogic::constructor()
{
	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback_object<ImitatorLogic>;
}
// -------------------------------------------------------------------------
ImitatorLogic::ImitatorLogic() :
	Glib::ObjectBase("imitatorlogic")
{
	constructor();
}
// -------------------------------------------------------------------------
ImitatorLogic::ImitatorLogic(StateLogic::BaseObjectType* gobject) :
	StateLogic(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
ImitatorLogic::~ImitatorLogic()
{
}
// -------------------------------------------------------------------------
void ImitatorLogic::set_current_state()
{
	assert(object_ != NULL);
	/*Если датчик установлен по-умолчанию UniWidgetsTypes::DefaultObjectId
	,то выходим без проверки его состояния*/
	if(get_state_ai() == UniWidgetsTypes::DefaultObjectId)
		return;

	long mode=mOFF;
	long init_state = object_->get_value_from_sm(get_state_ai(),get_node());
	if(logic != NULL)
	{
		mode = (get_mode() == -1 || !init_state) ? init_state : get_mode();
		if( mode != current_value_ )
		{
			if(on_handle_error_message(mode))
			{
				if(logic->show_state(mode,true))
				{
					set_state_object();
					logic->hide_state(current_value_);
					current_value_ = mode;
				}
			}
		}
	}
}
// -------------------------------------------------------------------------
