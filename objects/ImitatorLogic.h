#ifndef _IMITATORLOGIC_H
#define _IMITATORLOGIC_H
// -------------------------------------------------------------------------
#include <objects/StateLogic.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class Image;
/*!
 * \brief Логика для графического имитатора панелей.
 * \par
 * Логика используется для графического имитатора панелей. Отличие от StateLogic в том, что
 * при запуске графики, мигающие картинки будут мигать (если значение датчка уже выставленно).
*/
class ImitatorLogic : public StateLogic
{
public:
	ImitatorLogic();
	explicit ImitatorLogic(StateLogic::BaseObjectType* gobject);
	virtual ~ImitatorLogic();
protected: 
	void set_current_state();			/*!< выставить текущее состояние */
	DISALLOW_COPY_AND_ASSIGN(ImitatorLogic);
private:
	void constructor();
};

}
#endif
