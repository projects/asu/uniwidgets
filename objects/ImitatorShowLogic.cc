#include <types.h>
#include <components/SimpleView.h>
#include "ImitatorShowLogic.h"
#include "SimpleObject.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void ImitatorShowLogic::constructor()
{
	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback_object<ImitatorShowLogic>;
}
// -------------------------------------------------------------------------
ImitatorShowLogic::ImitatorShowLogic() :
	Glib::ObjectBase("imitatorshowlogic")
{
	constructor();
}
// -------------------------------------------------------------------------
ImitatorShowLogic::ImitatorShowLogic(ShowLogic::BaseObjectType* gobject) :
		ShowLogic(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
ImitatorShowLogic::~ImitatorShowLogic()
{
}
// -------------------------------------------------------------------------
bool ImitatorShowLogic::show_state( const long mode, bool blink )
{
	SimpleView* st = mode_to_state(mode);
	if(st == NULL)
		return false;

	if((current_state_ != NULL) && (current_state_->get_mode() == mode))
	{
		current_state_->set_show_(true);
		return true;
	}

	if( current_state_ != NULL )
		current_state_->hide();

	st->set_show_(true);
	set_state(mode,blink);

	return true;
}
// -------------------------------------------------------------------------
void ImitatorShowLogic::hide_state( const long mode )
{
  SimpleView* st = mode_to_state(mode);
	if(st == NULL)
		return;

	if(current_state_->get_mode() == mode)
	{
		current_state_->set_show_(false);
		if(!current_state_->is_blinking())
		{
			current_state_->hide();
		}
		else
		{
				current_state_->stop_blink();
				current_state_->set_blink_(false);

				current_state_->hide();
		}
	}
	else
	{
		st->set_show_(false);
		if(!st->get_blink_())
		{
			st->hide();
		}
		else
		{
			st->stop_blink();
			st->set_blink_(false);

			st->hide();
		}
	}

}
// -------------------------------------------------------------------------
void ImitatorShowLogic::set_state(const long mode,bool blink)
{
  SimpleView* state;

	state = mode_to_state(mode);

	if (current_state_ == state || state == NULL)
		return;

	state->show();
	/* Blink when connected and parameter blink is true only */
	if (object_->get_connector() && blink)
	{
		state->start_blink();
    state->can_blinking() ? state->set_blink_(true) : state->set_blink_(false);
	}
	else
		state->set_blink_(false);

	if(current_state_ != NULL)
	{
    if( current_state_->can_blinking() )
    {
      current_state_->set_blink_(current_state_->is_blinking());
    }
    else
    {
      current_state_->set_blink_(current_state_->is_blinking());
    }
    current_state_->stop_blink();
	}
	current_state_ = state;
	queue_draw();
}
// -------------------------------------------------------------------------
