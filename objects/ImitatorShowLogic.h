#ifndef _IMITATORSHOWLOGIC_H
#define _IMITATORSHOWLOGIC_H
// -------------------------------------------------------------------------
#include <objects/ShowLogic.h>
#include <objects/AbstractLogic.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Логика для графического имитатора панелей.
 * \par
 * Логика используется для графического имитатора панелей.
*/
class ImitatorShowLogic : public ShowLogic
{
public:
	ImitatorShowLogic();
	explicit ImitatorShowLogic(ShowLogic::BaseObjectType* gobject);
	virtual ~ImitatorShowLogic();
	DISALLOW_COPY_AND_ASSIGN(ImitatorShowLogic);

	bool show_state( const long mode, bool blink = true );
	void hide_state( const long mode);
	void set_state(const long mode,bool blink);
private:
	void constructor();
};

}

#endif
