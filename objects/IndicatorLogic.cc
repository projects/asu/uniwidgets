#include <iomanip>
#include <components/Text.h>
#include <types.h>
#include "IndicatorLogic.h"
#include "SimpleObject.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define INDICATORLOGIC_VALUE_AI			"value-ai"
#define INDICATORLOGIC_NODE			"node"
#define INDICATORLOGIC_DIGMODE               "digital-mode"
#define INDICATORLOGIC_DIGLENGTH           "digital-length"
#define INDICATORLOGIC_DIGPRICISION        "digital-precision"
#define INDICATORLOGIC_DIGFACTOR           "factor"
// -------------------------------------------------------------------------
#define INIT_INDICATORLOGIC_PROPERTIES() \
	value_ai(*this, INDICATORLOGIC_VALUE_AI, UniWidgetsTypes::DefaultObjectId) \
	,node(*this, INDICATORLOGIC_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,property_digital_mode_(*this, INDICATORLOGIC_DIGMODE, false) \
  ,property_digital_length_(*this, INDICATORLOGIC_DIGLENGTH, 3) \
  ,property_digital_precision_(*this, INDICATORLOGIC_DIGPRICISION, 0) \
  ,property_digital_factor_(*this, INDICATORLOGIC_DIGFACTOR, 1)
// -------------------------------------------------------------------------
void IndicatorLogic::constructor()
{

	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback_object<IndicatorLogic>;

//   connect_property_changed(TEXT_DIGMODE, sigc::mem_fun(*this, &Text::on_digitalmode_changed));
//   connect_property_changed(TEXT_DIGLENGTH, sigc::mem_fun(*this, &Text::on_digitalmode_changed));
//   connect_property_changed(TEXT_DIGPRICISION, sigc::mem_fun(*this, &Text::on_use_theme_changed)); 
	current_indicator_ = NULL;
}
// -------------------------------------------------------------------------
IndicatorLogic::IndicatorLogic() :
	Glib::ObjectBase("indicatorlogic")
	,INIT_INDICATORLOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
IndicatorLogic::IndicatorLogic(Gtk::EventBox::BaseObjectType* gobject) :
	AbstractLogic(gobject)
	,INIT_INDICATORLOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
IndicatorLogic::~IndicatorLogic()
{
}
// -------------------------------------------------------------------------
void IndicatorLogic::connect()
{
	// Set Initial State
	set_current_value();
}
// -------------------------------------------------------------------------
void IndicatorLogic::on_init()
{
	init_value();
// 	set_current_value();
	set_sensor_handler();
}
// -------------------------------------------------------------------------
void IndicatorLogic::set_current_value()
{
	assert(object_ != NULL);
	/*Если датчик установлен по-умолчанию UniWidgetsTypes::DefaultObjectId
	,то выходим без проверки его состояния*/
	if(get_value_ai() == UniWidgetsTypes::DefaultObjectId)
		return;

	float value;
	value = object_->get_analog_value(get_value_ai(),get_node());
	//Set value indicator
	set_value(value);
}
// -------------------------------------------------------------------------
void IndicatorLogic::init_value()
{
	assert(object_ != NULL);
	std::vector<Gtk::Widget*> widgets(
			object_->get_children(typeIndicator));

	Text* indicator = dynamic_cast<Text*>(widgets[0]);
	
	if ( indicator !=NULL )
	{
		current_indicator_=indicator;
    current_indicator_->set_property_text_("0");
	}else
	cerr << get_name() << ": widget doesn't load" << endl;
}
// -------------------------------------------------------------------------
void IndicatorLogic::set_sensor_handler()
{
	assert(object_ != NULL);
	object_->set_analog_sensor_handler(
			sigc::mem_fun(this, &IndicatorLogic::sensor_handler),
			get_value_ai(),
			get_node());
}
// -------------------------------------------------------------------------
void IndicatorLogic::sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, float value)
{
	set_value(value);
}
// -------------------------------------------------------------------------
void IndicatorLogic::set_value(const double value)
{
  assert(current_indicator_ != NULL);

  Glib::ustring value_string = "∞";

  int d = get_property_digital_length_();
  if( get_property_digital_precision_() > 0 )
    d-=(get_property_digital_precision_()+1);

  if( abs( value * get_property_digital_factor_() ) < pow(double(10),int(d)) )
  {
    value_string = Glib::ustring::format(std::fixed,
                                  std::setprecision(get_property_digital_precision_()),
                                  std::setw(get_property_digital_length_()),
                                  value * get_property_digital_factor_());
  }
  current_indicator_->set_property_text_(value_string);
}
// -------------------------------------------------------------------------
