#ifndef _INDICATORLOGIC_H
#define _INDICATORLOGIC_H
// -------------------------------------------------------------------------
#include "types.h"
#include <objects/AbstractLogic.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class Text;
/*!
 * \brief Логика для индикатора.
 * \par
 * Логика для выставления значения индикатора в зависимости от значения
 * отслеживаемого датчика.
*/
class IndicatorLogic : public AbstractLogic
{
public:
	IndicatorLogic();
	explicit IndicatorLogic(Gtk::EventBox::BaseObjectType* gobject);
	virtual ~IndicatorLogic();

	virtual void connect();
protected:
	/* Methods */
	virtual void on_init();						/*!< инициализация логики */

private:
	/* Variables */
	Text* current_indicator_;					/*!< ссылку на компонент индикатор */

	/* Methods */
	void constructor();
	void init_value();						/*!< инициализация ссылки на компонент индикатор путем запроса у контейнера виджета типа typeIndicator */
	void set_value(const double value);				/*!< выставление заданного уровня в цистерне */
	void set_current_value();					/*!< выставление текущего уровня в цистерне */

	/* Handlers */
	void sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, float value);
	void set_sensor_handler();

	DISALLOW_COPY_AND_ASSIGN(IndicatorLogic);
	/* Properties */
	ADD_PROPERTY( value_ai, UniWidgetsTypes::ObjectId )
	ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( property_digital_mode_, bool )
  ADD_PROPERTY( property_digital_length_, int )
  ADD_PROPERTY( property_digital_precision_, int )
  ADD_PROPERTY( property_digital_factor_, int )
};

}

#endif
