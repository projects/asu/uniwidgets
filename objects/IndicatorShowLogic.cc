#include <iomanip>
#include <types.h>
#include "IndicatorShowLogic.h"
#include "SimpleObject.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
using Glib::ustring;
// -------------------------------------------------------------------------
#define INDICATORSHOWLOGIC_FONT_COLOR_DEFAUILT    "default-color"
#define INDICATORSHOWLOGIC_FONT_COLOR_WARN    "warn-color"
#define INDICATORSHOWLOGIC_FONT_COLOR_ALARM    "alarm-color"
// -------------------------------------------------------------------------
#define INDICATORSHOWLOGIC_INIT_PROPERTIES() \
  default_color(*this, INDICATORSHOWLOGIC_FONT_COLOR_DEFAUILT, Gdk::Color("green")) \
  ,warn_color(*this, INDICATORSHOWLOGIC_FONT_COLOR_WARN, Gdk::Color("yellow")) \
  ,alarm_color(*this, INDICATORSHOWLOGIC_FONT_COLOR_ALARM, Gdk::Color("red"))
// -------------------------------------------------------------------------
void IndicatorShowLogic::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<IndicatorShowLogic>;

  colors_[mOFF] = get_default_color();
  colors_[mWARNING] = get_warn_color();
  colors_[mWARNING].set_red(0xffff);
  colors_[mWARNING].set_green(0xd0d0);
  colors_[mWARNING].set_blue(0x0000);
  colors_[mALARM] = get_alarm_color();

  current_indicator_ = NULL;
}
// -------------------------------------------------------------------------
IndicatorShowLogic::IndicatorShowLogic() :
  Glib::ObjectBase("indicatorshowlogic")
  ,INDICATORSHOWLOGIC_INIT_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
IndicatorShowLogic::IndicatorShowLogic(Gtk::EventBox::BaseObjectType* gobject) :
  QueueLogic(gobject)
  ,INDICATORSHOWLOGIC_INIT_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
// void IndicatorShowLogic::connect()
// {
//   // Sets default color for indicator
//   set_default_color();
// }
// -------------------------------------------------------------------------
void IndicatorShowLogic::set_default_color()
{
  assert(current_indicator_ != NULL);

//   current_indicator_->on_set_state_mode(mOFF);
  current_indicator_->set_property_font_color_(colors_[mOFF]);
  current_indicator_->set_property_state_(false);
}
// -------------------------------------------------------------------------
void IndicatorShowLogic::on_init()
{
  assert(object_ != NULL);

  std::vector<Gtk::Widget*> widgets(
      object_->get_children(typeIndicator));

  if( widgets.empty() )
    return;
  TextBlink *indicator = dynamic_cast<TextBlink*>(widgets[0]);
  if(indicator != NULL)
  {
    current_indicator_ = indicator;
    current_indicator_->set_property_font_color_(Gdk::Color("green"));
    current_indicator_->set_property_state_(false);
  }

}
// -------------------------------------------------------------------------
void IndicatorShowLogic::set_mode(long mode)
{
  if( colors_.find(mode) == colors_.end() )
    return;
  current_indicator_->set_property_on_font_color_(colors_[mode]);
  if(mode == mOFF && !is_blinking())
    current_indicator_->set_property_state_(false);
}
// -------------------------------------------------------------------------
void IndicatorShowLogic::on_set_state_mode(const long mode, const int type,bool blink)
{
  on_set_mode(mode,type,blink);
}
// -------------------------------------------------------------------------
void IndicatorShowLogic::set_state(bool state)
{
  current_indicator_->set_property_state_(state);
}
// -------------------------------------------------------------------------
void IndicatorShowLogic::off_set_state_mode(const long mode, const int type)
{
  off_set_mode(mode,type);
}
// -------------------------------------------------------------------------
void IndicatorShowLogic::stop_blink()
{
  current_indicator_->stop_blink();
  if( current_mode_ == mOFF )
    current_indicator_->set_property_state_(false);
  //current_indicator_->set_property_state_(false);
}
// -------------------------------------------------------------------------
bool IndicatorShowLogic::is_blinking()
{
  return current_indicator_ -> is_blinking();
}
// -------------------------------------------------------------------------
void IndicatorShowLogic::start_blink()
{
  current_indicator_->start_blink();
}
// -------------------------------------------------------------------------
long IndicatorShowLogic::get_state_obj()
{
  if(current_indicator_ -> is_blinking())
    return uwsWaitConfirm;
  else
  {
    long mode = current_mode_;
    if( mode == 11 or mode == 12 )
    {
      /*Значение соответствует порогам
      индикатора для одинакового состояния,
      но для разных типов порогов(верхний и нижний)*/
      return uwsWARNING;
    }
    else if( mode == 13 or mode == 14 )
    {
      return uwsALARM;
    }
    else if ( mode == mOFF )
    {
      return uwsOFF;
    }
    else
    {
      /*Установить соответствующее режиму значение*/
      return mode;
    }
  }
}
// -------------------------------------------------------------------------
void IndicatorShowLogic::addStateColor(const long mode,const Gdk::Color color,bool force)
{
  if( colors_.find(mode) == colors_.end() || force )
  {
    colors_[mode]=color;
  }
}
// -------------------------------------------------------------------------
