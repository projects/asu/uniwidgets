#ifndef _INDICATORSHOWLOGIC_H
#define _INDICATORSHOWLOGIC_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <gtkmm.h>
#include <gdkmm.h>
#include <objects/QueueLogic.h>
#include <SBlinker.h>
#include <USignals.h>
#include <ConfirmSignal.h>
#include <components/TextBlink.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class Text;
/*!
 * \brief Логика для индикатора.
 * \par
 * Логика для выставления порогового уровня в компоненте индикатор в зависимости от значения
 * отслеживаемого датчика. Если для индикатора существуют АПС сигналы уровней
 * (верхних и/или нижних) индикатор может мигать и менять цвет при наличии сработавшего
 * порога.
 * Индикатор на этом уровне представляет собой простое число без рамок и т.п. Это число выставляется
 * логикой IndicatorLogic и может мигать и менять цвета.
*/
class IndicatorShowLogic : public QueueLogic
{
public:
  IndicatorShowLogic();
  explicit IndicatorShowLogic(Gtk::EventBox::BaseObjectType* gobject);
  ~IndicatorShowLogic() {}

  /*! функция выставляет новый режим в зависимости от его типа и приоритета.
      Самый высокий приоритет обрабатывается в первую очередь, если приоритет одинаковый -
      то обработка по порядку поступления.
  */
  void on_set_state_mode(const long mode, const int type,bool blink=true);
  void off_set_state_mode(const long mode, const int type);      /*!< снятия режима */
  virtual void set_mode(long mode);            /*!< выставить режим для индикатора */
  virtual void set_state(bool state);            /*!< выставить состояние для индикатора (включен/выключен режим сработавшей АПС) */
  virtual void stop_blink();              /*!< выключить мигание индикатором */
  virtual void start_blink();              /*!< включить мигание индикатором */
  virtual bool is_blinking();              /*!< проверка индикатора на мигание */
  virtual long get_state_obj();              /*!< получить текущее состояние виджета */
  void addStateColor(const long mode,const Gdk::Color color, bool force = false);
//   virtual void connect();
protected:
  /* Methods */
  virtual void on_init();
private:
  /* Variables */
  TextBlink *current_indicator_;
  std::unordered_map<long , Gdk::Color> colors_;

  /* Methods */
  void constructor();
  void set_default_color();
  DISALLOW_COPY_AND_ASSIGN(IndicatorShowLogic);
  /* Properties */
  ADD_PROPERTY( default_color, Gdk::Color )
  ADD_PROPERTY( warn_color, Gdk::Color )
  ADD_PROPERTY( alarm_color, Gdk::Color ) 
};

}
#endif
