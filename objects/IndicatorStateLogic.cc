#include <types.h>
#include <ConfirmSignal.h>
#include "IndicatorStateLogic.h"
#include "SimpleObject.h"
#include "IndicatorShowLogic.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define INDICATORSTATELOGIC_STATE_DI    "state-di"
#define INDICATORSTATELOGIC_STATE_OBJ_AI  "state-obj-ai"
#define INDICATORSTATELOGIC_MODE    "mode"
#define INDICATORSTATELOGIC_CANBLINK    "blinking"
#define INDICATORSTATELOGIC_THRESHOLD    "threshold"
#define INDICATORSTATELOGIC_NODE    "node"
// -------------------------------------------------------------------------
#define INIT_INDICATORSTATELOGIC_PROPERTIES() \
  state_di(*this, INDICATORSTATELOGIC_STATE_DI, UniWidgetsTypes::DefaultObjectId)\
  ,state_obj_ai(*this, INDICATORSTATELOGIC_STATE_OBJ_AI, UniWidgetsTypes::DefaultObjectId)\
  ,mode(*this, INDICATORSTATELOGIC_MODE, -1)\
  ,blinking(*this, INDICATORSTATELOGIC_CANBLINK, true)\
  ,threshold(*this, INDICATORSTATELOGIC_THRESHOLD, UniWidgetsTypes::DefaultObjectId)\
  ,node(*this, INDICATORSTATELOGIC_NODE , UniWidgetsTypes::DefaultObjectId)
// -------------------------------------------------------------------------
void IndicatorStateLogic::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<IndicatorStateLogic>;

  current_indicator_ = NULL;
  current_value_= 0;
}
// -------------------------------------------------------------------------
IndicatorStateLogic::IndicatorStateLogic() :
    Glib::ObjectBase("indicatorstatelogic")
  ,INIT_INDICATORSTATELOGIC_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
IndicatorStateLogic::IndicatorStateLogic(Gtk::EventBox::BaseObjectType* gobject) :
  AbstractLogic(gobject)
  ,INIT_INDICATORSTATELOGIC_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
IndicatorStateLogic::~IndicatorStateLogic()
{
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::connect()
{
  // Set Initial State
  set_current_state();
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::on_init()
{
  init_state();
  set_sensor_handler();
  set_message_handler();
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::set_current_state()
{
  assert(object_ != NULL);
  /*Если датчик установлен по-умолчанию UniWidgetsTypes::DefaultObjectId
  ,то выходим без проверки его состояния*/
  if(get_state_di() == UniWidgetsTypes::DefaultObjectId)
    return;
  
  long value = object_->get_value_from_sm(get_state_di(),get_node());

  if(current_indicator_ != NULL)
  {
    long mode = (get_mode() == -1 || !value) ? value : get_mode();
    if( mode != current_value_ )
    {
      if(on_handle_error_message(mode))
      {
        current_indicator_->off_set_state_mode(current_value_,get_threshold());
        if(mode != mOFF)
            current_indicator_->on_set_state_mode(mode,get_threshold(),false);
        current_value_ = mode;
      }
      else
        return;
    }

  }

  /*Получить состояние объекта*/
  long state_obj = current_indicator_ -> get_state_obj();
  /*Установить новое состояние датчика через set_value*/
  object_->set_value_obj(state_obj,get_state_obj_ai(),get_node());
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::init_state()
{
  assert(object_ != NULL);
  std::vector<Gtk::Widget*> widgets(
      object_->get_children(typeLogic));

  IndicatorShowLogic* indicator = dynamic_cast<IndicatorShowLogic*>(widgets[0]);
  
  if ( indicator !=NULL )
  {
    current_indicator_=indicator;
  }
  else
    cerr << get_name() << ": widget doesn't load" << endl;
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::set_message_handler()
{
  assert(object_ != NULL);

  if( ! object_->get_connector() )
    return;

  UMessages::MessageId id(get_state_di(), get_node(), 1);

  UMessages::Message msg =
      object_->get_connector()->signals().get_message(id);
  if( ! msg.valid() )
    return;
  /* Check to warning and alarm messages */
  if ( msg.getMessageType() != msgWARNING &&
    msg.getMessageType() != msgALARM )
    return;
  object_->set_message_handler(
                 sigc::mem_fun(this, &IndicatorStateLogic::message_handler),msg.getMessageId());
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::set_sensor_handler()
{
  assert(object_ != NULL);

  object_->set_sensor_handler(
                              sigc::mem_fun(this, &IndicatorStateLogic::sensor_handler),
      get_state_di(),
      get_node());
  set_current_state();
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::set_confirm_handler(UMessages::MessageId id)
{
  assert(object_ != NULL);

  sigc::slot<void, UMessages::MessageId, time_t> slot = sigc::mem_fun(this, &IndicatorStateLogic::confirm_handler);
  object_->set_confirm_handler(
      slot,
      id);
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::confirm_handler(UMessages::MessageId id, time_t sec)
{
  long mode = (get_mode() == -1) ? id.get_long_value() : get_mode();
  current_indicator_->confirm(mode,get_threshold());
  long state_obj = current_indicator_ -> get_state_obj();
  /*Установить новое состояние датчика через set_value*/
  object_->set_value_obj(state_obj,get_state_obj_ai(),get_node());
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::message_handler(UMessages::MessageId id, Glib::ustring msg)
{
  if ( id._id != get_state_di() )
    return;
  if(current_indicator_!=NULL)
  {
     set_confirm_handler(id);
  }
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value)
{
  set_state(value);
}
// -------------------------------------------------------------------------
void IndicatorStateLogic::set_state(const long value)
{
  assert(current_indicator_ != NULL);

  long mode=mOFF;
  if(on_handle_error_message(value))
  {
    current_indicator_->off_set_state_mode(current_value_,get_threshold());
  }
  else
    return;

//   if(value == mOFF)
//   {
//     current_indicator_->on_set_state_mode(value,get_threshold(),false);
//   }
  if(value != mOFF)
  {
    mode = (get_mode() == -1) ? value : get_mode();
    current_indicator_->on_set_state_mode(mode,get_threshold(),get_blinking());
  }
  current_value_ = mode;
  long state_obj = current_indicator_ -> get_state_obj();
  /*Установить новое состояние датчика через set_value*/
  object_->set_value_obj(state_obj,get_state_obj_ai(),get_node());
}
// -------------------------------------------------------------------------
