#ifndef _INDICATORSTATELOGIC_H
#define _INDICATORSTATELOGIC_H
// -------------------------------------------------------------------------
#include "types.h"
#include <objects/AbstractLogic.h>
#include <USignals.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
  class IndicatorShowLogic;
/*!
 * \brief Логика для порогового уровня индикатора.
 * \par
 * Логика для выставления режима в компоненте индикатор в зависимости от значения
 * отслеживаемого датчика. Основной задачей логики является отслеживание состояния
 * заданного датчика и при смене его значения выставлять соответствующий режим для индикатора
 * и снимать текущее режим. Для логики в свойствах задается режим "mode" соответствующий
 * аварии или предупреждению,а свойство threshold задает тип логики - верхний или нижний порог,
 * где 1 - верхний порог, 0 - нижний.
*/
class IndicatorStateLogic : public AbstractLogic
{
public:
  IndicatorStateLogic();
  explicit IndicatorStateLogic(Gtk::EventBox::BaseObjectType* gobject);
  virtual ~IndicatorStateLogic();

  virtual void connect();
protected:
  /* Methods */
  virtual void on_init();          /*!< инициализация логики */

private:
  /* Variables */
  IndicatorShowLogic* current_indicator_;    /*!< ссылку на логику отвечющую за отображение состояния индикатора */
  long current_value_;          /*!< текущий режим */
  /* Methods */
  void constructor();
  void init_state();          /*!< инициализация указателя на логику отображения индикатора IndicatorShowLogic */
  void set_state(const long value);      /*!< выставить новый режим и снять текущий */
  void set_current_state();        /*!< выставить текущий режим */

  void set_message_handler();
  void set_confirm_handler(UMessages::MessageId id);
  void set_sensor_handler();

  /* Handlers */
  void confirm_handler(UMessages::MessageId id, time_t sec);
  void message_handler(UMessages::MessageId id, Glib::ustring msg);
  void sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value);

  DISALLOW_COPY_AND_ASSIGN(IndicatorStateLogic);
  /* Properties */
  ADD_PROPERTY( state_di, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( state_obj_ai, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( mode, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( blinking, bool )
  ADD_PROPERTY( threshold, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )
};

}

#endif
