#include <types.h>
#include <ConfirmSignal.h>
#include "LinkLogic.h"
#include "SimpleObject.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define LINKLOGIC_LINK_DI		"link-di"
#define LINKLOGIC_NODE			"node"
// -------------------------------------------------------------------------
#define INIT_LINKLOGIC_PROPERTIES() \
	link_di(*this, LINKLOGIC_LINK_DI , UniWidgetsTypes::DefaultObjectId) \
	,node(*this, LINKLOGIC_NODE , UniWidgetsTypes::DefaultObjectId)
// -------------------------------------------------------------------------
void LinkLogic::constructor()
{
	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback_object<LinkLogic>;
}
// -------------------------------------------------------------------------
LinkLogic::LinkLogic() :
	Glib::ObjectBase("linklogic")
	,INIT_LINKLOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
LinkLogic::LinkLogic(Gtk::EventBox::BaseObjectType* gobject) :
	AbstractLogic(gobject)
	,INIT_LINKLOGIC_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
LinkLogic::~LinkLogic()
{
}
// -------------------------------------------------------------------------
void LinkLogic::connect()
{
	set_current_link();
}
// -------------------------------------------------------------------------
void LinkLogic::on_init()
{
	assert(object_ != NULL);

	set_sensor_handler();
	connect_property_changed(LINKLOGIC_LINK_DI, sigc::mem_fun(*this, &LinkLogic::set_sensor_handler));
}
// -------------------------------------------------------------------------
void LinkLogic::set_current_link()
{
	assert(object_ != NULL);
	/*Если датчик установлен по-умолчанию UniWidgetsTypes::DefaultObjectId
	,то выходим без проверки его состояния*/
	if(get_link_di() == UniWidgetsTypes::DefaultObjectId)
		return;
	long link;
	link = object_->get_value_from_sm(get_link_di(), get_node());

	set_link(link);
}
// -------------------------------------------------------------------------
void LinkLogic::set_link(const long link)
{
	assert(object_ != NULL);

	if ( link == linkOn )
		object_->set_link(true);
	else if ( link == linkOff )
		object_->set_link(false);
	else
		cerr << get_name() << ": unexpected sensor value - " << link << endl;

	object_->queue_draw();
}
// -------------------------------------------------------------------------
void LinkLogic::set_sensor_handler()
{
	assert(object_ != NULL);
	if( get_link_di() != UniWidgetsTypes::DefaultObjectId )
		object_->set_sensor_handler(
				sigc::mem_fun(this, &LinkLogic::sensor_handler),
				get_link_di(),
				get_node());
	set_current_link();
}
// -------------------------------------------------------------------------
void LinkLogic::sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value)
{
	set_link(value);
}
// -------------------------------------------------------------------------
