#ifndef _LINKLOGIC_H
#define _LINKLOGIC_H
// -------------------------------------------------------------------------
#include "types.h"
#include <objects/AbstractLogic.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Логика наличия связи с SharedMemory.
 * \par
 * Логика отвечает за отрисовку на виджете эффекта "серения" при отсутствии
 * связи с SharedMemory.
*/
class LinkLogic : public AbstractLogic
{
public:
	LinkLogic();
	explicit LinkLogic(Gtk::EventBox::BaseObjectType* gobject);
	virtual ~LinkLogic();

	/* Methods */
	virtual void connect();

protected:
	/* Methods */
	virtual void on_init();				/*!< инициализация логики */

private:
	/* Methods */
	void set_sensor_handler();
	void set_current_link();
	void set_link(const long link);			/*!< выставить состояние соединения c SharedMemory */
	void constructor();

	/* Handlers */
	void sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value);

	DISALLOW_COPY_AND_ASSIGN(LinkLogic);

	/* Properties */
	ADD_PROPERTY( link_di, UniWidgetsTypes::ObjectId )
	ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )
};

}

#endif
