#ifndef _OBJECTS_H
#define _OBJECTS_H

#include <objects/SimpleObject.h>
#include <objects/StateLogic.h>
#include <objects/ImitatorLogic.h>
#include <objects/ImitatorShowLogic.h>
#include <objects/StateMultiLogic.h>
#include <objects/LinkLogic.h>
#include <objects/IndicatorLogic.h>
#include <objects/IndicatorStateLogic.h>
#include <objects/ThresholdAnalogLogic.h>
#include <objects/CisternLogic.h>
#include <objects/IndicatorShowLogic.h>
#include <objects/CisternShowLogic.h>
#include <objects/CisternStateLogic.h>
#include <objects/ShowLogic.h>
#include <objects/ADLogic.h>

#endif
