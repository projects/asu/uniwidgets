#include <iomanip>
#include <types.h>
#include "QueueLogic.h"
#include "SimpleObject.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
using Glib::ustring;
// -------------------------------------------------------------------------
struct modeFind : binary_function <Indinfo * ,int , bool>
{
  bool operator () (Indinfo *info , int type) const
  {
    if(info->type_ == type)
      return true;
    else
      return false;
  }
};
// -------------------------------------------------------------------------
void QueueLogic::constructor()
{
  current_mode_= 0;
  current_type_ = 1;
}
// -------------------------------------------------------------------------
QueueLogic::QueueLogic() :
  Glib::ObjectBase("queuelogic")
{
  constructor();
}
// -------------------------------------------------------------------------
QueueLogic::QueueLogic(Gtk::EventBox::BaseObjectType* gobject) :
  AbstractLogic(gobject)
{
  constructor();
}
// -------------------------------------------------------------------------
Indinfo* QueueLogic::set_info(bool show, bool blink, int type)
{
  Indinfo *new_info = new Indinfo();
  new_info->is_show_=show;
  new_info->blinking_=blink;
  new_info->type_=type;
  return new_info;
}
// -------------------------------------------------------------------------
void QueueLogic::on_set_mode(const long mode, const int type,bool blink)
{
  Modes::iterator m_it = on_modes_.find(mode);
  Indinfo *ii = NULL;
  
  if ( m_it == on_modes_.end() )
  {
    
    //Не найдено в векторе это состояние т.е. добавляется новое состояние
    std::deque<Indinfo *> v;
    ii = create_newinfo(mode, type, blink);
    if(blink)
      v.push_front(ii);
    else
      v.push_back(ii);
    //Запоминаем новое состояние в очереди
    on_modes_.insert(pair<long, std::deque<Indinfo *> >( mode, v ));
  }
  else
  {
    //Найдено состояние!
    ii = find_type(mode , type);
    if( ii != NULL )//Далее поиск по типу найденного сигнала
    {
      ii -> is_show_ = true;
    }
    else
    {
      ii = create_newinfo(mode, type, blink);
      if( blink )
        (m_it->second).push_front(ii);
      else
        (m_it->second).push_back(ii);
    }
  }
}
// -------------------------------------------------------------------------
void QueueLogic::off_set_mode(const long mode, const int type)
{
  Modes::iterator m_it = on_modes_.find(mode);

  if( m_it != on_modes_.end() )
  {
    Indinfo *ii = find_type( mode, type );
    if( ii == NULL )
      return;
    if( mode == current_mode_ && type == current_type_)/*Выключается текущий мод*/
    {
      if(ii -> blinking_ == true)
        ii -> is_show_ = false;
      else
      {
        erase_mode( mode , type );
        if( (m_it->second).empty() )
          on_modes_.erase(mode);
        long max_mode = get_max_mode();
        if(max_mode == -1)
        {
          set_mode(mOFF);
          current_mode_ = max_mode;
          current_type_ = 0;
        }
        else
        {
          Modes::iterator max_it = on_modes_.find(max_mode);
          Indinfo *ii = (max_it->second).front();
          set_mode(max_mode);
          if(ii -> blinking_ == true && !is_blinking())
            start_blink();
          current_mode_=max_mode;
          current_type_ = ii -> type_;
        }
      }
    }
    else
    {
      if(ii -> blinking_ == false)
      {
        erase_mode( mode , type );
        if((m_it->second).empty())
          on_modes_.erase(mode);
      }
      else
        ii -> is_show_ = false;
    }
  }
}
// -------------------------------------------------------------------------
void QueueLogic::erase_mode(long mode, int type)
{
  Modes::iterator it = on_modes_.find(mode);
  
  if( it == on_modes_.end() )
    return;

  if( (it->second).empty() )
    return;

  (it->second).erase(
  remove_if((it->second).begin(),
      (it->second).end(),
      bind2nd(modeFind(), type))
      ,(it->second).end());
}
// -------------------------------------------------------------------------
long QueueLogic::get_max_mode()
{
#warning "-1"! - нужен другой осмысленный возвращаемый параметр!
  if( on_modes_.empty() )
    return -1;
  Modes::iterator it = on_modes_.begin();
  return it->first;
}
// -------------------------------------------------------------------------
void QueueLogic::confirm(long mode, int type)
{
  Indinfo *ii = find_type( mode, type );
  if( mode == current_mode_ && type == current_type_ )
  {
    stop_blink();
  }
  if( ii != NULL)
    ii -> blinking_ = false;
  else
    return;

  if( ii -> is_show_ == false )
  {
    erase_mode( mode , type );
    if(on_modes_[mode].empty())
    {
      on_modes_.erase(mode);
    }
    long max_mode = get_max_mode();
    if(max_mode == -1)
    {
      set_mode(mOFF);
      current_mode_ = max_mode;
      current_type_ = 0;
    }
    else
    {
      Modes::iterator max_it = on_modes_.find(max_mode);
      Indinfo *ii = (max_it->second).front();
      set_mode(max_mode);
      if( ii -> blinking_ == true && !is_blinking() )
        start_blink();
      current_mode_ = max_mode;
      current_type_ = ii -> type_;
    }
  queue_draw();
  }
}
// -------------------------------------------------------------------------
Indinfo* QueueLogic::find_type(const long mode, const int type)
{
  Modes::iterator m_it = on_modes_.find(mode);

  if( m_it != on_modes_.end() )
  {
    std::deque<Indinfo *>::iterator it;

    if( (m_it->second).empty() )
      return NULL;
    it = find_if((m_it->second).begin(),
        (m_it->second).end(),
        bind2nd(modeFind(), type));
    if(it != (m_it->second).end())
      return (*it);
    else
      return NULL;
  }
  else
    return NULL;
}
// -------------------------------------------------------------------------
bool QueueLogic::is_blinking_mode(const long mode, const int type)
{
  Indinfo *ii = find_type( mode, type );
  if( ii != NULL)
    return ii->blinking_;

  return false;
}
// -------------------------------------------------------------------------
bool QueueLogic::is_show(const long mode, const int type)
{
  Modes::iterator m_it = on_modes_.find(mode);

  if( m_it != on_modes_.end() )
  {
    std::deque<Indinfo *>::iterator it;

    if( (m_it->second).empty() )
      return false;
    it = find_if((m_it->second).begin(),
                    (m_it->second).end(),
                    bind2nd(modeFind(), type));
    if(it != (m_it->second).end())
      return (*it)->is_show_;
  }

  return false;
}
// -------------------------------------------------------------------------
Indinfo* QueueLogic::create_newinfo(const long mode,const int type, bool blink)
{
  Indinfo *ii = new Indinfo();
  ii -> is_show_ = true;

  if( mode == mOFF || blink == false )
    ii -> blinking_ = false;
  else
    ii -> blinking_ = true;

  ii -> type_ = type;

  /* Проверка по приоритету нового состояния: Если приоритет больше текущего, то отображаем
  новое состояние по шагам: останавливаем текущее мигание виджета, выставляем новое состояние,
  запоминаем текущие состояние и тип, set_state - выставляет отображение главной части отображаемого
  объекта(для текста основной шрифт состояние "ON") и если текущие состояние предусматривает мигание,
  то запускаем мигание виджета */
  if ( mode > current_mode_ || ( mode == current_mode_ && !is_blinking()) )
  {
    stop_blink();
    set_mode(mode);
    current_mode_ = mode;
    current_type_ = type;
    set_state(true);
    if( ii -> blinking_ )
    {
      start_blink();
    }
  }
  return ii;
}
// -------------------------------------------------------------------------
