#ifndef _QUEUELOGIC_H
#define _QUEUELOGIC_H
// -------------------------------------------------------------------------
#include <map>
#include <gtkmm.h>
#include <gdkmm.h>
#include <objects/AbstractLogic.h>
#include <SBlinker.h>
#include <USignals.h>
#include <ConfirmSignal.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{

struct Indinfo
{
  bool is_show_;
  bool blinking_;
  /*"1" - high level
    "0" - low level*/
  int type_;
};
/*!
 * \brief Логика для работы с очередями режимов.
 * \par
 * Логика предназначена для образования очередей на установку режима(mode) для виджета,
 * если виджет содержит несколько режимов с одинаковым типом. Т.е., например, если
 * индикатор содержит верхний и нижний преждупредительный порог одновременно, то нужно
 * различать их типы как-то еще. Для этого вводится тип, где 1 - верхний уровень, 0 - нижний.
 * Логика является базовым классом для произвольных логик. Объектов QueueLogic не может существовать.
*/
class QueueLogic : public AbstractLogic
{
public:
  QueueLogic();
  explicit QueueLogic(Gtk::EventBox::BaseObjectType* gobject);
  ~QueueLogic() {}
  
  /*Types*/
  typedef std::map<long , std::deque<Indinfo*>, std::greater<long> > Modes;  /*!< тип вектора для хранения режимов */

  virtual void stop_blink(){};              /*!< выключить мигание */
  virtual void start_blink(){};              /*!< включить мигание */
  virtual void set_state(bool newstate_){};          /*!< установить новое состояние */
  virtual bool is_blinking(){return is_blinking_mode(current_mode_,current_type_);}          /*!< проверка на мигание текущего состояния */
  virtual void set_mode(long mode) = 0;            /*!< установить новый режим */
  virtual long get_state_obj() = 0;            /*!< получить состояние виджета */
  void confirm(long mode, int type);            /*!< обработчик квитирования */
  void on_set_mode(const long mode, const int type,bool blink=true);    /*!< установить новый режим реальная функция */
  void off_set_mode(const long mode, const int type);        /*!< сбросить режим реальная функция */
  bool is_show(const long mode, const int type);          /*!< стоит ли в очередь к показу данный режим */
  bool is_blinking_mode(const long mode, const int type);        /*!< стоит ли в очередь к показу с миганием данный режим */
protected:
  /* Methods */
  virtual void on_init(){};
  long current_mode_;                /*!< текущий режим */
private:
  /* Variables */
  Modes on_modes_;                /*!< режимы в очереди */
  int current_type_;                /*!< текущий тип режима */

  /* Methods */
  long get_max_mode();                /*!< получить режим с максимальным приоритетом */
  Indinfo* set_info(bool show, bool blink,int type);        /*!< создание структуры Indinfo описывающей тип режима */
  Indinfo* find_type(const long mode, const int type);        /*!< найти информацию о режиме */
  void erase_mode(const long mode, const int type);        /*!< удаление режима из очереди */
  Indinfo* create_newinfo(const long mode,const int type, bool blink);  /*!< добавление режима в очередь */
  
  void constructor();
  DISALLOW_COPY_AND_ASSIGN(QueueLogic);
};

}
#endif
