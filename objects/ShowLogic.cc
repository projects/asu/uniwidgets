#include <types.h>
#include <components/SimpleView.h>
#include "ShowLogic.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define INIT_SHOWLOGIC_PROPERTIES() \
  view_type(*this, "view-type" , 1) \
  ,noconfirm(*this, "no-confirms-string" , "")
// -------------------------------------------------------------------------
// Functor
template <typename T>
struct priorityCompare : binary_function <T *, T *, bool>
{
  bool operator () (T *lhs, T *rhs) const
  {
    if( !rhs->get_show_() && !rhs->get_blink_() )
      return true;
    if( !lhs->get_show_() && !lhs->get_blink_() )
      return false;
    if( (lhs->get_priority() == rhs->get_priority()) )
      return (!lhs->get_blink_() && rhs->get_blink_() ? false : true);//Если состояния равны, то считаем что сравниваемое состояние не больше!
    
    return (lhs->get_priority() > rhs->get_priority() ? true : false);
  }
};
// -------------------------------------------------------------------------
template <typename T>
struct modeFind : binary_function <T * ,long , bool>
{
  bool operator () (T *state, long mode) const
  {
    if(state->get_mode() == mode)
      return true;
    else
      return false;
  }
};
// -------------------------------------------------------------------------
template <typename T>
struct Widget2Target : unary_function <Gtk::Widget*, T*>
{
  T* operator () (Gtk::Widget* value) const
  {
    T* target = dynamic_cast<T*>(value);
    if( target != NULL )
        {
            target->hide();
        }
        else
        {
            cerr<<"Widget "<<value->get_name()<<" doesn't have right type"<<endl;
            assert( target!=NULL );
        }
    return target;
  }
};
// -------------------------------------------------------------------------

void ShowLogic::constructor()
{
  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<ShowLogic>;

  current_state_ = NULL;
}
// -------------------------------------------------------------------------

ShowLogic::ShowLogic() :
  Glib::ObjectBase("showlogic")
  ,is_initialize_(false)
  ,is_glade(false)
  ,INIT_SHOWLOGIC_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------

ShowLogic::ShowLogic(Gtk::EventBox::BaseObjectType* gobject) :
  AbstractLogic(gobject)
  ,is_initialize_(false)
  ,is_glade(true)
  ,INIT_SHOWLOGIC_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------

ShowLogic::~ShowLogic()
{
}
// -------------------------------------------------------------------------

void ShowLogic::on_init()
{
/*Virtual*/
  if(is_glade)
  {
    std::vector<Gtk::Widget*> widgets(object_->get_children(get_view_type()));
    if( widgets.empty() )
      return;
    for(unsigned int i=0;i<widgets.size();i++)
    {
      SimpleView* sv = dynamic_cast<SimpleView *>(widgets[i]);
      if(sv != NULL)
        sv->show();
    }
    object_->show_all();
    return;
  }
  init_states();
  configure_noconfirm();
  SimpleView* st = mode_to_state(mOFF);
  if( st != NULL )
  {
    st->set_show_(true);
    set_state( mOFF , false);
  }
  is_initialize_ = true;
}
// -------------------------------------------------------------------------

void ShowLogic::init_states()
{
  /*Virtual,rename*/
  assert(object_ != NULL);

  std::vector<Gtk::Widget*> widgets(object_->get_children(get_view_type()));
  if( widgets.empty() )
    return;
  transform(widgets.begin(),
                  widgets.end(),
                  back_inserter(states_),
                  Widget2Target<SimpleView>() );
}
// -------------------------------------------------------------------------

void ShowLogic::confirm_handler(const long mode)
{
  SimpleView* st = mode_to_state(mode);
  if(st == NULL)
    return;
  if( mode != current_state_->get_mode())
  {
    st->set_blink_(false);
    return;
  }
  if ( current_state_ != NULL )
  {
    current_state_->stop_blink();
    current_state_->set_blink_(false);
  }
  if( !current_state_->get_show_() )
  {
    current_state_->hide();
    long max_mode = get_mode_in_max_priority();
    set_state(max_mode, mode_to_state(max_mode)->get_blink_());
  }
}
// -------------------------------------------------------------------------

long ShowLogic::get_mode_in_max_priority()
{
  if( !states_.empty() )
  {
    partial_sort(states_.begin(),
      states_.begin()+1,
      states_.end(),
                  priorityCompare<SimpleView>());
    return states_.front()->get_mode();
  }
  else
    return mOFF;
}
// -------------------------------------------------------------------------

bool ShowLogic::show_state( const long mode, bool blink )
{
  SimpleView* st = mode_to_state(mode);
  if(st == NULL)
  {
  return false;
  }

  if((current_state_ != NULL) && (current_state_->get_mode() == mode))
  {
    current_state_->set_show_(true);
    if(blink)
    {
      current_state_->start_blink();
      current_state_->set_blink_( current_state_->can_blinking() );
    }
    else
      current_state_->set_blink_(false);
    return true;
  }

  long priority_ = mode_to_priority(mode);
  if (is_priority_higher(priority_))
  {
    /*Добавление текущего события с меньшим приоритетом, чем у нового*/
    if( current_state_ != NULL )
      current_state_->hide();
    st->set_show_(true);
    set_state(mode,blink);
  }
  else
  {
        SimpleView *state = mode_to_state(mode);
        state->set_show_(true);
        state->can_blinking() ? state->set_blink_(blink) : state->set_blink_(false);
  }

  return true;
}
// -------------------------------------------------------------------------

void ShowLogic::hide_state( const long mode )
{
  SimpleView* st = mode_to_state(mode);
  if(st == NULL)
    return;
  long current_mode = current_state_->get_mode() ;
  if( current_mode == mode )
  {
    current_state_->set_show_(false);
    if(!current_state_->is_blinking())
    {
      current_state_->hide();
      long max_mode = get_mode_in_max_priority();
      set_state(max_mode, mode_to_state(max_mode)->get_blink_());
    }
    else
    {  // Для того, чтобы mTRANSITIVE и mUNKNOWN скрывались без квитирования
      if(current_mode == mTRANSITIVE || current_mode == mUNKNOWN || is_noconfirm( current_mode ) )
      {
        current_state_->stop_blink();
        current_state_->set_blink_(false);
        current_state_->hide();
        long max_mode = get_mode_in_max_priority();
        set_state(max_mode, mode_to_state(max_mode)->get_blink_());
      }
    }
  }
  else
  {
    st->set_show_(false);
    if(!st->get_blink_() || mode == mTRANSITIVE || mode == mUNKNOWN || is_noconfirm( mode ))
    {
      st->set_blink_(false);
      st->hide();
    }
  }

}
// -------------------------------------------------------------------------

void ShowLogic::set_state(const long mode,bool blink)
{
  SimpleView* state;

  state = mode_to_state(mode);

  if (current_state_ == state || state == NULL)
    return;

  state->show();
  /* Blink when connected and parameter blink is true only */
  if (object_->get_connector() && blink)
  {
    state->start_blink();
    current_state_->can_blinking() ? current_state_->set_blink_(true) : current_state_->set_blink_(false);
  }
  else
    state->set_blink_(false);

  if(current_state_ != NULL)
  {
    long current_mode = current_state_->get_mode() ;
    if(current_mode == mTRANSITIVE || current_mode == mUNKNOWN || is_noconfirm( current_mode ) )
    {
      current_state_->set_blink_(false);
    }
    else
    {
      if( current_state_->can_blinking() )
      {
        current_state_->set_blink_(current_state_->is_blinking());
      }
      else
      {
        current_state_->set_blink_(false);
      }
    }
    current_state_->stop_blink();
  }
  current_state_ = state;
  queue_draw();
}
// -------------------------------------------------------------------------

bool ShowLogic::is_priority_higher(const long new_priority)
{
  if(current_state_ != NULL)
  {
    if ( current_state_->get_priority() < new_priority || ( current_state_->get_priority() == new_priority && (!current_state_->is_blinking() || is_noconfirm( current_state_->get_mode() ) ) ) )
    {
      return true;
    }
  }
  return false;
}
// -------------------------------------------------------------------------

long ShowLogic::mode_to_priority(const long mode)
{
  std::vector< SimpleView* >::iterator it;
  if( !states_.empty() )
  {
    it = find_if(states_.begin(),
        states_.end(),
                    bind2nd(modeFind<SimpleView>(),mode));
    if(it != states_.end())
      return (*it)->get_priority();
  }
  return 0;
}
// -------------------------------------------------------------------------

SimpleView* ShowLogic::mode_to_state(const long mode)
{
  std::vector< SimpleView* >::iterator it;
  if( !states_.empty() )
  {
    it = find_if(states_.begin(),
        states_.end(),
                    bind2nd(modeFind<SimpleView>(),mode));
    if(it != states_.end())
      return (*it);
  }
  return NULL;
}
// -------------------------------------------------------------------------

long ShowLogic::get_state_obj()
{
  if(current_state_ == NULL)
    return uwsUnknown;

  if(current_state_ -> is_blinking())
    return uwsWaitConfirm;
  else
  {
    long mode = current_state_ -> get_mode();
    if( mode == mWARNING_HIGH or mode == mWARNING_LOW )
    {
      /*Значение соответствует порогам
      индикатора для одинакового состояния,
      но для разных типов порогов(верхний и нижний)*/
      return uwsWARNING;
    }
    else if( mode == mALARM_HIGH or mode == mALARM_LOW )
    {
      return uwsALARM;
    }
    else if( mode == mWARNING )
    {
      return uwsWARNING;
    }
    else if( mode == mALARM )
    {
      return uwsALARM;
    }
    else if( mode != mOFF)
    {
      /*Любые значения отличные от "0" и от выше перечисленных
        воспринимаются как "включенное состояние"*/
      return uwsON;
    }
    else
    {
      /*В конце проверок остается только состояние соответствующее "0"*/
      return uwsOFF;
    }
  }
}
// -------------------------------------------------------------------------
//TODO Реализовать список для игноррируемых(неквитируемых) при квитирровании состояний
//В ShowLogic есть для этого vector noconfirms и метода для добавлении туда значений
//Здесь нужно реализовать свойство строку с перечислением состояний через ";", например, 7;6;4...
//Или перенести это свойство в сам ShowLogic - подумать
//
void ShowLogic::configure_noconfirm()
{
  if( !get_noconfirm().empty() )
  {
    std::stringstream s;
    std::vector<Glib::ustring> lines = Glib::Regex::split_simple(";", get_noconfirm());
    
    for(unsigned int i=0; i < lines.size(); i++) {
      Glib::RefPtr<Glib::Regex> regex = Glib::Regex::create("^[\\S\\d]$");
      bool check = regex->match(lines.at(i));
      if(!check)
      {
        continue;
      }
      int mode=0;
      s << lines[i].raw() << ' ';
      s >> mode;
      noconfirm_insert(mode);
    }
  }
}
// -------------------------------------------------------------------------
