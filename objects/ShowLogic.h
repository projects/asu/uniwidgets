#ifndef _SHOWLOGIC_H
#define _SHOWLOGIC_H
// -------------------------------------------------------------------------
#include "types.h"
#include <objects/AbstractLogic.h>
#include <global_macros.h>
#include <USignals.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Логика для работы с отображаемыми объектами.
 * \par
 * Логика предназначена для отображения заданных картинок,текста и т.п.
 * У каждой картинки есть свойство режим и приоритет и взависимости от этих
 * параметров определяется какая картинка должна быть показана.
 * Новое свойство для универсальности: свойство "view_type", необходимо для инициализации
 * массива отображаемых объектов states_, куда помещаются запрошенные у контейнера SimpleObject
 * указатели на его дочертие компонеты(картинки или текст). По-умолчанию запрашиваются объекты
 * с типом typeView - это может быть как картинки, так и текст, но что-то одно.В контейнере должны
 * содержатся компоненты одного типа и перемешивать картинки и текст в одном контейнере не стоит, так
 * как у контейнера может быть только одна логика отображения ShowLogic.
*/
class SimpleView;
class ShowLogic : public AbstractLogic
{
public:
  ShowLogic();
  explicit ShowLogic(Gtk::EventBox::BaseObjectType* gobject);
  virtual ~ShowLogic();

  /* Public methods */
  virtual bool show_state( const long mode, bool blink = true );  /*!< показать состояние */
  virtual void hide_state( const long mode);      /*!< скрыть состояние */
  virtual void confirm_handler(const long mode);      /*!< обработчик события квитирования */
  virtual long get_state_obj();          /*!< получить состояние виджета */
  inline void noconfirm_insert(const long mode)
  {
    if( std::find(noconfirms.begin(), noconfirms.end(), mode) == noconfirms.end() )
    {
      noconfirms.push_back(mode);
    }
  }
  
  inline bool is_noconfirm(const long mode)
  {
    if( std::find(noconfirms.begin(), noconfirms.end(), mode) != noconfirms.end() )
    {
      return true;
    }
    return false;
  }

protected:
  /* Methods */
  virtual void on_init();            /*!< инициализация логики */

  /* Variables */
  std::vector<SimpleView*> states_;          /*!< вектор с отображаемыми объектами полученными от контейнера SimplObject */
  SimpleView* current_state_;            /*!< текущий режим */
    bool is_initialize_;
    bool is_glade;
    std::vector<long> noconfirms;
    void configure_noconfirm();

  /* Methods */
  void constructor();
  virtual void init_states();          /*!< инициализация вектора с отображаемыми объектами */

  virtual long mode_to_priority(const long mode);      /*!< получить приоритет для данного режима */
  virtual bool is_priority_higher(const long new_priority);  /*!< проверить больше ли приоритет устанавливаемого режима чем приоритет текущего */
  virtual void set_state(const long mode,bool blink);    /*!< установить режим */
  SimpleView* mode_to_state(const long mode);        /*!< вернуть ссылку на отображаемый объект по данному режиму */
  long get_mode_in_max_priority();        /*!< вернуть режим с максимальным приоритетом из тех что стоят в очереди для показа */
  DISALLOW_COPY_AND_ASSIGN(ShowLogic);

  ADD_PROPERTY( view_type,long )  /*!< свойство: тип отображаемых объектов внутри виджета(Картинки либо текст). */
  ADD_PROPERTY( noconfirm, Glib::ustring ) /*!< свойство: список состояний, которые не требуют квитирования */

};

}

#endif
