#include <ConfirmSignal.h>
#include "SimpleObject.h"
#include "AbstractLogic.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
GType thresholdtype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0)) {
		static const GEnumValue values[] = {
			{ mWARNING_type, "WARMING_type", "warning" },
			{ mALARM_type, "ALARM_type", "alarm" },
			{ 0, NULL, NULL }
		};
	etype = g_enum_register_static (g_intern_static_string ("ThresholdType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UniWidgetsTypes::ThresholdType>::value_type()
{
  return thresholdtype_get_type();
}
// -------------------------------------------------------------------------
GType arrowdevicetype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0)) {
		static const GEnumValue values[] = {
			{ AD_type, "AD_type", "round 1" },
			{ EAD_type, "EAD_type", "triangular" },
			{ CAD_type, "CAD_type", "round 2" },
			{ 0, NULL, NULL }
		};
	etype = g_enum_register_static (g_intern_static_string ("ArrowDeviceType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UniWidgetsTypes::ArrowDeviceType>::value_type()
{
  return arrowdevicetype_get_type();
}
// -------------------------------------------------------------------------
/* Functor for transform the vector<Gtk::Widget*>
 * to vector<AbstractLogic*> */
template <typename T>
struct Widget2Target : unary_function <Gtk::Widget*, T*>
{
	T* operator () (Gtk::Widget* value) const
	{
		return dynamic_cast<T*>(value);
	}
};
// -------------------------------------------------------------------------
class SetLinks: public binary_function<Gtk::Widget*, bool, void>
{
public:
	void operator() (Gtk::Widget* object, bool value) const
	{
		SimpleObject *object_=dynamic_cast<SimpleObject *>(object);
		if (object_ == NULL)
			return;

		object_->set_link(value);
	}
};
// -------------------------------------------------------------------------
class ConnectLogic: public unary_function<AbstractLogic*, void>
{
public:
	void operator() (AbstractLogic* logic) const
	{
		if (logic == NULL)
			return;

		logic->connect();
	}
};
// -------------------------------------------------------------------------
class InitLogic: public binary_function<AbstractLogic*, SimpleObject*, void>
{
public:
	void operator() (AbstractLogic* logic, SimpleObject* object) const
	{
		if (logic == NULL)
			return;

		logic->init(object);
	}
};
// -------------------------------------------------------------------------
class IncreaseZOrder: public binary_function<Gtk::Widget*, SimpleObject*, void>
{
public:
	void operator() (Gtk::Widget* child, SimpleObject* object) const
	{
		object->increase_child_order(child);
	}
};
// -------------------------------------------------------------------------
class setSensitive: public binary_function<Gtk::Widget*, bool, void>
{
  public:
    void operator() (Gtk::Widget* widget, bool state) const
    {
      widget->set_sensitive(state);
    }
};
// -------------------------------------------------------------------------
void SimpleObject::constructor()
{
	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback<SimpleObject>;

	link_ = true;
	is_initialized = false;
}
// -------------------------------------------------------------------------
SimpleObject::SimpleObject() :
	Glib::ObjectBase("simpleobject")
    ,is_glade(false)
{
	constructor();
}
// -------------------------------------------------------------------------
SimpleObject::SimpleObject(GtkmmBaseType::BaseObjectType* gobject) :
	BaseType(gobject)
    ,is_glade(true)
{
	constructor();
}
// -------------------------------------------------------------------------
SimpleObject::~SimpleObject()
{
}
// -------------------------------------------------------------------------
long SimpleObject::get_value(const ObjectId sensor, const ObjectId node)
{
	if(sensor == UniWidgetsTypes::DefaultObjectId)
		return 0;

	long res = 0;

	try
	{
		if (get_connector())
			res = get_connector()->get_value(sensor, node);
	}
	catch(Exception& ex)
	{
		cerr << ex << endl;
	}
	return res;
}
// -------------------------------------------------------------------------
long SimpleObject::get_value_from_sm(const ObjectId sensor, const ObjectId node)
{
	if(sensor == UniWidgetsTypes::DefaultObjectId)
		return 0;

	long res = 0;

	try
	{
		if(!get_connector())
			return res;
		if (get_connector()->connected())
			res = get_connector()->get_value_directly(sensor, node);
	}
	catch(Exception& ex)
	{
		cerr << ex << endl;
	}
	return res;
}
// -------------------------------------------------------------------------
float SimpleObject::get_analog_value(const ObjectId sensor, const ObjectId node)
{
	if(sensor == UniWidgetsTypes::DefaultObjectId)
		return 0;

	float res = 0;

	try
	{
		if (get_connector())
			res = get_connector()->get_analog_value(sensor, node);
	}
	catch(Exception& ex)
	{
		cerr << ex << endl;
	}

	return res;
}
// -------------------------------------------------------------------------
long SimpleObject::get_value_obj(const ObjectId sensor, const ObjectId node)
{
	/*Return state this SimpleObject*/
	if(sensor != UniWidgetsTypes::DefaultObjectId)
// 		return state_object;
		return get_value(sensor,node);
	else
		return 0;
}
// -------------------------------------------------------------------------
void SimpleObject::save_value(const long value, const ObjectId sensor, const ObjectId node)
{
	try
	{
		if (get_connector())
			get_connector()->save_value(value, sensor, node);
	}
	catch(Exception& ex)
	{
		cerr << ex << endl;
	}
}
// -------------------------------------------------------------------------
void SimpleObject::set_value_obj(const long value, const ObjectId sensor, const ObjectId node)
{
	if( sensor != UniWidgetsTypes::DefaultObjectId )
	{
// 		state_object = value;
		save_value(value,sensor,node);
	}
}
// -------------------------------------------------------------------------
void SimpleObject::add_child(Gtk::Widget* child, const long type)
{
	assert( child != NULL );

	/* Check is vector for this type already exist */
	auto it = children_.find(type);
	if ( it == children_.end() )
	{
		std::vector<Gtk::Widget*> empty;

		auto result = children_.insert(pair<long, std::vector<Gtk::Widget*> >(type, empty));

		assert(result.second == true);
	}

	assert(children_.count(type) == 1);

	children_[type].push_back(child);
}
// -------------------------------------------------------------------------
void SimpleObject::on_connect() throw()
{
	UVoid::on_connect();
	vector<AbstractLogic*> logics;
	transform(children_[typeLogic].begin(),
			children_[typeLogic].end(),
			back_inserter(logics), Widget2Target<AbstractLogic>() );

	if(! is_initialized )
	{
		for_each(logics.begin(),
			logics.end(),
			bind2nd(InitLogic(), this));
		is_initialized = true;
	}

	for_each(logics.begin(),
			logics.end(),
			ConnectLogic());
}
// -------------------------------------------------------------------------
void SimpleObject::on_disconnect() throw()
{
	UVoid::on_disconnect();

	queue_draw();
}
// -------------------------------------------------------------------------
void SimpleObject::on_realize()
{
	BaseType::on_realize();

	if(! is_initialized )
	{
		vector<AbstractLogic*> logics;

		transform(children_[typeLogic].begin(),
			children_[typeLogic].end(),
			back_inserter(logics), Widget2Target<AbstractLogic>() );

		for_each(logics.begin(),
			logics.end(),
			bind2nd(InitLogic(), this));

		is_initialized = true;
	}

// 	for_each(children_[typeObject].begin(),
// 			children_[typeObject].end(),
// 			bind2nd(IncreaseZOrder(), this));
}
// -------------------------------------------------------------------------
void SimpleObject::increase_child_order(Gtk::Widget* child)
{
	/* The exposure event is propogate between the childrens
	 * according its Z-order in the container.
	 * Remove and put again the child for changing this order.
	 * For more information see -- http://library.gnome.org/devel/gtk/unstable/chap-drawing-model.html
	 */
	Gdk::Rectangle child_rect;
	Gdk::Rectangle object_rect;
	int x, y;

	child_rect = child->get_allocation();
	object_rect = get_allocation();
	/* Если дочерний виджет вернет значения по умолчанию, то
	 * установить их, иначе координаты будут глобальными следовательно необходимо вычислить новые.
	 */
	if( child_rect.get_x() != -1 && child_rect.get_y() != -1)
	{
		x = child_rect.get_x() - object_rect.get_x();
		y = child_rect.get_y() - object_rect.get_y();
	}
	else
	{
		x = child_rect.get_x()+1;
		y = child_rect.get_y()+1;
	}
	remove(*child);
	put(*child, x, y);
}
// -------------------------------------------------------------------------
bool SimpleObject::on_expose_event(GdkEventExpose* event)
{
  /* Ancestor handler */
  BaseType::on_expose_event(event);

  /* Get region to draw */
  Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
  const Gtk::Allocation alloc = get_allocation();

  Gdk::Cairo::rectangle(cr,alloc);
  cr->clip();

  /* Draw OLD disconnect effect */
  if (get_property_disconnect_effect() == 0)
    return true;
  if ( ! connected_ || ! link_ )
    UVoid::draw_disconnect_effect_1(cr, alloc);
#if 0
  /* Draw New disconnect effect */
  bool sensitive = true;
  if ( ! connected_ || ! link_ )
  {
    sensitive=false;
  }
//     GtkmmBaseType::set_state(Gtk::STATE_INSENSITIVE);
//     set_sensitive(false);
    
  std::vector<Gtk::Widget*> widgets(get_children(typeView));
    if( !widgets.empty()  )
      for_each(widgets.begin(),widgets.end(),
               bind2nd(setSensitive(),sensitive) );
    
    widgets=get_children(typeView);
    if( !widgets.empty()  )
      for_each(widgets.begin(),widgets.end(),
               bind2nd(setSensitive(),sensitive) );
    
    widgets=get_children(typeIndicator);
    if( !widgets.empty()  )
      for_each(widgets.begin(),widgets.end(),
               bind2nd(setSensitive(),sensitive) );
    
    widgets=get_children(typeCistern);
    if( !widgets.empty()  )
      for_each(widgets.begin(),widgets.end(),
               bind2nd(setSensitive(),sensitive) );

  /* End new disconnect effect */
#endif

  return true;
}
// -------------------------------------------------------------------------
void SimpleObject::set_child_property_vfunc(GtkWidget* inchild, guint property_id, const GValue* value, GParamSpec* pspec)
{
	Gtk::Widget* child = Glib::wrap(inchild);
	
	switch (property_id)
	{
		case type_prop:
			g_assert(G_VALUE_HOLDS_LONG(value));
			add_child(child, g_value_get_long(value));
			break;
		default:
			Gtk::Fixed::set_child_property_vfunc (inchild, property_id, value, pspec);
	}
}
// -------------------------------------------------------------------------
void SimpleObject::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UVoid::set_connector(connector);
}
// -------------------------------------------------------------------------
void SimpleObject::set_link(bool link)
{
	link_ = link;
	std::vector<Gtk::Widget*> objects(
			get_children(typeObject));

	if( objects.empty()  )
		return;
	
  for_each(objects.begin(),objects.end(),
				bind2nd(SetLinks(),link) );
}
// -------------------------------------------------------------------------
std::vector<Gtk::Widget*> SimpleObject::get_children(const long type)
{
	auto it = children_.find(type);
	if ( it != children_.end() )
	{
		std::vector<Gtk::Widget*> res(it->second);
		return res;
	}

	std::vector<Gtk::Widget*> res;
	return res;
}
// -------------------------------------------------------------------------
void SimpleObject::set_confirm_handler(sigc::slot<void, UMessages::MessageId, time_t>& slot, UMessages::MessageId id)
{
	UMessages::Message message =
		get_connector()->signals().get_message(id);

	if( ! message.valid() )
		return;

	/* Check to warning and alarm messages */
	if ( message.getMessageType() != msgWARNING &&
		 message.getMessageType() != msgALARM )
		return;

	get_connector()->signal_confirm().connect(slot, message.getMessageId());
}
// -------------------------------------------------------------------------
USignals::Connection SimpleObject::set_sensor_handler(const USignals::ValueChangedSlot& slot, const ObjectId sensor, const ObjectId node)
{
	USignals::Connection res;

	/* FIXME: == operator have been redifined.
	 * !=  is using instead it */
	if ( !get_connector() || sensor == UniWidgetsTypes::DefaultObjectId)
	{
		return res;
	}
	res = get_connector()->signals().connect_value_changed(
			slot,
			sensor,
			node);
	return res;
}
// -------------------------------------------------------------------------
USignals::Connection SimpleObject::set_analog_sensor_handler(const USignals::AnalogValueChangedSlot& slot, const ObjectId sensor, const ObjectId node)
{
	USignals::Connection res;

	/* FIXME: == operator have been redifined.
	 * !=  is using instead it */
	if ( !get_connector() || sensor == UniWidgetsTypes::DefaultObjectId)
		return res;

	res = get_connector()->signals().connect_analog_value_changed(
			slot,
			sensor,
			node);

	return res;
}
// -------------------------------------------------------------------------
USignals::Connection SimpleObject::set_any_message_handler(const USignals::FullMessageSlot& slot)
{
	USignals::Connection res;

	/* FIXME: == operator have been redifined.
	 * !=  is using instead it */
	if ( !get_connector() )
		return res;

	res = get_connector()->signals().connect_on_any_message_full(slot);

	return res;
}
// -------------------------------------------------------------------------
USignals::Connection SimpleObject::set_message_handler(const USignals::MessageSlot& slot, UMessages::MessageId id)
{
	USignals::Connection res;

	/* FIXME: == operator have been redifined.
	* !=  is using instead it */
	if ( !get_connector() )
		return res;

	res = get_connector()->signals().connect_on_message(slot,id);

	return res;
}
// -------------------------------------------------------------------------
// void SimpleObject::custom_set_property_callback(GObject * object, unsigned int property_id, const GValue * value, GParamSpec * param_spec)
// {
// 	if( !Glib::ObjectBase::_get_current_wrapper(object) )
// 		UObj_Wrap_New<SimpleObject>(object);
//         Glib::ObjectBase *const wrapper = Glib::ObjectBase::_get_current_wrapper(object);
//         if (!wrapper)
//                 return;
//         SimpleObject *renderer = dynamic_cast<SimpleObject *>(wrapper);
//         if (!renderer)
//                 return;
// 	Glib::custom_set_property_callback(object,property_id,value,param_spec);
// }
// // -------------------------------------------------------------------------
