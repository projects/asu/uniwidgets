#ifndef _SIMPLEOBJECT_H
#define _SIMPLEOBJECT_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <gtk/gtk.h>
#include <gtkmm.h>
#include <UDefaultFunctions.h>
#include <USignals.h>
#include <plugins.h>
#include <global_macros.h>
#include <types.h>
// -------------------------------------------------------------------------
/*! \namespace UniWidgets
 *  \brief Пространство имен содержащее классы, функции и т.п.
*/
namespace UniWidgets
{
class AbstractLogic;
/*!
 * \brief Главный контейнер.
 * \par
 * Контейнер,который обеспечивает всю работу виджета. Контейнер обеспечивает
 * взаимосвязь между логиками и отображаемыми объектами,обеспечивает взаимодействие
 * с коннектором от SharedMemory. Виджетам помещаемым в контейнер SimpleObject
 * присваивается свойство "type", таким образом происходит группировка виджетов и
 * дочерние виджеты имеют возможность получить ссылку на другой дочерний виджет.
 * Все логики для доступа к датчикам из SharedMemory просто используют соответствующие
 * методы определенные в SimpleObject.
*/
class SimpleObject : public UDefaultFunctions<Gtk::Fixed>
{
public:
  SimpleObject();
  explicit SimpleObject(GtkmmBaseType::BaseObjectType* gobject);
  virtual ~SimpleObject();

  /* Methods */
  /*! получить значение от дискретного датчика */
  long get_value(const UniWidgetsTypes::ObjectId sensor, const UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId);
  /*! получить значение от дискретного датчика прямо из коннектора */
  long get_value_from_sm(const UniWidgetsTypes::ObjectId sensor, const UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId);
  /*! получить значение от аналогового датчика */
  float get_analog_value(const UniWidgetsTypes::ObjectId sensor, const UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId);
  /*! установить значение датчика состояния для виджета */
  void set_value_obj(const long value,const UniWidgetsTypes::ObjectId sensor, const UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId);
  /*! получить значение датчика состояния для виджета */
  long get_value_obj(const UniWidgetsTypes::ObjectId sensor, const UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId);
  std::vector<Gtk::Widget*> get_children(const long type);
  /*! установить состояние соединен для дочерних контейнеров SimpleObject */
  void set_link(const bool link);
  /*! добавить дочерний виджет */
  /* FIXME: This method must be used only in the typical widgets */
  virtual void add_child(Gtk::Widget* child, const long type);

  /* Set the event handlers */
  /*! установить обработчик сообщений квитирования */
  void set_confirm_handler(sigc::slot<void, UMessages::MessageId, time_t>& slot, UMessages::MessageId id);

  /*! установить обработчик смены значений датчика */
  USignals::Connection set_sensor_handler(const USignals::ValueChangedSlot& slot,
      const UniWidgetsTypes::ObjectId sensor,
      const UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId);
  /*! установить обработчик смены значений аналогового датчика */
  USignals::Connection set_analog_sensor_handler(const USignals::AnalogValueChangedSlot& slot,
      const UniWidgetsTypes::ObjectId sensor,
      const UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId);
  /*! установить обработчик сообщений от всех датчиков */
  USignals::Connection set_any_message_handler(const USignals::FullMessageSlot& slot);
  /*! установить обработчик сообщений от конкретного датчика */
  USignals::Connection set_message_handler(const USignals::MessageSlot& slot, UMessages::MessageId id);
  /*! перерисовать дочерние виджеты контейнера */
  void increase_child_order(Gtk::Widget* child);

protected:
  /* Methods */
  /*! установить коннектор */
  virtual void set_connector(const ConnectorRef& connector) throw();

  /*! добавить виджет в map children_ c определенным типом */
  virtual void set_child_property_vfunc(GtkWidget* child,
                                                              guint property_id,
                                                              const GValue* value,
                                                              GParamSpec* pspec);
//   virtual void get_child_property_vfunc(GtkWidget* child,
//                                                               guint property_id,
//                                                               GValue* value,
//                                                               GParamSpec* pspec);

  /* Handlers */
  virtual bool on_expose_event(GdkEventExpose* event);
  virtual void on_connect() throw();
  virtual void on_disconnect() throw();
  virtual void on_realize();
  /* Variables */
  std::unordered_map<long, std::vector<Gtk::Widget*> > children_;    /*!< типизированный контейнер виджетов */

private:
  /* link_ variable is used for link sensor state
   * don't use UVoid::connected_ for this */
  bool link_;              /*! состояние связи с SharedMemory */
//   long state_object;
  bool is_initialized;            /*! переменная означающая был ли проинициализирован контейнер */
  bool is_glade;
  /* Methods */
  void constructor();
  /*! запись значения в датчик */
  void save_value(const long value,const UniWidgetsTypes::ObjectId sensor, const UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId);

  DISALLOW_COPY_AND_ASSIGN(SimpleObject);
};

}

// #ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace Glib
{
template <>
class Value<UniWidgetsTypes::ThresholdType> : public Value_Enum<UniWidgetsTypes::ThresholdType>
{
public:
  static GType value_type() G_GNUC_CONST;
};

template <>
class Value<UniWidgetsTypes::ArrowDeviceType> : public Value_Enum<UniWidgetsTypes::ArrowDeviceType>
{
public:
  static GType value_type() G_GNUC_CONST;
};
} // namespace Glib
// #endif /* DOXYGEN_SHOULD_SKIP_THIS */


/* Constants for SimpleObject_Get_Type() */
static const gchar* type = "type";
static const long type_prop = 5;

template<class T>
inline GType SimpleObject_Get_Type()
{
  static GType gtype = 0;
  if (gtype)
    return gtype;

  T* dummy = new T();
  GtkContainerClass* container_klass = GTK_CONTAINER_GET_CLASS( dummy->gobj() );
  GParamSpec* spec;
  spec = g_param_spec_long (type, type, type, LONG_MIN, LONG_MAX, -1,
      GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
  gtk_container_class_install_child_property( container_klass, type_prop, spec);
  gtype = G_OBJECT_TYPE(dummy->gobj());
  delete( dummy );
  Glib::wrap_register(gtype, &UObj_Wrap_New<T>);
  return gtype;
}

#endif
