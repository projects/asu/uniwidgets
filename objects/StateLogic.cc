#include <ConfirmSignal.h>
#include "StateLogic.h"
#include "SimpleObject.h"
#include "ShowLogic.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define STATELOGIC_STATE_AI    "state-ai"
#define STATELOGIC_STATE_OBJ_AI    "state-obj-ai"
#define STATELOGIC_MODE      "mode"
#define STATELOGIC_DETONATOR    "detonator-state"
#define STATELOGIC_INVERTMODE    "invert-mode"
#define STATELOGIC_INVERTMODE_STATE  "invert-mode-state"
#define STATELOGIC_CANBLINK    "blinking"
#define STATELOGIC_NODE      "node"
#define STATELOGIC_STATES_IGNORE  "states_ignore"
#define STATELOGIC_LOCK_VIEW    "lock-view"
// -------------------------------------------------------------------------
#define INIT_STATELOGIC_PROPERTIES() \
  state_ai(*this, STATELOGIC_STATE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,state_obj_ai(*this, STATELOGIC_STATE_OBJ_AI , UniWidgetsTypes::DefaultObjectId) \
  ,mode(*this, STATELOGIC_MODE , -1) \
  ,detntr(*this, STATELOGIC_DETONATOR , -1) \
  ,invert_mode(*this, STATELOGIC_INVERTMODE , false) \
  ,invert_mode_state(*this, STATELOGIC_INVERTMODE_STATE , mWARNING_type) \
  ,blinking(*this, STATELOGIC_CANBLINK , true) \
  ,node(*this, STATELOGIC_NODE , UniWidgetsTypes::DefaultObjectId)\
  ,states_ignore(*this, STATELOGIC_STATES_IGNORE , 0) \
  ,lock_view(*this, STATELOGIC_LOCK_VIEW , false)
// -------------------------------------------------------------------------
// Functor
template <typename T>
class InitShowLogic: public binary_function<Gtk::Widget*, T*, bool>
{
public:
  bool operator() (Gtk::Widget* widget, T* logic) const
  {
    T* Logic = dynamic_cast<T* >(widget);
    if ( Logic !=NULL )
      return true;
    return false;
  }
};
// -------------------------------------------------------------------------
void StateLogic::constructor()
{
  current_value_ = 0;
}
// -------------------------------------------------------------------------
StateLogic::StateLogic() :
  Glib::ObjectBase("statelogic")
  ,INIT_STATELOGIC_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
StateLogic::StateLogic(Gtk::EventBox::BaseObjectType* gobject) :
  AbstractLogic(gobject)
  ,INIT_STATELOGIC_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
StateLogic::~StateLogic()
{
}
// -------------------------------------------------------------------------
void StateLogic::connect()
{
  set_current_state();
}
// -------------------------------------------------------------------------
void StateLogic::on_init()
{
  assert(object_ != NULL);

  std::vector<Gtk::Widget*> widgets(
  object_->get_children(typeLogic));
  std::vector<Gtk::Widget*>::iterator it;

  if( widgets.empty() )
    return;
  it = find_if(widgets.begin(),
      widgets.end(),
      bind2nd(InitShowLogic< ShowLogic >(), logic));
  if(it != widgets.end())
  {
    ShowLogic * Logic = dynamic_cast< ShowLogic* >(*it);
    if ( Logic !=NULL )
      logic = Logic;
  }
  set_message_handler();
  set_sensor_handler();
}
// -------------------------------------------------------------------------
void StateLogic::set_current_state()
{
  assert(object_ != NULL);
  /*Если датчик установлен по-умолчанию UniWidgetsTypes::DefaultObjectId
  ,то выходим без проверки его состояния*/
  if(get_state_ai() == UniWidgetsTypes::DefaultObjectId)
    return;
  long mode=mOFF;
  long init_state = object_->get_value_from_sm(get_state_ai(),get_node());
  if(logic != NULL)
  {
    if( get_mode() != UniWidgetsTypes::DefaultObjectId && get_detntr() != UniWidgetsTypes::DefaultObjectId )
    {
      mode = ( init_state == get_detntr() ) ? get_mode() : mOFF;
    }
    else
      mode = (get_mode() == -1 || !init_state) ? init_state : get_mode();
    
    if( mode != current_value_ )
    {
      if(on_handle_error_message(mode))
      {
        bool blink=false;
        if(mode == mTRANSITIVE || mode == mUNKNOWN || logic->is_noconfirm( mode ))
          blink=true;
        if(logic->show_state(mode,blink))
        {
          logic->hide_state(current_value_);
          current_value_ = mode;
        }
      }
    }
    else
      object_->queue_draw();
    
    set_state_object();
  }
}
// -------------------------------------------------------------------------
void StateLogic::set_state_object()
{
  if(get_state_obj_ai() != UniWidgetsTypes::DefaultObjectId)
  {
    long state_obj = logic->get_state_obj();
    if(get_invert_mode() && !state_obj)
    {
      if(get_invert_mode_state() == mWARNING_type )
      {
        state_obj = uwsWARNING;
      }
      else if(get_invert_mode_state() == mALARM_type )
      {
        state_obj = uwsALARM;
      }
    }
    else if(get_invert_mode() && state_obj != uwsWaitConfirm && state_obj != uwsUnknown)
    {
      state_obj = uwsOFF;
    }
    else if( !get_invert_mode() && get_detntr() != UniWidgetsTypes::DefaultObjectId )
    {
      if(state_obj!=get_detntr())
        state_obj = uwsOFF;
    }
    /*Установить новое состояние датчика через set_value*/
    object_->set_value_obj(state_obj,get_state_obj_ai(),get_node());
  }
}
// -------------------------------------------------------------------------
void StateLogic::set_message_handler()
{
  assert(object_ != NULL);

  if( ! object_->get_connector() )
    return;
  UMessages::MessageId id(get_state_ai(), get_node(), 1);
  if( get_mode() != UniWidgetsTypes::DefaultObjectId && get_detntr() == UniWidgetsTypes::DefaultObjectId && !get_invert_mode() )
  {
    UMessages::Message msg =
        object_->get_connector()->signals().get_message(id);
    if( ! msg.valid() )
      return;
    /* Check to warning and alarm messages */
    if ( msg.getMessageType() != msgWARNING &&
      msg.getMessageType() != msgALARM )
      return;
    object_->set_message_handler(
        sigc::mem_fun(this, &StateLogic::message_handler),msg.getMessageId());
  }else
  {
    std::list<UMessages::Message > msgs =
        object_->get_connector()->signals().get_message_list(id);

    if( msgs.empty() )
      return;
    for(std::list<UMessages::Message >::iterator it = msgs.begin();it!=msgs.end();it++)
    {
      if( ! it->valid() )
        continue;
      /* Check to warning and alarm messages */
      if ( it->getMessageType() != msgWARNING &&
        it->getMessageType() != msgALARM )
        continue;
      object_->set_message_handler(
          sigc::mem_fun(this, &StateLogic::message_handler),it->getMessageId());
    }
  }
}
// -------------------------------------------------------------------------
void StateLogic::set_sensor_handler()
{
  assert(object_ != NULL);

  object_->set_sensor_handler(
      sigc::mem_fun(this, &StateLogic::sensor_handler),
      get_state_ai(),
      get_node());
  set_current_state();
}
// -------------------------------------------------------------------------
void StateLogic::set_confirm_handler(UMessages::MessageId id)
{
  assert(object_ != NULL);
  sigc::slot<void, UMessages::MessageId, time_t> slot = sigc::mem_fun(this, &StateLogic::confirm_handler);
  object_->set_confirm_handler(
      slot,
      id);
}
// -------------------------------------------------------------------------
void StateLogic::confirm_handler(UMessages::MessageId id, time_t sec)
{
  long mode = ( get_mode() == -1 || ( !id._value && get_detntr() == -1 ) ) ? id.get_long_value() : get_mode();
  logic->confirm_handler(mode);
  set_state_object();
  if(lock_view)
    object_->unlock_current();
}
// -------------------------------------------------------------------------
void StateLogic::message_handler(UMessages::MessageId id, Glib::ustring msg)
{
  if(logic!=NULL)
  {
    set_confirm_handler(id);
  }
  if(lock_view){
    object_->add_lock(*this);
  }
}
// -------------------------------------------------------------------------
void StateLogic::sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value)
{
  /*Если указано свойство mode == -1 - это значит, что датчик
  аналоговый и имеет несколько состояний. Если указано какое-то
  определенное число значит датчик дискретный и соответствует
  определенному режиму, принимаещему значения либо "0", либо "1"
  Если для свойства detntr установлено значение и в качестве mode
  указать требуемое значение состояния, то датчик считается
  дискретным. Но срабатывает не так как обычный ДД от value=true(1,2...) и
  скидывается от value=false(0), а от value=detntr и скидывается при
  value != detntr*/
  long newmode_=mOFF;
  if( get_mode() != UniWidgetsTypes::DefaultObjectId && get_detntr() != UniWidgetsTypes::DefaultObjectId && !get_invert_mode() )
  {
    newmode_ = ( value == get_detntr() ) ? get_mode() : mOFF;
  }
  else
    newmode_ = ( get_mode() == UniWidgetsTypes::DefaultObjectId || !value ) ? value : get_mode();
  /*Новая обработка ошбочных значений*/
  if(on_handle_error_message(newmode_))
  {
    if(logic->show_state(newmode_, get_blinking()))
    {
      if(newmode_ != current_value_)
        logic->hide_state(current_value_);
      current_value_ = newmode_;
      set_state_object();
    }
  }
}
// -------------------------------------------------------------------------
