#ifndef _STATELOGIC_H
#define _STATELOGIC_H
// -------------------------------------------------------------------------
#include <objects/AbstractLogic.h>
#include <USignals.h>
#include <global_macros.h>
#include <types.h>
#include <objects/ShowLogic.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Логика для работы с датчиками.
 * \par
 * Логика предназначена для работы с датчиками, получения от них значения и
 * подачи сигнала логике ShowLogic на выставление нового состояние(режима) для виджета.
 * Класс является шаблонным так как логика работы с отображаемыми объектами одинаковая,а
 * сами объекты могут быть разного типа. Для работы логики с новым типом нужно её
 * проинстанцировать так:
\code
  template class StateLogic;
\endcode
 * Логика, при инициализации в методе on_init(), ищет в контейнере объект типа ShowLogic<Typename>,
 * который вызывается для установки нового состояния виджета. Он соответственно тоже должен быть реализован
 * и его объект должен быть в контейнере. На текущий момент таких типа два: Image и Text, соответственно логика
 * датчика может работать логикой отображения картинок или текста.
*/

class StateLogic : public AbstractLogic
{
public:
  StateLogic();
  explicit StateLogic(Gtk::EventBox::BaseObjectType* gobject);
  virtual ~StateLogic();

  /* Methods */
  virtual void connect();

protected:
  /* Methods */
  virtual void on_init();          /*!< инициализация логики */

  /* Variables */
  long current_value_;          /*!< текущее состояние логики */
  ShowLogic *logic;          /*!< указатель на логику отображения визуальных объектов */

  /* Methods */
  void constructor();
  void set_message_handler();        /*!< установить обработчик АПС сообщений от датчика */
  void set_confirm_handler(UMessages::MessageId id);  /*!< установить обработчик квитирования АПС сообщений датчика */
  virtual void set_current_state();      /*!< установить текущее состояние логики */
  void set_state_object();        /*!< установить новое состояние виджета */

  /* Handlers */
  void confirm_handler(UMessages::MessageId id, time_t sec);        /*!< обработчик сообщений квитирования */
  void message_handler(UMessages::MessageId id, Glib::ustring msg);      /*!< обработчик АПС сообщений от датчика */
  /*! обработчик смены значений от датчика */
  virtual void sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value);
  void set_sensor_handler();                /*!< установить обработчик смены значений датчика */

  DISALLOW_COPY_AND_ASSIGN(StateLogic);

  /* Properties */
  ADD_PROPERTY( state_ai, UniWidgetsTypes::ObjectId )        /*!< свойство: id датчика */
  ADD_PROPERTY( state_obj_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика состояния виджета, выставляется логикой при смене состояния виджета */
  ADD_PROPERTY( mode, long )            /*!< свойство: номер режима, должен соответствовать значению типа определенного в types.h */
  ADD_PROPERTY( detntr, long )            /*! свойство: детонатор, значение датчика при котором срабатывает его выставление в состояние "включен",а все
                    остальные значения интерпретируются как "выключено" */
  ADD_PROPERTY( invert_mode, bool )          /*!< свойство: инверсия состояния дискретного датчика, т.е. срабатывает АПС при сбросе в "0",а "1" - нормальный режим работы */
  ADD_PROPERTY( invert_mode_state, UniWidgetsTypes::ThresholdType )  /*!< свойство: тип состояния "выключен" при инверсии(Warning или Alarm) */
  ADD_PROPERTY( blinking, bool )            /*!< включена/выключена способность мигать */
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )        /*!< свойство: id узла */
  ADD_PROPERTY( states_ignore, int )          /*!< свойство: значения датчика, которые нужно игнорировать */
  ADD_PROPERTY( lock_view, bool )            /*!< свойство: блокировать экран при срабатывании датчика(должен быть АПС) */
};

}
#endif
