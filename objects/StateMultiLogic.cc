#include <components/Text.h>
#include <types.h>
#include "SimpleObject.h"
#include "StateMultiLogic.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void StateMultiLogic::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback_object<StateMultiLogic>;

  current_text=NULL;
}
// -------------------------------------------------------------------------
StateMultiLogic::StateMultiLogic() :
  Glib::ObjectBase("statemultilogic")
{
constructor();
}
// -------------------------------------------------------------------------
StateMultiLogic::StateMultiLogic(StateLogic::BaseObjectType* gobject) :
  StateLogic(gobject)
{
constructor();
}
// -------------------------------------------------------------------------
StateMultiLogic::~StateMultiLogic()
{
}
// -------------------------------------------------------------------------
void StateMultiLogic::set_current_state()
{
  assert(object_ != NULL);
  
  /*Если датчик установлен по-умолчанию UniWidgetsTypes::DefaultObjectId
  ,то выходим без проверки его состояния*/
  if(get_state_ai() == UniWidgetsTypes::DefaultObjectId)
    return;

  long mode = mOFF;
  long init_state = object_->get_value_from_sm(get_state_ai(),get_node());
  if(logic != NULL)
  {
    mode = (get_mode() == UniWidgetsTypes::DefaultObjectId || !init_state) ? init_state : get_mode();

    if(current_text != NULL)
    {

      Glib::ustring mode_str = Glib::ustring::format(std::fixed, mode);

      if(on_handle_error_message(mode))
      {
        if(logic->show_state(mode, false ))
        {
          set_state_object();
          if(mode != current_value_)
            logic->hide_state(current_value_);

          current_value_ = mode;

          if(mode == mINDEF)
            current_text->set_property_text_(mode_str);
          else
            current_text->set_property_text_("");
        }
        else
        {
          set_state_object();
          if(logic->get_state_obj() != mINDEF)
          {
            logic->show_state(mINDEF, false);
            logic->hide_state(current_value_);
          }
          current_text->set_property_text_(mode_str);
          current_value_ = mode;
        }
      }
    }
    else
    {
      if( mode != current_value_ )
      {
        if( !ignore_modes.empty() && get_mode() == UniWidgetsTypes::DefaultObjectId )
        {
          IgnoreModeVector::iterator it = ignore_modes.begin();

          for(; it != ignore_modes.end(); it++)
            if(mode == *it)
              return;
        }

        if(on_handle_error_message(mode))
        {
          if(logic->show_state(mode,false))
          {
            set_state_object();
            logic->hide_state(current_value_);
            current_value_ = mode;
          }
        }
      }
    }
  }
}
// -------------------------------------------------------------------------
void StateMultiLogic::on_init()
{
  StateLogic::on_init();

  assert(object_ != NULL);
  std::vector<Gtk::Widget*> widgets(object_->get_children(typeText));

  std::vector<Gtk::Widget*>::iterator it;
  it=widgets.begin();

  if( widgets.empty() )
    return;

  Text *text;
  text = dynamic_cast<Text*>(*it);
  if( text != NULL )
    current_text=text;

  if( current_text != NULL )
    current_text->show();
}
// -------------------------------------------------------------------------
void StateMultiLogic::sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value)
{
  /*Если указано свойство mode == UniWidgetsTypes::DefaultObjectId - это значит, что датчик
  аналоговый и имеет несколько состояний. Если указано какое-то
  определенное число значит датчик дискретный и соответствует
  определенному режиму, принимаещему значения либо "0", либо "1"*/
  assert(object_ != NULL);
  
  long newmode_ = mOFF;
  newmode_ = ( get_mode() == UniWidgetsTypes::DefaultObjectId || !value ) ? value : get_mode();

  if(current_text != NULL)
  {

    Glib::ustring mode_str = Glib::ustring::format(std::fixed, newmode_);

    /*Новая обработка ошибочных значений*/
    if(on_handle_error_message(newmode_))
    {
      if(logic->show_state(newmode_, value?true:false ))
      {
        set_state_object();
        if(newmode_ != current_value_)
          logic->hide_state(current_value_);

        current_value_ = newmode_;

        if(newmode_ == mINDEF)
          current_text->set_property_text_(mode_str);
        else
          current_text->set_property_text_("");
      }
      else
      {
        set_state_object();
        if(logic->get_state_obj() != mINDEF)
        {
          logic->show_state(mINDEF, value?true:false);
          logic->hide_state(current_value_);
        }
        current_text->set_property_text_(mode_str);
        current_value_ = newmode_;
      }
    }
    object_->increase_child_order(current_text);
  }
  else
  {
    if( !ignore_modes.empty() && get_mode() == UniWidgetsTypes::DefaultObjectId )
    {
      IgnoreModeVector::iterator it = ignore_modes.begin();

      for(; it!=ignore_modes.end(); it++)
      {
        if(newmode_ == *it)
          return;
      }
    }

    if( !blinkoff_modes.empty() )
    {
      BlinkOffModeVector::iterator it = blinkoff_modes.begin();

      for(; it!=blinkoff_modes.end(); it++)
        if(newmode_ == *it)
          break;

      /*Новая обработка ошибочных значений*/
      if(on_handle_error_message(newmode_))
      {
        if(logic->show_state(newmode_, false))
        {
          set_state_object();
          if(newmode_ != current_value_)
            logic->hide_state(current_value_);
          current_value_ = newmode_;
        }
      }
    }
    else
    {
      /*Новая обработка ошибочных значений*/
      if(on_handle_error_message(newmode_))
      {
        if(logic->show_state(newmode_, value?true:false))
        {
          set_state_object();
          if(newmode_ != current_value_)
            logic->hide_state(current_value_);
          current_value_ = newmode_;
        }
      }
    }
  }
}
// -------------------------------------------------------------------------
void StateMultiLogic::add_ignoremode(long mode)
{
  ignore_modes.push_back(mode);
}
// -------------------------------------------------------------------------
void StateMultiLogic::add_blinkoff_mode(long mode)
{
  blinkoff_modes.push_back(mode);
}
// -------------------------------------------------------------------------
