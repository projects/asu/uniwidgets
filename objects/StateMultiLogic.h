#ifndef _STATEMULTILOGIC_H
#define _STATEMULTILOGIC_H
// -------------------------------------------------------------------------
#include <objects/StateLogic.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class Text;
/*!
 * \brief Логика для работы с картинками.
 * \par
 * Логика предназначена для формирования запроса на выставление режима виджета
 * к логике отображающей картинки(ShowImageLogic) в зависимости от значения датчика
 * привязанного к логике.
*/
class StateMultiLogic : public StateLogic
{
public:
	StateMultiLogic();
	explicit StateMultiLogic(StateLogic::BaseObjectType* gobject);
	virtual ~StateMultiLogic();

	typedef std::vector<long> IgnoreModeVector;
	typedef std::vector<long> BlinkOffModeVector;
	void add_ignoremode(long);
	void add_blinkoff_mode(long);

protected:
	void on_init();

private:
	void sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value);
	void set_current_state();
	void constructor();
	IgnoreModeVector ignore_modes;
	BlinkOffModeVector blinkoff_modes;

	Text *current_text;
	DISALLOW_COPY_AND_ASSIGN(StateMultiLogic);
};

}
#endif
