#include <ConfirmSignal.h>
#include "ThresholdAnalogLogic.h"
#include "SimpleObject.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define INDICATORANALOGLOGIC_VALUE_AI    "value-ai"
#define INDICATORANALOGLOGIC_MODE    "mode"
#define INDICATORANALOGLOGIC_THRESHOLDON_AI  "threshold-on-ai"
#define INDICATORANALOGLOGIC_THRESHOLDOFF_AI  "threshold-off-ai"
#define INDICATORANALOGLOGIC_THRESHOLDON  "threshold-on"
#define INDICATORANALOGLOGIC_THRESHOLDOFF  "threshold-off"
#define INDICATORANALOGLOGIC_NODE    "node"
// -------------------------------------------------------------------------
#define INIT_INDICATORANALOGLOGIC_PROPERTIES() \
  value_ai(*this, INDICATORANALOGLOGIC_VALUE_AI, UniWidgetsTypes::DefaultObjectId)\
  ,mode(*this, INDICATORANALOGLOGIC_MODE, -1)\
  ,threshold_on_ai(*this, INDICATORANALOGLOGIC_THRESHOLDON_AI, UniWidgetsTypes::DefaultObjectId) \
  ,threshold_off_ai(*this, INDICATORANALOGLOGIC_THRESHOLDOFF_AI, UniWidgetsTypes::DefaultObjectId) \
  ,threshold_on(*this, INDICATORANALOGLOGIC_THRESHOLDON, 0)\
  ,threshold_off(*this, INDICATORANALOGLOGIC_THRESHOLDOFF, 0)\
  ,node(*this, INDICATORANALOGLOGIC_NODE , UniWidgetsTypes::DefaultObjectId)
// -------------------------------------------------------------------------
template <typename T>
struct Widget2Target : unary_function <Gtk::Widget*,bool >
{
  bool operator () (Gtk::Widget* widget) const
  {
    ShowLogic* result = dynamic_cast<ShowLogic*>(widget);
    if(result)
      return true;
    else
      return false;
  }
};
// -------------------------------------------------------------------------
ThresholdAnalogLogic::ThresholdAnalogLogic() :
  Glib::ObjectBase("thresholdanaloglogic")
  ,INIT_INDICATORANALOGLOGIC_PROPERTIES()
{
}
// -------------------------------------------------------------------------
ThresholdAnalogLogic::ThresholdAnalogLogic(Gtk::EventBox::BaseObjectType* gobject) :
  AbstractLogic(gobject)
  ,INIT_INDICATORANALOGLOGIC_PROPERTIES()
{
}
// -------------------------------------------------------------------------
ThresholdAnalogLogic::~ThresholdAnalogLogic()
{
}
// -------------------------------------------------------------------------
void ThresholdAnalogLogic::connect()
{
  // Set Initial State
  set_current_state();
}
// -------------------------------------------------------------------------
void ThresholdAnalogLogic::on_init()
{
  assert(object_ != NULL);

  std::vector<Gtk::Widget*> widgets(
      object_->get_children(typeLogic));

  if( widgets.empty() )
    return;

#warning Можно цикл поменять на функтор или сделать метод возвращающий виджет по точному типу!
  for (unsigned int i = 0; i < widgets.size(); i++)
  {
    ShowLogic* ImageLogic = dynamic_cast<ShowLogic*>(widgets[i]);

    if ( ImageLogic !=NULL )
    {
      imagelogic = ImageLogic;
    }
  }

  if( ( get_threshold_on() - get_threshold_off() ) > 0 )
    threshold_mode_ = true;//HIGH
  else
    threshold_mode_ = false;//LOW
  
  state_ = false;

  set_sensor_handler();
}
// -------------------------------------------------------------------------
void ThresholdAnalogLogic::set_current_state()
{
  assert(object_ != NULL);

  if(get_threshold_on_ai() != UniWidgetsTypes::DefaultObjectId)
  {
    long cur_value = object_->get_value_from_sm(get_value_ai(),get_node());
    set_threshold_on(cur_value);
  }
  /*Если задан датчик, то граница отпускания выставляется по нему*/
  if(get_threshold_off_ai() != UniWidgetsTypes::DefaultObjectId)
  {
    long cur_value = object_->get_value_from_sm(get_value_ai(),get_node());
    set_threshold_off(cur_value);
  }

  if( ( get_threshold_on() - get_threshold_off() ) > 0 )
    threshold_mode_ = true;//HIGH
  else
    threshold_mode_ = false;//LOW

  if(imagelogic!=NULL)
  {
    long init_value = object_->get_value_from_sm(get_value_ai(),get_node());
    process_state(init_value);
  }
}
// -------------------------------------------------------------------------
void ThresholdAnalogLogic::set_sensor_handler()
{
  assert(object_ != NULL);

  object_->set_sensor_handler(
      sigc::mem_fun(this, &ThresholdAnalogLogic::sensor_handler),
      get_value_ai(),
      get_node());

  /*Если задан датчик, то граница срабатывания выставляется по нему*/
  if(get_threshold_on_ai() != UniWidgetsTypes::DefaultObjectId)
  {
    object_->set_sensor_handler(
        sigc::bind(sigc::mem_fun(this, &ThresholdAnalogLogic::thresholdcg_handler),1),
        get_threshold_on_ai(),
        get_node());
  }
  /*Если задан датчик, то граница отпускания выставляется по нему*/
  if(get_threshold_off_ai() != UniWidgetsTypes::DefaultObjectId)
  {
    object_->set_sensor_handler(
        sigc::bind(sigc::mem_fun(this, &ThresholdAnalogLogic::thresholdcg_handler),0),
        get_threshold_off_ai(),
        get_node());
  }
}
// -------------------------------------------------------------------------
void ThresholdAnalogLogic::thresholdcg_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value, int flag)
{
  /*Если порог срабатывания изменен, то*/
  if(flag == 1)
    set_threshold_on(value);
  else if(!flag)
    set_threshold_off(value);

  /*Проверка типа порога*/
  if( ( get_threshold_on() - get_threshold_off() ) > 0 )
    threshold_mode_ = true;//HIGH
  else
    threshold_mode_ = false;//LOW

  /*Проверка текущего значения с новыми порогами*/
  long value_ = object_->get_value(get_value_ai(),get_node());
  process_state(value_);
}
// -------------------------------------------------------------------------
void ThresholdAnalogLogic::sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value)
{
  process_state(value);
}
// -------------------------------------------------------------------------
void ThresholdAnalogLogic::process_state(const long value)
{
  if(threshold_mode_)
  {
    if( value > get_threshold_on() )
    {
      if(!state_) set_state( true );
    }
    else
    {
      if( value < get_threshold_off() )
      {
        if(state_) set_state( false );
      }
    }
  }
  else
  {
    if( value < get_threshold_on() )
    {
      if(!state_) set_state( true );
    }
    else
    {
      if( value > get_threshold_off() )
      {
        if(state_) set_state( false );
      }
    }
  }
}
// -------------------------------------------------------------------------
void ThresholdAnalogLogic::set_state(bool state)
{
  if(object_!=NULL && imagelogic != NULL)
  {
    state_=state;
    if(state_)
      imagelogic->show_state( get_mode() , false);
    else
      imagelogic->hide_state( get_mode() );
  }
}
// -------------------------------------------------------------------------
