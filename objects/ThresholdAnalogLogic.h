#ifndef _INDICATORANALOGLOGIC_H
#define _INDICATORANALOGLOGIC_H
// -------------------------------------------------------------------------
#include "types.h"
#include <objects/AbstractLogic.h>
#include <objects/ShowLogic.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Логика для работы с аналоговыми порогами.
 * \par
 * Логика предназначена для выставления порогов от аналогового датчика.
 * Порого выставляется при превышении значения порога threshold_on и сбрасывается при
 * снижении ниже порога "отпускания" threshold_off. Тип порога(верхний, нижний) определяется
 * путем сравнения двух значений threshold_on и threshold_off: если первое больше - порог верхний,
 * второе больше - нижний. Также предусмотрел механизм динамического порога когда порог изменяется
 * с помощью аналогового датчика(как значение срабатывая, так и отпускания). И вместе со значением порога может
 * меняться и тип порог(верхний или нижний), так каждый раз при смене значения порога проверяется
 * проверяется соответствие определенному типу порога.
*/
class ThresholdAnalogLogic : public AbstractLogic
{
public:
  ThresholdAnalogLogic();
  explicit ThresholdAnalogLogic(Gtk::EventBox::BaseObjectType* gobject);
  virtual ~ThresholdAnalogLogic();

  virtual void connect();
protected:
  /* Methods */
  virtual void on_init();

private:
  /* Variables */
  ShowLogic *imagelogic;          /*!< указатель на логику управляющую отображаемыми объектами(картинками,...) */
  bool threshold_mode_;            /*!< тип порога(верхний или нижний) */
  bool state_;              /*!< состояние порога(включен или выключен) */
  
  /* Methods */
  void set_state(bool state);          /*!< установить состояние порога(включен или выключен) */
  void set_current_state();          /*!< установить текущее состояние порога */
  void process_state(const long value);        /*!< обработка значений от датчика */
  /* Handlers */
  void sensor_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value);
  /*! обработка значений от датчика задающего значение порога */
  void thresholdcg_handler(UniWidgetsTypes::ObjectId sensor, UniWidgetsTypes::ObjectId node, long value, int flag);

  void set_sensor_handler();

  DISALLOW_COPY_AND_ASSIGN(ThresholdAnalogLogic);
  /* Properties */
  ADD_PROPERTY( value_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id аналогового датчика */
  ADD_PROPERTY( mode, long )          /*!< свойство: режим(mode) порога */
  ADD_PROPERTY( threshold_on_ai, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика, задающего значение порога срабатывания */
  ADD_PROPERTY( threshold_off_ai, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика, задающего значение порога отпускания */
  ADD_PROPERTY( threshold_on, long )        /*!< свойство: значение порога срабатывания */
  ADD_PROPERTY( threshold_off, long )        /*!< свойство: значение порога отпускания */
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )      /*!< свойство: id узла */
};

}
#endif
