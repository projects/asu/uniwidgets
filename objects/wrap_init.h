#ifndef OBJECTS_WRAP_INIT_H_INCLUDED
#define OBJECTS_WRAP_INIT_H_INCLUDED

namespace UniWidgets
{

void wrap_init_objects();

}

#endif /* !OBJECTS_WRAP_INIT_H_INCLUDED */
