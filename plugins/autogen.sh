#!/bin/sh

GEN=$1
case "$GEN" in
glade)TEMPL='\2(\1,\L\1)';;
libglade)TEMPL='glade_register_widget(\3<\1>(),\4,\5,NULL);';;
esac
PATTERN="s/^\([^,]*\),\([^,]*\),\([^,]*\),\([^,]*\),\([^;]*\);/${TEMPL}/p;g;"
echo '//This file is auto generated!'
#echo cat $2 | sed -ne "$PATTERN"
cat $2 | grep -v "^#"| sed -ne "$PATTERN"

