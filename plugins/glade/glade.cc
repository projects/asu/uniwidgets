#include <macros.h>
#include <gtkmm.h>

extern "C" void glade_init()
{
	Gtk::Main::init_gtkmm_internals();
}

extern "C" void uproxywidget_postcreate2(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)
{
	uwrap<UProxyWidget>(object);
	gtk_container_add(GTK_CONTAINER(object),glade_placeholder_new());
}

extern "C" void ucontainer_postcreate2(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)
{
	uwrap<UContainer>(object);
	gtk_container_add(GTK_CONTAINER(object),glade_placeholder_new());
}
//----------------------------------------------------------------------------------
extern "C" void upopupbox_packing_changed(GtkWidget* widget, const GParamSpec* pspec)
{
	if( !strcmp (pspec->name, "x") || !strcmp (pspec->name, "y") )
	{
		GladeWidget *gw = glade_widget_get_from_gobject(widget);
		int prop_value=0;
		if(glade_widget_pack_property_get(gw, pspec->name, &prop_value))
		{
			UPopupBox* uw = uwrap<UPopupBox>(G_OBJECT(widget));
//			std::cout<<gtk_widget_get_name(widget)<<"::on_packing_changed name="<<pspec->name<<" value="<<prop_value<<std::endl;
			if(uw && !strcmp (pspec->name, "y"))
				uw->set_widget_y(prop_value);
			else if(uw && !strcmp (pspec->name, "x"))
				uw->set_widget_x(prop_value);
		}
	}
}
//----------------------------------------------------------------------------------
extern "C" void upopupbox_postcreate2(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)
{
	//	std::cout<< glade_widget_get_name(glade_widget_get_from_gobject(object)) <<"::upopupbox_postcreate2()"<<std::endl;
	UPopupBox* uw = uwrap<UPopupBox>(object);
	if (uw)
		uw->set_is_glade_editor(true);
	uw->connect_packing_changed_function(&upopupbox_packing_changed);
	gtk_container_add(GTK_CONTAINER(object),glade_placeholder_new());
	if (reason == GLADE_CREATE_USER)
	{
		GladeWidget* gw = glade_widget_get_from_gobject(object);
		gw_set_default(gw);
	}
}
//----------------------------------------------------------------------------------
extern "C" void uindicatorcontainer_packing_changed(GtkWidget* widget, const GParamSpec* pspec)
{
	GladeWidget* gw = glade_widget_get_from_gobject(G_OBJECT(widget));
// 	std::cout<< glade_widget_get_name(gw) <<"::uindicatorcontainer_packing_changed()"<<std::endl;
	std::string prop_name(pspec->name);
	GladeProperty* prop = glade_widget_get_property(gw, prop_name.c_str());
	glade_property_set_value(prop, prop->value);
	if( prop_name == "ch-sensor-name" )
	{
	}
	else if( prop_name == "ch-node-name" )
	{
	}
	else if( prop_name == "ch-sensor-id" )
	{
	}
	else if( prop_name == "ch-node-id" )
	{
	}
	else if( prop_name == "chblinktime" )
	{
	}
}
//----------------------------------------------------------------------------------
extern "C" void uindicatorcontainer_postcreate2(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)
{
	GladeWidget* gw = glade_widget_get_from_gobject(object);
// 	std::cout<< glade_widget_get_name(gw) <<"::uindicatorcontainer_postcreate2()"<<std::endl;
	UIndicatorContainer* uw = uwrap<UIndicatorContainer>(object);
	if (uw)
		uw->set_is_glade_editor(true);
// 	gtk_container_add(GTK_CONTAINER(object),glade_placeholder_new());
	uw->connect_packing_changed_function(&uindicatorcontainer_packing_changed);
	if (reason == GLADE_CREATE_USER)
	{
		gw_set_default(gw);
	}
}
//----------------------------------------------------------------------------------
extern "C" void uindicatorcontainer_realize_child(GtkWidget *child)
{
	GladeWidget* gw = glade_widget_get_from_gobject(G_OBJECT(child));
// 	std::cout<< glade_widget_get_name(gw) <<"::uindicatorcontainer_event_child()"<<std::endl;
	gw_set_default(gw);
}
//----------------------------------------------------------------------------------
extern "C" gboolean uindicatorcontainer_timer(gpointer data)
{
	GtkWidget *child = GTK_WIDGET(data);
	GladeWidget* gw = glade_widget_get_from_gobject(G_OBJECT(child));
// 	std::cout<< glade_widget_get_name(gw) <<"::uindicatorcontainer_timer()"<<std::endl;
// 	gw_set_default(gw);

	GladeProperty* prop;
	prop = glade_widget_get_pack_property(gw, "ch-sensor-name");
	glade_widget_property_reset (gw, prop->klass->id);
	prop = glade_widget_get_pack_property(gw, "ch-node-name");
	glade_widget_property_reset (gw, prop->klass->id);
	prop = glade_widget_get_pack_property(gw, "ch-sensor-id");
	glade_widget_property_reset (gw, prop->klass->id);
	prop = glade_widget_get_pack_property(gw, "ch-node-id");
	glade_widget_property_reset (gw, prop->klass->id);
	prop = glade_widget_get_pack_property(gw, "chvalue");
	glade_widget_property_reset (gw, prop->klass->id);
	prop = glade_widget_get_pack_property(gw, "chpriority");
	glade_widget_property_reset (gw, prop->klass->id);
	prop = glade_widget_get_pack_property(gw, "chblink");
	glade_widget_property_reset (gw, prop->klass->id);
	prop = glade_widget_get_pack_property(gw, "chblinktime");
	glade_widget_property_reset (gw, prop->klass->id);
	prop = glade_widget_get_pack_property(gw, "chconfirm");
	glade_widget_property_reset (gw, prop->klass->id);

	return false;
}
//----------------------------------------------------------------------------------
extern "C" void uindicatorcontainer_replace_child(GladeWidgetAdaptor *adaptor, GtkWidget *container, GtkWidget *current, GtkWidget *new_widget)
{
	GladeWidget* gw = glade_widget_get_from_gobject(G_OBJECT(container));
// 	std::cout<< glade_widget_get_name(gw) <<"::uindicatorcontainer_replace_child()"<<std::endl;
	
// 	GParamSpec **param_spec;
// 	GladePropertyClass *pclass;
// 	GValue *value;
// 	guint nproperties;
// 	guint i;
// 
// 	g_return_if_fail (gtk_widget_get_parent (current) == container);
// 
// 	param_spec = gtk_container_class_list_child_properties(G_OBJECT_GET_CLASS (container), &nproperties);
// // 	value = g_malloc0 (sizeof (GValue) * nproperties);
// 
// 	for (i = 0; i < nproperties; i++)
// 	{
// 		g_value_init (&value[i], param_spec[i]->value_type);
// 		gtk_container_child_get_property(GTK_CONTAINER (container), current, param_spec[i]->name, &value[i]);
// 	}

	GValue pos = {0};
	g_value_init(&pos, G_TYPE_INT);
	gtk_container_child_get_property( GTK_CONTAINER(container),current, "position", &pos);
// 	std::cout<<glade_widget_get_name(gw)<<"::uindicatorcontainer_replace_child() GLADE_IS_PLACEHOLDER prop_name = 'position' prop_val = '"<<g_value_get_int(&pos)<<"'"<<std::endl;
	
	gtk_container_remove (GTK_CONTAINER (container), current);
	gtk_container_add (GTK_CONTAINER (container), new_widget);
	gtk_container_child_set_property( GTK_CONTAINER(container),new_widget, "position", &pos);

	UIndicatorContainer* uw = uwrap<UIndicatorContainer>(G_OBJECT(container));
	uw->set_property_page(!g_value_get_int(&pos));
// 	uw->set_property_page(g_value_get_int(&pos));
	glade_property_set_value(glade_widget_get_property(gw, "page"), &pos);

// 	gtk_box_pack_start_defaults (GTK_BOX(container), new_widget);
	
// 	for (i = 0; i < nproperties; i++)
// 	{
// 	/* If the added widget is a placeholder then we
// 	 * want to keep all the "tranfer-on-paste" properties
// 	 * as default so that it looks fresh (transfer-on-paste
// 	 * properties dont effect the position/slot inside a
// 	 * contianer)
// 	 */
// 		if (GLADE_IS_PLACEHOLDER (new_widget))
// 		{
// 			pclass = glade_widget_adaptor_get_pack_property_class(adaptor, param_spec[i]->name);
// 
// // 			if (pclass && glade_property_class_transfer_on_paste (pclass))
// // 				continue;
// 		}
// 
// 		gtk_container_child_set_property(GTK_CONTAINER (container), new_widget, param_spec[i]->name,&value[i]);
// 	}
// 
// 	for (i = 0; i < nproperties; i++)
// 		g_value_unset (&value[i]);
// 
// 	g_free (param_spec);
// 	g_free (value);
	
	if (!GLADE_IS_PLACEHOLDER (new_widget))
	{
// 		GladeProject *gproject = glade_widget_get_project(gw);
// 		
// 		GValue value;
// 		g_assert (!G_VALUE_HOLDS_STRING(&value));
// 		g_value_init(&value,G_TYPE_STRING);
// 		g_value_set_static_string(&value,"DefaultObjectId");
// // 		gtk_container_child_set_property(GTK_CONTAINER (container), new_widget, "ch-sensor-name", &value);
// 		std::cout<<glade_widget_get_name(ngw)<<"::uindicatorcontainer_replace_child() prop_name = 'ch-sensor-name' prop_val = '"<<g_value_get_string(&value)<<"'"<<std::endl;
// // 		glade_widget_adaptor_child_set_property(adaptor, G_OBJECT(container), G_OBJECT(new_widget), "ch-sensor-name", &value);
// 		GladeProperty* prop = glade_widget_get_pack_property(ngw, "ch-sensor-name");
// 		if(GLADE_IS_PROPERTY(prop))
// 		{
// // 			std::cout<<glade_widget_get_name(ngw)<<"::uindicatorcontainer_replace_child() prop_name = 'ch-sensor-name' prop_val = '"<<g_value_get_string(&value)<<"'"<<std::endl;
// // 			glade_property_set_value(prop, &value);
// 			glade_property_set_value(prop, glade_property_class_make_gvalue_from_string(prop->klass, "ch-sensor-name", gproject, gw) );
// // 			glade_property_sync(prop);
// 		}
// // 		glade_widget_child_set_property(gw,ngw,"ch-sensor-name", &value);
// 		g_value_unset(&value);
// 
// 		g_assert (!G_VALUE_HOLDS_STRING(&value));
// 		g_value_init(&value,G_TYPE_STRING);
// 		g_value_set_static_string(&value,"LocalhostNode");
// // 		gtk_container_child_set_property(GTK_CONTAINER (container), new_widget, "ch-node-name", &value);
// 		std::cout<<glade_widget_get_name(ngw)<<"::uindicatorcontainer_replace_child() prop_name = 'ch-node-name' prop_val = '"<<g_value_get_string(&value)<<"'"<<std::endl;
// // 		glade_widget_adaptor_child_set_property(adaptor, G_OBJECT(container), G_OBJECT(new_widget), "ch-node-name", &value);
// 		glade_widget_child_set_property(gw,ngw,"ch-node-name", &value);
// 		g_value_unset(&value);
// // 		glade_widget_rebuild(ngw);
		
// 		g_signal_connect(new_widget, "event", (GCallback)uindicatorcontainer_event_child, container);
// 		g_signal_connect_swapped(new_widget, "realize", (GCallback)gw_set_default,ngw/*, container*/);
// 		g_signal_connect_after (new_widget, "realize",G_CALLBACK(uindicatorcontainer_realize_child), NULL);
		g_timeout_add(100,uindicatorcontainer_timer,new_widget);
	}
}
//----------------------------------------------------------------------------------
extern "C" void usvgbutton_postcreate2(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)
{
	//	std::cout<< glade_widget_get_name(glade_widget_get_from_gobject(object)) <<"::usvgbutton_postcreate2()"<<std::endl;
	USVGButton* uw = uwrap<USVGButton>(object);
	if (uw)
		uw->set_is_glade_editor(true);
	if (reason == GLADE_CREATE_USER)
	{
		GladeWidget* gw = glade_widget_get_from_gobject(object);
		gw_set_default(gw);
	}
}
//----------------------------------------------------------------------------------
extern "C" void unisvgdialogbutton_postcreate2(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)
{
	UniSVGDialogButton* uw = uwrap<UniSVGDialogButton>(object);
	GladeWidget* gw = glade_widget_get_from_gobject(object);
	GladeProject* gp = glade_widget_get_project(gw);
	const gchar* path = glade_project_get_path(gp);
	if(path == NULL)
		uw->set_own_xml_path("");
	else
		uw->set_own_xml_path(path);
	GladeWidget* gwp = gw;
	while(glade_widget_get_parent(gwp))
	{
		gwp = glade_widget_get_parent(gwp);
	};
	uw->set_own_parent_window_name(glade_widget_get_name(gwp));
	GtkWidget *parent_widget = GTK_WIDGET(glade_widget_get_object(gwp));
	if(parent_widget)
	{
		Gtk::Widget* widget = Glib::wrap(parent_widget);
		if(widget)
		{
			Gtk::Window* win = dynamic_cast<Gtk::Window*>(widget);
			if(win)
				uw->set_own_parent_window(win);
		}
	}
	if (uw)
		uw->set_is_glade_editor(true);
	std::cout<< glade_widget_get_name(gw) <<"::unisvgdialogbutton_postcreate2() xml_path="<<glade_project_get_path(gp)<<" parent_window_name="<<glade_widget_get_name(gwp)<<std::endl;
	if (reason == GLADE_CREATE_USER)\
	{
		gw_set_default(gw);
	}
}
//----------------------------------------------------------------------------------
extern "C" void uslidenotebook_write_child(GladeWidgetAdaptor *adaptor, GladeWidget *widget, GladeXmlContext *context, GladeXmlNode *node)
{
	std::cout<< glade_widget_get_name(widget) <<":uslidenotebook_write_child() !!!!!!!!!!!!!"<<std::endl;
}
//----------------------------------------------------------------------------------
extern "C" void uslidenotebook_add_child(GladeWidgetAdaptor *adaptor, GObject *object, GObject *child)
{
	GladeWidget *parent = glade_widget_get_from_gobject(object);
	GladeWidget *gwidget = glade_widget_adaptor_create_internal(parent, child, "tab", "slidenotebook", false, GLADE_CREATE_USER);

	Glib::RefPtr<Glib::Object> w = Glib::wrap(object);
	Glib::RefPtr<Glib::Object> ch = Glib::wrap(child);
	std::cout<< dynamic_cast<USlideNotebook*>(w.operator->())->get_name() <<":uslidenotebook_add_child()"<<std::endl;
	dynamic_cast<USlideNotebook*>(w.operator->())->add_child(dynamic_cast<Gtk::Widget*>(ch.operator->()));

//	GladeWidget *gwidget = glade_widget_get_from_gobject (child);
//	glade_widget_adaptor_write_child (adaptor, gwidget);
}
//----------------------------------------------------------------------------------

extern "C" void glade_text_set_property (GladeWidgetAdaptor * adaptor,
                         GObject * object,
                         const gchar * id, const GValue * value)
{
//   GladeImageEditMode mode;
  GladeWidget *gwidget = glade_widget_get_from_gobject (object);
  GladeProperty *property = glade_widget_get_property (gwidget, id);

  if (!strcmp (id, "digital-mode"))
  {
//     glade_widget_property_set_sensitive (gwidget, "text", FALSE,
//                                          NOT_SELECTED_MSG);
//     glade_widget_property_set_sensitive (gwidget, "buffer", FALSE,
//                                          NOT_SELECTED_MSG);
    //
//     if (g_value_get_boolean (value))
//       glade_widget_property_set_sensitive (gwidget, "buffer", TRUE, NULL);
//     else
//       glade_widget_property_set_sensitive (gwidget, "text", TRUE, NULL);
  }
//   else if (GPC_VERSION_CHECK
//            (glade_property_get_class (property), gtk_major_version, gtk_minor_version + 1))
  else
    GWA_GET_CLASS (GTK_TYPE_WIDGET)->set_property (adaptor, object, id, value);
}

extern "C" void
glade_simpleobject_layout_sync_size_requests (GtkWidget * widget)
{
  GList *children, *l;

  if ((children = gtk_container_get_children (GTK_CONTAINER (widget))) != NULL)
  {
    for (l = children; l; l = l->next)
    {
      GtkWidget *child = (GtkWidget *)l->data;
      GladeWidget *gchild = glade_widget_get_from_gobject (child);
      gint width = -1, height = -1;

      if (!gchild)
        continue;

      glade_widget_property_get (gchild, "width-request", &width);
      glade_widget_property_get (gchild, "height-request", &height);

      gtk_widget_set_size_request (child, width, height);

    }
    g_list_free (children);
  }
}

extern "C" void
glade_simpleobject_layout_post_create (GladeWidgetAdaptor * adaptor,
                                                                GObject * object, GladeCreateReason reason)
{
  /* Sync up size request at project load time */
  if (reason == GLADE_CREATE_LOAD)
  {
    g_signal_connect_after (object, "realize",
                                          G_CALLBACK
                                              (glade_simpleobject_layout_sync_size_requests), NULL);
  }

//   g_signal_connect (object, "draw",
//                                 G_CALLBACK (glade_gtk_fixed_layout_draw), NULL);
}
