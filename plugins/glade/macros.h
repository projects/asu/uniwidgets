#ifndef _MACROS_GLADE_H
#define _MACROS_GLADE_H

#include <gladeui/glade.h>
#include <uwidgets/UWidgets.h>
#include <usvgwidgets/USVGWidgets.h>
#include <objects/Objects.h>
#include <components/Components.h>
#include <typical/Typical.h>

using namespace UniWidgets;

static void gw_set_default(GladeWidget *gw)
{
/* HACK to set default values from catalog
* It isn't set for some reason
* I hope later we will find the problem
*/
  if(!gw)
	  return;

  GList *l;

  for (l = gw->properties; l && l->data; l = l->next)
  {
    try
    {
		GladeProperty *prop  = GLADE_PROPERTY(l->data);
//		glade_property_reset(prop);
		glade_widget_property_reset (gw, prop->klass->id);
	}
	catch(...)
	{
		std::cout<<"Невозможно записать дефолт в пропертис!!!"<<std::endl;
	}
  }
//   for (l = gw->packing_properties; l && l->data; l = l->next)
//   {
// 	  try
// 	  {
// 		GladeProperty *prop  = GLADE_PROPERTY(l->data);
// //		glade_property_reset(prop);
// 		glade_widget_property_reset (gw, prop->klass->id);
// 		std::cout<<gw->name<<"::дефолт в упаковочный пропертис prop_name = '"<<prop->klass->id<<"' prop_val = '"<<const_cast<gchar*>( glade_property_class_make_string_from_gvalue(prop->klass, prop->value, GLADE_PROJECT_FORMAT_LIBGLADE) )<<"'"<<std::endl;
// 	  }
// 	  catch(...)
// 	  {
// 		  std::cout<<"Невозможно записать дефолт в упаковочный пропертис!!!"<<std::endl;
// 	  }
//   }
}

#define GLADE_UVOID_FUNCS(uname, cname)\
extern "C" GType cname##_get_type(); \
extern "C" void cname##_wrap_new(GObject* object);\
\
void cname##_init()\
{\
  UObj_Get_Type<uname>();\
}\
\
extern "C" GType gtkmm___custom_object_##cname##_get_type()\
{\
  GType type = UObj_Get_Type<uname>();\
  return type;\
}\
\
extern "C" void cname##_postcreate(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)\
{\
  uname* uw = uwrap<uname>(object);\
  if (uw) \
    uw->set_is_glade_editor(true); \
    GladeWidget* gw = glade_widget_get_from_gobject(object);\
    std::cout<< glade_widget_get_name(gw) <<"::postcreate()"<<std::endl;\
  gw_set_default(gw);\
 }

#define GLADE_DEFAULT_FUNCS(name, cname)\
extern "C" GType cname##_get_type(); \
extern "C" void cname##_wrap_new(GObject* object);\
\
void cname##_init()\
{\
  Obj_Get_Type<name>();\
}\
\
extern "C" GType gtkmm___custom_object_##cname##_get_type()\
{\
  GType type = Obj_Get_Type<name>();\
  return type;\
}\
\
extern "C" void cname##_postcreate(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)\
{\
  uwrap<name>(object);\
  if (reason == GLADE_CREATE_USER)\
  {\
    GladeWidget* gw = glade_widget_get_from_gobject(object);\
    gw_set_default(gw);\
  }\
}

#define GLADE_SIMPLEOBJECT_FUNCS(uname, cname)\
extern "C" GType cname##_get_type(); \
extern "C" void cname##_wrap_new(GObject* object);\
\
void cname##_init()\
{\
  SimpleObject_Get_Type<uname>();\
}\
\
extern "C" GType gtkmm___custom_object_##cname##_get_type()\
{\
  GType type = SimpleObject_Get_Type<uname>();\
  return type;\
}\
\
extern "C" void cname##_postcreate(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)\
{\
  uwrap<uname>(object);\
  if (reason == GLADE_CREATE_USER)\
  {\
    GladeWidget* gw = glade_widget_get_from_gobject(object);\
    gw_set_default(gw);\
  }\
}

#define GLADE_GROUPOBJECT_FUNCS(uname, cname)\
extern "C" GType cname##_get_type(); \
extern "C" void cname##_wrap_new(GObject* object);\
\
void cname##_init()\
{\
  GroupObject_Get_Type<uname>();\
}\
\
extern "C" GType gtkmm___custom_object_##cname##_get_type()\
{\
  GType type = GroupObject_Get_Type<uname>();\
  return type;\
}\
\
extern "C" void cname##_postcreate(GladeWidgetAdaptor *adaptor, GObject *object, GladeCreateReason reason)\
{\
  uwrap<uname>(object);\
  if (reason == GLADE_CREATE_USER)\
  {\
    GladeWidget* gw = glade_widget_get_from_gobject(object);\
    gw_set_default(gw);\
  }\
}

#endif
