#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <alloca.h>

#include <gladeui/glade.h>
#include <glib.h>
#include <glib/gprintf.h>

#include <uwidgets/UWidgets.h>
#include <uniwidgets/ThemeLoader-wrapper.h>
#include <dynamic_libs.h>

#undef atoi

//Temporary hack until we'll use normal gettext
#define _(x) x

using namespace std;

typedef struct _sensdata sensdata;

struct _sensdata
{
	long id;
	long node_id;
	gchar *name, *nodename, *stype, *textname;
	sensdata *next;
};

typedef struct
{
  GtkWidget *window, *list, *s_name, *s_id, *s_type, *s_descr, *n_name, *n_id, *oslist, *p1_name, *p2_name, *p3_name, *p4_name, *p1_entry, *p2_entry, *p3_entry, *p4_entry;
  GList *olist;
  gint onum;
  GladeWidget *gw;
  gchar *prname, *sname;
  gint prid;
  GtkTreeSelection *sel, *ossel;
  GtkListStore *list_store;
  gboolean firstrun;
  gpointer object;
  gchar *prefix; /* Current sensor name prefix */
  gboolean seek_result;
  gboolean pack;
} UniguiEditorObject;

typedef struct
{
  GladeWidget *gw;
  const gchar *id;
  gboolean firstrun;
  gchar *prefix;
  const gchar *theme;
  GtkWidget *list;
  GtkTreeSelection *sel;
} ThemeEditorObject;

void glade_init()
{
  //glue_init_gtkmm_internals();
}

static gboolean
event_delete (GtkWidget *w)
{
  gint x,y;

  gtk_window_get_position(GTK_WINDOW(w), &x, &y);
  g_object_set_data(G_OBJECT(w), "x-position", (gpointer)x);
  g_object_set_data(G_OBJECT(w), "y-position", (gpointer)y);
  gtk_widget_hide(w);

  return TRUE;
}

static
gboolean
find_func (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
  UniguiEditorObject *obj = (UniguiEditorObject *)data;
  
  if(obj->pack)
  {
    gint curid;
    gtk_tree_model_get(model, iter, 1, &curid, -1);
    if(obj->prid == curid) {
      gtk_tree_selection_select_iter(obj->sel, iter);
      gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(obj->list), path, NULL, TRUE, 0.5, 0.5);
      obj->seek_result = TRUE;
      return TRUE;
    }
  }
  else
  {
    gchar *curname;
    gtk_tree_model_get(model, iter, 0, &curname, -1);
    if(strcmp(obj->prname, curname) == 0) {
      gtk_tree_selection_select_iter(obj->sel, iter);
      gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(obj->list), path, NULL, TRUE, 0.5, 0.5);
      g_free(curname);
      obj->seek_result = TRUE;
      return TRUE;
    }
    g_free(curname);
  }

  return FALSE;
}

static void
clear_sensor_props (UniguiEditorObject *obj)
{
  gchar *buf = static_cast<gchar*>(alloca(128));
  g_sprintf(buf, "");
  gtk_label_set_text(GTK_LABEL(obj->p1_name), buf);
  gtk_label_set_text(GTK_LABEL(obj->p2_name), buf);
  gtk_label_set_text(GTK_LABEL(obj->p3_name), buf);
  gtk_label_set_text(GTK_LABEL(obj->p4_name), buf);
  gtk_widget_hide(obj->p1_entry);
  gtk_widget_hide(obj->p2_entry);
  gtk_widget_hide(obj->p3_entry);
  gtk_widget_hide(obj->p4_entry);
}

static void
set_sensor_props (UniguiEditorObject *obj)
{
  gchar *buf = static_cast<gchar*>(alloca(128));
  gint val;
  
  g_sprintf(buf, "chvalue");
  glade_widget_pack_property_get(obj->gw, buf, &val);
  g_sprintf(buf, "chvalue: ");
  gtk_label_set_text(GTK_LABEL(obj->p1_name), buf);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(obj->p1_entry), val);
  gtk_widget_show(obj->p1_entry);
  
  g_sprintf(buf, "chpriority");
  glade_widget_pack_property_get(obj->gw, buf, &val);
  g_sprintf(buf, "chpriority: ");
  gtk_label_set_text(GTK_LABEL(obj->p2_name), buf);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(obj->p2_entry), val);
  gtk_widget_show(obj->p2_entry);
}

static void
change_pack_property (GtkSpinButton *sb, UniguiEditorObject *obj)
{
  gchar *buf = static_cast<gchar*>(alloca(128));
  if(sb == GTK_SPIN_BUTTON(obj->p1_entry))
    g_sprintf(buf, "chvalue");
  else if(sb == GTK_SPIN_BUTTON(obj->p2_entry))
    g_sprintf(buf, "chpriority");
  else
    return;
  glade_widget_pack_property_set(obj->gw, buf, gtk_spin_button_get_value_as_int(sb));
}

static void
change_property (GtkTreeSelection *sel, UniguiEditorObject *obj)
{
  GtkTreeModel *mdl;
  GtkTreeIter iter;
  long id, node_id;
  gchar *name, *nodename, *stype, *textname;
  gchar *buf = static_cast<gchar*>(alloca(256));
  if(obj->firstrun) /* False selection change callback just after widget creation */
  return;

  if(gtk_tree_selection_get_selected(sel, &mdl, &iter)) {
    gtk_tree_model_get(mdl, &iter, 0, &name, 1, &id, 2, &nodename, 3, &node_id, 4, &stype, 5, &textname, -1);
    GtkTreeModel *m;
    GtkTreeIter it;
    gboolean all = FALSE;
    if(gtk_tree_selection_get_selected(obj->ossel, &m, &it))
      gtk_tree_model_get(m, &it, 1, &(obj->pack), 4, &all, -1);
    GladeWidget* gw = obj->gw;
    GList* list = NULL;
    if( all && ( obj->gw == glade_widget_get_from_gobject(obj->object) ) )
      list = glade_widget_get_children(obj->gw);
    do
    {
      if(list)
      {
	gw = glade_widget_get_from_gobject(G_OBJECT(list->data));
	list = g_list_next(list);
      }
      if(obj->pack)
      {
	g_sprintf(buf, "%s-sensor-id", obj->prefix);
	glade_widget_pack_property_set(gw, buf, id);
	g_sprintf(buf, "%s-node-id", obj->prefix);
	glade_widget_pack_property_set(gw, buf, node_id);
	if(!all)
	{
	  change_pack_property(GTK_SPIN_BUTTON(obj->p1_entry), obj);
	  change_pack_property(GTK_SPIN_BUTTON(obj->p2_entry), obj);
	}
      }
      else
      {
	g_sprintf(buf, "%s-sensor-name", obj->prefix);
	glade_widget_property_set(gw, buf, name);
	g_sprintf(buf, "%s-node-name", obj->prefix);
	glade_widget_property_set(gw, buf, nodename);
	g_sprintf(buf, "%s-stype", obj->prefix);
	glade_widget_property_set(gw, buf, stype);
	g_sprintf(buf, "%s-sensor-id", obj->prefix);
	glade_widget_property_set(gw, buf, id);
	g_sprintf(buf, "%s-node-id", obj->prefix);
	glade_widget_property_set(gw, buf, node_id);
	g_sprintf(buf, "%s-textname", obj->prefix);
	glade_widget_property_set(gw, "textname", textname);
      }
    }
    while(list);

    g_sprintf(buf, "Name: %s", name);
    gtk_label_set_text(GTK_LABEL(obj->s_name), buf);
    g_sprintf(buf, "ID: %ld", id);
    gtk_label_set_text(GTK_LABEL(obj->s_id), buf);
    g_sprintf(buf, "Type: %s", stype);
    gtk_label_set_text(GTK_LABEL(obj->s_type), buf);
    g_sprintf(buf, "Description: %s", textname);
    gtk_label_set_text(GTK_LABEL(obj->s_descr), buf);
    g_sprintf(buf, "Name: %s", nodename);
    gtk_label_set_text(GTK_LABEL(obj->n_name), buf);
    g_sprintf(buf, "ID: %ld", node_id);
    gtk_label_set_text(GTK_LABEL(obj->n_id), buf);
    
    g_free(name);
    g_free(nodename);
    g_free(stype);
    g_free(textname);
  }
}

static
gboolean
find_os_func (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
  gchar *curname;
  UniguiEditorObject *obj = (UniguiEditorObject *)data;

  gtk_tree_model_get(model, iter, 0, &curname, -1);
  if(strcmp(obj->sname, curname) == 0) {
    gtk_tree_selection_select_iter(obj->ossel, iter);
    gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(obj->oslist), path, NULL, TRUE, 0.5, 0.5);
    g_free(curname);
    return TRUE;
  }
  g_free(curname);

  return FALSE;
}

static
gboolean
find_theme_func (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
  gchar *curname;
  gchar *buf = static_cast<gchar*>(alloca(64));
  ThemeEditorObject *obj = (ThemeEditorObject *)data;

  gtk_tree_model_get(model, iter, 0, &curname, -1);
  g_sprintf(buf, "%s_%s", obj->prefix, curname);
  g_free(curname);

  if(strcmp(obj->theme, buf) == 0) {
    gtk_tree_selection_select_iter(obj->sel, iter);
    gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(obj->list), path, NULL, TRUE, 0.5, 0.5);
    return TRUE;
  }

  return FALSE;
}

static void change_sensor(UniguiEditorObject *obj, gchar *new_prefix)
{
  gchar *buf = static_cast<gchar*>(alloca(128));

  if(obj->prefix)
    g_free(obj->prefix);

  obj->prefix = new_prefix;
  obj->seek_result = FALSE;
  
  gboolean all = FALSE;
  GtkTreeModel *mdl;
  GtkTreeIter it;
  if(gtk_tree_selection_get_selected(obj->ossel, &mdl, &it))
    gtk_tree_model_get(mdl, &it, 1, &(obj->pack), 4, &all, -1);
  if( obj->pack && !all )
    set_sensor_props(obj);
  else
    clear_sensor_props(obj);
  gboolean find = TRUE;
  if( all && ( obj->gw == glade_widget_get_from_gobject(obj->object) ) )
  {
    GladeWidget* gw = obj->gw;
    GList* list = glade_widget_get_children(obj->gw);
    gint *prid = g_new0(gint, obj->onum);
    gchar **prname = g_new0(gchar*, obj->onum);
    gint id_count = 0;
    gint name_count = 0;
    do
    {
      if(list)
      {
	gw = glade_widget_get_from_gobject(G_OBJECT(list->data));
	list = g_list_next(list);
      }
      if(obj->pack)
      {
	sprintf(buf, "%s-sensor-id", obj->prefix);
	if(glade_widget_pack_property_get(gw, buf, &(prid[id_count])))
	  id_count++;
      }
      else
      {
	sprintf(buf, "%s-sensor-name", obj->prefix);
	if(glade_widget_property_get(gw, buf, &(prname[name_count])))
	  name_count++;
      }
    }
    while(list);
    gint id = prid[0];
    if(id_count > 0)
      for(int i=0; i<id_count; ++i)
	if(id != prid[i])
	  find = FALSE;
    gchar* name = prname[0];
    if(name_count > 0)
      for(int i=0; i<name_count; ++i)
	if( strcmp(name,prname[i]) != 0 )
	  find = FALSE;
    g_free(prid);
    g_free(prname);
  }
  GladeWidget* gw = obj->gw;
  GList* list = NULL;
  if( all && ( obj->gw == glade_widget_get_from_gobject(obj->object) ) )
    list = glade_widget_get_children(obj->gw);
  if(find)
    do
    {
      if(list)
      {
	gw = glade_widget_get_from_gobject(G_OBJECT(list->data));
	list = g_list_next(list);
      }
      if(obj->pack)
      {
	sprintf(buf, "%s-sensor-id", obj->prefix);
	if(glade_widget_pack_property_get(gw, buf, &(obj->prid)) && (obj->prid != UniWidgetsTypes::DefaultObjectId))
	  gtk_tree_model_foreach(GTK_TREE_MODEL(obj->list_store), find_func, obj);
      }
      else
      {
	sprintf(buf, "%s-sensor-name", obj->prefix);
	if(glade_widget_property_get(gw, buf, &(obj->prname)) && (obj->prname))
	  gtk_tree_model_foreach(GTK_TREE_MODEL(obj->list_store), find_func, obj);
      }
    }
    while(list);
  if( !obj->seek_result)
  {
      gtk_tree_selection_unselect_all(obj->sel);
  }
}

static void
choose_sensor (GtkTreeSelection *sel, UniguiEditorObject *obj)
{
  GtkTreeModel *mdl;
  GtkTreeIter iter;
  gchar *sname;
  gpointer gobj = obj->object;

  if(gtk_tree_selection_get_selected(sel, &mdl, &iter)) {
    gchar *buf = static_cast<gchar*>(alloca(128));
    gchar *str = gtk_tree_model_get_string_from_iter(mdl, &iter);
    guint row = atoi(str);
    g_free(str);
    
    gtk_tree_model_get(mdl, &iter, 3, &sname, 2, &gobj, -1);
    g_object_set_data(G_OBJECT(obj->object), "cur_sens", (gpointer)row);
    obj->gw = glade_widget_get_from_gobject(G_OBJECT(gobj));
    change_sensor(obj, sname); /* Sname will be freed later when it will not be needed */
  }
}

static gchar *
filename_to_utf8 (const gchar *oldname)
{
  gchar *file_name = NULL; // need to be freed after use

  if (!g_utf8_validate(oldname, -1, NULL)) {
  file_name = g_filename_to_utf8(oldname, -1, NULL, NULL, NULL);
  if(!file_name)
    g_warning("Some characters in the filename is neither UTF-8 nor your local encoding\n");
  }

  if(!file_name) {
    file_name = g_strdup(oldname);
  }

  return file_name;
}

static GtkWidget*
list_in_scrolled_window (guint num, GtkWidget *hbox, const gchar *title, GType *types, GtkListStore **list_store, GtkTreeSelection **sel)
{
  GtkWidget *list, *sw;
  GtkTreeViewColumn *column;
  GtkCellRenderer *renderer;

  *list_store = gtk_list_store_newv(num, types);
  list = gtk_tree_view_new_with_model(GTK_TREE_MODEL(*list_store));

  /* We show only the 1-st column (sensor name). The rest are displayed anywhere */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(title, renderer, "text", 0, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(list), column);
  *sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(list));
  gtk_tree_selection_set_mode(*sel, GTK_SELECTION_BROWSE);

  /* Miscallaneous options and adding to the container */
  sw = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(sw), GTK_SHADOW_ETCHED_IN);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_container_add(GTK_CONTAINER(sw), list);
  gtk_box_pack_start(GTK_BOX(hbox), sw, TRUE, TRUE, 0);

  return list;
}

GType avail_sens_list_types[] = {G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING};
GType obj_sens_list_types[] = {G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_POINTER, G_TYPE_STRING, G_TYPE_BOOLEAN};

static GtkListStore *oslist_store;
  
static void
update_sensors_editor_window (UniguiEditorObject *obj, GObject* object, const gchar *only_sensor)
{
  GladeWidget* gw = glade_widget_get_from_gobject(object);
  if(!gw )
  {
    return;
  }
  guint i;
  GtkTreeIter iter;
  gchar *buf = static_cast<gchar*>(alloca(64));
  
  sprintf(buf, "Sensors editor for %s", gw->name);
  gtk_window_set_title(GTK_WINDOW(obj->window), buf);

  obj->firstrun = FALSE;

  GList * olist = glade_widget_get_children (gw);
  if(obj->olist)
    g_list_free(obj->olist);
  obj->olist = olist;
  gint onum = 0;
  for(; olist; ++onum)
    olist = g_list_next(olist);
  /*
  GladeWidgetAdaptor *adaptor = reinterpret_cast<GladeWidgetAdaptor*>(data);
  GObject* object = G_OBJECT(widget);
  if(adaptor && object)
    show_sensors_editor_window(adaptor, object, NULL );*/
  
    //cerr << "onum=" << onum << endl;
  if( (obj->object != object) || ( ( onum > 0 ) && ( obj->onum != onum ) ) ) {/* The object was changed. Sensors list needs update */
    GObjectClass *oclass;
    GParamSpec **pspecs;
    GRegex *regex;
    GMatchInfo *match_info;
    guint nprop, nsens = 0;
    gchar *prefix_store;

    obj->gw = gw;
    obj->object = object;
    obj->onum = onum;

    /* Cleaning the object's sensors list */
    g_object_ref(oslist_store);
    gtk_tree_view_set_model(GTK_TREE_VIEW(obj->oslist), NULL);
    gtk_list_store_clear(oslist_store);
    gtk_tree_view_set_model(GTK_TREE_VIEW(obj->oslist), GTK_TREE_MODEL(oslist_store));
    g_object_unref(oslist_store);

    static const gint max_sensors_for_glade_widget = 5;
    gchar** prfxs = static_cast<gchar**>(alloca(onum*max_sensors_for_glade_widget));
    gint prfxnum = 0;
    GtkTreeIter prev_iter;
    gpointer gobj = object;
    gboolean child = FALSE;
    do
    {
      /* Filling list with new sensors */
      oclass = G_OBJECT_GET_CLASS(gobj);
      pspecs = g_object_class_list_properties(oclass, &nprop);
      regex = g_regex_new("^(.*)(-sensor-name)$", G_REGEX_OPTIMIZE, static_cast<GRegexMatchFlags>(0), NULL);

      for(i = 0; i < nprop; i++) {
      //cerr << "prop=" << pspecs[i]->name << endl;
	if(g_regex_match(regex, pspecs[i]->name, static_cast<GRegexMatchFlags>(0), &match_info)) {
	  gchar *prefix = g_match_info_fetch(match_info, 1);
	  if(nsens == 0)
	    prefix_store = g_strdup(prefix);
	  obj->pack = FALSE;
	  gtk_list_store_append(oslist_store, &iter);
	  GladeWidget* gw = glade_widget_get_from_gobject(G_OBJECT(gobj));
	  gchar *buf = static_cast<gchar*>(alloca(64));
	  if(child)
	    sprintf(buf, "%s: %s", gw->name, prefix);
	  else
	    sprintf(buf, "%s", prefix);
	  gtk_list_store_set(oslist_store, &iter, 0, buf, 1, FALSE, 2, gobj, 3, prefix, 4, FALSE, -1);
	  gboolean exist = FALSE;
	  for(gint j = 0; j < prfxnum; ++j)
	    if(strcmp(prfxs[j], prefix)==0)
	    {
	      exist = TRUE;
	      break;
	    }
	  if(!exist && (child) )
	  {
	    gtk_list_store_prepend(oslist_store, &iter);
	    gchar *buf = static_cast<gchar*>(alloca(64));
	    sprintf(buf, "%s (for all childs)", prefix);
	    gtk_list_store_set(oslist_store, &iter, 0, buf, 1, FALSE, 2, obj->object, 3, prefix, 4, TRUE, -1);
	    prfxs[prfxnum++] = g_strdup(prefix);
	    if(prfxnum != 0)
	      gtk_list_store_move_after(oslist_store, &iter, &prev_iter);
	    prev_iter = iter;
	  }
	  g_free(prefix);
	  g_free(match_info);
	  nsens++;
	}
      }
      
      /* Filling list with new sensors for packing params*/
      GtkWidget *parent = GTK_WIDGET (gobj)->parent;
      if(parent)
      {
      //cerr << "packing props:" << endl;
	GObjectClass *pclass;
	GParamSpec **pspecs;
	GRegex *regex;
	pclass = G_OBJECT_GET_CLASS( G_OBJECT(parent) );
	pspecs = gtk_container_class_list_child_properties(pclass, &nprop);
	regex = g_regex_new("^(.*)(-sensor-id)$", G_REGEX_OPTIMIZE, static_cast<GRegexMatchFlags>(0), NULL);

	for(i = 0; i < nprop; i++) {
      //cerr << "prop=" << pspecs[i]->name << endl;
	  if(g_regex_match(regex, pspecs[i]->name, static_cast<GRegexMatchFlags>(0), &match_info)) {
	    gchar *prefix = g_match_info_fetch(match_info, 1);
	    if(nsens == 0)
	      prefix_store = g_strdup(prefix);
	    obj->pack = TRUE;
	    gtk_list_store_append(oslist_store, &iter);
	    GladeWidget* gw = glade_widget_get_from_gobject(G_OBJECT(gobj));
	    gchar *buf = static_cast<gchar*>(alloca(64));
	    if(child)
	      sprintf(buf, "%s: %s", gw->name, prefix);
	    else
	      sprintf(buf, "%s", prefix);
	    gtk_list_store_set(oslist_store, &iter, 0, buf, 1, TRUE, 2, gobj, 3, prefix, 4, FALSE, -1);
	    gboolean exist = FALSE;
	    for(gint j = 0; j < prfxnum; ++j)
	      if(strcmp(prfxs[j], prefix)==0)
	      {
		exist = TRUE;
		break;
	      }
	    if(!exist && (child))
	    {
	      gtk_list_store_prepend(oslist_store, &iter);
	      gchar *buf = static_cast<gchar*>(alloca(64));
	      sprintf(buf, "%s (for all childs)", prefix);
	      gtk_list_store_set(oslist_store, &iter, 0, buf, 1, TRUE, 2, obj->object, 3, prefix, 4, TRUE, -1);
	      prfxs[prfxnum++] = g_strdup(prefix);
	      if(prfxnum != 0)
		gtk_list_store_move_after(oslist_store, &iter, &prev_iter);
	      prev_iter = iter;
	    }
	    g_free(prefix);
	    g_free(match_info);
	    nsens++;
	  }
	}
	
	g_free(pspecs);
	g_regex_unref(regex);
      }
      gobj = NULL;
      child = TRUE;
      if(obj->olist)
      {
	gobj = obj->olist->data;
	obj->olist = g_list_next(obj->olist);
      }
    }
    while(gobj);
    for(gint j = 0; j < prfxnum; ++j)
      if(prfxs[j])
	g_free(prfxs[j]);
      
    if(nsens == 0)
    {
      obj->object = NULL;
      return;
    }
    else if(nsens == 1) { /* The object have only one sensor -- simplified interface (without object's sensors list */
      gtk_tree_selection_unselect_all(obj->sel);
      change_sensor(obj, prefix_store);
      if(GTK_WIDGET_VISIBLE(obj->oslist->parent))
        gtk_widget_hide(obj->oslist->parent);
    } else {/* Several sensors: full interface */
      g_free(prefix_store);

      if(!only_sensor) {
        GtkTreeIter iter;
        GtkTreePath *path;

        gchar *buf = static_cast<gchar*>(alloca(16));
	  gtk_tree_selection_unselect_all(obj->sel);
        guint row = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(obj->object), "cur_sens"));/* Get remembered selected sensor and select it */
        g_sprintf(buf, "%u", row);
        if(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(oslist_store), &iter, buf)) {
          gtk_tree_selection_select_iter(obj->ossel, &iter);
          path = gtk_tree_path_new_from_string(buf);
          gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(obj->oslist), path, NULL, TRUE, 0.5, 0.0);
          gtk_tree_path_free(path);
        }
        else
	{
	  gtk_tree_selection_unselect_all(obj->ossel);
	}
      }

      if(!GTK_WIDGET_VISIBLE(obj->oslist->parent))
      gtk_widget_show(obj->oslist->parent);

      }
      g_free(pspecs);
      g_regex_unref(regex);
  }

  if(!only_sensor && !GTK_WIDGET_SENSITIVE(obj->oslist))
    gtk_widget_set_sensitive(obj->oslist, TRUE);

  if(only_sensor && GTK_WIDGET_VISIBLE(obj->oslist->parent)) { /* Object has more than one sensor. We should find appropriate */
    GRegex *regex = g_regex_new("-sensor-name$", static_cast<GRegexCompileFlags>(0),static_cast<GRegexMatchFlags>(0), NULL);
    obj->sname = g_regex_replace(regex, only_sensor, -1, 0, "", static_cast<GRegexMatchFlags>(0), NULL);
    gtk_widget_set_sensitive(obj->oslist, FALSE);
    gtk_tree_model_foreach(GTK_TREE_MODEL(oslist_store), find_os_func, obj);

    g_regex_unref(regex);
    g_free(obj->sname);
  }
}

static void
on_selection_changed (GladeProject *pr, UniguiEditorObject *obj)
{
  if(GTK_WIDGET_VISIBLE(obj->window))
  {
    GladeProject *project = pr;
    if(glade_app_get_project() != project)
    {
      project = glade_app_get_project();
      gboolean exist = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(project), "selection_handler"));
      if(!exist)
      {
	exist = TRUE;
	g_signal_connect (G_OBJECT (project), "selection_changed", G_CALLBACK (on_selection_changed), obj );
	g_object_set_data(G_OBJECT (project), "selection_handler", (gpointer)exist);
	on_selection_changed(project, obj);
      }
      return;
    }
    GladeWidget* gw = NULL;
    GList *list = glade_project_selection_get (project);
    while( list && list->data )
    {
      if(!GLADE_IS_PLACEHOLDER (list->data) )
      {
	gw = glade_widget_get_from_gobject (list->data);
	break;
      }
      list = list->next;
    }
    if(gw)
    {
      GObject* prev = G_OBJECT(obj->object);
      GObject* temp = glade_widget_get_object(gw);
      if(temp != prev)
	update_sensors_editor_window( obj, temp, NULL );
      if(obj->object == NULL && prev != NULL)
	update_sensors_editor_window( obj, prev, NULL );
      if(obj->object == NULL && GTK_WIDGET_VISIBLE(obj->window))
        gtk_widget_hide(obj->window);
    }
  }
}

static void
show_sensors_editor_window (GladeWidgetAdaptor *adaptor, GObject* object, const gchar *only_sensor)
{
  GladeWidget* gw = glade_widget_get_from_gobject(object);
  if(!gw )
  {
    /*GtkWidget* warning;
    warning = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                                                              "No childs in container!\n"
                                                              "Please add at least 1 child and re-run UniGui editor.");
    gtk_dialog_run(GTK_DIALOG(warning));
    gtk_widget_destroy(warning);*/
    return;
  }
  guint i;
  GtkTreeIter iter;
  gchar *buf = static_cast<gchar*>(alloca(64));

  static UniguiEditorObject *obj = NULL;

  if(!obj) {/* hide window when deleted and show it back again */
    long count;
    GtkWidget *filechooser, *vbox1, *vbox2, *hbox1, *hbox2, *thing, *frame;
    GtkFileFilter *filter;
    sensdata* sd;
    gchar *fname = NULL;

    /* Getting sensors list */
    filechooser = gtk_file_chooser_dialog_new("Select sensors config file", NULL,
                                                                        GTK_FILE_CHOOSER_ACTION_OPEN,
                                                                        GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);

    filter = gtk_file_filter_new();
    gtk_file_filter_set_name(filter, "XML files");
    gtk_file_filter_add_pattern(filter, "*.xml");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(filechooser), filter);
    filter = gtk_file_filter_new();
    gtk_file_filter_set_name(filter, "all files");
    gtk_file_filter_add_pattern(filter, "*");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(filechooser), filter);

    if (gtk_dialog_run(GTK_DIALOG(filechooser)) == GTK_RESPONSE_ACCEPT) {
      gchar* lfname;

      lfname = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooser));
      fname = filename_to_utf8(lfname);
      g_free(lfname);
    }

    gtk_widget_destroy(filechooser);

    if (!fname) {
      GtkWidget* warning;
      warning = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                                                                "Config file must be selected!\n"
                                                                "Please re-run UniGui editor and select the proper file.");
      gtk_dialog_run(GTK_DIALOG(warning));
      gtk_widget_destroy(warning);
      return;
    }
    
    std::shared_ptr<DynamicLib> uniset_lib = DynamicLib::open(std::string("libUniWidgets2-uniset2.so"));

    typedef int load_conf_t(int, const char**,  char*);
    load_conf_t* load_conf = (load_conf_t*) uniset_lib->load("load_conf");
    if(!load_conf)
      g_critical("Cannot load load_conf_t symbol!");
    
    typedef int get_sensors_t(sensdata**, long*);
    get_sensors_t* get_sensors = (get_sensors_t*) uniset_lib->load("get_sensors");
    if(!get_sensors)
      g_critical("Cannot load get_sensors_t symbol!");
    
    guint result = load_conf(0, NULL, fname);
    g_free(fname);
    if (result) {// Actually this line is not reached if result !=0. load_conf cause segfault at incorrect xml file.
      GtkWidget* warning;
      warning = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                                                                "Config file is invalid!\n"
                                                                "Please re-run UniGui editor and select the proper file.");
      gtk_dialog_run(GTK_DIALOG(warning));
      gtk_widget_destroy(warning);
      return;
    }

    if(get_sensors(&sd, &count))
      g_critical("Cannot get sensors list!");
    if(sd == NULL)
      g_critical("Sensors list is empty!");

    /* Building editor window */
    obj = new UniguiEditorObject;//malloc(sizeof(UniguiEditorObject));
    obj->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    obj->firstrun = TRUE;
    obj->prefix = NULL;
    obj->olist = NULL;
    obj->pack = FALSE;
    g_signal_connect(obj->window, "delete-event", G_CALLBACK(event_delete), NULL);

    hbox1 = gtk_hbox_new(FALSE, 5);

    /* Current object sensors list (will be filled later) */
    obj->oslist = list_in_scrolled_window(sizeof(obj_sens_list_types) / sizeof(GType), hbox1, "Object sensors", obj_sens_list_types, &oslist_store, &obj->ossel);
    g_signal_connect(obj->ossel, "changed", G_CALLBACK(choose_sensor), obj);

    /* Available sensors list */
    obj->list = list_in_scrolled_window(sizeof(avail_sens_list_types) / sizeof(GType), hbox1, "Available sensors", avail_sens_list_types, &(obj->list_store), &obj->sel);
    gtk_widget_set_size_request(obj->list, -1, 400);

    /* Fill the avalable sensor list with the contents */
    for (i = 0; i <= count - 1; i++) {
      gtk_list_store_append(obj->list_store, &iter);
      gtk_list_store_set(obj->list_store, &iter, 0, sd->name, 1, sd->id, 2, sd->nodename, 3, sd->node_id, 4, sd->stype, 5, sd->textname, -1);
      sd = sd->next;
    }
    g_signal_connect(obj->sel, "changed", G_CALLBACK(change_property), obj);

    /* The rest of the interface */
    vbox1 = gtk_vbox_new(FALSE, 0);

    frame = gtk_frame_new("Sensor");
    vbox2 = gtk_vbox_new(FALSE, 5);

    thing = obj->s_name = gtk_label_new("Name:");
    hbox2 = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
    thing = obj->s_id = gtk_label_new("ID:");
    hbox2 = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
    thing = obj->s_type = gtk_label_new("Type:");
    hbox2 = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
    thing = obj->s_descr = gtk_label_new("Description:");
    hbox2 = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);

    gtk_container_add(GTK_CONTAINER(frame), vbox2);
    gtk_box_pack_start(GTK_BOX(vbox1), frame, TRUE, TRUE, 5);

    frame = gtk_frame_new("Node");
    vbox2 = gtk_vbox_new(FALSE, 5);

    thing = obj->n_name = gtk_label_new("Name:");
    hbox2 = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
    thing = obj->n_id = gtk_label_new("ID:");
    hbox2 = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);

    gtk_container_add(GTK_CONTAINER(frame), vbox2);
    gtk_box_pack_start(GTK_BOX(vbox1), frame, TRUE, TRUE, 5);

    frame = gtk_frame_new("Params:");
    vbox2 = gtk_vbox_new(FALSE, 5);
    
    GtkAdjustment* adj = NULL;
    hbox2 = gtk_hbox_new(FALSE, 0);
    thing = obj->p1_name = gtk_label_new("");
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    adj = (GtkAdjustment*)gtk_adjustment_new(0,0,65535,1,10,0);
    thing = obj->p1_entry = gtk_spin_button_new(adj,0,0);
    g_signal_connect(GTK_SPIN_BUTTON(obj->p1_entry), "value-changed", G_CALLBACK(change_pack_property), obj);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
    hbox2 = gtk_hbox_new(FALSE, 0);
    thing = obj->p2_name = gtk_label_new("");
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    adj = (GtkAdjustment*)gtk_adjustment_new(0,0,65535,1,10,0);
    thing = obj->p2_entry = gtk_spin_button_new(adj,0,0);
    g_signal_connect(GTK_SPIN_BUTTON(obj->p2_entry), "value-changed", G_CALLBACK(change_pack_property), obj);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
    hbox2 = gtk_hbox_new(FALSE, 0);
    thing = obj->p3_name = gtk_label_new("");
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    adj = (GtkAdjustment*)gtk_adjustment_new(0,0,65535,1,10,0);
    thing = obj->p3_entry = gtk_spin_button_new(adj,0,0);
    g_signal_connect(GTK_SPIN_BUTTON(obj->p3_entry), "value-changed", G_CALLBACK(change_pack_property), obj);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
    hbox2 = gtk_hbox_new(FALSE, 0);
    thing = obj->p4_name = gtk_label_new("");
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    adj = (GtkAdjustment*)gtk_adjustment_new(0,0,65535,1,10,0);
    thing = obj->p4_entry = gtk_spin_button_new(adj,0,0);
    g_signal_connect(GTK_SPIN_BUTTON(obj->p4_entry), "value-changed", G_CALLBACK(change_pack_property), obj);
    gtk_box_pack_start(GTK_BOX(hbox2), thing, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox2), hbox2, FALSE, FALSE, 0);
    
    gtk_container_add(GTK_CONTAINER(frame), vbox2);
    gtk_box_pack_start(GTK_BOX(vbox1), frame, TRUE, TRUE, 5);
    
    gtk_box_pack_start(GTK_BOX(hbox1), vbox1, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(obj->window), hbox1);
    
    gtk_widget_show_all(obj->window);
    gtk_widget_hide(obj->p1_entry);
    gtk_widget_hide(obj->p2_entry);
    gtk_widget_hide(obj->p3_entry);
    gtk_widget_hide(obj->p4_entry);
    
    gboolean exist=TRUE;
    g_signal_connect (G_OBJECT ( glade_app_get_project() ), "selection_changed", G_CALLBACK (on_selection_changed), obj );
    g_object_set_data(G_OBJECT ( glade_app_get_project() ), "selection_handler", (gpointer)exist);
  } else {
    if(!GTK_WIDGET_VISIBLE(obj->window)) {
      gtk_widget_show(obj->window);
      gtk_window_move(GTK_WINDOW(obj->window), GPOINTER_TO_INT(g_object_get_data(G_OBJECT(obj->window), "x-position")),
                                    GPOINTER_TO_INT(g_object_get_data(G_OBJECT(obj->window), "y-position")));
    }
    gtk_window_present(GTK_WINDOW(obj->window));
  }
  update_sensors_editor_window(obj, object, only_sensor);
  if(obj->object == NULL && GTK_WIDGET_VISIBLE(obj->window))
    gtk_widget_hide(obj->window);
}

static GList *
create_command_property_list (GladeWidget *gnew, GList *saved_props)
{
  GList *l, *command_properties = NULL;

  for (l = saved_props; l; l = l->next)
  {
    GladeProperty *property = GLADE_PROPERTY(l->data);
    GladeProperty *orig_prop = glade_widget_get_pack_property (gnew, property->klass->id);
    GCSetPropData *pdata = g_new0 (GCSetPropData, 1);

    pdata->property  = orig_prop;
    pdata->old_value = g_new0 (GValue, 1);
    pdata->new_value = g_new0 (GValue, 1);

    glade_property_get_value (orig_prop, pdata->old_value);
    glade_property_get_value (property, pdata->new_value);

    command_properties = g_list_prepend (command_properties, pdata);
  }
  return g_list_reverse (command_properties);
}

extern "C" void glade_action_activate (GladeWidgetAdaptor *adaptor, GObject* object, const gchar *action_path);
extern "C" void glade_child_action_activate (GladeWidgetAdaptor *adaptor, GObject *container, GObject* object, const gchar *action_path)
{
  glade_action_activate(adaptor, object, action_path);
}

extern "C" void glade_action_activate (GladeWidgetAdaptor *adaptor, GObject* object, const gchar *action_path)
{
  GladeWidget *gwidget = glade_widget_get_from_gobject (object), *gparent;
  GList       this_widget = { 0, }, that_widget = { 0, };
  GtkWidget   *parent = GTK_WIDGET (object)->parent;

  g_assert (parent);

  gparent = glade_widget_get_from_gobject (parent);

  if(strcmp(action_path, "unisetedit")==0)
  {
    show_sensors_editor_window(adaptor, object, NULL);/* The sensor is not forced to selected. Choose appropriate */
  }
  else if(strcmp(action_path, "childunisetedit")==0)
  {
    show_sensors_editor_window(adaptor, object, NULL);/* The sensor is not forced to selected. Choose appropriate */
  }
  else if (strcmp (action_path, "remove_parent") == 0)
  {
    GladeWidget *new_gparent = gparent->parent;

    /* Since toplevel project objects for now must be GtkWindow,
    * we'll just assert this for now (should be an insensitive action).
    */
    g_assert (!GTK_IS_WINDOW (parent));

    glade_command_push_group (_("Removing parent of %s"),
                                                      gwidget->name);

    /* Remove "this" widget */
    this_widget.data = gwidget;
    glade_command_cut (&this_widget);

    /* Delete the parent */
    that_widget.data = gparent;
    glade_command_delete (&that_widget);

    /* Add "this" widget to the new parent */
    glade_command_paste(&this_widget, new_gparent, NULL);

    glade_command_pop_group ();
  }
  else if (strncmp (action_path, "add_parent/", 11) == 0)
  {
  GType new_type = 0;
  string ap(action_path + 11);
  if ( ap == "alignment")
    new_type = GTK_TYPE_ALIGNMENT;
  else if (ap == "viewport")
    new_type = GTK_TYPE_VIEWPORT;
  else if (ap == "eventbox")
    new_type = GTK_TYPE_EVENT_BOX;
  else if (ap == "frame")
    new_type = GTK_TYPE_FRAME;
  else if (ap == "aspect_frame")
    new_type = GTK_TYPE_ASPECT_FRAME;
  else if (ap == "scrolled_window")
    new_type = GTK_TYPE_SCROLLED_WINDOW;
  else if (ap == "expander")
    new_type = GTK_TYPE_EXPANDER;
  else if (ap == "table")
    new_type = GTK_TYPE_TABLE;
  else if (ap == "hbox")
    new_type = GTK_TYPE_HBOX;
  else if (ap == "vbox")
    new_type = GTK_TYPE_VBOX;
  else if (ap == "hpaned")
    new_type = GTK_TYPE_HPANED;
  else if (ap == "vpaned")
    new_type = GTK_TYPE_VPANED;

  if (new_type)
  {
    GladeWidgetAdaptor *adaptor = glade_widget_adaptor_get_by_type (new_type);
    GList              *saved_props, *prop_cmds;

    glade_command_push_group (_("Adding parent %s to %s"), 
                                                      adaptor->title, gwidget->name);

    /* Record packing properties */
    saved_props = glade_widget_dup_properties (gwidget, gwidget->packing_properties, FALSE, FALSE, FALSE);

    /* Remove "this" widget */
    this_widget.data = gwidget;
    glade_command_cut (&this_widget);

    /* Create new widget and put it where the placeholder was */
    that_widget.data =
    glade_command_create (adaptor, gparent, NULL, 
                                            glade_widget_get_project (gparent));


    /* Remove the alignment that we added in the frame's post_create... */
    if (new_type == GTK_TYPE_FRAME)
    {
      GObject     *frame = glade_widget_get_object (GLADE_WIDGET(that_widget.data));
      GladeWidget *galign = glade_widget_get_from_gobject (GTK_BIN (frame)->child);
      GList        to_delete = { 0, };

      to_delete.data = galign;
      glade_command_delete (&to_delete);
    }

    /* Create heavy-duty glade-command properties stuff */
    prop_cmds = create_command_property_list (GLADE_WIDGET(that_widget.data), saved_props);
    g_list_foreach (saved_props, (GFunc)g_object_unref, NULL);
    g_list_free (saved_props);

    /* Apply the properties in an undoable way */
    if (prop_cmds)
      glade_command_set_properties_list (glade_widget_get_project (gparent), prop_cmds);

    /* Add "this" widget to the new parent */
    glade_command_paste(&this_widget, GLADE_WIDGET (that_widget.data), NULL);

    glade_command_pop_group ();
  }
  }
  else //!!!Try to associate each glade widget with editor object. But how to trace when the widget was changed (without re-activating the action)???
    GWA_GET_CLASS (G_TYPE_OBJECT)->action_activate (adaptor,
                                                                                                object,
                                                                                                action_path);
}

void
glade_set_sensor (GladeWidget        *gw,
                              GladeWidgetAdaptor *adaptor,
                              GObject            *object,
                              const gchar        *id,
                              const GValue       *value)
{
  show_sensors_editor_window(adaptor, object, id); /* Force sensor to be edited */
}

GType theme_list_types[] = {G_TYPE_STRING};

static void
choose_theme(GtkTreeSelection *sel, ThemeEditorObject *spd)
{
  GtkTreeModel *mdl;
  GtkTreeIter iter;
  gchar *theme;
  gchar *buf = static_cast<gchar*>(alloca(64));

  if(spd->firstrun)
    return;

  if(gtk_tree_selection_get_selected(sel, &mdl, &iter)) {
    gtk_tree_model_get(mdl, &iter, 0, &theme, -1);
    g_sprintf(buf, "%s_%s", spd->prefix, theme);
    glade_widget_property_set(spd->gw, spd->id, buf);
    g_free(theme);
  }
}

void
glade_set_theme (GladeWidget        *gw,
                              GladeWidgetAdaptor *adaptor,
                              GObject            *object,
                              const gchar        *id,
                              const GValue       *value)
{
  gsize i, length;
  gchar *theme_prefix, *buf = static_cast<gchar*>(alloca(64));
  gchar **groups = get_theme_groups(&length);
  GRegex *regex = g_regex_new("^usvg", static_cast<GRegexCompileFlags>(0), static_cast<GRegexMatchFlags>(0), NULL);
  GMatchInfo *match_info;
  GtkTreeIter iter;
  GtkWidget *hbox1;

  static GtkListStore *list_store;
  static GtkWidget *window = NULL;
  static ThemeEditorObject spd;

  if(!window) { /* Creating window */
    spd.firstrun = TRUE;
    spd.prefix = NULL;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_widget_set_size_request(window, 200, 200);
    g_signal_connect(window, "delete-event", G_CALLBACK(event_delete), NULL);

    hbox1 = gtk_hbox_new(FALSE, 5);

    /* Themes list (will be filled later) */
    spd.list = list_in_scrolled_window(1, hbox1, "Themes", theme_list_types, &list_store, &spd.sel);
    g_signal_connect(spd.sel, "changed", G_CALLBACK(choose_theme), &spd);

    gtk_container_add(GTK_CONTAINER(window), hbox1);
    gtk_widget_show_all(window);
  } else {/* If window exists just show it */
    if(!GTK_WIDGET_VISIBLE(window)) {
      gtk_widget_show(window);
      gtk_window_move(GTK_WINDOW(window), GPOINTER_TO_INT(g_object_get_data(G_OBJECT(window), "x-position")),
                                    GPOINTER_TO_INT(g_object_get_data(G_OBJECT(window), "y-position")));
    }
    gtk_window_present(GTK_WINDOW(window));
  }

  spd.gw = gw;
  spd.id = id;

  sprintf(buf, "Set theme for %s widget", gw->name);
  gtk_window_set_title(GTK_WINDOW(window), buf);

  /* Cleaning the themes list */
  g_object_ref(list_store);
  gtk_tree_view_set_model(GTK_TREE_VIEW(spd.list), NULL);
  gtk_list_store_clear(list_store);
  gtk_tree_view_set_model(GTK_TREE_VIEW(spd.list), GTK_TREE_MODEL(list_store));
  g_object_unref(list_store);

  /* Founding themes related to current widget and put them into the list */
  if(spd.prefix)
    g_free(spd.prefix);
  spd.prefix = g_regex_replace(regex, adaptor->generic_name, -1, 0, "", static_cast<GRegexMatchFlags>(0), NULL);

  g_regex_unref(regex);
  g_sprintf(buf, "^(%s_)(.*)$", spd.prefix);
  regex = g_regex_new(buf, G_REGEX_OPTIMIZE, static_cast<GRegexMatchFlags>(0), NULL);

  for(i = 0; i < length; i++) {
    if(g_regex_match(regex, groups[i], static_cast<GRegexMatchFlags>(0), &match_info)) {
      gchar *theme = g_match_info_fetch(match_info, 2);
      gtk_list_store_append(list_store, &iter);
      gtk_list_store_set(list_store, &iter, 0, theme, -1);

      g_free(theme);
      g_free(match_info);
    }
  }

  spd.theme = g_value_get_string(value);
  gtk_tree_model_foreach(GTK_TREE_MODEL(list_store), find_theme_func, &spd);

  if(spd.firstrun);
    spd.firstrun = FALSE;

  g_strfreev(groups);
}
