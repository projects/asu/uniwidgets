#ifndef _FUNCTIONS_LIBGLADE_H
#define _FUNCTIONS_LIBGLADE_H

#include <gtkmm.h>
#include <libglademm.h>
#include <glibmm.h>
#include <glade/glade-init.h>
#include <glade/glade-build.h>
#include <uwidgets/UWidgets.h>
#include <usvgwidgets/USVGWidgets.h>
#include <objects/Objects.h>
#include <components/Components.h>
#include <typical/Typical.h>
#include <UPostcreate.h>

using namespace UniWidgets;
using namespace std;

//----------------------------------------------------------
template <class UType> static GtkWidget *glade_new(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
  UType *uw = manage(new UType);
//  cout<<"glade_new() uw_name="<<uw->get_name()<<endl;
  GtkWidget *w = GTK_WIDGET(uw->gobj());
  GObjectClass *oclass = G_OBJECT_GET_CLASS(w);
//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);

  for (unsigned int i=0; i<info->n_properties; i++) {
    Glib::ustring name_prop(info->properties[i].name);
    const gchar *name;
    if(name_prop == "tooltip")
      name = "tooltip-text";
    else
      name = info->properties[i].name;
    const gchar *value = info->properties[i].value;
    GParamSpec *pspec;

    pspec = g_object_class_find_property (oclass, name);
    if (pspec) {
      GValue gvalue = { 0 };

      if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
        g_object_set_property(G_OBJECT(w), name, &gvalue);
        g_value_unset(&gvalue);
      }
    }
  }

  return w;
}
//----------------------------------------------------------
static GtkWidget *glade_new_unioscillograph(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
	UniOscillograph *uw = manage(new UniOscillograph());
	GtkWidget *w = GTK_WIDGET(uw->gobj());
	GObjectClass *oclass = G_OBJECT_GET_CLASS(w);
	//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);
	
	for (unsigned int i=0; i<info->n_properties; i++) {
		Glib::ustring name_prop(info->properties[i].name);
		const gchar *name = info->properties[i].name;
		const gchar *value = info->properties[i].value;
//		cout<<"UniOscillograph prop_name="<<name<<" prop_value="<<value<<endl;
		
		if(name_prop == "expand_widget")
			uw->set_expand_parent_widget_name(value);
		
		GParamSpec *pspec;
		pspec = g_object_class_find_property (oclass, name);
		if (pspec) {
			GValue gvalue = { 0 };
			
			if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
				g_object_set_property(G_OBJECT(w), name, &gvalue);
				g_value_unset(&gvalue);
			}
		}
	}
	
	GladeWidgetInfo *parent = info;
	while(parent->parent)
	{
		parent = parent->parent;
		cout<<"glade_new_unioscillograph parent_name="<<parent->name<<" parent_classname="<<parent->classname<<endl;
		GtkWidget *parent_widget = glade_xml_get_widget(xml,parent->name);
		Gtk::Widget* widget = Glib::wrap(parent_widget);
		if(widget)
		{
			Gtk::Container* container = dynamic_cast<Gtk::Container*>(widget);
			if(container)
				uw->widget_connection_list.push_back(container->signal_add().connect(sigc::mem_fun(*uw, &UniOscillograph::init_expand_parent_widget)));
		}
	}
	
	return w;
}
//----------------------------------------------------------
static GtkWidget *glade_new_unisvgtabbutton(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
	UniSVGTabButton *uw = manage(new UniSVGTabButton());
	GtkWidget *w = GTK_WIDGET(uw->gobj());
	GObjectClass *oclass = G_OBJECT_GET_CLASS(w);
	//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);
	
	for (unsigned int i=0; i<info->n_properties; i++) {
		Glib::ustring name_prop(info->properties[i].name);
		const gchar *name = info->properties[i].name;
		const gchar *value = info->properties[i].value;
//		cout<<"UniSVGTabButton prop_name="<<name<<" prop_value="<<value<<endl;
		
		if(name_prop == "notebook_widget")
			uw->set_notebook_widget_name(value);
		
		GParamSpec *pspec;
		pspec = g_object_class_find_property (oclass, name);
		if (pspec) {
			GValue gvalue = { 0 };
			
			if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
				g_object_set_property(G_OBJECT(w), name, &gvalue);
				g_value_unset(&gvalue);
			}
		}
	}
	
	GladeWidgetInfo *parent = info;
	while(parent->parent)
	{
		parent = parent->parent;
		cout<<"glade_new_unisvgtabbutton parent_name="<<parent->name<<" parent_classname="<<parent->classname<<endl;
		GtkWidget *parent_widget = glade_xml_get_widget(xml,parent->name);
		Gtk::Widget* widget = Glib::wrap(parent_widget);
		if(widget)
		{
			Gtk::Container* container = dynamic_cast<Gtk::Container*>(widget);
			if(container)
				container->signal_add().connect(sigc::mem_fun(*uw, &UniSVGTabButton::init_notebook_widget));
		}
	}
	
	return w;
}
//----------------------------------------------------------
static GtkWidget *glade_new_upopupbox(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
	UPopupBox *uw = manage(new UPopupBox());
	GtkWidget *w = GTK_WIDGET(uw->gobj());
	GObjectClass *oclass = G_OBJECT_GET_CLASS(w);
	//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);
	
	for (unsigned int i=0; i<info->n_properties; i++) {
		Glib::ustring name_prop(info->properties[i].name);
		const gchar *name = info->properties[i].name;
		const gchar *value = info->properties[i].value;
// 		cout<<"UPopupBox prop_name="<<name<<" prop_value="<<value<<endl;
		
		if(name_prop == "consumer_widget_1")
			uw->set_consumer_widget_1_name(value);
		else if(name_prop == "consumer_widget_2")
			uw->set_consumer_widget_2_name(value);
		else if(name_prop == "consumer_widget_3")
			uw->set_consumer_widget_3_name(value);
		else if(name_prop == "consumer_widget_4")
			uw->set_consumer_widget_4_name(value);
		else if(name_prop == "consumer_widget_5")
			uw->set_consumer_widget_5_name(value);
		else if(name_prop == "consumer_minimize_widget")
		{
			uw->set_consumer_minimize_widget_name(value);
			Gtk::Widget* widget = Glib::wrap(glade_xml_get_widget(xml,value));
			uw->set_prop_consumer_minimize_widget(widget);
		}
		
		GParamSpec *pspec;
		pspec = g_object_class_find_property (oclass, name);
		if (pspec) {
			GValue gvalue = { 0 };
			
			if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
				g_object_set_property(G_OBJECT(w), name, &gvalue);
				g_value_unset(&gvalue);
			}
		}
	}
	
	uw->signal_add().connect(sigc::mem_fun(*uw, &UPopupBox::init_consumers));
	GladeWidgetInfo *parent = info;
	while(parent->parent)
	{
		parent = parent->parent;
// 		cout<<"glade_new_upopupbox parent_name="<<parent->name<<" parent_classname="<<parent->classname<<endl;
		GtkWidget *parent_widget = glade_xml_get_widget(xml,parent->name);
		Gtk::Widget* widget = Glib::wrap(parent_widget);
		if(widget)
		{
			Gtk::Container* container = dynamic_cast<Gtk::Container*>(widget);
			if(container)
				container->signal_add().connect(sigc::mem_fun(*uw, &UPopupBox::init_consumers));
		}
	}
	
	return w;
}
//----------------------------------------------------------
static GtkWidget *glade_new_unisvgdialogbutton(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
	UniSVGDialogButton *uw = manage(new UniSVGDialogButton());
	std::string full_path = xml->filename;
	full_path = realpath(full_path.c_str(),NULL);
	uw->set_own_xml_path(full_path);
	cout<<"glade_new_unisvgdialogbutton xml->filename="<<full_path<<endl;
	
	GtkWidget *w = GTK_WIDGET(uw->gobj());
	GObjectClass *oclass = G_OBJECT_GET_CLASS(w);
	//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);
	
	for (unsigned int i=0; i<info->n_properties; i++) {
		Glib::ustring name_prop(info->properties[i].name);
		const gchar *name = info->properties[i].name;
		const gchar *value = info->properties[i].value;
		
		if(name_prop == "dialog_castom_position_from_widget")
			uw->set_dialog_pos_from_widget_name(value);

		GParamSpec *pspec;
		pspec = g_object_class_find_property (oclass, name);
		if (pspec) {
			GValue gvalue = { 0 };
			
			if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
				g_object_set_property(G_OBJECT(w), name, &gvalue);
				g_value_unset(&gvalue);
			}
		}
	}
	
	GladeWidgetInfo *parent = info;
	GtkWidget *parent_widget;
	Gtk::Widget* widget;
	while(parent->parent)
	{
		parent = parent->parent;
		cout<<"glade_new_unisvgdialogbutton parent_name="<<parent->name<<" parent_classname="<<parent->classname<<endl;
		parent_widget = glade_xml_get_widget(xml,parent->name);
		widget = Glib::wrap(parent_widget);
		if(widget)
		{
			Gtk::Container* container = dynamic_cast<Gtk::Container*>(widget);
			if(container)
				container->signal_add().connect(sigc::mem_fun(*uw, &UniSVGDialogButton::init_dialog_pos_from_widget));
		}
	}
	if(widget)
	{
		Gtk::Window* win = dynamic_cast<Gtk::Window*>(widget);
		if(win)
			uw->set_own_parent_window(win);
	}
	uw->set_own_parent_window_name(parent->name);
	cout<<"glade_new_unisvgdialogbutton parent_window_name="<<parent->name<<endl;
	
	return w;
}
//----------------------------------------------------------
template <class UType> static GtkWidget *glade_new_indicatortwo(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
  UType *uw = manage(new UType);
  GtkWidget *w = GTK_WIDGET(uw->gobj());
  GObjectClass *oclass = G_OBJECT_GET_CLASS(w);
//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);

  for (unsigned int i=0; i<info->n_properties; i++) {
    Glib::ustring name_prop(info->properties[i].name);
    const gchar *name;
    if(name_prop == "tooltip")
      name = "tooltip-text";
    else
      name = info->properties[i].name;
    const gchar *value = info->properties[i].value;
    GParamSpec *pspec;

    pspec = g_object_class_find_property (oclass, name);
    if (pspec) {
      GValue gvalue = { 0 };

      if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
        g_object_set_property(G_OBJECT(w), name, &gvalue);
        g_value_unset(&gvalue);
      }
    }
  }

  UPostcreate<IndicatorTwoState>::run(uw);

  return w;
}
//----------------------------------------------------------
template <class UType> static GtkWidget *glade_new_indicatorfour(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
  UType *uw = manage(new UType);
  GtkWidget *w = GTK_WIDGET(uw->gobj());
  GObjectClass *oclass = G_OBJECT_GET_CLASS(w);
//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);

  for (unsigned int i=0; i<info->n_properties; i++) {
    Glib::ustring name_prop(info->properties[i].name);
    const gchar *name;
    if(name_prop == "tooltip")
      name = "tooltip-text";
    else
      name = info->properties[i].name;
    const gchar *value = info->properties[i].value;
    GParamSpec *pspec;

    pspec = g_object_class_find_property (oclass, name);
    if (pspec) {
      GValue gvalue = { 0 };

      if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
        g_object_set_property(G_OBJECT(w), name, &gvalue);
        g_value_unset(&gvalue);
      }
    }
  }

  UPostcreate<IndicatorFourState>::run(uw);

  return w;
}
//----------------------------------------------------------
template <class UType> static GtkWidget *glade_new_gdg(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
  UType *uw = manage(new UType);
  GtkWidget *w = GTK_WIDGET(uw->gobj());
  GObjectClass *oclass = G_OBJECT_GET_CLASS(w);
//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);

  for (unsigned int i=0; i<info->n_properties; i++) {
    Glib::ustring name_prop(info->properties[i].name);
    const gchar *name;
    if(name_prop == "tooltip")
      name = "tooltip-text";
    else
      name = info->properties[i].name;
    const gchar *value = info->properties[i].value;
    GParamSpec *pspec;

    pspec = g_object_class_find_property (oclass, name);
    if (pspec) {
      GValue gvalue = { 0 };

      if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
        g_object_set_property(G_OBJECT(w), name, &gvalue);
        g_value_unset(&gvalue);
      }
    }
  }

  UPostcreate<GDG>::run(uw);

  return w;
}
//----------------------------------------------------------
template <class UType> static GtkWidget *glade_new_vdg(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
  UType *uw = manage(new UType);
  GtkWidget *w = GTK_WIDGET(uw->gobj());
  GObjectClass *oclass = G_OBJECT_GET_CLASS(w);
//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);

  for (unsigned int i=0; i<info->n_properties; i++) {
    Glib::ustring name_prop(info->properties[i].name);
    const gchar *name;
    if(name_prop == "tooltip")
      name = "tooltip-text";
    else
      name = info->properties[i].name;
    const gchar *value = info->properties[i].value;
    GParamSpec *pspec;

    pspec = g_object_class_find_property (oclass, name);
    if (pspec) {
      GValue gvalue = { 0 };

      if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
        g_object_set_property(G_OBJECT(w), name, &gvalue);
        g_value_unset(&gvalue);
      }
    }
  }

  UPostcreate<VDG>::run(uw);

  return w;
}
//----------------------------------------------------------
static GtkWidget
*proxywidget_glade_new(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
//GtkWidgetClass *widget_class = GTK_WIDGET_GET_CLASS(w);
  Glib::ustring module_name;
  Glib::ustring manager_name;
  Glib::ustring alive_sensor;
  Glib::ustring confirm_sensor;
  Glib::ustring auto_confirm_time_str;

  for (unsigned int i=0; i<info->n_properties ; i++) {
    Glib::ustring name(info->properties[i].name);
    if(name == "tooltip")
      name = "tooltip-text";
    if ( name == "module_name" ) {
      module_name = info->properties[i].value;
    }
    if ( name == "manager_name" ) {
      manager_name = info->properties[i].value;
    }
    if ( name == "alive_sensor" ) {
      alive_sensor = info->properties[i].value;
    }
    if ( name == "confirm_sensor" ) {
      confirm_sensor = info->properties[i].value;
    }
    if ( name == "auto_confirm_time_str" ) {
      auto_confirm_time_str = info->properties[i].value;
    }
  }

  UProxyWidget *proxy_widget = manage(new UProxyWidget(module_name, manager_name
                                                                                                  ,alive_sensor
                                                                                                  ,confirm_sensor
                                                                                                  ,auto_confirm_time_str));
  GtkWidget *w = GTK_WIDGET(proxy_widget->gobj());
  GObjectClass *oclass = G_OBJECT_GET_CLASS(w);

  for (unsigned int i=0; i<info->n_properties; i++) {
    Glib::ustring name_prop(info->properties[i].name);
    const gchar *name;
    if(name_prop == "tooltip")
      name = "tooltip-text";
    else
      name = info->properties[i].name;
    if (Glib::ustring(name) == "module_name")
      continue;
    if (Glib::ustring(name) == "manager_name")
      continue;
    if (Glib::ustring(name) == "alive_sensor")
      continue;
    if (Glib::ustring(name) == "confirm_sensor")
      continue;
    if (Glib::ustring(name) == "auto_confirm_time_str")
      continue;

    const gchar *value = info->properties[i].value;
    GParamSpec *pspec;

    pspec = g_object_class_find_property (oclass, name);
    if (pspec) {
      GValue gvalue = { 0 };

      if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
        g_object_set_property(G_OBJECT(w), name, &gvalue);
        g_value_unset(&gvalue);
      }
    }
  }

// UPostcreateProxyWidget::run(w);
// UPostcreate<GtkWidget>::run(w);

  return w;
}

static GtkWidget
*usvgindicatorplus_glade_new(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
  Glib::ustring image0_path;
  Glib::ustring image1_path;
  int n=0;
  for (unsigned int i=0; i<info->n_properties && n<2; i++) {
    Glib::ustring name(info->properties[i].name);
    if(name == "tooltip")
      name = "tooltip-text";
    if (name == "image0_path") {
      image0_path = info->properties[i].value;
      n++;
    }
    if (name == "image1_path") {
      image1_path = info->properties[i].value;
      n++;
    }
  }
  USVGIndicatorPlus *usvgindicatorplus = manage(new USVGIndicatorPlus(image0_path, image1_path));
  GtkWidget *w = GTK_WIDGET(usvgindicatorplus->gobj());
  GObjectClass *oclass = G_OBJECT_GET_CLASS(w);

  for (unsigned int i=0; i<info->n_properties; i++) {
    Glib::ustring name_prop(info->properties[i].name);
    const gchar *name;
    if(name_prop == "tooltip")
      name = "tooltip-text";
    else
      name = info->properties[i].name;

    if (Glib::ustring(name) == "image0-path")
      continue;
    if (Glib::ustring(name) == "image1-path")
      continue;

    const gchar *value = info->properties[i].value;
    GParamSpec *pspec;

    pspec = g_object_class_find_property (oclass, name);
    if (pspec) {
    GValue gvalue = { 0 };

    if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
      g_object_set_property(G_OBJECT(w), name, &gvalue);
      g_value_unset(&gvalue);
    }
    }
  }

// UPostcreateSvgIndicatorPlus::run(w);
// UPostcreate<GtkWidget>::run(w);

  return w;
}

static GtkWidget
*usvgbuttonindicator_glade_new(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
  Glib::ustring image0_path;
  Glib::ustring image1_path;
  int n=0;
  for (unsigned int i=0; i<info->n_properties && n<2; i++) {
    Glib::ustring name(info->properties[i].name);
    if(name == "tooltip")
      name = "tooltip-text";
    if (name == "image0_path") {
      image0_path = info->properties[i].value;
      n++;
    }
    if (name == "image1_path") {
      image1_path = info->properties[i].value;
      n++;
    }
  }
  USVGButtonIndicator *usvgbuttonindicator = manage(new USVGButtonIndicator(image0_path, image1_path));
  GtkWidget *w = GTK_WIDGET(usvgbuttonindicator->gobj());
  GObjectClass *oclass = G_OBJECT_GET_CLASS(w);

  for (unsigned int i=0; i<info->n_properties; i++) {
    Glib::ustring name_prop(info->properties[i].name);
    const gchar *name;
    if(name_prop == "tooltip")
      name = "tooltip-text";
    else
      name = info->properties[i].name;

    if (Glib::ustring(name) == "image0-path")
        continue;
    if (Glib::ustring(name) == "image1-path")
        continue;

    const gchar *value = info->properties[i].value;
    GParamSpec *pspec;

    pspec = g_object_class_find_property (oclass, name);
    if (pspec) {
      GValue gvalue = { 0 };

      if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
        g_object_set_property(G_OBJECT(w), name, &gvalue);
        g_value_unset(&gvalue);
      }
    }
  }

// UPostcreateSvgIndicator::run(w);
// UPostcreate<GtkWidget>::run(w);

  return w;
}

static GtkWidget
*usvgindicatorbig_glade_new(GladeXML *xml, GType widget_type, GladeWidgetInfo *info)
{
  Glib::ustring image0_path;
  Glib::ustring image1_path;
  Glib::ustring name;
  int n=0;
  for (unsigned int i=0; i<info->n_properties && n<3; i++) {
    Glib::ustring name(info->properties[i].name);
    if(name == "tooltip")
      name = "tooltip-text";
    if ( name == "falsefile" ) {
      image0_path = info->properties[i].value;
      n++;
    }
    if ( name == "truefile" ) {
      image1_path = info->properties[i].value;
      n++;
    }
    if ( name == "name" ) {
      name = info->properties[i].value;
      n++;
    }
  }

  USVGIndicatorBig *usvgindicatorbig = manage(new USVGIndicatorBig(image0_path, image1_path));
  GtkWidget *w = GTK_WIDGET(usvgindicatorbig->gobj());
  GObjectClass *oclass = G_OBJECT_GET_CLASS(w);

  for (unsigned int i=0; i<info->n_properties; i++) {
    Glib::ustring name_prop(info->properties[i].name);
    const gchar *name;
    if(name_prop == "tooltip")
      name = "tooltip-text";
    else
      name = info->properties[i].name;

    if (Glib::ustring(name) == "falsefile")
      continue;
    if (Glib::ustring(name) == "truefile")
      continue;

    const gchar *value = info->properties[i].value;
    GParamSpec *pspec;

    pspec = g_object_class_find_property (oclass, name);
    if (pspec) {
      GValue gvalue = { 0 };

      if (glade_xml_set_value_from_string(xml, pspec, value, &gvalue)) {
        g_object_set_property(G_OBJECT(w), name, &gvalue);
        g_value_unset(&gvalue);
      }
    }
  }

// UPostcreateSvgIndicatorBig::run(w);
// UPostcreate<GtkWidget>::run(w);

  return w;
}

static void
notebook_build_children(GladeXML *self, GtkWidget *parent,
                                        GladeWidgetInfo *info)
{
  unsigned int i, j, tab = 0;
  enum {
    PANE_ITEM,
    TAB_ITEM,
    MENU_ITEM
  } type;

  g_object_ref(G_OBJECT(parent));
  for (i = 0; i < info->n_children; i++) {
    GladeWidgetInfo *childinfo = info->children[i].child;
    GtkWidget *child = glade_xml_build_widget(self, childinfo);

    type = PANE_ITEM;
    for (j = 0; j < info->children[i].n_properties; j++) {
      if (!strcmp (info->children[i].properties[j].name, "type")) {
        const char *value = info->children[i].properties[j].value;

        if (!strcmp (value, "tab"))
          type = TAB_ITEM;
        break;
      }
    }

    if (type == TAB_ITEM) { /* The GtkNotebook API blows */
      GtkWidget *body;

    body = gtk_notebook_get_nth_page (GTK_NOTEBOOK (parent), (tab - 1));
    gtk_notebook_set_tab_label (GTK_NOTEBOOK (parent), body, child);
    } else {
    gtk_notebook_append_page (GTK_NOTEBOOK (parent), child, NULL);
    tab++;
    }
  }
  g_object_unref(G_OBJECT(parent));
}

static void slidenotebook_build_children(GladeXML *self, GtkWidget *parent,GladeWidgetInfo *info)
{
	g_object_ref(G_OBJECT(parent));
	GladeWidgetInfo *scrollinfo = info->children[0].child;
	std::cout<< info->classname <<" !!!!!!!!!!!!!!!!!!!!!!!!!! "<< info->name  <<std::endl;
	GladeWidgetInfo *vboxinfo = scrollinfo->children[0].child;
	for (int i = 0; i < vboxinfo->n_children; i++)
	{
		GladeWidgetInfo *childinfo = vboxinfo->children[i].child;
		GtkWidget *child = glade_xml_build_widget(self, childinfo);
	}
	g_object_unref(G_OBJECT(parent));
//	Glib::RefPtr<Glib::Object> parentWidget = Glib::wrap(parent);

}

//----------------------------------------------------------
#endif
