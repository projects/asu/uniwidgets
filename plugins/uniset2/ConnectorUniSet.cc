#include "ConnectorUniSet.h"
#include "uniwidgets/ConfirmSignal.h"
//#include "uniwidgets/CheckedSignal.h"
#include <iostream>
#include <glibmm/main.h>
#include <glibmm/ustring.h>
#include <UniSetActivator.h>
#include <Configuration.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace UniSetTypes;
const int POLL_TIME = 100;
// -------------------------------------------------------------------------
ConnectorUniSet::ConnectorUniSet()
{
	if(UniSetTypes::uniset_conf() == nullptr) //shouldn't connect
		throw Exception("NOT IN UNISET");
	UniWidgetsTypes::uniwidgets_init(UniSetTypes::uniset_conf()->getArgc(), UniSetTypes::uniset_conf()->getArgv());
}
// -------------------------------------------------------------------------
void ConnectorUniSet::init_connector(const Glib::ustring& manager_name
				,const Glib::ustring& alive_sensor
				,const Glib::ustring& confirm_sensor
				,const Glib::ustring& auto_confirm_time_str)
{
	int poll_time = uniset_conf()->getArgPInt("--connector-poll-time",POLL_TIME);

	if (manager_name.empty()) {
		ucrit << "(ConnectorUniSet): ObjectName not set. Parameter manager_name is empty";
	}
	if (alive_sensor.empty()) {
		ucrit << "(ConnectorUniSet): AliveSensor not set. Parameter alive_sensor is empty";
	}
	if (confirm_sensor.empty()) {
		ucrit << "(ConnectorUniSet): ConfirmSensor not set. Parameter confirm_sensor is empty";
	}

	ObjectId alive_sensor_id(uni_atoi(alive_sensor));
	ObjectId confirm_sensor_id(uni_atoi(confirm_sensor));
	ObjectId oid = DefaultObjectId;
	try {
		oid = uniset_conf()->getObjectID( manager_name );
	}
	catch (Exception& ex) {
		ucrit << "(ConnectorUniSet):" << ex << endl;
	}

	int auto_confirm_time = uni_atoi(auto_confirm_time_str);

	gpm_ = make_shared<GuiPM>(const_cast<ConnectorUniSet*>(this), oid, alive_sensor_id, confirm_sensor_id, auto_confirm_time);
	gpm_->set_msg_amount(uniset_conf()->getArgPInt("--connector-msg-amount",40));
	gpm_->set_log(uniset_conf()->getArgPInt("--connector-set-log",0));
	gpm_->set_wait_ready_timeout(uniset_conf()->getArgPInt("--connector-wait-ready-timeout",30000));
	gpm_->set_connection_timeout(uniset_conf()->getArgPInt("--connector-timeout",0));
	SignalInfoHolder::debug = uniset_conf()->getArgPInt("--signals-debug",0);
	SignalInfoHolder::set_long_time(uniset_conf()->getArgPInt("--signals-long-interval",0));
	SignalInfoHolder::set_short_time(uniset_conf()->getArgPInt("--signals-short-interval",0));

	try
	{
		uinfo << "(ConnectorUniSet): Activating." << endl;

		auto objects_activator_ = UniSetActivator::Instance();
		objects_activator_->add(gpm_);
		objects_activator_->run(true);
		msleep(500);
		SystemMessage sm(SystemMessage::StartUp);
		objects_activator_->broadcast(sm.transport_msg());
		msleep(500);
		if( poll_time )
			connection_poll_ = Glib::signal_timeout().
				connect(sigc::mem_fun(*gpm_, &GuiPM::poll),poll_time);
	}
	catch (Exception& ex)
	{
		ucrit << "(ConnectorUniSet):" << ex << endl;
	}
	catch (CORBA::SystemException& ex) {
		ucrit << "(ConnectorUniSet):" << ex.NP_minorString() << endl;
	}
	catch (CORBA::Exception& ex) {
		ucrit << "(ConnectorUniSet):CORBA::Exception:" << ex._name() << endl;
	}
	catch ( ... ) {
		ucrit << "(ConnectorUniSet): catch (...)" << endl;
	}
}
// -------------------------------------------------------------------------
ConnectorUniSet::~ConnectorUniSet()
{
	try {
		connection_poll_.disconnect();
		
		SystemMessage sm(SystemMessage::Finish);
		auto objects_activator_ = UniSetActivator::Instance();
		objects_activator_->broadcast(sm.transport_msg());
		objects_activator_->remove(gpm_);
		gpm_.reset();
	}
	catch (Exception& ex)
	{
		ucrit << "(ConnectorUniSet):" << ex << endl;
	}
	catch (CORBA::SystemException& ex) {
		ucrit << "(ConnectorUniSet):" << ex.NP_minorString() << endl;
	}
	catch (CORBA::Exception& ex) {
		ucrit << "(ConnectorUniSet):CORBA::Exception:" << ex._name() << endl;
	}
	catch ( ... ) {
		ucrit << "(ConnectorUniSet): catch (...)" << endl;
	}
}
// -------------------------------------------------------------------------
bool
ConnectorUniSet::connected()
{
	return gpm_->connected();
}
// -------------------------------------------------------------------------
sigc::signal<void>&
ConnectorUniSet::signal_connected()
{
	return gpm_->signal_connected;
}
// -------------------------------------------------------------------------
sigc::signal<void>&
ConnectorUniSet::signal_disconnected()
{
	return gpm_->signal_disconnected;
}
// -------------------------------------------------------------------------
sigc::signal<void>&
ConnectorUniSet::signal_startup()
{
	return gpm_->signal_startup;
}
// -------------------------------------------------------------------------
sigc::signal<void, UniversalIO::UIOCommand>&
ConnectorUniSet::signal_ask_sensors()
{
	return gpm_->signal_ask_sensors;
}
// -------------------------------------------------------------------------
SmartSignal<void, const UniSetTypes::SensorMessage*>&
ConnectorUniSet::signal_sensor_info()
{
	return gpm_->signal_sensor_info;
}
// -------------------------------------------------------------------------
std::shared_ptr<UInterface>
ConnectorUniSet::get_uin()
{
	return gpm_->uin;
}
// -------------------------------------------------------------------------
CheckedSignal&
ConnectorUniSet::signal_checked()
{
	return *(gpm_->signal_checked);
}
// -------------------------------------------------------------------------
ConfirmSignal&
ConnectorUniSet::signal_confirm()
{
	return *(gpm_->signal_confirm);
}
// -------------------------------------------------------------------------
void
ConnectorUniSet::set_process_signal_confirm(UMessages::MessageId id)
{
	sigc::slot<void, UMessages::MessageId, time_t> confirm = sigc::mem_fun( this, &ConnectorUniSet::confirm );
	signal_confirm().connect( confirm, id );	
}
// -------------------------------------------------------------------------
void
ConnectorUniSet::confirm(UMessages::MessageId id, time_t sec)
{
	if(UniSetTypes::uniset_conf()->getDBServer() == DefaultObjectId)
		return;
	UMessages::Message message =
		signals().get_message(id);

	if(!message.valid())
		return;

	timespec confirm_delay = { (sec - message.getLastTime()), 0 };

	/* type = UniSetTypes::ConfirmInfo */
	timespec message_tm = { message.getLastTime(), message.getLastTimeNsec() };
	ConfirmMessage cm(id._id,
			  id._value,
			  message_tm,
			  confirm_delay
			 );//Priority in_priority = Message::Medium

	TransportMessage tm( std::move(cm.transport_msg()) );
	try{
		send(UniSetTypes::uniset_conf()->getDBServer(), tm);
	}
	catch(...){};
}
// -------------------------------------------------------------------------
USignals::SigController&
ConnectorUniSet::signals()
{
	return gpm_->_signals;
}
// -------------------------------------------------------------------------
long
ConnectorUniSet::get_value(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node)
{
	return gpm_->_signals.get_value(id,node);	
}
// -------------------------------------------------------------------------
long
ConnectorUniSet::get_value_directly(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node)
{
	assert (gpm_->connected());
	return gpm_->getValue( id, node );
}
// -------------------------------------------------------------------------
float
ConnectorUniSet::get_analog_value(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node)
{
	return gpm_->_signals.get_analog_value(id,node);	
}
// -------------------------------------------------------------------------
void
ConnectorUniSet::save_value(const long value, const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node)
{
	gpm_->saveValue(value,id,node);
}
// -------------------------------------------------------------------------
void
ConnectorUniSet::send(UniSetTypes::ObjectId name, UniSetTypes::TransportMessage& msg)
{
	gpm_->send(name, msg);
}
// -------------------------------------------------------------------------
UniWidgetsTypes::ObjectInfo
ConnectorUniSet::get_info(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node)

{
    IOController_i::SensorInfo si;
    try
    {
      si.id = id;
      si.node = node;
      IOController_i::CalibrateInfo calibrate = gpm_->uin->getCalibrateInfo(si);
      UniWidgetsTypes::ObjectInfo oi;
      oi.precision = calibrate.precision;
      oi.min = calibrate.minCal;
      oi.max = calibrate.maxCal;
      return oi;
    }
    catch(UniSetTypes::Exception& ex){ucrit << ex << endl;}
    return UniWidgetsTypes::ObjectInfo();
}
// -------------------------------------------------------------------------
Glib::ustring
ConnectorUniSet::get_type() const
{
	return "uniset2";
}
// -------------------------------------------------------------------------
xmlNode*
ConnectorUniSet::get_xml_node( const std::string& node, const std::string& name )
{
	auto unixml = uniset_conf()->getConfXML();
	return unixml->findNode(unixml->getFirstNode(), node, name);
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, const UMessages::MessageId& t )
{
	std::ostringstream tmpos;
	tmpos << UniSetTypes::uniset_conf()->oind->getBaseName( UniSetTypes::uniset_conf()->oind->getNameById(t._id) );
	tmpos << "(" << std::setw(6) << t._id << ")";
	return os << tmpos.str();
}
// -------------------------------------------------------------------------
template<>
std::ostream& operator<<( std::ostream& os, const SignalInfoKey<UniWidgetsTypes::ObjectId>& t )
{
	std::ostringstream tmpos;
	tmpos << UniSetTypes::uniset_conf()->oind->getBaseName( UniSetTypes::uniset_conf()->oind->getNameById(t.key) );
	tmpos << "(" << std::setw(6) << t.key << ")";
	return os << tmpos.str();
}
// -----------------------------------------------------------------------------------------
extern "C" std::shared_ptr<Connector> create_connector()
{
	return std::shared_ptr<Connector>(new ConnectorUniSet(), ConnectorDeleter());
}
// -------------------------------------------------------------------------
