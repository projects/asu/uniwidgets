#ifndef _CONNECTORUNISET_H
#define _CONNECTORUNISET_H
// -------------------------------------------------------------------------
#include "uniwidgets/Connector.h"
#include "GuiPM.h"
#include <UniSetTypes.h>
#include <UInterface.h>
#include "types.h"
// -------------------------------------------------------------------------
class CheckedSignal;
class ConfirmSignal;
// -------------------------------------------------------------------------
/*!
 * \brief Коннектор.
 * \par
 * Класс реализует коннектор к SharedMemory.
*/
class ConnectorUniSet : public Connector
{
public: 
	ConnectorUniSet();
	virtual void init_connector(const Glib::ustring& manager_name
			,const Glib::ustring& alive_sensor
			,const Glib::ustring& confirm_sensor
			,const Glib::ustring& auto_confirm_time_str) override;
	virtual ~ConnectorUniSet();

	/* Signals */
	virtual sigc::signal<void>& signal_connected() override;				/*!< см. Connector::signal_connected */
	virtual sigc::signal<void>& signal_disconnected() override;				/*!< см. Connector::signal_disconnected */
	virtual sigc::signal<void>& signal_startup() override;					/*!< см. Connector::signal_startup */
	virtual sigc::signal<void, UniversalIO::UIOCommand>& signal_ask_sensors();		/*!< см. Connector::signal_ask_sensors */
	virtual SmartSignal<void, const UniSetTypes::SensorMessage*>& signal_sensor_info();	/*!< см. Connector::signal_sensor_info */

	virtual CheckedSignal& signal_checked() override;	/*!< см. Connector::signal_checked */
	virtual ConfirmSignal& signal_confirm() override;	/*!< см. Connector::signal_confirm */
	virtual void set_process_signal_confirm(UMessages::MessageId id) override;		/*!< установить обработку квитирования коннектором*/
	virtual USignals::SigController& signals() override;	/*!< см. Connector::signals */

	/* Methods */
	virtual bool connected() override;			/*!< см. Connector::connected */

	std::shared_ptr<UInterface> get_uin();		/*!< см. Connector::get_uin */
	//virtual ObjectId get_id();			/*!< см. Connector::get_id */
	/*! см. Connector::get_info */
	virtual UniWidgetsTypes::ObjectInfo get_info(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) override;
	/*! см. Connector::get_value */
	virtual long get_value(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) override;
	/*! см. Connector::get_value_from_gpm */
 	virtual long get_value_directly(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) override;
	/*! см. Connector::get_analog_value */
	virtual float get_analog_value(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) override;
	/*! см. Connector::save_value */
	virtual void save_value(const long value, const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) override;
	/*! см. Connector::get_type */
	virtual Glib::ustring get_type() const override;
	/*! см. Connector::get_xml_node */
	virtual xmlNode* get_xml_node( const std::string& node, const std::string& name = "" ) override;
	/*! см. Connector::send */
	virtual void send(UniSetTypes::ObjectId name, UniSetTypes::TransportMessage& msg);
	virtual UniWidgetsTypes::ObjectId getLocalNode() override { return UniSetTypes::uniset_conf()->getLocalNode(); };
	virtual UniWidgetsTypes::ObjectId getSensorID( const std::string& name ) override { return UniSetTypes::uniset_conf()->getSensorID(name); };
	virtual UniWidgetsTypes::ObjectId getNodeID( const std::string& name ) override { return UniSetTypes::uniset_conf()->getNodeID(name); };
	//virtual void askRemoteSensor( const UniWidgetsTypes::ObjectId id, UniversalIO::UIOCommand cmd, const UniWidgetsTypes::ObjectId node,
	//						  UniWidgetsTypes::ObjectId backid = UniWidgetsTypes::DefaultObjectId ){ get_uin()->askRemoteSensor(id, cmd, node, backid); };

protected:
	void confirm(UMessages::MessageId id, time_t sec);

private:
	std::shared_ptr<GuiPM> gpm_;
	sigc::connection connection_poll_;
};
std::ostream& operator<<( std::ostream& os, const UMessages::MessageId& t );
template<> std::ostream& operator<<( std::ostream& os, const SignalInfoKey<UniWidgetsTypes::ObjectId>& t );
// -------------------------------------------------------------------------
#endif
