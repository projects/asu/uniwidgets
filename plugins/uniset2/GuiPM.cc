#include "GuiPM.h"
#include "uniwidgets/ConfirmSignal.h"
#include "uniwidgets/CheckedSignal.h"
#include <iostream>
#include <glibmm/main.h>
#include <glibmm/ustring.h>
#include <UniSetActivator.h>
#include <SigControllerUniSet.h>
#include "thread.h"
#undef atoi
// -------------------------------------------------------------------------
using namespace std;
using namespace UniSetTypes;
// -------------------------------------------------------------------------
GuiPM::GuiPM(Connector* connref, ObjectId oid, ObjectId alive_sensor_id, ObjectId confirm_sensor_id, int auto_confirm_time) :
	ProxyManager(oid),
	connected_(false),
	signal_sensor_info("signal_sensor_info"),
	_signals(new USignals::SigControllerUniSet(this)), // ~SigController will delete it
	alive_sensor_id_(alive_sensor_id),
	confirm_sensor_id_(confirm_sensor_id),
	msg_amount(40),
	log(false)
{
	thread(false);
	if( uin )
		uin->setCacheMaxSize(5000);

	signal_checked = new CheckedSignal;
	signal_confirm = new ConfirmSignal(connref);
	signal_confirm->set_autoconfirm_time(auto_confirm_time);

	if( uniset_conf() ) {
		long tmp = uniset_conf()->getArgInt("--pulsar-msec", "3000");
		timeout_ = tmp + (2*tmp)/3;
	}
}
// -------------------------------------------------------------------------
GuiPM::~GuiPM()
{
	delete signal_checked;
	delete signal_confirm;
}
// -------------------------------------------------------------------------
bool GuiPM::poll()
{
	try {
		time_t t1;
		if(log)
			t1 = time(NULL);
		int i=0;
		for( ; i<msg_amount; i++ ) {
			auto msg = receiveMessage();
			if( !msg )
				break;
			processingMessage(msg.get());
		}
		if(log)
		{
			time_t dt = time(NULL) - t1;
			ucrit << "(poll): " << i << " for " << dt << " msec" << endl;
		}
	}
	catch(Exception& ex) {
//		dlog[Debug::CRIT] << "(MainWindow): " << ex << endl;
		ucrit << "(GuiPM): " << ex << endl;
	}
	catch(...) {
//		dlog[Debug::CRIT] << "(MainWindow): catch ..." <<endl; 
		ucrit << "(GuiPM): catch ..." << endl;
	}
	return true;
}
// -------------------------------------------------------------------------
void GuiPM::processingMessage( const VoidMessage *msg )
{
	try {
		switch( msg->type ) {
			case Message::SensorInfo: { 
				SensorMessage sm( msg );
				sensorInfo(&sm);
				break;
			}

			case Message::SysCommand: {
				SystemMessage sysm( msg );
				sysCommand( &sysm );
				ProxyManager::processingMessage(msg);
				break;
			}

			case Message::Timer: {
				TimerMessage tm(msg);
				timerInfo(&tm);
				ProxyManager::processingMessage(msg);
				break;
			}

			default: {
				ProxyManager::processingMessage(msg);
				break;
			}
		}
	}
	catch(Exception& ex) {
		ucrit<<"["<<myname<<"](processingMessage): " << ex << std::endl;
	}
}
// -------------------------------------------------------------------------
void GuiPM::sysCommand( const SystemMessage* sm )
{
	switch( sm->command ) {
		case SystemMessage::StartUp:
			ucrit<<"["<<myname<<"](sysCommand): StartUP message recived."<<endl;
			signal_startup.emit();
			startUp();
			break;

		case SystemMessage::FoldUp:
			ucrit<<"["<<myname<<"](sysCommand): FoldUp message recived."<<endl;
			break;

		case SystemMessage::Finish:
			ucrit<<"["<<myname<<"](sysCommand): Finish message recived."<<endl;
			break;

		case SystemMessage::WatchDog:
			ucrit<<"["<<myname<<"](sysCommand): WatchDog message recived."<<endl;
			watchDog();
			break;

		default:
			ucrit<<"["<<myname<<"](sysCommand): Unknown system command message recived."<<endl;
			break;
	}
}
// -------------------------------------------------------------------------
void
GuiPM::timerInfo( const TimerMessage* tm )
{
}
// -------------------------------------------------------------------------
void
GuiPM::askSensors( UniversalIO::UIOCommand cmd )
{
	bool no_errors = false;
	try {
//		confirm_sensor_id_ = uniset_conf()->getArgPInt("--cbid", DefaultObjectId);
		if (confirm_sensor_id_ != DefaultObjectId)
			uin->askSensor(confirm_sensor_id_, UniversalIO::UIONotifyChange, getId());
		else
			ucrit<<"["<<myname<<"](Warning): Confim not set. Start without confirm."<<endl;

		signal_ask_sensors.emit( cmd );
		no_errors = true;
	}
	catch( Exception& ex ) {
		ucrit <<"["<< myname << "](GuiPM::askSensors): " << ex << endl;
	}
}
// -------------------------------------------------------------------------
void
GuiPM::askSensor(ObjectId id, ObjectId node, UniversalIO::UIOCommand cmd) throw()
{
	if (!connected_) {
		ucrit <<"["<<myname<<"](Warning)(GuiPM::askSensor): failed to ask ("<<id<<":"<<node<<"). Not active." << endl;
		return;
	}

	//TODO: remove if UniSet does it;
	if (node == DefaultObjectId)
		node = uniset_conf()->getLocalNode();

	try {
		uin->askRemoteSensor(id, cmd, node, getId());
	}
	catch (IOBadParam& ex) {
		ucrit << "[" << myname << "]:(Warning)(GuiPM::askSensor):" << ex << endl;
	}
	catch (TimeOut& ex) {
		ucrit << "[" << myname << "](Warning)(GuiPM::askSensor): ask time out." << endl;
	}
	catch (Exception& ex) {
		ucrit << "[" << myname << "](GuiPM::askSensor):" << ex << endl;
	}
}
// -------------------------------------------------------------------------
long
GuiPM::getValue(UniSetTypes::ObjectId id, UniSetTypes::ObjectId node)
{
	if (!connected_) {
		ucrit <<"["<<myname<<"](Warning)(GuiPM::getValue): failed to get value of ("<<id<<":"<<node<<"). Not active." << endl;
		return 0;
	}

	if (node == DefaultObjectId)
		node = uniset_conf()->getLocalNode();

	try
	{
		return uin->getValue( id, node );
	}
	catch (Exception& ex)
	{
		ucrit << "[" << myname <<"](Warning)(GuiPM::getValue): error while getting value of "
		     << id << ":" << node
		     << " || " << ex << endl;
	}
	catch( std::exception& ex )
	{
		ucrit << "[" << myname <<"](Warning)(GuiPM::getValue): " << ex.what() << " of ("<<id<<":"<<node<<")" << endl;
		// throw ex;
	}
	
	return 0;
}
// -------------------------------------------------------------------------
void
GuiPM::saveValue( long value, UniSetTypes::ObjectId id, UniSetTypes::ObjectId node)
{
	if (!connected_) {
		ucrit <<"["<<myname<<"](Warning): failed to save value ("<<id<<":"<<node<<"). Not active." << endl;
		return;
	}

	try
	{
		if (node == DefaultObjectId)
			node = uniset_conf()->getLocalNode();

		uin->setValue( id, value, node );
	}
	catch( std::exception& ex )
	{
		ucrit << "[" << myname <<"](Warning)(GuiPM::saveValue): " << ex.what() << " of ("<<id<<":"<<node<<")" << endl;
	}
}
// -------------------------------------------------------------------------
void GuiPM::send(ObjectId name, TransportMessage& msg)
{
	uin->send(name, msg);
}
// -------------------------------------------------------------------------
void
GuiPM::startUp()
{
	if( alive_sensor_id_ != UniSetTypes::DefaultObjectId && !ui->waitReady(alive_sensor_id_, wait_ready_timeout_) )
		return;
	try {
		if ( alive_sensor_id_ != UniSetTypes::DefaultObjectId)
		{
			uin->askRemoteSensor(alive_sensor_id_, UniversalIO::UIONotify,uniset_conf()->getLocalNode(), getId());
			alive_value_ = uin->getValue(alive_sensor_id_, uniset_conf()->getLocalNode());

			to_connection_ = Glib::signal_timeout().
				connect(sigc::mem_fun(this, &GuiPM::connectionTimeOut), timeout_);
		}
		connected_ = true;
	}
	catch(UniSetTypes::Exception& ex) {
		ucrit << ex << endl;
	}

	if (!connected_)
		return;

	ucrit << "GuiPM connected" << endl;
	signal_connected.emit();
	askSensors(UniversalIO::UIONotify);

//  	if (auto_confirm_time_ > 0)
//  		Glib::signal_timeout().connect(sigc::mem_fun(this, &GuiPM::auto_confirm), auto_confirm_time_);
}
// -------------------------------------------------------------------------
bool GuiPM::connectionTimeOut()
{
	connected_ = false;
	ucrit << "GuiPM disconnected" << endl;
	signal_disconnected.emit();
	return false;
}
// -------------------------------------------------------------------------
void
GuiPM::sensorInfo(const SensorMessage* sm)
{
	if ( connected_ ) {
		if ( sm->id == alive_sensor_id_ ) {
			to_connection_.disconnect();
			to_connection_ = Glib::signal_timeout().
				connect(sigc::mem_fun(this, &GuiPM::connectionTimeOut), timeout_);
		}

//		if ( auto_confirm_time_ <= 0 &&
		if (sm->id == confirm_sensor_id_ && sm->value)
		{
			signal_checked->emit();
			time_t sec = sm->sm_tv.tv_sec;
			signal_confirm->emit(sec);
		}
		signal_sensor_info.emit( sm );
	}
	else {
		if ( sm->id == alive_sensor_id_ ) {
			ucrit << "*WARNING* SensorInfo recieved after timeout" << endl;
			connected_ = true;
			ucrit << "GuiPM connected" << endl;
			signal_connected.emit();
			askSensors(UniversalIO::UIONotify);
			
			to_connection_.disconnect();
			to_connection_ = Glib::signal_timeout().
				connect(sigc::mem_fun(this, &GuiPM::connectionTimeOut), timeout_);
		}
	}
}
// -------------------------------------------------------------------------
void
GuiPM::watchDog()
{
	if(connected_) {
		to_connection_.disconnect();
		connected_ = false;
		ucrit << "GuiPM disconnected" << endl;
		signal_disconnected.emit();
	}

	startUp();
}
// -------------------------------------------------------------------------
// bool
// GuiPM::auto_confirm()
// {
// 	//TODO: use true time of confirm
// 	signal_confirm->emit(0);
// 
// 	if (!connected_)
// 		return false;
// 	else
// 		return true;
// }
// -------------------------------------------------------------------------
UniversalIO::IOType
GuiPM::getIOType(ObjectId id, ObjectId node) 
{
	if (!connected_) {
		ucrit <<"["<<myname<<"](Warning): failed to ask ("<<id<<":"<<node<<"). Not active." << endl;
		return UniversalIO::DI;
	}

	//TODO: remove if UniSet does it;
	if (node == DefaultObjectId)
		node = uniset_conf()->getLocalNode();

	try {
		UniversalIO::IOType iotype = uin->getIOType(id, node);
		return iotype;
	}
	catch (IOBadParam& ex) {
		ucrit << "[" << myname << "]:(Warning):" << ex << endl;
		return UniversalIO::DI;
	}
	catch (TimeOut& ex) {
		ucrit << "[" << myname << "](Warning): get iotype time out." << endl;
		return UniversalIO::DI;
	}
	catch (Exception& ex) {
		ucrit << "[" << myname << "]:" << ex << endl;
		return UniversalIO::DI;
	}
}
// -------------------------------------------------------------------------
void gtk_thread_get_info(GuiPM* gpm, UniSetTypes::SimpleInfo* si, ::CORBA::Long userparam)
{
	ostringstream inf;
	if(userparam >= 0)
	{
		UniSetTypes::SimpleInfo_var i = gpm->ProxyManager::getInfo();
		inf << i->info << endl;
	}
	switch(userparam)
	{
		case -1:
			inf << gpm->get_info_buffer;
			break;
		case 0: 
			inf << "For more information use param:" << endl;
			inf << "	1 - [UniWidgets] all SM signal info" << endl;
			inf << "	2 - [UniWidgets] GuiPM::signal_sensor_info" << endl;
			inf << gpm->_signals.get_info( 0 );
			break;
		case 1:
			inf << gpm->signal_sensor_info.get_info();
			inf << gpm->_signals.get_info( 1 );
			break;
		case 2:
			inf << gpm->signal_sensor_info.get_info();
			break;
		default:
			inf << gpm->_signals.get_info( userparam );
			break;
	}
	gpm->get_info_buffer = "";
	std::string info_buffer = inf.str();
	if( info_buffer.size() > 2000000 )
	{
		string::size_type pos = info_buffer.find_first_of("\n", 2000000);
		if( pos != string::npos )
		{
			gpm->get_info_buffer = info_buffer.substr(pos);
			info_buffer = info_buffer.substr(0, pos) + "\n#warning: getInfo size > 2000000 chars!\nfor more output use param: -1";
		}
	}
	si->info = info_buffer.c_str();
}
// -------------------------------------------------------------------------
UniSetTypes::SimpleInfo* GuiPM::getInfo( ::CORBA::Long userparam )
{
	UniSetTypes::SimpleInfo* i = new SimpleInfo();
	GtkThread3<GuiPM*, UniSetTypes::SimpleInfo*, ::CORBA::Long>::execute(&gtk_thread_get_info, const_cast<GuiPM*>(this), i, userparam);
	return i;
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, const UniSetTypes::SensorMessage* t )
{
	return os << *t;
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, const UniSetTypes::SensorMessage& t )
{
	std::ostringstream tmpos;
	tmpos << UniSetTypes::uniset_conf()->oind->getBaseName( UniSetTypes::uniset_conf()->oind->getNameById(t.id) );
	tmpos << "(" << std::setw(6) << t.id << ")";
	return os << tmpos.str();
}
// -------------------------------------------------------------------------
bool operator<(const UniSetTypes::SensorMessage& lhs, const UniSetTypes::SensorMessage& rhs)
{
	return lhs.id < rhs.id;
}
// -------------------------------------------------------------------------
bool operator==(const UniSetTypes::SensorMessage& lhs, const UniSetTypes::SensorMessage& rhs)
{
	return lhs.id == rhs.id;
}
// -------------------------------------------------------------------------
std::size_t std::hash<const UniSetTypes::SensorMessage>::operator()(const UniSetTypes::SensorMessage& s) const
{
	return std::hash<UniSetTypes::ObjectId>()(s.id);
}
// -------------------------------------------------------------------------
