#ifndef _GUIPM_H
#define _GUIPM_H
// -------------------------------------------------------------------------
#include <ProxyManager.h>
#include "uniwidgets/USignals.h"
#include "uniwidgets/SmartSignal.h"
// -------------------------------------------------------------------------
class CheckedSignal;
class ConfirmSignal;
class Connector;
// -------------------------------------------------------------------------
/*!
 * \brief Менеджер объектов(SharedMemory).
 * \par
 * Менеджер объектов, который выполняет задачу посредника вместо них во всех внешних связях.
*/
class GuiPM : public ProxyManager
{
private:
	bool connected_;

public:
	GuiPM( Connector* connref, UniSetTypes::ObjectId id, UniSetTypes::ObjectId alive_sensor_id, UniSetTypes::ObjectId confirm_sensor_id, int auto_confirm_time);
	virtual ~GuiPM();

	/*! опрос SharedMemory, если пришло сообщение, то оно обрабатывается;
	    проверка запускается по таймеру через POLL_TIME(по-умолчанию 100 мсек)
	*/
	bool poll();
//	bool is_activated() {return activated;}
	bool connected();							/*!< получить состояние соединения с SharedMemory */

	/*! опросить датчик */
	void askSensor(const UniSetTypes::ObjectId id, const UniSetTypes::ObjectId node, const UniversalIO::UIOCommand cmd) throw();
	/*! заказать значение датчика */
	long getValue(const UniSetTypes::ObjectId id, const UniSetTypes::ObjectId node);
	/*! сохранить значение датчика */
	void saveValue(const long value, const UniSetTypes::ObjectId id, const UniSetTypes::ObjectId node);
	/*! послать сообщение объекту */
	void send(UniSetTypes::ObjectId name, UniSetTypes::TransportMessage& msg);
	/*! получить тип датчика */
	UniversalIO::IOType getIOType(UniSetTypes::ObjectId id, UniSetTypes::ObjectId node);

	sigc::signal<void, UniversalIO::UIOCommand> signal_ask_sensors;		/*!< сигнал опроса датчиков при старте */
	SmartSignal<void, const UniSetTypes::SensorMessage*> signal_sensor_info;	/*!< сигнал получения сообщений от датчиков */
	sigc::signal<void> signal_startup;					/*!< сигнал старта SharedMemory */
	sigc::signal<void> signal_connected;					/*!< сигнал о том что произошло соединение с SharedMemory */
	sigc::signal<void> signal_disconnected;					/*!< сигнал о том что произошло отсоединение от SharedMemory */

	CheckedSignal* signal_checked;
	ConfirmSignal* signal_confirm;						/*!< сигнал квитирования */

	USignals::SigController _signals;					/*!< сигналы для работы с сообщениями о датчиках */
	
	inline void set_msg_amount(int amount){ msg_amount = amount; };
	inline void set_log(bool l){ log = l; };
	inline void set_wait_ready_timeout(long t){ wait_ready_timeout_ = t; };
	inline void set_connection_timeout(long t){ if(t > 0) timeout_ = t; };
	virtual UniSetTypes::SimpleInfo* getInfo( ::CORBA::Long userparam = 0 ) override;

protected:
	void processingMessage( const UniSetTypes::VoidMessage* msg ) override;		/*!< обработка сообщений */
	void askSensors( UniversalIO::UIOCommand cmd );				/*!< опрос датчиков */
	void sysCommand( const UniSetTypes::SystemMessage* sm ) override;			/*!< обработка системных сообщений */
	void sensorInfo( const UniSetTypes::SensorMessage* sm ) override;			/*!< обработка сообщений от датчиков */
	void timerInfo( const UniSetTypes::TimerMessage* tm ) override;			/*!< обработка сообщений о срабатывании таймера */
	void startUp();								/*!< обработка системного сообщения о старте SharedMemory */
	void watchDog(); 							/*!< обработка системного сообщения об остановке SharedMemory */
	bool connectionTimeOut();						/*!< обработка ситуации, когда SharedMemory не отвечает(для виджетов посылается сигнал disconnected и они сереют) */

private:
	bool auto_confirm();
	UniSetTypes::ObjectId alive_sensor_id_;
	UniSetTypes::ObjectId confirm_sensor_id_;
	long alive_value_;

	long timeout_;
	long wait_ready_timeout_;
	sigc::connection to_connection_;
	int msg_amount;
	bool log;
	std::string get_info_buffer;
	friend void gtk_thread_get_info(GuiPM* gpm, UniSetTypes::SimpleInfo* si, ::CORBA::Long userparam);
};

inline bool
GuiPM::connected()
{
	return connected_;
}

namespace std
{
	template<>
	struct hash<const UniSetTypes::SensorMessage>
	{
		size_t operator()(const UniSetTypes::SensorMessage& s) const;
	};
}
// -------------------------------------------------------------------------
bool operator<(const UniSetTypes::SensorMessage& l, const UniSetTypes::SensorMessage& r);
bool operator==(const UniSetTypes::SensorMessage& l, const UniSetTypes::SensorMessage& r);
std::ostream& operator<<( std::ostream& os, const UniSetTypes::SensorMessage* t );
std::ostream& operator<<( std::ostream& os, const UniSetTypes::SensorMessage& t );
#endif
