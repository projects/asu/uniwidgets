#include "SigControllerUniSet.h"
#include <glibmm/ustring.h>
#include <glibmm/convert.h>
#include <map>
#include <ctime>
#include <sys/time.h>
#include <math.h>
#include <bitset>
#include <unordered_map>
#undef atoi
// -------------------------------------------------------------------------
using namespace UniSetTypes;
using namespace std;
using namespace UMessages;
using namespace USignals;
using Glib::ustring;
using UniversalIO::AI;
using UniversalIO::AO;
// -------------------------------------------------------------------------
namespace USignals
{

//#warning "Need comment for this realization and prepare to move in UniSet"
//static xmlNode* findNode(xmlNode* node, const string searchnode )
//{
//	assert( node != NULL );
//	if (!node->children)
//		return NULL;
//	node = node->children;
//	while (node != NULL) {
//		if (searchnode == UniXML::xml2local(node->name))
//			return node;
//		node = node->next;
//	}
//	return NULL;
//}
// -------------------------------------------------------------------------
SigControllerUniSet::SigControllerUniSet(GuiPM* gpm) : gpm_(gpm)
{
	assert( uniset_conf() != nullptr );
	UniXML_iterator it(uniset_conf()->getXMLSensorsSection());
	if ( !it.goChildren() )
	{
	}
	// Parsing configure for messages
	for ( ; it.getCurrent(); it.goNext() )
	{
		string tmp = it.getProp("mtype");
		// processing short entry
		if ( !tmp.empty() )
		{
			int mtype(uni_atoi(tmp));
			ustring text = it.getProp("textname");
			ObjectId id = it.getIntProp("id");
			tmp = it.getProp("node");
			ObjectId node;
			if ( tmp.empty() )
				node = uniset_conf()->getLocalNode();
			else
				node = uniset_conf()->getNodeID( tmp );

			if(!it.getProp("msg_for_bits").empty())
			{
				int mbit = it.getIntProp("mbit");
				if(mbit<0)
					mbit = 0;
				if(mbit>31)
					mbit = 31;
				long bit_mask = 1<<mbit;
				MessageId mid( id, node, bit_mask);
				mid._msg_groups = it.getProp("mgroups");
				mid._bits_msg = true;
				SensorMapItem* item = addMessage( mid, text, mtype );
				item->msg_for_bits = true;
			}
			else
			{
				long min = it.getProp("mvalue").empty() ? 1:it.getIntProp("mvalue");
				if(it.getProp("mvalue").empty())
					min = it.getProp("mvalue_min").empty() ? 1:it.getIntProp("mvalue_min");
				long max = it.getProp("mvalue_max").empty() ? min:it.getIntProp("mvalue_max");
				MessageId mid( id, node, min, max);
				mid._msg_groups = it.getProp("mgroups");
				addMessage( mid, text, mtype );
			}
			continue;
		}
		// processing MessageList section
		auto uxml = uniset_conf()->getConfXML();
		xmlNode* xmlnode = uxml->extFindNode(it, 1, 1, "MessagesList");
		if ( xmlnode == NULL )
			continue;
		UniXML_iterator m_it(xmlnode);
		if(!m_it.getProp("show_any").empty())
		{
			ObjectId id = it.getIntProp("id");
			tmp = it.getProp("node");
			ObjectId node;
			if ( tmp.empty() )
				node = uniset_conf()->getLocalNode();
			else
				node = uniset_conf()->getNodeID( tmp );

			const SensorKey sk(id, node);
			SensorMap::iterator s_it = _sensor_map.find(sk);
			SensorMapItem* item;
			if ( s_it != _sensor_map.end() )
				item = s_it->second;
			else
			{
				item = new SensorMapItem(id);
			}
			item->show_any_value = true;
			item->msg_groups = m_it.getProp("mgroups");
			item->default_msg_text = m_it.getProp("default_text");
			_sensor_map[sk] = item;
		}
		bool msg_for_bits = !m_it.getProp("msg_for_bits").empty();
		std::string msg_groups = m_it.getProp("mgroups");
		if ( !m_it.goChildren() )
			continue;
		if(msg_for_bits)
		{
			for (;m_it.getCurrent();m_it.goNext())
			{
				int mtype = m_it.getIntProp("mtype");
				int mbit = m_it.getIntProp("mbit");
				if(mbit<0)
					mbit = 0;
				if(mbit>31)
					mbit = 31;
				long bit_mask = 1<<mbit;
				ObjectId id = it.getIntProp("id");
				ustring text = m_it.getProp("text");
				if(m_it.getProp("text").empty())
				{
					stringstream msg_text;
					msg_text<<m_it.getProp("default_text")<<" id="<<id<<" bit="<<mbit;
					text = msg_text.str();
				}
				tmp = it.getProp("node");
				ObjectId node;
				if ( tmp.empty() )
					node = uniset_conf()->getLocalNode();
				else
					node = uniset_conf()->getNodeID( tmp );
				MessageId mid( id, node, bit_mask);
				mid._msg_groups = msg_groups;
				mid._bits_msg = true;
				SensorMapItem* item = addMessage( mid, text, mtype );
				item->msg_for_bits = true;
			}
		}
		else
		{
			for (;m_it.getCurrent();m_it.goNext())
			{
				int mtype = m_it.getIntProp("mtype");
				long min = m_it.getIntProp("value");
				if(m_it.getProp("value").empty())
					min = m_it.getIntProp("mvalue");
				if(m_it.getProp("mvalue").empty() && m_it.getProp("value").empty())
					min = m_it.getIntProp("mvalue_min");
				long max = m_it.getProp("mvalue_max").empty() ? min:m_it.getIntProp("mvalue_max");
				ObjectId id = it.getIntProp("id");
				ustring text = m_it.getProp("text");
				if(m_it.getProp("text").empty())
				{
					stringstream msg_text;
					msg_text<<m_it.getProp("default_text")<<" id="<<id<<" "<<min<<" <= value <= "<<max;
					text = msg_text.str();
				}
				tmp = it.getProp("node");
				ObjectId node;
				if ( tmp.empty() )
					node = uniset_conf()->getLocalNode();
				else
					node = uniset_conf()->getNodeID( tmp );
				MessageId mid( id, node, min, max);
				mid._msg_groups = msg_groups;
				addMessage( mid, text, mtype );
			}
		}
	}
	gpm_->signal_connected.connect(sigc::mem_fun(this, &SigControllerUniSet::on_connect));
}
// -------------------------------------------------------------------------
void SigControllerUniSet::on_connect()
{
	sensor_info_connection_.disconnect();
	sensor_info_connection_ = gpm_->signal_sensor_info.connect(
			sigc::mem_fun(this, &SigControllerUniSet::sensorInfo));
	for (SensorMap::iterator it = _sensor_map.begin();it != _sensor_map.end(); ++it)
	{
		const ObjectId& id = it->first._id;
		const ObjectId& node = it->first._node;
		SensorMapItem * const si = it->second;
		try {
			si->_last_value = gpm_->getValue(id, node);
			ask_sensor(id, node, UniversalIO::UIONotifyChange);
			UniversalIO::IOType iotype = gpm_->getIOType(id, node);

			if (iotype == AI || iotype == AO) {
				IOController_i::SensorInfo sinfo;
				sinfo.id = id;
				sinfo.node = node;
				IOController_i::CalibrateInfo ci =
					gpm_->uin->getCalibrateInfo(sinfo);
				si->_last_analog_value =
					float(si->_last_value)/pow(double(10), int(ci.precision));
			}
			else
				si->_last_analog_value = si->_last_value;
		}
		catch (IOBadParam& ex) {
			ucrit << "[" << gpm_->getName() << "]:(Warning)(SigControllerUniSet::on_connect):" << ex << endl;
		}
		catch (Exception& ex) {
			ucrit << "[" << gpm_->getName() << "]:(Warning)(SigControllerUniSet::on_connect):" << ex << endl;
		}
	}

}
// -------------------------------------------------------------------------
void SigControllerUniSet::on_disconnect() throw()
{
	for (SensorMap::iterator it = _sensor_map.begin();
			it != _sensor_map.end(); ++it)
	{
		it->second->_last_value = 0;
	}
}
// -------------------------------------------------------------------------
Connection
SigControllerUniSet::connect_value_changed( const ValueChangedSlot& slot,
		UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node )
{
	node = check_for_localnode(node);
	const SensorKey si(id,node);
	SensorMapItem* item;
	SensorMap::iterator it = _sensor_map.find( si );
	if ( it != _sensor_map.end() )
		item = it->second;
	else {
		item = new SensorMapItem(id);
		_sensor_map[si] = item;
		if (gpm_->connected()) {
			item->_last_value = gpm_->getValue( id, node );
			ask_sensor( id, node, UniversalIO::UIONotifyChange );
		}
	}
	sigc::connection conn = item->_value_changed.connect( slot );
	return Connection(conn);// this, si, conn);
}
// -------------------------------------------------------------------------
Connection
SigControllerUniSet::connect_analog_value_changed( const AnalogValueChangedSlot& slot,
		UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node )
{
	node = check_for_localnode(node);
	SensorKey si(id,node);
	SensorMapItem* item;
	SensorMap::iterator it = _sensor_map.find( si );
	if ( it != _sensor_map.end() )
		item = it->second;
	else {
		item = new SensorMapItem(id);
		_sensor_map[si] = item;
		if (gpm_->connected()) {
			item->_last_value = gpm_->getValue( id, node );
			ask_sensor( id, node, UniversalIO::UIONotifyChange );
			UniversalIO::IOType iotype = gpm_->getIOType(id, node);
			if (iotype == AI || iotype == AO) {
				IOController_i::SensorInfo sinfo;
				sinfo.id = id;
				sinfo.node = node;
				IOController_i::CalibrateInfo ci =
					gpm_->uin->getCalibrateInfo(sinfo);
				item->_last_analog_value =
					float(item->_last_value)/pow(double(10), int(ci.precision));
			}
			else
			{
				item->_last_analog_value = item->_last_value;
			}
		}
	}
	sigc::connection conn = item->_analog_value_changed.connect( slot );
	return Connection(conn);// this, si, conn);
}
// -------------------------------------------------------------------------
void
SigControllerUniSet::ask_sensor( ObjectId id, ObjectId node, UniversalIO::UIOCommand cmd) throw()
{
	gpm_->askSensor(id, node, cmd);
}
// -------------------------------------------------------------------------
VConn
SigControllerUniSet::connect_value_in_out( const ValueInOutSlot& slot,
		UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node,
		long target_value, bool on)
{
	node = check_for_localnode(node);
	SensorKey si ( id, node );
	SensorMapItem* item;
		SensorMap::iterator s_it = _sensor_map.find( si );
	if ( s_it != _sensor_map.end() )
		item= s_it->second;
	else
	{
		item = new SensorMapItem(id);
		_sensor_map[si] = item;
		if (gpm_->connected()) {
			item->_last_value = gpm_->getValue( id, node );
			ask_sensor( id, node, UniversalIO::UIONotifyChange );
		}
	}
	ValueMap::iterator v_it= item->find_in_value_map(target_value);
	ValueMapItem* vitem;
	if ( v_it != item->get_value_map_end() )
	{
		vitem= v_it->second;
	}
	else
	{
		vitem = new ValueMapItem(id, target_value);
		item->value_map_add_item(target_value,vitem);
	}
	sigc::connection conn;
	if (on)
		conn = vitem->_value_in.connect(slot);
	else
		conn = vitem->_value_out.connect(slot);
	VConn vconn;
	vconn.conn = conn;
	vconn.id = id;
	vconn.node = node;
	vconn.value = target_value;
	return vconn;
}
// -------------------------------------------------------------------------
inline VConn
SigControllerUniSet::connect_value_in( const ValueInOutSlot& slot,
		UniWidgetsTypes::ObjectId id,
		UniWidgetsTypes::ObjectId node,
		long target_value)
{
	return connect_value_in_out( slot, id, node, target_value, true);
}
// -------------------------------------------------------------------------
inline VConn
SigControllerUniSet::connect_value_out( const ValueInOutSlot& slot,
		UniWidgetsTypes::ObjectId id,
		UniWidgetsTypes::ObjectId node,
		long target_value)
{
	return connect_value_in_out( slot, id, node, target_value, false);
}
// -------------------------------------------------------------------------
Connection
SigControllerUniSet::connect_on_any_message( const MessageSlot& slot )
{
	sigc::connection conn = _signal_any_message.connect(slot);
	return Connection(conn);
}
// -------------------------------------------------------------------------
Connection
SigControllerUniSet::connect_on_any_message_full ( const FullMessageSlot& slot )
{
	sigc::connection conn = _signal_any_message_full.connect(slot);
	return Connection(conn);
}
// -------------------------------------------------------------------------
Connection
SigControllerUniSet::connect_on_message( const MessageSlot& slot,
		MessageId id)
{
	SensorKey si ( id._id, check_for_localnode(id._node) );
	SensorMap::iterator s_it = _sensor_map.find( si );
	if ( s_it == _sensor_map.end() )
		return Connection(sigc::connection());

	SensorMapItem* item = s_it->second;
	ValueMap::iterator v_it= item->find_in_value_map(id._value);
	if ( v_it == item->get_value_map_end() )
		return Connection(sigc::connection());

	ValueMapItem* vitem= v_it->second;
	assert(vitem != NULL);
	if ( !vitem->_is_message )
		return Connection(sigc::connection());

	sigc::connection conn = vitem->_signal_message.connect( slot );
	return Connection(conn);
}
// -------------------------------------------------------------------------
void
SigControllerUniSet::sensorInfo(const SensorMessage* sm) throw()
{
	SensorKey si( sm->id, sm->node );
	SensorMap::iterator it = _sensor_map.find(si);
	if ( it == _sensor_map.end() )
		return;
	SensorMapItem* item = it->second;
//	uinfo<<"sensorInfo() sm->value="<<sm->value<<" last_value="<<(item->_last_value)<<endl;
	if ( item->_last_value == sm->value ) //nothing changed
		return;

	item->_value_changed.emit(sm->id, sm->node, sm->value);

	item->_last_analog_value = float(sm->value) / pow(double(10),int(sm->ci.precision));
	item->_analog_value_changed.emit(sm->id, sm->node, item->_last_analog_value);

//	if ( item->_value_map.empty() ) //no VALUE IN or VALUE OUT
//		return;

	if(item->msg_for_bits)
	{
		for (int nbit=0;nbit<32;nbit++)
		{
			long bit_mask = 1<<nbit;
			//search and emit VALUE OUT signal
			if( ((item->_last_value)&bit_mask) && ((sm->value)&bit_mask) == 0 )
			{
				ValueMap::iterator v_it = item->find_in_value_map(bit_mask);
				if (v_it != item->get_value_map_end() )
				{
					v_it->second->_value_out.emit(sm->id,sm->node,bit_mask);
// 					ucrit << "VALUE OUT" << sm->id << ":" << bit_mask << endl;
				}
			}

			//search and emit VALUE IN signal
//			bitset<32> bitset32{bit_mask};
//			uinfo<<"sensorInfo() bit_mask="<<bitset32<<" value&bit_mask="<<((sm->value)&bit_mask)<<" _last_value="<<(item->_last_value)<<" last_value&bit_mask="<<((item->_last_value)&bit_mask)<<endl;
			if( (((sm->value)&bit_mask) == ((item->_last_value)&bit_mask)) || ((sm->value)&bit_mask) == 0 )
				continue;
//			uinfo<<"sensorInfo() bit_mask="<<bitset32<<" value&bit_mask="<<((sm->value)&bit_mask)<<endl;

			ValueMap::iterator v_it = item->find_in_value_map(bit_mask);
			if (v_it != item->get_value_map_end() )
			{
				ValueMapItem* vitem = v_it->second;
				if ( vitem->_is_message)
				{
					MessageId msg_id(sm->id,sm->node,bit_mask);
					msg_id._bits_msg = true;
					msg_id._msg_groups = item->msg_groups;
					_signal_any_message.emit(msg_id, vitem->get_first_text());
					vitem->_last_time = sm->sm_tv.tv_sec;
					vitem->_time_nsec = sm->sm_tv.tv_nsec;
					_signal_any_message_full.emit(msg_id, vitem->_mtype, vitem->_last_time, vitem->get_first_text());
					vitem->_signal_message.emit(msg_id, vitem->get_first_text());
				}
			}
			else if(item->show_any_value)
			{
				MessageId msg_id(sm->id,sm->node,bit_mask);
				msg_id._msg_groups = item->msg_groups;
				stringstream msg_text;
				msg_text<<item->default_msg_text<<" id="<<sm->id<<" bit="<<nbit;
				_signal_any_message.emit(msg_id, msg_text.str());
				_signal_any_message_full.emit(msg_id, 0, sm->sm_tv.tv_sec, msg_text.str());
			}
		}
	}
	else
	{
		//search and emit VALUE OUT signal
		ValueMap::iterator v_it = item->find_in_value_map(item->_last_value);
		if (v_it != item->get_value_map_end() )
		{
			v_it->second->_value_out.emit(sm->id,sm->node,item->_last_value);
// 			ucrit << "VALUE OUT" << sm->id << ":" << item->_last_value << endl;
		}

		//search and emit VALUE IN signal
		v_it = item->find_in_value_map(sm->value);
		if (v_it != item->get_value_map_end() )
		{
			ValueMapItem* vitem = v_it->second;
			vitem->_value_in.emit(sm->id,sm->node,sm->value);
			if ( vitem->_is_message)
			{
				MessageId msg_id(sm->id,sm->node,sm->value);
				msg_id._msg_groups = item->msg_groups;
				_signal_any_message.emit(msg_id, vitem->get_first_text());
				vitem->_last_time = sm->sm_tv.tv_sec;
				vitem->_time_nsec = sm->sm_tv.tv_nsec;
				_signal_any_message_full.emit(msg_id, vitem->_mtype, vitem->_last_time, vitem->get_first_text());
				vitem->_signal_message.emit(msg_id, vitem->get_first_text());
			}
		}
		else if(item->show_any_value && !item->msg_for_bits)
		{
			MessageId msg_id(sm->id,sm->node,sm->value);
			msg_id._msg_groups = item->msg_groups;
			stringstream msg_text;
			msg_text<<item->default_msg_text<<" id="<<sm->id<<" val="<<sm->value;
			_signal_any_message.emit(msg_id, msg_text.str());
			_signal_any_message_full.emit(msg_id, 0, sm->sm_tv.tv_sec, msg_text.str());
		}
	}

	item->_last_value = sm->value;
	return;
}
// -------------------------------------------------------------------------
inline ObjectId
SigControllerUniSet::check_for_localnode( ObjectId nodeid )
{
	if ( nodeid == -1 )
		return uniset_conf()->getLocalNode();
	else
		return nodeid;
}
// -------------------------------------------------------------------------
UMessages::Message
SigControllerUniSet::get_message( const UMessages::MessageId& id )
{
	SensorKey sk( id._id, check_for_localnode( id._node) );
	SensorMap::iterator it = _sensor_map.find(sk);
	if ( it == _sensor_map.end() )
		return UMessages::Message();
	ValueMap::iterator v_it = it->second->find_in_value_map(id._value);
	if (v_it == it->second->get_value_map_end() )
		return UMessages::Message();
	return UMessages::Message(*(v_it->second), id);

}
// -------------------------------------------------------------------------
std::list<UMessages::Message >
SigControllerUniSet::get_message_list( const UMessages::MessageId& id )
{
	std::list<UMessages::Message > msg_list;

	SensorKey sk( id._id, check_for_localnode( id._node) );
	SensorMap::iterator it = _sensor_map.find(sk);
	if ( it == _sensor_map.end() )
		return msg_list;

	ValueMap::iterator v_it = it->second->get_value_map_begin();
	for( ; v_it != it->second->get_value_map_end(); v_it++ )
	{
		UMessages::MessageId m_id(id._id, id._node , v_it->first);
		m_id._msg_groups = it->second->msg_groups;
		msg_list.push_back(UMessages::Message(*(v_it->second), m_id));
	}
	return msg_list;
}
// -------------------------------------------------------------------------
std::list<UMessages::Message >
SigControllerUniSet::get_all_messages_list()
{
	uinfo<< "SigControllerUniSet::get_all_messages_list()"<< endl;

	std::list<UMessages::Message > msg_list;
	for( SensorMap::iterator s_it = _sensor_map.begin(); s_it != _sensor_map.end(); s_it++ )
	{
		if(s_it->second->msg_for_bits)
		{
			for (int nbit=0;nbit<32;nbit++)
			{
				long bit_mask = 1<<nbit;
				if( ((s_it->second->_last_value)&bit_mask) == 0 )
					continue;

				ValueMap::iterator v_it = s_it->second->find_in_value_map(bit_mask);
				if (v_it != s_it->second->get_value_map_end())
				{
					if (v_it->second->_is_message)
					{
						for(std::list<Glib::ustring>::iterator text_it = v_it->second->_text.begin();text_it != v_it->second->_text.end();++text_it)
							uinfo<<"SigControllerUniSet id="<<s_it->first._id<<" nbit="<<nbit<<" is_message="<<v_it->second->_is_message<<" _text='"<<*text_it<<"'"<< endl;

						v_it->second->_last_time  = time(0);
						UMessages::MessageId m_id(s_it->first._id, s_it->first._node, v_it->first);
						m_id._msg_groups = s_it->second->msg_groups;
						msg_list.push_back(UMessages::Message(*(v_it->second), m_id));
					}
				}
			}
		}
		else
		{
			ValueMap::iterator v_it = s_it->second->find_in_value_map(s_it->second->_last_value);
			if (v_it != s_it->second->get_value_map_end())
			{
				if (v_it->second->_is_message)
				{
					for(std::list<Glib::ustring>::iterator text_it = v_it->second->_text.begin();text_it != v_it->second->_text.end();++text_it)
						uinfo<<"SigControllerUniSet id="<<s_it->first._id<<" value="<<v_it->first<<" is_message="<<v_it->second->_is_message<<" _text='"<<*text_it<<"'"<< endl;

					v_it->second->_last_time  = time(0);
					UMessages::MessageId m_id(s_it->first._id, s_it->first._node, v_it->first);
					m_id._msg_groups = s_it->second->msg_groups;
					msg_list.push_back(UMessages::Message(*(v_it->second), m_id));
				}
			}
		}
	}
	return msg_list;
}
// -------------------------------------------------------------------------
long
SigControllerUniSet::get_value(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node) throw()
{
	SensorKey sk(id, check_for_localnode(node));
	SensorMap::iterator it = _sensor_map.find(sk);
	if (it == _sensor_map.end())
		return 0;

	return it->second->_last_value;

}
// -------------------------------------------------------------------------
float
SigControllerUniSet::get_analog_value(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node) throw()
{
	SensorKey sk(id, check_for_localnode(node));
	SensorMap::iterator it = _sensor_map.find(sk);
	if (it == _sensor_map.end())
		return 0;

	return it->second->_last_analog_value;

}
// -------------------------------------------------------------------------
std::string
SigControllerUniSet::get_info(int signal_num)
{
	ostringstream os, v_ch_os, a_v_ch_os, v_in_os, v_out_os, v_m_os;
	bool first_loop = signal_num > 2 ;
	for( auto&& s_it : _sensor_map )
	{
		if( first_loop )
		{
			v_ch_os << s_it.second->_value_changed.get_info();
			a_v_ch_os << s_it.second->_analog_value_changed.get_info();
		}
		else
		{
			v_ch_os << s_it.second->_value_changed.get_short_info();
			a_v_ch_os << s_it.second->_analog_value_changed.get_short_info();
		}
		auto v_it = s_it.second->get_value_map_begin();
		for( ; v_it != s_it.second->get_value_map_end(); v_it++ )
		{
			if( first_loop )
			{
				v_in_os << v_it->second->_value_in.get_info();
				v_out_os << v_it->second->_value_out.get_info();
				v_m_os << v_it->second->_signal_message.get_info();
			}
			else
			{
				v_in_os << v_it->second->_value_in.get_short_info();
				v_out_os << v_it->second->_value_out.get_short_info();
				v_m_os << v_it->second->_signal_message.get_short_info();
			}
		}
		first_loop = false;
	}
	int size;
	switch(signal_num)
	{
		case 0:
			os << "	3 - [UniWidgets] USignals::_value_changed" << endl;
			os << "	4 - [UniWidgets] USignals::_analog_value_changed" << endl;
			os << "	5 - [UniWidgets] USignals::_value_in" << endl;
			os << "	6 - [UniWidgets] USignals::_value_out" << endl;
			os << "	7 - [UniWidgets] USignals::_signal_message" << endl;
			size = _info_slots.size();
			for( int i = 0; i < size; ++i )
			{
				GetInfoSlotItem& _info_slot = _info_slots[i];
				os << "	" << (i + 8) << " - [" << (!_info_slot.component.empty() ? _info_slot.component : "Unknown") << "] " << _info_slot.info << endl;
			}
			break;
		case 1:
			os << v_ch_os.str();
			os << a_v_ch_os.str();
			os << v_in_os.str();
			os << v_out_os.str();
			os << v_m_os.str();
			os << _signal_any_message.get_info();
			os << _signal_any_message_full.get_info();
			break;
		case 2:
			break;
		case 3:
			os << v_ch_os.str();
			break;
		case 4:
			os << a_v_ch_os.str();
			break;
		case 5:
			os << v_in_os.str();
			break;
		case 6:
			os << v_out_os.str();
			break;
		case 7:
			os << v_m_os.str();
			break;
		default:
			int slot_num = signal_num - 8;
			if( _info_slots.find(slot_num) != _info_slots.end() )
				os << _info_slots[slot_num].callback();
			break;
	}
	return os.str();
}

} //namespace USignals
