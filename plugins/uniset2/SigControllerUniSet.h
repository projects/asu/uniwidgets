#ifndef _SIGCONTROLLERUNISET_H
#define _SIGCONTROLLERUNISET_H
// -------------------------------------------------------------------------
#include <sigc++/sigc++.h>
#include <UniSetTypes.h>
#include <MessageType.h>
#include <glibmm/ustring.h>
#include <glibmm/convert.h>
#include <ctime>
#include <uniwidgets/SigControllerImpl.h>
#include <GuiPM.h>
// -------------------------------------------------------------------------
namespace USignals
{
/*!
 * \brief Класс реализующий управление сигналами от датчиков.
 * \par
 * Данный класс реализует методы управления сигналами изменения значения, состояния датчиков
 * ,а также заказа датчиков из Sharedmemory.
*/
class SigControllerUniSet : public SigControllerImpl
{
public:
	SigControllerUniSet(GuiPM* gpm);
	/*! назначить обработчик сигнала об изменении значения датчика */
	virtual Connection connect_value_changed( const ValueChangedSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node ) override;
	/*! назначить обработчик сигнала об изменении аналогового значения датчика */
	virtual Connection connect_analog_value_changed(const AnalogValueChangedSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node) override;
	/*! назначить обработчик сигнала об изменении датчика в заданное значение */
	virtual VConn connect_value_in( const ValueInOutSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value) override;
	/*! назначить обработчик сигнала об изменении заданного значения датчика */
	virtual VConn connect_value_out( const ValueInOutSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value ) override;
	/*! назначить обработчик сигнала об изменении заданного значения датчика */
	virtual VConn connect_value_in_out( const ValueInOutSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value, bool on) override;
	/*! назначить обработчик на приходящее сообщение от любого датчика */
	virtual Connection connect_on_any_message( const MessageSlot& slot) override;
	/*! назначить обработчик на приходящее сообщение от заданного датчика */
	virtual Connection connect_on_message( const MessageSlot& slot, UMessages::MessageId id) override;
	/*! назначить обработчик на приходящее сообщение от любого датчика(расширенное количество параметров) */
	virtual Connection connect_on_any_message_full ( const FullMessageSlot& slot ) override;
	/*! получить объект класса Message, в котором описан сигнал для id.
	    Если для сигнала задано поле mtype, то для этого сигнала хранится
	    описание с параметрами сигнала, временем срабатывания и различными
	    сигналами(см. USignals::ValueMapItem) т.е. это сигнал АПС и он должен
	    отображаться в журнале и квитироваться, если это предусмотрено. Этот
	    метод применяется, например, когда нужно повесить обработчик сообщения на
	    конкретный датчик и конкретное значение */
	virtual UMessages::Message get_message( const UMessages::MessageId& id) override;
	/*! получить объекты класса Message, в котором описаны сигнал для id.
	    Этот метод выполняет такую же задачу как и UMessages::Message get_message,
	    только в отличие от него работает с аналоговыми датчиками, у которых есть
	    "MessageList" в описании(configure.xml). Данная функция возвращает описание
	    для каждого пункта MessageList. Применяется метод, когда нужно повесить
	    обработчик сообщений для нескольких значений одного и того же датчика. */
	virtual std::list<UMessages::Message > get_message_list( const UMessages::MessageId& id) override;
	/*! запросить все датчики */
	virtual std::list<UMessages::Message > get_all_messages_list() override;
	virtual void on_connect() override;
	virtual void on_disconnect() throw() override;
	/*! получить значение для датчика */
	virtual long get_value(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node) throw() override;
	/*! получить значение для аналогового датчика */
	virtual float get_analog_value(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node) throw() override;
	/*! запросить статистику о сигналах*/
	virtual std::string get_info(int signal_num) override;
	/*! обработчик сигнала получения сообщений от датчиков */
	void sensorInfo( const UniSetTypes::SensorMessage* sm ) throw();

private:
	void ask_sensor( UniSetTypes::ObjectId id, UniSetTypes::ObjectId node, UniversalIO::UIOCommand ) throw();
	UniSetTypes::ObjectId check_for_localnode( UniSetTypes::ObjectId nodeid );
	sigc::connection sensor_info_connection_;
	GuiPM* gpm_;
};
} //namespace USignals
#endif
