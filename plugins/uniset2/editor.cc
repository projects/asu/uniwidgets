//#include <stdio.h>
//#include <stdlib.h>
//#include <cstring>
//#include <alloca.h>

//#include <gladeui/glade.h>
//#include <glib.h>
//#include <glib/gprintf.h>

#include <UniXML.h>
#include <UniSetTypes.h>
#include <Configuration.h>

//#include <uwidgets/UWidgets.h>
//#include <uniwidgets/ThemeLoader-wrapper.h>
#include <dynamic_libs.h>

#undef atoi

using namespace UniSetTypes;
using namespace std;

typedef struct _sensdata sensdata;

struct _sensdata
{
	long id;
	long node_id;
	gchar *name, *nodename, *stype, *textname;
	sensdata *next;
};


//-------------------------------------------------
static UniXML_iterator g_it;
static UniSetTypes::Configuration* glade_conf;

//����� ������ load_conf g_it ��������� �� ������ ������� ������ sensors
extern "C" int load_conf(int argc, const char** argv,  char* confile)
{
  if (glade_conf != NULL)
    delete glade_conf;

  glade_conf = new Configuration(argc, argv, confile);

  g_it = UniXML_iterator( glade_conf->getXMLSensorsSection() );
  if( !g_it.goChildren() )
  {
    cerr<<"<sensors> - ������"<<endl;
    return 1;
  }
  return 0;
}
//-------------------------------------------------

//-------------------------------------------------
extern "C" int g_it_next()
{
  return g_it.goNext();
}
//-------------------------------------------------

//-------------------------------------------------
extern "C" int g_it_prev()
{
	return g_it.goPrev();
}
//-------------------------------------------------

//-------------------------------------------------
extern "C" int g_it_getdata(sensdata* sd)
{
  sd->id = g_it.getIntProp("id");
  sd->name = strdup(g_it.getProp("name").c_str());
  sd->nodename = strdup(g_it.getProp("node").c_str());
}
//-------------------------------------------------


//sensors - ��������� �� ������ ������� �������� ������
//count - ����� ��������� � ������
//-------------------------------------------------
extern "C" int get_sensors(sensdata** sensors, long* count)
{
  assert(glade_conf != NULL);
  sensdata* SD = new sensdata;
  sensdata* first = SD;
  *count = 0;
  xmlNode* sensSection = glade_conf->getXMLSensorsSection();
  UniXML_iterator it(sensSection);
  if( !it.goChildren() )
  {
    cerr<<"<sensors> - ������"<<endl;
    return 1;
  }
  for(; it.getCurrent(); it.goNext() )
  {
    SD->id = it.getIntProp("id");

    SD->name = strdup(it.getProp("name").c_str());
    SD->nodename = strdup(it.getProp("node").c_str());
    SD->node_id = glade_conf->getNodeID(SD->nodename);
    SD->stype = strdup(it.getProp("iotype").c_str());
    SD->textname = strdup (it.getProp("textname").c_str());

    SD->next = new sensdata;
    SD = SD->next;
    (*count)++;
  }
  *sensors = first;
  return 0;
}
//-------------------------------------------------

//-------------------------------------------------
extern "C" int free_sensor_data(sensdata** sd, long* count)
{
  UniXML_iterator it (glade_conf->getXMLSensorsSection());

  if( !it.goChildren() )
  {
    cerr<<"<sensors> - ������"<<endl;
    return 1;
  }

  for(int i = 1; i < *count; i++ )
  {
    sensdata* tmp;
    free ((*sd)->name);
    free ((*sd)->nodename);
    tmp = (*sd)->next;
    delete *sd;
    *sd = tmp;
  }
  return 0;
}
//-------------------------------------------------
