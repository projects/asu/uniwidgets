#! /bin/sh

scp lib/.libs/libUniWidgets.so.0.0.0 testing:/usr/lib/
scp plugins/glade/.libs/libglade-uniwidgets.so testing:/usr/lib/glade3/modules/
scp plugins/libglade/.libs/libuniwidgets.so testing:/usr/lib/libglade/2.0/

USER=`whoami`

cp plugins/glade/uniwidgets.xml /tmp/uniwidgets.xml.tmp

subst "s/<glade-widget-group name=\"UniSet\" title=\"\(.*\)\">/<glade-widget-group name=\"UniSet\" title=\"$USER\'s uniwidgets\">/" /tmp/uniwidgets.xml.tmp

scp /tmp/uniwidgets.xml.tmp testing:/usr/share/glade3/catalogs/uniwidgets.xml

rm -f /tmp/uniwidgets.xml.tmp

scp svg/*.svg gentro@testing:/usr/share/uniwidgets/svg/
