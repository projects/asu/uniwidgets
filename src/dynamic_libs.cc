#include <dynamic_libs.h>
#include <types.h>
#include <cstdio>
// -------------------------------------------------------------------------
std::unordered_map<std::string, std::weak_ptr<DynamicLib> > DynamicLib::libs;
// -------------------------------------------------------------------------
std::shared_ptr<DynamicLib> DynamicLib::open(const std::string& path)
{
	std::shared_ptr<DynamicLib> temp_lib = std::shared_ptr<DynamicLib>();
	if( libs.find(path) == libs.end() || !libs[path].lock() )
	{
		char* lib_env = getenv("UNIWIDGETS_MODULE_PATH");
		std::string lib_path( lib_env ? lib_env : "" );
		if( path.substr(0,1) == "/" )
			lib_path = path;
		else if( !lib_path.empty() )
			lib_path = lib_path + "/" + path;
		else
			lib_path = "/usr/lib/" + path;
		LibPtr lib = dlopen(lib_path.c_str(), RTLD_LAZY);
		if( !lib )
		{
			//cerr << "Cannot load library: " << dlerror() << endl;
			throw UniWidgetsTypes::Exception(std::string("Cannot load library: ") + dlerror());
		}
		temp_lib = std::shared_ptr<DynamicLib>(new DynamicLib(lib), DynamicLibDeleter());
		libs[path] = temp_lib;
	}
	else
	{
		temp_lib = libs[path].lock();
	}
	return temp_lib;
}
// -------------------------------------------------------------------------
void* DynamicLib::load(const std::string& func)
{
	void* symbol = dlsym(lib, func.c_str());
	if( !symbol )
	{
		//cerr << "Cannot load symbols: " << dlerror() << endl;
		throw UniWidgetsTypes::Exception(std::string("Cannot load symbol: ") + dlerror());
	}
	return symbol;
}
