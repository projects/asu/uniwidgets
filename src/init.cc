#include "init.h"
#include "typical/wrap_init.h"
#include "objects/wrap_init.h"
#include "components/wrap_init.h"
#include "uwidgets/wrap_init.h"
#include "usvgwidgets/wrap_init.h"

namespace UniWidgets
{

void init(int argc, char** argv)
{
	wrap_init_typical();
	wrap_init_objects();
	wrap_init_components();
	wrap_init_uwidgets();
	wrap_init_usvgwidgets();
}

}
