#include <cstdio>
#include <cstring>

#include <stdarg.h>

#include "ports.h"

/* sprintf for Glib::ustring. Use Glib::ustring::compose if possible. */
size_t usprintf(Glib::ustring &ustr, const Glib::ustring fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    gchar *buf = g_strdup_vprintf(fmt.c_str(), args);
    va_end(args);
    if (buf) {
        ustr = Glib::ustring(buf, std::strlen(buf));
        g_free(buf);
        return 1;
    }
    return 0;
}

void get_time_str(Glib::ustring& dtimeStr, Glib::ustring& ttimeStr, time_t sec)
{
	struct tm *tms = localtime(&sec);
	usprintf(dtimeStr, "%02d/%02d/%04d ", tms->tm_mday, tms->tm_mon+1, tms->tm_year+1900);
	usprintf(ttimeStr, "%02d:%02d:%02d ", tms->tm_hour, tms->tm_min, tms->tm_sec);
}

void get_gmtime_str(Glib::ustring& dtimeStr, Glib::ustring& ttimeStr, time_t sec)
{
	struct tm *tms = gmtime(&sec);
	usprintf(dtimeStr, "%02d/%02d/%04d ", tms->tm_mday, tms->tm_mon+1, tms->tm_year+1900);
	usprintf(ttimeStr, "%02d:%02d:%02d ", tms->tm_hour, tms->tm_min, tms->tm_sec);
}
