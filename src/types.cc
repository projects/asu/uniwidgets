#include "types.h"
#include <fstream>

namespace UniWidgetsTypes
{
  /*
int _argc = { 0 };
const char* const* _argv = nullptr;*/
	
void uniwidgets_init(int argc, const char* const* argv)
{
	_argc = argc;
	_argv = argv;
}
// -------------------------------------------------------------------------
bool file_exist( const std::string& filename )
{
	std::ifstream file;
#ifdef HAVE_IOS_NOCREATE
	file.open( filename.c_str(), std::ios::in | std::ios::nocreate );
#else
	file.open( filename.c_str(), std::ios::in );
#endif
	bool result = false;

	if( file )
		result = true;

	file.close();
	return result;
}
// -------------------------------------------------------------------------

xmlNode* xmlFindNode( xmlNode* node, const std::string& searchnode, const std::string& name )
{
	xmlNode* fnode = node;

	while (fnode != nullptr)
	{
		if (searchnode == (const char*)fnode->name)
		{
			/* Если name не задано, не сверяем. Иначе ищем, пока не найдём с таким именем */
			if( name.empty() )
				return fnode;

			if( name == xmlGetProp(fnode, "name") )
				return fnode;

		}

		xmlNode* nodeFound = xmlFindNode(fnode->children, searchnode, name);

		if ( nodeFound != nullptr )
			return nodeFound;

		fnode = fnode->next;
	}

	return nullptr;
}

xmlNode* xmlExtFindNode( xmlNode* node, int depth, int width, const std::string& searchnode, const std::string& name, bool top )
{
	auto i = 0;
	xmlNode* fnode = node;

	while( fnode != NULL )
	{
		if(top && (i >= width)) return NULL;

		if (searchnode == (const char*)fnode->name)
		{
			if( name == xmlGetProp(fnode, "name") )
				return fnode;

			if( name.empty() )
				return node;
		}

		if(depth > 0)
		{
			xmlNode* nodeFound = xmlExtFindNode(fnode->children, depth - 1, width, searchnode, name, false);

			if ( nodeFound != NULL )
				return nodeFound;
		}

		i++;
		fnode = fnode->next;
	}

	return NULL;
}

std::string xmlGetProp(const xmlNode* node, const std::string& name)
{
	xmlChar* text = ::xmlGetProp((xmlNode*)node, (const xmlChar*)name.c_str());

	if( text == NULL )
	{
		xmlFree(text);
		return "";
	}

	const std::string t( (const char*)text );
	xmlFree( (xmlChar*) text );
	return std::move(t);
}

std::string getArgParam( const std::string& name, const std::string& defval )
{
	for( int i = 1; i < (_argc - 1) ; i++ )
	{
		if( name == _argv[i] )
			return _argv[i + 1];
	}
	return defval;
}

}
