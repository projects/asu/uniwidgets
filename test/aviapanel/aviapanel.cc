#include <string>
#include <gtkmm.h>
#include <libglademm.h>
#include "URadialValueIndicator.h"
#include "URadialBar.h"
#include "UScrollValue.h"
#include "UScrollScale.h"
#include "UValueIndicator.h"
#include "UIndicatorContainer.h"

using namespace std;
//using namespace UniSetTypes;

static const string GLADE_DIR = "./";
static const string GLADE_FILE = "main.glade";
static gboolean alpha_channel_support = false;

URadialValueIndicator* uradialvalueindicator_aviahorisont;
UScrollValue* uscrollvalue1;
UScrollScale* uscrollscale_H;
double uscrollscale_val_h=0;
//UValueIndicator* uvalueindicator_H;
double kompas_val=0;
UScrollScale* kompas;
//UValueIndicator* uvalueindicator_kompas;

UScrollScale* uscrollscale_Mph;
double uscrollscale_val_mph=100;
//UValueIndicator* uvalueindicator_Mph;

bool on_timer_tick()
{
	double val = -20 + (random() % 40);
	uradialvalueindicator_aviahorisont->set_value(val);
// 	uvalueindicator1->set_property_value_(val);
	uscrollscale_val_h += -20 + (random() % 40);
	uscrollscale_H->set_value(uscrollscale_val_h);
//	uvalueindicator_H->set_property_value_(uscrollscale_val_h);

	uscrollscale_val_mph += -25 + (random() % 50);
	uscrollscale_Mph->set_value(uscrollscale_val_mph);
//	uvalueindicator_Mph->set_property_value_(uscrollscale_val_mph);

	kompas_val += -30 + (random() % 90);
	kompas_val = kompas_val>360? kompas_val-360:kompas_val;
	kompas_val = kompas_val<0? 360+kompas_val:kompas_val;
	kompas->set_value(kompas_val);
//	uvalueindicator_kompas->set_property_value_(kompas_val);
	
	return true;
}
bool on_plane_tick()
{
// 	double val = -(random() % 2) + (random() % 2);
// 	uscrollvalue_val += val;
// 	if(uscrollvalue_val>20)
// 		uscrollvalue_val=20;
// 	else if(uscrollvalue_val<-20)
// 		uscrollvalue_val = -20;
//	uscrollvalue1->set_value(uscrollvalue_val);
	double val = -10 + (random() % 20);
	uscrollvalue1->set_value(val);
	// 	uvalueindicator1->set_property_value_(val);
	
	return true;
}
int main (int argc, char **argv)
{
	Gtk::Window *w;
//	Gtk::Button *btn;

	Gtk::Main Kit(argc, argv);

	if (!g_thread_supported())
		g_thread_init(NULL);

	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(GLADE_DIR + GLADE_FILE);

	gxml->get_widget("window3", w);
	gxml->get_widget("uradialvalueindicator_aviahorisont", uradialvalueindicator_aviahorisont);
	gxml->get_widget("uscrollvalue1", uscrollvalue1);
	gxml->get_widget("uscrollscale_H", uscrollscale_H);
//	gxml->get_widget("uvalueindicator_H", uvalueindicator_H);
	gxml->get_widget("uscrollscale_Mph", uscrollscale_Mph);
//	gxml->get_widget("uvalueindicator_Mph", uvalueindicator_Mph);
	gxml->get_widget("kompas", kompas);
//	gxml->get_widget("uvalueindicator_kompas", uvalueindicator_kompas);
	
    sigc::connection inert_tmr= Glib::signal_timeout().connect(&on_timer_tick,2000);
	sigc::connection plane_tmr= Glib::signal_timeout().connect(&on_plane_tick,800);
	
#if 0
	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );
#endif  
  	Kit.run(*w);

	return 0;
}
