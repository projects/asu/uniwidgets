#include <string>
#include <gtkmm.h>
#include <libglademm.h>
#include "UniOscilChannel.h"
#include "UniOscillograph.h"
#include "UValueIndicatormb745.h"

using namespace std;
//using namespace UniSetTypes;

static const string GLADE_DIR = "./";
static const string GLADE_FILE = "main.glade";
static gboolean alpha_channel_support = false;

UniOscillograph* unioscillograph1;
UniOscillograph* unioscillograph2;
UniOscillograph* unioscillograph3;

UValueIndicatormb745* uvalueindicator1;
UValueIndicatormb745* uvalueindicator2;
UValueIndicatormb745* uvalueindicator3;
UValueIndicatormb745* uvalueindicator4;
UValueIndicatormb745* uvalueindicator5;
UValueIndicatormb745* uvalueindicator6;

UniOscilChannel* add_unioscilchannel1;
UniOscilChannel* add_unioscilchannel2;
UniOscilChannel* add_unioscilchannel3;
UniOscilChannel* add_unioscilchannel4;
UniOscilChannel* add_unioscilchannel5;
UniOscilChannel* add_unioscilchannel6;

bool on_timer_tick()
{
    double val = random() % 100;

	for(UniOscillograph::ChannelsList::iterator it = unioscillograph1->get_channels()->begin();it != unioscillograph1->get_channels()->end();++it)
	{
		if(it==unioscillograph1->get_channels()->begin())
		{
			it->second->set_value(val);
//			cout<<"Test::on_timer_tick() for "<<it->second->get_channelname()<<" val="<<val<<endl;
			continue;
		}
		it->second->set_value(random() % 100);
	}
	for(UniOscillograph::ChannelsList::iterator it = unioscillograph2->get_channels()->begin();it != unioscillograph2->get_channels()->end();++it)
	{
		it->second->set_value(val);
	}
	for(UniOscillograph::ChannelsList::iterator it = unioscillograph3->get_channels()->begin();it != unioscillograph3->get_channels()->end();++it)
	{
		it->second->set_value(val);
	}
	
	val = random() % 100;
	uvalueindicator1->set_value(val);
	add_unioscilchannel1->set_value(val);
//	cout<<"Test::on_timer_tick() for "<<add_unioscilchannel1->get_channelname()<<" val="<<val<<endl;
	val = random() % 100;
	uvalueindicator2->set_value(val);
	add_unioscilchannel2->set_value(val);
//	cout<<"Test::on_timer_tick() for "<<add_unioscilchannel2->get_channelname()<<" val="<<val<<endl;
	val = random() % 100;
	uvalueindicator3->set_value(val);
	add_unioscilchannel3->set_value(val);
//	cout<<"Test::on_timer_tick() for "<<add_unioscilchannel3->get_channelname()<<" val="<<val<<endl;
	val = random() % 100;
	uvalueindicator4->set_value(val);
	add_unioscilchannel4->set_value(val);
//	cout<<"Test::on_timer_tick() for "<<add_unioscilchannel4->get_channelname()<<" val="<<val<<endl;
	val = random() % 100;
	uvalueindicator5->set_value(val);
	add_unioscilchannel5->set_value(val);
//	cout<<"Test::on_timer_tick() for "<<add_unioscilchannel5->get_channelname()<<" val="<<val<<endl;
	val = random() % 100;
	uvalueindicator6->set_value(val);
	add_unioscilchannel6->set_value(val);
//	cout<<"Test::on_timer_tick() for "<<add_unioscilchannel6->get_channelname()<<" val="<<val<<endl;
	
	return true;
}
int main (int argc, char **argv)
{
	Gtk::Window *w;
//	Gtk::Button *btn;

	Gtk::Main Kit(argc, argv);

	if (!g_thread_supported())
		g_thread_init(NULL);

	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(GLADE_DIR + GLADE_FILE);

	gxml->get_widget("window1", w);
	
	gxml->get_widget("unioscillograph1", unioscillograph1);
	unioscillograph1->set_force_history(true);
	unioscillograph1->set_enableHisory(true);
	gxml->get_widget("unioscillograph2", unioscillograph2);
	gxml->get_widget("unioscillograph3", unioscillograph3);

	gxml->get_widget("uvalueindicator1", uvalueindicator1);
	gxml->get_widget("uvalueindicator2", uvalueindicator2);
	gxml->get_widget("uvalueindicator3", uvalueindicator3);
	gxml->get_widget("uvalueindicator4", uvalueindicator4);
	gxml->get_widget("uvalueindicator5", uvalueindicator5);
	gxml->get_widget("uvalueindicator6", uvalueindicator6);

	gxml->get_widget("add_unioscilchannel1", add_unioscilchannel1);
	gxml->get_widget("add_unioscilchannel2", add_unioscilchannel2);
	gxml->get_widget("add_unioscilchannel3", add_unioscilchannel3);
	gxml->get_widget("add_unioscilchannel4", add_unioscilchannel4);
	gxml->get_widget("add_unioscilchannel5", add_unioscilchannel5);
	gxml->get_widget("add_unioscilchannel6", add_unioscilchannel6);
	
    sigc::connection inert_tmr= Glib::signal_timeout().connect(&on_timer_tick,1000);


#if 0
	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );
#endif  
  	Kit.run(*w);

	return 0;
}
