#include <string>
#include <gtkmm.h>
#include <libglademm.h>
#include "UniOscilChannel.h"
#include "UniOscillograph.h"
#include "UValueIndicatormb745.h"

using namespace std;
//using namespace UniSetTypes;

static const string GLADE_DIR = "./";
static const string GLADE_FILE = "main.glade";
static gboolean alpha_channel_support = false;

UniOscillograph* unioscillograph1;
UValueIndicatormb745* uvalueindicator1;

bool on_timer_tick()
{
    double val = random() % 50;
	for(UniOscillograph::ChannelsList::iterator it = unioscillograph1->get_channels()->begin();it != unioscillograph1->get_channels()->end();++it)
	{
		if(it==unioscillograph1->get_channels()->begin())
		{
			it->second->set_value(val);
			continue;
		}
		it->second->set_value(random() % 50);
	}
	uvalueindicator1->set_value(val);
    return true;
}
int main (int argc, char **argv)
{
	Gtk::Window *w;
//	Gtk::Button *btn;

	Gtk::Main Kit(argc, argv);

	if (!g_thread_supported())
		g_thread_init(NULL);
	
	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(GLADE_DIR + GLADE_FILE);
	
	gxml->get_widget("window1", w);
	
	gxml->get_widget("unioscillograph1", unioscillograph1);
	gxml->get_widget("uvalueindicator1", uvalueindicator1);
	
    sigc::connection inert_tmr= Glib::signal_timeout().connect(&on_timer_tick,500);


#if 0
	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );
#endif  
  	Kit.run(*w);

	return 0;
}
