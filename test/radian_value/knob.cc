#include <string>
#include <gtkmm.h>
#include <libglademm.h>
#include "URadialValueIndicator.h"
#include "URadialBar.h"
#include "UValueIndicator.h"
#include "UIndicatorContainer.h"

using namespace std;
//using namespace UniSetTypes;

static const string GLADE_DIR = "./";
static const string GLADE_FILE = "main.glade";
static gboolean alpha_channel_support = false;

URadialValueIndicator* knob1_uradialvalueindicator;
URadialBar* knob1_uradialbar;
URadialValueIndicator* knob2_uradialvalueindicator;
URadialBar* knob2_uradialbar;

bool on_timer_tick()
{
	double val = (random() % 100);
	
	return true;
}
void on_knob1_value_changed(double val)
{
	knob1_uradialbar->set_value(val);
}
void on_knob2_value_changed(double val)
{
	knob2_uradialbar->set_value(val);
}
int main (int argc, char **argv)
{
	Gtk::Window *w;
//	Gtk::Button *btn;

	Gtk::Main Kit(argc, argv);

	if (!g_thread_supported())
		g_thread_init(NULL);

	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(GLADE_DIR + GLADE_FILE);

	gxml->get_widget("window3", w);
	gxml->get_widget("knob1_uradialvalueindicator", knob1_uradialvalueindicator);
	gxml->get_widget("knob1_uradialbar", knob1_uradialbar);
	gxml->get_widget("knob2_uradialvalueindicator", knob2_uradialvalueindicator);
	gxml->get_widget("knob2_uradialbar", knob2_uradialbar);
	
	knob1_uradialvalueindicator->signal_radial_value_change().connect(&on_knob1_value_changed);
	knob2_uradialvalueindicator->signal_radial_value_change().connect(&on_knob2_value_changed);
	
    sigc::connection inert_tmr= Glib::signal_timeout().connect(&on_timer_tick,2000);

#if 0
	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );
#endif  
  	Kit.run(*w);

	return 0;
}
