#include <string>
#include <gtkmm.h>
#include <libglademm.h>
#include "URadialValueIndicator.h"
#include "URadialBar.h"
#include "UValueIndicator.h"
#include "UIndicatorContainer.h"

using namespace std;
//using namespace UniSetTypes;

static const string GLADE_DIR = "./";
static const string GLADE_FILE = "main.glade";
static gboolean alpha_channel_support = false;

URadialValueIndicator* pensel;
URadialBar* uradialbar1;
URadialBar* uradialbar2;
// UValueIndicator* uvalueindicator1;

bool on_timer_tick()
{
	double val = (random() % 100);
	pensel->set_value(val);
	uradialbar1->set_value(val);
	uradialbar2->set_value(val);
// 	uvalueindicator1->set_property_value_(val);
	
	return true;
}
int main (int argc, char **argv)
{
	Gtk::Window *w;
//	Gtk::Button *btn;

	Gtk::Main Kit(argc, argv);

	if (!g_thread_supported())
		g_thread_init(NULL);

	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(GLADE_DIR + GLADE_FILE);

	gxml->get_widget("window2", w);
	gxml->get_widget("uradialbar1", uradialbar1);
	gxml->get_widget("uradialbar2", uradialbar2);
	gxml->get_widget("pensel", pensel);
// 	gxml->get_widget("uvalueindicator1", uvalueindicator1);
	
    sigc::connection inert_tmr= Glib::signal_timeout().connect(&on_timer_tick,2000);

#if 0
	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );
#endif  
  	Kit.run(*w);

	return 0;
}
