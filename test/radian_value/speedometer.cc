#include <string>
#include <gtkmm.h>
#include <libglademm.h>
#include "URadialValueIndicator.h"
#include "UValueIndicator.h"
#include "UIndicatorContainer.h"

using namespace std;
//using namespace UniSetTypes;

static const string GLADE_DIR = "./";
static const string GLADE_FILE = "speedometer.glade";
static gboolean alpha_channel_support = false;

URadialValueIndicator* tahometer;
URadialValueIndicator* coolerwatermeter;
URadialValueIndicator* fuelmeter;
URadialValueIndicator* speedometer1;
UValueIndicator* odometer1;
UValueIndicator* speed_digital1;
URadialValueIndicator* speedometer2;
UValueIndicator* odometer2;
UValueIndicator* odometer3;
UIndicatorContainer* speed_box;

double long odometer_value = 0;
int speed_box_page = 0;

bool on_timer_tick()
{
	tahometer->set_value(1000 * (random() % 7));
	coolerwatermeter->set_value(30 + random()%90);
	fuelmeter->set_value(random() % 40);
	double speed = 10 * (random() % 12);
	speedometer1->set_value(speed);
	speed_digital1->set_property_value_(speed);
	odometer_value = odometer_value + random()%5;
	odometer1->set_property_value_(odometer_value);
	
	speedometer2->set_value(5 * (random() % 32));
	odometer2->set_property_value_(odometer_value);
	odometer3->set_property_value_(10245 + odometer_value);
	
	return true;
}
bool on_conteyner_timer_tick()
{
	speed_box->set_page(speed_box_page);
	speed_box_page = speed_box_page>1 ? 0:(speed_box_page+1);
	return true;
}
int main (int argc, char **argv)
{
	Gtk::Window *w;
//	Gtk::Button *btn;

	Gtk::Main Kit(argc, argv);

	if (!g_thread_supported())
		g_thread_init(NULL);

	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(GLADE_DIR + GLADE_FILE);

	gxml->get_widget("window1", w);
	gxml->get_widget("tahometer", tahometer);
	gxml->get_widget("coolerwatermeter", coolerwatermeter);
	gxml->get_widget("fuelmeter", fuelmeter);
	gxml->get_widget("speedometer1", speedometer1);
	gxml->get_widget("odometer1", odometer1);
	gxml->get_widget("speed_digital1", speed_digital1);
	gxml->get_widget("speedometer2", speedometer2);
	gxml->get_widget("odometer2", odometer2);
	gxml->get_widget("odometer3", odometer3);
	gxml->get_widget("speed_box", speed_box);
	
    sigc::connection inert_tmr= Glib::signal_timeout().connect(&on_timer_tick,2000);
	sigc::connection conteyner_tmr= Glib::signal_timeout().connect(&on_conteyner_timer_tick,5000);
	

#if 0
	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );
#endif  
  	Kit.run(*w);

	return 0;
}
