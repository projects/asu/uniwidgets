#include <string>
#include <gtkmm.h>
#include <libglademm.h>
#include "URadialValueIndicator.h"

using namespace std;
//using namespace UniSetTypes;

static const string GLADE_DIR = "./";
static const string GLADE_FILE = "main.glade";
static gboolean alpha_channel_support = false;

URadialValueIndicator* uradialvalueindicator1;
URadialValueIndicator* uradialvalueindicator2;
URadialValueIndicator* uradialvalueindicator3;
URadialValueIndicator* uradialvalueindicator4;
URadialValueIndicator* uradialvalueindicator5;
URadialValueIndicator* uradialvalueindicator6;
URadialValueIndicator* uradialvalueindicator7;
URadialValueIndicator* uradialvalueindicator8;
URadialValueIndicator* uradialvalueindicator9;

bool on_timer_tick()
{
    double val = random() % 100;
    uradialvalueindicator1->set_value(val);
    uradialvalueindicator2->set_value(random() % 200 -100);
    uradialvalueindicator3->set_value(val);
    uradialvalueindicator4->set_value(val);
    uradialvalueindicator5->set_value(val);
    uradialvalueindicator6->set_value(val);
    uradialvalueindicator7->set_value(val);
    uradialvalueindicator8->set_value(val);
    uradialvalueindicator9->set_value(val);
    return true;
}
int main (int argc, char **argv)
{
	Gtk::Window *w;
//	Gtk::Button *btn;

	Gtk::Main Kit(argc, argv);

	if (!g_thread_supported())
		g_thread_init(NULL);

	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(GLADE_DIR + GLADE_FILE);

	gxml->get_widget("window1", w);
    gxml->get_widget("uradialvalueindicator1", uradialvalueindicator1);
    gxml->get_widget("uradialvalueindicator2", uradialvalueindicator2);
    gxml->get_widget("uradialvalueindicator3", uradialvalueindicator3);
    gxml->get_widget("uradialvalueindicator4", uradialvalueindicator4);
    gxml->get_widget("uradialvalueindicator5", uradialvalueindicator5);
    gxml->get_widget("uradialvalueindicator6", uradialvalueindicator6);
    gxml->get_widget("uradialvalueindicator7", uradialvalueindicator7);
    gxml->get_widget("uradialvalueindicator8", uradialvalueindicator8);
    gxml->get_widget("uradialvalueindicator9", uradialvalueindicator9);

    sigc::connection inert_tmr= Glib::signal_timeout().connect(&on_timer_tick,3000);


#if 0
	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );
#endif  
  	Kit.run(*w);

	return 0;
}
