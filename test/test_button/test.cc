#include <iostream>
#include <gtkmm.h>
#include <libglademm.h>
#include <Configuration.h>
#include "UProxyWidget.h"
#include "Image.h"
#include <usvgwidgets/USVGButton.h>
#include <IButton.h>

using namespace std;
using namespace UniSetTypes;
using namespace UniWidgets;

static const string GLADE_DIR = "glade/";
static const string GLADE_FILE = "database.glade";
static gboolean alpha_channel_support = false;

bool on_button_event(GdkEvent *event)
{
	cout<<"EVENT BUTTON"<<endl;
	return false;
}
bool on_button_press_event(GdkEventButton* event)
{
	cout<<"EVENT BUTTON PRESS"<<endl;
	return true;
}
void clicked()
{
	cout<<"EVENT BUTTON CLICKED"<<endl;
}
int main (int argc, char **argv)
{
	Gtk::Window *w;
	Gtk::Button *btn;
	USVGButton *ubtn;
	IButton *ibtn;

	Gtk::Main Kit(argc, argv);

	if (!g_thread_supported())
		g_thread_init(NULL);

	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(GLADE_DIR + GLADE_FILE);

	gxml->get_widget("window1", w);
	gxml->get_widget("jrn_btnTab1", btn);
	gxml->get_widget("test_usvgbutton", ubtn);
	gxml->get_widget("ibutton1", ibtn);
	cout<<"TEXT label - "<<ibtn->get_label()<<endl;
	cout<<"TEXT width - "<<ibtn->get_width()<<endl;
	cout<<"TEXT tooltip - "<<ibtn->get_tooltip_text()<<endl;
	cout<<"TEXT border - "<<ibtn->get_border_width ()<<endl;
// 	btn->signal_event().connect(sigc::ptr_fun(on_button_event));
	btn->signal_button_press_event().connect(sigc::ptr_fun(on_button_press_event));
	btn->signal_clicked().connect(sigc::ptr_fun(clicked));
	
	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );
	Kit.run(*w);
	delete w;
	return 0;
}
