#ifdef __cplusplus
  extern "C" {
#endif
  
#include <config.h>

#include "glade-gtkuni.h"
#include "glade-unibar-editor.h"
#include "glade-unimodel-data.h"

#include <gladeui/glade-editor-property.h>
#include <gladeui/glade-base-editor.h>
#include <gladeui/glade-xml-utils.h>
#include <gladeui/glade-property.h>

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>

#include "bar.h"

struct _GladeBarData
{
  GValue    value;
  gchar    *name;

//   gboolean  i18n_translatable;
//   gchar    *i18n_context;
//   gchar    *i18n_comment;
};

typedef _GladeBarData GladeBarData;

GladeBarData *
glade_bar_data_copy (GladeBarData *data)
{
  if (!data)
    return NULL;

  GladeBarData *dup = g_new0 (GladeBarData, 1);

  g_value_init (&dup->value, G_VALUE_TYPE (&data->value));
  g_value_copy (&data->value, &dup->value);

  dup->name              = g_strdup (data->name);

//   dup->i18n_translatable = data->i18n_translatable;
//   dup->i18n_context      = g_strdup (data->i18n_context);
//   dup->i18n_comment      = g_strdup (data->i18n_comment);

  return dup;
}

GNode *
glade_bar_data_tree_copy (GNode *node)
{
  printf("glade_model_data_tree_copy\n");
  return g_node_copy_deep (node, (GCopyFunc)glade_bar_data_copy, NULL);
}

void
glade_bar_data_tree_free (GNode *node)
{
  printf("glade_model_data_tree_free\n");
  if (node)
  {
      g_node_destroy (node);
  }
}

GType
glade_bar_data_tree_get_type (void)
{
static GType type_id = 0;

if (!type_id)
    type_id = g_boxed_type_register_static
      ("GladeBarDataTree",(GBoxedCopyFunc) glade_bar_data_tree_copy,
       (GBoxedFreeFunc) glade_bar_data_tree_free);
return type_id;
}

/* -------------------------------- GtkUnibar -------------------------------- */

gboolean
glade_gtk_unibar_depends (GladeWidgetAdaptor * adaptor,
                         GladeWidget * widget, GladeWidget * another)
{
//   if (GTK_IS_UNIBAR_BUFFER (glade_widget_get_object (another)))
//     return TRUE;
  printf("glade_gtk_bar_read_columns\n");
  return GWA_GET_CLASS (GTK_TYPE_WIDGET)->depends (adaptor, widget, another);
}


static void
glade_gtk_unibar_changed (GtkEditable * editable, GladeWidget * gunibar)
{
  printf("glade_gtk_unibar_changed\n");
//   const gchar *text, *text_prop;
//   GladeProperty *prop;
//   gboolean use_buffer;
// 
//   if (glade_widget_superuser ())
//     return;
// 
//   text = gtk_unibar_get_text (GTK_UNIBAR (editable));
// 
//   glade_widget_property_get (gunibar, "text", &text_prop);
//   glade_widget_property_get (gunibar, "use-unibar-buffer", &use_buffer);
// 
//   if (use_buffer == FALSE && g_strcmp0 (text, text_prop))
//     {
//       if ((prop = glade_widget_get_property (gunibar, "text")))
//         glade_command_set_property (prop, text);
//     }
}

void
glade_gtk_unibar_post_create (GladeWidgetAdaptor * adaptor,
                             GObject * object, GladeCreateReason reason)
{
  printf("NOW glade_gtk_unibar_post_create\n");
//   GladeWidget *gunibar;
// 
//   g_return_if_fail (GTK_IS_UNIBAR (object));
//   gunibar = glade_widget_get_from_gobject (object);
//   g_return_if_fail (GLADE_IS_WIDGET (gunibar));
// 
//   g_signal_connect (object, "changed",
//                     G_CALLBACK (glade_gtk_unibar_changed), gunibar);
}

GladeEditable *
glade_gtk_unibar_create_editable (GladeWidgetAdaptor * adaptor,
                                 GladeEditorPageType type)
{
  printf("NOW glade_gtk_unibar_create_editable\n");
  GladeEditable *editable;

  /* Get base editable */
  editable = GWA_GET_CLASS (GTK_TYPE_WIDGET)->create_editable (adaptor, type);
  printf("NOW glade_gtk_unibar_create_editable 1\n");
  if (type == GLADE_PAGE_GENERAL)
    return (GladeEditable *) glade_unibar_editor_new (adaptor, editable);
  printf("NOW glade_gtk_unibar_create_editable 2\n");
  return editable;
}


void glade_gtk_unibar_set_property (GladeWidgetAdaptor * adaptor,
                              GObject * object,
                              const gchar * id, const GValue * value)
{
  printf("NOW glade_gtk_unibar_set_property\n");
//   GladeImageEditMode mode;
  GladeWidget *gwidget = glade_widget_get_from_gobject (object);
  GladeProperty *property = glade_widget_get_property (gwidget, id);

//   if (!strcmp (id, "use-unibar-buffer"))
//     {
//       printf("glade_gtk_unibar_set_property 1\n");
//       glade_widget_property_set_sensitive (gwidget, "text", FALSE,
//                                            NOT_SELECTED_MSG);
//       glade_widget_property_set_sensitive (gwidget, "buffer", FALSE,
//                                            NOT_SELECTED_MSG);
// 
//       if (g_value_get_boolean (value))
//         glade_widget_property_set_sensitive (gwidget, "buffer", TRUE, NULL);
//       else
//         glade_widget_property_set_sensitive (gwidget, "text", TRUE, NULL);
//     }
//   else if (!strcmp (id, "primary-icon-mode"))
//     {
//       printf("glade_gtk_unibar_set_property 2\n");
//       mode = g_value_get_int (value);
// 
//       glade_widget_property_set_sensitive (gwidget, "primary-icon-stock", FALSE,
//                                            NOT_SELECTED_MSG);
//       glade_widget_property_set_sensitive (gwidget, "primary-icon-name", FALSE,
//                                            NOT_SELECTED_MSG);
//       glade_widget_property_set_sensitive (gwidget, "primary-icon-pixbuf",
//                                            FALSE, NOT_SELECTED_MSG);
// 
//       switch (mode)
//         {
//           case GLADE_IMAGE_MODE_STOCK:
//             glade_widget_property_set_sensitive (gwidget, "primary-icon-stock",
//                                                  TRUE, NULL);
//             break;
//           case GLADE_IMAGE_MODE_ICON:
//             glade_widget_property_set_sensitive (gwidget, "primary-icon-name",
//                                                  TRUE, NULL);
//             break;
//           case GLADE_IMAGE_MODE_FILENAME:
//             glade_widget_property_set_sensitive (gwidget, "primary-icon-pixbuf",
//                                                  TRUE, NULL);
//             break;
//         }
//     }
//   else if (!strcmp (id, "secondary-icon-mode"))
//     {
//       printf("glade_gtk_unibar_set_property 3\n");
//       mode = g_value_get_int (value);
// 
//       glade_widget_property_set_sensitive (gwidget, "secondary-icon-stock",
//                                            FALSE, NOT_SELECTED_MSG);
//       glade_widget_property_set_sensitive (gwidget, "secondary-icon-name",
//                                            FALSE, NOT_SELECTED_MSG);
//       glade_widget_property_set_sensitive (gwidget, "secondary-icon-pixbuf",
//                                            FALSE, NOT_SELECTED_MSG);
// 
//       switch (mode)
//         {
//           case GLADE_IMAGE_MODE_STOCK:
//             glade_widget_property_set_sensitive (gwidget,
//                                                  "secondary-icon-stock", TRUE,
//                                                  NULL);
//             break;
//           case GLADE_IMAGE_MODE_ICON:
//             glade_widget_property_set_sensitive (gwidget, "secondary-icon-name",
//                                                  TRUE, NULL);
//             break;
//           case GLADE_IMAGE_MODE_FILENAME:
//             glade_widget_property_set_sensitive (gwidget,
//                                                  "secondary-icon-pixbuf", TRUE,
//                                                  NULL);
//             break;
//         }
//     }
//   else if (!strcmp (id, "primary-icon-tooltip-text") ||
//            !strcmp (id, "primary-icon-tooltip-markup"))
//     {
//       printf("glade_gtk_unibar_set_property 4\n");
//       /* Avoid a silly crash in GTK+ */
//       if (gtk_unibar_get_icon_storage_type (GTK_UNIBAR (object),
//                                            GTK_UNIBAR_ICON_PRIMARY) !=
//           GTK_IMAGE_EMPTY)
//         GWA_GET_CLASS (GTK_TYPE_WIDGET)->set_property (adaptor, object, id,
//                                                        value);
//     }
//   else if (!strcmp (id, "secondary-icon-tooltip-text") ||
//            !strcmp (id, "secondary-icon-tooltip-markup"))
//     {
//       printf("glade_gtk_unibar_set_property 5\n");
      /* Avoid a silly crash in GTK+ */
//       if (gtk_unibar_get_icon_storage_type (GTK_UNIBAR (object),
//                                            GTK_UNIBAR_ICON_SECONDARY) !=
//           GTK_IMAGE_EMPTY)
//         GWA_GET_CLASS (GTK_TYPE_WIDGET)->set_property (adaptor, object, id,
//                                                        value);
//     }
//   else
    if (!strcmp (id, "color"))
    {
      printf("glade_gtk_unibar_set_property \"COLOR\"\n");
//       g_signal_handlers_block_by_func (object, glade_gtk_unibar_changed,
//                                        gwidget);
// 
      if (g_value_get_string (value))
        gtk_bar_set_color (GTK_BAR (object), g_value_get_string (value));
      else
        gtk_bar_set_color (GTK_BAR (object), "");
// 
//       g_signal_handlers_unblock_by_func (object, glade_gtk_unibar_changed,
//                                          gwidget);

    }
    else//if (GPC_VERSION_CHECK(GLADE_PROPERTY_GET_KLASS (property), gtk_major_version, gtk_minor_version + 1))
      GWA_GET_CLASS (GTK_TYPE_WIDGET)->set_property (adaptor, object, id, value);
}

void
glade_gtk_unibar_read_widget (GladeWidgetAdaptor * adaptor,
                             GladeWidget * widget, GladeXmlNode * node)
{
  printf("NOW glade_gtk_unibar_read_widget\n");
  GladeProperty *property;
  printf("NOW glade_gtk_unibar_read_widget 2\n");
  /* First chain up and read in all the normal properties.. */
  GWA_GET_CLASS (GTK_TYPE_WIDGET)->read_widget (adaptor, widget, node);

//   if (glade_widget_property_original_default (widget, "color") == FALSE)
//   {
//     property = glade_widget_get_property (widget, "color");
//     glade_widget_property_set (widget, "use-unibar-buffer", FALSE);
// 
//     glade_property_sync (property);
//   }
//   else
//     {
//       gint target_minor, target_major;
// 
//       glade_project_get_target_version (glade_widget_get_project (widget), "gtk+", 
// 					&target_major, &target_minor);
// 
//       property = glade_widget_get_property (widget, "buffer");
// 
//       /* Only default to the buffer setting if the project version supports it. */
//       if (GPC_VERSION_CHECK (glade_property_get_class (property), target_major, target_minor))
//         {
//           glade_widget_property_set (widget, "use-unibar-buffer", TRUE);
//           glade_property_sync (property);
//         }
//       else
//         glade_widget_property_set (widget, "use-unibar-buffer", FALSE);
//     }

//   if (glade_widget_property_original_default (widget, "primary-icon-name") ==
//       FALSE)
//     {
//       property = glade_widget_get_property (widget, "primary-icon-name");
//       glade_widget_property_set (widget, "primary-icon-mode",
//                                  GLADE_IMAGE_MODE_ICON);
//     }
//   else if (glade_widget_property_original_default
//            (widget, "primary-icon-pixbuf") == FALSE)
//     {
//       property = glade_widget_get_property (widget, "primary-icon-pixbuf");
//       glade_widget_property_set (widget, "primary-icon-mode",
//                                  GLADE_IMAGE_MODE_FILENAME);
//     }
//   else                          /*  if (glade_widget_property_original_default (widget, "stock") == FALSE) */
//     {
//       property = glade_widget_get_property (widget, "primary-icon-stock");
//       glade_widget_property_set (widget, "primary-icon-mode",
//                                  GLADE_IMAGE_MODE_STOCK);
//     }

//   glade_property_sync (property);

//   if (glade_widget_property_original_default (widget, "secondary-icon-name") ==
//       FALSE)
//     {
//       property = glade_widget_get_property (widget, "secondary-icon-name");
//       glade_widget_property_set (widget, "secondary-icon-mode",
//                                  GLADE_IMAGE_MODE_ICON);
//     }
//   else if (glade_widget_property_original_default
//            (widget, "secondary-icon-pixbuf") == FALSE)
//     {
//       property = glade_widget_get_property (widget, "secondary-icon-pixbuf");
//       glade_widget_property_set (widget, "secondary-icon-mode",
//                                  GLADE_IMAGE_MODE_FILENAME);
//     }
//   else                          /*  if (glade_widget_property_original_default (widget, "stock") == FALSE) */
//     {
//       property = glade_widget_get_property (widget, "secondary-icon-stock");
//       glade_widget_property_set (widget, "secondary-icon-mode",
//                                  GLADE_IMAGE_MODE_STOCK);
//     }

//   glade_property_sync (property);
  printf("NOW glade_gtk_unibar_read_widget end\n");
}

/*--------------------------- GtkListStore/GtkTreeStore ---------------------------------*/

#define GLADE_TAG_COLUMNS "columns"
#define GLADE_TAG_COLUMN  "column"
#define GLADE_TAG_TYPE    "type"

#define GLADE_TAG_ROW           "row"
#define GLADE_TAG_DATA          "data"
#define GLADE_TAG_COL           "col"

static void
glade_gtk_bar_set_columns (GObject *object,
const GValue *value)
{
  printf("glade_gtk_bar_set_columns\n");
#if 0
  GList *l = (GList*)g_value_get_boxed (value);
  gint i, n = g_list_length (l);
  GType *types = g_new (GType, n);

  for (i = 0; l; l = g_list_next (l), i++)
  {
    GladeColumnType *data = l->data;
    types[i] = data->type;
  }

  if (GTK_IS_BAR (object))
    gtk_bar_set_column_types (GTK_BAR (object), n, types);
  else
    gtk_tree_store_set_column_types (GTK_TREE_STORE (object), n, types);
#endif
}

static void
glade_gtk_bar_set_data (GObject *object,
                              const GValue *value)
{
  printf("glade_gtk_bar_set_data\n");
  GladeWidget     *gwidget = glade_widget_get_from_gobject (object);
  GList           *columns = NULL;
  GNode           *data_tree, *row, *iter;
  gint             colnum;
  GtkTreeIter      row_iter;
  GladeUniModelData  *data;
  GType            column_type;
  /*
  if (GTK_IS_BAR (object))
    gtk_bar_clear (GTK_BAR (object));
  else
    gtk_tree_store_clear (GTK_TREE_STORE (object));
  */
  glade_widget_property_get (gwidget, "columns", &columns);
  data_tree = (GNode*)g_value_get_boxed (value);
  printf("glade_gtk_bar_set_data2\n");
  if(!data_tree)
    printf("glade_gtk_bar_set_data2 not data_tree\n");
  if(!columns)
    printf("glade_gtk_bar_set_data2 not columns\n");
  /* Nothing to enter without columns defined */
  if (!data_tree || !columns)
    return;
  printf("glade_gtk_bar_set_data3\n");
  for (row = data_tree->children; row; row = row->next)
  {
    printf("glade_gtk_bar_set_data4\n");
#if 0
    if (GTK_IS_BAR (object))
      gtk_bar_append (GTK_BAR (object), &row_iter);
    else
      /* (for now no child data... ) */
      gtk_tree_store_append (GTK_TREE_STORE (object), &row_iter, NULL);
#endif
    for (colnum = 0, iter = row->children; iter;
         colnum++, iter = iter->next)
    {
      data = (GladeUniModelData*)iter->data;

      if (!g_list_nth (columns, colnum))
        break;

      /* Abort if theres a type mismatch, the widget's being rebuilt
      * and a sync will come soon with the right values
      */
      column_type = gtk_tree_model_get_column_type (GTK_TREE_MODEL (object), colnum);
      if (!g_type_is_a (G_VALUE_TYPE (&data->value), column_type))
        break;
      /*
      if (GTK_IS_BAR (object))
        gtk_bar_set_value (GTK_BAR (object),
                                  &row_iter,
                                  colnum, &data->value);
      else
        gtk_tree_store_set_value (GTK_TREE_STORE (object),
                                  &row_iter,
                                  colnum, &data->value);
      */
    }
  }
}

void
glade_gtk_bar_set_property (GladeWidgetAdaptor *adaptor,
                              GObject *object,
                              const gchar *property_name,
                              const GValue *value)
{
  printf("glade_gtk_bar_set_property\n");
  if (strcmp (property_name, "columns") == 0)
  {
    glade_gtk_bar_set_columns (object, value);
  }
  else if (strcmp (property_name, "data") == 0)
  {
    printf("HERE value=%d\n",value);
    if(!value)
      printf("empty value!!!\n");
    glade_gtk_bar_set_data (object, value);
  }
  else
    /* Chain Up */
    GWA_GET_CLASS (G_TYPE_OBJECT)->set_property (adaptor,
                   object,
                   property_name,
                   value);
}

GladeEditorProperty *
glade_gtk_bar_create_eprop (GladeWidgetAdaptor *adaptor,
                            GladePropertyClass *klass,
                            gboolean            use_command)
{
  printf("glade_gtk_bar_create_eprop ---------> %s\n",klass->pspec->name);
  GladeEditorProperty *eprop;

  /* chain up.. */
//   if (klass->pspec->value_type == GLADE_TYPE_COLUMN_TYPE_LIST)
//     eprop = g_object_new (GLADE_TYPE_EPROP_COLUMN_TYPES,
//                           "property-class", klass,
//                           "use-command", use_command,
//                           NULL);
//   else
  if (klass->pspec->value_type == GLADE_TYPE_UNIMODEL_DATA_UNIBAR)
  {
    printf("glade_gtk_bar_create_eprop = type unimodel\n");
    eprop = (GladeEditorProperty*)g_object_new (GLADE_TYPE_EPROP_UNIMODEL_DATA,
                          "property-class", klass,
                          "use-command", use_command,
                          NULL);
  }
  else
    eprop = GWA_GET_CLASS
        (G_TYPE_OBJECT)->create_eprop (adaptor,
         klass,
         use_command);
  return eprop;
}
#if 0
GladeEditable *
    glade_gtk_bar_create_editable (GladeWidgetAdaptor  *adaptor,
                                     GladeEditorPageType  type)
{
  GladeEditable *editable;

  /* Get base editable */
  editable = GWA_GET_CLASS (G_TYPE_OBJECT)->create_editable (adaptor, type);

  if (type == GLADE_PAGE_GENERAL)
    return (GladeEditable *)glade_store_editor_new (adaptor, editable);

  return editable;
}
#endif
gchar *
    glade_gtk_bar_string_from_value (GladeWidgetAdaptor *adaptor,
                                       GladePropertyClass *klass,
                                       const GValue       *value,
                                       GladeProjectFormat  fmt)
{
  printf("glade_gtk_bar_string_from_value\n");
  GString *string;
#if 0
  if (klass->pspec->value_type == GLADE_TYPE_COLUMN_TYPE_LIST)
  {
    GList *l;
    string = g_string_new ("");
    for (l = g_value_get_boxed (value); l; l = g_list_next (l))
    {
      GladeColumnType *data = l->data;
      g_string_append_printf (string, (g_list_next (l)) ? "%s:%s|" : "%s:%s",
                              g_type_name (data->type), data->column_name);
    }
    return g_string_free (string, FALSE);
  }
  else
#endif
  if (klass->pspec->value_type == GLADE_TYPE_UNIMODEL_DATA_UNIBAR)
  {
    printf("glade_gtk_bar_string_from_value 2 name---->! %s\n",klass->pspec->name);
    GladeUniModelData *data;
    GNode *data_tree, *row, *iter;
    gint rownum;
    gchar *str;
    gboolean is_last;
    printf("glade_gtk_bar_string_from_value 3---->!\n");
    /* Return a unique string for the backend to compare */
    data_tree = (GNode*)g_value_get_boxed (value);
    printf("glade_gtk_bar_string_from_value 4---->! %d\n",data_tree);
    if (!data_tree || !data_tree->children)
      return g_strdup ("");
    printf("glade_gtk_bar_string_from_value 5---->!\n");
    string = g_string_new ("");
    for (rownum = 0, row = data_tree->children; row;
         rownum++, row = row->next)
    {
      for (iter = row->children; iter; iter = iter->next)
      {
        data = (GladeUniModelData*)iter->data;

        str = glade_utils_string_from_value (&data->value, fmt);

        is_last = !row->next && !iter->next;
        g_string_append_printf (string, "%s[%d]:%s",
                                data->name, rownum, str);
/*
        if (data->i18n_translatable)
          g_string_append_printf (string, " translatable");
        if (data->i18n_context)
          g_string_append_printf (string, " i18n-context:%s", data->i18n_context);
        if (data->i18n_comment)
          g_string_append_printf (string, " i18n-comment:%s", data->i18n_comment);
*/
        if (!is_last)
          g_string_append_printf (string, "|");

        g_free (str);
      }
    }
    return g_string_free (string, FALSE);
  }
  else
    return GWA_GET_CLASS
        (G_TYPE_OBJECT)->string_from_value (adaptor,
         klass,
         value,
         fmt);
}

static void
glade_gtk_bar_write_columns (GladeWidget        *widget,
                                   GladeXmlContext    *context,
                                   GladeXmlNode       *node)
{
  printf("glade_gtk_bar_write_columns\n");
#if 0
  GladeXmlNode  *columns_node;
  GladeProperty *prop;
  GList *l;

  prop = glade_widget_get_property (widget, "columns");

  columns_node = glade_xml_node_new (context, GLADE_TAG_COLUMNS);
  
  for (l = g_value_get_boxed (prop->value); l; l = g_list_next (l))
  {
    GladeColumnType *data = l->data;
    GladeXmlNode  *column_node, *comment_node;
    
    /* Write column names in comments... */
    gchar *comment = g_strdup_printf (" column-name %s ", data->column_name);
    comment_node = glade_xml_node_new_comment (context, comment);
    glade_xml_node_append_child (columns_node, comment_node);
    g_free (comment);
    
    column_node = glade_xml_node_new (context, GLADE_TAG_COLUMN);
    glade_xml_node_append_child (columns_node, column_node);
    glade_xml_node_set_property_string (column_node, GLADE_TAG_TYPE,
                                        g_type_name (data->type));
  }

  if (!glade_xml_node_get_children (columns_node))
    glade_xml_node_delete (columns_node);
  else
    glade_xml_node_append_child (node, columns_node);
#endif
}

static void
glade_gtk_bar_write_data (GladeWidget        *widget,
                                GladeXmlContext    *context,
                                GladeXmlNode       *node)
{
  printf("glade_gtk_bar_write_data\n");
#if 0
  GladeXmlNode   *data_node, *col_node, *row_node;
  GList          *columns = NULL;
  GladeModelData *data;
  GNode          *data_tree = NULL, *row, *iter;
  gint            colnum;

  glade_widget_property_get (widget, "data", &data_tree);
  glade_widget_property_get (widget, "columns", &columns);

  /* XXX log errors about data not fitting columns here when
  * loggin is available
  */
  if (!data_tree || !columns)
    return;

  data_node = glade_xml_node_new (context, GLADE_TAG_DATA);

  for (row = data_tree->children; row; row = row->next)
  {
    row_node = glade_xml_node_new (context, GLADE_TAG_ROW);
    glade_xml_node_append_child (data_node, row_node);

    for (colnum = 0, iter = row->children; iter;
         colnum++, iter = iter->next)
    {
      gchar   *string, *column_number;

      data = iter->data;

      string = glade_utils_string_from_value (&data->value,
                                               glade_project_get_format (widget->project));

      /* XXX Log error: data col j exceeds columns on row i */
      if (!g_list_nth (columns, colnum))
        break;
      
      column_number = g_strdup_printf ("%d", colnum);
      
      col_node = glade_xml_node_new (context, GLADE_TAG_COL);
      glade_xml_node_append_child (row_node, col_node);
      glade_xml_node_set_property_string (col_node, GLADE_TAG_ID,
                                          column_number);
      glade_xml_set_content (col_node, string);

      if (data->i18n_translatable)
        glade_xml_node_set_property_string (col_node,
                                            GLADE_TAG_TRANSLATABLE,
                                            GLADE_XML_TAG_I18N_TRUE);
      if (data->i18n_context)
        glade_xml_node_set_property_string (col_node,
                                            GLADE_TAG_CONTEXT,
                                            data->i18n_context);
      if (data->i18n_comment)
        glade_xml_node_set_property_string (col_node,
                                            GLADE_TAG_COMMENT,
                                            data->i18n_comment);

      
      g_free (column_number);
      g_free (string);
    }
  }

  if (!glade_xml_node_get_children (data_node))
    glade_xml_node_delete (data_node);
  else
    glade_xml_node_append_child (node, data_node);
#endif
}


void
glade_gtk_bar_write_widget (GladeWidgetAdaptor *adaptor,
                                  GladeWidget        *widget,
                                  GladeXmlContext    *context,
                                  GladeXmlNode       *node)
{
  printf("glade_gtk_bar_write_widget\n");
  if (!glade_xml_node_verify
       (node, GLADE_XML_TAG_WIDGET (glade_project_get_format (widget->project))))
    return;

  /* First chain up and write all the normal properties.. */
  GWA_GET_CLASS (G_TYPE_OBJECT)->write_widget (adaptor, widget, context, node);

  glade_gtk_bar_write_columns (widget, context, node);
  glade_gtk_bar_write_data (widget, context, node);
}

static void
glade_gtk_bar_read_columns (GladeWidget *widget, GladeXmlNode *node)
{
  printf("glade_gtk_bar_read_columns\n");
#if 0
  GladeNameContext *context;
  GladeXmlNode *columns_node;
  GladeProperty *property;
  GladeXmlNode *prop;
  GList *types = NULL;
  GValue value = {0,};
  gchar column_name[256];

  column_name[0] = '\0';
  column_name[255] = '\0';

  if ((columns_node = glade_xml_search_child (node, GLADE_TAG_COLUMNS)) == NULL)
    return;

  context = glade_name_context_new ();

  for (prop = glade_xml_node_get_children_with_comments (columns_node); prop;
       prop = glade_xml_node_next_with_comments (prop))
  {
    GladeColumnType *data = g_new0 (GladeColumnType, 1);
    gchar *type, *comment_str, buffer[256];

    if (!glade_xml_node_verify_silent (prop, GLADE_TAG_COLUMN) &&
         !glade_xml_node_is_comment (prop)) continue;

    if (glade_xml_node_is_comment (prop))
    {
      comment_str = glade_xml_get_content (prop);
      if (sscanf (comment_str, " column-name %s", buffer) == 1)
        strncpy (column_name, buffer, 255);

      g_free (comment_str);
      continue;
    }

    type = glade_xml_get_property_string_required (prop, GLADE_TAG_TYPE, NULL);
    data->type        = g_type_from_name (type);
    data->column_name = column_name[0] ? g_strdup (column_name) : g_ascii_strdown (type, -1);

    if (glade_name_context_has_name (context, data->column_name))
    {
      gchar *name = glade_name_context_new_name (context, data->column_name);
      g_free (data->column_name);
      data->column_name = name;
    }
    glade_name_context_add_name (context, data->column_name);
    
    types = g_list_prepend (types, data);
    g_free (type);

    column_name[0] = '\0';
  }
  
  property = glade_widget_get_property (widget, "columns");
  g_value_init (&value, GLADE_TYPE_COLUMN_TYPE_LIST);
  g_value_take_boxed (&value, g_list_reverse (types));
  glade_property_set_value (property, &value);
  g_value_unset (&value);
#endif
}

static void
glade_gtk_bar_read_data (GladeWidget *widget, GladeXmlNode *node)
{
  printf("glade_gtk_bar_read_data\n");
#if 0
  GladeXmlNode *data_node, *row_node, *col_node;
  GNode *data_tree, *row, *item;
  GladeModelData *data;
  GValue *value;
  GList *column_types = NULL, *list;
  GladeColumnType *column_type;
  gint colnum;

  if ((data_node = glade_xml_search_child (node, GLADE_TAG_DATA)) == NULL)
    return;

  /* XXX FIXME: Warn that columns werent there when parsing */
  if (!glade_widget_property_get (widget, "columns", &column_types) || !column_types)
    return;

  /* Create root... */
  data_tree = g_node_new (NULL);
  
  for (row_node = glade_xml_node_get_children (data_node); row_node;
       row_node = glade_xml_node_next (row_node))
  {
    gchar *value_str;

    if (!glade_xml_node_verify (row_node, GLADE_TAG_ROW))
      continue;

    row = g_node_new (NULL);
    g_node_append (data_tree, row);

    /* XXX FIXME: we are assuming that the columns are listed in order */
    for (colnum = 0, col_node = glade_xml_node_get_children (row_node); col_node;
         col_node = glade_xml_node_next (col_node))
    {

      if (!glade_xml_node_verify (col_node, GLADE_TAG_COL))
        continue;

      if (!(list = g_list_nth (column_types, colnum)))
        /* XXX Log this too... */
        continue;

      column_type = list->data;

      /* XXX Do we need object properties to somehow work at load time here ??
      * should we be doing this part in "finished" ? ... todo thinkso...
      */
      value_str = glade_xml_get_content (col_node);
      value     = glade_utils_value_from_string (column_type->type, value_str, widget->project, widget);
      g_free (value_str);

      data = glade_model_data_new (column_type->type, column_type->column_name);

      g_value_copy (value, &data->value);
      g_value_unset (value);
      g_free (value);

      data->name = g_strdup (column_type->column_name);
      data->i18n_translatable = glade_xml_get_property_boolean (col_node, GLADE_TAG_TRANSLATABLE, FALSE);
      data->i18n_context = glade_xml_get_property_string (col_node, GLADE_TAG_CONTEXT);
      data->i18n_comment = glade_xml_get_property_string (col_node, GLADE_TAG_COMMENT);

      item = g_node_new (data);
      g_node_append (row, item);

      /* dont increment colnum on invalid xml tags... */
      colnum++;
    }
  }

  if (data_tree->children)
    glade_widget_property_set (widget, "data", data_tree);

  glade_model_data_tree_free (data_tree);
#endif
}

void
glade_gtk_bar_read_widget (GladeWidgetAdaptor *adaptor,
                           GladeWidget        *widget,
                           GladeXmlNode       *node)
{
  printf("glade_gtk_bar_read_widget\n");
  if (!glade_xml_node_verify
       (node, GLADE_XML_TAG_WIDGET (glade_project_get_format (widget->project))))
    return;

  /* First chain up and read in all the normal properties.. */
  GWA_GET_CLASS (G_TYPE_OBJECT)->read_widget (adaptor, widget, node);

  glade_gtk_bar_read_columns (widget, node);

  if (GTK_IS_BAR (widget->object))
    glade_gtk_bar_read_data (widget, node);
}

#ifdef __cplusplus
  }
#endif
  