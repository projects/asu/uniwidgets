#ifdef __cplusplus
  extern "C" {
#endif
  
#include <config.h>
#include <gladeui/glade.h>
#include <gladeui/glade-editable.h>
// #include <glib/gi18n-lib.h>
#include <gdk/gdkkeysyms.h>

#include "glade-unibar-editor.h"
#include "../unibar/wrap_init.h"
//#include "glade-image-editor.h" // For GladeImageEditMode
#define _(x) x

static void glade_unibar_editor_finalize (GObject * object);

static void glade_unibar_editor_editable_init (GladeEditableIface * iface);

static void glade_unibar_editor_grab_focus (GtkWidget * widget);

#if 0
static void gtk_unibar_buildable_init  (GtkBuildableIface      *iface);
// static GladeEditableIface *parent_editable_iface;

/* buildable */

static gboolean gtk_unibar_buildable_custom_tag_start (GtkBuildable  *buildable,
                                                          GtkBuilder    *builder,
                                                          GObject       *child,
                                                          const gchar   *tagname,
                                                          GMarkupParser *parser,
                                                          gpointer      *data);
static void     gtk_unibar_buildable_custom_finished (GtkBuildable   *buildable,
                                                          GtkBuilder     *builder,
                                                          GObject        *child,
                                                          const gchar    *tagname,
                                                          gpointer        user_data);
#endif

G_DEFINE_TYPE_WITH_CODE (GladeUnibarEditor, glade_unibar_editor, GTK_TYPE_VBOX,
                         G_IMPLEMENT_INTERFACE (GLADE_TYPE_EDITABLE,
                             glade_unibar_editor_editable_init));

#if 0
//G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE, gtk_tree_store_buildable_init)
static void
unibar_start_element (GMarkupParseContext *context,
                              const gchar         *element_name,
                              const gchar        **names,
                              const gchar        **values,
                              gpointer            user_data,
                              GError            **error)
{
  guint i;
  GSListSubParserData *data = (GSListSubParserData*)user_data;

  for (i = 0; names[i]; i++)
  {
    if (strcmp (names[i], "type") == 0)
      data->items = g_slist_prepend (data->items, g_strdup (values[i]));
  }
}

static void
unibar_end_element (GMarkupParseContext *context,
                            const gchar         *element_name,
                            gpointer             user_data,
                            GError             **error)
{
  GSListSubParserData *data = (GSListSubParserData*)user_data;

  g_assert(data->builder);

//   if (strcmp (element_name, "columns") == 0)
//   {
//     GSList *l;
//     GType *types;
//     int i;
//     GType type;
// 
//     data = (GSListSubParserData*)user_data;
//     data->items = g_slist_reverse (data->items);
//     types = g_new0 (GType, g_slist_length (data->items));
// 
//     for (l = data->items, i = 0; l; l = l->next, i++)
//     {
//       type = gtk_builder_get_type_from_name (data->builder, l->data);
//       if (type == G_TYPE_INVALID)
//       {
//         g_warning ("Unknown type %s specified in treemodel %s",
//                    (const gchar*)l->data,
//                     gtk_buildable_get_name (GTK_BUILDABLE (data->object)));
//         continue;
//       }
//       types[i] = type;
// 
//       g_free (l->data);
//     }
// 
//     gtk_tree_store_set_column_types (GTK_TREE_STORE (data->object), i, types);
// 
//     g_free (types);
//   }
}

static const GMarkupParser unibar_parser =
{
  unibar_start_element,
  unibar_end_element
};

void
gtk_unibar_buildable_init (GtkBuildableIface *iface)
{
  iface->custom_tag_start = gtk_unibar_buildable_custom_tag_start;
  iface->custom_finished = gtk_unibar_buildable_custom_finished;
}

static gboolean
gtk_unibar_buildable_custom_tag_start (GtkBuildable  *buildable,
                                      GtkBuilder    *builder,
                                      GObject       *child,
                                      const gchar   *tagname,
                                      GMarkupParser *parser,
                                      gpointer      *data)
{
  GSListSubParserData *parser_data;

  if (child)
    return FALSE;

  if (strcmp (tagname, "columns") == 0)
  {
    parser_data = g_slice_new0 (GSListSubParserData);
    parser_data->builder = builder;
    parser_data->items = NULL;
    parser_data->object = G_OBJECT (buildable);

    *parser = unibar_parser;
    *data = parser_data;
    return TRUE;
  }

  return FALSE;
}

static void
gtk_unibar_buildable_custom_finished (GtkBuildable *buildable,
                                              GtkBuilder   *builder,
                                              GObject      *child,
                                              const gchar  *tagname,
                                              gpointer      user_data)
{
  GSListSubParserData *data;

  if (strcmp (tagname, "columns"))
    return;

  data = (GSListSubParserData*)user_data;

  g_slist_free (data->items);
  g_slice_free (GSListSubParserData, data);
}
#endif

static void
glade_unibar_editor_class_init (GladeUnibarEditorClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = glade_unibar_editor_finalize;
  widget_class->grab_focus = glade_unibar_editor_grab_focus;
}

static void
glade_unibar_editor_init (GladeUnibarEditor * self)
{
}

static void
glade_unibar_editor_load (GladeEditable * editable, GladeWidget * widget)
{
  printf("NOW glade_unibar_editor_load 1\n");

  GladeUnibarEditor *unibar_editor = GLADE_UNIBAR_EDITOR (editable);

  GList *l;
  
  if (unibar_editor->embed)
    glade_editable_load (GLADE_EDITABLE (unibar_editor->embed), widget);

  printf("NOW glade_unibar_editor_load 2.2\n");
  for (l = unibar_editor->properties; l; l = l->next)
    glade_editor_property_load_by_widget (GLADE_EDITOR_PROPERTY (l->data),
                                          widget);

#if 0
  printf("NOW glade_unibar_editor_load 3\n");
  if (widget)
    {
//       glade_widget_property_get (widget, "use-unibar-buffer", &use_buffer);
//       if (use_buffer)
//         gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
//                                       (unibar_editor->buffer_radio), TRUE);
//       else
//         gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
//                                       (unibar_editor->text_radio), TRUE);

      printf("NOW glade_unibar_editor_load 4\n");
//       glade_widget_property_get (widget, "primary-icon-mode", &icon_mode);
//       switch (icon_mode)
//         {
//           case GLADE_IMAGE_MODE_STOCK:
//             gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
//                                           (unibar_editor->primary_stock_radio),
//                                           TRUE);
//             break;
//           case GLADE_IMAGE_MODE_ICON:
//             gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
//                                           (unibar_editor->
//                                            primary_icon_name_radio), TRUE);
//             break;
//           case GLADE_IMAGE_MODE_FILENAME:
//             gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
//                                           (unibar_editor->primary_pixbuf_radio),
//                                           TRUE);
//             break;
//           default:
//             break;
//         }

//       glade_widget_property_get (widget, "secondary-icon-mode", &icon_mode);
//       switch (icon_mode)
//         {
//           case GLADE_IMAGE_MODE_STOCK:
//             gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
//                                           (unibar_editor->secondary_stock_radio),
//                                           TRUE);
//             break;
//           case GLADE_IMAGE_MODE_ICON:
//             gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
//                                           (unibar_editor->
//                                            secondary_icon_name_radio), TRUE);
//             break;
//           case GLADE_IMAGE_MODE_FILENAME:
//             gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
//                                           (unibar_editor->
//                                            secondary_pixbuf_radio), TRUE);
//             break;
//           default:
//             break;
//         }
    }
#endif
}

static void
glade_unibar_editor_set_show_name (GladeEditable * editable, gboolean show_name)
{
  GladeUnibarEditor *unibar_editor = GLADE_UNIBAR_EDITOR (editable);

  glade_editable_set_show_name (GLADE_EDITABLE (unibar_editor->embed),
                                show_name);
}

static void
glade_unibar_editor_editable_init (GladeEditableIface * iface)
{
  printf("HERE glade_unibar_editor_editable_init is 1\n");
//   parent_editable_iface = (GladeEditableIface *)g_type_default_interface_peek (GLADE_TYPE_EDITABLE);

  iface->load = glade_unibar_editor_load;
  iface->set_show_name = glade_unibar_editor_set_show_name;
}

static void
glade_unibar_editor_finalize (GObject * object)
{
  GladeUnibarEditor *unibar_editor = GLADE_UNIBAR_EDITOR (object);

  if (unibar_editor->properties)
    g_list_free (unibar_editor->properties);
  unibar_editor->properties = NULL;
  unibar_editor->embed = NULL;

  glade_editable_load (GLADE_EDITABLE (object), NULL);

  G_OBJECT_CLASS (glade_unibar_editor_parent_class)->finalize (object);
}

static void
glade_unibar_editor_grab_focus (GtkWidget * widget)
{
  GladeUnibarEditor *unibar_editor = GLADE_UNIBAR_EDITOR (widget);

  gtk_widget_grab_focus (unibar_editor->embed);
}

#if 0
static void
text_toggled (GtkWidget * widget, GladeUnibarEditor * unibar_editor)
{
  printf("HERE text_toggled\n");
  GladeProperty *property;
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
  GladeWidget   *gwidget = (GladeWidget *)g_object_get_qdata (G_OBJECT (unibar_editor),NULL);

//   if (glade_editable_loading (GLADE_EDITABLE (unibar_editor)) || !gwidget)
  if ( GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (unibar_editor), NULL)) || !gwidget)
    return;

//   if (!gtk_toggle_button_get_active
//       (GTK_TOGGLE_BUTTON (unibar_editor->text_radio)))
    return;

//   glade_editable_block (GLADE_EDITABLE (unibar_editor));
// 
//   glade_command_push_group (_("Setting %s to use static text"),
//                             glade_widget_get_name (gwidget));
// 
//   property = glade_widget_get_property (gwidget, "buffer");
//   glade_command_set_property (property, NULL);
// 
//   property =
//       glade_widget_get_property (gwidget,
//                                  "use-unibar-buffer");
//   glade_command_set_property (property, FALSE);
// 
//   /* Text will only take effect after setting the property under the hood */
//   property = glade_widget_get_property (gwidget, "text");
//   glade_command_set_property (property, NULL);
// 
//   /* Incase the NULL text didnt change */
//   glade_property_sync (property);
// 
//   glade_command_pop_group ();
// 
//   glade_editable_unblock (GLADE_EDITABLE (unibar_editor));

  /* reload buttons and sensitivity and stuff... */
  glade_editable_load (GLADE_EDITABLE (unibar_editor), gwidget);
}

static void
buffer_toggled (GtkWidget * widget, GladeUnibarEditor * unibar_editor)
{
  GladeProperty *property;
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
// 
//   if (glade_editable_loading (GLADE_EDITABLE (unibar_editor)) || !gwidget)
//     return;
  GladeWidget   *gwidget = (GladeWidget *)g_object_get_qdata (G_OBJECT (unibar_editor),NULL);

//   if (glade_editable_loading (GLADE_EDITABLE (unibar_editor)) || !gwidget)
  if ( GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (unibar_editor), NULL)) || !gwidget)
    return;

//   if (!gtk_toggle_button_get_active
//       (GTK_TOGGLE_BUTTON (unibar_editor->buffer_radio)))
    return;

//   glade_editable_block (GLADE_EDITABLE (unibar_editor));
// 
//   glade_command_push_group (_("Setting %s to use an external buffer"),
//                             glade_widget_get_name (gwidget));
// 
//   /* Reset the text while still in static text mode */
//   property = glade_widget_get_property (gwidget, "text");
//   glade_command_set_property (property, NULL);
// 
//   property =
//       glade_widget_get_property (gwidget, "use-unibar-buffer");
//   glade_command_set_property (property, TRUE);
// 
//   glade_command_pop_group ();
// 
//   glade_editable_unblock (GLADE_EDITABLE (unibar_editor));

  /* reload buttons and sensitivity and stuff... */
  glade_editable_load (GLADE_EDITABLE (unibar_editor), gwidget);
}


// #define ICON_MODE_NAME(primary)   ((primary) ? "primary-icon-mode"    : "secondary-icon-mode")
// #define PIXBUF_NAME(primary)      ((primary) ? "primary-icon-pixbuf"  : "secondary-icon-pixbuf")
// #define ICON_NAME_NAME(primary)   ((primary) ? "primary-icon-name"    : "secondary-icon-name")
// #define STOCK_NAME(primary)       ((primary) ? "primary-icon-stock"   : "secondary-icon-stock")

static void
set_stock_mode (GladeUnibarEditor * unibar_editor, gboolean primary)
{
//   GladeProperty *property;
// //   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
//   GladeWidget   *gwidget = (GladeWidget *)g_object_get_qdata (G_OBJECT (unibar_editor),NULL);

//   GValue value = { 0, };

//   property = glade_widget_get_property (gwidget, ICON_NAME_NAME (primary));
//   glade_command_set_property (property, NULL);
//   property = glade_widget_get_property (gwidget, PIXBUF_NAME (primary));
//   glade_command_set_property (property, NULL);

//   property = glade_widget_get_property (gwidget, STOCK_NAME (primary));
//   glade_property_get_default (property, &value);
//   glade_command_set_property_value (property, &value);
//   g_value_unset (&value);

//   property = glade_widget_get_property (gwidget, ICON_MODE_NAME (primary));
//   glade_command_set_property (property, GLADE_IMAGE_MODE_STOCK);
}

static void
set_icon_name_mode (GladeUnibarEditor * unibar_editor, gboolean primary)
{
//   GladeProperty *property;
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
// 
//   property = glade_widget_get_property (gwidget, STOCK_NAME (primary));
//   glade_command_set_property (property, NULL);
//   property = glade_widget_get_property (gwidget, PIXBUF_NAME (primary));
//   glade_command_set_property (property, NULL);
//   property = glade_widget_get_property (gwidget, ICON_MODE_NAME (primary));
//   glade_command_set_property (property, GLADE_IMAGE_MODE_ICON);
}

static void
set_pixbuf_mode (GladeUnibarEditor * unibar_editor, gboolean primary)
{
//   GladeProperty *property;
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
// 
//   property = glade_widget_get_property (gwidget, STOCK_NAME (primary));
//   glade_command_set_property (property, NULL);
// 
//   property = glade_widget_get_property (gwidget, ICON_NAME_NAME (primary));
//   glade_command_set_property (property, NULL);
// 
//   property = glade_widget_get_property (gwidget, ICON_MODE_NAME (primary));
//   glade_command_set_property (property, GLADE_IMAGE_MODE_FILENAME);
}

static void
primary_stock_toggled (GtkWidget * widget, GladeUnibarEditor * unibar_editor)
{
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
// 
//   if (glade_editable_loading (GLADE_EDITABLE (unibar_editor)) || !gwidget)
//     return;
// 
//   if (!gtk_toggle_button_get_active
//       (GTK_TOGGLE_BUTTON (unibar_editor->primary_stock_radio)))
//     return;
// 
//   glade_editable_block (GLADE_EDITABLE (unibar_editor));
// 
//   glade_command_push_group (_("Setting %s to use a primary icon from stock"),
//                             glade_widget_get_name (gwidget));
//   set_stock_mode (unibar_editor, TRUE);
//   glade_command_pop_group ();
// 
//   glade_editable_unblock (GLADE_EDITABLE (unibar_editor));
// 
//   /* reload buttons and sensitivity and stuff... */
//   glade_editable_load (GLADE_EDITABLE (unibar_editor), gwidget);
}


static void
primary_icon_name_toggled (GtkWidget * widget, GladeUnibarEditor * unibar_editor)
{
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
// 
//   if (glade_editable_loading (GLADE_EDITABLE (unibar_editor)) || !gwidget)
//     return;
// 
//   if (!gtk_toggle_button_get_active
//       (GTK_TOGGLE_BUTTON (unibar_editor->primary_icon_name_radio)))
//     return;
// 
//   glade_editable_block (GLADE_EDITABLE (unibar_editor));
// 
//   glade_command_push_group (_("Setting %s to use a primary icon from the icon theme"),
//                             glade_widget_get_name (gwidget));
//   set_icon_name_mode (unibar_editor, TRUE);
//   glade_command_pop_group ();
// 
//   glade_editable_unblock (GLADE_EDITABLE (unibar_editor));
// 
//   /* reload buttons and sensitivity and stuff... */
//   glade_editable_load (GLADE_EDITABLE (unibar_editor), gwidget);
}

static void
primary_pixbuf_toggled (GtkWidget * widget, GladeUnibarEditor * unibar_editor)
{
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
// 
//   if (glade_editable_loading (GLADE_EDITABLE (unibar_editor)) || !gwidget)
//     return;
// 
//   if (!gtk_toggle_button_get_active
//       (GTK_TOGGLE_BUTTON (unibar_editor->primary_pixbuf_radio)))
//     return;
// 
//   glade_editable_block (GLADE_EDITABLE (unibar_editor));
// 
//   glade_command_push_group (_("Setting %s to use a primary icon from filename"),
//                             glade_widget_get_name (gwidget));
//   set_pixbuf_mode (unibar_editor, TRUE);
//   glade_command_pop_group ();
// 
//   glade_editable_unblock (GLADE_EDITABLE (unibar_editor));
// 
//   /* reload buttons and sensitivity and stuff... */
//   glade_editable_load (GLADE_EDITABLE (unibar_editor), gwidget);
}


static void
secondary_stock_toggled (GtkWidget * widget, GladeUnibarEditor * unibar_editor)
{
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
// 
//   if (glade_editable_loading (GLADE_EDITABLE (unibar_editor)) || !gwidget)
//     return;
// 
//   if (!gtk_toggle_button_get_active
//       (GTK_TOGGLE_BUTTON (unibar_editor->secondary_stock_radio)))
//     return;
// 
//   glade_editable_block (GLADE_EDITABLE (unibar_editor));
// 
//   glade_command_push_group (_("Setting %s to use a secondary icon from stock"),
//                             glade_widget_get_name (gwidget));
//   set_stock_mode (unibar_editor, FALSE);
//   glade_command_pop_group ();
// 
//   glade_editable_unblock (GLADE_EDITABLE (unibar_editor));
// 
//   /* reload buttons and sensitivity and stuff... */
//   glade_editable_load (GLADE_EDITABLE (unibar_editor), gwidget);
}


static void
secondary_icon_name_toggled (GtkWidget * widget,
                             GladeUnibarEditor * unibar_editor)
{
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
// 
//   if (glade_editable_loading (GLADE_EDITABLE (unibar_editor)) || !gwidget)
//     return;
// 
//   if (!gtk_toggle_button_get_active
//       (GTK_TOGGLE_BUTTON (unibar_editor->secondary_icon_name_radio)))
//     return;
// 
//   glade_editable_block (GLADE_EDITABLE (unibar_editor));
// 
//   glade_command_push_group (_("Setting %s to use a secondary icon from the icon theme"),
//                             glade_widget_get_name (gwidget));
//   set_icon_name_mode (unibar_editor, FALSE);
//   glade_command_pop_group ();
// 
//   glade_editable_unblock (GLADE_EDITABLE (unibar_editor));
// 
//   /* reload buttons and sensitivity and stuff... */
//   glade_editable_load (GLADE_EDITABLE (unibar_editor), gwidget);
}

static void
secondary_pixbuf_toggled (GtkWidget * widget, GladeUnibarEditor * unibar_editor)
{
//   GladeWidget   *gwidget = glade_editable_loaded_widget (GLADE_EDITABLE (unibar_editor));
// 
//   if (glade_editable_loading (GLADE_EDITABLE (unibar_editor)) || !gwidget)
//     return;
// 
//   if (!gtk_toggle_button_get_active
//       (GTK_TOGGLE_BUTTON (unibar_editor->secondary_pixbuf_radio)))
//     return;
// 
//   glade_editable_block (GLADE_EDITABLE (unibar_editor));
// 
//   glade_command_push_group (_("Setting %s to use a secondary icon from filename"),
//                             glade_widget_get_name (gwidget));
//   set_pixbuf_mode (unibar_editor, FALSE);
//   glade_command_pop_group ();
// 
//   glade_editable_unblock (GLADE_EDITABLE (unibar_editor));
// 
//   /* reload buttons and sensitivity and stuff... */
//   glade_editable_load (GLADE_EDITABLE (unibar_editor), gwidget);
}
#endif

static void
table_attach (GtkWidget * table, GtkWidget * child, gint pos, gint row)
{
//   gtk_grid_attach (GTK_GRID (table), child, pos, row, 1, 1);
// 
//   if (pos)
//     gtk_widget_set_hexpand (child, TRUE);
}

GtkWidget *
glade_unibar_editor_new (GladeWidgetAdaptor * adaptor, GladeEditable * embed)
{
  printf("NOW glade_unibar_editor_new 1\n");
  GladeUnibarEditor *unibar_editor;
  GladeEditorProperty *eprop;
//   GtkWidget *table, *frame, *alignment, *label, *hbox;
//   gchar *str;
  printf("NOW glade_unibar_editor_new 2\n");
  g_return_val_if_fail (GLADE_IS_WIDGET_ADAPTOR (adaptor), NULL);
  g_return_val_if_fail (GLADE_IS_EDITABLE (embed), NULL);

  unibar_editor = (GladeUnibarEditor *)g_object_new (GLADE_TYPE_UNIBAR_EDITOR, NULL);
  unibar_editor->embed = GTK_WIDGET (embed);
  printf("NOW glade_unibar_editor_new 3\n");
  /* Pack the parent on top... */
  gtk_box_pack_start (GTK_BOX (unibar_editor), GTK_WIDGET (embed), FALSE, FALSE,
                      0);


  /* Text... */
//   str = g_strdup_printf ("<b>%s</b>", _("Some Text here is!"));
//   label = gtk_label_new (str);
//   gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
//   g_free (str);
  
//   frame = gtk_frame_new (NULL);
//   gtk_frame_set_label_widget (GTK_FRAME (frame), label);
//   gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
//   gtk_box_pack_start (GTK_BOX (unibar_editor), frame, FALSE, FALSE, 8);

//   alignment = gtk_alignment_new (0.5F, 0.5F, 1.0F, 1.0F);
//   gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 6, 0, 12, 0);
//   gtk_container_add (GTK_CONTAINER (frame), alignment);
  printf("NOW glade_unibar_editor_new 4\n");
//   table = gtk_grid_new ();
//   gtk_orientable_set_orientation (GTK_ORIENTABLE (table),
//                                   GTK_ORIENTATION_VERTICAL);
//   gtk_grid_set_row_spacing (GTK_GRID (table), 4);
//   gtk_container_add (GTK_CONTAINER (alignment), table);

  /* Color */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor, "color", FALSE, TRUE);
//   hbox = gtk_hbox_new (GTK_ORIENTATION_HORIZONTAL,0);
//   unibar_editor->text_radio = gtk_radio_button_new (NULL);
//   gtk_box_pack_start (GTK_BOX (hbox), unibar_editor->text_radio, FALSE, FALSE,
//                       2);
//   gtk_box_pack_start (GTK_BOX (hbox), eprop->item_label, TRUE, TRUE, 2);
//   table_attach (table, hbox, 0, 0);
//   table_attach (table, GTK_WIDGET (eprop), 1, 0);
//   unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);
  printf("NOW glade_unibar_editor_new 4.1\n");
#if 0
  /* Buffer */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor, "buffer", FALSE,
                                                 TRUE);
  hbox = gtk_hbox_new (GTK_ORIENTATION_HORIZONTAL, 0);
  unibar_editor->buffer_radio = gtk_radio_button_new_from_widget
      (GTK_RADIO_BUTTON (unibar_editor->text_radio));
  gtk_box_pack_start (GTK_BOX (hbox), unibar_editor->buffer_radio, FALSE, FALSE,
                      2);
  gtk_box_pack_start (GTK_BOX (hbox), eprop->item_label, TRUE, TRUE, 2);
  table_attach (table, hbox, 0, 1);
  table_attach (table, GTK_WIDGET (eprop), 1, 1);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  
  printf("NOW glade_unibar_editor_new 5\n");
  /* Progress... */
  str = g_strdup_printf ("<b>%s</b>", _("Progress"));
  label = gtk_label_new (str);
  gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
  g_free (str);
  frame = gtk_frame_new (NULL);
  gtk_frame_set_label_widget (GTK_FRAME (frame), label);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
  gtk_box_pack_start (GTK_BOX (unibar_editor), frame, FALSE, FALSE, 8);

  alignment = gtk_alignment_new (0.5F, 0.5F, 1.0F, 1.0F);
  gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 6, 0, 12, 0);
  gtk_container_add (GTK_CONTAINER (frame), alignment);

//   table = gtk_grid_new ();
//   gtk_orientable_set_orientation (GTK_ORIENTABLE (table),
//                                   GTK_ORIENTATION_VERTICAL);
//   gtk_grid_set_row_spacing (GTK_GRID (table), 4);
//   gtk_container_add (GTK_CONTAINER (alignment), table);

  /* Fraction */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor, "progress-fraction",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 0);
//   table_attach (table, GTK_WIDGET (eprop), 1, 0);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);
  printf("NOW glade_unibar_editor_new 6\n");
  /* Pulse */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor, "progress-pulse-step",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 1);
//   table_attach (table, GTK_WIDGET (eprop), 1, 1);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  /* Primary icon... */
  str = g_strdup_printf ("<b>%s</b>", _("Primary icon"));
  label = gtk_label_new (str);
  gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
  g_free (str);
  frame = gtk_frame_new (NULL);
  gtk_frame_set_label_widget (GTK_FRAME (frame), label);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
  gtk_box_pack_start (GTK_BOX (unibar_editor), frame, FALSE, FALSE, 8);

  alignment = gtk_alignment_new (0.5F, 0.5F, 1.0F, 1.0F);
  gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 6, 0, 12, 0);
  gtk_container_add (GTK_CONTAINER (frame), alignment);

//   table = gtk_grid_new ();
//   gtk_orientable_set_orientation (GTK_ORIENTABLE (table),
//                                   GTK_ORIENTATION_VERTICAL);
//   gtk_grid_set_row_spacing (GTK_GRID (table), 4);
//   gtk_container_add (GTK_CONTAINER (alignment), table);

  /* Pixbuf */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor, PIXBUF_NAME (TRUE),
                                                 FALSE, TRUE);
  hbox = gtk_hbox_new (GTK_ORIENTATION_HORIZONTAL, 0);
  unibar_editor->primary_pixbuf_radio = gtk_radio_button_new (NULL);
  gtk_box_pack_start (GTK_BOX (hbox), unibar_editor->primary_pixbuf_radio, FALSE,
                      FALSE, 2);
  gtk_box_pack_start (GTK_BOX (hbox), eprop->item_label, TRUE, TRUE, 2);
  table_attach (table, hbox, 0, 0);
  table_attach (table, GTK_WIDGET (eprop), 1, 0);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  /* Stock */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor, STOCK_NAME (TRUE),
                                                 FALSE, TRUE);
  hbox = gtk_hbox_new (GTK_ORIENTATION_HORIZONTAL, 0);
  unibar_editor->primary_stock_radio = gtk_radio_button_new_from_widget
      (GTK_RADIO_BUTTON (unibar_editor->primary_pixbuf_radio));
  gtk_box_pack_start (GTK_BOX (hbox), unibar_editor->primary_stock_radio, FALSE,
                      FALSE, 2);
  gtk_box_pack_start (GTK_BOX (hbox), eprop->item_label, TRUE, TRUE, 2);
  table_attach (table, hbox, 0, 1);
  table_attach (table, GTK_WIDGET (eprop), 1, 1);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  /* Icon name */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor, ICON_NAME_NAME (TRUE),
                                                 FALSE, TRUE);
  hbox = gtk_hbox_new (GTK_ORIENTATION_HORIZONTAL, 0);
  unibar_editor->primary_icon_name_radio = gtk_radio_button_new_from_widget
      (GTK_RADIO_BUTTON (unibar_editor->primary_pixbuf_radio));
  gtk_box_pack_start (GTK_BOX (hbox), unibar_editor->primary_icon_name_radio,
                      FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (hbox), eprop->item_label, TRUE, TRUE, 2);
  table_attach (table, hbox, 0, 2);
  table_attach (table, GTK_WIDGET (eprop), 1, 2);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  /* Other primary icon related properties */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor,
                                                 "primary-icon-activatable",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 3);
//   table_attach (table, GTK_WIDGET (eprop), 1, 3);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor,
                                                 "primary-icon-sensitive",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 4);
//   table_attach (table, GTK_WIDGET (eprop), 1, 4);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor,
                                                 "primary-icon-tooltip-text",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 5);
//   table_attach (table, GTK_WIDGET (eprop), 1, 5);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor,
                                                 "primary-icon-tooltip-markup",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 6);
//   table_attach (table, GTK_WIDGET (eprop), 1, 6);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  /* Secondary icon... */
  str = g_strdup_printf ("<b>%s</b>", _("Secondary icon"));
  label = gtk_label_new (str);
  gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
  g_free (str);
  frame = gtk_frame_new (NULL);
  gtk_frame_set_label_widget (GTK_FRAME (frame), label);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
  gtk_box_pack_start (GTK_BOX (unibar_editor), frame, FALSE, FALSE, 8);

  alignment = gtk_alignment_new (0.5F, 0.5F, 1.0F, 1.0F);
  gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 6, 0, 12, 0);
  gtk_container_add (GTK_CONTAINER (frame), alignment);

//   table = gtk_grid_new ();
//   gtk_orientable_set_orientation (GTK_ORIENTABLE (table),
//                                   GTK_ORIENTATION_VERTICAL);
//   gtk_grid_set_row_spacing (GTK_GRID (table), 4);
//   gtk_container_add (GTK_CONTAINER (alignment), table);

  /* Pixbuf */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor, PIXBUF_NAME (FALSE),
                                                 FALSE, TRUE);
  hbox = gtk_hbox_new (GTK_ORIENTATION_HORIZONTAL, 0);
  unibar_editor->secondary_pixbuf_radio = gtk_radio_button_new (NULL);
  gtk_box_pack_start (GTK_BOX (hbox), unibar_editor->secondary_pixbuf_radio,
                      FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (hbox), eprop->item_label, TRUE, TRUE, 2);
  table_attach (table, hbox, 0, 0);
  table_attach (table, GTK_WIDGET (eprop), 1, 0);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  /* Stock */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor, STOCK_NAME (FALSE),
                                                 FALSE, TRUE);
  hbox = gtk_hbox_new (GTK_ORIENTATION_HORIZONTAL, 0);
  unibar_editor->secondary_stock_radio = gtk_radio_button_new_from_widget
      (GTK_RADIO_BUTTON (unibar_editor->secondary_pixbuf_radio));
  gtk_box_pack_start (GTK_BOX (hbox), unibar_editor->secondary_stock_radio,
                      FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (hbox), eprop->item_label, TRUE, TRUE, 2);
  table_attach (table, hbox, 0, 1);
  table_attach (table, GTK_WIDGET (eprop), 1, 1);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  /* Icon name */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor,
                                                 ICON_NAME_NAME (FALSE), FALSE,
                                                 TRUE);
  hbox = gtk_hbox_new (GTK_ORIENTATION_HORIZONTAL, 0);
  unibar_editor->secondary_icon_name_radio = gtk_radio_button_new_from_widget
      (GTK_RADIO_BUTTON (unibar_editor->secondary_pixbuf_radio));
  gtk_box_pack_start (GTK_BOX (hbox), unibar_editor->secondary_icon_name_radio,
                      FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (hbox), eprop->item_label, TRUE, TRUE, 2);
  table_attach (table, hbox, 0, 2);
  table_attach (table, GTK_WIDGET (eprop), 1, 2);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  /* Other secondary icon related properties */
  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor,
                                                 "secondary-icon-activatable",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 3);
//   table_attach (table, GTK_WIDGET (eprop), 1, 3);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor,
                                                 "secondary-icon-sensitive",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 4);
//   table_attach (table, GTK_WIDGET (eprop), 1, 4);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor,
                                                 "secondary-icon-tooltip-text",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 5);
//   table_attach (table, GTK_WIDGET (eprop), 1, 5);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);

  eprop =
      glade_widget_adaptor_create_eprop_by_name (adaptor,
                                                 "secondary-icon-tooltip-markup",
                                                 FALSE, TRUE);
//   table_attach (table, glade_editor_property_get_item_label (eprop), 0, 6);
//   table_attach (table, GTK_WIDGET (eprop), 1, 6);
  unibar_editor->properties = g_list_prepend (unibar_editor->properties, eprop);
#endif
  gtk_widget_show_all (GTK_WIDGET (unibar_editor));


  /* Connect radio button signals... */
//   g_signal_connect (G_OBJECT (unibar_editor->text_radio), "toggled",
//                     G_CALLBACK (text_toggled), unibar_editor);
#if 0
  g_signal_connect (G_OBJECT (unibar_editor->buffer_radio), "toggled",
                    G_CALLBACK (buffer_toggled), unibar_editor);

  g_signal_connect (G_OBJECT (unibar_editor->primary_stock_radio), "toggled",
                    G_CALLBACK (primary_stock_toggled), unibar_editor);
  g_signal_connect (G_OBJECT (unibar_editor->primary_icon_name_radio), "toggled",
                    G_CALLBACK (primary_icon_name_toggled), unibar_editor);
  g_signal_connect (G_OBJECT (unibar_editor->primary_pixbuf_radio), "toggled",
                    G_CALLBACK (primary_pixbuf_toggled), unibar_editor);

  g_signal_connect (G_OBJECT (unibar_editor->secondary_stock_radio), "toggled",
                    G_CALLBACK (secondary_stock_toggled), unibar_editor);
  g_signal_connect (G_OBJECT (unibar_editor->secondary_icon_name_radio),
                    "toggled", G_CALLBACK (secondary_icon_name_toggled),
                    unibar_editor);
  g_signal_connect (G_OBJECT (unibar_editor->secondary_pixbuf_radio), "toggled",
                    G_CALLBACK (secondary_pixbuf_toggled), unibar_editor);
#endif
  printf("NOW glade_unibar_editor_new 10\n");
  return GTK_WIDGET (unibar_editor);
}
#ifdef __cplusplus
  }
#endif
  
