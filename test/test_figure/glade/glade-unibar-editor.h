#ifdef __cplusplus
  extern "C" {
#endif
  
#ifndef _GLADE_UNIBAR_EDITOR_H_
#define _GLADE_UNIBAR_EDITOR_H_

#include <gtk/gtk.h>
#include <gladeui/glade.h>

G_BEGIN_DECLS

#define GLADE_TYPE_UNIBAR_EDITOR	            (glade_unibar_editor_get_type ())
#define GLADE_UNIBAR_EDITOR(obj)		        (G_TYPE_CHECK_INSTANCE_CAST ((obj), GLADE_TYPE_UNIBAR_EDITOR, GladeUnibarEditor))
#define GLADE_unibar_EDITOR_CLASS(klass)	    (G_TYPE_CHECK_CLASS_CAST ((klass), GLADE_TYPE_unibar_EDITOR, GladeUnibarEditorClass))
#define GLADE_IS_unibar_EDITOR(obj)	            (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GLADE_TYPE_unibar_EDITOR))
#define GLADE_IS_unibar_EDITOR_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GLADE_TYPE_unibar_EDITOR))
#define GLADE_unibar_EDITOR_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GLADE_TYPE_unibar_EDITOR, GladeUnibarEditorClass))

typedef struct _GladeUnibarEditor        GladeUnibarEditor;
typedef struct _GladeUnibarEditorClass   GladeUnibarEditorClass;

struct _GladeUnibarEditor
{
	GtkVBox  parent;

	GtkWidget *embed;

	/*GtkWidget *text_radio;
	GtkWidget *buffer_radio;

	GtkWidget *primary_pixbuf_radio;
	GtkWidget *primary_stock_radio;
	GtkWidget *primary_icon_name_radio;

	GtkWidget *secondary_pixbuf_radio;
	GtkWidget *secondary_stock_radio;
	GtkWidget *secondary_icon_name_radio;*/

	GList     *properties;
};

struct _GladeUnibarEditorClass
{
	GtkVBoxClass parent;
};

GType            glade_unibar_editor_get_type (void) G_GNUC_CONST;

GtkWidget       *glade_unibar_editor_new(GladeWidgetAdaptor *adaptor, GladeEditable      *editable);

G_END_DECLS

#endif  /* _GLADE_unibar_EDITOR_H_ */
#ifdef __cplusplus
  }
#endif
