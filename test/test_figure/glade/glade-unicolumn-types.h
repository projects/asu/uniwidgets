#ifndef _GLADE_UNICOLUMN_TYPES_H_
#define _STV_CAP_H_

#include <glib.h>

G_BEGIN_DECLS

typedef struct
{
	GType type;
	gchar *column_name;
} GladeUniColumnType;

#define	GLADE_TYPE_UNICOLUMN_TYPE_LIST   (glade_uni_column_type_list_get_type())
#define GLADE_TYPE_EPROP_UNICOLUMN_TYPES (glade_eprop_unicolumn_types_get_type())

GType        glade_uni_column_type_list_get_type      (void) G_GNUC_CONST;
GType        glade_eprop_unicolumn_types_get_type    (void) G_GNUC_CONST;

void         glade_unicolumn_list_free               (GList *list);
GList       *glade_unicolumn_list_copy               (GList *list);

void             glade_unicolumn_type_free           (GladeUniColumnType *column);
GladeUniColumnType *glade_unicolumn_list_find_column    (GList *list, const gchar *column_name);

G_END_DECLS

#endif /* _GLADE_COLUMN_TYPES_H_ */
