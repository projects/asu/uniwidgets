#ifndef _GLADE_UNIMODEL_DATA_UNIBAR_H_
#define _STV_CAP_H_

#include <glib.h>

G_BEGIN_DECLS

struct _GladeUniModelData
{
	GValue    value;
	gchar    *name;
	
//	gboolean  i18n_translatable;
//	gchar    *i18n_context;
//	gchar    *i18n_comment;
};

typedef struct _GladeUniModelData GladeUniModelData;


#define	GLADE_TYPE_UNIMODEL_DATA_UNIBAR  (glade_uni_model_data_uni_bar_get_type())
#define GLADE_TYPE_EPROP_UNIMODEL_DATA (glade_eprop_unimodel_data_get_type())

GType           glade_uni_model_data_uni_bar_get_type     (void) G_GNUC_CONST;
GType           glade_eprop_unimodel_data_get_type    (void) G_GNUC_CONST;


GladeUniModelData *glade_unimodel_data_new              (GType           type,
						         const gchar    *column_name);
GladeUniModelData *glade_unimodel_data_copy             (GladeUniModelData *data);
void            glade_unimodel_data_free                (GladeUniModelData *data);

GNode          *glade_unimodel_data_unibar_copy         (GNode          *node);
void            glade_unimodel_data_unibar_free         (GNode          *node);

GladeUniModelData *glade_unimodel_data_unibar_get_data     (GNode          *data_tree, 
						            gint            row, 
						            gint            colnum);
#if 0
void            glade_unimodel_data_insert_column     (GNode          *node,
						       GType           type,
						       const gchar    *column_name,
						       gint            nth);
void            glade_unimodel_data_remove_column     (GNode          *node,
						       gint            nth);
void            glade_unimodel_data_reorder_column    (GNode          *node,
						       gint            column,
						       gint            nth);
gint            glade_unimodel_data_column_index      (GNode          *node,
						       const gchar    *column_name);
void            glade_unimodel_data_column_rename     (GNode          *node,
						       const gchar    *column_name,
						       const gchar    *new_name);
#endif

G_END_DECLS

#endif /* _GLADE_UNIMODEL_DATA_H_ */
