//#include "functions.h"
#include <glade/glade-init.h>
#include <glade/glade-build.h>
#include "bar.h"
extern "C" void glade_module_register_widgets()
{
glade_register_widget   (gtk_bar_get_type(),
                         glade_standard_build_widget,
                         NULL, NULL);
}
