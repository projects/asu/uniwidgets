#include <iostream>
#include <gtkmm.h>
#include <libglademm.h>
#include <gdk/gdkx.h>
#include <cairomm/context.h>
#include <gtkmm/drawingarea.h>

using namespace std;

static gboolean alpha_channel_support = false;

class Sensor : public Gtk::EventBox
{
public:
  Sensor();
  virtual ~Sensor();

protected:
  //Override default signal handler:
  virtual bool on_expose_event(GdkEventExpose* event);
  int state;
  bool on_timeout();
std::map<int, std::vector<int > >_states;

  double m_radius;
  double m_line_width;

};

Sensor::Sensor()
: m_radius(0.42), m_line_width(0.05),state(1)
{
  Glib::signal_timeout().connect( sigc::mem_fun(*this, &Sensor::on_timeout), 250 );
//red 1.0, 0.0, 0.0, 1.0
std::vector<int >state1;
state1.push_back(255);
state1.push_back(0);
state1.push_back(0);
state1.push_back(255);
//yellow 1.0, 0.8, 0.0, 1.0
std::vector<int >state2;
state2.push_back(255);
state2.push_back(208);
state2.push_back(0);
state2.push_back(255);
//dark 0.1, 0.1, 0.1, 1.0
std::vector<int >state3;
state3.push_back(30);
state3.push_back(30);
state3.push_back(30);
state3.push_back(255);
pair<std::map<int, std::vector<int > >::iterator, bool> result;

result = _states.insert(pair<int, std::vector<int > >(1, state1));
// assert(result.second == true);

result = _states.insert(pair<int, std::vector<int > >(2, state2));
// assert(result.second == true);

result = _states.insert(pair<int, std::vector<int > >(3, state3));
// assert(result.second == true);

  #ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  //Connect the signal handler if it isn't already a virtual method override:
  signal_expose_event().connect(sigc::mem_fun(*this, &Sensor::on_expose_event), false);
  #endif //GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
}

Sensor::~Sensor()
{
}

bool Sensor::on_expose_event(GdkEventExpose* event)
{
	// This is where we draw on the window
	Glib::RefPtr<Gdk::Window> window = get_window();
	if(window)
	{
	Gtk::Allocation allocation = get_allocation();
	const int width = allocation.get_width();
	const int height = allocation.get_height();
	
	int x0,y0;
	x0=width/2;
	y0=height/2;
	
	// coordinates for the center of the window
	int xc, yc;
	xc = 50;
	yc = 50;
	
	Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();
	cr->set_line_width(0);  // outline thickness changes
						// with window size
	
	// clip to the area indicated by the expose event so that we only redraw
	// the portion of the window that needs to be redrawn
	cr->rectangle(event->area.x, event->area.y,
		event->area.width, event->area.height);
	cr->clip();
	
	// now draw a circle
	cr->save();
	//     cr->arc(100, 100, 200, 10, 1 ); // full circle

	cr->move_to(xc/2+x0, 0+y0);
	cr->line_to(0+x0, yc+y0);
	cr->line_to(xc+x0, yc+y0);
	cr->close_path();
	cout<<"NEW "<<_states[state][0]<<endl;
	cr->set_source_rgba(_states[state][0], _states[state][1], _states[state][2], _states[state][3]);    // partially translucent
	cr->fill_preserve();
	cr->restore();  // back to opaque black
	cr->stroke();
  }

  return true;

  }


bool Sensor::on_timeout()
{
	cout<<"on timeout"<<endl;
//     // force our program to redraw the entire clock.
//     Glib::RefPtr<Gdk::Window> win = get_window();
//     if (win)
//     {
//         Gdk::Rectangle r(0, 0, get_allocation().get_width(),
//                 get_allocation().get_height());
//         win->invalidate_rect(r, false);
//     }
	if(state == 3)
		state=1;
	else
		state++;
	queue_draw();
    return true;
}

main (int argc, char **argv)
{
	Glib::RefPtr<Gdk::Screen> screen;
	Glib::RefPtr<Gdk::Colormap> colormap;

	Gtk::Main Kit(argc, argv);
	Gtk::Window window;
	window.set_title("Example simple sensor");
	window.set_default_size(400, 400);
	Sensor c;
	window.add(c);
	c.show();
//	Box.set_visible_window(false);
//	cout<<"11"<<endl;
//	Cairo::RefPtr<Cairo::Context> cr;
//	cout<<"here"<<endl;
//	try
//	{
//	    cr = Box.get_window()->create_cairo_context();
//	}catch(...){
//	    cout<<"SOME EXCEPTION!"<<endl;
//	    return 1;
//	}
//	cout<<"1"<<endl;
//	const Gtk::Allocation alloc = Box.get_allocation();
//	cout<<"w="<<alloc.get_width()<<", h="<<alloc.get_height()<<endl;
//	Gdk::Cairo::rectangle(cr,alloc);
//	cr->clip();

//	cr->set_line_width(1);
//	Gdk::Cairo::rectangle(cr, alloc);
//	cr->stroke();

//	cr->paint();

	window.show_all();
	Kit.run(window);
}
