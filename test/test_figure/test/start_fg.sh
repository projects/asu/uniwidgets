#!/bin/sh

#export LIBGLADE_MODULE_PATH=../plugins/libglade/.libs

START=uniset-start.sh

GTK2_RC_FILES=/srv/ilyap/Projects/yauza/src/GUI1/theme/gtkrc ${START} -f ./bar --svg-path ../../svg/ --cbid 105 \
	--confile configure.xml --localNode LocalhostNode \
	--heartbeat-node localhost \
	--unideb-add-levels info,warn,crit,system --dlog-add-levels info,warn,crit
