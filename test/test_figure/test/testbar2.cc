#include <iostream>
#include <gtkmm.h>
#include "../unibar/unibar2.h"
#include "../unibar/init.h"

using namespace std;
using namespace Gtk;
using namespace UniWidgets;

static bool set_value(Gtk::ScrollType scroll, double new_value, Gtk::UniWidgets::UniBar2* _bar)
{
// 	cout<<"Changed value"<<endl;
	_bar->set_sel(new_value);
	_bar->queue_draw();
}

int main (int argc, char ** argv)
{
 	if (!g_thread_supported())
 		g_thread_init(NULL);

	Gtk::Main Kit(argc, (char**&)argv);
	Gtk::UniWidgets::init(argc, (char**&)argv);

	std::cout<<"\ntest\n"<<std::endl;
// 	Gtk::Window w(Gtk::WINDOW_TOPLEVEL);
	Gtk::Window* w;
	Glib::RefPtr<Gtk::Builder> builder;
	builder = Gtk::Builder::create();
	try
	{
		builder->add_from_file("./glade/objects.ui");
	}catch(Gtk::BuilderError& ex){
		cerr << "MW builder create: " << ex.what() << endl;
		return 1;
	}
	builder->get_widget("window1", w);
	w->set_title("BAR widget");
	w->set_position(Gtk::WIN_POS_CENTER);
// 	w->set_default_size(300, 280);
	w->set_size_request(500, 500);

//	Gtk::UniWidgets::Bar* bar;
	UniWidgets::UniBar2* bar;
	builder->get_widget("bar", bar);
	bar->property_color()="red";
	cout<<"Property color="<<bar->property_color()<<endl;
	cout<<"Property back-color="<<bar->property_back_color()<<endl;
	bar->set_size_request(200, 200);
	Gtk::Fixed* fx;
	builder->get_widget("fx", fx);
	w->add(*fx);
//	Gtk::VScrollbar vs(0.0, 101.0, 1.0);
	Gtk::VScrollbar* vs;
	builder->get_widget("vs", vs);
	vs->set_range(0.0,100.0);
	vs->set_increments(1.0,1);
	vs->set_inverted(true);
//	vs->set_value_pos(Gtk::POS_TOP);
	vs->set_size_request(50, 200);

	vs->signal_change_value().connect(sigc::bind(sigc::ptr_fun(&set_value),bar));

	fx->put(*vs,300, 20);
	fx->put(*bar,30,40);
	bar->show();
	w->show_all();

	Kit.run(*w);
	return 0;
}