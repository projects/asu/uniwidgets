#include <string.h>
#include "bar.h"

#define _(x) x

enum {
    ARG_0,
    ARG_COLOR
};

// static gpointer gtk_bar_parent_class = NULL;
// static void gtk_bar_class_init(GtkBarClass *klass);
static void gtk_bar_init(GtkBar *bar);
static void gtk_bar_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_bar_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_bar_buildable_init  (GtkBuildableIface      *iface);
static void gtk_bar_realize(GtkWidget *widget);
static gboolean gtk_bar_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_bar_paint_real(GtkBar *bar);
static void gtk_bar_destroy(GtkObject *object);
static void gtk_bar_set_arg(GtkObject *object,
        GtkArg *arg,
        guint arg_id);
static void gtk_bar_get_arg(GtkObject *object,
        GtkArg *arg,
        guint arg_id);
static void gtk_bar_paint(GtkBar *bar);
static GtkWidgetClass *parent_class = NULL;

/* buildable */
static gboolean gtk_bar_buildable_custom_tag_start (GtkBuildable  *buildable,
							   GtkBuilder    *builder,
							   GObject       *child,
							   const gchar   *tagname,
							   GMarkupParser *parser,
							   gpointer      *data);
static void     gtk_bar_buildable_custom_tag_end (GtkBuildable *buildable,
							 GtkBuilder   *builder,
							 GObject      *child,
							 const gchar  *tagname,
							 gpointer     *data);

        G_DEFINE_TYPE_WITH_CODE (GtkBar, gtk_bar, GTK_TYPE_WIDGET,
                                     G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE,
                                         gtk_bar_buildable_init))

void
gtk_bar_buildable_init (GtkBuildableIface *iface)
{
  printf("\nHEREx1\n");
  iface->custom_tag_start = gtk_bar_buildable_custom_tag_start;
  iface->custom_tag_end = gtk_bar_buildable_custom_tag_end;
}

#if 0
GtkType
gtk_bar_get_type(void)
{
  printf("\nHEREx10\n");
  static GtkType bar_type = 0;
  
  if (!bar_type)
  {
    const GTypeInfo bar_info =
    {
      sizeof (GtkBarClass),
      (GBaseInitFunc) NULL,//gtk_bar_base_class_init
      (GBaseFinalizeFunc) NULL,//gtk_bar_base_class_finalize
      (GClassInitFunc) gtk_bar_class_init,
      NULL        /* class_finalize */,
      NULL        /* class_data */,
      sizeof (GtkBar),
      0           /* n_preallocs */,
      (GInstanceInitFunc) gtk_bar_init,
      NULL,       /* value_table */
    };

    const GInterfaceInfo buildable_info =
    {
      (GInterfaceInitFunc) gtk_bar_buildable_init,
      (GInterfaceFinalizeFunc)NULL,
      (gpointer)NULL
    };

    bar_type =
        g_type_register_static (gtk_widget_get_type(), "GtkBar",
                                &bar_info, (GTypeFlags)0);

    g_type_add_interface_static (bar_type,
                                  GTK_TYPE_BUILDABLE,
                                  &buildable_info);

#if 0
    static const GtkTypeInfo bar_info =
    {
    "GtkBar",
    sizeof (GtkBar),
    sizeof (GtkBarClass),
    (GtkClassInitFunc) gtk_bar_class_init,
    (GtkObjectInitFunc) gtk_bar_init,
    /* reserved_1 */ NULL,
    /* reserved_2 */ NULL,
    (GtkClassInitFunc) NULL,
    };
    
    bar_type = gtk_type_unique (GTK_TYPE_WIDGET, &bar_info);
#endif
  }
  
  return bar_type;
}
#endif

void
gtk_bar_set_sel(GtkBar *bar, gint num)
{
   bar->sel = num;
   gtk_bar_paint(bar);
}


GtkWidget * gtk_bar_new()
{
//   printf("\nHEREx9\n");
   return GTK_WIDGET(gtk_type_new(gtk_bar_get_type()));
}


static void
gtk_bar_class_init(GtkBarClass *klass)
{
//   printf("\nHEREx8\n");
  GtkWidgetClass *widget_class;
  GtkObjectClass *object_class;


  widget_class = (GtkWidgetClass *) klass;
  object_class = (GtkObjectClass *) klass;

  parent_class = (GtkWidgetClass *)gtk_type_class(gtk_widget_get_type());

  gtk_object_add_arg_type ("GtkBar::color", GTK_TYPE_STRING, GTK_ARG_READWRITE,ARG_COLOR);

  gtk_bar_parent_class = g_type_class_peek_parent (klass);
  klass->bar_paint = gtk_bar_paint_real;

  object_class->set_arg = gtk_bar_set_arg;
  object_class->get_arg = gtk_bar_get_arg;
  widget_class->realize = gtk_bar_realize;
  widget_class->size_request = gtk_bar_size_request;
  widget_class->size_allocate = gtk_bar_size_allocate;
  widget_class->expose_event = gtk_bar_expose;
  object_class->destroy = gtk_bar_destroy;
}


static void
gtk_bar_init(GtkBar *bar)
{
//   printf("\nHEREx6\n");
  bar->sel = 0;
  bar->color=NULL;
}


static void
gtk_bar_size_request(GtkWidget *widget,
    GtkRequisition *requisition)
{
  printf("\nHERExgtk_bar_size_request\n");
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_BAR(widget));
  g_return_if_fail(requisition != NULL);

  requisition->width = widget->allocation.width;
  requisition->height = widget->allocation.height;
}


static void
gtk_bar_size_allocate(GtkWidget *widget,
    GtkAllocation *allocation)
{
  printf("\nHEREx gtk_bar_size_allocate\n");
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_BAR(widget));
  g_return_if_fail(allocation != NULL);

  widget->allocation = *allocation;

  if (GTK_WIDGET_REALIZED(widget)) {
     gdk_window_move_resize(
         widget->window,
         allocation->x, allocation->y,
         allocation->width, allocation->height
     );
   }
}


static void
gtk_bar_realize(GtkWidget *widget)
{
  printf("\nHEREx1\n");
  GdkWindowAttr attributes;
  guint attributes_mask;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_BAR(widget));

  GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;

  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.event_mask = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;

  attributes_mask = GDK_WA_X | GDK_WA_Y;

  widget->window = gdk_window_new(
     gtk_widget_get_parent_window (widget),
     & attributes, attributes_mask
  );

  gdk_window_set_user_data(widget->window, widget);

  widget->style = gtk_style_attach(widget->style, widget->window);
  gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}


static gboolean
gtk_bar_expose(GtkWidget *widget,
    GdkEventExpose *event)
{
  printf("\nHEREx----gtk_bar_expose\n");
  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_BAR(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  gtk_bar_paint(GTK_BAR(widget));

  return FALSE;
}

static void
gtk_bar_paint(GtkBar *bar)
{
  GtkBarClass *klass;
  
  g_return_if_fail (GTK_IS_BAR (bar));
  
  klass = GTK_BAR_GET_CLASS (bar);

  if (klass->bar_paint)
    klass->bar_paint (bar);
}

static void
gtk_bar_paint_real(GtkBar *bar)
{
  printf("\nHEREx3 gtk_bar_paint %d\n",bar->sel);
  cairo_t *cr;

  cr = gdk_cairo_create(GTK_WIDGET(bar)->window);

  cairo_translate(cr, 0, 7);
  cairo_set_source_rgb(cr, 0, 0, 0);
  cairo_paint(cr);
  gint pos = bar->sel;
  gint rect = pos / 5;
  cairo_set_source_rgb(cr, 0.2, 0.4, 0);
  gint i;
  for ( i = 1; i <= 20; i++) {
      if (i > 20 - rect) {
          cairo_set_source_rgb(cr, 0.6, 1.0, 0);
//   printf("\nHEREx3 gtk_bar_paint HERE\n");

      } else {
          cairo_set_source_rgb(cr, 0.2, 0.4, 0);
//   printf("\nHEREx3 gtk_bar_paint 5\n");

      }
      cairo_rectangle(cr, 8, i*4, 30, 3);
      cairo_rectangle(cr, 42, i*4, 30, 3);
      cairo_fill(cr);
  }
  cairo_destroy(cr);
}

static void
gtk_bar_destroy(GtkObject *object)
{
//   printf("\n Destroy \n");
  GtkBar *bar;
  gpointer klass;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_BAR(object));

  bar = GTK_BAR(object);

  klass = gtk_type_class(gtk_widget_get_type());

  if (GTK_OBJECT_CLASS(klass)->destroy) {
     (* GTK_OBJECT_CLASS(klass)->destroy) (object);
  }
}

static void
gtk_bar_set_arg (GtkObject  *object,
        GtkArg     *arg,
        guint  arg_id)
{
  GtkBar  *bar;

  bar = GTK_BAR (object);

  switch (arg_id)
    {
    case ARG_COLOR:
      gtk_bar_set_color(bar, GTK_VALUE_STRING (*arg));
      break;
    default:
      break;
    }
}

static void
gtk_bar_get_arg (GtkObject  *object,
        GtkArg     *arg,
        guint  arg_id)
{
  GtkBar  *bar;

  bar = GTK_BAR (object);

  switch (arg_id)
    {
      GtkWindowGeometryInfo *info;
    case ARG_COLOR:
      GTK_VALUE_STRING (*arg) = g_strdup (bar->color);
      break;
    default:
      arg->type = GTK_TYPE_INVALID;
      break;
    }
}

void
gtk_bar_set_color(GtkBar    *bar,
      const gchar *str)
{

  g_return_if_fail (bar != NULL);
  g_return_if_fail (GTK_IS_BAR (bar));

  if(!bar->color || strcmp(bar->color, str))
    g_free (bar->color);

  bar->color = g_strdup (str);
}

gchar*
gtk_bar_get_color (GtkBar *bar)
{
  g_return_val_if_fail (bar != NULL,NULL);
  g_return_val_if_fail (GTK_IS_BAR (bar),NULL);
  
  return bar->color;
}

gint
gtk_bar_get_sel(GtkBar *bar)
{
  g_return_val_if_fail (bar != NULL,NULL);
  g_return_val_if_fail (GTK_IS_BAR (bar),NULL);
  
  return bar->sel;
}

/* GtkBuildable custom tag implementation
 *
 * <columns>
 *   <column type="..."/>
 *   <column type="..."/>
 * </columns>
 */
/*
void
gtk_list_store_set_column_types (GtkListStore *list_store,
                                     gint          n_columns,
                                     GType        *types)
{
  gint i;

  g_return_if_fail (GTK_IS_LIST_STORE (list_store));
  g_return_if_fail (list_store->columns_dirty == 0);

  gtk_list_store_set_n_columns (list_store, n_columns);
  for (i = 0; i < n_columns; i++)
  {
    if (! _gtk_tree_data_list_check_type (types[i]))
      {
        g_warning ("%s: Invalid type %s\n", G_STRLOC, g_type_name (types[i]));
        continue;
      }
      gtk_list_store_set_column_type (list_store, i, types[i]);
  }
}
*/
typedef struct {
  gboolean translatable;
  gchar *context;
  int id;
} ColInfo;

typedef struct {
  GtkBuilder *builder;
  GObject *object;
  GSList *column_type_names;
  GType *column_types;
  GValue *values;
  gint *colids;
  ColInfo **columns;
  gint last_row;
  gint n_columns;
  gint row_column;
  GQuark error_quark;
  gboolean is_data;
  const gchar *domain;
} SubParserData;

static void
bar_start_element (GMarkupParseContext *context,
			  const gchar         *element_name,
			  const gchar        **names,
			  const gchar        **values,
			  gpointer             user_data,
			  GError             **error)
{
  printf("\nHEREx2\n");
  guint i;
  SubParserData *data = (SubParserData*)user_data;

  if (strcmp (element_name, "col") == 0)
    {
      int i, id = -1;
      gchar *context = NULL;
      gboolean translatable = FALSE;
      ColInfo *info;

      if (data->row_column >= data->n_columns)
        {
	  g_set_error (error, data->error_quark, 0,
	  	       "Too many columns, maximum is %d\n", data->n_columns - 1);
          return;
        }

      for (i = 0; names[i]; i++)
	if (strcmp (names[i], "id") == 0)
	  {
/*	    errno = 0;
	    id = atoi (values[i]);
	    if (errno)
              {
	        g_set_error (error, data->error_quark, 0,
		  	     "the id tag %s could not be converted to an integer",
			     values[i]);
                return;
              }
	    if (id < 0 || id >= data->n_columns)
              {
                g_set_error (error, data->error_quark, 0,
                             "id value %d out of range", id);
                return;
              }*/
	  }
	else if (strcmp (names[i], "translatable") == 0)
	  {
/*	    if (!_gtk_builder_boolean_from_string (values[i], &translatable,
						   error))
	      return;*/
	  }
	else if (strcmp (names[i], "comments") == 0)
	  {
	    /* do nothing, comments are for translators */
	  }
	else if (strcmp (names[i], "context") == 0) 
	  {
	    context = g_strdup (values[i]);
	  }

      if (id == -1)
        {
	  g_set_error (error, data->error_quark, 0,
	  	       "<col> needs an id attribute");
          return;
        }
      
      info = g_slice_new0 (ColInfo);
      info->translatable = translatable;
      info->context = context;
      info->id = id;

      data->colids[data->row_column] = id;
      data->columns[data->row_column] = info;
      data->row_column++;
      data->is_data = TRUE;
    }
  else if (strcmp (element_name, "row") == 0)
    ;
  else if (strcmp (element_name, "column") == 0)
    for (i = 0; names[i]; i++)
      if (strcmp (names[i], "type") == 0)
	data->column_type_names = g_slist_prepend (data->column_type_names,
						   g_strdup (values[i]));
  else if (strcmp (element_name, "columns") == 0)
    ;
  else if (strcmp (element_name, "data") == 0)
    ;
  else
    g_set_error (error, data->error_quark, 0,
		 "Unknown start tag: %s", element_name);
}

static void
bar_end_element (GMarkupParseContext *context,
			const gchar         *element_name,
			gpointer             user_data,
			GError             **error)
{
  printf("\nHEREx3\n");
  SubParserData *data = (SubParserData*)user_data;

  g_assert (data->builder);
  
  if (strcmp (element_name, "row") == 0)
    {
      GtkTreeIter iter;
      int i;

      /*gtk_bar_insert_with_valuesv (GTK_BAR (data->object),
					  &iter,
					  data->last_row,
					  data->colids,
					  data->values,
					  data->row_column);
      */
      for (i = 0; i < data->row_column; i++)
	{
	  ColInfo *info = data->columns[i];
	  g_free (info->context);
	  g_slice_free (ColInfo, info);
	  data->columns[i] = NULL;
	  g_value_unset (&data->values[i]);
	}
      g_free (data->values);
      data->values = g_new0 (GValue, data->n_columns);
      data->last_row++;
      data->row_column = 0;
    }
  else if (strcmp (element_name, "columns") == 0)
    {
      GType *column_types;
      GSList *l;
      int i;
      GType type;

      data->column_type_names = g_slist_reverse (data->column_type_names);
      column_types = g_new0 (GType, g_slist_length (data->column_type_names));

      for (l = data->column_type_names, i = 0; l; l = l->next, i++)
	{
    type = gtk_builder_get_type_from_name (data->builder, (const char*)l->data);
	  if (type == G_TYPE_INVALID)
	    {
	      g_warning ("Unknown type %s specified in treemodel %s",
			 (const gchar*)l->data,
			 gtk_buildable_get_name (GTK_BUILDABLE (data->object)));
	      continue;
	    }
	  column_types[i] = type;

	  g_free (l->data);
	}
/*
      gtk_bar_set_column_types (GTK_BAR (data->object), i,
				       column_types);
*/
      g_free (column_types);
    }
  else if (strcmp (element_name, "col") == 0)
    data->is_data = FALSE;
  else if (strcmp (element_name, "data") == 0)
    ;
  else if (strcmp (element_name, "column") == 0)
    ;
  else
    g_set_error (error, data->error_quark, 0,
		 "Unknown end tag: %s", element_name);
}

static void
bar_text (GMarkupParseContext *context,
		 const gchar         *text,
		 gsize                text_len,
		 gpointer             user_data,
		 GError             **error)
{
  SubParserData *data = (SubParserData*)user_data;
  gint i;
  GError *tmp_error = NULL;
  gchar *string;
  ColInfo *info;
  
  if (!data->is_data)
    return;

  i = data->row_column - 1;
  info = data->columns[i];

  string = g_strndup (text, text_len);
#if 0
  if (info->translatable && text_len)
    {
      gchar *translated;

      /* FIXME: This will not use the domain set in the .ui file,
       * since the parser is not telling the builder about the domain.
       * However, it will work for gtk_builder_set_translation_domain() calls.
       */
      translated = _gtk_builder_parser_translate (data->domain,
						  info->context,
						  string);
      g_free (string);
      string = translated;
    }
#endif
  if (!gtk_builder_value_from_string_type (data->builder,
					   data->column_types[info->id],
					   string,
					   &data->values[i],
					   &tmp_error))
    {
      g_set_error (error,
		   tmp_error->domain,
		   tmp_error->code,
		   "Could not convert '%s' to type %s: %s\n",
		   text, g_type_name (data->column_types[info->id]),
		   tmp_error->message);
      g_error_free (tmp_error);
    }
  g_free (string);
}

static const GMarkupParser bar_parser =
  {
    bar_start_element,
    bar_end_element,
    bar_text
  };

static gboolean
gtk_bar_buildable_custom_tag_start (GtkBuildable  *buildable,
					   GtkBuilder    *builder,
					   GObject       *child,
					   const gchar   *tagname,
					   GMarkupParser *parser,
					   gpointer      *data)
{
  printf("\nHEREx4\n");
  SubParserData *parser_data;

  if (child)
    return FALSE;

  if (strcmp (tagname, "columns") == 0)
    {

      parser_data = g_slice_new0 (SubParserData);
      parser_data->builder = builder;
      parser_data->object = G_OBJECT (buildable);
      parser_data->column_type_names = NULL;

      *parser = bar_parser;
      *data = parser_data;
      return TRUE;
    }
  else if (strcmp (tagname, "data") == 0)
    {
#if 0
      gint n_columns = gtk_bar_get_n_columns (GTK_TREE_MODEL (buildable));
      if (n_columns == 0)
	g_error ("Cannot append data to an empty model");

      parser_data = g_slice_new0 (SubParserData);
      parser_data->builder = builder;
      parser_data->object = G_OBJECT (buildable);
      parser_data->values = g_new0 (GValue, n_columns);
      parser_data->colids = g_new0 (gint, n_columns);
      parser_data->columns = g_new0 (ColInfo*, n_columns);
      parser_data->column_types = GTK_BAR (buildable)->column_headers;
      parser_data->n_columns = n_columns;
      parser_data->last_row = 0;
      parser_data->error_quark = g_quark_from_static_string ("GtkListStore");
      parser_data->domain = gtk_builder_get_translation_domain (builder);
      
      *parser = bar_parser;
      *data = parser_data;
      return TRUE;
#endif
    }
  else
    g_warning ("Unknown custom list store tag: %s", tagname);
  
  return FALSE;
}

static void
gtk_bar_buildable_custom_tag_end (GtkBuildable *buildable,
					 GtkBuilder   *builder,
					 GObject      *child,
					 const gchar  *tagname,
					 gpointer     *data)
{
  printf("\nHEREx5\n");
  SubParserData *sub = (SubParserData*)data;
  
  if (strcmp (tagname, "columns") == 0)
    {
      g_slist_free (sub->column_type_names);
      g_slice_free (SubParserData, sub);
    }
  else if (strcmp (tagname, "data") == 0)
    {
      int i;
      for (i = 0; i < sub->n_columns; i++)
	{
	  ColInfo *info = sub->columns[i];
	  if (info)
	    {
	      g_free (info->context);
	      g_slice_free (ColInfo, info);
	    }
	}
      g_free (sub->colids);
      g_free (sub->columns);
      g_free (sub->values);
      g_slice_free (SubParserData, sub);
    }
  else
    g_warning ("Unknown custom list store tag: %s", tagname);
}
