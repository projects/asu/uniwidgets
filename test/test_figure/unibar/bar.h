#ifndef __BAR_H
#define __BAR_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTK_TYPE_BAR            (gtk_bar_get_type ())
#define GTK_BAR(obj)            (GTK_CHECK_CAST ((obj), GTK_TYPE_BAR, GtkBar))
#define GTK_BAR_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_BAR, GtkBarClass))
#define GTK_IS_BAR(obj)         (GTK_CHECK_TYPE ((obj), GTK_TYPE_BAR))
#define GTK_IS_BAR_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_BAR))
#define GTK_BAR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_BAR, GtkBarClass))

typedef struct _GtkBar GtkBar;
typedef struct _GtkBarClass GtkBarClass;


struct _GtkBar {
  GtkWidget widget;

  gchar *GSEAL(color);
  gint GSEAL(sel);
};

struct _GtkBarClass {
  GtkWidgetClass parent_class;

  /* Hook to customize paint function */
  void (* bar_paint) (GtkBar *bar);
};


GtkType gtk_bar_get_type(void);
GtkWidget * gtk_bar_new();

gint gtk_bar_get_sel(GtkBar *bar);
void gtk_bar_set_sel(GtkBar *bar,
                     gint sel);

void gtk_bar_set_color(GtkBar *bar,
                       const gchar *str);
gchar* gtk_bar_get_color(GtkBar *bar);

G_END_DECLS

#endif /* __BAR_H */