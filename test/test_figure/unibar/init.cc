#include <cstdlib>

#include "init.h"
#include "wrap_init.h"

namespace Gtk
{
  namespace UniWidgets
  {

    void init(int& argc, char**& argv)
    {
	wrap_init();
    }

  } // namespace UniWidgets
} // namespace Gtk
