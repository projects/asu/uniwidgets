#ifndef _GTKMM_UNIWIDGETS_INIT_H
#define _GTKMM_UNIWIDGETS_INIT_H

namespace Gtk
{
  namespace UniWidgets
  {

    void init(int& argc, char**& argv);

  } // namespace UniWidgets
} // namespace Gtk

#endif // _GTKMM_GL_INIT_H
