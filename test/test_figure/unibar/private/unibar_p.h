#ifndef _GTKMM_BAR_P_H
#define _GTKMM_BAR_P_H
// -------------------------------------------------------------------------
#include <gtkmm/private/widget_p.h>
#include <glibmm/class.h>
// // -------------------------------------------------------------------------
namespace Gtk
{
namespace UniWidgets
{

class Bar_Class : public Glib::Class
{
public:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  typedef Bar CppObjectType;
  typedef GtkBar BaseObjectType;
  typedef GtkBarClass BaseClassType;
  typedef Gtk::Widget_Class CppClassParent;
  typedef GtkWidgetClass BaseClassParent;

  friend class Bar;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

  const Glib::Class& init();

  static void class_init_function(void* g_class, void* class_data);

  static Glib::ObjectBase* wrap_new(GObject*);

protected:

#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
#endif //GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED

  //Callbacks (virtual functions):
#ifdef GLIBMM_VFUNCS_ENABLED
  static void bar_paint_vfunc_callback(GtkBar* self);
#endif //GLIBMM_VFUNCS_ENABLED
};

} // namespace UniWidgets
} // namespace Gtk


#endif /* _GTKMM_BAR_P_H */
