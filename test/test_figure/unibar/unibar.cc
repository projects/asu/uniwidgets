#include "unibar.h"
// #include <gtkmm/private/unibar_p.h>
#include <gtk/gtk.h>
#include <gtkmm/stock.h>
#include "bar.h"
#include "private/unibar_p.h"
// -------------------------------------------------------------------------
namespace Gtk
{
namespace UniWidgets
{

Bar::Bar()
:
  // Mark this class as non-derived to allow C++ vfuncs to be skipped.
  Glib::ObjectBase(0),
  Gtk::Widget(Glib::ConstructParams(bar_class_.init()))
{
  printf("\nconstructor3\n");
}
// -------------------------------------------------------------------------
Bar::Bar(const StockID& stock_id)
:
  // Mark this class as non-derived to allow C++ vfuncs to be skipped.
  Glib::ObjectBase(0),
  Gtk::Widget(Glib::ConstructParams(bar_class_.init()))
{}


// -------------------------------------------------------------------------
Glib::ObjectBase* Bar_Class::wrap_new(GObject* o)
{
  printf("WRAP NEW");
  return manage(new Bar((GtkBar*)(o)));
}
// -------------------------------------------------------------------------
/* The *_Class implementation: */
const Glib::Class& Bar_Class::init()
{
  printf("\nHERE3\n");
  if(!gtype_) // create the GType if necessary
  {
    printf("\nHERE3 - gtype==0\n");
    // Glib::Class has to know the class init function to clone custom types.
    class_init_func_ = &Bar_Class::class_init_function;

    // This is actually just optimized away, apparently with no harm.
    // Make sure that the parent type has been created.
    //CppClassParent::CppObjectType::get_type();

    // Create the wrapper type, with the same class/instance size as the base type.
    register_derived_type(gtk_bar_get_type());

    // Add derived versions of interfaces, if the C type implements any interfaces:

  }

  return *this;
}
// -------------------------------------------------------------------------
void Bar_Class::class_init_function(void* g_class, void* class_data)
{
  printf("\nHERE4\n");
  BaseClassType *const klass = static_cast<BaseClassType*>(g_class);
  CppClassParent::class_init_function(klass, class_data);

#ifdef GLIBMM_VFUNCS_ENABLED
  printf("INITIALIZED VFUNC\n");
  klass->bar_paint = &bar_paint_vfunc_callback;
#endif //GLIBMM_VFUNCS_ENABLED
}
// -------------------------------------------------------------------------
#ifdef GLIBMM_VFUNCS_ENABLED
void Bar_Class::bar_paint_vfunc_callback(GtkBar* self)
{
  printf("bar_paint_vfunc_callback\n");
  Glib::ObjectBase *const obj_base = static_cast<Glib::ObjectBase*>(
      Glib::ObjectBase::_get_current_wrapper((GObject*)self));

  // Non-gtkmmproc-generated custom classes implicitly call the default
  // Glib::ObjectBase constructor, which sets is_derived_. But gtkmmproc-
  // generated classes can use this optimisation, which avoids the unnecessary
  // parameter conversions if there is no possibility of the virtual function
  // being overridden:
  if(obj_base && obj_base->is_derived_())
  {
    CppObjectType *const obj = dynamic_cast<CppObjectType* const>(obj_base);
    if(obj) // This can be NULL during destruction.
    {
      #ifdef GLIBMM_EXCEPTIONS_ENABLED
      try // Trap C++ exceptions which would normally be lost because this is a C callback.
      {
      #endif //GLIBMM_EXCEPTIONS_ENABLED
        // Call the virtual member method, which derived classes might override.
        printf("DERIVED\n");
        obj->bar_paint_vfunc();
        return;
      #ifdef GLIBMM_EXCEPTIONS_ENABLED
      }
      catch(...)
      {
        Glib::exception_handlers_invoke();
      }
      #endif //GLIBMM_EXCEPTIONS_ENABLED
    }
  }

  BaseClassType *const base = static_cast<BaseClassType*>(
      g_type_class_peek_parent(G_OBJECT_GET_CLASS(self)) // Get the parent class of the object class (The original underlying C class).
  );
  printf("ORIGINAL\n");
  // Call the original underlying C function:
  if(base && base->bar_paint)
    (*base->bar_paint)(self);

}
#endif //GLIBMM_VFUNCS_ENABLED
// -------------------------------------------------------------------------
#ifdef GLIBMM_VFUNCS_ENABLED
void Gtk::UniWidgets::Bar::bar_paint_vfunc()
{
  printf("bar_paint_vfunc\n");
  BaseClassType *const base = static_cast<BaseClassType*>(
      g_type_class_peek_parent(G_OBJECT_GET_CLASS(gobject_)) // Get the parent class of the object class (The original underlying C class).
  );

  if(base && base->bar_paint)
    (*base->bar_paint)(gobj());
}
#endif //GLIBMM_VFUNCS_ENABLED
// -------------------------------------------------------------------------


/* The implementation: */
Bar::Bar(const Glib::ConstructParams& construct_params)
:
  Gtk::Widget(construct_params)
{
  printf("\nconstructor\n");
}
// -------------------------------------------------------------------------
Bar::Bar(GtkBar* castitem)
:
  Gtk::Widget((GtkWidget*)(castitem))
{
  printf("\nconstructor2\n");
}
// -------------------------------------------------------------------------
Bar::~Bar()
{
  destroy_();
}
// -------------------------------------------------------------------------
Bar::CppClassType Bar::bar_class_; // initialize static member

GType Bar::get_type()
{
  printf("\nHERE2\n");
  return bar_class_.init().get_type();
}
// -------------------------------------------------------------------------
GType Bar::get_base_type()
{
  return gtk_bar_get_type();
}
// -------------------------------------------------------------------------
void Bar::set_sel(gint value)
{
  gtk_bar_set_sel(gobj(),value);
}
// -------------------------------------------------------------------------
gint Bar::get_sel()
{
  gtk_bar_get_sel(gobj());
}
// -------------------------------------------------------------------------
#ifdef GLIBMM_PROPERTIES_ENABLED
Glib::PropertyProxy<Glib::ustring> Bar::property_color() 
{
  return Glib::PropertyProxy<Glib::ustring>(this, "color");
}
#endif //GLIBMM_PROPERTIES_ENABLED
// -------------------------------------------------------------------------
#ifdef GLIBMM_PROPERTIES_ENABLED
Glib::PropertyProxy_ReadOnly<Glib::ustring> Bar::property_color() const
{
  return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this, "color");
}
#endif //GLIBMM_PROPERTIES_ENABLED
// -------------------------------------------------------------------------
} //namespace UniWidgets
} // namespace Gtk
// -------------------------------------------------------------------------
namespace Glib
{

Gtk::UniWidgets::Bar* wrap(GtkBar* object, bool take_copy)
{
  return dynamic_cast<Gtk::UniWidgets::Bar *> (Glib::wrap_auto ((GObject*)(object), take_copy));
}

} /* namespace Glib */
// -------------------------------------------------------------------------
