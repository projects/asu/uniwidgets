#ifndef _GTKMM_BAR_H
#define _GTKMM_BAR_H


#include <glibmm.h>

#include <gtkmm/widget.h>
#include <gtkmm/stockid.h>


#ifndef DOXYGEN_SHOULD_SKIP_THIS
typedef struct _GtkBar GtkBar;
typedef struct _GtkBarClass GtkBarClass;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

namespace Gtk
{
namespace UniWidgets
{

class Bar_Class;

namespace Stock { struct BuiltinStockID; }

class Bar : public Widget
{
  public:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  typedef Bar CppObjectType;
  typedef Bar_Class CppClassType;
  typedef GtkBar BaseObjectType;
  typedef GtkBarClass BaseClassType;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

  virtual ~Bar();

#ifndef DOXYGEN_SHOULD_SKIP_THIS
  void set_sel(gint value);
  gint get_sel();
private:
  friend class Bar_Class;
  static CppClassType bar_class_;

  // noncopyable
  Bar(const Bar&);
  Bar& operator=(const Bar&);

protected:
  explicit Bar(const Glib::ConstructParams& construct_params);
  explicit Bar(GtkBar* castitem);

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

  virtual void bar_paint_vfunc();

public:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  static GType get_type()      G_GNUC_CONST;
  static GType get_base_type() G_GNUC_CONST;
#endif

  ///Provides access to the underlying C GtkObject.
  GtkBar*       gobj()       { return reinterpret_cast<GtkBar*>(gobject_); }

  ///Provides access to the underlying C GtkObject.
  const GtkBar* gobj() const { return reinterpret_cast<GtkBar*>(gobject_); }


public:
#ifdef GLIBMM_PROPERTIES_ENABLED
  Glib::PropertyProxy<Glib::ustring> property_color() ;
#endif //GLIBMM_PROPERTIES_ENABLED
#ifdef GLIBMM_PROPERTIES_ENABLED
  Glib::PropertyProxy_ReadOnly<Glib::ustring> property_color() const;
#endif //GLIBMM_PROPERTIES_ENABLED
private:

public:

  Bar();

//   explicit Bar();

  explicit Bar(const StockID& stock_id);

};

} //namespace UniWidgets
} // namespace Gtk


namespace Glib
{
  /** A Glib::wrap() method for this object.
   * 
   * @param object The C instance.
   * @param take_copy False if the result should take ownership of the C instance. True if it should take a new copy or ref.
   * @result A C++ instance that wraps this C instance.
   *
   * @relates Gtk::UniWidgets::Bar
   */
  Gtk::UniWidgets::Bar* wrap(GtkBar* object, bool take_copy = false);
} //namespace Glib


#endif /* _GTKMM_BAR_H */

