#include <iostream>
#include <gtkmm.h>
#include "unibar2.h"
#include <gtk/gtk.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace Gtk;
using namespace UniWidgets;
// -------------------------------------------------------------------------
#define AXIS_WIDTH 2
#define AXIS_Y_COORD 20

#define MAX_VALUE 100 //percent 100% Нужно для соблюдения масштаба

#define BAR_WIDTH 60
#define BAR_HEIGHT 198
#define COORD_W 200
#define COORD_H 200

#define TOP_BAR 0
#define LEFT_BAR 100

#define N_BIG_MARKS 6
#define N_SMALL_MARKS 5

#define BIG_MARK_W 6
#define BIG_MARK_H 2
#define SMALL_MARK_W 4
#define SMALL_MARK_H 1

#define UNIBAR2_BACK  "back-color"

#define INIT_UNIBAR2_PROPERTIES() \
  property_back_color_(*this, UNIBAR2_BACK, "black" )
// -------------------------------------------------------------------------
void UniBar2::custom_get_property_callback(GObject * object, unsigned int property_id, GValue * value, GParamSpec * param_spec)
{
  cout<<"custom_get_property_callback"<<endl;
    Glib::ObjectBase *const wrapper = Glib::ObjectBase::_get_current_wrapper(object);
    if (!wrapper)
            return;
    UniBar2 *renderer = dynamic_cast<UniBar2 *>(wrapper);
    if (!renderer)
            return;
    Glib::custom_get_property_callback(object,property_id,value,param_spec);
//         if (property_id == renderer->m_property_id) {
//                 if (renderer->m_property_paramspec == param_spec) {
//                         g_value_copy(renderer->m_property_value.gobj(), value);
//                 } else {
//                         G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, param_spec);
//                 }
//         } else {
//                 G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, param_spec);
//         }
}
// -------------------------------------------------------------------------
void UniBar2::custom_set_property_callback(GObject * object, unsigned int property_id, const GValue * value, GParamSpec * param_spec)
{
  cout<<"custom_set_property_callback"<<endl;
  Wrap_New(object);
  Glib::ObjectBase *const wrapper = Glib::ObjectBase::_get_current_wrapper(object);
  cout<<"custom_set_property_callback wrapper"<<wrapper<<endl;
  if (!wrapper)
    return;
  UniBar2 *renderer = dynamic_cast<UniBar2 *>(wrapper);
  if (!renderer)
    return;
  cout<<"custom_set_property_callback OK"<<endl;
  Glib::custom_set_property_callback(object,property_id,value,param_spec);
        //std::cerr << "UniBar2::custom_set_property_callback: prop id: " << property_id << std::endl;
//         if (property_id == renderer->m_property_id) {
//                 if (renderer->m_property_paramspec == param_spec) {
//                         g_value_copy(value, renderer->m_property_value.gobj());
//                         g_object_notify(object, g_param_spec_get_name(param_spec));
//                 } else {
//                         G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, param_spec);
//                 }
//         } else {
//                 G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, param_spec);
//         }
}
// -------------------------------------------------------------------------
void UniBar2::constructor()
{
  cout<<"UniBar2 constructor"<<endl;

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
//         GParamSpec *paramspec = g_object_class_find_property(goclass, get_property_name());
//         if (paramspec) {
//                 g_param_spec_unref(m_property_paramspec);
//                 m_property_paramspec = paramspec;
//                 g_param_spec_ref(m_property_paramspec);
//         } else {
  goclass->get_property = &UniBar2::custom_get_property_callback;
  goclass->set_property = &UniBar2::custom_set_property_callback;
//                 g_object_class_install_property(goclass, get_property_id(), m_property_paramspec);
//         }
//         std::cerr << "UniBar2::UniBar2: pspec " << G_PARAM_SPEC_VALUE_TYPE(m_property_paramspec)
//                   << " prop " << G_VALUE_TYPE(m_property_value.gobj()) << std::endl;
//         g_assert(G_PARAM_SPEC_VALUE_TYPE(m_property_paramspec) == G_VALUE_TYPE(m_property_value.gobj()));
}
// -------------------------------------------------------------------------
UniBar2::UniBar2(const Glib::ConstructParams& construct_params):
  Bar(construct_params)
  ,INIT_UNIBAR2_PROPERTIES()
//   ,m_property_value(0)
{
  cout<<"UniBar2 HERE 1"<<endl;
}
// -------------------------------------------------------------------------
UniBar2::UniBar2() :
  Glib::ObjectBase("UniBar2")
  ,INIT_UNIBAR2_PROPERTIES()
//   ,m_property_value(0)
{
  cout<<"UniBar2 HERE 2"<<endl;
  constructor();
}
// -------------------------------------------------------------------------
UniBar2::UniBar2(Bar::BaseObjectType* gobject) :
  Bar(gobject)
  ,INIT_UNIBAR2_PROPERTIES()
//   ,m_property_value(0)
{
  constructor();
}
// -------------------------------------------------------------------------
UniBar2::~UniBar2()
{
}
// -------------------------------------------------------------------------
Glib::PropertyProxy<Glib::ustring> UniBar2::property_back_color() 
{
  return property_back_color_.get_proxy();
}
// -------------------------------------------------------------------------
Glib::PropertyProxy_ReadOnly<Glib::ustring> UniBar2::property_back_color() const
{
  return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this, UNIBAR2_BACK);
}
// -------------------------------------------------------------------------
Glib::ObjectBase* UniBar2::Wrap_New(GObject* o)
{
  std::cout<<"UniBar2 Wrap_New"<<std::endl;

//   GParamSpec **specs;
//   GParamSpec *pspec;
//   guint n_specs = 0;
//   gint i;
//   GValue value = { 0 };

//   specs = g_object_class_list_properties(G_OBJECT_GET_CLASS(o), &n_specs);
//   for(i=0; i<n_specs; i++)
//   {
//     if( !strcmp(specs[i]->name,"user-data") )
//     {
//       g_value_set_string(&value, "WHITE!!!");
//       value = gtk_object_get_user_data(o);
//       g_value_init(&value,G_TYPE_POINTER);
//       g_object_get_property(o,specs[i]->name,&value);
//       std::cout<<"!!!!!!!!!!!NAME="<<specs[i]->name<<" type-- "<<g_value_get_pointer(&value)<<endl;
//     }
//   }

  UniBar2 *uw = manage(new UniBar2(( Bar::BaseObjectType* )(o)));
  GtkWidget *w = GTK_WIDGET(uw->gobj());
  GObjectClass *klass = G_OBJECT_GET_CLASS(w);

//   for(i=0; i<n_specs; i++)
//   {
//       std::cout<<"NAME="<<specs[i]->name<<" -- "<<endl;
//   }
  return uw;
}
// -------------------------------------------------------------------------
void
uni_bar2_set_arg (GtkObject  *object,
        GtkArg     *arg,
        guint  arg_id)
{
  printf("SET ARG UNIBAR!!!! %d \n\n",arg_id);
//   GtkBar  *bar;

//   bar = GTK_BAR (object);

  switch (arg_id)
    {
    case '1':
//       gtk_bar_set_color(bar, GTK_VALUE_STRING (*arg));
      break;
    default:
  printf("another type=\n\n",arg_id);
      break;
    }
}
// -------------------------------------------------------------------------
void
uni_bar2_get_arg (GtkObject  *object,
        GtkArg     *arg,
        guint  arg_id)
{
  printf("GET ARG UNIBAR!!!! %d \n\n",arg_id);
//   GtkBar  *bar;

//   bar = GTK_BAR (object);

  switch (arg_id)
    {
    case '1':
//       GTK_VALUE_STRING (*arg) = g_strdup (bar->color);
      break;
    default:
      arg->type = GTK_TYPE_INVALID;
      printf("another type=\n\n",arg_id);
      break;
    }
}
// -------------------------------------------------------------------------
GType UniBar2::Get_Type()
{
  std::cout<<"UniBar2 Get_Type"<<std::endl;
  static GType gtype = 0;
  if (!gtype) {
    std::cout<<"Get_Type UniBar2 type==0"<<std::endl;
//     gtk_object_add_arg_type("UniBar::font-color", GTK_TYPE_STRING, GTK_ARG_READWRITE,'2');
    UniBar2* dummy = new UniBar2();
    std::cout<<"Get_Type UniBar2 type==0 after new"<<std::endl;
    gtype = G_OBJECT_TYPE(dummy->gobj());
    delete( dummy );
    Glib::wrap_register(gtype, &UniBar2::Wrap_New);
  }
  return gtype;
}
// -------------------------------------------------------------------------
// bool UniBar2::on_expose_event(GdkEventExpose* event)
// {
//   cout<<"ON EXPOSE UNIBAR2"<<endl;
//   Bar::on_expose_event(event);
// //   bar_paint_vfunc();
//
//   return false;
// }
// -------------------------------------------------------------------------
#if 1
void UniBar2::bar_paint_vfunc()
{
  cout<<"BAR PAINT VFUNC UNIBAR2"<<endl;
//   Bar::bar_paint_vfunc();

  Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
  Gtk::Allocation alloc = get_allocation();

  cr->set_line_width(AXIS_WIDTH);
//   //Background
//   cr->save();
//   cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);
//   cr->paint();
//   cr->restore();
//   cr->clip();
// 
  //Шкала
  cr->save();
  cr->set_source_rgba(0.2, 0.4, 0.0, 1.0);
  cr->move_to(alloc.get_width() -1- AXIS_Y_COORD, alloc.get_height()-1);
  cr->rel_line_to(-alloc.get_width() +1 + AXIS_Y_COORD, 0);
  cr->move_to(alloc.get_width() -1- AXIS_Y_COORD, alloc.get_height()-1);
  cr->rel_line_to(0, - alloc.get_height()+1);
  cr->stroke_preserve();
  cr->restore();
//   cr->clip();

//   //Деления шклы
  cr->save();
  cr->reset_clip();
  cr->set_source_rgba(0.2, 0.4, 0.0, 1.0);
  for(int i=0;i<=N_BIG_MARKS;i++)
  {
    cr->set_line_width(BIG_MARK_H);
    cr->move_to ( alloc.get_width() - AXIS_Y_COORD - AXIS_WIDTH - BIG_MARK_W/2, i*(COORD_H/(N_BIG_MARKS-1) ));
    cr->line_to ( alloc.get_width() - AXIS_Y_COORD + BIG_MARK_W/2, i*(COORD_H/(N_BIG_MARKS-1) ));
    cr->stroke();
  }
  cr->stroke_preserve();
//   cr->clip();
  cr->restore();

  //Bar 0-100 % for example;size is 200x200;bar size is 100
  double pos = get_sel();
  cout<<"VALUE IS: "<<pos<<endl;

  if(pos < 0)
  {
    pos = 0;
  }
  else if(pos > 100)
  {
    pos = 100*double(BAR_HEIGHT)/double(MAX_VALUE);
  }
  else
    pos = pos*double(BAR_HEIGHT)/double(MAX_VALUE);

  cr->save();
  cr->set_source_rgb(0.2, 0.4, 0);
  cr->rectangle(LEFT_BAR , TOP_BAR , BAR_WIDTH , BAR_HEIGHT - pos );
  cr->fill();
  cr->set_source_rgb(0.6, 1.0, 0);//Light
  cr->rectangle(LEFT_BAR , BAR_HEIGHT-pos , BAR_WIDTH , pos );
  cr->fill();
  cr->restore();
//   Gdk::Window::end_paint(cr);
}
#endif
// -------------------------------------------------------------------------
// namespace Glib
// {
// 
// Gtk::UniWidgets::Bar* wrap(GObject* object, bool take_copy=false)
// {
//   cout<<"WRAP WRAP WRAP!!!"<<endl;
//   return dynamic_cast<UniBar2*> (Glib::wrap_auto((GObject*)(object), take_copy));
// }
// 
// }
// -------------------------------------------------------------------------
