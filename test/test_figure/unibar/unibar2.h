#ifndef _UNIBAR2_H
#define _UNIBAR2_H
// -------------------------------------------------------------------------
// #include <components/SimpleImage.h>
// #include <global_macros.h>
#include <glibmm.h>
#include "unibar.h"
// -------------------------------------------------------------------------
namespace Gtk
{

namespace UniWidgets
{

class UniBar2 : public Bar
{
public:
  UniBar2();
  UniBar2(const Glib::ConstructParams& construct_params);
  explicit UniBar2(Bar::BaseObjectType* gobject);
  virtual ~UniBar2();

//   virtual bool on_expose_event(GdkEventExpose*);
  virtual void bar_paint_vfunc();
  static GType Get_Type();
  static Glib::ObjectBase* Wrap_New(GObject* o);

protected:

  /* Properties */
  Glib::Property<Glib::ustring> property_back_color_;
  GParamSpec *m_property_paramspec;

private:

  void constructor();

  static void custom_get_property_callback(GObject *object, unsigned int property_id,
                                                 GValue *value, GParamSpec *param_spec);
  static void custom_set_property_callback(GObject *object, unsigned int property_id,
                                                 const GValue *value, GParamSpec *param_spec);

public:

  Glib::PropertyProxy<Glib::ustring> property_back_color() ;
  Glib::PropertyProxy_ReadOnly<Glib::ustring> property_back_color() const;

};

}//namespace UniWidgets
}//namespace Gtk

// namespace Glib
// {
//   /** @relates UniWidgets::Bar
//    * @param object The C instance
//    * @param take_copy False if the result should take ownership of the C instance. True if it should take a new copy or ref.
//    * @result A C++ instance that wraps this C instance.
//    */
//    Gtk::UniWidgets::Bar* wrap(GtkBar* object, bool take_copy = false);
// }

#endif
