#include <glib.h>

// Disable the 'const' function attribute of the get_type() functions.
// GCC would optimize them out because we don't use the return value.
#undef  G_GNUC_CONST
#define G_GNUC_CONST /* empty */

#include "wrap_init.h"
#include <glibmm/error.h>
#include <glibmm/object.h>

// #include the widget headers so that we can call the get_type() static methods:

#include <gtkmm/main.h>
#include "unibar.h"
#include "bar.h"
#include "unibar2.h"

#define UniBar2GetType ( Gtk::UniWidgets::UniBar2::Get_Type() )

extern "C"
{

//Declarations of the *_get_type() functions:

GType gtk_bar_get_type(void);
GType gtkmm___custom_object__uni_bar2_get_type(void)
{
	GType type = Gtk::UniWidgets::UniBar2::Get_Type();
	return type;
}

} // extern "C"


//Declarations of the *_Class::wrap_new() methods, instead of including all the private headers:

namespace Gtk {

namespace UniWidgets
{

class Bar_Class { public: static Glib::ObjectBase* wrap_new(GObject*); };
}
}

namespace Gtk { 

namespace UniWidgets{

void wrap_init()
{
	printf("HERE1");
	// Map gtypes to gtkmm wrapper-creation functions:
	Glib::wrap_register(gtk_bar_get_type(), &Gtk::UniWidgets::Bar_Class::wrap_new);
	Glib::wrap_register(UniBar2GetType, &Gtk::UniWidgets::UniBar2::Wrap_New);

	// Register the gtkmm gtypes:
	Gtk::UniWidgets::Bar::get_type();
	Gtk::UniWidgets::UniBar2::Get_Type;

} // wrap_init()

}//UniWidgets
} //Gtk


