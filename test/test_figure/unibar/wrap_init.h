#ifndef _GTKMM_BAR_WRAP_INIT_H
#define _GTKMM_BAR_WRAP_INIT_H

namespace Gtk
{

namespace UniWidgets{

void wrap_init();

}//UniWidgets

} /* namespace Gtk */

#endif // _GTKMM_BAR_WRAP_INIT_H
