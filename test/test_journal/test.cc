#include <iostream>
#include <gtkmm.h>
#include <libglademm.h>
#include <Configuration.h>

using namespace std;
using namespace UniSetTypes;

static const string GLADE_DIR = "glade/";
static const string GLADE_FILE = "main.glade";
static gboolean alpha_channel_support = false;

int main (int argc, char **argv)
{
	Gtk::Window *w;
	Gtk::Button *btn;

	Gtk::Main Kit(argc, argv);

	if (!g_thread_supported())
		g_thread_init(NULL);

	Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(GLADE_DIR + GLADE_FILE);

	gxml->get_widget("window1", w);

	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );

	Kit.run(*w);

	return 0;
}
