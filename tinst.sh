#! /bin/sh

cp lib/.libs/libUniWidgets.so.0.0.0 /usr/lib/
cp plugins/glade/.libs/libglade-uniwidgets.so /usr/lib/glade3/modules/
cp plugins/libglade/.libs/libuniwidgets.so /usr/lib/libglade/2.0/

USER=`whoami`

cp plugins/glade/uniwidgets.xml /tmp/uniwidgets.xml.tmp

subst "s/<glade-widget-group name=\"UniSet\" title=\"\(.*\)\">/<glade-widget-group name=\"UniSet\" title=\"$USER\'s uniwidgets\">/" /tmp/uniwidgets.xml.tmp

cp /tmp/uniwidgets.xml.tmp /usr/share/glade3/catalogs/uniwidgets.xml

rm -f /tmp/uniwidgets.xml.tmp

#scp -r plugins/galade/icons/16x16/ root@testing:/usr/share/glade3/pixmaps/hicolor/16x16/actions/
