#include <iostream>
#include <types.h>
#include "ADG.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define ADG_NODE        "node"
#define ADG_LINK_DI        "link-di"
#define ADG_AG_AI        "ag-ai"
#define ADG_KEY_ON_DI        "key-on-di"
#define ADG_KEY_ALARM_DI      "key-alarm-di"
#define ADG_WORK_DI        "adg-work-di"
#define ADG_OVERLOAD_DI        "adg-overload-di"
#define ADG_ALARM_DI        "adg-alarm-di"
#define ADG_WARNING_DI        "adg-warning-di"
#define ADG_BACK_AI        "adg-back-ai"
#define ADG_BACK_WIDTH        "back-width"
#define ADG_BACK_HEIGHT        "back-heigth"
#define X_AD          "x-ad"
#define X_AG          "x-ag"
#define Y_AD          "y-ad"
#define Y_AG          "y-ag"
#define W_H_AD          "w-h-ad"
#define W_H_AG          "w-h-ag"
#define X_GEN_STATES        "x-gen-states"
#define Y_GEN_STATES        "y-gen-states"
#define W_GEN_STATES        "w-gen-states"
#define H_GEN_STATES        "h-gen-states"
#define X_KEY          "x-key"
#define Y_KEY          "y-key"
#define W_KEY          "w-key"
#define H_KEY          "h-key"
#define SVG_PATH        "svg-path"
// -------------------------------------------------------------------------
#define INIT_ADG_PROPERTIES() \
  property_lock_view_generator(*this, "lock-view-generator" , false) \
  ,property_lock_view_diesel(*this, "lock-view-diesel" , false) \
  ,property_lock_view_key(*this, "lock-view-key" , false) \
  ,node(*this, ADG_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,svg_path(*this, SVG_PATH ,"/usr/share/yauza/19/svg/ses/") \
  ,ag_ai(*this, ADG_AG_AI , UniWidgetsTypes::DefaultObjectId) \
  ,property_link_di(*this, ADG_LINK_DI , UniWidgetsTypes::DefaultObjectId) \
  ,key_on_di(*this, ADG_KEY_ON_DI , UniWidgetsTypes::DefaultObjectId) \
  ,key_alarm_di(*this, ADG_KEY_ALARM_DI , UniWidgetsTypes::DefaultObjectId) \
  ,adg_work_di(*this, ADG_WORK_DI , UniWidgetsTypes::DefaultObjectId) \
  ,adg_overload_di(*this, ADG_OVERLOAD_DI , UniWidgetsTypes::DefaultObjectId) \
  ,adg_warn_di(*this, ADG_WARNING_DI , UniWidgetsTypes::DefaultObjectId) \
  ,adg_alarm_di(*this, ADG_ALARM_DI , UniWidgetsTypes::DefaultObjectId) \
  ,adg_back_ai(*this, ADG_BACK_AI , UniWidgetsTypes::DefaultObjectId) \
  ,back_width(*this, ADG_BACK_WIDTH, 150) \
  ,back_height(*this, ADG_BACK_HEIGHT, 169) \
  ,x_ad(*this, X_AD, 10) \
  ,x_ag(*this, X_AG, 10) \
  ,y_ad(*this, Y_AD, 45) \
  ,y_ag(*this, Y_AG, 110) \
  ,w_h_ad(*this, W_H_AD, 51) \
  ,w_h_ag(*this, W_H_AG, 50) \
  ,x_gen_states(*this, X_GEN_STATES, 63) \
  ,y_gen_states(*this, Y_GEN_STATES, 30) \
  ,w_gen_states(*this, W_GEN_STATES, 80) \
  ,h_gen_states(*this, H_GEN_STATES, 60) \
  ,x_key(*this, X_KEY, 29) \
  ,y_key(*this, Y_KEY, 167) \
  ,w_key(*this, W_KEY, 26) \
  ,h_key(*this, H_KEY, 51)
// -------------------------------------------------------------------------
/* Static definitions */
const string ADG::img_back_path = "adg_back.svg";
const string ADG::img_back_alarm_path = "adg_back_alarm.svg";

const string ADG::img_d_off_path = "adg_AD_off.svg";
const string ADG::img_d_on_path = "adg_AD_on.svg";
const string ADG::img_d_warn_path = "adg_AD_warn.svg";
const string ADG::img_d_alarm_path = "adg_AD_alarm.svg";

const string ADG::img_g_off_path = "adg_AG_off.svg";
const string ADG::img_g_on_path = "adg_AG_on.svg";
const string ADG::img_g_warn_path = "adg_AG_warn.svg";
const string ADG::img_g_alarm_path = "adg_AG_alarm.svg";

const string ADG::img_adg_work_path = "adg_work.svg";
const string ADG::img_adg_overload_path = "adg_overload.svg";

/*Keys in the chain*/
const string ADG::img_key_off_path = "gdg_key_off.svg";
const string ADG::img_key_on_path = "gdg_key_on.svg";
const string ADG::img_key_alarm_path = "gdg_key_alarm.svg";
const string ADG::img_key_undef_path = "gdg_key_undef.svg";
const string ADG::img_key_undef_off_path = "gdg_key_undef_off.svg";

const string ADG::img_adg_off_path = "adg_off.svg";
const string ADG::img_states_background_path = "adg_states_background.svg";
// -------------------------------------------------------------------------
void ADG::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback<ADG>;

  is_configured = false;
  /*Добавляем к родительскому контейнеру с типом*/
  add_child( &state_back,typeObject );
  add_child( &state_key,typeObject );
  add_child( &state_D,typeObject );
  add_child( &state_G,typeObject );
  add_child( &generator_states,typeObject );

  add_child(&link_, typeLogic);
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  back_rect = new Gdk::Rectangle(0, 0, get_back_width(), get_back_height());
  D_rect = new Gdk::Rectangle(get_x_ad(), get_y_ad(), get_w_h_ad(), get_w_h_ad());
  G_rect = new Gdk::Rectangle(get_x_ag(), get_y_ag(), get_w_h_ag(), get_w_h_ag());
  gen_states_rect = new Gdk::Rectangle(get_x_gen_states(), get_y_gen_states(), get_w_gen_states(), get_h_gen_states());
  key_rect = new Gdk::Rectangle(get_x_key(), get_y_key(), get_w_key(), get_h_key());

  connect_property_changed("back_width", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("back_heigth", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("x_ad", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("y_ad", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("x_ag", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("y_ag", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("w_h_ad", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("y_ag", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("w_h_ag", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("x_gen_states", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("y_gen_states", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("w_gen_states", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("h_gen_states", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("x_key", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("y_key", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("w_key", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
  connect_property_changed("h_key", sigc::mem_fun(*this, &ADG::on_rectangle_changed));
}
// -------------------------------------------------------------------------
ADG::ADG() :
  Glib::ObjectBase("adg")
  ,INIT_ADG_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
ADG::ADG(SimpleObject::BaseObjectType* gobject) :
  SimpleObject(gobject)
  ,INIT_ADG_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
ADG::~ADG()
{
}
// -------------------------------------------------------------------------
void ADG::on_rectangle_changed()
{
  set_size_request(-1,-1);
  back_rect->set_x(0);
  back_rect->set_y(0);
  back_rect->set_width(get_back_width());
  back_rect->set_height(get_back_height());

  D_rect->set_x(get_x_ad());
  D_rect->set_y(get_y_ad());
  D_rect->set_width(get_w_h_ad());
  D_rect->set_height(get_w_h_ad());

  G_rect->set_x(get_x_ag());
  G_rect->set_y(get_y_ag());
  G_rect->set_width(get_w_h_ag());
  G_rect->set_height(get_w_h_ag());

  gen_states_rect->set_x(get_x_gen_states());
  gen_states_rect->set_y(get_y_gen_states());
  gen_states_rect->set_width(get_w_gen_states());
  gen_states_rect->set_height(get_h_gen_states());

  key_rect->set_x(get_x_key());
  key_rect->set_y(get_y_key());
  key_rect->set_width(get_w_key());
  key_rect->set_height(get_h_key());
  queue_resize();
}
// -------------------------------------------------------------------------
void ADG::on_realize()
{
  SimpleObject::on_realize();
  on_rectangle_changed();

  if ( is_configured )
    return;
  is_configured = true;
  on_configure();

}
// -------------------------------------------------------------------------
void ADG::on_connect() throw()
{
  SimpleObject::on_connect();
  on_rectangle_changed();

  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void ADG::on_configure()
{
  link_.set_link_di( get_property_link_di() );
  link_.set_node( get_node() );
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  
  /* Create background Object Reserv */
  state_back.set_rect(*back_rect);

  state_back.set_path(mOFF, get_svg_path() + img_back_path);
  state_back.set_path(mON, get_svg_path() + img_back_alarm_path);

  state_back.insert_mode(mOFF,-1,false);
  state_back.insert_mode(mON,-1,false);

  state_back.set_state_ai(get_adg_back_ai());
  state_back.set_node(get_node());
    
  state_back.configure();
  
  put(state_back,
      back_rect->get_x(),
      back_rect->get_y());



  /*Key*/
  state_key.set_rect(*key_rect);

  state_key.set_path(mOFF, get_svg_path() + img_key_off_path);
  state_key.set_path(mON, get_svg_path() + img_key_on_path);
  state_key.set_path(mALARM_HIGH, get_svg_path() + img_key_alarm_path);
  state_key.insert_mode(mOFF,-1,false);
  state_key.insert_mode(mON,-1,false);
  state_key.insert_mode(mALARM_HIGH,-1,true);

  state_key.set_mode_state_low(mON);
  state_key.set_state_ai_low(get_key_on_di());
  state_key.set_node_low(get_node());

  state_key.set_mode_state_high(mALARM_HIGH);
  state_key.set_state_ai_high(get_key_alarm_di());
  state_key.set_node_high(get_node());

  state_key.set_lock_view(get_property_lock_view_key());

  state_key.configure();

  /* The SimpleObject::on_realize will be called after this put */
  put(state_key,
      key_rect->get_x(),
      key_rect->get_y());

  /* Diesel */
  state_D.set_rect(*D_rect);

  state_D.set_path(mOFF, get_svg_path() + img_d_off_path);
  state_D.set_path(mON, get_svg_path() + img_d_on_path);
  state_D.set_path(mWARNING, get_svg_path() + img_d_warn_path);
  state_D.set_path(mALARM, get_svg_path() + img_d_alarm_path);

  state_D.insert_mode(mOFF,-1,false);
  state_D.insert_mode(mON,-1,false);
  state_D.insert_mode(mWARNING,-1,true);
  state_D.insert_mode(mALARM,-1,true);

  state_D.set_mode_state(mON);
  state_D.set_state_ai(get_adg_work_di());
  state_D.set_node(get_node());
  

  if(get_adg_warn_di() != UniWidgetsTypes::DefaultObjectId)
  {
    /*Image*/
    StateLogic *sd_warn = new StateLogic();
    sd_warn->set_mode( mWARNING );
    sd_warn->set_state_ai( get_adg_warn_di() );
    sd_warn->set_node( get_node() );
#if GTK_VERSION_GE(2,18)
    sd_warn->set_visible(false);
#else
    sd_warn->hide();
#endif
    state_D.add_child(sd_warn, typeLogic);
//     image_logics.insert(std::pair<long, StateLogic *>(2, sl2 ) );
  }

  if(get_adg_alarm_di() != UniWidgetsTypes::DefaultObjectId)
  {
    /*Image*/
    StateLogic *sd_alarm = new StateLogic();
    sd_alarm->set_mode( mALARM );
    sd_alarm->set_state_ai( get_adg_alarm_di() );
    sd_alarm->set_node( get_node() );
#if GTK_VERSION_GE(2,18)
    sd_alarm->set_visible(false);
#else
    sd_alarm->hide();
#endif
    state_D.add_child(sd_alarm, typeLogic);
//     image_logics.insert(std::pair<long, StateLogic *>(2, sl2 ) );
  }
  state_D.set_lock_view(get_property_lock_view_diesel());
  
  state_D.configure();

  /* The SimpleObject::on_realize will be called after this put */
  put(state_D,
      D_rect->get_x(),
      D_rect->get_y());

  /* Generator */
  state_G.set_rect(*G_rect);

  state_G.set_path(mOFF, get_svg_path() + img_g_off_path);
  state_G.set_path(mON, get_svg_path() + img_g_on_path);
  state_G.set_path(mWARNING, get_svg_path() + img_g_warn_path);
  state_G.set_path(mALARM, get_svg_path() + img_g_alarm_path);

  state_G.insert_mode(mOFF,-1,false);
  state_G.insert_mode(mON,-1,false);
  state_G.insert_mode(mWARNING,-1,true);
  state_G.insert_mode(mALARM,-1,true);

  state_G.set_mode_state(mON);
  state_G.set_state_ai(get_adg_work_di());
  state_G.set_node(get_node());

  if(get_adg_warn_di() != UniWidgetsTypes::DefaultObjectId)
  {
    /*Image*/
    StateLogic *sg_warn = new StateLogic();
    sg_warn->set_mode( mWARNING );
    sg_warn->set_state_ai( get_adg_warn_di() );
    sg_warn->set_node( get_node() );
#if GTK_VERSION_GE(2,18)
    sg_warn->set_visible(false);
#else
    sg_warn->hide();
#endif
    state_G.add_child(sg_warn, typeLogic);
//     image_logics.insert(std::pair<long, StateLogic *>(2, sl2 ) );
  }

  if(get_adg_alarm_di() != UniWidgetsTypes::DefaultObjectId)
  {
    /*Image*/
    StateLogic *sg_alarm = new StateLogic();
    sg_alarm->set_mode( mALARM );
    sg_alarm->set_state_ai( get_adg_alarm_di() );
    sg_alarm->set_node( get_node() );
#if GTK_VERSION_GE(2,18)
    sg_alarm->set_visible(false);
#else
    sg_alarm->hide();
#endif
    state_G.add_child(sg_alarm, typeLogic);
//     image_logics.insert(std::pair<long, StateLogic *>(2, sl2 ) );
  }
  
  state_G.configure();
  
  /* The SimpleObject::on_realize will be called after this put */
  put(state_G,
      G_rect->get_x(),
      G_rect->get_y());

  
  /* Generator states */
  generator_states.set_rect(*gen_states_rect);

  generator_states.set_path(mOFF, get_svg_path() + img_adg_off_path);
  generator_states.set_path(mWARNING_HIGH, get_svg_path() + img_adg_overload_path);
  generator_states.set_path(mON, get_svg_path() + img_adg_work_path);
  generator_states.set_path(mBACKGROUND, get_svg_path() + img_states_background_path);
  generator_states.insert_mode(mOFF,-1,false);
  generator_states.insert_mode(mON,-1,false);
  generator_states.insert_mode(mWARNING_HIGH,-1,true);

  generator_states.set_mode_state_high_warn(mWARNING_HIGH);
  generator_states.set_state_ai_high_warn(get_adg_overload_di());
  generator_states.set_node_high_warn(get_node());

  generator_states.set_mode_state_high_alarm(mON);
  generator_states.set_state_ai_high_alarm(get_adg_work_di());
  generator_states.set_node_high_alarm(get_node());

  generator_states.set_lock_view(get_property_lock_view_generator());

  generator_states.configure();
  
  /* The SimpleObject::on_realize will be called after this put */
  put(generator_states,
      gen_states_rect->get_x(),
      gen_states_rect->get_y());


  /* Show all objects */
  show();
  state_back.show();
  state_key.show();
  state_D.show();
  state_G.show();
  generator_states.show();
}
// -------------------------------------------------------------------------
