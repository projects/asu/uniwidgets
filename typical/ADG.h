#ifndef _ADG_H
#define _ADG_H
// -------------------------------------------------------------------------
#include <string>
#include <map>
#include <components/Image.h>
#include <objects/SimpleObject.h>
#include <objects/LinkLogic.h>
#include <global_macros.h>
#include "TypicalState.h"
#include "TypicalTwoState.h"
#include "TypicalFourState.h"
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой виджет АДГ.
 * \par
 * Виджет отображает работу Аварийного Дизель Генератора.
 * 
 * \n Пример АДГ: \n \n
 * \image html adg.png
*/
class ADG : public SimpleObject
{
public:
  ADG();
  explicit ADG(SimpleObject::BaseObjectType* gobject);
  virtual ~ADG();

  /* Constants */
  Gdk::Rectangle *back_rect;        /*!< координаты и размер рамки с фонов виджета */
  Gdk::Rectangle *D_rect;          /*!< координаты и размер дизеля */
  Gdk::Rectangle *G_rect;          /*!< координаты и размер генератора */
  Gdk::Rectangle *gen_states_rect;      /*!< координаты и размер надписи состояния генератора */
  Gdk::Rectangle *key_rect;        /*!< координаты и размер ГА */
  static const std::string img_back_path;      /*!< путь к картинке фона */
  static const std::string img_back_alarm_path;    /*!< путь к картинке фона(авария) */
  
  static const std::string img_d_off_path;      /*!< путь к картинке дизеля(отключен) */
  static const std::string img_d_on_path;      /*!< путь к картинке дизеля(включен) */
  static const std::string img_d_warn_path;      /*!< путь к картинке дизеля(предупреждение) */
  static const std::string img_d_alarm_path;      /*!< путь к картинке дизеля(авария) */

  static const std::string img_g_off_path;      /*!< путь к картинке генератора(отключен) */
  static const std::string img_g_on_path;      /*!< путь к картинке генератора(включен) */
  static const std::string img_g_warn_path;      /*!< путь к картинке генератора(предупреждение) */
  static const std::string img_g_alarm_path;      /*!< путь к картинке генератора(авария) */

  static const std::string img_adg_work_path;      /*!< путь к картинке состояние АДГ(работает) */
  static const std::string img_adg_overload_path;    /*!< путь к картинке состояние АДГ(перегрузка) */

  static const std::string img_key_off_path;      /*!< путь к картинке ГА(выключен) */
  static const std::string img_key_on_path;      /*!< путь к картинке ГА(включен) */
  static const std::string img_key_alarm_path;      /*!< путь к картинке ГА(авария) */
  static const std::string img_key_undef_path;      /*!< путь к картинке ГА(неопределенное состояние) */
  static const std::string img_key_undef_off_path;    /*!< путь к картинке ГА(неопределенное состояние)(картинка для мигания) */

  static const std::string img_adg_off_path;      /*!< путь к картинке состояние АДГ(выключен) */
  static const std::string img_states_background_path;    /*!< путь к картинке состояние АДГ(фон надписей) */

protected:
  /* Handlers */
  virtual void on_realize();
  virtual void on_connect() throw();

private:
  /* Variables */
  TypicalState state_back;
  TypicalTwoState state_key;
  TypicalState state_D;
  TypicalState state_G;
  TypicalFourState generator_states;
  LinkLogic link_;
 
  bool is_configured;

  /* Methods */
  void constructor();
  void on_rectangle_changed();
  void on_configure();

  DISALLOW_COPY_AND_ASSIGN(ADG);

  /* Properties */
  ADD_PROPERTY( property_lock_view_generator, bool )    /*!< свойство: блокировка экрана по АПС от генератора */
  ADD_PROPERTY( property_lock_view_diesel, bool )      /*!< свойство: блокировка экрана по АПС от дизеля */
  ADD_PROPERTY( property_lock_view_key, bool )      /*!< свойство: блокировка экрана по АПС от ГА */
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )      /*!< свойство: id узла */
  ADD_PROPERTY( svg_path, Glib::ustring )        /*!< свойство: путь к директории с картинкам */
  ADD_PROPERTY( ag_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика состояния АДГ */
  ADD_PROPERTY( property_link_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика состояния связи с SharedMemory */
  ADD_PROPERTY( key_on_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика ГА включен */
  ADD_PROPERTY( key_alarm_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика ГА сработала защита */
  ADD_PROPERTY( adg_work_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика АДГ работает */
  ADD_PROPERTY( adg_overload_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика перегрузка АДГ */
  ADD_PROPERTY( adg_warn_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика АДГ предупреждение */
  ADD_PROPERTY( adg_alarm_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика АДГ авария */
  ADD_PROPERTY( adg_back_ai, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика состояния АДГ */
  ADD_PROPERTY( back_width, long )        /*!< свойство: ширина виджета */
  ADD_PROPERTY( back_height, long )        /*!< свойство: высота виджета */
  ADD_PROPERTY( x_ad, long )          /*!< свойство: x-координата дизеля */
  ADD_PROPERTY( x_ag, long )          /*!< свойство: x-координата генератора */
  ADD_PROPERTY( y_ad, long )          /*!< свойство: y-координата дизеля */
  ADD_PROPERTY( y_ag, long )          /*!< свойство: y-координата генератора */
  ADD_PROPERTY( w_h_ad, long )          /*!< свойство: размер(сторона квадрата) дизеля */
  ADD_PROPERTY( w_h_ag, long )          /*!< свойство: размер(сторона квадрата) генератора */
  ADD_PROPERTY( x_gen_states, long )        /*!< свойство: x-координата надписи состояния генератора */
  ADD_PROPERTY( y_gen_states, long )        /*!< свойство: y-координата надписи состояния генератора */
  ADD_PROPERTY( w_gen_states, long )        /*!< свойство: ширина надписи состояния генератора */
  ADD_PROPERTY( h_gen_states, long )        /*!< свойство: высота надписи состояния генератора */
  ADD_PROPERTY( x_key, long )          /*!< свойство: x-координата ГА */
  ADD_PROPERTY( y_key, long )          /*!< свойство: y-координата ГА */
  ADD_PROPERTY( w_key, long )          /*!< свойство: ширина ГА */
  ADD_PROPERTY( h_key, long )          /*!< свойство: высота ГА */

};

}
#endif
