#include <iostream>
#include <types.h>
#include "AbstractTypical.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void AbstractTypical::constructor()
{
	/*Приоритеты для картинок. Необходимы для правильного
	 *отобраения наиболее приоритетного состояния виджета*/
	set_priority(mOFF, 0);
	set_priority(mON, 2);
	set_priority(mPROTECTION, 6);
	set_priority(mINIT, 4);
	set_priority(mTRANSITIVE, 4);
	set_priority(mSLEEP, 0);
	set_priority(mWARNING, 5);
	set_priority(mALARM, 6);
	set_priority(mREADY, 1);
	set_priority(mRunning, 2);
	set_priority(mUNKNOWN, 7);
}
// -------------------------------------------------------------------------
AbstractTypical::AbstractTypical() :
	Glib::ObjectBase("abstracttypical")
{
	constructor();
}
// -------------------------------------------------------------------------
AbstractTypical::AbstractTypical(SimpleObject::BaseObjectType* gobject) :
	SimpleObject(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
AbstractTypical::~AbstractTypical()
{
}
// -------------------------------------------------------------------------
int AbstractTypical::get_priority(int mode)
{
	if( !mode_priorities_.count(mode) )
		return -1;
	return mode_priorities_[mode];
}
// -------------------------------------------------------------------------
void AbstractTypical::set_priority(int mode, int priority)
{
	ModePriorityMap::iterator it = mode_priorities_.find(mode);

	if( it != mode_priorities_.end() )
		mode_priorities_.erase(mode);

	pair<ModePriorityMap::iterator, bool> ret;
	ret = mode_priorities_.insert(ModePriorityPair(mode, priority));

	assert(ret.second == true);
}
// -------------------------------------------------------------------------
