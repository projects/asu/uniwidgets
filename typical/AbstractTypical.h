#ifndef _ABSTRACTTYPICAL_H
#define _ABSTRACTTYPICAL_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <objects/SimpleObject.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Базовый класс для типовых объектов.
 * \par
 * Базовый класс для всех типовых виджетов. Типовой - это виджет состоящий из
 * нескольких простых частей скомпанованных в один большой виджет для выполнения
 * одной задачи. Пример такого виджета: цистерна, индикатор с порогами, ГДГ и т.д.
 * Например. цистерна состоит из рамки цистерны со шкалой и заполнителем, цифрового
 * индикатора уровня, рамки для цифрового индикатора и порогов для цистерны нижнего
 * и верхнего(если они предусмотрены). Если описывать такой виджет из простых
 * компонентов в glade файле, то файл получится не только большим, но еще и трудно
 * настраиваемым т.к. разные компоненоты виджета могут обрабатывать одни и те же
 * датчики и при смене этих джатчиков прийдется менять их в нескольких местах, что
 * пораждает ошибки. Удобнее и надежнее задавать датчик в одном месте, а в реализации
 * типового виджета производить настройку компонентов. Вместо длинноого glade файла
 * получается гораздо менее громоздкий и более читабельный.
 * Основная идея состоит в том, что бы выполнить всю работу по размещению компонентов
 * виджета и настройку их свойств в коде.
*/
class AbstractTypical : public SimpleObject
{
public:
	AbstractTypical();
	explicit AbstractTypical(SimpleObject::BaseObjectType* gobject);
	virtual ~AbstractTypical();

	/* Methods */
	/*! получить приоритет для режима(режим соответствует картинкам; логика при обработке датчика выставляет
	    определенный режим, соответствующий значению датчика; этот режим соответствует какой-то картинке и у
	    каждого режима есть свой приоритет) */
	int get_priority(int mode);				
	void set_priority(int mode, int priority);		/*!< установить приоритет для режима */
	virtual void set_lock_view(const bool lock) {}		/*!< установить блокировку экрана при срабатывании датчика АПС */

private:
	/* Types */
	typedef std::pair<int, int> ModePriorityPair;
	typedef std::unordered_map<int, int> ModePriorityMap;

	/* Variables */
	ModePriorityMap mode_priorities_;

	/* Methods */
	void constructor();

	DISALLOW_COPY_AND_ASSIGN(AbstractTypical);
};

}
#endif
