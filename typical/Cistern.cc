#include <iostream>
#include <objects/LinkLogic.h>
#include <types.h>
#include "TypicalState.h"
#include "TypicalText.h"
#include "TypicalIndicator.h"
#include "TypicalCisternBlink.h"
#include "TypicalTwoState.h"
#include "TypicalIndicatorBlink.h"
#include "Cistern.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define CISTERN_STATE_AI      "state-ai"
#define CISTERN_CUBE_AI        "cube-ai"
#define CISTERN_LINK_DI        "link-di"
#define CISTERN_SELF_STATE_AI      "self-state-ai"
#define CISTERN_THRESHOLDHIGH_AI    "threshold-high-ai"
#define CISTERN_THRESHOLDLOW_AI      "threshold-low-ai"
#define CISTERN_BACKGROUND_OFF_PATH    "cistern-background-off-path"
#define CISTERN_BACKGROUND_WARN_PATH    "cistern-background-warn-path"
#define CISTERN_BACKGROUND_ALARM_PATH    "cistern-background-alarm-path"
#define CISTERN_FILLING_PATH      "cistern-filling-path"
#define CISTERN_SCALE_PATH      "cistern-scale-path"
#define CISTERN_HIGH_LEVEL_OFF_PATH    "high-level-off-path"
#define CISTERN_HIGH_LEVEL_ALARM_PATH    "high-level-alarm-path"
#define CISTERN_LOW_LEVEL_OFF_PATH    "low-level-off-path"
#define CISTERN_LOW_LEVEL_ALARM_PATH    "low-level-alarm-path"
#define CISTERN_NODE        "node"
#define CISTERN_SCALE_SWITCH      "cistern-scale-switch"
#define CISTERN_INDICATOR_PATH      "cistern_indicator_path"
#define W_CISTERN        "w-cistern"
#define H_CISTERN        "h-cistern"
#define X_STATES        "x-states"
#define Y_STATE_HIGH        "y-state_high"
#define Y_STATE_LOW        "y-state_low"
#define W_STATES        "w-states"
#define H_STATES        "h-states"
#define X_INDICATOR        "x-indicator"
#define Y_INDICATOR        "y-indicator"
#define W_INDICATOR        "w-indicator"
#define H_INDICATOR        "h-indicator"
#define CISTERN_HIGH_ALARM_PATH      "cistern-high-alarm-path"
#define CISTERN_LOW_ALARM_PATH      "cistern-low-alarm-path"
#define CISTERN_INDICATOR_ALARM_PATH    "cistern-indicator-alarm-path"
#define CISTERN_FONT        "indicator-font"
#define CISTERN_FONT_COLOR      "indicator-font-color"
#define CISTERN_FACTOR        "factor"
#define CISTERN_PRECISION           "indicator-precision"
#define CISTERN_DIGITS              "indicator-digits"
#define CISTERN_HIGH_LEVEL      "cistern-high-level"
#define CISTERN_LOCK_VIEW_HIGH      "cistern-lock-view-high"
#define CISTERN_LOCK_VIEW_LOW      "cistern-lock-view-low"
#define CISTERN_STATE_INDICATOR      "state-indicator"
/*Text of number for level(High, low)*/
#define CISTERN_FONT_SIZE      "font-size"
#define CISTERN_FONT_COLOR_OFF      "font-color-off-high"
#define CISTERN_FONT_SHADOW_ON      "font-shadow-on-high"
#define CISTERN_FONT_SHADOW_OFF      "font-shadow-off-high"

#define CISTERN_NUM_TEXT_HIGH      "num-text-high"
#define CISTERN_FONT_COLOR_ON_HIGH    "font-color-on-high"
#define CISTERN_X_TEXTNUM_HIGH      "x-text-high-number"
#define CISTERN_Y_TEXTNUM_HIGH      "y-text-high-number"

#define CISTERN_NUM_TEXT_LOW      "num-text-low"
#define CISTERN_FONT_COLOR_ON_LOW    "font-color-on-low"
#define CISTERN_X_TEXTNUM_LOW      "x-text-low-number"
#define CISTERN_Y_TEXTNUM_LOW      "y-text-low-number"

#define CISTERN_MODE_LOGIC_ON_HIGH    "mode-logic-high"
#define CISTERN_MODE_ON_HIGH      "mode-on-high-type"
#define CISTERN_PRIORITY_ON_HIGH    "priority-on-high"
#define CISTERN_MODE_LOGIC_ON_LOW    "mode-logic-low"
#define CISTERN_MODE_ON_LOW      "mode-on-low-type"
#define CISTERN_PRIORITY_ON_LOW      "priority-on-low"
// -------------------------------------------------------------------------
#define INIT_CISTERN_PROPERTIES() \
  cistern_lock_view_high(*this, CISTERN_LOCK_VIEW_HIGH, false) \
  ,cistern_lock_view_low(*this, CISTERN_LOCK_VIEW_LOW, false) \
  ,state_ai(*this, CISTERN_STATE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,node(*this, CISTERN_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,cube_ai(*this, CISTERN_CUBE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,property_link_di(*this, CISTERN_LINK_DI , UniWidgetsTypes::DefaultObjectId) \
  ,self_state_ai(*this, CISTERN_SELF_STATE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,threshold_high_ai(*this, CISTERN_THRESHOLDHIGH_AI , UniWidgetsTypes::DefaultObjectId) \
  ,cistern_high_level(*this, CISTERN_HIGH_LEVEL , 100) \
  ,threshold_low_ai(*this, CISTERN_THRESHOLDLOW_AI , UniWidgetsTypes::DefaultObjectId) \
  \
  ,mode_logic_on_high(*this, CISTERN_MODE_LOGIC_ON_HIGH , UniWidgetsTypes::DefaultObjectId) \
  ,mode_on_high_type(*this, CISTERN_MODE_ON_HIGH , mWARNING_type) \
  ,priority_on_high(*this, CISTERN_PRIORITY_ON_HIGH , 1) \
  ,mode_logic_on_low(*this, CISTERN_MODE_LOGIC_ON_LOW , UniWidgetsTypes::DefaultObjectId) \
  ,mode_on_low_type(*this, CISTERN_MODE_ON_LOW , mWARNING_type) \
  ,priority_on_low(*this, CISTERN_PRIORITY_ON_LOW , 1) \
  \
  ,cistern_background_off_path(*this, CISTERN_BACKGROUND_OFF_PATH ) \
  ,cistern_background_warn_path(*this, CISTERN_BACKGROUND_WARN_PATH ) \
  ,cistern_background_alarm_path(*this, CISTERN_BACKGROUND_ALARM_PATH ) \
  ,cistern_filling_path(*this, CISTERN_FILLING_PATH ) \
  ,cistern_scale_path(*this, CISTERN_SCALE_PATH ) \
  ,cistern_scale_switch(*this, CISTERN_SCALE_SWITCH, false) \
  ,high_level_off_path(*this, CISTERN_HIGH_LEVEL_OFF_PATH ) \
  ,high_level_alarm_path(*this, CISTERN_HIGH_LEVEL_ALARM_PATH ) \
  ,low_level_alarm_path(*this, CISTERN_LOW_LEVEL_ALARM_PATH ) \
  ,low_level_off_path(*this, CISTERN_LOW_LEVEL_OFF_PATH ) \
  ,cistern_indicator_path(*this, CISTERN_INDICATOR_PATH ) \
  ,cistern_indicator_alarm_path(*this, CISTERN_INDICATOR_ALARM_PATH ) \
  ,cistern_high_alarm_path(*this, CISTERN_HIGH_ALARM_PATH ) \
  ,cistern_low_alarm_path(*this, CISTERN_LOW_ALARM_PATH ) \
  ,indicator_font(*this, CISTERN_FONT, "") \
  ,indicator_font_color(*this, CISTERN_FONT_COLOR, Gdk::Color("green")) \
  ,indicator_precision(*this, CISTERN_PRECISION, 0) \
  ,indicator_digits(*this, CISTERN_DIGITS, 3) \
  ,property_factor(*this, CISTERN_FACTOR, 1) \
  \
  ,w_cistern(*this, W_CISTERN , 96) \
  ,h_cistern(*this, H_CISTERN , 165) \
  ,x_states(*this, X_STATES , 54) \
  ,y_state_high(*this, Y_STATE_HIGH , 30) \
  ,y_state_low(*this, Y_STATE_LOW , 62) \
  ,w_states(*this, W_STATES , 36) \
  ,h_states(*this, H_STATES , 31) \
  ,x_indicator(*this, X_INDICATOR , 50) \
  ,y_indicator(*this, Y_INDICATOR , 120) \
  ,w_indicator(*this, W_INDICATOR , 64) \
  ,h_indicator(*this, H_INDICATOR , 21) \
  \
  ,font_size(*this, CISTERN_FONT_SIZE , 16) \
  ,font_shadow_on(*this, CISTERN_FONT_SHADOW_ON , false) \
  ,font_shadow_off(*this, CISTERN_FONT_SHADOW_OFF , false) \
  ,font_color_off(*this, CISTERN_FONT_COLOR_OFF , Gdk::Color("grey")) \
  \
  ,num_text_high(*this, CISTERN_NUM_TEXT_HIGH , "00.00") \
  ,font_color_on_high(*this, CISTERN_FONT_COLOR_ON_HIGH , Gdk::Color("yellow")) \
  ,x_num_text_high_rect(*this, CISTERN_X_TEXTNUM_HIGH , 0) \
  ,y_num_text_high_rect(*this, CISTERN_Y_TEXTNUM_HIGH , 0) \
  \
  ,num_text_low(*this, CISTERN_NUM_TEXT_LOW , "00.00") \
  ,font_color_on_low(*this, CISTERN_FONT_COLOR_ON_LOW , Gdk::Color("yellow")) \
  ,x_num_text_low_rect(*this, CISTERN_X_TEXTNUM_LOW , 0) \
  ,y_num_text_low_rect(*this, CISTERN_Y_TEXTNUM_LOW , 0)
// -------------------------------------------------------------------------
void Cistern::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback<Cistern>;

  indicator_ = new TypicalIndicatorBlink();
  cistern_ = new TypicalCisternBlink();
  state_h_ = new TypicalState();
  state_l_ = new TypicalState();
  frame_ = new TypicalTwoState();
  n_text_h_ = new TypicalText();
  n_text_l_ = new TypicalText();
  link_ = new LinkLogic();

  is_configured = false;
  /*Добавляем к родительскому контейнеру с типом*/
  add_child( indicator_,typeObject );
  add_child( cistern_,typeObject );
  add_child( state_h_,typeObject );
  add_child( state_l_,typeObject );
  add_child( frame_,typeObject );
  add_child( n_text_h_,typeObject );
  add_child( n_text_l_,typeObject );
  add_child(link_, typeLogic);
#if GTK_VERSION_GE(2,18)
  link_->set_visible(false);
#else
  link_->hide();
#endif
  cistern_rect = new Gdk::Rectangle(0, 0, get_w_cistern(), get_h_cistern());
  state_h_rect = new Gdk::Rectangle(get_x_states(), get_y_state_high(), get_w_states(), get_h_states());
  state_l_rect = new Gdk::Rectangle(get_x_states(), get_y_state_low(), get_w_states(), get_h_states());
  indicator_rect = new Gdk::Rectangle(get_x_indicator(), get_y_indicator(), get_w_indicator(), get_h_indicator());
  n_text_h_rect = new Gdk::Rectangle(get_x_num_text_high_rect(), get_y_num_text_high_rect(), -1, -1);
  n_text_l_rect = new Gdk::Rectangle(get_x_num_text_low_rect(), get_y_num_text_low_rect(), -1, -1);

  connect_property_changed("w-cistern", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("h-cistern", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("x-states", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("y-state_high", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("y-state_low", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("w-states", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("h-states", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("x-indicator", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("y-indicator", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("w-indicator", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed("h-indicator", sigc::mem_fun(*this, &Cistern::on_rectangle_changed));

  connect_property_changed(CISTERN_X_TEXTNUM_HIGH, sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed(CISTERN_Y_TEXTNUM_HIGH, sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed(CISTERN_X_TEXTNUM_LOW, sigc::mem_fun(*this, &Cistern::on_rectangle_changed));
  connect_property_changed(CISTERN_Y_TEXTNUM_LOW, sigc::mem_fun(*this, &Cistern::on_rectangle_changed));

  connect_property_changed(CISTERN_NUM_TEXT_HIGH, sigc::bind(sigc::mem_fun(*this, &Cistern::on_text_changed),n_text_h_,&num_text_high));
  connect_property_changed(CISTERN_NUM_TEXT_LOW, sigc::bind(sigc::mem_fun(*this, &Cistern::on_text_changed), n_text_l_,&num_text_low));
}
// -------------------------------------------------------------------------
Cistern::Cistern() :
  Glib::ObjectBase("cistern")
  ,INIT_CISTERN_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
Cistern::Cistern(SimpleObject::BaseObjectType* gobject) :
  SimpleObject(gobject)
  ,INIT_CISTERN_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
Cistern::~Cistern()
{
  delete cistern_;
  delete state_h_;
  delete state_l_;
  delete frame_;
  delete n_text_h_;
  delete n_text_l_;
  delete link_;
  delete indicator_;
}
// -------------------------------------------------------------------------
void Cistern::set_rectangle(Gdk::Rectangle* rect_,const long x, const long y, const long w, const long h)
{
  rect_->set_x(x);
  rect_->set_y(y);
  rect_->set_width(w);
  rect_->set_height(h);
}
// -------------------------------------------------------------------------
void Cistern::on_rectangle_changed()
{
  set_size_request(-1,-1);
  set_rectangle(cistern_rect,0,0,get_w_cistern(),get_h_cistern());
  set_rectangle(state_h_rect,get_x_states(),get_y_state_high(),get_w_states(),get_h_states());
  set_rectangle(state_l_rect,get_x_states(),get_y_state_low(),get_w_states(),get_h_states());
  set_rectangle(indicator_rect,get_x_indicator(),get_y_indicator(),get_w_indicator(),get_h_indicator());
  set_rectangle(n_text_h_rect,get_x_num_text_high_rect(), get_y_num_text_high_rect(), -1, -1);
  set_rectangle(n_text_l_rect,get_x_num_text_low_rect(), get_y_num_text_low_rect(), -1, -1);

  if(is_configured)
  {
    Gtk::Widget *w;

    if( get_threshold_high_ai() != UniWidgetsTypes::DefaultObjectId )
    {
      w = dynamic_cast<Gtk::Widget* >(state_h_);
      move(*w,get_x_states(),get_y_state_high());

      w = dynamic_cast<Gtk::Widget* >(n_text_h_);
      move(*w,get_x_num_text_high_rect(), get_y_num_text_high_rect());
    }

    if( get_threshold_low_ai() != UniWidgetsTypes::DefaultObjectId )
    {
      w = dynamic_cast<Gtk::Widget* >(state_l_);
      move(*w,get_x_states(),get_y_state_low());

      w = dynamic_cast<Gtk::Widget* >(n_text_l_);
      move(*w,get_x_num_text_low_rect(), get_y_num_text_low_rect());
    }
    w = dynamic_cast<Gtk::Widget* >(indicator_);
    move(*w,get_x_indicator(),get_y_indicator());
  }

  queue_resize();
}
// -------------------------------------------------------------------------
void Cistern::on_text_changed(TypicalText *tx, const Glib::Property<Glib::ustring> *proper)
{
  for(std::vector<Text *>::iterator it = tx->texts_.begin();it != tx->texts_.end(); it++)
    (*it)->set_property_text_(proper->get_value());
  queue_resize();
}
// -------------------------------------------------------------------------
void Cistern::on_realize()
{
  SimpleObject::on_realize();
  on_rectangle_changed();

  /* FIXME: Use another event for this initialization.
   * on_realize() is called twice in glade-3 editor */
  if ( is_configured )
    return;

  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void Cistern::on_connect() throw()
{
  SimpleObject::on_connect();
  on_rectangle_changed();

  if ( is_configured )
    return;

  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void Cistern::on_configure()
{
  link_->set_link_di( get_property_link_di() );
  link_->set_node( get_node() );
#if GTK_VERSION_GE(2,18)
  link_->set_visible(false);
#else
  link_->hide();
#endif

  cistern_->set_rect(*cistern_rect);
  cistern_->set_background_off_path( get_cistern_background_off_path());
  cistern_->set_background_warn_path( get_cistern_background_warn_path());
  cistern_->set_background_alarm_path( get_cistern_background_alarm_path());
  cistern_->set_filling_path( get_cistern_filling_path());
  cistern_->set_scale_path( get_cistern_scale_path());
  cistern_->set_scale_switch_on(get_cistern_scale_switch());
  cistern_->set_high_alarm( get_cistern_high_alarm_path());
  cistern_->set_low_alarm( get_cistern_low_alarm_path());

  cistern_->set_state_di_statelow_warn(get_threshold_low_ai());
  cistern_->set_state_di_statehigh_warn(get_threshold_high_ai());
  cistern_->set_cistern_high_level(get_cistern_high_level());

  if(get_mode_on_high_type() == mWARNING_type )
  {
    cistern_->set_mode_statehigh_warn(mWARNING);
  }
  else if(get_mode_on_high_type() == mALARM_type )
  {
    cistern_->set_mode_statehigh_warn(mALARM);
  }

  if(get_mode_on_low_type() == mWARNING_type )
  {
    cistern_->set_mode_statelow_warn(mWARNING);
  }
  else if(get_mode_on_low_type() == mALARM_type )
  {
    cistern_->set_mode_statelow_warn(mALARM);
  }

  cistern_->set_value_ai(get_state_ai());
  cistern_->set_node(get_node());
  
  cistern_->set_state_obj(get_self_state_ai());
  cistern_->configure();
  /* The SimpleObject::on_realize will be called after this put */
  put(*cistern_,
      cistern_rect->get_x(),
      cistern_rect->get_y());
  cistern_->show();
  /*Frame of indicator*/
  frame_->set_rect(*indicator_rect);
  frame_->set_path(mOFF,  get_cistern_indicator_path());
  frame_->insert_mode(mOFF,-1,false);
  if( get_threshold_high_ai() != UniWidgetsTypes::DefaultObjectId )
  {
    if(get_mode_on_high_type() == mWARNING_type )
    {
      frame_->set_path(mWARNING_HIGH,  get_cistern_indicator_alarm_path());
      frame_->insert_mode(mWARNING_HIGH,-1,true);
      frame_->set_mode_state_high(mWARNING_HIGH);
      frame_->set_state_ai_high(get_threshold_high_ai());
      frame_->set_node_high(get_node());
    }
    else if(get_mode_on_high_type() == mALARM_type )
    {
      frame_->set_path(mALARM_HIGH,  get_cistern_indicator_alarm_path());
      frame_->insert_mode(mALARM_HIGH,-1,true);
      frame_->set_mode_state_high(mALARM_HIGH);
      frame_->set_state_ai_high(get_threshold_high_ai());
      frame_->set_node_high(get_node());
    }
  }

  if( get_threshold_low_ai() != UniWidgetsTypes::DefaultObjectId )
  {
    
    if(get_mode_on_low_type() == mWARNING_type )
    {
      frame_->set_path(mWARNING_LOW,  get_cistern_indicator_alarm_path());
      frame_->insert_mode(mWARNING_LOW,-1,true);
      frame_->set_mode_state_low(mWARNING_LOW);
      frame_->set_state_ai_low(get_threshold_low_ai());
      frame_->set_node_low(get_node());
    }
    else if(get_mode_on_low_type() == mALARM_type )
    {
      frame_->set_path(mALARM_LOW,  get_cistern_indicator_alarm_path());
      frame_->insert_mode(mALARM_LOW,-1,true);
      frame_->set_mode_state_low(mALARM_LOW);
      frame_->set_state_ai_low(get_threshold_low_ai());
      frame_->set_node_low(get_node());
    }
  }

  frame_->configure();

  /* The SimpleObject::on_realize will be called after this put */
  put(*frame_,
      indicator_rect->get_x(),
      indicator_rect->get_y());
  frame_->show();
  /* Thresholds of cistern. high*/
  if(get_threshold_high_ai() != UniWidgetsTypes::DefaultObjectId)
  {
    state_h_->set_rect(*state_h_rect);
    state_h_->set_path(mOFF,  get_high_level_off_path());
    state_h_->insert_mode(mOFF,-1,false);
    if(get_mode_on_high_type() == mWARNING_type )
    {
      state_h_->set_path(mWARNING,  get_high_level_alarm_path());
      state_h_->insert_mode(mWARNING,-1,true);
      state_h_->set_mode_state(mWARNING);
    }
    else if(get_mode_on_high_type() == mALARM_type )
    {
      state_h_->set_path(mALARM,  get_high_level_alarm_path());
      state_h_->insert_mode(mALARM,-1,true);
      state_h_->set_mode_state(mALARM);
    }
    state_h_->set_state_ai(get_threshold_high_ai());
    state_h_->set_lock_view(get_cistern_lock_view_high());
    state_h_->set_node(get_node());
  
    state_h_->configure();
  
    /* The SimpleObject::on_realize will be called after this put */
    put(*state_h_,
        state_h_rect->get_x(),
        state_h_rect->get_y());
    state_h_->show();

    /*Configure NUM TEXT/OFF */
    n_text_h_->set_rect( n_text_h_rect );
    text_prop *n_tp_h_off = new text_prop();
    n_tp_h_off->mode=0;
    n_tp_h_off->priority=0;
    n_tp_h_off->text=get_num_text_high();
    n_tp_h_off->fsize=get_font_size();
    n_tp_h_off->color_on = get_font_color_off();
    n_tp_h_off->color_off = get_font_color_off();
    n_tp_h_off->bstate=false;
    n_tp_h_off->shadow = get_font_shadow_off();
    n_tp_h_off->al = Pango::ALIGN_CENTER;
    n_text_h_->add_mode_text(n_tp_h_off);

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_h_on = new text_prop();

    if(get_mode_on_high_type() == mWARNING_type )
      n_tp_h_on->mode=mWARNING;
    else if(get_mode_on_high_type() == mALARM_type )
      n_tp_h_on->mode=mALARM;

    n_tp_h_on->priority=get_priority_on_high();
    n_tp_h_on->text=get_num_text_high();
    n_tp_h_on->fsize=get_font_size();
    n_tp_h_on->color_on = get_font_color_on_high();
    n_tp_h_on->color_off = get_font_color_off();
    n_tp_h_on->bstate = true;
    n_tp_h_on->shadow = get_font_shadow_on();
    n_tp_h_on->al = Pango::ALIGN_CENTER;
    n_text_h_->add_mode_text( n_tp_h_on );

    n_text_h_->set_textlogic_state_ai( get_threshold_high_ai() );
    n_text_h_->set_textlogic_mode( get_mode_logic_on_high() );
    n_text_h_->set_node( get_node() );
    n_text_h_->configure();
    put( *n_text_h_,
        n_text_h_rect->get_x(),
        n_text_h_rect->get_y() );

    n_text_h_->show();
  }
  /* low */
  if(get_threshold_low_ai() != UniWidgetsTypes::DefaultObjectId)
  {
    state_l_->set_rect(*state_l_rect);
    state_l_->set_path(mOFF,  get_low_level_off_path());
    state_l_->insert_mode(mOFF,-1,false);
    if(get_mode_on_low_type() == mWARNING_type )
    {
      state_l_->set_path(mWARNING,  get_low_level_alarm_path());
      state_l_->insert_mode(mWARNING,-1,true);
      state_l_->set_mode_state(mWARNING);
    }
    else if(get_mode_on_low_type() == mALARM_type )
    {
      state_l_->set_path(mALARM,  get_low_level_alarm_path());
      state_l_->insert_mode(mALARM,-1,true);
      state_l_->set_mode_state(mALARM);
    }
    state_l_->set_state_ai(get_threshold_low_ai());
    state_l_->set_lock_view(get_cistern_lock_view_low());

    state_l_->set_node(get_node());
  
    state_l_->configure();
  
    /* The SimpleObject::on_realize will be called after this put */
    put(*state_l_,
        state_l_rect->get_x(),
        state_l_rect->get_y());
    state_l_->show();

    /*Configure NUM TEXT/OFF */
    n_text_l_->set_rect( n_text_l_rect );
    text_prop *n_tp_l_off = new text_prop();
    n_tp_l_off->mode=0;
    n_tp_l_off->priority=0;
    n_tp_l_off->text=get_num_text_low();
    n_tp_l_off->fsize=get_font_size();
    n_tp_l_off->color_on = get_font_color_off();
    n_tp_l_off->color_off = get_font_color_off();
    n_tp_l_off->bstate=false;
    n_tp_l_off->shadow = get_font_shadow_off();
    n_tp_l_off->al = Pango::ALIGN_CENTER;
    n_text_l_->add_mode_text(n_tp_l_off);

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_l_on = new text_prop();

    if(get_mode_on_low_type() == mWARNING_type )
      n_tp_l_on->mode=mWARNING;
    else if(get_mode_on_low_type() == mALARM_type )
      n_tp_l_on->mode=mALARM;

    n_tp_l_on->priority=get_priority_on_low();
    n_tp_l_on->text=get_num_text_low();
    n_tp_l_on->fsize=get_font_size();
    n_tp_l_on->color_on = get_font_color_on_low();
    n_tp_l_on->color_off = get_font_color_off();
    n_tp_l_on->bstate = true;
    n_tp_l_on->shadow = get_font_shadow_on();
    n_tp_l_on->al = Pango::ALIGN_CENTER;
    n_text_l_->add_mode_text( n_tp_l_on );

    n_text_l_->set_textlogic_state_ai( get_threshold_low_ai() );
    n_text_l_->set_textlogic_mode( get_mode_logic_on_low() );
    n_text_l_->set_node( get_node() );
    n_text_l_->configure();
    put( *n_text_l_,
        n_text_l_rect->get_x(),
        n_text_l_rect->get_y() );

    n_text_l_->show();
  }

  /*Indicator 1*/
  indicator_->set_rect(*indicator_rect);
  indicator_->set_state_di_colorlow_warn(get_threshold_low_ai());
  indicator_->set_state_di_colorhigh_warn(get_threshold_high_ai());
  if(get_mode_on_high_type() == mWARNING_type )
    indicator_->set_mode_colorhigh_warn(mWARNING);
  else if(get_mode_on_high_type() == mALARM_type )
    indicator_->set_mode_colorhigh_warn(mALARM);

  if(get_mode_on_low_type() == mWARNING_type )
    indicator_->set_mode_colorlow_warn(mWARNING);
  else if(get_mode_on_low_type() == mALARM_type )
    indicator_->set_mode_colorlow_warn(mALARM);

    indicator_->set_precision(get_indicator_precision());
    indicator_->set_digits(get_indicator_digits());
  indicator_->set_factor(get_property_factor());

  if( get_cube_ai() == UniWidgetsTypes::DefaultObjectId )
    indicator_->set_value_ai(get_state_ai());
  else
    indicator_->set_value_ai(get_cube_ai());

  indicator_->set_node(get_node());

  if(!get_indicator_font().empty() && indicator_->get_indicator())
  {
//     indicator_->get_indicator()->set_font(get_indicator_font());
    indicator_->get_indicator()->set_property_font_name_(get_indicator_font());
    indicator_->get_indicator()->set_property_on_font_name_(get_indicator_font());
//     indicator_->get_indicator()->set_default_color(get_indicator_font_color());
    indicator_->get_indicator()->set_property_font_color_(get_indicator_font_color());
    indicator_->get_indicator()->set_property_state_(false);
  }

  indicator_->configure();

  /* The SimpleObject::on_realize will be called after this put */
  put(*indicator_,
      indicator_rect->get_x(),
      indicator_rect->get_y());

//   set_size_request(-1,-1);
  show();
  indicator_->show();
}
// -------------------------------------------------------------------------
