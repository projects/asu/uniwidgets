#ifndef _CISTERN_H
#define _CISTERN_H
// -------------------------------------------------------------------------
#include <string>
#include <map>
#include <objects/SimpleObject.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class LinkLogic;
class TypicalState;
class TypicalText;
class TypicalIndicator;
class TypicalCisternBlink;
class TypicalTwoState;
class TypicalIndicatorBlink;
/*!
 * \brief Типовой виджет цистерна.
 * \par
 * Виджет отображает работу цистерны. Отображается рамка со шкалой и наполнителем
 * , цифровой индикатор с рамкой и пороги цистерны(верхий и нижний).
 * \image html cistern.png
*/
class Cistern : public SimpleObject
{
public:
  Cistern();
  explicit Cistern(SimpleObject::BaseObjectType* gobject);
  virtual ~Cistern();

  /* Constants */
  Gdk::Rectangle* state_h_rect;      /*!< координаты и размер верхнего порога */
  Gdk::Rectangle* state_l_rect;      /*!< координаты и размер нижнего порога */
  Gdk::Rectangle* indicator_rect;      /*!< координаты и размер индикатора */
  Gdk::Rectangle* cistern_rect;      /*!< координаты и размер виджета цистерна */
  Gdk::Rectangle* n_text_h_rect;      /*!< координаты и размер текстового обозначения(номера) верхнего порога */
  Gdk::Rectangle* n_text_l_rect;      /*!< координаты и размер индикатора текстового обозначения(номера) нижнего порога */
protected:
  /* Handlers */
  virtual void on_realize();
  virtual void on_connect() throw();

private:
  /* Variables */
  TypicalIndicatorBlink* indicator_;
  TypicalCisternBlink* cistern_;
  TypicalTwoState* frame_;
  TypicalState* state_h_;
  TypicalState* state_l_;
  TypicalText* n_text_h_;
  TypicalText* n_text_l_;
  LinkLogic* link_;
  bool is_configured;

  /* Methods */
  void constructor();
  void on_rectangle_changed();
  void on_text_changed(TypicalText *, const Glib::Property<Glib::ustring> *);
  void set_rectangle(Gdk::Rectangle* rect_,const long x, const long y, const long w, const long h);
  void on_configure();

  DISALLOW_COPY_AND_ASSIGN(Cistern);

  /* Properties */
  /*Lock view for ULockNotebook*/
  ADD_PROPERTY( cistern_lock_view_high, bool);          /*!< свойство: блокировка экрана по АПС от срабатывания верхнего уровня */
  ADD_PROPERTY( cistern_lock_view_low, bool);          /*!< свойство: блокировка экрана по АПС от срабатывания нижнего уровня */
  /*Sensors*/
  ADD_PROPERTY( state_ai, UniWidgetsTypes::ObjectId )          /*!< свойство: id датчика уровня */
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )          /*!< свойство: id узла */
  ADD_PROPERTY( cube_ai, UniWidgetsTypes::ObjectId )          /*!< свойство: id датчика текущего объема */
  ADD_PROPERTY( property_link_di, UniWidgetsTypes::ObjectId )        /*!< свойство: id датчика состояния связи с SharedMemory */
  ADD_PROPERTY( self_state_ai, UniWidgetsTypes::ObjectId )        /*!< свойство: id датчика внутреннего состояния виджета */
  ADD_PROPERTY( threshold_high_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика верхнего порога уровня */
  ADD_PROPERTY( cistern_high_level, long)            /*!< свойство: числовое значение верхнего порога уровня */
  ADD_PROPERTY( threshold_low_ai, UniWidgetsTypes::ObjectId )        /*!< свойство: id датчика нижнего порога уровня */
  /*States of thresholds*/
  /*High*/
  ADD_PROPERTY( mode_logic_on_high, long )          /*!< свойство: режим для верхнего уровня */
  ADD_PROPERTY( mode_on_high_type, UniWidgetsTypes::ThresholdType )    /*!< свойство: тип для верхнего уровня */
  ADD_PROPERTY( priority_on_high, long )            /*!< свойство: приоритет для верхнего уровня */
  /*Low*/
  ADD_PROPERTY( mode_logic_on_low, long )            /*!< свойство: режим для гижнего уровня */
  ADD_PROPERTY( mode_on_low_type, UniWidgetsTypes::ThresholdType )    /*!< свойство: тип для нижнего уровня */
  ADD_PROPERTY( priority_on_low, long )            /*!< свойство: приоритет для нижнего уровня */
  /*Images*/
  ADD_PROPERTY( cistern_background_off_path, Glib::ustring )      /*!< свойство: картинка для фона(выключено) */
  ADD_PROPERTY( cistern_background_warn_path, Glib::ustring )      /*!< свойство: картинка для фона(предупреждение) */
  ADD_PROPERTY( cistern_background_alarm_path, Glib::ustring )      /*!< свойство: картинка для фона(авария) */
  ADD_PROPERTY( cistern_filling_path, Glib::ustring )        /*!< свойство: картинка для заполнителя */
  ADD_PROPERTY( cistern_scale_path, Glib::ustring )        /*!< свойство: картинка для шкалы */
  ADD_PROPERTY( cistern_scale_switch, bool)                            /*!< свойство: отображать шкалу */
  ADD_PROPERTY( high_level_off_path, Glib::ustring )        /*!< свойство: картинка для знака верхнего уровня(выключено) */
  ADD_PROPERTY( high_level_alarm_path, Glib::ustring )        /*!< свойство: картинка для знака верхнего уровня(авария) */
  ADD_PROPERTY( low_level_alarm_path, Glib::ustring )        /*!< свойство: картинка для знака нижнего уровня(авария) */
  ADD_PROPERTY( low_level_off_path, Glib::ustring )        /*!< свойство: картинка для знака нижнего уровня(выключено) */
  ADD_PROPERTY( cistern_indicator_path, Glib::ustring )        /*!< свойство: картинка для рамки индикатора(выключено) */
  ADD_PROPERTY( cistern_indicator_alarm_path, Glib::ustring )      /*!< свойство: картинка для рамки индикатора(авария) */
  ADD_PROPERTY( cistern_high_alarm_path, Glib::ustring )        /*!< свойство: картинка заполнителя отображаемая при срабатывании верхнего уровня */
  ADD_PROPERTY( cistern_low_alarm_path, Glib::ustring )        /*!< свойство: артинка заполнителя отображаемая при срабатывании нижнего уровня */

  ADD_PROPERTY( indicator_font, std::string )          /*!< свойство: шрифт цифр индикатора */
  ADD_PROPERTY( indicator_font_color, Gdk::Color )        /*!< свойство: цвет цифр индикатора */
  ADD_PROPERTY( indicator_precision, long )
  ADD_PROPERTY( indicator_digits, long )
  ADD_PROPERTY( property_factor, double )            /*!< свойство: поправочный коэффициент для значения индикатора, получаемого от аналогового датчика */
  
  ADD_PROPERTY( w_cistern, long)              /*!< свойство: ширина виджета */
  ADD_PROPERTY( h_cistern, long)              /*!< свойство: высота виджета */
  ADD_PROPERTY( x_states, long)              /*!< свойство: x-координата порогов уровня */
  ADD_PROPERTY( y_state_high, long)            /*!< свойство: y-координата верхнего порога уровня */
  ADD_PROPERTY( y_state_low, long)            /*!< свойство: y-координата нижнего порога уровня */
  ADD_PROPERTY( w_states, long)              /*!< свойство: ширина порогов уровня */
  ADD_PROPERTY( h_states, long)              /*!< свойство: высота порогов уровня */
  ADD_PROPERTY( x_indicator, long)            /*!< свойство: x-координата цифрового индикатора */
  ADD_PROPERTY( y_indicator, long)            /*!< свойство: y-координата цифрового индикатора */
  ADD_PROPERTY( w_indicator, long)            /*!< свойство: ширина цифрового индикатора */
  ADD_PROPERTY( h_indicator, long)            /*!< свойство: высота цифрового индикатора */

  ADD_PROPERTY( font_size, long )              /*!< свойство: шрифт текста в состоянии включен */
  ADD_PROPERTY( font_shadow_on, bool )            /*!< свойство: показывать тень для текста в состоянии включен */
  ADD_PROPERTY( font_shadow_off, bool )            /*!< свойство: показывать тень для текста в состоянии выключен */
  ADD_PROPERTY( font_color_off, Gdk::Color )          /*!< свойство: шрифт текста в состоянии выключен */

  ADD_PROPERTY( num_text_high, Glib::ustring )          /*!< свойство: текст номера сигнала верхнего порога */
  ADD_PROPERTY( font_color_on_high, Gdk::Color )          /*!< свойство: шрифт текста номера сигнала верхнего порога */
  ADD_PROPERTY( x_num_text_high_rect, long )          /*!< свойство: x-координата текста номера сигнала верхнего порога */
  ADD_PROPERTY( y_num_text_high_rect, long )          /*!< свойство: y-координата текста номера сигнала верхнего порога */

  ADD_PROPERTY( num_text_low, Glib::ustring )          /*!< свойство: текст номера сигнала нижнего порога */
  ADD_PROPERTY( font_color_on_low, Gdk::Color )          /*!< свойство: шрифт номера сигнала нижнего порога */
  ADD_PROPERTY( x_num_text_low_rect, long )          /*!< свойство: x-координата текста номера сигнала нижнего порога */
  ADD_PROPERTY( y_num_text_low_rect, long )          /*!< свойство: y-координата текста номера сигнала нижнего порога */

};

}
#endif
