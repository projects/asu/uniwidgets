#include <iostream>
#include <components/Image.h>
#include <types.h>
#include "GDG.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define GDG_NODE				"node"
#define GDG_NAME_PATH				"name-path"
#define GDG_G_OFF_PATH				"g-off-path"
#define GDG_G_ON_PATH				"g-on-path"
#define GDG_G_TRANS_PATH			"g-trans-path"
#define GDG_G_TRANS2_PATH			"g-trans2-path"
#define GDG_G_WARN_PATH				"g-warn-path"
#define GDG_G_ALARM_PATH			"g-alarm-path"
#define GDG_D_OFF_PATH				"d-off-path"
#define GDG_D_ON_PATH				"d-on-path"
#define GDG_D_READY_PATH			"d-ready-path"
#define GDG_D_TRANS_PATH			"d-trans-path"
#define GDG_D_TRANS2_PATH			"d-trans2-path"
#define GDG_D_WARN_PATH				"d-warn-path"
#define GDG_D_ALARM_PATH			"d-alarm-path"
#define GDG_D_UNDEF_PATH			"d-undef-path"
#define GDG_LINK_DI				"link-di"
#define GDG_G_AI				"g-ai"
#define GDG_D_AI				"d-ai"
#define GDG_KEY_AI				"key-ai"
#define GDG_CONTROL_AI				"control-ai"
#define GDG_BACK_PROTECTION_AI			"back-protection-ai"
#define GDG_BACK_RESERV_DI			"back-reserv-di"
#define GDG_READY_DI				"gdg-ready-di"
#define GDG_READY_PATH				"gdg-ready-path"
#define GDG_WORK_AI				"gdg-work-ai"
#define GDG_WORK_PATH				"gdg-work-path"
#define GDG_GEN_OVERLOAD95_DI			"gen-overload95-di"
#define GDG_GEN_OVERLOAD95_PATH			"gen-overload95-path"
#define GDG_GEN_OVERLOAD110_DI			"gen-overload110-di"
#define GDG_GEN_OVERLOAD110_PATH		"gen-overload110-path"
#define GDG_INDICATOR_U_AI			"indicator-u-ai"
#define GDG_INDICATOR_P_AI			"indicator-p-ai"
#define GDG_INDICATOR_F_AI			"indicator-f-ai"
#define GDG_INDICATOR_I_AI			"indicator-i-ai"
#define GDG_INDICATOR_THRESHOLD_U_WARN		"indicator-threshold-u-warn"
#define GDG_INDICATOR_THRESHOLD_P_LOW_WARN	"indicator-threshold-p-low-warn"
#define GDG_INDICATOR_THRESHOLD_P_WARN		"indicator-threshold-p-warn"
#define GDG_INDICATOR_THRESHOLD_P_ALARM		"indicator-threshold-p-alarm"
#define GDG_INDICATOR_THRESHOLD_F_WARN		"indicator-threshold-f-warn"
#define GDG_BACK_WIDTH				"back-width"
#define GDG_BACK_HEIGHT				"back-heigth"
#define X_CONTROL				"x-control"
#define Y_CONTROL				"y-control"
#define W_CONTROL				"w-control"
#define H_CONTROL				"h-control"
#define X_DIESEL_GEN				"x-diesel-gen"
#define Y_DIESEL				"y-diesel"
#define W_H_DIESEL				"w-h-diesel"
#define Y_GEN					"y-gen"
#define W_H_GEN					"w-h-gen"
#define X_GEN_STATES				"x-gen-states"
#define Y_GEN_STATES				"y-gen-states"
#define W_GEN_STATES				"w-gen-states"
#define H_GEN_STATES				"h-gen-states"
#define X_INDICATORS_U_P			"x-indicators-u-p"
#define X_INDICATORS_I_F			"x-indicators-i-f"
#define Y_INDICATORS_U				"y-indicators-u"
#define Y_INDICATORS_P				"y-indicators-p"
#define Y_INDICATORS_I				"y-indicators-i"
#define Y_INDICATORS_F				"y-indicators-f"
#define W_INDICATORS_U_P			"w-indicators-u-p"
#define H_INDICATORS_U_P			"h-indicators-u-p"
#define W_INDICATORS_I_F			"w-indicators-i-f"
#define H_INDICATORS_I_F			"h-indicators-i-f"
#define X_INDICATOR_VALUE_U_P			"x-indicator-value-u-p"
#define X_INDICATOR_VALUE_I_F			"x-indicator-value-i-f"
#define X_KEY					"x-key"
#define Y_KEY					"y-key"
#define W_KEY					"w-key"
#define H_KEY					"h-key"
#define X_NAME					"x-name"
#define Y_NAME					"y-name"
#define W_NAME					"w-name"
#define H_NAME					"h-name"
#define SVG_PATH				"svg-path"
#define X_TEXT_CONTROL				"x-text-control"
#define Y_TEXT_CONTROL				"y-text-control"
#define SIZE_TEXT_CONTROL			"size-text-control"
#define COLOR_TEXT_CONTROL			"color-text-control"
// -------------------------------------------------------------------------
#define INIT_GDG_PROPERTIES() \
	node(*this, GDG_NODE , UniWidgetsTypes::DefaultObjectId) \
	,svg_path(*this, SVG_PATH ,"/usr/share/yauza/19/svg/ses/") \
	,name_path(*this, GDG_NAME_PATH, "" ) \
	,g_off_path(*this, GDG_G_OFF_PATH, "" ) \
	,g_on_path(*this, GDG_G_ON_PATH, "" ) \
	,g_trans_path(*this, GDG_G_TRANS_PATH, "" ) \
	,g_trans2_path(*this, GDG_G_TRANS2_PATH, "" ) \
	,g_warn_path(*this, GDG_G_WARN_PATH, "" ) \
	,g_alarm_path(*this, GDG_G_ALARM_PATH, "" ) \
	,d_off_path(*this, GDG_D_OFF_PATH, "" ) \
	,d_on_path(*this, GDG_D_ON_PATH, "" ) \
	,d_ready_path(*this, GDG_D_READY_PATH, "" ) \
	,d_trans_path(*this, GDG_D_TRANS_PATH, "" ) \
	,d_trans2_path(*this, GDG_D_TRANS2_PATH, "" ) \
	,d_warn_path(*this, GDG_D_WARN_PATH, "" ) \
	,d_alarm_path(*this, GDG_D_ALARM_PATH, "" ) \
	,d_undef_path(*this, GDG_D_UNDEF_PATH, "" ) \
	,property_link_di(*this, GDG_LINK_DI , UniWidgetsTypes::DefaultObjectId) \
	,g_ai(*this, GDG_G_AI , UniWidgetsTypes::DefaultObjectId) \
	,d_ai(*this, GDG_D_AI , UniWidgetsTypes::DefaultObjectId) \
	,key_ai(*this, GDG_KEY_AI , UniWidgetsTypes::DefaultObjectId) \
	,control_ai(*this, GDG_CONTROL_AI , UniWidgetsTypes::DefaultObjectId) \
	,back_protection_ai(*this, GDG_BACK_PROTECTION_AI , UniWidgetsTypes::DefaultObjectId) \
	,back_reserv_di(*this, GDG_BACK_RESERV_DI , UniWidgetsTypes::DefaultObjectId) \
	,gdg_ready_di(*this, GDG_READY_DI , UniWidgetsTypes::DefaultObjectId) \
	,gdg_ready_path(*this, GDG_READY_PATH , "") \
	,gdg_work_ai(*this, GDG_WORK_AI , UniWidgetsTypes::DefaultObjectId) \
	,gdg_work_path(*this, GDG_WORK_PATH , "") \
	,gen_overload95_di(*this, GDG_GEN_OVERLOAD95_DI , UniWidgetsTypes::DefaultObjectId) \
	,gen_overload95_path(*this, GDG_GEN_OVERLOAD95_PATH , "") \
	,gen_overload110_di(*this, GDG_GEN_OVERLOAD110_DI , UniWidgetsTypes::DefaultObjectId) \
	,gen_overload110_path(*this, GDG_GEN_OVERLOAD110_PATH , "") \
	,indicator_u_ai(*this, GDG_INDICATOR_U_AI, UniWidgetsTypes::DefaultObjectId) \
	,indicator_p_ai(*this, GDG_INDICATOR_P_AI, UniWidgetsTypes::DefaultObjectId) \
	,indicator_f_ai(*this, GDG_INDICATOR_F_AI, UniWidgetsTypes::DefaultObjectId) \
	,indicator_i_ai(*this, GDG_INDICATOR_I_AI, UniWidgetsTypes::DefaultObjectId) \
	,indicator_threshold_u_warn(*this, GDG_INDICATOR_THRESHOLD_U_WARN , UniWidgetsTypes::DefaultObjectId) \
	,indicator_threshold_p_low_warn(*this, GDG_INDICATOR_THRESHOLD_P_LOW_WARN , UniWidgetsTypes::DefaultObjectId) \
	,indicator_threshold_p_warn(*this, GDG_INDICATOR_THRESHOLD_P_WARN , UniWidgetsTypes::DefaultObjectId) \
	,indicator_threshold_p_alarm(*this, GDG_INDICATOR_THRESHOLD_P_ALARM , UniWidgetsTypes::DefaultObjectId) \
	,indicator_threshold_f_warn(*this, GDG_INDICATOR_THRESHOLD_F_WARN , UniWidgetsTypes::DefaultObjectId) \
	,back_width(*this, GDG_BACK_WIDTH, 265) \
	,back_height(*this, GDG_BACK_HEIGHT, 202) \
	,x_control(*this, X_CONTROL, 46) \
	,y_control(*this, Y_CONTROL, 26) \
	,w_control(*this, W_CONTROL, 210) \
	,h_control(*this, H_CONTROL, 18) \
	,x_diesel_gen(*this, X_DIESEL_GEN, 18) \
	,y_diesel(*this, Y_DIESEL, 57) \
	,w_h_diesel(*this, W_H_DIESEL, 51) \
	,y_gen(*this, Y_GEN, 121) \
	,w_h_gen(*this, W_H_GEN, 50) \
	,x_gen_states(*this, X_GEN_STATES, 91) \
	,y_gen_states(*this, Y_GEN_STATES, 55) \
	,w_gen_states(*this, W_GEN_STATES, 142) \
	,h_gen_states(*this, H_GEN_STATES, 48) \
	,x_indicators_u_p(*this, X_INDICATORS_U_P, 66) \
	,x_indicators_i_f(*this, X_INDICATORS_I_F, 174) \
	,y_indicators_u(*this, Y_INDICATORS_U, 115) \
	,y_indicators_p(*this, Y_INDICATORS_P, 154) \
	,y_indicators_i(*this, Y_INDICATORS_I, 155) \
	,y_indicators_f(*this, Y_INDICATORS_F, 116) \
	,w_indicators_u_p(*this, W_INDICATORS_U_P, 105) \
	,h_indicators_u_p(*this, H_INDICATORS_U_P, 32) \
	,w_indicators_i_f(*this, W_INDICATORS_I_F, 86) \
	,h_indicators_i_f(*this, H_INDICATORS_I_F, 32) \
	,x_indicator_value_u_p(*this, X_INDICATOR_VALUE_U_P, 4 ) \
	,x_indicator_value_i_f(*this, X_INDICATOR_VALUE_I_F, 2 ) \
	,x_key(*this, X_KEY, 38) \
	,y_key(*this, Y_KEY, 198) \
	,w_key(*this, W_KEY, 26) \
	,h_key(*this, H_KEY, 51) \
	,x_name(*this, X_NAME, 9) \
	,y_name(*this, Y_NAME, 17) \
	,w_name(*this, W_NAME, 27) \
	,h_name(*this, H_NAME, 11) \
	,x_text_control(*this, X_TEXT_CONTROL, 180) \
	,y_text_control(*this, Y_TEXT_CONTROL, -1) \
	,size_text_control(*this, SIZE_TEXT_CONTROL, 16) \
	,color_text_control(*this, COLOR_TEXT_CONTROL, Gdk::Color("yellow"))
// -------------------------------------------------------------------------
/* Static definitions */
const string GDG::img_back_path = "gdg_back.svg";
const string GDG::img_back_alarm_path = "gdg_back_alarm.svg";
const string GDG::img_back_reserv_path = "gdg_back_reserv.svg";
/* Keys */
const string GDG::img_key_off_path = "gdg_key_off.svg";
const string GDG::img_key_on_path = "gdg_key_on.svg";
const string GDG::img_key_protection_path = "gdg_key_alarm.svg";
const string GDG::img_key_undef_path = "gdg_key_undef.svg";
const string GDG::img_key_undef_off_path = "gdg_key_undef_off.svg";
const string GDG::img_key_trans_path = "gdg_key_transient.svg";
const string GDG::img_key_trans2_path = "gdg_key_transient2.svg";

/*Control*/
const string GDG::img_control_auto_path = "gdg_control_auto.svg";
const string GDG::img_control_hand_path = "gdg_control_hand.svg";
const string GDG::img_control_cpu_path = "gdg_control_cpu.svg";
const string GDG::img_control_unknown_path = "gdg_control_unknown.svg";

const string GDG::img_gen_off_path = "gdg_gen_off.svg";

/*Indicator*/
const string GDG::img_indicator_u_path = "gdg_indicator_u_off.svg";
const string GDG::img_indicator_u_warn_path = "gdg_indicator_u_warn.svg";
const string GDG::img_indicator_p_path = "gdg_indicator_p_off.svg";
const string GDG::img_indicator_p_warn_path = "gdg_indicator_p_warn.svg";
const string GDG::img_indicator_p_alarm_path = "gdg_indicator_p_alarm.svg";
const string GDG::img_indicator_f_path = "gdg_indicator_f_off.svg";
const string GDG::img_indicator_f_warn_path = "gdg_indicator_f_warn.svg";
const string GDG::img_indicator_i_path = "gdg_indicator_i.svg";
// -------------------------------------------------------------------------
void GDG::constructor()
{

	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback<GDG>;

	is_configured = false;
  /*Добавляем к родительскому контейнеру с типом*/
  add_child( &state_back,typeObject );
  add_child( &state_control,typeObject );
  add_child( &state_key,typeObject );
  add_child( &state_D,typeObject );
  add_child( &state_G,typeObject );
  add_child( &generator_states,typeObject );
  add_child( &indicator_u,typeObject );
  add_child( &indicator_p,typeObject );
  add_child( &indicator_f,typeObject );
  add_child( &indicator_i,typeObject );
	add_child(&link_, typeLogic);
#if GTK_VERSION_GE(2,18)
	link_.set_visible(false);
#else
	link_.hide();
#endif
	back_rect = new Gdk::Rectangle(0, 0, get_back_width(), get_back_height());
	control_rect = new Gdk::Rectangle(get_x_control(), get_y_control(), get_w_control(), get_h_control());
	D_rect = new Gdk::Rectangle(get_x_diesel_gen(), get_y_diesel(), get_w_h_diesel(), get_w_h_diesel());
	G_rect = new Gdk::Rectangle(get_x_diesel_gen(), get_y_gen(), get_w_h_gen(), get_w_h_gen());
	gen_states_rect = new Gdk::Rectangle(get_x_gen_states(), get_y_gen_states(), get_w_gen_states(), get_h_gen_states());
	key_rect = new Gdk::Rectangle(get_x_key(), get_y_key(), get_w_key(), get_h_key());

	connect_property_changed("back_width", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("back_heigth", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("x-control", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("y-control", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("w-control", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("h-control", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("x-diesel-gen", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("y-diesel", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("w-h-diesel", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("y-gen", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("w-h-gen", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("x-gen-states", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("y-gen-states", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("w-gen-states", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("h-gen-states", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("x-key", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("y-key", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("w-key", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	connect_property_changed("h-key", sigc::mem_fun(*this, &GDG::on_rectangle_changed));
	name = new Image();
  add_child(name,  typeView);
}
// -------------------------------------------------------------------------
GDG::GDG() :
	Glib::ObjectBase("gdg")
	,INIT_GDG_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
GDG::GDG(SimpleObject::BaseObjectType* gobject) :
	SimpleObject(gobject)
	,INIT_GDG_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
GDG::~GDG()
{
	delete back_rect;
	delete control_rect;
	delete D_rect;
	delete G_rect;
	delete gen_states_rect;
	delete key_rect;
	delete name;
}
// -------------------------------------------------------------------------
void GDG::on_rectangle_changed()
{
	set_size_request(-1,-1);
	back_rect->set_x(0);
	back_rect->set_y(0);
	back_rect->set_width(get_back_width());
	back_rect->set_height(get_back_height());

	control_rect->set_x(get_x_control());
	control_rect->set_y(get_y_control());
	control_rect->set_width(get_w_control());
	control_rect->set_height(get_h_control());

	D_rect->set_x(get_x_diesel_gen());
	D_rect->set_y(get_y_diesel());
	D_rect->set_width(get_w_h_diesel());
	D_rect->set_height(get_w_h_diesel());

	G_rect->set_x(get_x_diesel_gen());
	G_rect->set_y(get_y_gen());
	G_rect->set_width(get_w_h_gen());
	G_rect->set_height(get_w_h_gen());

	gen_states_rect->set_x(get_x_gen_states());
	gen_states_rect->set_y(get_y_gen_states());
	gen_states_rect->set_width(get_w_gen_states());
	gen_states_rect->set_height(get_h_gen_states());

	key_rect->set_x(get_x_key());
	key_rect->set_y(get_y_key());
	key_rect->set_width(get_w_key());
	key_rect->set_height(get_h_key());
	queue_resize();
}
// -------------------------------------------------------------------------
void GDG::on_realize()
{
	SimpleObject::on_realize();
	on_rectangle_changed();

	if (!is_configured )
	{
		is_configured = true;
		on_configure();
	}
}
// -------------------------------------------------------------------------
void GDG::on_connect() throw()
{
	SimpleObject::on_connect();
	on_rectangle_changed();

	if (!is_configured )
	{
		is_configured = true;
		on_configure();
	}
}
// -------------------------------------------------------------------------
void GDG::on_configure()
{
	link_.set_link_di( get_property_link_di() );
	link_.set_node( get_node() );
#if GTK_VERSION_GE(2,18)
	link_.set_visible(false);
#else
	link_.hide();
#endif
	/* Frame */
	state_back.set_rect(*back_rect);

	state_back.set_path(mOFF, get_svg_path() + img_back_path);
	state_back.set_path(mRESERV, get_svg_path() + img_back_reserv_path);
	state_back.set_path(mPROTECTION, get_svg_path() + img_back_alarm_path);
	state_back.insert_mode(mOFF,-1,false);
	state_back.insert_mode(mRESERV,-1,false);
	state_back.insert_mode(mPROTECTION,-1,true);
	
	state_back.set_firststate_mode(mRESERV);
	state_back.set_firststate_ai(get_back_reserv_di());
	state_back.set_firststate_node(get_node());

	state_back.set_secondstate_mode(mPROTECTION);
	state_back.set_secondstate_ai(get_back_protection_ai());
	state_back.set_secondstate_node(get_node());
	state_back.add_secondstate_blinkoff_mode(mPROTECTION);
	state_back.set_lock_view(get_property_lock_view());

	state_back.configure();
	
	put(state_back,
			back_rect->get_x(),
			back_rect->get_y());

	/* Control */
	state_control.set_rect(*control_rect);

	state_control.set_path(mINDEF, get_svg_path() + img_control_unknown_path);
	state_control.set_path(mAUTO, get_svg_path() + img_control_auto_path);
	state_control.set_path(mHAND, get_svg_path() + img_control_hand_path);
	state_control.set_path(mCPU, get_svg_path() + img_control_cpu_path);

	state_control.set_text_property(get_x_text_control(), get_y_text_control(), get_size_text_control());
	state_control.set_text_color(get_color_text_control());

	state_control.set_state_ai(get_control_ai());
	state_control.set_node(get_node());
	state_control.set_lock_view(get_property_lock_view());

	state_control.configure();

	/* The SimpleObject::on_realize will be called after this put */
	put(state_control,
			control_rect->get_x(),
			control_rect->get_y());
	name->set_size_request(get_w_name(),get_h_name());
	name->set_image_path(get_name_path());

	put(*name,get_x_name(),get_y_name());

	/* Key */
	state_key.set_rect(*key_rect);

	state_key.set_path(mOFF, get_svg_path() + img_key_off_path);
	state_key.set_path(mON, get_svg_path() + img_key_on_path);
	state_key.set_path(mPROTECTION, get_svg_path() + img_key_protection_path);
	state_key.set_path(mUNKNOWN, get_svg_path() + img_key_undef_path);
	state_key.set_path2(mUNKNOWN, get_svg_path() + img_key_undef_off_path);
	state_key.set_path(mTRANSITIVE, get_svg_path() + img_key_trans_path);
	state_key.set_path2(mTRANSITIVE, get_svg_path() + img_key_trans2_path);
	state_key.insert_mode(mOFF,-1,false);
	state_key.insert_mode(mON,-1,false);
	state_key.insert_mode(mPROTECTION,-1,true);
	state_key.insert_mode(mUNKNOWN,-1,true);
	state_key.insert_mode(mTRANSITIVE,-1,true);

	state_key.set_state_ai(get_key_ai());
	state_key.set_node(get_node());
	state_key.set_lock_view(get_property_lock_view());

	state_key.configure();

	/* The SimpleObject::on_realize will be called after this put */
	put(state_key,
			key_rect->get_x(),
			key_rect->get_y());

	/* Diesel */
	state_D.set_rect(*D_rect);
	state_D.set_path(mOFF, get_d_off_path());
	state_D.set_path(mON, get_d_on_path());
	state_D.set_path(mTRANSITIVE, get_d_trans_path());
	state_D.set_path2(mTRANSITIVE, get_d_trans2_path());
	state_D.set_path(mPROTECTION, get_d_alarm_path());
	state_D.set_path(mUNKNOWN, get_d_undef_path());
	state_D.insert_mode(mOFF,-1,false);
	state_D.insert_mode(mON,-1,false);
	state_D.insert_mode(mPROTECTION,-1,true);
	state_D.insert_mode(mUNKNOWN,-1,true);
	state_D.insert_mode(mTRANSITIVE,-1,true);

	state_D.set_state_ai(get_d_ai());
	state_D.set_node(get_node());
	state_D.set_lock_view(get_property_lock_view());

	state_D.configure();

	/* The SimpleObject::on_realize will be called after this put */
	put(state_D,
			D_rect->get_x(),
			D_rect->get_y());

	/* Generator */
	state_G.set_rect(*G_rect);
	state_G.set_path(mOFF, get_g_off_path());
	state_G.set_path(mON, get_g_on_path());
	state_G.set_path(mTRANSITIVE, get_g_trans_path());
	state_G.set_path2(mTRANSITIVE, get_g_trans2_path());
	state_G.set_path(mWARNING, get_g_warn_path());
	state_G.set_path(mPROTECTION, get_g_alarm_path());
	state_G.insert_mode(mOFF,-1,false);
	state_G.insert_mode(mON,-1,false);
	state_G.insert_mode(mPROTECTION,-1,true);
	state_G.insert_mode(mWARNING,-1,true);
	state_G.insert_mode(mTRANSITIVE,-1,true);

	state_G.set_state_ai(get_g_ai());
	state_G.set_node(get_node());
	state_G.set_lock_view(get_property_lock_view());
	
	state_G.configure();
	
	/* The SimpleObject::on_realize will be called after this put */
	put(state_G,
			G_rect->get_x(),
			G_rect->get_y());

	
	/* Generator states */
	generator_states.set_rect(*gen_states_rect);

	generator_states.set_path(mOFF, get_svg_path() + img_gen_off_path);
	generator_states.set_path(mREADY, get_gdg_ready_path());
	generator_states.set_path(mON, get_gdg_work_path());
    generator_states.set_path(mRunning, get_gdg_work_path());
	generator_states.set_path(mWARNING_HIGH, get_gen_overload95_path());
	generator_states.set_path(mALARM_HIGH, get_gen_overload110_path());
	generator_states.insert_mode(mOFF,-1,false);
	generator_states.insert_mode(mREADY,-1,false);
	generator_states.insert_mode(mON,-1,false);
    generator_states.insert_mode(mRunning,-1,false);
	generator_states.insert_mode(mWARNING_HIGH,-1,true);
	generator_states.insert_mode(mALARM_HIGH,-1,true);

	generator_states.set_firststate_mode(mREADY); //DGxx_ReadyForStart_S
	generator_states.set_firststate_ai(get_gdg_ready_di());
	generator_states.set_firststate_node(get_node());

	generator_states.set_secondstate_ai(get_gdg_work_ai()); //SEESxx_State_AS
	generator_states.set_secondstate_node(get_node());
	generator_states.add_secondstate_ignoremode(mWARNING_HIGH);
	generator_states.add_secondstate_ignoremode(mALARM_HIGH);

	generator_states.set_thirdstate_mode(mWARNING_HIGH);
	generator_states.set_thirdstate_ai(get_gen_overload95_di());
	generator_states.set_thirdstate_node(get_node());

	generator_states.set_fourthstate_mode(mALARM_HIGH);
	generator_states.set_fourthstate_ai(get_gen_overload110_di());
	generator_states.set_fourthstate_node(get_node());

	generator_states.set_lock_view(get_property_lock_view());
	generator_states.configure();
	
	/* The SimpleObject::on_realize will be called after this put */
	put(generator_states,
			gen_states_rect->get_x(),
			gen_states_rect->get_y());

	/* Indicator U */
	indicator_u.set_svg_path(get_svg_path());
	indicator_u.set_value_ai(get_indicator_u_ai());
	indicator_u.set_node(get_node());

	indicator_u.set_off_path(get_svg_path() + img_indicator_u_path);
	indicator_u.set_warn_path(get_svg_path() + img_indicator_u_warn_path);

	indicator_u.set_thresholdlow_warn(get_indicator_threshold_u_warn());
	indicator_u.set_width(get_w_indicators_u_p());
	indicator_u.set_height(get_h_indicators_u_p());
	indicator_u.set_ind_rect_x(get_x_indicator_value_u_p());
	indicator_u.set_property_precision(1);
	indicator_u.set_property_digits(5);
	indicator_u.set_indicator_lock_view_high(get_property_lock_view());
	indicator_u.set_indicator_lock_view_low(get_property_lock_view());

	put(indicator_u,get_x_indicators_u_p(),get_y_indicators_u());

	/* Indicator P */
	indicator_p.set_svg_path(get_svg_path());
	indicator_p.set_value_ai(get_indicator_p_ai());
	indicator_p.set_node(get_node());

	indicator_p.set_off_path(get_svg_path() + img_indicator_p_path);
	indicator_p.set_warn_path(get_svg_path() + img_indicator_p_warn_path);
	indicator_p.set_alarm_path(get_svg_path() + img_indicator_p_alarm_path);

	indicator_p.set_thresholdlow_warn(get_indicator_threshold_p_low_warn());
	indicator_p.set_thresholdhigh_warn(get_indicator_threshold_p_warn());
	indicator_p.set_thresholdhigh_alarm(get_indicator_threshold_p_alarm());
	indicator_p.set_width(get_w_indicators_u_p());
	indicator_p.set_height(get_h_indicators_u_p());
	indicator_p.set_ind_rect_x(get_x_indicator_value_u_p());
	indicator_p.set_blinking_low(false);
	indicator_p.set_property_precision(0);
	indicator_p.set_property_digits(4);
	indicator_p.set_indicator_lock_view_high(get_property_lock_view());
	indicator_p.set_indicator_lock_view_low(get_property_lock_view());
	
	put(indicator_p,get_x_indicators_u_p(),get_y_indicators_p());


	/* Indicator F */
	indicator_f.set_svg_path(get_svg_path());
	indicator_f.set_value_ai(get_indicator_f_ai());
	indicator_f.set_node(get_node());

	indicator_f.set_off_path(get_svg_path() + img_indicator_f_path);
	indicator_f.set_warn_path(get_svg_path() + img_indicator_f_warn_path);

	indicator_f.set_thresholdlow_warn(get_indicator_threshold_f_warn());
	indicator_f.set_width(get_w_indicators_i_f());
	indicator_f.set_height(get_h_indicators_i_f());
	indicator_f.set_ind_rect_x(get_x_indicator_value_i_f());
	indicator_f.set_property_precision(2);
	indicator_f.set_property_digits(5);
	indicator_f.set_indicator_lock_view_high(get_property_lock_view());
	indicator_f.set_indicator_lock_view_low(get_property_lock_view());
	
	put(indicator_f,get_x_indicators_i_f(),get_y_indicators_f());

	/* Indicator I */
	indicator_i.set_svg_path(get_svg_path());
	indicator_i.set_value_ai(get_indicator_i_ai());
	indicator_i.set_node(get_node());

	indicator_i.set_off_path(get_svg_path() + img_indicator_i_path);

	indicator_i.set_width(get_w_indicators_i_f());
	indicator_i.set_height(get_h_indicators_i_f());
	indicator_i.set_ind_rect_x(get_x_indicator_value_i_f());
	indicator_i.set_property_precision(0);
	indicator_i.set_property_digits(4);
	indicator_i.set_indicator_lock_view_high(get_property_lock_view());
	indicator_i.set_indicator_lock_view_low(get_property_lock_view());
	
	put(indicator_i,get_x_indicators_i_f(), get_y_indicators_i());

	/* Show all objects */
	name->show();
	state_back.show();
	state_control.show();
	state_key.show();
	state_D.show();
	state_G.show();
	generator_states.show();
	show();
}
// -------------------------------------------------------------------------
