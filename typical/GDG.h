#ifndef _GDG_H
#define _GDG_H
// -------------------------------------------------------------------------
#include <string>
#include <map>
#include <global_macros.h>
#include <objects/SimpleObject.h>
#include <typical/TypicalState.h>
#include <typical/IndicatorTwoState.h>
#include <typical/IndicatorFourState.h>
#include <typical/TypicalGDGControl.h>
#include <typical/TypicalMultiState.h>
#include <objects/LinkLogic.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
  class Image;
//   class  TypicalMultiState;
//   class  TypicalGDGControl;
//   class  TypicalState;
//   class  IndicatorTwoState;
//   class  IndicatorFourState;
/*!
 * \brief Типовой виджет ГДГ.
 * \par
 * Виджет отображает работу Главного Дизель Генератора. На виджете отображается
 * состояние ГДГ, рамка, ГА, состояния дизеля и генератора, и четыре цифровых индикатора.
 * 
 * \n Пример ГДГ: \n \n
 * \image html gdg.png
*/
class GDG : public SimpleObject
{
public:
	GDG();
	explicit GDG(SimpleObject::BaseObjectType* gobject);
	virtual ~GDG();

	/* Constants */
	Gdk::Rectangle *back_rect;				/*!< координаты и размер рамки и фона виджета */
	Gdk::Rectangle *D_rect;					/*!< координаты и размер области отображения дизеля */
	Gdk::Rectangle *G_rect;					/*!< координаты и размер области отображения генератора */
	Gdk::Rectangle *control_rect;				/*!< координаты и размер области отображения состояния управления */
	Gdk::Rectangle *gen_states_rect;			/*!< координаты и размер области отображения состояния виджета */
	Gdk::Rectangle *key_rect;				/*!< координаты и размер области отображения ГА */
	static const std::string img_back_path;			/*!< путь к картинке рамки с фоном(отключен) */
	static const std::string img_back_reserv_path;		/*!< путь к картинке рамки с фоном(резерв) */
	static const std::string img_back_alarm_path;		/*!< путь к картинке рамки с фоном(защита) */

	static const std::string img_key_off_path;			/*!< путь к картинке ГА(отключен) */
	static const std::string img_key_on_path;			/*!< путь к картинке ГА(включен) */
	static const std::string img_key_protection_path;		/*!< путь к картинке ГА(сработала защита) */
	static const std::string img_key_undef_path;			/*!< путь к картинке ГА(неопределенное состояние) */
	static const std::string img_key_undef_off_path;		/*!< путь к картинке ГА(неопределенное состояние подложка) */
	static const std::string img_key_trans_path;			/*!< путь к картинке ГА(переходное состояние) */
	static const std::string img_key_trans2_path;		/*!< путь к картинке ГА(переходное состояние подложка) */

	static const std::string img_control_auto_path;		/*!< путь к картинке управление(автоматическое) */
	static const std::string img_control_hand_path;		/*!< путь к картинке управление(ручное) */
	static const std::string img_control_cpu_path;		/*!< путь к картинке управление(с пульта ЦПУ) */
	static const std::string img_control_unknown_path;		/*!< путь к картинке управление(неопределенное значение) */

	static const std::string img_gen_off_path;			/*!< путь к картинке состояния виджета(отключен) */
	static const std::string img_gen_alarm_off_path;		/*!< путь к картинке состояния виджета(авария) */

	static const std::string img_indicator_u_path;		/*!< путь к картинке индикатора напряжения */
	static const std::string img_indicator_u_warn_path;		/*!< путь к картинке индикатора напряжения(предупреждение) */
	static const std::string img_indicator_p_path;		/*!< путь к картинке индикатора мощности */
	static const std::string img_indicator_p_warn_path;		/*!< путь к картинке индикатора мощности(предупреждение) */
	static const std::string img_indicator_p_alarm_path;		/*!< путь к картинке индикатора мощности(авария) */
	static const std::string img_indicator_f_path;		/*!< путь к картинке индикатора частоты */
	static const std::string img_indicator_f_warn_path;		/*!< путь к картинке индикатора частоты(предупреждение) */
	static const std::string img_indicator_i_path;		/*!< путь к картинке индикатора силы тока */

	sigc::connection button_release_conn_;			/*!< ссылка на коннектор для события signal_button_release_event */

protected:
	/* Handlers */
	virtual void on_realize();
	virtual void on_connect() throw();

private:
	/* Variables */
	Image *name;
	TypicalMultiState state_back;
	TypicalGDGControl state_control;
	TypicalState state_key;
	TypicalState state_D;
	TypicalState state_G;
	TypicalMultiState generator_states;
	IndicatorTwoState indicator_u;
	IndicatorFourState indicator_p;
	IndicatorTwoState indicator_f;
	IndicatorTwoState indicator_i;
	LinkLogic link_;
		
 
	bool is_configured;

	/* Methods */
	void constructor();
	void on_rectangle_changed();
	void on_configure();


	DISALLOW_COPY_AND_ASSIGN(GDG);

	/* Properties */
	ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )					/*!< свойство: id узла */
	ADD_PROPERTY( svg_path, Glib::ustring )						/*!< свойство: путь к директории с картинкам */
	ADD_PROPERTY( name_path, Glib::ustring )					/*!< свойство: картинка для знака низвания виджета */
	ADD_PROPERTY( g_off_path, Glib::ustring )					/*!< свойство: картинка для генератора(выключен) */
	ADD_PROPERTY( g_on_path, Glib::ustring )					/*!< свойство: картинка для генератора(включен) */
	ADD_PROPERTY( g_trans_path, Glib::ustring )					/*!< свойство: картинка для генератора(переходный процесс) */
	ADD_PROPERTY( g_trans2_path, Glib::ustring )					/*!< свойство: картинка для генератора(переходный процесс, подложка при мигании) */
	ADD_PROPERTY( g_warn_path, Glib::ustring )					/*!< свойство: картинка для генератора(предупреждение) */
	ADD_PROPERTY( g_alarm_path, Glib::ustring )					/*!< свойство: картинка для генератора(авария) */
	ADD_PROPERTY( d_off_path, Glib::ustring )					/*!< свойство: картинка для дизеля(выключен) */
	ADD_PROPERTY( d_on_path, Glib::ustring )					/*!< свойство: картинка для дизеля(включен) */
	ADD_PROPERTY( d_ready_path, Glib::ustring )					/*!< свойство: картинка для дизеля(готов к пуску) */
	ADD_PROPERTY( d_trans_path, Glib::ustring )					/*!< свойство: картинка для дизеля(переходный процесс) */
	ADD_PROPERTY( d_trans2_path, Glib::ustring )					/*!< свойство: картинка для дизеля(переходный процесс, подложка при мигании) */
	ADD_PROPERTY( d_warn_path, Glib::ustring )					/*!< свойство: картинка для дизеля(предупреждение) */
	ADD_PROPERTY( d_alarm_path, Glib::ustring )					/*!< свойство: картинка для дизеля(авария) */
	ADD_PROPERTY( d_undef_path, Glib::ustring )					/*!< свойство: картинка для дизеля(неопределенное состояние) */
	ADD_PROPERTY( property_link_di, UniWidgetsTypes::ObjectId )				/*!< свойство: id датчика состояния связи с SharedMemory */
	ADD_PROPERTY( g_ai, UniWidgetsTypes::ObjectId )					/*!< свойство: id датчика состояние генератора */
	ADD_PROPERTY( d_ai, UniWidgetsTypes::ObjectId )					/*!< свойство: id датчика состояние дизеля */
	ADD_PROPERTY( key_ai, UniWidgetsTypes::ObjectId )					/*!< свойство: id датчика состояние ГА */
	ADD_PROPERTY( control_ai, UniWidgetsTypes::ObjectId )				/*!< свойство: id датчика состояние управления */
	ADD_PROPERTY( back_protection_ai, UniWidgetsTypes::ObjectId )			/*!< свойство: id датчика режим защиты */
	ADD_PROPERTY( back_reserv_di, UniWidgetsTypes::ObjectId )				/*!< свойство: id датчика резерв */
	ADD_PROPERTY( gdg_ready_di, UniWidgetsTypes::ObjectId )				/*!< свойство: id датчика готовность к пуску */
	ADD_PROPERTY( gdg_ready_path, Glib::ustring )					/*!< свойство: картинка для надписи состояния(готов к пуску) */
	ADD_PROPERTY( gdg_work_ai, UniWidgetsTypes::ObjectId )				/*!< свойство: id датчика ГДГ работает */
	ADD_PROPERTY( gdg_work_path, Glib::ustring )					/*!< свойство: картинка для для надписи состояния(ГДГ работает) */
	ADD_PROPERTY( gen_overload95_di, UniWidgetsTypes::ObjectId )			/*!< свойство: id датчика перегрузка 95% */
	ADD_PROPERTY( gen_overload95_path, Glib::ustring )				/*!< свойство: картинка для для надписи состояния(перегрузка 95%) */
	ADD_PROPERTY( gen_overload110_di, UniWidgetsTypes::ObjectId )			/*!< свойство: id датчика перегрузка 110% */
	ADD_PROPERTY( gen_overload110_path, Glib::ustring )				/*!< свойство: картинка для для надписи состояния(перегрузка 110%) */
	ADD_PROPERTY( indicator_u_ai, UniWidgetsTypes::ObjectId )				/*!< свойство: id датчика для индикатора напряжения */
	ADD_PROPERTY( indicator_p_ai, UniWidgetsTypes::ObjectId )				/*!< свойство: id датчика для индикатора мощности */
	ADD_PROPERTY( indicator_f_ai, UniWidgetsTypes::ObjectId )				/*!< свойство: id датчика для индикатора частоты тока */
	ADD_PROPERTY( indicator_i_ai, UniWidgetsTypes::ObjectId )				/*!< свойство: id датчика для индикатора силы тока */
	ADD_PROPERTY( indicator_threshold_u_warn, UniWidgetsTypes::ObjectId )		/*!< свойство: id датчика для порога напряжения(предупреждение) */
	ADD_PROPERTY( indicator_threshold_p_low_warn, UniWidgetsTypes::ObjectId )		/*!< свойство: id датчика для нижнего порога мощности(предупреждение) */
	ADD_PROPERTY( indicator_threshold_p_warn, UniWidgetsTypes::ObjectId )		/*!< свойство: id датчика для порога мощности(предупреждение) */
	ADD_PROPERTY( indicator_threshold_p_alarm, UniWidgetsTypes::ObjectId )		/*!< свойство: id датчика для порога мощности(авария) */
	ADD_PROPERTY( indicator_threshold_f_warn, UniWidgetsTypes::ObjectId )		/*!< свойство: id датчика для порога частоты(предупреждение) */
	ADD_PROPERTY( back_width, long )						/*!< свойство: ширина виджета */
	ADD_PROPERTY( back_height, long )						/*!< свойство: высота виджета */
	ADD_PROPERTY( x_control, long )							/*!< свойство: x-координата надписи состояния управления */
	ADD_PROPERTY( y_control, long )							/*!< свойство: y-координата надписи состояния управления */
	ADD_PROPERTY( w_control, long )							/*!< свойство: ширина надписи состояния управления */
	ADD_PROPERTY( h_control, long )							/*!< свойство: высота надписи состояния управления */
	ADD_PROPERTY( x_diesel_gen, long )						/*!< свойство: x-координата знака для дизеля и генератора */
	ADD_PROPERTY( y_diesel, long )							/*!< свойство: y-координата знака для дизеля */
	ADD_PROPERTY( w_h_diesel, long )						/*!< свойство: размеры знака для дизеля */
	ADD_PROPERTY( y_gen, long )							/*!< свойство: y-координата знака для генератора */
	ADD_PROPERTY( w_h_gen, long )							/*!< свойство: размеры знака для генератора */
	ADD_PROPERTY( x_gen_states, long )						/*!< свойство: x-координата надписи состояния ГДГ */
	ADD_PROPERTY( y_gen_states, long )						/*!< свойство: y-координата надписи состояния ГДГ */
	ADD_PROPERTY( w_gen_states, long )						/*!< свойство: ширина надписи состояния ГДГ */
	ADD_PROPERTY( h_gen_states, long )						/*!< свойство: высота надписи состояния ГДГ */
	ADD_PROPERTY( x_indicators_u_p, long )						/*!< свойство: x-координата индикаторов напряжения и мощности */
	ADD_PROPERTY( x_indicators_i_f, long )						/*!< свойство: x-координата индикаторов силы тока и частоты */
	ADD_PROPERTY( y_indicators_u, long )						/*!< свойство: y-координата индикатора напряжения */
	ADD_PROPERTY( y_indicators_p, long )						/*!< свойство: y-координата индикатора мощности */
	ADD_PROPERTY( y_indicators_i, long )						/*!< свойство: y-координата индикатора силы тока */
	ADD_PROPERTY( y_indicators_f, long )						/*!< свойство: y-координата индикатора частоты */
	ADD_PROPERTY( w_indicators_u_p, long )						/*!< свойство: ширина индикаторов напряжения и мощности */
	ADD_PROPERTY( h_indicators_u_p, long )						/*!< свойство: высота индикаторов напряжения и мощности */
	ADD_PROPERTY( w_indicators_i_f, long )						/*!< свойство: ширина индикаторов силы тока и частоты */
	ADD_PROPERTY( h_indicators_i_f, long )						/*!< свойство: высота индикаторов силы тока и частоты */
	ADD_PROPERTY( x_indicator_value_u_p, long )					/*!< свойство: x-координата индикаторов напряжения и мощности */
	ADD_PROPERTY( x_indicator_value_i_f, long )					/*!< свойство: x-координата индикаторов силы тока и частоты */
	ADD_PROPERTY( x_key, long )							/*!< свойство: x-координата знака ГА */
	ADD_PROPERTY( y_key, long )							/*!< свойство: y-координата знака ГА */
	ADD_PROPERTY( w_key, long )							/*!< свойство: ширина знака ГА */
	ADD_PROPERTY( h_key, long )							/*!< свойство: высота знака ГА */
	ADD_PROPERTY( x_name, long )							/*!< свойство: x-координата картинки названия виджета */
	ADD_PROPERTY( y_name, long )							/*!< свойство: y-координата картинки названия виджета */
	ADD_PROPERTY( w_name, long )							/*!< свойство: ширина картинки названия виджета */
	ADD_PROPERTY( h_name, long )							/*!< свойство: высота картинки названия виджета */
	ADD_PROPERTY( x_text_control, long )						/*!< свойство: x-координата текста состояния управления */
	ADD_PROPERTY( y_text_control, long )						/*!< свойство: y-координата текста состояния управления */
	ADD_PROPERTY( size_text_control, long )						/*!< свойство: размеры текста состояния управления */
	ADD_PROPERTY( color_text_control, Gdk::Color)					/*!< свойство: цвет текста состояния управления */
};

}
#endif
