#include <iostream>
#include <types.h>
#include "IndicatorFourState.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define INDICATORFOURSTATE_VALUE_AI        "value-ai"
#define INDICATORFOURSTATE_LINK_DI        "link-di"
#define INDICATORFOURSTATE_DIGITS        "digits"
#define INDICATORFOURSTATE_PRICISION        "pricision"
#define INDICATORFOURSTATE_FACTOR        "factor"
#define INDICATORFOURSTATE_THRESHOLDHIGH_WARN      "thresholdhigh_warn"
#define INDICATORFOURSTATE_THRESHOLDLOW_WARN      "thresholdlow_warn"
#define INDICATORFOURSTATE_THRESHOLDHIGH_ALARM      "thresholdhigh_alarm"
#define INDICATORFOURSTATE_THRESHOLDLOW_ALARM      "thresholdlow_alarm"
#define INDICATORFOURSTATE_BLINKING_LOW        "blinking-low"
#define INDICATORFOURSTATE_SELF_STATE_AI      "self-state-ai"
#define INDICATORFOURSTATE_NODE          "node"
#define INDICATORFOURSTATE_OFF_PATH        "off-path"
#define INDICATORFOURSTATE_WARN_PATH        "warn-path"
#define INDICATORFOURSTATE_ALARM_PATH        "alarm-path"
#define  INDICATORFOURSTATE_HEIGHT        "height"
#define  INDICATORFOURSTATE_WIDTH        "width"
#define  INDICATORFOURSTATE_IND_RECT_X        "ind-rect-x"
#define  INDICATORFOURSTATE_IND_RECT_Y        "ind-rect-y"
#define SVG_PATH            "svg-path"
#define INDICATOR_LOCK_VIEW_HIGH      "indicator-lock-view-high"
#define INDICATOR_LOCK_VIEW_LOW        "indicator-lock-view-low"
// -------------------------------------------------------------------------
#define INIT_INDICATORFOURSTATE_PROPERTIES() \
  value_ai(*this, INDICATORFOURSTATE_VALUE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,property_link_di(*this, INDICATORFOURSTATE_LINK_DI , UniWidgetsTypes::DefaultObjectId) \
  ,property_digits(*this, INDICATORFOURSTATE_DIGITS , 4) \
  ,property_precision(*this, INDICATORFOURSTATE_PRICISION , 0) \
  ,property_factor(*this, INDICATORFOURSTATE_FACTOR, 1) \
  ,svg_path(*this, SVG_PATH ,"/usr/share/yauza/19/svg/") \
  ,thresholdhigh_warn(*this, INDICATORFOURSTATE_THRESHOLDHIGH_WARN , UniWidgetsTypes::DefaultObjectId) \
  ,thresholdhigh_alarm(*this, INDICATORFOURSTATE_THRESHOLDHIGH_ALARM , UniWidgetsTypes::DefaultObjectId) \
  ,thresholdlow_warn(*this, INDICATORFOURSTATE_THRESHOLDLOW_WARN , UniWidgetsTypes::DefaultObjectId) \
  ,thresholdlow_alarm(*this, INDICATORFOURSTATE_THRESHOLDLOW_ALARM , UniWidgetsTypes::DefaultObjectId) \
  ,blinking_low(*this, INDICATORFOURSTATE_BLINKING_LOW , true) \
  ,self_state_ai(*this, INDICATORFOURSTATE_SELF_STATE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,node(*this, INDICATORFOURSTATE_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,off_path(*this, INDICATORFOURSTATE_OFF_PATH, "" ) \
  ,warn_path(*this, INDICATORFOURSTATE_WARN_PATH, "" ) \
  ,alarm_path(*this, INDICATORFOURSTATE_ALARM_PATH, "" ) \
  ,width(*this, INDICATORFOURSTATE_WIDTH, 106 ) \
  ,height(*this, INDICATORFOURSTATE_HEIGHT, 34 ) \
  ,ind_rect_x(*this, INDICATORFOURSTATE_IND_RECT_X, 8) \
  ,ind_rect_y(*this, INDICATORFOURSTATE_IND_RECT_Y, 6) \
  ,indicator_lock_view_high(*this, INDICATOR_LOCK_VIEW_HIGH, false) \
  ,indicator_lock_view_low(*this, INDICATOR_LOCK_VIEW_LOW, false)
// -------------------------------------------------------------------------
/* Static const definitions */
const Gdk::Rectangle IndicatorFourState::highlevel_rect(25, 10, 11, 10);
const Gdk::Rectangle IndicatorFourState::lowlevel_rect(25, 21, 11, 10);
/**/
const string IndicatorFourState::highlevel_off_path = "High_grey_level.svg";
const string IndicatorFourState::highlevel_warn_path = "High_yellow_level.svg";
const string IndicatorFourState::highlevel_alarm_path = "High_red_level.svg";
/**/
const string IndicatorFourState::lowlevel_off_path = "low_grey_level.svg";
const string IndicatorFourState::lowlevel_warn_path = "low_yellow_level.svg";
const string IndicatorFourState::lowlevel_alarm_path = "low_red_level.svg";
// -------------------------------------------------------------------------
void IndicatorFourState::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback<IndicatorFourState>;

  is_configured = false;
  add_child(&link_, typeLogic);
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  /*Добавляем к родительскому контейнеру с типом*/
  add_child( &frame_,typeObject );
  add_child( &highlevel_,typeObject );
  add_child( &lowlevel_,typeObject );
  add_child( &indicator_,typeObject );
   
  frame_rect = new Gdk::Rectangle(0, 0, get_width(), get_height());
  indicator_rect = new Gdk::Rectangle(get_ind_rect_x(), get_ind_rect_y(), get_width(), get_height());
  connect_property_changed("width", sigc::mem_fun(*this, &IndicatorFourState::on_rectangle_changed));
  connect_property_changed("height", sigc::mem_fun(*this, &IndicatorFourState::on_rectangle_changed));
  connect_property_changed("ind_rect_x", sigc::mem_fun(*this, &IndicatorFourState::on_rectangle_changed));
}
// -------------------------------------------------------------------------
IndicatorFourState::IndicatorFourState() :
  Glib::ObjectBase("indicatorfourstate")
  ,INIT_INDICATORFOURSTATE_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
IndicatorFourState::IndicatorFourState(SimpleObject::BaseObjectType* gobject) :
  SimpleObject(gobject)
  ,INIT_INDICATORFOURSTATE_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
IndicatorFourState::~IndicatorFourState()
{
}
// -------------------------------------------------------------------------
void IndicatorFourState::on_rectangle_changed()
{
  set_size_request(-1,-1);
  frame_rect->set_x(0);
  frame_rect->set_y(0);
  frame_rect->set_width(get_width());
  frame_rect->set_height(get_height());
  indicator_rect->set_x(get_ind_rect_x());
  indicator_rect->set_y(get_ind_rect_y());
  indicator_rect->set_width(get_width());
  indicator_rect->set_height(get_height());
  queue_resize();
}
// -------------------------------------------------------------------------
void IndicatorFourState::on_realize()
{
  SimpleObject::on_realize();
  on_rectangle_changed();

  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void IndicatorFourState::on_connect() throw()
{
  SimpleObject::on_connect();
  on_rectangle_changed();

  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void IndicatorFourState::on_configure()
{
  link_.set_link_di( get_property_link_di() );
  link_.set_node( get_node() );
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  /* Create background Object */
  frame_.set_rect(*frame_rect);
  frame_.set_path(mOFF, get_off_path());
  frame_.insert_mode(mOFF,-1,false);
  if( get_thresholdhigh_warn() != UniWidgetsTypes::DefaultObjectId )
  {
    frame_.set_path(mWARNING_HIGH, get_warn_path());
    frame_.insert_mode(mWARNING_HIGH,-1,true);
    frame_.set_mode_state_high_warn(mWARNING_HIGH);
    frame_.set_state_ai_high_warn(get_thresholdhigh_warn());
    frame_.set_node_high_warn(get_node());
  }

  if( get_thresholdlow_warn() != UniWidgetsTypes::DefaultObjectId )
  {
    frame_.set_path(mWARNING_LOW, get_warn_path());
    frame_.insert_mode(mWARNING_LOW,-1,true);
    frame_.set_mode_state_low_warn(mWARNING_LOW);
    frame_.set_state_ai_low_warn(get_thresholdlow_warn());
    frame_.set_node_low_warn(get_node());
  }

  if( get_thresholdhigh_alarm() != UniWidgetsTypes::DefaultObjectId )
  {
    frame_.set_path(mALARM_HIGH, get_alarm_path());
    frame_.insert_mode(mALARM_HIGH,-1,true);
    frame_.set_mode_state_high_alarm(mALARM_HIGH);
    frame_.set_state_ai_high_alarm(get_thresholdhigh_alarm());
    frame_.set_node_high_alarm(get_node());
  }

  if( get_thresholdlow_alarm() != UniWidgetsTypes::DefaultObjectId )
  {
    frame_.set_path(mALARM_LOW, get_alarm_path());
    frame_.insert_mode(mALARM_LOW,-1,true);
    frame_.set_mode_state_low_alarm(mALARM_LOW);
    frame_.set_state_ai_low_alarm(get_thresholdlow_alarm());
    frame_.set_node_low_alarm(get_node());
  }

  frame_.set_state_obj(get_self_state_ai());

  frame_.set_state_blink_low(get_blinking_low());

  frame_.configure();

  /* The SimpleObject::on_realize will be called after this put */
  put(frame_,
      frame_rect->get_x(),
      frame_rect->get_y());
  frame_.show();

  /* Create first state Object */
  if(get_thresholdhigh_warn() != UniWidgetsTypes::DefaultObjectId || get_thresholdhigh_alarm() != UniWidgetsTypes::DefaultObjectId)
  {
    highlevel_.set_rect(highlevel_rect);

    highlevel_.set_path(mOFF, get_svg_path() + highlevel_off_path);
    highlevel_.insert_mode(mOFF,-1,false);
    if(get_thresholdhigh_warn() != UniWidgetsTypes::DefaultObjectId)
    {
      highlevel_.set_path(mWARNING_HIGH,get_svg_path() + highlevel_warn_path);
      highlevel_.insert_mode(mWARNING_HIGH,-1,true);
      highlevel_.set_mode_state_low(mWARNING_HIGH);
      highlevel_.set_state_ai_low(get_thresholdhigh_warn());
      highlevel_.set_node_low(get_node());
    }
    if(get_thresholdhigh_alarm() != UniWidgetsTypes::DefaultObjectId)
    {
      highlevel_.set_path(mALARM_HIGH,get_svg_path() + highlevel_alarm_path);
      highlevel_.insert_mode(mALARM_HIGH,-1,true);
      highlevel_.set_mode_state_high(mALARM_HIGH);
      highlevel_.set_state_ai_high(get_thresholdhigh_alarm());
      highlevel_.set_node_high(get_node());
    }
    highlevel_.set_lock_view(get_indicator_lock_view_low());
    highlevel_.configure();
  
    /* The SimpleObject::on_realize will be called after this put */
    put(highlevel_,
        highlevel_rect.get_x(),
        highlevel_rect.get_y());
    highlevel_.show();
  }
  /*State 2*/
  if(get_thresholdlow_warn() != UniWidgetsTypes::DefaultObjectId || get_thresholdlow_alarm() != UniWidgetsTypes::DefaultObjectId)
  {
    lowlevel_.set_rect(lowlevel_rect);

    lowlevel_.set_rect(lowlevel_rect);
    lowlevel_.set_path(mOFF, get_svg_path() + lowlevel_off_path);
    lowlevel_.insert_mode(mOFF,-1,false);
    if(get_thresholdlow_warn() != UniWidgetsTypes::DefaultObjectId)
    {
      lowlevel_.set_path(mWARNING_LOW,get_svg_path() + lowlevel_warn_path);
      lowlevel_.insert_mode(mWARNING_LOW,-1,true);
      lowlevel_.set_mode_state_low(mWARNING_LOW);
      lowlevel_.set_state_ai_low(get_thresholdlow_warn());
      lowlevel_.set_node_low(get_node());
    }
    if(get_thresholdlow_alarm() != UniWidgetsTypes::DefaultObjectId)
    {
      lowlevel_.set_path(mALARM_LOW,get_svg_path() + lowlevel_alarm_path);
      lowlevel_.insert_mode(mALARM_LOW,-1,true);
      lowlevel_.set_mode_state_high(mALARM_LOW);
      lowlevel_.set_state_ai_high(get_thresholdlow_alarm());
      lowlevel_.set_node_high(get_node());
    }
    lowlevel_.set_state_blink_low(get_blinking_low());//вкл/выкл мигание при сигналах
        lowlevel_.set_state_blink_high(get_blinking_low());
    lowlevel_.set_lock_view(get_indicator_lock_view_low());
  
    lowlevel_.configure();
  
    /* The SimpleObject::on_realize will be called after this put */
    put(lowlevel_,
        lowlevel_rect.get_x(),
        lowlevel_rect.get_y());
    lowlevel_.show();
  }
  /*Indicator 1*/
  indicator_.set_rect(*indicator_rect);
  indicator_.set_state_di_colorlow_warn(get_thresholdlow_warn());
  indicator_.set_state_di_colorhigh_warn(get_thresholdhigh_warn());
  indicator_.set_mode_colorlow_warn(6);
  indicator_.set_mode_colorhigh_warn(6);

  indicator_.set_state_di_colorlow_alarm(get_thresholdlow_alarm());
  indicator_.set_state_di_colorhigh_alarm(get_thresholdhigh_alarm());
  indicator_.set_mode_colorlow_alarm(7);
  indicator_.set_mode_colorhigh_alarm(7);

  indicator_.set_precision(get_property_precision());
  indicator_.set_digits(get_property_digits());
  indicator_.set_factor(get_property_factor());

  indicator_.set_value_ai(get_value_ai());
  indicator_.set_node(get_node());

  indicator_.set_state_blink_low(get_blinking_low());

  indicator_.configure();

  /* The SimpleObject::on_realize will be called after this put */
  
  put(indicator_,
      indicator_rect->get_x(),
      indicator_rect->get_y());
  /* Show all objects */
  indicator_.show();
  show();
}
// -------------------------------------------------------------------------
