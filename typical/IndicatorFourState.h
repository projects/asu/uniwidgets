#ifndef _INDICATORFOURSTATE_H
#define _INDICATORFOURSTATE_H
// -------------------------------------------------------------------------
#include <string>
#include <map>
#include <objects/SimpleObject.h>
#include <objects/LinkLogic.h>
#include <typical/TypicalTwoState.h>
#include <typical/TypicalFourState.h>
#include <typical/TypicalIndicatorBlink.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой виджет индикатор с 4мя порогами.
 * \par
 * Виджет отображает индикатор с двумя верхними(предупреждение и авария)
 * и двумя нижними порогами. Пороги отображаются слева от цифрового индикатора
 * в виде треугольников. Рамка и цифры индикатора также мигают и меняют цвет в
 * зависимости от типа сработавшего порога.
 * 
 * \n Пример индикатора: \n \n
 * \image html indicator_four_state.png
*/
class IndicatorFourState : public SimpleObject
{
public:
  IndicatorFourState();
  explicit IndicatorFourState(SimpleObject::BaseObjectType* gobject);
  virtual ~IndicatorFourState();

  /* Constants */
  static const Gdk::Rectangle highlevel_rect;      /*!< координаты и размер области отображения вернего уровня */
  static const Gdk::Rectangle lowlevel_rect;      /*!< координаты и размер области отображения нижнего уровня */

  static const std::string highlevel_off_path;        /*!< путь к картинке верхнего уровня(выключен) */
  static const std::string highlevel_warn_path;      /*!< путь к картинке верхнего уровня(предупреждение) */
  static const std::string highlevel_alarm_path;      /*!< путь к картинке верхнего уровня(авария) */

  static const std::string lowlevel_off_path;        /*!< путь к картинке нижнего уровня(выключен) */
  static const std::string lowlevel_warn_path;        /*!< путь к картинке нижнего уровня(предупреждение) */
  static const std::string lowlevel_alarm_path;      /*!< путь к картинке нижнего уровня(авария) */

  sigc::connection button_release_conn_;        /*!< ссылка на коннектор для события signal_button_release_event */

protected:
  /* Handlers */
  virtual void on_realize();
  virtual void on_connect() throw();

private:
  /* Variables */
  TypicalFourState frame_;
  TypicalTwoState highlevel_;
  TypicalTwoState lowlevel_;
  /*Индикатор*/
  TypicalIndicatorBlink indicator_;
  Gdk::Rectangle* frame_rect;
  Gdk::Rectangle* indicator_rect;
  LinkLogic link_;
  
  bool is_configured;

  /* Methods */
  void constructor();
  void on_rectangle_changed();
  void on_configure();

  DISALLOW_COPY_AND_ASSIGN(IndicatorFourState);

  /* Properties */
  ADD_PROPERTY( value_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика состояния */
  ADD_PROPERTY( property_link_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика состояния связи с SharedMemory */
  ADD_PROPERTY( property_digits, int )        /*!< свойство: максимальное количество цифр в числе*/
  ADD_PROPERTY( property_precision, int )        /*!< свойство: точность после запятой */
  ADD_PROPERTY( property_factor, double )        /*!< свойство: поправочный коэффициент */
  ADD_PROPERTY( svg_path, Glib::ustring )        /*!< свойство: путь к директории с картинкам */
  ADD_PROPERTY( thresholdhigh_warn, UniWidgetsTypes::ObjectId )  /*!< свойство: id датчика верхнего порога уровня(предупреждение)*/
  ADD_PROPERTY( thresholdhigh_alarm, UniWidgetsTypes::ObjectId )  /*!< свойство: id датчика верхнего порога уровня(авария) */
  ADD_PROPERTY( thresholdlow_warn, UniWidgetsTypes::ObjectId )  /*!< свойство: id датчика нижнего порога уровня(предупреждение) */
  ADD_PROPERTY( thresholdlow_alarm, UniWidgetsTypes::ObjectId )  /*!< свойство: id датчика нижнего порога уровня(авария) */
  ADD_PROPERTY( blinking_low, bool )        /*!< свойство: включение мигания при срабатывании нижнего порога уровня*/
  ADD_PROPERTY( self_state_ai, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика внутреннего состояния */
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )      /*!< свойство: id узла */
  ADD_PROPERTY( off_path, Glib::ustring )        /*!< свойство: картинка для индикатора(выключен) */
  ADD_PROPERTY( warn_path, Glib::ustring )      /*!< свойство: картинка для индикатора(предупреждение) */
  ADD_PROPERTY( alarm_path, Glib::ustring )      /*!< свойство: картинка для индикатора(авария) */
  ADD_PROPERTY( width, long )          /*!< свойство: ширина виджета */
  ADD_PROPERTY( height, long )          /*!< свойство: высота виджета */
  ADD_PROPERTY( ind_rect_x, long )        /*!< свойство: x-координата цифр индикатора*/
  ADD_PROPERTY( ind_rect_y, long )        /*!< свойство: y-координата цифр индикатора*/
  ADD_PROPERTY( indicator_lock_view_high, bool);      /*!< свойство: блокировка экрана по АПС от верхнего порога уровня */
  ADD_PROPERTY( indicator_lock_view_low, bool);      /*!< свойство: блокировка экрана по АПС от нижнего порога уровня */
};

}
#endif
