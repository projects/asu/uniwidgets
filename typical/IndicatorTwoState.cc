#include <iostream>
#include <types.h>
#include "IndicatorTwoState.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
// #define TYPE_THRESHOLD (thresholdtype_get_type ())
#define INDICATORTWOSTATE_VALUE_AI      "value-ai"
#define INDICATORTWOSTATE_LINK_DI      "link-di"
#define INDICATORTWOSTATE_DIGITS      "digits"
#define INDICATORTWOSTATE_PRICISION      "pricision"
#define INDICATORTWOSTATE_FACTOR      "factor"
#define INDICATORTWOSTATE_THRESHOLDHIGH_WARN    "thresholdhigh_warn"
#define INDICATORTWOSTATE_THRESHOLDHIGH_TYPE    "thresholdhigh_type"
#define INDICATORTWOSTATE_THRESHOLDLOW_WARN    "thresholdlow_warn"
#define INDICATORTWOSTATE_THRESHOLDLOW_TYPE    "thresholdlow_type"
#define INDICATORTWOSTATE_SELF_STATE_AI      "self-state-ai"
#define INDICATORTWOSTATE_NODE        "node"
#define INDICATORTWOSTATE_OFF_PATH      "off-path"
#define INDICATORTWOSTATE_WARN_PATH      "warn-path"
#define INDICATORTWOSTATE_ALARM_PATH      "alarm-path"
#define  INDICATORTWOSTATE_HEIGHT      "height"
#define  INDICATORTWOSTATE_WIDTH        "width"
#define  INDICATORTWOSTATE_IND_RECT_X      "ind-rect-x"
#define  INDICATORTWOSTATE_IND_RECT_Y      "ind-rect-y"
#define SVG_PATH          "svg-path"
#define INDICATOR_LOCK_VIEW_HIGH      "indicator-lock-view-high"
#define INDICATOR_LOCK_VIEW_LOW        "indicator-lock-view-low"
#define INDICATORTWOSTATE_FONT        "font"
#define INDICATORTWOSTATE_FONT_COLOR      "font-color"
// -------------------------------------------------------------------------
#define INIT_INDICATORTWOSTATE_PROPERTIES() \
  value_ai(*this, INDICATORTWOSTATE_VALUE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,property_link_di(*this, INDICATORTWOSTATE_LINK_DI , UniWidgetsTypes::DefaultObjectId) \
  ,property_digits(*this, INDICATORTWOSTATE_DIGITS , 4) \
  ,property_precision(*this, INDICATORTWOSTATE_PRICISION , 0) \
  ,property_factor(*this, INDICATORTWOSTATE_FACTOR, 1) \
  ,svg_path(*this, SVG_PATH ,"/usr/share/yauza/19/svg/") \
  ,thresholdhigh_warn(*this, INDICATORTWOSTATE_THRESHOLDHIGH_WARN , UniWidgetsTypes::DefaultObjectId) \
  ,thresholdhigh_type(*this, INDICATORTWOSTATE_THRESHOLDHIGH_TYPE , mWARNING_type) \
  ,thresholdlow_warn(*this, INDICATORTWOSTATE_THRESHOLDLOW_WARN , UniWidgetsTypes::DefaultObjectId) \
  ,thresholdlow_type(*this, INDICATORTWOSTATE_THRESHOLDLOW_TYPE , mWARNING_type) \
  ,self_state_ai(*this, INDICATORTWOSTATE_SELF_STATE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,node(*this, INDICATORTWOSTATE_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,off_path(*this, INDICATORTWOSTATE_OFF_PATH, "" ) \
  ,warn_path(*this, INDICATORTWOSTATE_WARN_PATH, "" ) \
  ,alarm_path(*this, INDICATORTWOSTATE_ALARM_PATH, "" ) \
  ,width(*this, INDICATORTWOSTATE_WIDTH, 106 ) \
  ,height(*this, INDICATORTWOSTATE_HEIGHT, 34 ) \
  ,ind_rect_x(*this, INDICATORTWOSTATE_IND_RECT_X, 8) \
  ,ind_rect_y(*this, INDICATORTWOSTATE_IND_RECT_Y, 6) \
  ,indicator_lock_view_high(*this, INDICATOR_LOCK_VIEW_HIGH, false) \
  ,indicator_lock_view_low(*this, INDICATOR_LOCK_VIEW_LOW, false) \
  ,font(*this, INDICATORTWOSTATE_FONT, "") \
  ,font_color(*this, INDICATORTWOSTATE_FONT_COLOR, Gdk::Color("green"))
// -------------------------------------------------------------------------
/* Static const definitions */
const Gdk::Rectangle IndicatorTwoState::highlevel_rect(24, 10, 11, 10);
const Gdk::Rectangle IndicatorTwoState::lowlevel_rect(24, 21, 11, 10);
//const Gdk::Rectangle IndicatorTwoState::indicator_rect(8, 4, 105, 33);

/**/
const string IndicatorTwoState::highlevel_off_path = "High_grey_level.svg";
const string IndicatorTwoState::highlevel_warn_path = "High_yellow_level.svg";
const string IndicatorTwoState::highlevel_alarm_path = "High_red_level.svg";
/**/
const string IndicatorTwoState::lowlevel_off_path = "low_grey_level.svg";
const string IndicatorTwoState::lowlevel_warn_path = "low_yellow_level.svg";
const string IndicatorTwoState::lowlevel_alarm_path = "low_red_level.svg";
// -------------------------------------------------------------------------
void IndicatorTwoState::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback<IndicatorTwoState>;

  is_configured = false;
  add_child(&link_, typeLogic);
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  /*Добавляем к родительскому контейнеру с типом*/
  add_child( &frame_,typeObject );
  add_child( &highlevel_,typeObject );
  add_child( &lowlevel_,typeObject );
  add_child( &indicator_,typeObject );

  frame_rect = new Gdk::Rectangle(0, 0, get_width(), get_height());
  indicator_rect = new Gdk::Rectangle(get_ind_rect_x(), get_ind_rect_y(), get_width(), get_height());
  connect_property_changed("width", sigc::mem_fun(*this, &IndicatorTwoState::on_rectangle_changed));
  connect_property_changed("height", sigc::mem_fun(*this, &IndicatorTwoState::on_rectangle_changed));
  connect_property_changed("ind_rect_x", sigc::mem_fun(*this, &IndicatorTwoState::on_rectangle_changed));
}
// -------------------------------------------------------------------------
IndicatorTwoState::IndicatorTwoState() :
  Glib::ObjectBase("indicatortwostate")
  ,INIT_INDICATORTWOSTATE_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
IndicatorTwoState::IndicatorTwoState(SimpleObject::BaseObjectType* gobject) :
  SimpleObject(gobject)
  ,INIT_INDICATORTWOSTATE_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
IndicatorTwoState::~IndicatorTwoState()
{
}
// -------------------------------------------------------------------------
void IndicatorTwoState::on_rectangle_changed()
{
  set_size_request(-1,-1);
  frame_rect->set_x(0);
  frame_rect->set_y(0);
  frame_rect->set_width(get_width());
  frame_rect->set_height(get_height());

  indicator_rect->set_x(get_ind_rect_x());
  indicator_rect->set_y(get_ind_rect_y());
  indicator_rect->set_width(get_width());
  indicator_rect->set_height(get_height());
  queue_resize();
}
// -------------------------------------------------------------------------
void IndicatorTwoState::on_realize()
{
  SimpleObject::on_realize();

  on_rectangle_changed();
  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void IndicatorTwoState::on_connect() throw()
{
  SimpleObject::on_connect();
  
  on_rectangle_changed();
  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void IndicatorTwoState::on_configure()
{
  link_.set_link_di( get_property_link_di() );
  link_.set_node( get_node() );
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  /* Create background Object */
  frame_.set_rect(*frame_rect);
  frame_.set_path(mOFF, get_off_path());
  frame_.insert_mode(mOFF,-1,false);

  if( get_thresholdhigh_warn() != UniWidgetsTypes::DefaultObjectId )
  {
    if(get_thresholdhigh_type() == mWARNING_type )
    {
      frame_.set_path(mWARNING_HIGH, get_warn_path());
      frame_.insert_mode(mWARNING_HIGH,-1,true);
      frame_.set_mode_state_high(mWARNING_HIGH);
      frame_.set_state_ai_high(get_thresholdhigh_warn());
    }
    else if(get_thresholdhigh_type() == mALARM_type )
    {
      frame_.set_path(mALARM_HIGH, get_alarm_path());
      frame_.insert_mode(mALARM_HIGH,-1,true);
      frame_.set_mode_state_high(mALARM_HIGH);
      frame_.set_state_ai_high(get_thresholdhigh_warn());
    }
    frame_.set_node_high(get_node());
  }
  if( get_thresholdlow_warn() != UniWidgetsTypes::DefaultObjectId )
  {
    if(get_thresholdlow_type() == mWARNING_type )
    {
      frame_.set_path(mWARNING_LOW, get_warn_path());
      frame_.insert_mode(mWARNING_LOW,-1,true);
      frame_.set_mode_state_low(mWARNING_LOW);
      frame_.set_state_ai_low(get_thresholdlow_warn());
    }
    else if(get_thresholdlow_type() == mALARM_type )
    {
      frame_.set_path(mALARM_LOW, get_alarm_path());
      frame_.insert_mode(mALARM_LOW,-1,true);
      frame_.set_mode_state_low(mALARM_LOW);
      frame_.set_state_ai_low(get_thresholdlow_warn());
    }
    frame_.set_node_low(get_node());
  }
  frame_.set_state_obj(get_self_state_ai());
  frame_.configure();

  /* The SimpleObject::on_realize will be called after this put */
  put(frame_,
      frame_rect->get_x(),
      frame_rect->get_y());

  frame_.show();
  /* Create first state Object */
  if(get_thresholdhigh_warn() != UniWidgetsTypes::DefaultObjectId)
  {
    highlevel_.set_rect(highlevel_rect);

    highlevel_.set_path(mOFF, get_svg_path() + highlevel_off_path);
    highlevel_.insert_mode(mOFF,-1,false);
    if(get_thresholdhigh_type() == mWARNING_type )
    {
      highlevel_.set_path(mWARNING, get_svg_path() + highlevel_warn_path);
      highlevel_.insert_mode(mWARNING,-1,true);
      highlevel_.set_mode_state(mWARNING);
    }
    else if(get_thresholdhigh_type() == mALARM_type )
    {
      highlevel_.set_path(mALARM, get_svg_path() + highlevel_alarm_path);
      highlevel_.insert_mode(mALARM,-1,true);
      highlevel_.set_mode_state(mALARM);
    }

    highlevel_.set_state_ai(get_thresholdhigh_warn());
    highlevel_.set_node(get_node());
    highlevel_.set_lock_view(get_indicator_lock_view_high());
    highlevel_.configure();
    /* The SimpleObject::on_realize will be called after this put */
    put(highlevel_,
        highlevel_rect.get_x(),
        highlevel_rect.get_y());

    highlevel_.show();
  }
  /*State 2*/
  if(get_thresholdlow_warn() != UniWidgetsTypes::DefaultObjectId)
  {
    lowlevel_.set_rect(lowlevel_rect);
    lowlevel_.set_path(mOFF, get_svg_path() + lowlevel_off_path);
    lowlevel_.insert_mode(mOFF,-1,false);
    if(get_thresholdlow_type() == mWARNING_type )
    {
      lowlevel_.set_path(mWARNING, get_svg_path() + lowlevel_warn_path);
      lowlevel_.insert_mode(mWARNING,-1,true);
      lowlevel_.set_mode_state(mWARNING);
    }
    else if(get_thresholdlow_type() == mALARM_type )
    {
      lowlevel_.set_path(mALARM, get_svg_path() + lowlevel_alarm_path);
      lowlevel_.insert_mode(mALARM,-1,true);
      lowlevel_.set_mode_state(mALARM);
    }
  
    lowlevel_.set_state_ai(get_thresholdlow_warn());
    lowlevel_.set_node(get_node());
    lowlevel_.set_lock_view(get_indicator_lock_view_low());
  
    lowlevel_.configure();
  
    /* The SimpleObject::on_realize will be called after this put */
    put(lowlevel_,
        lowlevel_rect.get_x(),
        lowlevel_rect.get_y());
    
    lowlevel_.show();
  }
  /*Indicator 1*/
  indicator_.set_rect(*indicator_rect);
  indicator_.set_state_di_colorlow_warn(get_thresholdlow_warn());
  indicator_.set_state_di_colorhigh_warn(get_thresholdhigh_warn());

  if(get_thresholdlow_type() == mWARNING_type )
    indicator_.set_mode_colorlow_warn(mWARNING);
  else if(get_thresholdlow_type() == mALARM_type )
    indicator_.set_mode_colorlow_warn(mALARM);

  if(get_thresholdhigh_type() == mWARNING_type )
    indicator_.set_mode_colorhigh_warn(mWARNING);
  else if(get_thresholdhigh_type() == mALARM_type )
    indicator_.set_mode_colorhigh_warn(mALARM);

  indicator_.set_precision(get_property_precision());
  indicator_.set_digits(get_property_digits());
  indicator_.set_factor(get_property_factor());

  indicator_.set_value_ai(get_value_ai());
  indicator_.set_node(get_node());

  if(!get_font().empty() && indicator_.get_indicator())
  {
    indicator_.get_indicator()->set_property_font_name_(get_font());
    indicator_.get_indicator()->set_property_on_font_name_(get_font());
    indicator_.get_indicator()->set_property_font_color_(get_font_color());
    indicator_.get_indicator()->set_property_state_(false);

//     indicator_.get_indicator()->set_font(get_font());
//     indicator_.get_indicator()->set_default_color(get_font_color());
  }
  indicator_.configure();
  /* The SimpleObject::on_realize will be called after this put */
  put(indicator_,
      indicator_rect->get_x(),
      indicator_rect->get_y());
  /* Show all objects */
  show();
  indicator_.show();
}
// -------------------------------------------------------------------------
