#include <iostream>
#include <types.h>
#include <components/EAD.h>
#include <components/CAD.h>
#include "RIndicatingInstrument.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define RINDICATINGINSTRUMENT_VALUE_AI      "value-ai"
// #define RINDICATINGINSTRUMENT_LINK_DI    "link-di"
#define RINDICATINGINSTRUMENT_NODE      "node"
#define RINDICATINGINSTRUMENT_TYPE      "type-of-device"
#define RINDICATINGINSTRUMENT_NAME_DEVICE    "name-of-device"
#define RINDICATINGINSTRUMENT_MIN_VAL      "min-value"
#define RINDICATINGINSTRUMENT_MAX_VAL      "max-value"
#define RINDICATINGINSTRUMENT_STEP_VAL      "step-value"
#define RINDICATINGINSTRUMENT_COLOR_SECTORS    "color-sectors"
#define RINDICATINGINSTRUMENT_LAG      "lag"
#define RINDICATINGINSTRUMENT_OPTIM      "optimization"
#define RINDICATINGINSTRUMENT_SCALEMARKS_NUMBER    "scale-marks-number"
#define RINDICATINGINSTRUMENT_SCALEMARKS_FONTSIZE  "scale-marks-font-size"
#define RINDICATINGINSTRUMENT_DIGITALVALUE_FONTSIZE  "digital-value-font-size"
#define RINDICATINGINSTRUMENT_LABEL_FONTSIZE    "label-font-size"
#define RINDICATINGINSTRUMENT_SCALEMARK_WIDTH    "scale-mark-width"
#define RINDICATINGINSTRUMENT_COLORLINE_WIDTH    "color-line-width"
// -------------------------------------------------------------------------
#define INIT_RINDICATINGINSTRUMENT_PROPERTIES() \
  value_ai(*this, RINDICATINGINSTRUMENT_VALUE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,node(*this, RINDICATINGINSTRUMENT_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,type(*this, RINDICATINGINSTRUMENT_TYPE, UniWidgetsTypes::AD_type) \
  ,name_device(*this, RINDICATINGINSTRUMENT_NAME_DEVICE, "dev") \
  ,min_value(*this, RINDICATINGINSTRUMENT_MIN_VAL, 0) \
  ,max_value(*this, RINDICATINGINSTRUMENT_MAX_VAL, 100) \
  ,step_value(*this, RINDICATINGINSTRUMENT_STEP_VAL, 10) \
  ,color_sectors(*this, RINDICATINGINSTRUMENT_COLOR_SECTORS, "") \
  ,lag(*this, RINDICATINGINSTRUMENT_LAG , 6) \
  ,optimization(*this, RINDICATINGINSTRUMENT_OPTIM , true) \
  ,scalemarks_number(*this,RINDICATINGINSTRUMENT_SCALEMARKS_NUMBER,5) \
  ,scalemarks_fontsize(*this,RINDICATINGINSTRUMENT_SCALEMARKS_FONTSIZE,14) \
  ,digitalvalue_fontsize(*this,RINDICATINGINSTRUMENT_DIGITALVALUE_FONTSIZE,24) \
  ,label_fontsize(*this,RINDICATINGINSTRUMENT_LABEL_FONTSIZE,16) \
  ,scalemark_width(*this,RINDICATINGINSTRUMENT_SCALEMARK_WIDTH,3) \
  ,colorline_width(*this,RINDICATINGINSTRUMENT_COLORLINE_WIDTH,16)
// -------------------------------------------------------------------------
void RIndicatingInstrument::update_view()
{
  if(!is_configured)
    return;

  AD *new_device = NULL;
  AD *old_device = NULL;
  //Создание нового объекта виджета
  if(get_type() == UniWidgetsTypes::AD_type)
  {
    new_device = new AD();
  }
  else if(get_type() == UniWidgetsTypes::EAD_type)
  {
    new_device = new EAD();
  }
  else if(get_type() == UniWidgetsTypes::CAD_type)
  {
    new_device = new CAD();
  }
  else
  {
    new_device = new AD();
  }
  //Обмен указателей на объект
  old_device = device;
  device = new_device;
  //Уничтожение старого объекта,если он существовал
  if(old_device && is_device )
    delete old_device;
  //Показать и перерисовать виджет в контейнере
  configure_device();
  device->show();
  is_device = true;
  increase_child_order(device);
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::update_name()
{
  if(!is_device)
    return;
  device->setFacialString(get_name_device());
  queue_draw();
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::update_scale()
{
  if(!is_device)
    return;
  device->setScale(get_min_value(),get_max_value(),get_step_value());
  queue_draw();
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::update_colorsectors()
{
  if(!is_device)
    return;
  configure_colorsectors();
  queue_draw();
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::update_lagopti()
{
  if(!is_device)
    return;
  device->setInerc(get_lag());
  device->setGLO(get_optimization());
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::update_fonts()
{
  if(!is_device)
    return;

  device->setScaleMarksNumber(get_scalemarks_number());
  device->setMarksFontSize(get_scalemarks_fontsize());
  device->setDigitFontSize(get_digitalvalue_fontsize());
  device->setLabelFontSize(get_label_fontsize());
  device->setScaleMarkWidth(get_scalemark_width());
  device->setColorLineWidth(get_colorline_width());
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback<RIndicatingInstrument>;

  is_configured = false;
  is_device = false;
  logic = new ADLogic();
  add_child(logic, typeLogic);
#if GTK_VERSION_GE(2,18)
  logic->set_visible(false);
#else
  logic->hide();
#endif
//   add_child(&link_, typeLogic);
// #if GTK_VERSION_GE(2,18)
//   link_.set_visible(false);
// #else
//   link_.hide();
// #endif
  //Обновление параметров виджета
  connect_property_changed(RINDICATINGINSTRUMENT_TYPE, sigc::mem_fun(*this, &RIndicatingInstrument::update_view));
  connect_property_changed(RINDICATINGINSTRUMENT_NAME_DEVICE, sigc::mem_fun(*this, &RIndicatingInstrument::update_name));
  connect_property_changed(RINDICATINGINSTRUMENT_MIN_VAL, sigc::mem_fun(*this, &RIndicatingInstrument::update_scale));
  connect_property_changed(RINDICATINGINSTRUMENT_MAX_VAL, sigc::mem_fun(*this, &RIndicatingInstrument::update_scale));
  connect_property_changed(RINDICATINGINSTRUMENT_STEP_VAL, sigc::mem_fun(*this, &RIndicatingInstrument::update_scale));
  connect_property_changed(RINDICATINGINSTRUMENT_COLOR_SECTORS, sigc::mem_fun(*this, &RIndicatingInstrument::update_colorsectors));
  connect_property_changed(RINDICATINGINSTRUMENT_LAG, sigc::mem_fun(*this, &RIndicatingInstrument::update_lagopti));
  connect_property_changed(RINDICATINGINSTRUMENT_OPTIM, sigc::mem_fun(*this, &RIndicatingInstrument::update_lagopti));
  connect_property_changed(RINDICATINGINSTRUMENT_SCALEMARKS_NUMBER, sigc::mem_fun(*this, &RIndicatingInstrument::update_fonts));
  connect_property_changed(RINDICATINGINSTRUMENT_SCALEMARKS_FONTSIZE, sigc::mem_fun(*this, &RIndicatingInstrument::update_fonts));
  connect_property_changed(RINDICATINGINSTRUMENT_DIGITALVALUE_FONTSIZE, sigc::mem_fun(*this, &RIndicatingInstrument::update_fonts));
  connect_property_changed(RINDICATINGINSTRUMENT_LABEL_FONTSIZE, sigc::mem_fun(*this, &RIndicatingInstrument::update_fonts));
  connect_property_changed(RINDICATINGINSTRUMENT_SCALEMARK_WIDTH, sigc::mem_fun(*this, &RIndicatingInstrument::update_fonts));
  connect_property_changed(RINDICATINGINSTRUMENT_COLORLINE_WIDTH, sigc::mem_fun(*this, &RIndicatingInstrument::update_fonts));
}
// -------------------------------------------------------------------------
RIndicatingInstrument::RIndicatingInstrument() :
  Glib::ObjectBase("rindicatinginstrument")
  ,INIT_RINDICATINGINSTRUMENT_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
RIndicatingInstrument::RIndicatingInstrument(SimpleObject::BaseObjectType* gobject) :
  SimpleObject(gobject)
  ,INIT_RINDICATINGINSTRUMENT_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
RIndicatingInstrument::~RIndicatingInstrument()
{
  if(is_device)
  {
    delete device;
  }
  if(logic != NULL)
    delete logic;
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::create_device()
{
  if(!is_device)
  {
    if(get_type() == UniWidgetsTypes::AD_type)
    {
      device = new AD();
    }
    else if(get_type() == UniWidgetsTypes::EAD_type)
    {
      device = new EAD();
    }
    else if(get_type() == UniWidgetsTypes::CAD_type)
    {
      device = new CAD();
    }
    else
    {
      device = new AD();
    }
    add_child(device, typeIndicator);
    is_device = true;
  }
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::configure_colorsectors()
{
  if(!is_device)
    return;
  if(!get_color_sectors().empty() )
  {
    /*Первое - это очистка текущего списка, иначе все подряд добавляются*/
    device->cleanSect();
    /* "color_sectors" - строка, которой задаются закрашенные сектора.
    Например:
      start_value1 end_value1 R1 G1 B1 alpha1 solid1;start_value2 end_value2 R2 G2 B2 alpha2 solid2;....
    Строки разделяются ";",а внутри параметры разделены пробелами.
    start_value - начало сектора;
    end_value - конец сектора;
    R - градиент красного;
    G - градиент зеленого;
    B - градиент синего;
    alpha - прозрачность;
    solid - сектор от стрелки или по шкале.*/
    std::vector<Glib::ustring> lines = Glib::Regex::split_simple(";", get_color_sectors()); //THIS IS IT!!
    for(unsigned int i=0; i<lines.size(); i++) {
      std::vector<Glib::ustring> params = Glib::Regex::split_simple(" ", lines.at(i));
      if(params.size() != 7)
        continue;

      Glib::RefPtr<Glib::Regex> regex = Glib::Regex::create("^[\\S\\d\\.\\,\\-]+\\s[\\S\\d\\.\\,\\-]+\\s[\\S\\d\\.\\,]+\\s[\\S\\d\\.\\,]+\\s[\\S\\d\\.\\,]+\\s[\\S\\d\\.\\,]+\\s((0|1)|(T|t)rue|(F|f)alse)$");
      bool check = regex->match(lines.at(i));
      if(!check)
        continue;
      bool solid=false;
      std::vector<double> vparam(6,0.0);
      for(unsigned int i=0; i<params.size(); i++) {
        std::stringstream s;
        if( i == params.size()-1 ) // Последний параметр типа bool
        {
          if( params[i] == "False" || params[i] == "false" || params[i] == "0" )
          {
            solid=false;
            continue;
          }
          else if( params[i] == "True" || params[i] == "true" || params[i] == "1" )
          {
            solid=true;
            continue;
          }
        }
        else
        {
          s << params[i].raw();
          s >> vparam[i];
        }
      }
  //       device->addSect(int startBorder,int endBorder,
  //             double red,double green,
  //             double blue,double alfa, bool solid);
      device->addSect(int(vparam[0]),int(vparam[1]),
            vparam[2],vparam[3],vparam[4],vparam[5],
            solid
          );
    }
  }
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::configure_device()
{
  if(is_device)
  {
    device->set_name_device(get_name_device());
    device->set_min_value(get_min_value());
    device->set_max_value(get_max_value());
    device->set_step_value(get_step_value());
    device->set_lag(get_lag());
    configure_colorsectors();
    device->queue_draw();
  }
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::on_realize()
{
  create_device();
  SimpleObject::on_realize();

  /* FIXME: Use another event for this initialization.
   * on_realize() is called twice in glade-3 editor */
  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::on_connect() throw()
{
  create_device();
  SimpleObject::on_connect();

  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::on_size_allocate(Gtk::Allocation& allocation)
{
  SimpleObject::on_size_allocate(allocation);
  set_allocation(allocation);

  Gtk::Allocation child_allocation;

  child_allocation.set_x( allocation.get_x() );
  child_allocation.set_y( allocation.get_y() );
  child_allocation.set_width( allocation.get_width() );
  child_allocation.set_height( allocation.get_height() );
  if(is_device)
  {
    device->size_allocate(child_allocation);
  }
}
// -------------------------------------------------------------------------
void RIndicatingInstrument::on_configure()
{

//   link_.set_link_di( get_property_link_di() );
//   link_.set_node( get_node() );
// #if GTK_VERSION_GE(2,18)
//   link_.set_visible(false);
// #else
//   link_.hide();
// #endif
  configure_device();
  put(*device,0,0);

  logic->set_node(this->get_node());
  logic->set_value_ai(this->get_value_ai());
  
  show();
  device->show();
}
// -------------------------------------------------------------------------
