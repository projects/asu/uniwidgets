#ifndef _RINDICATINGINSTRUMENT_H
#define _RINDICATINGINSTRUMENT_H
// -------------------------------------------------------------------------
#include <string>
#include <map>
#include <objects/SimpleObject.h>
#include <objects/LinkLogic.h>
#include <objects/ADLogic.h>
#include <components/AD.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой виджет аналоговый прибор.
 * \par
 * Виджет отображает аналоговый прибор. Аналоговый прибор включает в себя три типа,
 * выбрать один из которых можно задав соответствующее свойство или в glade-3 редакторе.
 * Типы прибора: AD, CAD и EAD. Все три имеют разный вид, но пу сути выполняют одну и ту же
 * задачу - отображение аналогового значения от датчика на приборе и отображение различных
 * пороговых уровней, которые выделяются на шкале различным текстом. В glade-3 редакторе можно
 * настраивать виджет динамически и результат будет сразу же отображаться.
 * \note
 *  Для добавления цветных секторов на шкалу нужно воспользоваться следующим способом:
 * start_value1 end_value1 R1 G1 B1 alpha1 solid1;start_value2 end_value2 R2 G2 B2 alpha2 solid2;....
 * Строки разделяются ";",а внутри параметры разделены пробелами, где:
 *  start_value - начало сектора;
 *  end_value - конец сектора;
 *  R - градиент красного;
 *  G - градиент зеленого;
 *  B - градиент синего;
 *  alpha - прозрачность;
 *  solid - сектор от стрелки или по шкале.
 *
 * \n Пример приборов представленных в последовательности CAD,AD,EAD слева-направо: \n \n
 * \image html indicatinginstruments.png
 */
class RIndicatingInstrument : public SimpleObject
{
public:
  RIndicatingInstrument();
  explicit RIndicatingInstrument(SimpleObject::BaseObjectType* gobject);
  virtual ~RIndicatingInstrument();

protected:
  /* Handlers */
  virtual void on_realize();
  virtual void on_connect() throw();
  void on_size_allocate(Gtk::Allocation& allocation);

private:
  /* Variables */
  AD *device;
  ADLogic *logic;
//   LinkLogic link_;
  bool is_configured;
  bool is_device;

  /* Methods */
  void constructor();
  void on_configure();
  void create_device();
  void configure_device();
  void configure_colorsectors();
  void update_view();
  void update_name();
  void update_scale();
  void update_colorsectors();
  void update_lagopti();
  void update_fonts();

  DISALLOW_COPY_AND_ASSIGN(RIndicatingInstrument);

  /* Properties */
  ADD_PROPERTY( value_ai, UniWidgetsTypes::ObjectId )    /*!< свойство: id аналогового датчика */
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )    /*!< свойство: id узла */
//   ADD_PROPERTY( property_link_di, UniWidgetsTypes::ObjectId )/*!< свойство: */
  ADD_PROPERTY( type, UniWidgetsTypes::ArrowDeviceType )  /*!< свойство: тип аналогового прибора */
  ADD_PROPERTY( name_device, Glib::ustring )    /*!< свойство: имя устройства */
  ADD_PROPERTY( min_value, long )        /*!< свойство: минимальное значение шкалы */
  ADD_PROPERTY( max_value, long )        /*!< свойство: максимальное значение шкалы */
  ADD_PROPERTY( step_value, long )      /*!< свойство: шаг значений шкалы */
  ADD_PROPERTY( color_sectors, Glib::ustring )    /*!< свойство: строка для настройки цветных секторов */
  ADD_PROPERTY( lag, long )         /*!< свойство: инерционность стрелки */
  ADD_PROPERTY( optimization, bool )      /*!< свойство: оптимизация */
  ADD_PROPERTY( scalemarks_number, int )      /*!< свойство: количество делений шкалы в каждом шаге */
  ADD_PROPERTY( scalemarks_fontsize, int )    /*!< свойство: шрифт для числовых обозначений шкалы */
  ADD_PROPERTY( digitalvalue_fontsize, int )    /*!< свойство: шрифт цифрового значения выводимого на приборе */
  ADD_PROPERTY( label_fontsize, int )      /*!< свойство: шрифт для надписи имени устройства */
  ADD_PROPERTY( scalemark_width, int )      /*!< свойство: толщина линий делений шкалы */
  ADD_PROPERTY( colorline_width, int )      /*!< свойство: толщина линии цветного сектора шкалы */
};

}
#endif
