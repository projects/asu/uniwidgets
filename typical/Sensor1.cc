#include <iostream>
#include <types.h>
#include "Sensor1.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define SENSOR1_STATE_AI      "state-ai"
#define SENSOR1_USE_IMAGE      "use-image"
#define SENSOR1_NODE        "node"
#define SENSOR1_SELF_STATE_AI      "self-state-ai"
#define SENSOR1_LINK_DI        "link-di"
#define SENSOR1_MODE_LOGIC      "mode-logic"
#define SENSOR1_NODE        "node"
#define SENSOR1_TEXT        "text"
#define SENSOR1_NUM_TEXT      "num-text"
#define SENSOR1_FONT_NAME      "font-name"
#define SENSOR1_FONT_SIZE      "font-size"
#define SENSOR1_MODE_ON        "mode-on"
#define SENSOR1_DETONATOR_ON      "detonator-on"
#define SENSOR1_PRIORITY_ON      "priority-on"
#define SENSOR1_FONT_COLOR_ON      "font-color-on"
#define SENSOR1_FONT_COLOR_OFF      "font-color-off"
#define SENSOR1_FONT_SHADOW_ON      "font-shadow-on"
#define SENSOR1_FONT_SHADOW_OFF      "font-shadow-off"
#define SENSOR1_BLINK_STATE_ON      "blink-state-on"
#define SENSOR1_ALIGNMENT      "alignment"

#define SENSOR1_IMG_ON_PATH      "on-image-path"
#define SENSOR1_BACK_IMG_ON_PATH    "back-on-image-path"
#define SENSOR1_IMG_OFF_PATH      "off-image-path"

#define SENSOR1_NUMBER_OF_MODE      "number-of-mode"

#define SENSOR1_X_TEXT        "x-text"
#define SENSOR1_Y_TEXT        "y-text"
#define SENSOR1_X_TEXTNUM      "x-text-number"
#define SENSOR1_Y_TEXTNUM      "y-text-number"
#define SENSOR1_X_IMAGE        "x-image"
#define SENSOR1_Y_IMAGE        "y-image"
#define SENSOR1_W_IMAGE        "w-image"
#define SENSOR1_H_IMAGE        "h-image"
// -------------------------------------------------------------------------
#define INIT_SENSOR1_PROPERTIES() \
  state_ai(*this, SENSOR1_STATE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,self_state_ai(*this, SENSOR1_SELF_STATE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,property_link_di(*this, SENSOR1_LINK_DI , UniWidgetsTypes::DefaultObjectId) \
  ,mode_logic(*this, SENSOR1_MODE_LOGIC , UniWidgetsTypes::DefaultObjectId) \
  ,property_node(*this, SENSOR1_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,text(*this, SENSOR1_TEXT , "Text") \
  ,num_text(*this, SENSOR1_NUM_TEXT , "00.00") \
  ,font_name(*this, SENSOR1_FONT_NAME, "Liberation Sans 16")\
  ,font_size(*this, SENSOR1_FONT_SIZE , 16) \
  ,mode_on(*this, SENSOR1_MODE_ON , UniWidgetsTypes::DefaultObjectId) \
  ,detntr_on(*this, SENSOR1_DETONATOR_ON , UniWidgetsTypes::DefaultObjectId) \
  ,priority_on(*this, SENSOR1_PRIORITY_ON , 1) \
  ,font_color_on(*this, SENSOR1_FONT_COLOR_ON , Gdk::Color("yellow")) \
  ,font_color_off(*this, SENSOR1_FONT_COLOR_OFF , Gdk::Color("grey")) \
  ,font_shadow_on(*this, SENSOR1_FONT_SHADOW_ON , false) \
  ,font_shadow_off(*this, SENSOR1_FONT_SHADOW_OFF , false) \
  ,blink_state_on(*this, SENSOR1_BLINK_STATE_ON , true) \
  ,alignment(*this, SENSOR1_ALIGNMENT , Pango::ALIGN_LEFT) \
  ,property_use_image(*this, SENSOR1_USE_IMAGE , true) \
  ,number_of_mode(*this, SENSOR1_NUMBER_OF_MODE , 1) \
  ,invert_moff(*this, "invert-mOFF" , false) \
  ,invert_moff_state(*this, "invert-mOFF-state" , mALARM_type) \
  ,img_on_path(*this, SENSOR1_IMG_ON_PATH ) \
  ,use_back(*this, "use-back-image" , false) \
  ,back_img_on_path(*this, SENSOR1_BACK_IMG_ON_PATH ) \
  ,img_off_path(*this, SENSOR1_IMG_OFF_PATH ) \
  \
  ,x_text_rect(*this, SENSOR1_X_TEXT , 0) \
  ,y_text_rect(*this, SENSOR1_Y_TEXT , 0) \
  ,x_num_text_rect(*this, SENSOR1_X_TEXTNUM , 0) \
  ,y_num_text_rect(*this, SENSOR1_Y_TEXTNUM , 0) \
  ,x_image_rect(*this, SENSOR1_X_IMAGE , 0) \
  ,y_image_rect(*this, SENSOR1_Y_IMAGE , 0) \
  ,w_image_rect(*this, SENSOR1_W_IMAGE , -1) \
  ,h_image_rect(*this, SENSOR1_H_IMAGE , -1)
// -------------------------------------------------------------------------
void Sensor1::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback<Sensor1>;

  is_configured = false;
  add_child(&link_, typeLogic);
  text_rect = new Gdk::Rectangle(get_x_text_rect(), get_y_text_rect(), -1, -1);
  num_text_rect = new Gdk::Rectangle(get_x_num_text_rect(), get_y_num_text_rect(), -1, -1);
  image_rect = new Gdk::Rectangle(get_x_image_rect(), get_y_image_rect(), get_w_image_rect(), get_h_image_rect());
  /*Добавляем к родительскому контейнеру с типом*/
  add_child( &image_,typeObject );
  add_child( &text_,typeObject );
  add_child( &num_text_,typeObject );
 
  connect_property_changed(SENSOR1_X_TEXT, sigc::mem_fun(*this, &Sensor1::on_rectangle_changed));
  connect_property_changed(SENSOR1_Y_TEXT, sigc::mem_fun(*this, &Sensor1::on_rectangle_changed));
  connect_property_changed(SENSOR1_X_TEXTNUM, sigc::mem_fun(*this, &Sensor1::on_rectangle_changed));
  connect_property_changed(SENSOR1_Y_TEXTNUM, sigc::mem_fun(*this, &Sensor1::on_rectangle_changed));
  connect_property_changed(SENSOR1_X_IMAGE, sigc::mem_fun(*this, &Sensor1::on_rectangle_changed));
  connect_property_changed(SENSOR1_Y_IMAGE, sigc::mem_fun(*this, &Sensor1::on_rectangle_changed));
  connect_property_changed(SENSOR1_W_IMAGE, sigc::mem_fun(*this, &Sensor1::on_rectangle_changed));
  connect_property_changed(SENSOR1_H_IMAGE, sigc::mem_fun(*this, &Sensor1::on_rectangle_changed));

  connect_property_changed(SENSOR1_TEXT, sigc::bind(sigc::mem_fun(*this, &Sensor1::on_text_changed),&text_, &text));
  connect_property_changed(SENSOR1_NUM_TEXT, sigc::bind(sigc::mem_fun(*this, &Sensor1::on_text_changed), &num_text_, &num_text));

}
// -------------------------------------------------------------------------
Sensor1::Sensor1() :
  Glib::ObjectBase("sensor1")
  ,INIT_SENSOR1_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
Sensor1::Sensor1(SimpleObject::BaseObjectType* gobject) :
  SimpleObject(gobject)
  ,INIT_SENSOR1_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
Sensor1::~Sensor1()
{
  delete text_rect;
  delete num_text_rect;
  delete image_rect;
}
// -------------------------------------------------------------------------
void Sensor1::on_realize()
{
  SimpleObject::on_realize();
  on_rectangle_changed();

  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void Sensor1::on_connect() throw()
{
  SimpleObject::on_connect();
  on_rectangle_changed();

  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void Sensor1::on_configure()
{
  /* Set name for debuging*/
  set_name("Sensor 1: Main");
  image_.set_name("Sensor 1: TypicalState image");
  text_.set_name("Sensor 1: TypicalText text");
  num_text_.set_name("Sensor 1: TypicalText number");
  /*Создание заданного количества состояний*/
//   for(i=0; i<get_number_of_mode();i++ )
//   {
//     text_prop * tp_ = new text_prop();
//     texts_prop.push_back(tp);
//   }
  link_.set_link_di( get_property_link_di() );
  link_.set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  if( get_property_use_image() )
  {
    image_.set_rect( *image_rect );
    if(get_invert_moff() == true)
    {
      /*Если параметр inverte_moff установлен, значит сенсор работает
       *на отлключение датчика(а не на срабатывание как по-умолчанию).
       *В этом случае режим mOFF может срабатывать как сигнал аварии
       *или предупреждения, может требовать квитирования и будет продолжать
       *мигать пока его не заквитируют.
      */
      image_.set_invert_mode( get_invert_moff() );
      image_.invert_mode_off(get_priority_on());
      image_.invert_mode_off_state(get_invert_moff_state());
      image_.set_path( get_mode_on(), get_img_off_path() );
      image_.set_path( mOFF, get_img_on_path() );
      image_.insert_mode(get_mode_on(),-1,false);
      /*Необходимо указывать картинку подложку, чтобы мигание при mOFF было включено*/
      if(get_use_back() == true)
        image_.set_path2( mOFF, get_back_img_on_path() );
      else
        image_.set_path2( mOFF, get_img_off_path() );
    }
    else
    {
      image_.set_path( mOFF, get_img_off_path() );
      image_.set_path( get_mode_on(), get_img_on_path() );
      image_.insert_mode(mOFF,-1,false);
      image_.insert_mode(get_mode_on(),get_priority_on(),get_blink_state_on());
      if(get_use_back() == true)
        image_.set_path2( get_mode_logic(), get_back_img_on_path() );
    }
  
    image_.set_mode_state( get_mode_logic() );
    image_.set_detntr_state( get_detntr_on() );
    image_.set_state_ai( get_state_ai() );
    image_.set_state_obj(get_self_state_ai());
    image_.set_node( get_property_node() );
    image_.set_lock_view(get_property_lock_view());
    
    image_.configure();
    /* The SimpleObject::on_realize will be called after this put */
    put( image_,
        image_rect->get_x(),
        image_rect->get_y() );
    image_.show();
  }else
    text_.set_lock_view(get_property_lock_view());

  if(get_text() != "")
  {
    /*Configure TEXT/OFF */
    text_.set_rect( text_rect );
    text_prop *tp_off = new text_prop();
    tp_off->mode=0;
    tp_off->priority=0;
    tp_off->text=get_text();
    tp_off->fname=get_font_name();
    tp_off->fsize=get_font_size();
    if(get_invert_moff() == true)
    {
      tp_off->color_on = get_font_color_on();
      tp_off->color_off = get_font_color_off();
      tp_off->bstate=get_blink_state_on();
      tp_off->shadow = get_font_shadow_on();
    }else
    {
      tp_off->color_on = get_font_color_off();
      tp_off->color_off = get_font_color_off();
      tp_off->bstate=false;
      tp_off->shadow = get_font_shadow_off();
    }
    tp_off->al = get_alignment();
    text_.add_mode_text( tp_off );

    /*Configure TEXT/ON */
    text_prop* tp_on = new text_prop();
    tp_on->mode=get_mode_on();
    tp_on->priority=get_priority_on();
    tp_on->text=get_text();
    tp_on->fname=get_font_name();
    tp_on->fsize=get_font_size();
    if(get_invert_moff() == true)
    {
      tp_on->color_on = get_font_color_off();
      tp_on->color_off = get_font_color_off();
      tp_on->bstate=false;
      tp_on->shadow = get_font_shadow_off();
    }else
    {
      tp_on->color_on = get_font_color_on();
      tp_on->color_off = get_font_color_off();
      tp_on->bstate=get_blink_state_on();
      tp_on->shadow = get_font_shadow_on();
    }
    tp_on->al = get_alignment();
    text_.add_mode_text( tp_on );

    if(get_invert_moff() == true)
      text_.set_invert_mode( get_invert_moff() );
    text_.set_textlogic_state_ai( get_state_ai() );
    text_.set_state_obj(get_self_state_ai());
    text_.set_textlogic_mode( get_mode_logic() );
    text_.set_textlogic_detntr( get_detntr_on() );
    text_.set_node( get_property_node() );
    text_.configure();
    /* The SimpleObject::on_realize will be called after this put */
    put( text_,
        text_rect->get_x(),
        text_rect->get_y() );
    
    text_.show();
  }
  
  if(get_num_text() != "")
  {
    /*Configure NUM TEXT/OFF */
    num_text_.set_rect( num_text_rect );
    text_prop *n_tp_off = new text_prop();
    n_tp_off->mode=0;
    n_tp_off->priority=0;
    n_tp_off->text=get_num_text();
    n_tp_off->fname=get_font_name();
    n_tp_off->fsize=get_font_size();
    n_tp_off->color_on = get_font_color_off();
    n_tp_off->color_off = get_font_color_off();
    n_tp_off->bstate=false;
    n_tp_off->shadow = get_font_shadow_off();
    n_tp_off->al = get_alignment();
    num_text_.add_mode_text(n_tp_off);

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_on = new text_prop();
    n_tp_on->mode=get_mode_on();
    n_tp_on->priority=get_priority_on();
    n_tp_on->text=get_num_text();
    n_tp_on->fname=get_font_name();
    n_tp_on->fsize=get_font_size();
    n_tp_on->color_on = get_font_color_on();
    n_tp_on->color_off = get_font_color_off();
    n_tp_on->bstate=get_blink_state_on();
    n_tp_on->shadow = get_font_shadow_on();
    n_tp_on->al = get_alignment();
    num_text_.add_mode_text( n_tp_on );

    if(get_invert_moff() == true)
      num_text_.set_invert_mode( get_invert_moff() );
    num_text_.set_textlogic_state_ai( get_state_ai() );
    num_text_.set_textlogic_mode( get_mode_logic() );
    num_text_.set_textlogic_detntr( get_detntr_on() );
    num_text_.set_node( get_property_node() );
    num_text_.configure();
    put( num_text_,
        num_text_rect->get_x(),
        num_text_rect->get_y() );
    num_text_.show();
  }

  show();

}
// -------------------------------------------------------------------------
void Sensor1::set_rectangle(Gdk::Rectangle* rect_,const long x, const long y, const long w, const long h)
{
  rect_->set_x(x);
  rect_->set_y(y);
  rect_->set_width(w);
  rect_->set_height(h);
}
// -------------------------------------------------------------------------
void Sensor1::on_rectangle_changed()
{
  set_size_request(-1,-1);
  set_rectangle(text_rect,get_x_text_rect(), get_y_text_rect(), -1, -1);
  set_rectangle(num_text_rect,get_x_num_text_rect(), get_y_num_text_rect(), -1, -1);
  set_rectangle(image_rect,get_x_image_rect(), get_y_image_rect(), get_w_image_rect(), get_h_image_rect());

  if(is_configured)
  {
    Gtk::Widget *w;
    if( get_property_use_image() )
    {
      w = dynamic_cast<Gtk::Widget* >(&image_);
      move(*w,get_x_image_rect(), get_y_image_rect());
    }
    if(get_text() != "")
    {
      w = dynamic_cast<Gtk::Widget* >(&text_);
      move(*w,get_x_text_rect(), get_y_text_rect());
    }
    if(get_num_text() != "")
    {
      w = dynamic_cast<Gtk::Widget* >(&num_text_);
      move(*w,get_x_num_text_rect(), get_y_num_text_rect());
    }
  }

  queue_resize();
}
// -------------------------------------------------------------------------
void Sensor1::on_text_changed(TypicalText *tx, const Glib::Property<Glib::ustring> *proper)
{
  for(std::vector<Text *>::iterator it = tx->texts_.begin();it != tx->texts_.end(); it++)
    (*it)->set_property_text_( proper->get_value() );
  queue_resize();
}
// -------------------------------------------------------------------------
