#ifndef _SENSOR1_H
#define _SENSOR1_H
// -------------------------------------------------------------------------
#include <string>
#include <map>
#include <objects/SimpleObject.h>
#include <objects/LinkLogic.h>
#include <typical/TypicalState.h>
#include <typical/TypicalText.h>
#include <plugins.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой виджет сенсор.
 * \par
 * Виджет отображает работу сенсора. Сенсор может быть с дискретным датчиком, так и
 * и с аналоговым. Во втором случае будет обрабатываться только одно значение аналогового
 * датчика для которого задан режим,а остальные, кроме "0", будут игонорироваться. Также есть
 * возможность представить аналоговый датчик как дискретный. Для этого есть свойство "детонатор",
 * выставив которое в определенное значение, обрабатываться будет только это значение от аналогового
 * датчика,а все остальные будут считаться "0" т.е. сбросом датчика.
 * 
 * \n Пример простого датчика с мнемознаком, текстом сигнала и номером сигнала. \n \n
 * \image html sensor.png
 */
class Sensor1 : public SimpleObject
{
public:
  Sensor1();
  explicit Sensor1(SimpleObject::BaseObjectType* gobject);
  virtual ~Sensor1();

  /* Constants */
  Gdk::Rectangle *text_rect;    /*!< координаты и размер текста сенсора */
  Gdk::Rectangle *num_text_rect;    /*!< координаты и размер текстового номера сигнала сенсора */
  Gdk::Rectangle *image_rect;    /*!< координаты и размер картинки */

protected:
  /* Handlers */
  virtual void on_realize();
  virtual void on_connect() throw();

private:
  /* Variables */
  std::unordered_map<long ,text_prop *> texts_prop;
  TypicalState image_;
  TypicalText text_;
  TypicalText num_text_;
  LinkLogic link_;
  bool is_configured;

  /* Methods */
  void constructor();
  void on_configure();
  void on_rectangle_changed();
  void on_text_changed(TypicalText *, const Glib::Property<Glib::ustring> *);
  void set_rectangle(Gdk::Rectangle* rect_,const long x, const long y, const long w, const long h);

  DISALLOW_COPY_AND_ASSIGN(Sensor1);

  /* Properties */
  ADD_PROPERTY( state_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика состояния сенсора */
  ADD_PROPERTY( self_state_ai, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика внутреннего состояния сенсора */
  ADD_PROPERTY( property_link_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика состояния связи с SharedMemory */
  /*! свойство для определения к какому типу принадлежит датчик.
    AI - если значение оставить по-умолчанию на -1, то виджет будет обрабатывать все изменения датчика
    и сработает по приходу значения равного значению свойства "mode_on", для которого есть отображение.
    Все остальные значения, кроме "0", будут игнорироваться
    DI - Необходимо выставить определенный "mode" в поле "mode_logic",которому соответствует "mode_on".
    Так как состояния определены заранее в types.h и приоритеты к ним в AbstractTypical.cc, то необходимо
    правильно указаывать "mode" ориентируясь на имеющийся список.
  */
  ADD_PROPERTY( mode_logic, long )
  ADD_PROPERTY( property_node, UniWidgetsTypes::ObjectId )    /*!< свойство: id узла */
  ADD_PROPERTY( text, Glib::ustring )        /*!< свойство: текст сенсора */
  ADD_PROPERTY( num_text, Glib::ustring )        /*!< свойство: текстовый номер сигнала сенсора */
  ADD_PROPERTY( font_name, Glib::ustring )      /*!< свойство: шрифт текста */
  ADD_PROPERTY( font_size, long )          /*!< свойство: размер шрифта */
  ADD_PROPERTY( mode_on, long )          /*!< свойство: режим сенсора */
  ADD_PROPERTY( detntr_on, long )          /*!< свойство: значение "детонатора" */
  ADD_PROPERTY( priority_on, long )        /*!< свойство: приоритет режима включен */
  ADD_PROPERTY( font_color_on, Gdk::Color )      /*!< свойство: цвет шрифта в режиме включен */
  ADD_PROPERTY( font_color_off, Gdk::Color )      /*!< свойство: цвет шрифта в режиме выключен */
  ADD_PROPERTY( font_shadow_on, bool )        /*!< свойство: тень в режиме включен */
  ADD_PROPERTY( font_shadow_off, bool )        /*!< свойство: тень в режиме выключен */
  ADD_PROPERTY( blink_state_on, bool )        /*!< свойство: мигать в режиме включен */
  ADD_PROPERTY( alignment, Pango::Alignment )      /*!< свойство: выравнивание текста */

  ADD_PROPERTY( property_use_image, bool )      /*!< свойство: использовать картинку в виджете */
  ADD_PROPERTY( number_of_mode, long )        /*!< свойство: число режимов(неиспользуется!) */

  ADD_PROPERTY( invert_moff,bool )        /*!< свойство: инвертировать режим выключенного состояния */
  ADD_PROPERTY( invert_moff_state,UniWidgetsTypes::ThresholdType )/*!< свойство: тип режима при инверсии выключенного состояния */
  ADD_PROPERTY( img_on_path, Glib::ustring )      /*!< свойство: картинка для режима включен */
  ADD_PROPERTY( use_back, bool )          /*!< свойство: использовать подложку для картинки режима включен при мигании */
  ADD_PROPERTY( back_img_on_path, Glib::ustring )      /*!< свойство: картинка подложки для режима включен */
  ADD_PROPERTY( img_off_path, Glib::ustring )      /*!< свойство: картинка для режима выключен */

  ADD_PROPERTY( x_text_rect, long )        /*!< свойство: x-координата текста сенсора */
  ADD_PROPERTY( y_text_rect, long )        /*!< свойство: y-координата текста сенсора */
  ADD_PROPERTY( x_num_text_rect, long )        /*!< свойство: x-координата текстового номера сигнала сенсора */
  ADD_PROPERTY( y_num_text_rect, long )        /*!< свойство: y-координата текстового номера сигнала сенсора */
  ADD_PROPERTY( x_image_rect, long )        /*!< свойство: x-координата картинки */
  ADD_PROPERTY( y_image_rect, long )        /*!< свойство: y-координата картинки */
  ADD_PROPERTY( w_image_rect, long )        /*!< свойство: ширина картинки */
  ADD_PROPERTY( h_image_rect, long )        /*!< свойство: высота картинки */
};

}
#endif
