#include <iostream>
#include <types.h>
#include "SensorM.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define SENSORM_STATE_AI      "state-ai"
#define SENSORM_NODE        "node"
#define SENSORM_USE_IMAGE      "use-image"
#define SENSORM_SELF_STATE_AI      "self-state-ai"
#define SENSORM_LINK_DI        "link-di"
#define SENSORM_STATE_DI1      "state-di-1"
#define SENSORM_NUM_TEXT1      "num-text-1"
#define SENSORM_TEXT1        "text-1"
#define SENSORM_BLINK1        "blink-1"
#define SENSORM_MODE1        "mode-1"
#define SENSORM_DETONATOR_ON1      "detonator-1"
#define SENSORM_PRIORITY1      "priority-1"
#define SENSORM_FONT_COLOR_ON1      "font-color-on-1"
#define SENSORM_IMG_ON_PATH1      "on-image-path-1"
#define SENSORM_STATE_DI2      "state-di-2"
#define SENSORM_NUM_TEXT2      "num-text-2"
#define SENSORM_TEXT2        "text-2"
#define SENSORM_BLINK2        "blink-2"
#define SENSORM_MODE2        "mode-2"
#define SENSORM_DETONATOR_ON2      "detonator-2"
#define SENSORM_PRIORITY2      "priority-2"
#define SENSORM_FONT_COLOR_ON2      "font-color-on-2"
#define SENSORM_IMG_ON_PATH2      "on-image-path-2"
#define SENSORM_STATE_DI3      "state-di-3"
#define SENSORM_NUM_TEXT3      "num-text-3"
#define SENSORM_TEXT3        "text-3"
#define SENSORM_BLINK3        "blink-3"
#define SENSORM_MODE3        "mode-3"
#define SENSORM_DETONATOR_ON3      "detonator-3"
#define SENSORM_PRIORITY3      "priority-3"
#define SENSORM_FONT_COLOR_ON3      "font-color-on-3"
#define SENSORM_IMG_ON_PATH3      "on-image-path-3"
#define SENSORM_STATE_DI4      "state-di-4"
#define SENSORM_NUM_TEXT4      "num-text-4"
#define SENSORM_TEXT4        "text-4"
#define SENSORM_BLINK4        "blink-4"
#define SENSORM_MODE4        "mode-4"
#define SENSORM_DETONATOR_ON4      "detonator-4"
#define SENSORM_PRIORITY4      "priority-4"
#define SENSORM_FONT_COLOR_ON4      "font-color-on-4"
#define SENSORM_IMG_ON_PATH4      "on-image-path-4"

#define SENSORM_STATE_DI5      "state-di-5"
#define SENSORM_NUM_TEXT5      "num-text-5"
#define SENSORM_TEXT5        "text-5"
#define SENSORM_BLINK5        "blink-5"
#define SENSORM_MODE5        "mode-5"
#define SENSORM_DETONATOR_ON5      "detonator-5"
#define SENSORM_PRIORITY5      "priority-5"
#define SENSORM_FONT_COLOR_ON5      "font-color-on-5"
#define SENSORM_IMG_ON_PATH5      "on-image-path-5"

#define SENSORM_STATE_DI6      "state-di-6"
#define SENSORM_NUM_TEXT6      "num-text-6"
#define SENSORM_TEXT6        "text-6"
#define SENSORM_BLINK6        "blink-6"
#define SENSORM_MODE6        "mode-6"
#define SENSORM_DETONATOR_ON6      "detonator-6"
#define SENSORM_PRIORITY6      "priority-6"
#define SENSORM_FONT_COLOR_ON6      "font-color-on-6"
#define SENSORM_IMG_ON_PATH6      "on-image-path-6"

#define SENSORM_STATE_DI7      "state-di-7"
#define SENSORM_NUM_TEXT7      "num-text-7"
#define SENSORM_TEXT7        "text-7"
#define SENSORM_BLINK7        "blink-7"
#define SENSORM_MODE7        "mode-7"
#define SENSORM_DETONATOR_ON7      "detonator-7"
#define SENSORM_PRIORITY7      "priority-7"
#define SENSORM_FONT_COLOR_ON7      "font-color-on-7"
#define SENSORM_IMG_ON_PATH7      "on-image-path-7"

#define SENSORM_STATE_DI8      "state-di-8"
#define SENSORM_NUM_TEXT8      "num-text-8"
#define SENSORM_TEXT8        "text-8"
#define SENSORM_BLINK8        "blink-8"
#define SENSORM_MODE8        "mode-8"
#define SENSORM_DETONATOR_ON8      "detonator-8"
#define SENSORM_PRIORITY8      "priority-8"
#define SENSORM_FONT_COLOR_ON8      "font-color-on-8"
#define SENSORM_IMG_ON_PATH8      "on-image-path-8"

#define SENSORM_IMG_OFF_PATH      "off-image-path"
#define SENSORM_TEXT        "text"
#define SENSORM_FONT_NAME      "font-name"
#define SENSORM_FONT_SIZE      "font-size"

#define SENSORM_FONT_COLOR_OFF      "font-color-off"
#define SENSORM_FONT_SHADOW_ON      "font-shadow-on"
#define SENSORM_FONT_SHADOW_OFF      "font-shadow-off"
#define SENSORM_ALIGNMENT      "alignment"

#define SENSORM_NUMBER_OF_MODE      "number-of-mode"

#define SENSORM_X_TEXT        "x-text"
#define SENSORM_Y_TEXT        "y-text"
#define SENSORM_X_TEXTNUM      "x-text-number"
#define SENSORM_Y_TEXTNUM      "y-text-number"
#define SENSORM_X_IMAGE        "x-image"
#define SENSORM_Y_IMAGE        "y-image"
#define SENSORM_W_IMAGE        "w-image"
#define SENSORM_H_IMAGE        "h-image"
// -------------------------------------------------------------------------
#define INIT_SENSORM_PROPERTIES() \
  state_ai(*this, SENSORM_STATE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,property_node(*this, SENSORM_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,self_state_ai(*this, SENSORM_SELF_STATE_AI , UniWidgetsTypes::DefaultObjectId) \
  ,property_link_di(*this, SENSORM_LINK_DI , UniWidgetsTypes::DefaultObjectId) \
  ,property_use_image(*this, SENSORM_USE_IMAGE , true) \
  \
  ,state_di_1(*this, SENSORM_STATE_DI1 , UniWidgetsTypes::DefaultObjectId) \
  ,num_text_1(*this, SENSORM_NUM_TEXT1 , "00.00") \
  ,text_1(*this, SENSORM_TEXT1 , "") \
  ,blink_1(*this, SENSORM_BLINK1 , true) \
  ,mode_1(*this, SENSORM_MODE1 , UniWidgetsTypes::DefaultObjectId) \
  ,detntr_on_1(*this, SENSORM_DETONATOR_ON1 , UniWidgetsTypes::DefaultObjectId) \
  ,priority_1(*this, SENSORM_PRIORITY1 , UniWidgetsTypes::DefaultObjectId) \
  ,font_color_on_1(*this, SENSORM_FONT_COLOR_ON1 , Gdk::Color("grey")) \
  ,on_image_path_1(*this, SENSORM_IMG_ON_PATH1 ) \
  ,use_back_1(*this, "use-back-image-1" , false) \
  ,back_image_path_1(*this, "back-image-path-1" ) \
  \
  ,state_di_2(*this, SENSORM_STATE_DI2 , UniWidgetsTypes::DefaultObjectId) \
  ,num_text_2(*this, SENSORM_NUM_TEXT2 , "00.00") \
  ,text_2(*this, SENSORM_TEXT2 , "") \
  ,blink_2(*this, SENSORM_BLINK2 , true) \
  ,mode_2(*this, SENSORM_MODE2 , UniWidgetsTypes::DefaultObjectId) \
  ,detntr_on_2(*this, SENSORM_DETONATOR_ON2 , UniWidgetsTypes::DefaultObjectId) \
  ,priority_2(*this, SENSORM_PRIORITY2 , UniWidgetsTypes::DefaultObjectId) \
  ,font_color_on_2(*this, SENSORM_FONT_COLOR_ON2 , Gdk::Color("grey")) \
  ,on_image_path_2(*this, SENSORM_IMG_ON_PATH2 ) \
  ,use_back_2(*this, "use-back-image-2" , false) \
  ,back_image_path_2(*this, "back-image-path-2" ) \
  \
  ,state_di_3(*this, SENSORM_STATE_DI3 , UniWidgetsTypes::DefaultObjectId) \
  ,num_text_3(*this, SENSORM_NUM_TEXT3 , "00.00") \
  ,text_3(*this, SENSORM_TEXT3 , "") \
  ,blink_3(*this, SENSORM_BLINK3 , true) \
  ,mode_3(*this, SENSORM_MODE3 , UniWidgetsTypes::DefaultObjectId) \
  ,detntr_on_3(*this, SENSORM_DETONATOR_ON3 , UniWidgetsTypes::DefaultObjectId) \
  ,priority_3(*this, SENSORM_PRIORITY3 , UniWidgetsTypes::DefaultObjectId) \
  ,font_color_on_3(*this, SENSORM_FONT_COLOR_ON3 , Gdk::Color("grey")) \
  ,on_image_path_3(*this, SENSORM_IMG_ON_PATH3 ) \
  ,use_back_3(*this, "use-back-image-3" , false) \
  ,back_image_path_3(*this, "back-image-path-3" ) \
  \
  ,state_di_4(*this, SENSORM_STATE_DI4 , UniWidgetsTypes::DefaultObjectId) \
  ,num_text_4(*this, SENSORM_NUM_TEXT4 , "00.00") \
  ,text_4(*this, SENSORM_TEXT4 , "") \
  ,blink_4(*this, SENSORM_BLINK4 , true) \
  ,mode_4(*this, SENSORM_MODE4 , UniWidgetsTypes::DefaultObjectId) \
  ,detntr_on_4(*this, SENSORM_DETONATOR_ON4 , UniWidgetsTypes::DefaultObjectId) \
  ,priority_4(*this, SENSORM_PRIORITY4 , UniWidgetsTypes::DefaultObjectId) \
  ,font_color_on_4(*this, SENSORM_FONT_COLOR_ON4 , Gdk::Color("grey")) \
  ,on_image_path_4(*this, SENSORM_IMG_ON_PATH4 ) \
  ,use_back_4(*this, "use-back-image-4" , false) \
  ,back_image_path_4(*this, "back-image-path-4" ) \
  \
  ,state_di_5(*this, SENSORM_STATE_DI5 , UniWidgetsTypes::DefaultObjectId) \
  ,num_text_5(*this, SENSORM_NUM_TEXT5 , "00.00") \
  ,text_5(*this, SENSORM_TEXT5 , "") \
  ,blink_5(*this, SENSORM_BLINK5 , true) \
  ,mode_5(*this, SENSORM_MODE5 , UniWidgetsTypes::DefaultObjectId) \
  ,detntr_on_5(*this, SENSORM_DETONATOR_ON5 , UniWidgetsTypes::DefaultObjectId) \
  ,priority_5(*this, SENSORM_PRIORITY5 , UniWidgetsTypes::DefaultObjectId) \
  ,font_color_on_5(*this, SENSORM_FONT_COLOR_ON5 , Gdk::Color("grey")) \
  ,on_image_path_5(*this, SENSORM_IMG_ON_PATH5 ) \
  ,use_back_5(*this, "use-back-image-5" , false) \
  ,back_image_path_5(*this, "back-image-path-5" ) \
  \
  ,state_di_6(*this, SENSORM_STATE_DI6 , UniWidgetsTypes::DefaultObjectId) \
  ,num_text_6(*this, SENSORM_NUM_TEXT6 , "00.00") \
  ,text_6(*this, SENSORM_TEXT6 , "") \
  ,blink_6(*this, SENSORM_BLINK6 , true) \
  ,mode_6(*this, SENSORM_MODE6 , UniWidgetsTypes::DefaultObjectId) \
  ,detntr_on_6(*this, SENSORM_DETONATOR_ON6 , UniWidgetsTypes::DefaultObjectId) \
  ,priority_6(*this, SENSORM_PRIORITY6 , UniWidgetsTypes::DefaultObjectId) \
  ,font_color_on_6(*this, SENSORM_FONT_COLOR_ON6 , Gdk::Color("grey")) \
  ,on_image_path_6(*this, SENSORM_IMG_ON_PATH6 ) \
  ,use_back_6(*this, "use-back-image-6" , false) \
  ,back_image_path_6(*this, "back-image-path-6" ) \
  \
  ,state_di_7(*this, SENSORM_STATE_DI7 , UniWidgetsTypes::DefaultObjectId) \
  ,num_text_7(*this, SENSORM_NUM_TEXT7 , "00.00") \
  ,text_7(*this, SENSORM_TEXT7 , "") \
  ,blink_7(*this, SENSORM_BLINK7 , true) \
  ,mode_7(*this, SENSORM_MODE7 , UniWidgetsTypes::DefaultObjectId) \
  ,detntr_on_7(*this, SENSORM_DETONATOR_ON7 , UniWidgetsTypes::DefaultObjectId) \
  ,priority_7(*this, SENSORM_PRIORITY7 , UniWidgetsTypes::DefaultObjectId) \
  ,font_color_on_7(*this, SENSORM_FONT_COLOR_ON7 , Gdk::Color("grey")) \
  ,on_image_path_7(*this, SENSORM_IMG_ON_PATH7 ) \
  ,use_back_7(*this, "use-back-image-7" , false) \
  ,back_image_path_7(*this, "back-image-path-7" ) \
  \
  ,state_di_8(*this, SENSORM_STATE_DI8 , UniWidgetsTypes::DefaultObjectId) \
  ,num_text_8(*this, SENSORM_NUM_TEXT8 , "00.00") \
  ,text_8(*this, SENSORM_TEXT8 , "") \
  ,blink_8(*this, SENSORM_BLINK8 , true) \
  ,mode_8(*this, SENSORM_MODE8 , UniWidgetsTypes::DefaultObjectId) \
  ,detntr_on_8(*this, SENSORM_DETONATOR_ON8 , UniWidgetsTypes::DefaultObjectId) \
  ,priority_8(*this, SENSORM_PRIORITY8 , UniWidgetsTypes::DefaultObjectId) \
  ,font_color_on_8(*this, SENSORM_FONT_COLOR_ON8 , Gdk::Color("grey")) \
  ,on_image_path_8(*this, SENSORM_IMG_ON_PATH8 ) \
  ,use_back_8(*this, "use-back-image-8" , false) \
  ,back_image_path_8(*this, "back-image-path-8" ) \
  \
  ,img_off_path(*this, SENSORM_IMG_OFF_PATH ) \
  ,text(*this, SENSORM_TEXT , "Text") \
  ,font_name(*this, SENSORM_FONT_NAME, "Liberation Sans 16")\
  ,font_size(*this, SENSORM_FONT_SIZE , 16) \
  ,font_color_off(*this, SENSORM_FONT_COLOR_OFF , Gdk::Color("grey")) \
  ,font_shadow_on(*this, SENSORM_FONT_SHADOW_ON , false) \
  ,font_shadow_off(*this, SENSORM_FONT_SHADOW_OFF , false) \
  ,alignment(*this, SENSORM_ALIGNMENT , Pango::ALIGN_LEFT) \
  ,number_of_mode(*this, SENSORM_NUMBER_OF_MODE , 1) \
  \
  ,x_text_rect(*this, SENSORM_X_TEXT , 0) \
  ,y_text_rect(*this, SENSORM_Y_TEXT , 0) \
  ,x_num_text_rect(*this, SENSORM_X_TEXTNUM , 0) \
  ,y_num_text_rect(*this, SENSORM_Y_TEXTNUM , 0) \
  ,x_image_rect(*this, SENSORM_X_IMAGE , 0) \
  ,y_image_rect(*this, SENSORM_Y_IMAGE , 0) \
  ,w_image_rect(*this, SENSORM_W_IMAGE , -1) \
  ,h_image_rect(*this, SENSORM_H_IMAGE , -1)
// -------------------------------------------------------------------------
void SensorM::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback<SensorM>;

  is_configured = false;
  /*Добавляем к родительскому контейнеру с типом*/
  add_child( &image_,typeObject );
  add_child( &text_,typeObject );
  add_child( &num_text_,typeObject );
  add_child(&link_, typeLogic);
  text_rect = new Gdk::Rectangle(get_x_text_rect(), get_y_text_rect(), -1, -1);
  num_text_rect = new Gdk::Rectangle(get_x_num_text_rect(), get_y_num_text_rect(), -1, -1);
  image_rect = new Gdk::Rectangle(get_x_image_rect(), get_y_image_rect(), get_w_image_rect(), get_h_image_rect());

  connect_property_changed(SENSORM_X_TEXT, sigc::mem_fun(*this, &SensorM::on_rectangle_changed));
  connect_property_changed(SENSORM_Y_TEXT, sigc::mem_fun(*this, &SensorM::on_rectangle_changed));
  connect_property_changed(SENSORM_X_TEXTNUM, sigc::mem_fun(*this, &SensorM::on_rectangle_changed));
  connect_property_changed(SENSORM_Y_TEXTNUM, sigc::mem_fun(*this, &SensorM::on_rectangle_changed));
  connect_property_changed(SENSORM_X_IMAGE, sigc::mem_fun(*this, &SensorM::on_rectangle_changed));
  connect_property_changed(SENSORM_Y_IMAGE, sigc::mem_fun(*this, &SensorM::on_rectangle_changed));
  connect_property_changed(SENSORM_W_IMAGE, sigc::mem_fun(*this, &SensorM::on_rectangle_changed));
  connect_property_changed(SENSORM_H_IMAGE, sigc::mem_fun(*this, &SensorM::on_rectangle_changed));

  connect_property_changed(SENSORM_TEXT, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed),&text_, &text));
  connect_property_changed(SENSORM_TEXT1, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed),&text_, &text_1));
  connect_property_changed(SENSORM_TEXT2, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed),&text_, &text_2));
  connect_property_changed(SENSORM_TEXT3, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed),&text_, &text_3));
  connect_property_changed(SENSORM_TEXT4, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed),&text_, &text_4));
  connect_property_changed(SENSORM_TEXT5, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed),&text_, &text_5));
  connect_property_changed(SENSORM_TEXT6, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed),&text_, &text_6));
  connect_property_changed(SENSORM_TEXT7, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed),&text_, &text_7));
  connect_property_changed(SENSORM_TEXT8, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed),&text_, &text_8));
  connect_property_changed(SENSORM_NUM_TEXT1, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed), &num_text_, &num_text_1));
  connect_property_changed(SENSORM_NUM_TEXT2, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed), &num_text_, &num_text_2));
  connect_property_changed(SENSORM_NUM_TEXT3, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed), &num_text_, &num_text_3));
  connect_property_changed(SENSORM_NUM_TEXT4, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed), &num_text_, &num_text_4));
  connect_property_changed(SENSORM_NUM_TEXT5, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed), &num_text_, &num_text_5));
  connect_property_changed(SENSORM_NUM_TEXT6, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed), &num_text_, &num_text_6));
  connect_property_changed(SENSORM_NUM_TEXT7, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed), &num_text_, &num_text_7));
  connect_property_changed(SENSORM_NUM_TEXT8, sigc::bind(sigc::mem_fun(*this, &SensorM::on_text_changed), &num_text_, &num_text_8));
}
// -------------------------------------------------------------------------
SensorM::SensorM() :
  Glib::ObjectBase("sensorm")
  ,INIT_SENSORM_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
SensorM::SensorM(SimpleObject::BaseObjectType* gobject) :
  SimpleObject(gobject)
  ,INIT_SENSORM_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
SensorM::~SensorM()
{
  delete text_rect;
  delete num_text_rect;
  delete image_rect;
}
// -------------------------------------------------------------------------
void SensorM::on_realize()
{
  SimpleObject::on_realize();
  on_rectangle_changed();

  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void SensorM::on_connect() throw()
{
  SimpleObject::on_connect();
  on_rectangle_changed();

  if ( is_configured )
    return;
  is_configured = true;
  on_configure();
}
// -------------------------------------------------------------------------
void SensorM::on_configure_property()
{
//   for(int i=0; i < get_number_of_mode();i++)
//   {
//     Glib::Property<long> new_mode(*this, Glib::ustring::compose("text-mode%1", i) , 1 );
//     std::pair<long, Glib::RefPtr< Glib::Property<long> >, bool >::iterator res;
//     properties.insert(std::pair<long, Glib::Property<long> *>(i, &new_mode ) );
//     assert(res.second == true);
//   }
}
// -------------------------------------------------------------------------
void SensorM::on_configure()
{
  /*Создание заданного количества состояний*/
//   for(i=0; i<get_number_of_mode();i++ )
//   {
//     text_prop * tp_ = new text_prop();
//     texts_prop.push_back(tp);
//   }
  link_.set_link_di( get_property_link_di() );
  link_.set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  if( get_property_use_image() )
  {
    image_.set_rect( *image_rect );
    image_.set_path( mOFF, get_img_off_path() );
    image_.insert_mode(mOFF,-1,false);
    image_.set_state_obj(get_self_state_ai());
  }
  /*Configure TEXT/OFF */
  text_.set_rect( text_rect );
  text_prop *tp_off = new text_prop();
  tp_off->mode=0;
  tp_off->priority=0;
  tp_off->text=get_text();
  tp_off->fname=get_font_name();
  tp_off->fsize=get_font_size();
  tp_off->color_on = get_font_color_off();
  tp_off->color_off = get_font_color_off();
  tp_off->bstate=false;
  tp_off->shadow = get_font_shadow_off();
  tp_off->al = get_alignment();
  text_.add_mode_text( tp_off );

  /*Configure NUM TEXT/OFF */
  num_text_.set_rect( num_text_rect );
  text_prop *n_tp_off = new text_prop();
  n_tp_off->mode=0;
  n_tp_off->priority=0;
  n_tp_off->text=get_num_text_1();
  n_tp_off->fname=get_font_name();
  n_tp_off->fsize=get_font_size();
  n_tp_off->color_on = get_font_color_off();
  n_tp_off->color_off = get_font_color_off();
  n_tp_off->bstate=false;
  n_tp_off->shadow = get_font_shadow_off();
  n_tp_off->al = get_alignment();
  num_text_.add_mode_text(n_tp_off);

  if( get_mode_1() != UniWidgetsTypes::DefaultObjectId )
  {
    /*Configure TEXT/ON */
    text_prop* tp_on1 = new text_prop();
    tp_on1->mode=get_mode_1();
    tp_on1->priority=get_priority_1();
    if( get_text_1() == "" )
      tp_on1->text=get_text();
    else
      tp_on1->text=get_text_1();
    tp_on1->fname=get_font_name();
    tp_on1->fsize=get_font_size();
    tp_on1->color_on = get_font_color_on_1();
    tp_on1->color_off = get_font_color_off();
    tp_on1->bstate=get_blink_1();
    tp_on1->shadow = get_font_shadow_on();
    tp_on1->al = get_alignment();
    text_.add_mode_text( tp_on1 );

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_on1 = new text_prop();
    n_tp_on1->mode=get_mode_1();
    n_tp_on1->priority=get_priority_1();
    n_tp_on1->text=get_num_text_1();
    n_tp_on1->fname=get_font_name();
    n_tp_on1->fsize=get_font_size();
    n_tp_on1->color_on = get_font_color_on_1();
    n_tp_on1->color_off = get_font_color_off();
    n_tp_on1->bstate=get_blink_1();
    n_tp_on1->shadow = get_font_shadow_on();
    n_tp_on1->al = get_alignment();
    num_text_.add_mode_text( n_tp_on1 );

    if( get_property_use_image() )
    {
      image_.insert_mode(get_mode_1(), get_priority_1(),get_blink_1());
      image_.set_path( get_mode_1(), get_on_image_path_1() );
      if(get_use_back_1() == true)
        image_.set_path2( get_mode_1(), get_back_image_path_1() );
    }
    if( get_state_di_1() != UniWidgetsTypes::DefaultObjectId )
    {
      /*Image*/
      if( get_property_use_image() )
      {
        image_.set_mode_state( get_mode_1() );
        image_.set_detntr_state( get_detntr_on_1() );
        image_.set_state_ai( get_state_di_1() );
        image_.set_state_obj(get_self_state_ai());
        image_.set_node( get_property_node() );
        image_.set_lock_view(get_property_lock_view());
      }else
        text_.set_lock_view(get_property_lock_view());
      /*Text*/
      text_.set_textlogic_state_ai( get_state_di_1() );
      text_.set_state_obj(get_self_state_ai());
      text_.set_textlogic_mode( get_mode_1() );
      text_.set_textlogic_detntr(get_detntr_on_1());
      text_.set_node( get_property_node() );

      num_text_.set_textlogic_state_ai( get_state_di_1() );
      num_text_.set_textlogic_mode( get_mode_1() );
      num_text_.set_textlogic_detntr(get_detntr_on_1());
      num_text_.set_node( get_property_node() );
    }
    else
    {
      /*Image*/
      if( get_property_use_image() )
      {
        image_.set_state_ai( get_state_ai() );
        image_.set_state_obj(get_self_state_ai());
        image_.set_node( get_property_node() );
        image_.set_lock_view(get_property_lock_view());
      }else
        text_.set_lock_view(get_property_lock_view());
      /*Text*/
      text_.set_textlogic_state_ai( get_state_ai() );
      text_.set_state_obj(get_self_state_ai());
      text_.set_node( get_property_node() );

      num_text_.set_textlogic_state_ai( get_state_ai() );
      num_text_.set_node( get_property_node() );
    }
  }

  if( get_mode_2() != UniWidgetsTypes::DefaultObjectId )
  {
    /*Configure TEXT/ON */
    text_prop* tp_on2 = new text_prop();
    tp_on2->mode=get_mode_2();
    tp_on2->priority=get_priority_2();
    if( get_text_2() == "" )
      tp_on2->text=get_text();
    else
      tp_on2->text=get_text_2();
    tp_on2->fname=get_font_name();
    tp_on2->fsize=get_font_size();
    tp_on2->color_on = get_font_color_on_2();
    tp_on2->color_off = get_font_color_off();
    tp_on2->bstate=get_blink_2();
    tp_on2->shadow = get_font_shadow_on();
    tp_on2->al = get_alignment();
    text_.add_mode_text( tp_on2 );

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_on2 = new text_prop();
    n_tp_on2->mode=get_mode_2();
    n_tp_on2->priority=get_priority_2();
    n_tp_on2->text=get_num_text_2();
    n_tp_on2->fname=get_font_name();
    n_tp_on2->fsize=get_font_size();
    n_tp_on2->color_on = get_font_color_on_2();
    n_tp_on2->color_off = get_font_color_off();
    n_tp_on2->bstate=get_blink_2();
    n_tp_on2->shadow = get_font_shadow_on();
    n_tp_on2->al = get_alignment();
    num_text_.add_mode_text( n_tp_on2 );

    if( get_property_use_image() )
    {
      image_.insert_mode(get_mode_2(), get_priority_2(),get_blink_2());
      image_.set_path( get_mode_2(), get_on_image_path_2() );
      if(get_use_back_2() == true)
        image_.set_path2( get_mode_2(), get_back_image_path_2() );
    }
    if(get_state_di_2() != UniWidgetsTypes::DefaultObjectId)
    {
      /*Image*/
      StateLogic *sl2 = new StateLogic();
      sl2->set_mode( get_mode_2() );
      sl2->set_detntr( get_detntr_on_2() );
      sl2->set_state_ai( get_state_di_2() );
      sl2->set_state_obj_ai(get_self_state_ai());
      sl2->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      sl2->set_visible(false);
#else
      sl2->hide();
#endif
      if( get_property_use_image() )
      {
        image_.add_child(sl2, typeLogic);
        image_logics.insert(std::pair<long, StateLogic *>(2, sl2 ) );
      }
      /*Text*/
      StateLogic *stl2 = new StateLogic();
      stl2->set_state_ai( get_state_di_2() );
      stl2->set_state_obj_ai(get_self_state_ai());
      stl2->set_mode( get_mode_2() );
      stl2->set_detntr(get_detntr_on_2());
      stl2->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      stl2->set_visible(false);
#else
      stl2->hide();
#endif
      text_.add_child(stl2, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(2, stl2 ) );

      StateLogic *nstl2 = new StateLogic();
      nstl2->set_state_ai( get_state_di_2() );
      nstl2->set_mode( get_mode_2() );
      nstl2->set_detntr(get_detntr_on_2());
      nstl2->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      nstl2->set_visible(false);
#else
      nstl2->hide();
#endif
      num_text_.add_child(nstl2, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(2, nstl2 ) );

      if( get_property_use_image() )
      {
        sl2->set_lock_view(get_property_lock_view());
      }else
        stl2->set_lock_view(get_property_lock_view());
    }
  }

  if( get_mode_3() != UniWidgetsTypes::DefaultObjectId )
  {
    /*Configure TEXT/ON */
    text_prop* tp_on3 = new text_prop();
    tp_on3->mode=get_mode_3();
    tp_on3->priority=get_priority_3();
    if( get_text_3() == "" )
      tp_on3->text=get_text();
    else
      tp_on3->text=get_text_3();
    tp_on3->fname=get_font_name();
    tp_on3->fsize=get_font_size();
    tp_on3->color_on = get_font_color_on_3();
    tp_on3->color_off = get_font_color_off();
    tp_on3->bstate=get_blink_3();
    tp_on3->shadow = get_font_shadow_on();
    tp_on3->al = get_alignment();
    text_.add_mode_text( tp_on3 );

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_on3 = new text_prop();
    n_tp_on3->mode=get_mode_3();
    n_tp_on3->priority=get_priority_3();
    n_tp_on3->text=get_num_text_3();
    n_tp_on3->fname=get_font_name();
    n_tp_on3->fsize=get_font_size();
    n_tp_on3->color_on = get_font_color_on_3();
    n_tp_on3->color_off = get_font_color_off();
    n_tp_on3->bstate=get_blink_3();
    n_tp_on3->shadow = get_font_shadow_on();
    n_tp_on3->al = get_alignment();
    num_text_.add_mode_text( n_tp_on3 );

    if( get_property_use_image() )
    {
      image_.insert_mode(get_mode_3(), get_priority_3(),get_blink_3());
      image_.set_path( get_mode_3(), get_on_image_path_3() );
      if(get_use_back_3() == true)
        image_.set_path2( get_mode_3(), get_back_image_path_3() );
    }
    if(get_state_di_3() != UniWidgetsTypes::DefaultObjectId)
    {
      /*Image*/
      StateLogic *sl3 = new StateLogic();
      sl3->set_mode( get_mode_3() );
      sl3->set_detntr( get_detntr_on_3() );
      sl3->set_state_ai( get_state_di_3() );
      sl3->set_state_obj_ai(get_self_state_ai());
      sl3->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      sl3->set_visible(false);
#else
      sl3->hide();
#endif
      if( get_property_use_image() )
      {
        image_.add_child(sl3, typeLogic);
        image_logics.insert(std::pair<long, StateLogic *>(3, sl3 ) );
      }
      /*Text*/
      StateLogic *stl3 = new StateLogic();
      stl3->set_state_ai( get_state_di_3() );
      stl3->set_state_obj_ai(get_self_state_ai());
      stl3->set_mode( get_mode_3() );
      stl3->set_detntr(get_detntr_on_3());
      stl3->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      stl3->set_visible(false);
#else
      stl3->hide();
#endif
      text_.add_child(stl3, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(3, stl3 ) );

      StateLogic *nstl3 = new StateLogic();
      nstl3->set_state_ai( get_state_di_3() );
      nstl3->set_mode( get_mode_3() );
      nstl3->set_detntr(get_detntr_on_3());
      nstl3->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      nstl3->set_visible(false);
#else
      nstl3->hide();
#endif
      num_text_.add_child(nstl3, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(3, nstl3 ) );

      if( get_property_use_image() )
      {
        sl3->set_lock_view(get_property_lock_view());
      }else
        stl3->set_lock_view(get_property_lock_view());
    }
  }

  if( get_mode_4() != UniWidgetsTypes::DefaultObjectId )
  {
    /*Configure TEXT/ON */
    text_prop* tp_on4 = new text_prop();
    tp_on4->mode=get_mode_4();
    tp_on4->priority=get_priority_4();
    if( get_text_4() == "" )
      tp_on4->text=get_text();
    else
      tp_on4->text=get_text_4();
    tp_on4->fname=get_font_name();
    tp_on4->fsize=get_font_size();
    tp_on4->color_on = get_font_color_on_4();
    tp_on4->color_off = get_font_color_off();
    tp_on4->bstate=get_blink_4();
    tp_on4->shadow = get_font_shadow_on();
    tp_on4->al = get_alignment();
    text_.add_mode_text( tp_on4 );

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_on4 = new text_prop();
    n_tp_on4->mode=get_mode_4();
    n_tp_on4->priority=get_priority_4();
    n_tp_on4->text=get_num_text_4();
    n_tp_on4->fname=get_font_name();
    n_tp_on4->fsize=get_font_size();
    n_tp_on4->color_on = get_font_color_on_4();
    n_tp_on4->color_off = get_font_color_off();
    n_tp_on4->bstate=get_blink_4();
    n_tp_on4->shadow = get_font_shadow_on();
    n_tp_on4->al = get_alignment();
    num_text_.add_mode_text( n_tp_on4 );

    if( get_property_use_image() )
    {
      image_.insert_mode(get_mode_4(), get_priority_4(),get_blink_4());
      image_.set_path( get_mode_4(), get_on_image_path_4() );
      if(get_use_back_4() == true)
        image_.set_path2( get_mode_4(), get_back_image_path_4() );
    }
    if(get_state_di_4() != UniWidgetsTypes::DefaultObjectId)
    {
      /*Image*/
      StateLogic *sl4 = new StateLogic();
      sl4->set_mode( get_mode_4() );
      sl4->set_detntr( get_detntr_on_4() );
      sl4->set_state_ai( get_state_di_4() );
      sl4->set_state_obj_ai(get_self_state_ai());
      sl4->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      sl4->set_visible(false);
#else
      sl4->hide();
#endif
      if( get_property_use_image() )
      {
        image_.add_child(sl4, typeLogic);
        image_logics.insert(std::pair<long, StateLogic *>(4, sl4 ) );
      }
      /*Text*/
      StateLogic *stl4 = new StateLogic();
      stl4->set_state_ai( get_state_di_4() );
      stl4->set_state_obj_ai(get_self_state_ai());
      stl4->set_mode( get_mode_4() );
      stl4->set_detntr(get_detntr_on_4());
      stl4->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      stl4->set_visible(false);
#else
      stl4->hide();
#endif
      text_.add_child(stl4, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(4, stl4 ) );

      StateLogic *nstl4 = new StateLogic();
      nstl4->set_state_ai( get_state_di_4() );
      nstl4->set_mode( get_mode_4() );
      nstl4->set_detntr(get_detntr_on_4());
      nstl4->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      nstl4->set_visible(false);
#else
      nstl4->hide();
#endif
      num_text_.add_child(nstl4, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(4, nstl4 ) );

      if( get_property_use_image() )
      {
        sl4->set_lock_view(get_property_lock_view());
      }else
        stl4->set_lock_view(get_property_lock_view());
    }
  }

  if( get_mode_5() != UniWidgetsTypes::DefaultObjectId )
  {
    /*Configure TEXT/ON */
    text_prop* tp_on5 = new text_prop();
    tp_on5->mode=get_mode_5();
    tp_on5->priority=get_priority_5();
    if( get_text_5() == "" )
      tp_on5->text=get_text();
    else
      tp_on5->text=get_text_5();
    tp_on5->fname=get_font_name();
    tp_on5->fsize=get_font_size();
    tp_on5->color_on = get_font_color_on_5();
    tp_on5->color_off = get_font_color_off();
    tp_on5->bstate=get_blink_5();
    tp_on5->shadow = get_font_shadow_on();
    tp_on5->al = get_alignment();
    text_.add_mode_text( tp_on5 );

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_on5 = new text_prop();
    n_tp_on5->mode=get_mode_5();
    n_tp_on5->priority=get_priority_5();
    n_tp_on5->text=get_num_text_5();
    n_tp_on5->fname=get_font_name();
    n_tp_on5->fsize=get_font_size();
    n_tp_on5->color_on = get_font_color_on_5();
    n_tp_on5->color_off = get_font_color_off();
    n_tp_on5->bstate=get_blink_5();
    n_tp_on5->shadow = get_font_shadow_on();
    n_tp_on5->al = get_alignment();
    num_text_.add_mode_text( n_tp_on5 );

    if( get_property_use_image() )
    {
      image_.insert_mode(get_mode_5(), get_priority_5(),get_blink_5());
      image_.set_path( get_mode_5(), get_on_image_path_5() );
      if(get_use_back_5() == true)
        image_.set_path2( get_mode_5(), get_back_image_path_5() );
    }
    if(get_state_di_5() != UniWidgetsTypes::DefaultObjectId)
    {
      /*Image*/
      StateLogic *sl5 = new StateLogic();
      sl5->set_mode( get_mode_5() );
      sl5->set_detntr( get_detntr_on_5() );
      sl5->set_state_ai( get_state_di_5() );
      sl5->set_state_obj_ai(get_self_state_ai());
      sl5->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      sl5->set_visible(false);
#else
      sl5->hide();
#endif
      if( get_property_use_image() )
      {
        image_.add_child(sl5, typeLogic);
        image_logics.insert(std::pair<long, StateLogic *>(5, sl5 ) );
      }
      /*Text*/
      StateLogic *stl5 = new StateLogic();
      stl5->set_state_ai( get_state_di_5() );
      stl5->set_state_obj_ai(get_self_state_ai());
      stl5->set_mode( get_mode_5() );
      stl5->set_detntr(get_detntr_on_5());
      stl5->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      stl5->set_visible(false);
#else
      stl5->hide();
#endif
      text_.add_child(stl5, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(5, stl5 ) );

      StateLogic *nstl5 = new StateLogic();
      nstl5->set_state_ai( get_state_di_5() );
      nstl5->set_mode( get_mode_5() );
      nstl5->set_detntr(get_detntr_on_5());
      nstl5->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      nstl5->set_visible(false);
#else
      nstl5->hide();
#endif
      num_text_.add_child(nstl5, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(5, nstl5 ) );

      if( get_property_use_image() )
      {
        sl5->set_lock_view(get_property_lock_view());
      }else
        stl5->set_lock_view(get_property_lock_view());
    }
  }

  if( get_mode_6() != UniWidgetsTypes::DefaultObjectId )
  {
    /*Configure TEXT/ON */
    text_prop* tp_on6 = new text_prop();
    tp_on6->mode=get_mode_6();
    tp_on6->priority=get_priority_6();
    if( get_text_6() == "" )
      tp_on6->text=get_text();
    else
      tp_on6->text=get_text_6();
    tp_on6->fname=get_font_name();
    tp_on6->fsize=get_font_size();
    tp_on6->color_on = get_font_color_on_6();
    tp_on6->color_off = get_font_color_off();
    tp_on6->bstate=get_blink_6();
    tp_on6->shadow = get_font_shadow_on();
    tp_on6->al = get_alignment();
    text_.add_mode_text( tp_on6 );

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_on6 = new text_prop();
    n_tp_on6->mode=get_mode_6();
    n_tp_on6->priority=get_priority_6();
    n_tp_on6->text=get_num_text_6();
    n_tp_on6->fname=get_font_name();
    n_tp_on6->fsize=get_font_size();
    n_tp_on6->color_on = get_font_color_on_6();
    n_tp_on6->color_off = get_font_color_off();
    n_tp_on6->bstate=get_blink_6();
    n_tp_on6->shadow = get_font_shadow_on();
    n_tp_on6->al = get_alignment();
    num_text_.add_mode_text( n_tp_on6 );

    if( get_property_use_image() )
    {
      image_.insert_mode(get_mode_6(), get_priority_6(),get_blink_6());
      image_.set_path( get_mode_6(), get_on_image_path_6() );
      if(get_use_back_6() == true)
        image_.set_path2( get_mode_6(), get_back_image_path_6() );
    }
    if(get_state_di_6() != UniWidgetsTypes::DefaultObjectId)
    {
      /*Image*/
      StateLogic *sl6 = new StateLogic();
      sl6->set_mode( get_mode_6() );
      sl6->set_detntr( get_detntr_on_6() );
      sl6->set_state_ai( get_state_di_6() );
      sl6->set_state_obj_ai(get_self_state_ai());
      sl6->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      sl6->set_visible(false);
#else
      sl6->hide();
#endif
      if( get_property_use_image() )
      {
        image_.add_child(sl6, typeLogic);
        image_logics.insert(std::pair<long, StateLogic *>(6, sl6 ) );
      }
      /*Text*/
      StateLogic *stl6 = new StateLogic();
      stl6->set_state_ai( get_state_di_6() );
      stl6->set_state_obj_ai(get_self_state_ai());
      stl6->set_mode( get_mode_6() );
      stl6->set_detntr(get_detntr_on_6());
      stl6->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      stl6->set_visible(false);
#else
      stl6->hide();
#endif
      text_.add_child(stl6, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(6, stl6 ) );

      StateLogic *nstl6 = new StateLogic();
      nstl6->set_state_ai( get_state_di_6() );
      nstl6->set_mode( get_mode_6() );
      nstl6->set_detntr(get_detntr_on_6());
      nstl6->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      nstl6->set_visible(false);
#else
      nstl6->hide();
#endif
      num_text_.add_child(nstl6, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(6, nstl6 ) );

      if( get_property_use_image() )
      {
        sl6->set_lock_view(get_property_lock_view());
      }else
        stl6->set_lock_view(get_property_lock_view());
    }
  }

  if( get_mode_7() != UniWidgetsTypes::DefaultObjectId )
  {
    /*Configure TEXT/ON */
    text_prop* tp_on7 = new text_prop();
    tp_on7->mode=get_mode_7();
    tp_on7->priority=get_priority_7();
    if( get_text_7() == "" )
      tp_on7->text=get_text();
    else
      tp_on7->text=get_text_7();
    tp_on7->fname=get_font_name();
    tp_on7->fsize=get_font_size();
    tp_on7->color_on = get_font_color_on_7();
    tp_on7->color_off = get_font_color_off();
    tp_on7->bstate=get_blink_7();
    tp_on7->shadow = get_font_shadow_on();
    tp_on7->al = get_alignment();
    text_.add_mode_text( tp_on7 );

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_on7 = new text_prop();
    n_tp_on7->mode=get_mode_7();
    n_tp_on7->priority=get_priority_7();
    n_tp_on7->text=get_num_text_7();
    n_tp_on7->fname=get_font_name();
    n_tp_on7->fsize=get_font_size();
    n_tp_on7->color_on = get_font_color_on_7();
    n_tp_on7->color_off = get_font_color_off();
    n_tp_on7->bstate=get_blink_7();
    n_tp_on7->shadow = get_font_shadow_on();
    n_tp_on7->al = get_alignment();
    num_text_.add_mode_text( n_tp_on7 );

    if( get_property_use_image() )
    {
      image_.insert_mode(get_mode_7(), get_priority_7(),get_blink_7());
      image_.set_path( get_mode_7(), get_on_image_path_7() );
      if(get_use_back_7() == true)
        image_.set_path2( get_mode_7(), get_back_image_path_7() );
    }
    if(get_state_di_7() != UniWidgetsTypes::DefaultObjectId)
    {
      /*Image*/
      StateLogic *sl7 = new StateLogic();
      sl7->set_mode( get_mode_7() );
      sl7->set_detntr( get_detntr_on_7() );
      sl7->set_state_ai( get_state_di_7() );
      sl7->set_state_obj_ai(get_self_state_ai());
      sl7->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      sl7->set_visible(false);
#else
      sl7->hide();
#endif
      if( get_property_use_image() )
      {
        image_.add_child(sl7, typeLogic);
        image_logics.insert(std::pair<long, StateLogic *>(7, sl7 ) );
      }
      /*Text*/
      StateLogic *stl7 = new StateLogic();
      stl7->set_state_ai( get_state_di_7() );
      stl7->set_state_obj_ai(get_self_state_ai());
      stl7->set_mode( get_mode_7() );
      stl7->set_detntr(get_detntr_on_7());
      stl7->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      stl7->set_visible(false);
#else
      stl7->hide();
#endif
      text_.add_child(stl7, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(7, stl7 ) );

      StateLogic *nstl7 = new StateLogic();
      nstl7->set_state_ai( get_state_di_7() );
      nstl7->set_mode( get_mode_7() );
      nstl7->set_detntr(get_detntr_on_7());
      nstl7->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      nstl7->set_visible(false);
#else
      nstl7->hide();
#endif
      num_text_.add_child(nstl7, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(7, nstl7 ) );

      if( get_property_use_image() )
      {
        sl7->set_lock_view(get_property_lock_view());
      }else
        stl7->set_lock_view(get_property_lock_view());
    }
  }

  if( get_mode_8() != UniWidgetsTypes::DefaultObjectId )
  {
    /*Configure TEXT/ON */
    text_prop* tp_on8 = new text_prop();
    tp_on8->mode=get_mode_8();
    tp_on8->priority=get_priority_8();
    if( get_text_8() == "" )
      tp_on8->text=get_text();
    else
      tp_on8->text=get_text_8();
    tp_on8->fname=get_font_name();
    tp_on8->fsize=get_font_size();
    tp_on8->color_on = get_font_color_on_8();
    tp_on8->color_off = get_font_color_off();
    tp_on8->bstate=get_blink_8();
    tp_on8->shadow = get_font_shadow_on();
    tp_on8->al = get_alignment();
    text_.add_mode_text( tp_on8 );

    /*Configure NUM TEXT/ON */
    text_prop* n_tp_on8 = new text_prop();
    n_tp_on8->mode=get_mode_8();
    n_tp_on8->priority=get_priority_8();
    n_tp_on8->text=get_num_text_8();
    n_tp_on8->fname=get_font_name();
    n_tp_on8->fsize=get_font_size();
    n_tp_on8->color_on = get_font_color_on_8();
    n_tp_on8->color_off = get_font_color_off();
    n_tp_on8->bstate=get_blink_8();
    n_tp_on8->shadow = get_font_shadow_on();
    n_tp_on8->al = get_alignment();
    num_text_.add_mode_text( n_tp_on8 );

    if( get_property_use_image() )
    {
      image_.insert_mode(get_mode_8(), get_priority_8(),get_blink_8());
      image_.set_path( get_mode_8(), get_on_image_path_8() );
      if(get_use_back_8() == true)
        image_.set_path2( get_mode_8(), get_back_image_path_8() );
    }
    if(get_state_di_8() != UniWidgetsTypes::DefaultObjectId)
    {
      /*Image*/
      StateLogic *sl8 = new StateLogic();
      sl8->set_mode( get_mode_8() );
      sl8->set_detntr( get_detntr_on_8() );
      sl8->set_state_ai( get_state_di_8() );
      sl8->set_state_obj_ai(get_self_state_ai());
      sl8->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      sl8->set_visible(false);
#else
      sl8->hide();
#endif
      if( get_property_use_image() )
      {
        image_.add_child(sl8, typeLogic);
        image_logics.insert(std::pair<long, StateLogic *>(8, sl8 ) );
      }
      /*Text*/
      StateLogic *stl8 = new StateLogic();
      stl8->set_state_ai( get_state_di_8() );
      stl8->set_state_obj_ai(get_self_state_ai());
      stl8->set_mode( get_mode_8() );
      stl8->set_detntr(get_detntr_on_8());
      stl8->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      stl8->set_visible(false);
#else
      stl8->hide();
#endif
      text_.add_child(stl8, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(8, stl8 ) );

      StateLogic *nstl8 = new StateLogic();
      nstl8->set_state_ai( get_state_di_8() );
      nstl8->set_mode( get_mode_8() );
      nstl8->set_detntr(get_detntr_on_8());
      nstl8->set_node( get_property_node() );
#if GTK_VERSION_GE(2,18)
      nstl8->set_visible(false);
#else
      nstl8->hide();
#endif
      num_text_.add_child(nstl8, typeLogic);
      text_logics.insert(std::pair<long, StateLogic *>(8, nstl8 ) );

      if( get_property_use_image() )
      {
        sl8->set_lock_view(get_property_lock_view());
      }else
        stl8->set_lock_view(get_property_lock_view());
    }
  }
  if( get_property_use_image() )
  {
    image_.configure();
    /* The SimpleObject::on_realize will be called after this put */
    put( image_,
        image_rect->get_x(),
        image_rect->get_y() );

    image_.show();
  }
  text_.configure();
  /* The SimpleObject::on_realize will be called after this put */
  put( text_,
      text_rect->get_x(),
      text_rect->get_y() );
  num_text_.configure();
  put( num_text_,
      num_text_rect->get_x(),
      num_text_rect->get_y() );

  show();
  text_.show();
  num_text_.show();
}
// -------------------------------------------------------------------------
void SensorM::set_rectangle(Gdk::Rectangle* rect_,const long x, const long y, const long w, const long h)
{
  rect_->set_x(x);
  rect_->set_y(y);
  rect_->set_width(w);
  rect_->set_height(h);
}
// -------------------------------------------------------------------------
void SensorM::on_rectangle_changed()
{
  set_size_request(-1,-1);
  set_rectangle(text_rect,get_x_text_rect(), get_y_text_rect(), -1, -1);
  set_rectangle(num_text_rect,get_x_num_text_rect(), get_y_num_text_rect(), -1, -1);
  set_rectangle(image_rect,get_x_image_rect(), get_y_image_rect(), get_w_image_rect(), get_h_image_rect());

  if(is_configured)
  {
    Gtk::Widget *w;
    if( get_property_use_image() )
    {
      w = dynamic_cast<Gtk::Widget* >(&image_);
      move(*w,get_x_image_rect(), get_y_image_rect());
    }
    if(get_text() != "")
    {
      w = dynamic_cast<Gtk::Widget* >(&text_);
      move(*w,get_x_text_rect(), get_y_text_rect());
    }
    if(get_num_text_1() != "" || get_num_text_2() != "" || get_num_text_3() != "" || get_num_text_4() != ""
      || get_num_text_5() != "" || get_num_text_6() != "" || get_num_text_7() != "" || get_num_text_8() != "")
    {
      w = dynamic_cast<Gtk::Widget* >(&num_text_);
      move(*w,get_x_num_text_rect(), get_y_num_text_rect());
    }
  }

  queue_resize();
}
// -------------------------------------------------------------------------
void SensorM::on_text_changed(TypicalText *tx, const Glib::Property<Glib::ustring> *proper)
{
  for(std::vector<Text *>::iterator it = tx->texts_.begin();it != tx->texts_.end(); it++)
    (*it)->set_property_text_( proper->get_value() );
  queue_resize();
}
// -------------------------------------------------------------------------
