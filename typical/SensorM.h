#ifndef _SENSORM_H
#define _SENSORM_H
// -------------------------------------------------------------------------
#include <string>
#include <unordered_map>
#include <objects/SimpleObject.h>
#include <objects/LinkLogic.h>
#include <objects/StateLogic.h>
#include <typical/TypicalState.h>
#include <typical/TypicalText.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой виджет сенсор.
 * \par
 * Виджет отображает работу сенсора. Данный сенсор отличается от Sensor1 тем,
 * что у него есть до 9 режимов вместо одного. Значит при работе с аналоговым датчиком,
 * можно отображать до 9 различных режимов, либо использовать до 8 дискретных датчиков для
 * каждого режима. Каждый режим имеет приоритет для определения порядка отображения,а
 * также свой набор свойств(текст, номер сигнала и картинки). Также можно комбинировать
 * использование аналоговых и дискретных датчиков.
 * 
 * \n Пример виджета представлен надписью "Низкое сопротивление изоляции"(всего пунктов 8,
 * но надпись загорается и мигает по каждому из них): \n \n
 * \image html list_sensorm.png
 */
class SensorM : public SimpleObject
{
public:
  SensorM();
  explicit SensorM(SimpleObject::BaseObjectType* gobject);
  virtual ~SensorM();

  /* Constants */
  Gdk::Rectangle *text_rect;      /*!< координаты и размер текста сенсора */
  Gdk::Rectangle *num_text_rect;      /*!< координаты и размер текстового номера сигнала сенсора */
  Gdk::Rectangle *image_rect;      /*!< координаты и размер картинки */

protected:
  /* Handlers */
  virtual void on_realize();
  virtual void on_connect() throw();

private:
  /* Variables */
  std::unordered_map<long ,text_prop *> texts_prop;
  std::unordered_map<long , Glib::Property<long> *> properties;
  std::unordered_map<long ,StateLogic *> text_logics;
  std::unordered_map<long ,StateLogic *> image_logics;
  TypicalState image_;
  TypicalText text_;
  TypicalText num_text_;
  LinkLogic link_;
  bool is_configured;

  /* Methods */
  void constructor();
  void on_configure();
  void on_configure_property();
  void on_rectangle_changed();
  void on_text_changed(TypicalText *, const Glib::Property<Glib::ustring> *);
  void set_rectangle(Gdk::Rectangle* rect_,const long x, const long y, const long w, const long h);

  DISALLOW_COPY_AND_ASSIGN(SensorM);

  /* Properties */
  ADD_PROPERTY( state_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id аналогового датчика состояния сенсора(используется для аналогового датчика с множеством ржимов) */
  ADD_PROPERTY( property_node, UniWidgetsTypes::ObjectId )    /*!< свойство: id узла */
  ADD_PROPERTY( self_state_ai, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика внутреннего состояния сенсора */
  ADD_PROPERTY( property_link_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика состояния связи с SharedMemory */

  ADD_PROPERTY( property_use_image, bool )      /*!< свойство: использовать картинку в виджете */

  ADD_PROPERTY( state_di_1, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика состояния сенсора для певого режима */
  ADD_PROPERTY( num_text_1, Glib::ustring )      /*!< свойство: текстовый номер сигнала сенсора для певого режима */
  ADD_PROPERTY( text_1, Glib::ustring )        /*!< свойство: текст сенсора для певого режима */
  ADD_PROPERTY( blink_1, bool )          /*!< свойство: мигать в режиме включен для певого режима */
  ADD_PROPERTY( mode_1, long )          /*!< свойство: режим сенсора для певого режима */
  ADD_PROPERTY( detntr_on_1, long )        /*!< свойство: значение "детонатора" для певого режима */
  ADD_PROPERTY( priority_1, long )        /*!< свойство: приоритет режима включен для певого режима */
  ADD_PROPERTY( font_color_on_1, Gdk::Color )      /*!< свойство: цвет шрифта в режиме включен для певого режима */
  ADD_PROPERTY( on_image_path_1, Glib::ustring )      /*!< свойство: картинка для режима включен для певого режима */
  ADD_PROPERTY( use_back_1, bool )        /*!< свойство: использовать подложку для картинки режима включен при мигании для певого режима */
  ADD_PROPERTY( back_image_path_1, Glib::ustring )    /*!< свойство: картинка подложки для режима включен для певого режима */

  ADD_PROPERTY( state_di_2, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( num_text_2, Glib::ustring )
  ADD_PROPERTY( text_2, Glib::ustring )
  ADD_PROPERTY( blink_2, bool )
  ADD_PROPERTY( mode_2, long )
  ADD_PROPERTY( detntr_on_2, long )
  ADD_PROPERTY( priority_2, long )
  ADD_PROPERTY( font_color_on_2, Gdk::Color )
  ADD_PROPERTY( on_image_path_2, Glib::ustring )
  ADD_PROPERTY( use_back_2, bool )
  ADD_PROPERTY( back_image_path_2, Glib::ustring )

  ADD_PROPERTY( state_di_3, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( num_text_3, Glib::ustring )
  ADD_PROPERTY( text_3, Glib::ustring )
  ADD_PROPERTY( blink_3, bool )
  ADD_PROPERTY( mode_3, long )
  ADD_PROPERTY( detntr_on_3, long )
  ADD_PROPERTY( priority_3, long )
  ADD_PROPERTY( font_color_on_3, Gdk::Color )
  ADD_PROPERTY( on_image_path_3, Glib::ustring )
  ADD_PROPERTY( use_back_3, bool )
  ADD_PROPERTY( back_image_path_3, Glib::ustring )

  ADD_PROPERTY( state_di_4, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( num_text_4, Glib::ustring )
  ADD_PROPERTY( text_4, Glib::ustring )
  ADD_PROPERTY( blink_4, bool )
  ADD_PROPERTY( mode_4, long )
  ADD_PROPERTY( detntr_on_4, long )
  ADD_PROPERTY( priority_4, long )
  ADD_PROPERTY( font_color_on_4, Gdk::Color )
  ADD_PROPERTY( on_image_path_4, Glib::ustring )
  ADD_PROPERTY( use_back_4, bool )
  ADD_PROPERTY( back_image_path_4, Glib::ustring )

  ADD_PROPERTY( state_di_5, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( num_text_5, Glib::ustring )
  ADD_PROPERTY( text_5, Glib::ustring )
  ADD_PROPERTY( blink_5, bool )
  ADD_PROPERTY( mode_5, long )
  ADD_PROPERTY( detntr_on_5, long )
  ADD_PROPERTY( priority_5, long )
  ADD_PROPERTY( font_color_on_5, Gdk::Color )
  ADD_PROPERTY( on_image_path_5, Glib::ustring )
  ADD_PROPERTY( use_back_5, bool )
  ADD_PROPERTY( back_image_path_5, Glib::ustring )

  ADD_PROPERTY( state_di_6, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( num_text_6, Glib::ustring )
  ADD_PROPERTY( text_6, Glib::ustring )
  ADD_PROPERTY( blink_6, bool )
  ADD_PROPERTY( mode_6, long )
  ADD_PROPERTY( detntr_on_6, long )
  ADD_PROPERTY( priority_6, long )
  ADD_PROPERTY( font_color_on_6, Gdk::Color )
  ADD_PROPERTY( on_image_path_6, Glib::ustring )
  ADD_PROPERTY( use_back_6, bool )
  ADD_PROPERTY( back_image_path_6, Glib::ustring )

  ADD_PROPERTY( state_di_7, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( num_text_7, Glib::ustring )
  ADD_PROPERTY( text_7, Glib::ustring )
  ADD_PROPERTY( blink_7, bool )
  ADD_PROPERTY( mode_7, long )
  ADD_PROPERTY( detntr_on_7, long )
  ADD_PROPERTY( priority_7, long )
  ADD_PROPERTY( font_color_on_7, Gdk::Color )
  ADD_PROPERTY( on_image_path_7, Glib::ustring )
  ADD_PROPERTY( use_back_7, bool )
  ADD_PROPERTY( back_image_path_7, Glib::ustring )

  ADD_PROPERTY( state_di_8, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( num_text_8, Glib::ustring )
  ADD_PROPERTY( text_8, Glib::ustring )
  ADD_PROPERTY( blink_8, bool )
  ADD_PROPERTY( mode_8, long )
  ADD_PROPERTY( detntr_on_8, long )
  ADD_PROPERTY( priority_8, long )
  ADD_PROPERTY( font_color_on_8, Gdk::Color )
  ADD_PROPERTY( on_image_path_8, Glib::ustring )
  ADD_PROPERTY( use_back_8, bool )
  ADD_PROPERTY( back_image_path_8, Glib::ustring )

  ADD_PROPERTY( img_off_path, Glib::ustring )      /*!< свойство: картинка для режима выключен */
  ADD_PROPERTY( text, Glib::ustring )        /*!< свойство: текст сенсора */
  ADD_PROPERTY( font_name, Glib::ustring )      /*!< свойство: шрифт текста */
  ADD_PROPERTY( font_size, long )          /*!< свойство: размер шрифта */
  ADD_PROPERTY( font_color_off, Gdk::Color )      /*!< свойство: цвет шрифта в режиме выключен */
  ADD_PROPERTY( font_shadow_on, bool )        /*!< свойство: тень в режиме включен */
  ADD_PROPERTY( font_shadow_off, bool )        /*!< свойство: тень в режиме выключен */
  ADD_PROPERTY( alignment, Pango::Alignment )      /*!< свойство: выравнивание текста */
  ADD_PROPERTY( number_of_mode, long )        /*!< свойство: число режимов(неиспользуется!) */
  
  ADD_PROPERTY( x_text_rect, long )        /*!< свойство: x-координата текста сенсора */
  ADD_PROPERTY( y_text_rect, long )        /*!< свойство: y-координата текста сенсора */
  ADD_PROPERTY( x_num_text_rect, long )        /*!< свойство: x-координата текстового номера сигнала сенсора */
  ADD_PROPERTY( y_num_text_rect, long )        /*!< свойство: y-координата текстового номера сигнала сенсора */
  ADD_PROPERTY( x_image_rect, long )        /*!< свойство: x-координата картинки */
  ADD_PROPERTY( y_image_rect, long )        /*!< свойство: y-координата картинки */
  ADD_PROPERTY( w_image_rect, long )        /*!< свойство: ширина картинки */
  ADD_PROPERTY( h_image_rect, long )        /*!< свойство: высота картинки */
};

}

#endif
