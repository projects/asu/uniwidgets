#ifndef _TYPICAL_H
#define _TYPICAL_H

#include <typical/Cistern.h>
#include <typical/GDG.h>
#include <typical/IndicatorTwoState.h>
#include <typical/IndicatorFourState.h>
#include <typical/VDG.h>
#include <typical/ADG.h>
#include <typical/Sensor1.h>
#include <typical/SensorM.h>
#include <typical/RIndicatingInstrument.h>

#endif
