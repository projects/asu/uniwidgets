#include <iostream>
#include <types.h>
#include "TypicalCistern.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void TypicalCistern::constructor()
{
	create_cistern();

	put(*cistern_,0,0);

	add_child(cistern_, typeCistern);
	add_child(&logic_, typeLogic);
#if GTK_VERSION_GE(2,18)
	logic_.set_visible(false);
#else
	logic_.hide();
#endif
}
// -------------------------------------------------------------------------
TypicalCistern::TypicalCistern() :
	Glib::ObjectBase("typicalcistern")
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalCistern::TypicalCistern(SimpleObject::BaseObjectType* gobject) :
	SimpleObject(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalCistern::~TypicalCistern()
{
}
// -------------------------------------------------------------------------
void TypicalCistern::set_rect(const Gdk::Rectangle rect)
{
	rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalCistern::set_background_path(const Glib::ustring& path)
{
	background_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalCistern::set_filling_path(const Glib::ustring& path)
{
	filling_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalCistern::set_scale_path(const Glib::ustring& path)
{
	scale_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalCistern::create_cistern()
{
	cistern_ = new CisternImage();
}
// -------------------------------------------------------------------------
void TypicalCistern::configure()
{
	set_property_disconnect_effect(0);//Отключаем рисование серой области.

	Gdk::Rectangle* rect = get_rect();
	cistern_->set_size_request(rect->get_width(),rect->get_height());
	cistern_->set_background_image_path(get_background_path());
	cistern_->set_filling_image_path(get_filling_path());
	cistern_->set_scale_image_path(get_scale_path());
	cistern_->show();
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCistern::get_background_path()
{
	return background_path_;
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCistern::get_filling_path()
{
	return filling_path_;
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCistern::get_scale_path()
{
	return scale_path_;
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalCistern::get_rect()
{
	return &rect_;
}
// -------------------------------------------------------------------------
void TypicalCistern::set_scale_switch_on(bool switch_on)
{
	cistern_->set_scale_switch(switch_on);
}
// -------------------------------------------------------------------------
void TypicalCistern::set_value_ai(const UniWidgetsTypes::ObjectId sensor)
{
	logic_.set_value_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalCistern::set_node(const UniWidgetsTypes::ObjectId node)
{
	logic_.set_node(node);
}
// -------------------------------------------------------------------------
