#ifndef _TYPICALCISTERN_H
#define _TYPICALCISTERN_H
// -------------------------------------------------------------------------
#include <objects/SimpleObject.h>
#include <objects/CisternLogic.h>
#include <components/CisternImage.h>
#include <USignals.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой контейнер SimpleObject цистерна.
 * \par
 * Это контейнер содержащий логику и компоненты для отображения цистерны
 * с уровнем.
 */
class TypicalCistern : public SimpleObject
{
public:
	TypicalCistern();
	explicit TypicalCistern(SimpleObject::BaseObjectType* gobject);
	virtual ~TypicalCistern();

	/* Methods */
	void configure();						/*!< конфигурирование контейнера */

	void set_rect(const Gdk::Rectangle rect);			/*!< задать размеры контейнера */
	void set_background_path(const Glib::ustring& path);		/*!< задать картинку для фона цистерны */
	void set_filling_path(const Glib::ustring& path);		/*!< задать картинку для заполнителя цистерны */
	void set_scale_path(const Glib::ustring& path);			/*!< задать картинку для шкалы цистерны */
	void set_value_ai(const UniWidgetsTypes::ObjectId sensor);		/*!< задать аналоговый датчик для уровня цистерны */
	void set_node(const UniWidgetsTypes::ObjectId node);		/*!< задать узел датчика */
	void set_scale_switch_on(bool switch_on);			/*!< задать отображение шкалы цистерны */

	Glib::ustring& get_background_path();				/*!< получить картинку для фона цистерны */
	Glib::ustring& get_filling_path();				/*!< получить картинку для заполнителя цистерны */
	Glib::ustring& get_scale_path();				/*!< получить картинку для шкалы цистерны */
	Gdk::Rectangle* get_rect();					/*!< пучить размеры цистерны */

private:
	/* Variables */
	Gdk::Rectangle rect_;
	CisternLogic logic_;
	CisternImage *cistern_;
	Glib::ustring background_path_;
	Glib::ustring filling_path_;
	Glib::ustring scale_path_;
	
	/* Methods */
	void create_cistern();
	void constructor();

	DISALLOW_COPY_AND_ASSIGN(TypicalCistern);
};

}
#endif
