#include <iostream>
#include <types.h>
#include "TypicalCisternBlink.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void TypicalCisternBlink::constructor()
{
	create_cistern();

	Gdk::Rectangle* rect = get_rect();
	put(*cistern_,0,0);

	add_child(cistern_, typeCistern);
	add_child(&blinklogic_, typeLogic);
	add_child(&statelogichigh_warn_, typeLogic);
	add_child(&statelogiclow_warn_, typeLogic);
	add_child(&statelogichigh_alarm_, typeLogic);
	add_child(&statelogiclow_alarm_, typeLogic);
	add_child(&logic_, typeLogic);
#if GTK_VERSION_GE(2,18)
	blinklogic_.set_visible(false);
	statelogichigh_warn_.set_visible(false);
	statelogiclow_warn_.set_visible(false);
	statelogichigh_alarm_.set_visible(false);
	statelogiclow_alarm_.set_visible(false);
	logic_.set_visible(false);
#else
	blinklogic_.hide();
	statelogichigh_warn_.hide();
	statelogiclow_warn_.hide();
	statelogichigh_alarm_.hide();
	statelogiclow_alarm_.hide();
	logic_.hide();
#endif
}
// -------------------------------------------------------------------------
TypicalCisternBlink::TypicalCisternBlink() :
	Glib::ObjectBase("typicalcisternblink")
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalCisternBlink::TypicalCisternBlink(SimpleObject::BaseObjectType* gobject) :
	SimpleObject(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalCisternBlink::~TypicalCisternBlink()
{
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_rect(const Gdk::Rectangle rect)
{
	rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::create_cistern()
{
	cistern_ = new CisternImageBlink();
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCisternBlink::get_background_off_path()
{
	return background_off_path_;
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCisternBlink::get_background_warn_path()
{
	return background_warn_path_;
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCisternBlink::get_background_alarm_path()
{
	return background_alarm_path_;
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCisternBlink::get_filling_path()
{
	return filling_path_;
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCisternBlink::get_scale_path()
{
	return scale_path_;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_background_off_path(const Glib::ustring& path)
{
	background_off_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_background_warn_path(const Glib::ustring& path)
{
	background_warn_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_background_alarm_path(const Glib::ustring& path)
{
	background_alarm_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_filling_path(const Glib::ustring& path)
{
	filling_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_scale_path(const Glib::ustring& path)
{
	scale_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_high_alarm(const Glib::ustring& path)
{
	high_alarm_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_low_alarm(const Glib::ustring& path)
{
	low_alarm_path_ = path;
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCisternBlink::get_high_alarm()
{
	return high_alarm_path_;
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalCisternBlink::get_low_alarm()
{
	return low_alarm_path_;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::configure()
{
	set_property_disconnect_effect(0);//Отключаем рисование серой области.

	Gdk::Rectangle* rect = get_rect();

	cistern_->set_size_request(rect->get_width(),rect->get_height());
	cistern_->set_background_image_path(get_background_off_path());
	cistern_->set_image_warn_path(get_background_warn_path());
	cistern_->set_image_alarm_path(get_background_alarm_path());
	cistern_->set_filling_image_path(get_filling_path());
	cistern_->set_scale_image_path(get_scale_path());
	cistern_->set_scale_switch(scale_on);
	cistern_->set_high_alarm_image_path(get_high_alarm());
	cistern_->set_low_alarm_image_path(get_low_alarm());
	cistern_->set_threshold_high(level);
	cistern_->show();

	statelogichigh_warn_.set_threshold(1);
	statelogiclow_warn_.set_threshold(0);

	statelogichigh_alarm_.set_threshold(1);
	statelogiclow_alarm_.set_threshold(0);
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalCisternBlink::get_rect()
{
	return &rect_;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_scale_switch_on(bool switch_on)
{
	scale_on = switch_on;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_state_obj(const UniWidgetsTypes::ObjectId sensor)
{
	statelogichigh_warn_.set_state_obj_ai(sensor);
	statelogichigh_alarm_.set_state_obj_ai(sensor);
	statelogiclow_warn_.set_state_obj_ai(sensor);
	statelogiclow_alarm_.set_state_obj_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_value_ai(const UniWidgetsTypes::ObjectId sensor)
{
	logic_.set_value_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_node(const UniWidgetsTypes::ObjectId node)
{
	logic_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_state_di_statehigh_warn(const UniWidgetsTypes::ObjectId sensor)
{
	statelogichigh_warn_.set_state_di(sensor);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_cistern_high_level(const long val)
{
	level = val;
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_node_statehigh_warn(const UniWidgetsTypes::ObjectId node)
{
	statelogichigh_warn_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_mode_statehigh_warn(const int mode)
{
	statelogichigh_warn_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_state_di_statelow_warn(const UniWidgetsTypes::ObjectId sensor)
{
	statelogiclow_warn_.set_state_di(sensor);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_node_statelow_warn(const UniWidgetsTypes::ObjectId node)
{
	statelogiclow_warn_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_mode_statelow_warn(const int mode)
{
	statelogiclow_warn_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_state_di_statehigh_alarm(const UniWidgetsTypes::ObjectId sensor)
{
	statelogichigh_alarm_.set_state_di(sensor);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_node_statehigh_alarm(const UniWidgetsTypes::ObjectId node)
{
	statelogichigh_alarm_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_mode_statehigh_alarm(const int mode)
{
	statelogichigh_alarm_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_state_di_statelow_alarm(const UniWidgetsTypes::ObjectId sensor)
{
	statelogiclow_alarm_.set_state_di(sensor);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_node_statelow_alarm(const UniWidgetsTypes::ObjectId node)
{
	statelogiclow_alarm_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalCisternBlink::set_mode_statelow_alarm(const int mode)
{
	statelogiclow_alarm_.set_mode(mode);
}
// -------------------------------------------------------------------------
