#ifndef _TYPICALCISTERNBLINK_H
#define _TYPICALCISTERNBLINK_H
// -------------------------------------------------------------------------
#include <map>
#include <typical/AbstractTypical.h>
#include <objects/CisternLogic.h>
#include <objects/CisternStateLogic.h>
#include <objects/CisternShowLogic.h>
#include <components/CisternImageBlink.h>
#include <USignals.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой контейнер SimpleObject цистерна.
 * \par
 * Это контейнер содержащий логику и компоненты для отображения мигающей цистерны
 * с уровнем.
 */
class TypicalCisternBlink : public SimpleObject
{
public:
	TypicalCisternBlink();
	explicit TypicalCisternBlink(SimpleObject::BaseObjectType* gobject);
	virtual ~TypicalCisternBlink();

	/* Methods */
	void configure();							/*!< конфигурирование контейнера */

	void set_rect(const Gdk::Rectangle rect);				/*!< задать размеры контейнера */
	void set_value_ai(const UniWidgetsTypes::ObjectId sensor);			/*!< задать аналоговый датчик для уровня цистерны */
	void set_node(const UniWidgetsTypes::ObjectId node);			/*!< задать узел датчика */
	
	void set_state_di_statehigh_warn(const UniWidgetsTypes::ObjectId sensor);	/*!< задать id датчика верхнего порога уровня цистерны(предупреждение) */
	void set_node_statehigh_warn(const UniWidgetsTypes::ObjectId node);		/*!< задать узел датчика верхнего порога уровня цистерны(предупреждение) */
	void set_mode_statehigh_warn(const int mode);				/*!< задать режим верхнего порога датчика(предупреждение) */
	
	void set_state_di_statelow_warn(const UniWidgetsTypes::ObjectId sensor);	/*!< задать id датчика нижнего порога уровня цистерны(предупреждение) */
	void set_node_statelow_warn(const UniWidgetsTypes::ObjectId node);		/*!< задать id датчика нижнего порога уровня цистерны(предупреждение) */
	void set_mode_statelow_warn(const int mode);				/*!< задать режим нижнего порога датчика(предупреждение) */
	
	void set_state_di_statehigh_alarm(const UniWidgetsTypes::ObjectId sensor);	/*!< задать id датчика верхнего порога уровня цистерны(авария) */
	void set_node_statehigh_alarm(const UniWidgetsTypes::ObjectId node);	/*!< задать узел датчика верхнего порога уровня цистерны(авария) */
	void set_mode_statehigh_alarm(const int mode);				/*!< задать режим верхнего порога датчика(авария) */
	
	void set_state_di_statelow_alarm(const UniWidgetsTypes::ObjectId sensor);	/*!< задать id датчика нижнего порога уровня цистерны(авария) */
	void set_node_statelow_alarm(const UniWidgetsTypes::ObjectId node);		/*!< задать id датчика нижнего порога уровня цистерны(авария) */
	void set_mode_statelow_alarm(const int mode);				/*!< задать режим нижнего порога датчика(авария) */

	void set_state_obj(const UniWidgetsTypes::ObjectId sensor);			/*!< задать датчик внутреннего состояния */
	void set_cistern_high_level(const long val);				/*!< задать значение верхнего порога уровня */

	void set_background_off_path(const Glib::ustring& path);		/*!< задать картинку для фона цистерны(выключен) */
	void set_background_warn_path(const Glib::ustring& path);		/*!< задать картинку для фона цистерны(предупреждение) */
	void set_background_alarm_path(const Glib::ustring& path);		/*!< задать картинку для фона цистерны(авария) */
	void set_filling_path(const Glib::ustring& path);			/*!< задать картинку для заполнителя цистерны(выключен) */
	void set_scale_path(const Glib::ustring& path);				/*!< задать картинку для шкалы цистерны */
	void set_scale_switch_on(bool switch_on);				/*!< задать отображение шкалы цистерны */
	void set_high_alarm(const Glib::ustring& path);				/*!< задать картинку для вернего уровня заполнителя цистерны */
	void set_low_alarm(const Glib::ustring& path);				/*!< задать картинку для нижнего уровня заполнителя цистерны */

	Glib::ustring& get_background_off_path();				/*!< получить картинку для фона цистерны(выключен) */
	Glib::ustring& get_background_warn_path();				/*!< получить картинку для фона цистерны(предупреждение) */
	Glib::ustring& get_background_alarm_path();				/*!< получить картинку для фона цистерны(авария) */
	Glib::ustring& get_high_alarm();					/*!< получить картинку для вернего уровня заполнителя цистерны */
	Glib::ustring& get_low_alarm();						/*!< получить картинку для нижнего уровня заполнителя цистерны */
	Glib::ustring& get_filling_path();					/*!< получить картинку для заполнителя цистерны(выключен) */
	Glib::ustring& get_scale_path();					/*!< получить картинку для шкалы цистерны */

	Gdk::Rectangle* get_rect();						/*!< пучить размеры цистерны */

private:
	/* Variables */
	Gdk::Rectangle rect_;
	CisternImageBlink *cistern_;
	Glib::ustring background_off_path_;
	Glib::ustring background_warn_path_;
	Glib::ustring background_alarm_path_;
	Glib::ustring filling_path_;
	Glib::ustring scale_path_;
	Glib::ustring high_alarm_path_;
	Glib::ustring low_alarm_path_;
	bool scale_on;
	long level;

	/*Loigcs*/
	CisternLogic logic_;
  CisternShowLogic blinklogic_;
	CisternStateLogic statelogichigh_warn_;
	CisternStateLogic statelogichigh_alarm_;

	CisternStateLogic statelogiclow_warn_;
	CisternStateLogic statelogiclow_alarm_;

	/* Methods */
	void create_cistern();
	void constructor();

	DISALLOW_COPY_AND_ASSIGN(TypicalCisternBlink);
};
}

#endif
