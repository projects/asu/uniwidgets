#include <iostream>
#include <types.h>
#include <components/Image.h>
#include <components/ImageBlink.h>
#include "TypicalFourState.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
class ConfigureImageFourState: public binary_function<TypicalFourState::ModeImagePair, TypicalFourState*, void>
{
public:
  void operator() (TypicalFourState::ModeImagePair pair, TypicalFourState* object) const
  {
    Image* image = pair.second;
    int mode = pair.first;

    if( object->get_path(mode).empty() )
      return;

    Gdk::Rectangle* rect = object->get_rect();

    object->put(*image,0,0);

    image->set_size_request(rect->get_width(),
      rect->get_height());

    ImageBlink* imageblink;
    imageblink = dynamic_cast<ImageBlink*>(image);
    if (imageblink != NULL)
    {
      if( !object->get_path(mBACKGROUND).empty() )
        imageblink->set_image_path(object->get_path(mBACKGROUND));
      else
        imageblink->set_image_path(object->get_path(mOFF));
      imageblink->set_image2_path(object->get_path(mode));
    }
    else
    {
      image->set_image_path(object->get_path(mode));
    }

    image->set_mode(mode);
    image->set_priority(object->get_priority(mode));
    object->add_child(image, typeView);
  }
};
// -------------------------------------------------------------------------
void TypicalFourState::constructor()
{
  set_priority(mWARNING_HIGH, 5);
  set_priority(mWARNING_LOW, 5);
  set_priority(mRESERV, 2);
  set_priority(mALARM_HIGH, 6);
  set_priority(mALARM_LOW, 6);

  add_child(&statelogic_high_warn_, typeLogic);
  add_child(&statelogic_low_warn_, typeLogic);
  add_child(&statelogic_high_alarm_, typeLogic);
  add_child(&statelogic_low_alarm_, typeLogic);
  add_child(&showlogic_, typeLogic);
#if GTK_VERSION_GE(2,18)
  statelogic_high_warn_.set_visible(false);
  statelogic_low_warn_.set_visible(false);
  statelogic_high_alarm_.set_visible(false);
  statelogic_low_alarm_.set_visible(false);
  showlogic_.set_visible(false);
#else
  statelogic_high_warn_.hide();
  statelogic_low_warn_.hide();
  statelogic_high_alarm_.hide();
  statelogic_low_alarm_.hide();
  showlogic_.hide();
#endif
}
// -------------------------------------------------------------------------
TypicalFourState::TypicalFourState() :
  Glib::ObjectBase("typicalfourstate")
{
  constructor();
}
// -------------------------------------------------------------------------
TypicalFourState::TypicalFourState(AbstractTypical::BaseObjectType* gobject) :
  AbstractTypical(gobject)
{
  constructor();
}
// -------------------------------------------------------------------------
TypicalFourState::~TypicalFourState()
{
}
// -------------------------------------------------------------------------
void TypicalFourState::set_rect(const Gdk::Rectangle rect)
{
  rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalFourState::set_path(const long mode, const Glib::ustring& path)
{
  pair<ModePathMap::iterator, bool> ret;
  ret = paths_.insert(ModePathPair(mode, path));

  assert(ret.second == true);
}
// -------------------------------------------------------------------------
void TypicalFourState::insert_mode(const long mode, const long priority, const long blink)
{
  ModeImageMap::iterator it = images_.find(mode);
  if ( it != images_.end() )
  {
    delete it->second;
    it->second = ( blink ? new ImageBlink():new Image() );
  }
  else
    images_.insert(ModeImagePair(mode, blink ? new ImageBlink():new Image()));
  if(priority != -1)
    set_priority(mode, priority);

  assert( !images_.empty() );
}
// -------------------------------------------------------------------------
void TypicalFourState::configure()
{
  set_property_disconnect_effect(0);//Отключаем рисование серой области.
  /* TODO: Use TypicalFourState non static method or make
   * the ConfigureImage functor friend of TypicalFourState class  */
  for_each(images_.begin(),
      images_.end(),
      bind2nd(ConfigureImageFourState(), this));
}
// -------------------------------------------------------------------------
void TypicalFourState::set_lock_view(const bool lock)
{
  statelogic_high_warn_.set_lock_view(lock);
  statelogic_low_warn_.set_lock_view(lock);
  statelogic_high_alarm_.set_lock_view(lock);
  statelogic_low_alarm_.set_lock_view(lock);
}
// -------------------------------------------------------------------------
Glib::ustring TypicalFourState::get_path(const long mode)
{
  ModePathMap::iterator it = paths_.find(mode);

  if ( it != paths_.end() )
    return it->second;
  Glib::ustring found;
  return found;
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalFourState::get_rect()
{
  return &rect_;
}
// -------------------------------------------------------------------------
void TypicalFourState::set_state_obj(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_high_warn_.set_state_obj_ai(sensor);
  statelogic_high_alarm_.set_state_obj_ai(sensor);
  statelogic_low_warn_.set_state_obj_ai(sensor);
  statelogic_low_alarm_.set_state_obj_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_state_ai_high_warn(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_high_warn_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_node_high_warn(const UniWidgetsTypes::ObjectId node)
{
  statelogic_high_warn_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_mode_state_high_warn(const long mode)
{
  statelogic_high_warn_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_state_ai_low_warn(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_low_warn_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_node_low_warn(const UniWidgetsTypes::ObjectId node)
{
  statelogic_low_warn_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_mode_state_low_warn(const long mode)
{
  statelogic_low_warn_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_state_ai_high_alarm(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_high_alarm_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_node_high_alarm(const UniWidgetsTypes::ObjectId node)
{
  statelogic_high_alarm_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_mode_state_high_alarm(const long mode)
{
  statelogic_high_alarm_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_state_ai_low_alarm(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_low_alarm_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_node_low_alarm(const UniWidgetsTypes::ObjectId node)
{
  statelogic_low_alarm_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_mode_state_low_alarm(const long mode)
{
  statelogic_low_alarm_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_state_blink_low(bool state)
{
  /*Для индикатора в ГДГ. Сигнал о превышеннии нижнего
  порога не должен мигать*/
  statelogic_low_alarm_.set_blinking(state);
  statelogic_low_warn_.set_blinking(state);
}
// -------------------------------------------------------------------------
void TypicalFourState::set_state_blink_high(bool state)
{
  statelogic_high_alarm_.set_blinking(state);
  statelogic_high_warn_.set_blinking(state);
}
// -------------------------------------------------------------------------
