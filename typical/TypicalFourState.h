#ifndef _TYPICALFOURSTATE_H
#define _TYPICALFOURSTATE_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <global_macros.h>
#include <objects/StateLogic.h>
#include <objects/ShowLogic.h>
#include <typical/AbstractTypical.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
 class Image;
/*!
 * \brief Типовой контейнер SimpleObject сенсор с четырьмя логиками состояния.
 * \par
 * Это контейнер содержит четыре логики состояния датчика, логику отображения картинок
 * и, собственно, сами картинки. Контейнер применяется когда нужно на одном виджете
 * разместить от 3-4 различных состояния от различных датчиков.
 * Например, рамка индикатора с 4мя разными порогами(два верхних и два нижних).
 * При этом получается, что две пары режимов совпадают по типу и нужно их различать.
 * Для этого ввели новые типы: mWARNING_HIGH,mWARNING_LOW,mALARM_HIGH,mALARM_LOW,
 * которые по сути попарно одикаковые, но различаются типом.
 */
class TypicalFourState : public AbstractTypical
{
public:
  TypicalFourState();
  explicit TypicalFourState(AbstractTypical::BaseObjectType* gobject);
  virtual ~TypicalFourState();

  /* FIXME: Move this types to private or into include/types.h */
  /* Types */
  typedef std::unordered_map<long, Image*> ModeImageMap;        /*!< тип stl контейнера для хранения картинок для режимов */
  typedef std::pair<long, Image*> ModeImagePair;        /*!< тип элемента для stl контейнера для хранения объектов Image */
  typedef std::unordered_map<long, Glib::ustring > ModePathMap;      /*!< тип stl контейнера для хранения картинок для режимов */
  typedef std::pair<long, Glib::ustring > ModePathPair;      /*!< тип элемента для stl контейнера для хранения картинок */

  /* Methods */
  void configure();              /*!< конфигурирование контейнера */

  void set_rect(const Gdk::Rectangle rect);        /*!< задать размеры контейнера */
  void set_path(const long mode, const Glib::ustring& path);    /*!< задать путь для картинки определенного режима */
  void set_state_ai_high_warn(const UniWidgetsTypes::ObjectId sensor);  /*!< задать id датчик для логики(mWARNING_HIGH) */
  void set_node_high_warn(const UniWidgetsTypes::ObjectId node);    /*!< задать id узла для логики(mWARNING_HIGH) */
  void set_mode_state_high_warn(const long mode);        /*!< задать режим для логики(mWARNING_HIGH) */
  void set_state_ai_low_warn(const UniWidgetsTypes::ObjectId sensor);    /*!< задать id датчик для логики(mWARNING_LOW) */
  void set_node_low_warn(const UniWidgetsTypes::ObjectId node);    /*!< задать id узла для логики(mWARNING_LOW) */
  void set_mode_state_low_warn(const long mode);        /*!< задать режим для логики(mWARNING_LOW) */

  void set_state_obj(const UniWidgetsTypes::ObjectId sensor);      /*!< задать id датчик внутреннего состояния */

  void set_state_ai_high_alarm(const UniWidgetsTypes::ObjectId sensor);  /*!< задать id датчик для логики(mALARM_HIGH) */
  void set_node_high_alarm(const UniWidgetsTypes::ObjectId node);    /*!< задать id узла для логики(mALARM_HIGH) */
  void set_mode_state_high_alarm(const long mode);      /*!< задать режим для логики(mALARM_HIGH) */
  void set_state_ai_low_alarm(const UniWidgetsTypes::ObjectId sensor);  /*!< задать id датчик для логики(mALARM_LOW) */
  void set_node_low_alarm(const UniWidgetsTypes::ObjectId node);    /*!< задать id узла для логики(mALARM_LOW) */
  void set_mode_state_low_alarm(const long mode);        /*!< задать режим для логики(mALARM_LOW) */
  void set_state_blink_low(bool state);          /*!< задать мигание нижним порогом(нужно для некоторых индикаторов, когда верхний уровень АПС и должени мигать при срабатывании, а нижний нет)*/
  void set_state_blink_high(bool state);
  virtual void set_lock_view(const bool lock);        /*!< задать блокировку экрана при срабатывании АПС */
  void insert_mode(const long mode, const long priority, const long blink);/*!< добавить режим с параметрами */

  Glib::ustring get_path(const long mode);        /*!< получить картинку для нужного режима */
  
  Gdk::Rectangle* get_rect();            /*!< получить размеры виджета */

private:
  /* Variables */
  Gdk::Rectangle rect_;
  ShowLogic showlogic_;
  StateLogic statelogic_high_warn_;
  StateLogic statelogic_low_warn_;
  StateLogic statelogic_high_alarm_;
  StateLogic statelogic_low_alarm_;
  ModeImageMap images_;
  ModePathMap paths_;

  /* Methods */
  void constructor();

  DISALLOW_COPY_AND_ASSIGN(TypicalFourState);
};

}
#endif
