#include <iostream>
#include <types.h>
#include <typical/TypicalGDGControl.h>
#include <components/ImageBlink.h>
#include <components/Text.h>
#include <objects/ShowLogic.h>
// #include "StateMultiLogic.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
class ConfigureGDGImageControl: public binary_function<TypicalGDGControl::ModeImagePair, TypicalGDGControl*, void>
{
public:
	void operator() (TypicalGDGControl::ModeImagePair pair, TypicalGDGControl* object) const
	{
		Image* image = pair.second;
		int mode = pair.first;
		Gdk::Rectangle* rect = object->get_rect();

		object->put(*image,0,0);
	
		image->set_size_request(rect->get_width(),
			rect->get_height());

		ImageBlink* imageblink;
		imageblink = dynamic_cast<ImageBlink*>(image);

		if (imageblink != NULL)
		{
			if(!object->get_path(mBACKGROUND).empty())
			{
				imageblink->set_image_path(object->get_path(mBACKGROUND));
				imageblink->set_image2_path(object->get_path(mode));
			}
			else
			{
				imageblink->set_image_path(object->get_path(mOFF));
				imageblink->set_image2_path(object->get_path(mode));
			}
		}
		else
		{
			image->set_image_path(object->get_path(mode));
		}

		image->set_mode(mode);
		image->set_priority(object->get_priority(mode));

    object->add_child(image, typeView);
		
	}
};
// -------------------------------------------------------------------------
void TypicalGDGControl::constructor()
{
	create_images();
	x_text_=0;
	y_text_=0;
	// Важен порядок вызова add_child(), логика должа быть последней
	text_ = new Text();
	text_->set_property_text_("0");
  add_child(text_, typeText);
	add_child(&statemultilogic_, typeLogic);
	add_child(&showlogic_, typeLogic);
#if GTK_VERSION_GE(2,18)
	statemultilogic_.set_visible(false);
	showlogic_.set_visible(false);
#else
	statemultilogic_.hide();
	showlogic_.hide();
#endif
}
// -------------------------------------------------------------------------
TypicalGDGControl::TypicalGDGControl() :
	Glib::ObjectBase("typicalgdgcontrol")
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalGDGControl::TypicalGDGControl(AbstractTypical::BaseObjectType* gobject) :
	AbstractTypical(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalGDGControl::~TypicalGDGControl()
{
	delete text_;
}
// -------------------------------------------------------------------------
void TypicalGDGControl::set_rect(const Gdk::Rectangle rect)
{
	rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalGDGControl::set_path(const long mode, const Glib::ustring& path)
{
	pair<ModePathMap::iterator, bool> ret;
	ret = paths_.insert(ModePathPair(mode, path));

	assert(ret.second == true);
}
// -------------------------------------------------------------------------
void TypicalGDGControl::create_images()
{
	images_.insert(ModeImagePair(mINDEF, new Image()));
	images_.insert(ModeImagePair(mHAND, new Image()));
	images_.insert(ModeImagePair(mCPU, new Image()));
	images_.insert(ModeImagePair(mAUTO, new Image()));

	assert( !images_.empty() );
}
// -------------------------------------------------------------------------
void TypicalGDGControl::configure()
{
	set_property_disconnect_effect(0);//Отключаем рисование серой области.
	/* TODO: Use TypicalGDGControl non static method or make
	 * the ConfigureImage functor friend of TypicalGDGControl class  */
	for_each(images_.begin(),
			images_.end(),
			bind2nd(ConfigureGDGImageControl(), this));
	/*Удаляем и вставляем заново text для правильного отображения инициализации.
	increase_child нельзя вызывать пока не создаться виджет
	, иначе неправильно обрабатывабтся координаты*/
	if(text_->is_visible())
		put(*text_,x_text_,y_text_);
	else
	{
#if GTK_VERSION_GE(2,18)
	text_->set_visible(true);
	put(*text_,x_text_,y_text_);
	text_->set_visible(false);
#else
	text_->show();
	put(*text_,x_text_,y_text_);
	text_->hide();
#endif
	}
}
// -------------------------------------------------------------------------
void TypicalGDGControl::set_lock_view(const bool lock)
{
	statemultilogic_.set_lock_view(lock);
}
// -------------------------------------------------------------------------
Glib::ustring TypicalGDGControl::get_path(const long mode)
{
	ModePathMap::iterator it;

	it = paths_.find(mode);

	if ( it != paths_.end() )
		return it->second;
	return Glib::ustring("");
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalGDGControl::get_rect()
{
	return &rect_;
}
// -------------------------------------------------------------------------
void TypicalGDGControl::set_state_ai(const UniWidgetsTypes::ObjectId sensor)
{
	statemultilogic_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalGDGControl::set_state_obj(const UniWidgetsTypes::ObjectId sensor)
{
	statemultilogic_.set_state_obj_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalGDGControl::set_node(const UniWidgetsTypes::ObjectId node)
{
	statemultilogic_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalGDGControl::set_mode_state(const long mode)
{
	statemultilogic_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalGDGControl::set_text_property(const long x, const long y, const long keg)
{
	text_->set_property_abs_font_size_(keg);
	x_text_ = x;
	y_text_ = y;
}
// -------------------------------------------------------------------------
void TypicalGDGControl::set_text_color(Gdk::Color color)
{
	text_->set_property_font_color_(color);
}
// -------------------------------------------------------------------------
