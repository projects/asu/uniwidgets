#ifndef _TYPICALGDGCONTROL_H
#define _TYPICALGDGCONTROL_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <typical/AbstractTypical.h>
#include <USignals.h>
#include <global_macros.h>
#include <objects/StateMultiLogic.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class Image;
class Text;
class ShowLogic;
// class StateMultiLogic;
/*!
 * \brief Типовой контейнер SimpleObject сенсор с текстом.
 * \par
 * Это контейнер предназначен для отображения картинки состояния виджета с
 * текстом содержащим значение датчика.
 */
class TypicalGDGControl : public AbstractTypical
{
public:
	TypicalGDGControl();
	explicit TypicalGDGControl(AbstractTypical::BaseObjectType* gobject);
	virtual ~TypicalGDGControl();

	/* FIXME: Move this types to private or into include/types.h */
	/* Types */
	typedef std::unordered_map<long, Image*> ModeImageMap;				/*!< тип stl контейнера для хранения картинок для режимов */
	typedef std::pair<long, Image*> ModeImagePair;				/*!< тип элемента для stl контейнера для хранения объектов Image */
	typedef std::unordered_map<long, Glib::ustring > ModePathMap;			/*!< тип stl контейнера для хранения картинок для режимов */
	typedef std::pair<long, Glib::ustring > ModePathPair;			/*!< тип элемента для stl контейнера для хранения картинок */

	/* Methods */
	void configure();							/*!< конфигурирование контейнера */

	void set_rect(const Gdk::Rectangle rect);				/*!< задать размеры контейнера */
	void set_path(const long mode, const Glib::ustring& path);		/*!< задать путь для картинки определенного режима */
	void set_state_ai(const UniWidgetsTypes::ObjectId sensor);			/*!< задать id датчика */
	void set_state_obj(const UniWidgetsTypes::ObjectId sensor);			/*!< задать id датчик внутреннего состояния */
	void set_node(const UniWidgetsTypes::ObjectId node);			/*!< задать id узла */
	void set_mode_state(const long mode);					/*!< задать режим для картинки */
	void set_text_property(const long x, const long y, const long keg);	/*!< задать свойства для текста(координаты и размер шрифта) */
	void set_text_color(Gdk::Color color);					/*!< задать цвет для текста */
	virtual void set_lock_view(const bool lock);				/*!< задать блокировку экрана при срабатывании АПС */



	Glib::ustring get_path(const long mode);				/*!< получить картинку для нужного режима */

	Gdk::Rectangle* get_rect();						/*!< получить размеры виджета */

private:
	/* Variables */
	Gdk::Rectangle rect_;
	Text *text_;
	ShowLogic showlogic_;
	StateMultiLogic statemultilogic_;
	ModeImageMap images_;
	ModePathMap paths_;
	long x_text_;
	long y_text_;

	/* Methods */
	void constructor();
	void create_images();

	DISALLOW_COPY_AND_ASSIGN(TypicalGDGControl);
};

}
#endif
