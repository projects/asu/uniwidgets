#include <iostream>
#include <components/ImageBlink.h>
#include <types.h>
#include "TypicalImitatorLamp.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
class ConfigureImage: public binary_function<TypicalImitatorLamp::ModeImagePair, TypicalImitatorLamp*, void>
{
public:
	void operator() (TypicalImitatorLamp::ModeImagePair pair, TypicalImitatorLamp* object) const
	{
		Image* image = pair.second;
		int mode = pair.first;
		if( object->get_path(mode).empty() )
			return;

		Gdk::Rectangle* rect = object->get_rect();

		object->put(*image,0,0);

		image->set_size_request(rect->get_width(),
			rect->get_height());

		ImageBlink* imageblink;
		imageblink = dynamic_cast<ImageBlink*>(image);
		
		if (imageblink != NULL)
		{
			imageblink->set_blinktime(object->get_blinking_time(mode));

			if( !object->get_path(mBACKGROUND).empty() )
			{
				imageblink->set_image_path(object->get_path(mBACKGROUND));
				imageblink->set_image2_path(object->get_path(mode));
			}
			else
			{
				if( !object->get_path(mode,true).empty() )
				{
					imageblink->set_image_path(object->get_path(mode,true));
					imageblink->set_image2_path(object->get_path(mode));
				}
				else
				{
					imageblink->set_image_path(object->get_path(mOFF));
					imageblink->set_image2_path(object->get_path(mode));
				}
			}
		}
		else
		{
			image->set_image_path(object->get_path(mode));
		}

		image->set_mode(mode);
		image->set_priority(object->get_priority(mode));

    object->add_child(image, typeView);
	}
};
// -------------------------------------------------------------------------
void TypicalImitatorLamp::constructor()
{
	create_images();
	add_child(&imitatorlogic_, typeLogic);
	add_child(&imitatorshowlogic_, typeLogic);
#if GTK_VERSION_GE(2,18)
	imitatorlogic_.set_visible(false);
	imitatorshowlogic_.set_visible(false);
#else
	imitatorlogic_.hide();
	imitatorshowlogic_.hide();
#endif
}
// -------------------------------------------------------------------------
TypicalImitatorLamp::TypicalImitatorLamp() :
	Glib::ObjectBase("typicalimitatorlamp")
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalImitatorLamp::TypicalImitatorLamp(AbstractTypical::BaseObjectType* gobject) :
	AbstractTypical(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalImitatorLamp::~TypicalImitatorLamp()
{
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::set_rect(const Gdk::Rectangle rect)
{
	rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::set_path(const long mode, const Glib::ustring& path)
{
	pair<ModePathMap::iterator, bool> ret;
	ret = paths_.insert(ModePathPair(mode, path));

	assert(ret.second == true);
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::set_path2(const long mode, const Glib::ustring& path)
{
	pair<ModePathMap::iterator, bool> ret;
	ret = paths_2.insert(ModePathPair(mode, path));

	assert(ret.second == true);
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::create_images()
{
	images_.insert(ModeImagePair(lmpOFF, new Image()));
	images_.insert(ModeImagePair(lmpON, new Image()));
	images_.insert(ModeImagePair(lmpBLINK, new ImageBlink()));
	images_.insert(ModeImagePair(lmpBLINK2, new ImageBlink()));
	images_.insert(ModeImagePair(lmpBLINK3, new ImageBlink()));
	assert( !images_.empty() );
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::configure()
{
	set_property_disconnect_effect(0);//Отключаем рисование серой области.
	/* TODO: Use TypicalImitatorLamp non static method or make
	 * the ConfigureImage functor friend of TypicalImitatorLamp class  */
	
	for_each(images_.begin(),
			images_.end(),
			bind2nd(ConfigureImage(), this));
}
// -------------------------------------------------------------------------
Glib::ustring TypicalImitatorLamp::get_path(const long mode, bool is_back)
{
	ModePathMap::iterator it;

	if( !is_back )
	{
		it = paths_.find(mode);
		if( it != paths_.end() )
 			return it->second;
	}
	else
	{
		it = paths_2.find(mode);
		if( it != paths_2.end() )
			return it->second;
	}
	Glib::ustring found;
	return found;
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalImitatorLamp::get_rect()
{
	return &rect_;
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::set_state_ai(const UniWidgetsTypes::ObjectId sensor)
{
	imitatorlogic_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::set_state_obj(const UniWidgetsTypes::ObjectId sensor)
{
	imitatorlogic_.set_state_obj_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::set_node(const UniWidgetsTypes::ObjectId node)
{
	imitatorlogic_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::set_mode_state(const long mode)
{
	imitatorlogic_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalImitatorLamp::set_blinking_time(const long mode, const int sec)
{
	blinktimes_.insert(BlinkTimePair(mode, sec));

}
// -------------------------------------------------------------------------
int TypicalImitatorLamp::get_blinking_time(const long mode)
{
	int found;
	BlinkTimeMap::iterator it;

	it = blinktimes_.find(mode);
	if( it != blinktimes_.end() )
 		found = it->second;
	else
		found = DEFAULT_BLINK_TIME;

	return found;
}
// -------------------------------------------------------------------------
