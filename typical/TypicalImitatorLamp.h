#ifndef _TYPICALIMITATORLAMP_H
#define _TYPICALIMITATORLAMP_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <typical/AbstractTypical.h>
#include <objects/ImitatorLogic.h>
#include <objects/ImitatorShowLogic.h>
#include <components/Image.h>
#include <USignals.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой контейнер SimpleObject лампочка.
 * \par
 * Типовой контейнер представляет собой лампочку на имитаторе панелей с логикой
 * ImitatorLogic и картинками для состояний:
 *					    lmpOFF
 *					    lmpON
 *					    lmpBLINK
 *					    lmpBLINK2
 *					    lmpBLINK3
 */
class TypicalImitatorLamp : public AbstractTypical
{
public:
	TypicalImitatorLamp();
	explicit TypicalImitatorLamp(AbstractTypical::BaseObjectType* gobject);
	virtual ~TypicalImitatorLamp();

	/* Types */
	typedef std::unordered_map<long, Image*> ModeImageMap;
	typedef std::pair<long, Image*> ModeImagePair;
	typedef std::unordered_map<long, Glib::ustring > ModePathMap;
	typedef std::pair<long, Glib::ustring > ModePathPair;
	typedef std::unordered_map<long, int> BlinkTimeMap;
	typedef std::pair<long, int> BlinkTimePair;

	/* Methods */
	void configure();

	void set_rect(const Gdk::Rectangle rect);
	void set_path(const long mode, const Glib::ustring& path);
	void set_path2(const long mode, const Glib::ustring& path);
	void set_state_ai(const UniWidgetsTypes::ObjectId sensor);
	void set_state_obj(const UniWidgetsTypes::ObjectId sensor);
	void set_node(const UniWidgetsTypes::ObjectId node);
	void set_mode_state(const long mode);
	void set_blinking_time(const long mode, const int sec);			/*!< задать частоту мигания лампочки */

	Glib::ustring get_path(const long, bool is_back = false);
	Gdk::Rectangle* get_rect();
	int get_blinking_time(const long mode);					/*!< получить частоту мигания лампочки */

private:
	/* Variables */
	Gdk::Rectangle rect_;
	ImitatorShowLogic imitatorshowlogic_;
	ImitatorLogic imitatorlogic_;
	ModeImageMap images_;
	ModePathMap paths_; 		/*Пути для картинок*/
	ModePathMap paths_2; 		/*Пути для картинок "подложек" основного состояния*/
	BlinkTimeMap blinktimes_;	/* Тут содержаться тайминги мигания для двух состояний лампочек lmpBLINK2 и lmpBLINK3 */
	
	int blinktimesec1;
	int blinktimesec2;

	/* Methods */
	void create_images();
	void constructor();

	DISALLOW_COPY_AND_ASSIGN(TypicalImitatorLamp);
};

}
#endif
