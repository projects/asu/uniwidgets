#include <iostream>
#include <types.h>
#include "TypicalIndicator.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
TypicalIndicator::TypicalIndicator() :
	Glib::ObjectBase("typicalindicator")
{
}
// -------------------------------------------------------------------------
TypicalIndicator::TypicalIndicator(SimpleObject::BaseObjectType* gobject) :
	SimpleObject(gobject)
{
}
// -------------------------------------------------------------------------
TypicalIndicator::~TypicalIndicator()
{
}
// -------------------------------------------------------------------------
void TypicalIndicator::set_rect(const Gdk::Rectangle rect)
{
	rect_ = rect;
	indicator_rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalIndicator::set_indicator_rect(const Gdk::Rectangle rect)
{
	indicator_rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalIndicator::set_image_path(const Glib::ustring& path)
{
	image_path_ = path;
}
// -------------------------------------------------------------------------
void TypicalIndicator::set_precision(int precision)
{
	precision_ = precision;
}
// -------------------------------------------------------------------------
void TypicalIndicator::set_digits(int digits)
{
	digits_ = digits;
}
// -------------------------------------------------------------------------
void TypicalIndicator::create_indicator()
{
	indicator_ = new Text();
}
// -------------------------------------------------------------------------
void TypicalIndicator::configure()
{
	set_property_disconnect_effect(0);//Отключаем рисование серой области.

	create_indicator();

	Gdk::Rectangle* rect = get_rect();
	Gdk::Rectangle* indicator_rect = get_indicator_rect();

	put(image_,0,0);
	image_.set_size_request(rect->get_width(),rect->get_height());
	image_.set_image_path(get_image_path());
	image_.show();

	put(*indicator_,indicator_rect->get_x() - rect->get_x(),indicator_rect->get_y() - rect->get_y());
	indicator_->set_size_request(indicator_rect->get_width(),indicator_rect->get_height());
	indicator_->set_property_abs_font_size_(14);
	indicator_->set_property_font_name_("Liberation Serif Bold");
	indicator_->set_property_font_color_(Gdk::Color("green"));
	indicator_->show();

  logic_.set_property_digital_mode_(true);
  logic_.set_property_digital_precision_(get_precision());
  logic_.set_property_digital_length_(get_digits());


  add_child(&image_, typeView);
  add_child(indicator_, typeIndicator);
	add_child(&logic_, typeLogic);
#if GTK_VERSION_GE(2,18)
	logic_.set_visible(false);
#else
	logic_.hide();
#endif
}
// -------------------------------------------------------------------------
Glib::ustring& TypicalIndicator::get_image_path()
{
	return image_path_;
}
// -------------------------------------------------------------------------
int TypicalIndicator::get_precision()
{
	return precision_;
}
// -------------------------------------------------------------------------
int TypicalIndicator::get_digits()
{
	return digits_;
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalIndicator::get_rect()
{
	return &rect_;
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalIndicator::get_indicator_rect()
{
	return &indicator_rect_;
}
// -------------------------------------------------------------------------
void TypicalIndicator::set_value_ai(const UniWidgetsTypes::ObjectId sensor)
{
	logic_.set_value_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalIndicator::set_node(const UniWidgetsTypes::ObjectId node)
{
	logic_.set_node(node);
}
// -------------------------------------------------------------------------
