#ifndef _TYPICALINDICATOR_H
#define _TYPICALINDICATOR_H
// -------------------------------------------------------------------------
#include <map>
#include <typical/AbstractTypical.h>
#include <objects/IndicatorLogic.h>
#include <components/Text.h>
#include <components/Image.h>
#include <USignals.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой контейнер SimpleObject индикатор.
 * \par
 * Типовой контейнер представляет собой индикатор с рамкой.
 */
class TypicalIndicator : public SimpleObject
{
public:
	TypicalIndicator();
	explicit TypicalIndicator(SimpleObject::BaseObjectType* gobject);
	virtual ~TypicalIndicator();

	/* Methods */
	void configure();

	void set_rect(const Gdk::Rectangle rect);
	void set_indicator_rect(const Gdk::Rectangle rect);		/*!< задать координаты для индикатора */
	void set_image_path(const Glib::ustring& path);			/*!< задать картинку для рамки */
	void set_precision(int precision);
	void set_digits(int digits);
	void set_value_ai(const UniWidgetsTypes::ObjectId sensor);
	void set_node(const UniWidgetsTypes::ObjectId node);

	Glib::ustring& get_image_path();				/*!< получить картинку для рамки */
	int get_precision();
	int get_digits();
	Gdk::Rectangle* get_rect();
	Gdk::Rectangle* get_indicator_rect();

private:
	/* Variables */
	Gdk::Rectangle rect_;
	Gdk::Rectangle indicator_rect_;
	IndicatorLogic logic_;
	Text *indicator_;
	Image image_;
	Glib::ustring image_path_;
	int precision_;
	int digits_;

	/* Methods */
	void create_indicator();

	DISALLOW_COPY_AND_ASSIGN(TypicalIndicator);
};

}
#endif
