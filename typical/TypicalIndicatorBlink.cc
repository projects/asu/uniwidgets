#include <iostream>
#include <types.h>
#include "TypicalIndicatorBlink.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::constructor()
{
  create_indicator();

  Gdk::Rectangle* rect = get_rect();
  indicator_->set_property_abs_font_size_(14);
  indicator_->set_property_font_name_("Liberation Serif Bold");
  put(*indicator_,0,0);
  indicator_->show();

  add_child(indicator_, typeIndicator);
  add_child(&blinklogic_, typeLogic);
  add_child(&colorlogichigh_warn_, typeLogic);
  add_child(&colorlogiclow_warn_, typeLogic);
  add_child(&colorlogichigh_alarm_, typeLogic);
  add_child(&colorlogiclow_alarm_, typeLogic);
  add_child(&logic_, typeLogic);
#if GTK_VERSION_GE(2,18)
  blinklogic_.set_visible(false);
  colorlogichigh_warn_.set_visible(false);
  colorlogiclow_warn_.set_visible(false);
  colorlogichigh_alarm_.set_visible(false);
  colorlogiclow_alarm_.set_visible(false);
  logic_.set_visible(false);
#else
  blinklogic_.hide();
  colorlogichigh_warn_.hide();
  colorlogiclow_warn_.hide();
  colorlogichigh_alarm_.hide();
  colorlogiclow_alarm_.hide();
  logic_.hide();
#endif
}
// -------------------------------------------------------------------------
TypicalIndicatorBlink::TypicalIndicatorBlink() :
  Glib::ObjectBase("typicalindicatorblink")
{
  constructor();
}
// -------------------------------------------------------------------------
TypicalIndicatorBlink::TypicalIndicatorBlink(SimpleObject::BaseObjectType* gobject) :
  SimpleObject(gobject)
{
  constructor();
}
// -------------------------------------------------------------------------
TypicalIndicatorBlink::~TypicalIndicatorBlink()
{
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_rect(const Gdk::Rectangle rect)
{
  rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_precision(int precision)
{
  precision_ = precision;
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_digits(int digits)
{
  digits_ = digits;
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_factor(double factor)
{
  factor_ = factor;
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::create_indicator()
{
  indicator_ = new TextBlink();
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::configure()
{
  set_property_disconnect_effect(0);//Отключаем рисование серой области.

  Gdk::Rectangle* rect = get_rect();
  indicator_->set_size_request(rect->get_width(),rect->get_height());

  logic_.set_property_digital_mode_(true);
  logic_.set_property_digital_precision_(get_precision());
  logic_.set_property_digital_length_(get_digits());
  logic_.set_property_digital_factor_(get_factor());
  
  colorlogichigh_warn_.set_threshold(1);
  colorlogiclow_warn_.set_threshold(0);

  colorlogichigh_alarm_.set_threshold(1);
  colorlogiclow_alarm_.set_threshold(0);
}
// -------------------------------------------------------------------------
int TypicalIndicatorBlink::get_precision()
{
  return precision_;
}
// -------------------------------------------------------------------------
int TypicalIndicatorBlink::get_digits()
{
  return digits_;
}
// -------------------------------------------------------------------------
double TypicalIndicatorBlink::get_factor()
{
  return factor_;
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalIndicatorBlink::get_rect()
{
  return &rect_;
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_state_obj(const UniWidgetsTypes::ObjectId sensor)
{
  colorlogichigh_warn_.set_state_obj_ai(sensor);
  colorlogichigh_alarm_.set_state_obj_ai(sensor);
  colorlogiclow_warn_.set_state_obj_ai(sensor);
  colorlogiclow_alarm_.set_state_obj_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_value_ai(const UniWidgetsTypes::ObjectId sensor)
{
  logic_.set_value_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_node(const UniWidgetsTypes::ObjectId node)
{
  logic_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_state_di_colorhigh_warn(const UniWidgetsTypes::ObjectId sensor)
{
  colorlogichigh_warn_.set_state_di(sensor);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_node_colorhigh_warn(const UniWidgetsTypes::ObjectId node)
{
  colorlogichigh_warn_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_mode_colorhigh_warn(const int mode)
{
  colorlogichigh_warn_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_state_di_colorlow_warn(const UniWidgetsTypes::ObjectId sensor)
{
  colorlogiclow_warn_.set_state_di(sensor);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_node_colorlow_warn(const UniWidgetsTypes::ObjectId node)
{
  colorlogiclow_warn_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_mode_colorlow_warn(const int mode)
{
  colorlogiclow_warn_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_state_di_colorhigh_alarm(const UniWidgetsTypes::ObjectId sensor)
{
  colorlogichigh_alarm_.set_state_di(sensor);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_node_colorhigh_alarm(const UniWidgetsTypes::ObjectId node)
{
  colorlogichigh_alarm_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_mode_colorhigh_alarm(const int mode)
{
  colorlogichigh_alarm_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_state_di_colorlow_alarm(const UniWidgetsTypes::ObjectId sensor)
{
  colorlogiclow_alarm_.set_state_di(sensor);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_node_colorlow_alarm(const UniWidgetsTypes::ObjectId node)
{
  colorlogiclow_alarm_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_mode_colorlow_alarm(const int mode)
{
  colorlogiclow_alarm_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::set_state_blink_low(bool state)
{
  colorlogiclow_warn_.set_blinking(state);
  colorlogiclow_alarm_.set_blinking(state);
}
// -------------------------------------------------------------------------
void TypicalIndicatorBlink::addStateColor(const long mode,const Gdk::Color color,bool force)
{
  blinklogic_.addStateColor(mode,color,force);
}
// -------------------------------------------------------------------------
