#ifndef _TYPICALINDICATORBLINK_H
#define _TYPICALINDICATORBLINK_H
// -------------------------------------------------------------------------
#include <map>
#include <typical/AbstractTypical.h>
#include <objects/IndicatorLogic.h>
#include <objects/IndicatorStateLogic.h>
#include <objects/IndicatorShowLogic.h>
#include <components/TextBlink.h>
#include <USignals.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой контейнер SimpleObject индикатор с четырьмя порогами.
 * \par
 * Типовой контейнер представляет собой индикатор(без рамки, просто число) с
 * возможностью отображения до четырех порогов одновременно.
 *
 */
class TypicalIndicatorBlink : public SimpleObject
{
public:
  TypicalIndicatorBlink();
  explicit TypicalIndicatorBlink(SimpleObject::BaseObjectType* gobject);
  virtual ~TypicalIndicatorBlink();

  /* Methods */
  void configure();

  void set_rect(const Gdk::Rectangle rect);
  void set_precision(int precision);          /*!< задать точность выводимого числа после запятой */
  void set_digits(int digits);            /*!< задать максимальное количество выводимых цифр */
  void set_factor(double factor);            /*!< задать поправочный коэффициент */
  void set_value_ai(const UniWidgetsTypes::ObjectId sensor);
  void set_node(const UniWidgetsTypes::ObjectId node);
  
  void set_state_di_colorhigh_warn(const UniWidgetsTypes::ObjectId sensor);  /*!< задать id датчика для верхнего уровня(предупреждение) */
  void set_node_colorhigh_warn(const UniWidgetsTypes::ObjectId node);    /*!< задать id узла для верхнего уровня(предупреждение) */
  void set_mode_colorhigh_warn(const int mode);        /*!< задать режим для верхнего уровня(предупреждение) */
  
  void set_state_di_colorlow_warn(const UniWidgetsTypes::ObjectId sensor);
  void set_node_colorlow_warn(const UniWidgetsTypes::ObjectId node);
  void set_mode_colorlow_warn(const int mode);
  
  void set_state_di_colorhigh_alarm(const UniWidgetsTypes::ObjectId sensor);
  void set_node_colorhigh_alarm(const UniWidgetsTypes::ObjectId node);
  void set_mode_colorhigh_alarm(const int mode);
  
  void set_state_di_colorlow_alarm(const UniWidgetsTypes::ObjectId sensor);
  void set_node_colorlow_alarm(const UniWidgetsTypes::ObjectId node);
  void set_mode_colorlow_alarm(const int mode);
  void addStateColor(const long mode,const Gdk::Color color,bool force = false);

  void set_state_blink_low(bool state);

  void set_state_obj(const UniWidgetsTypes::ObjectId sensor);

  int get_precision();
  int get_digits();
  double get_factor();
  Gdk::Rectangle* get_rect();
  inline TextBlink* get_indicator(){return indicator_;};

private:
  /* Variables */
  Gdk::Rectangle rect_;
  TextBlink *indicator_;
  
  /*Loigcs*/
  IndicatorLogic logic_;
  IndicatorShowLogic blinklogic_;
  IndicatorStateLogic colorlogichigh_warn_;
  IndicatorStateLogic colorlogichigh_alarm_;

  IndicatorStateLogic colorlogiclow_warn_;
  IndicatorStateLogic colorlogiclow_alarm_;

  int precision_;
  int digits_;
  double factor_;

  /* Methods */
  void create_indicator();
  void constructor();

  DISALLOW_COPY_AND_ASSIGN(TypicalIndicatorBlink);
};

}
#endif
