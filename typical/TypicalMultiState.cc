#include <iostream>
#include <types.h>
#include <components/ImageBlink.h>
#include <components/Image.h>
#include "TypicalMultiState.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
class ConfigureGDGImageFourState: public binary_function<TypicalMultiState::ModeImagePair, TypicalMultiState*, void>
{
public:
	void operator() (TypicalMultiState::ModeImagePair pair, TypicalMultiState* object) const
	{
		Image* image = pair.second;
		int mode = pair.first;

		if( object->get_path(mode).empty() )
			return;

		Gdk::Rectangle* rect = object->get_rect();

		object->put(*image,0,0);

		image->set_size_request(rect->get_width(),
			rect->get_height());

		ImageBlink* imageblink;
		imageblink = dynamic_cast<ImageBlink*>(image);
		if (imageblink != NULL)
		{
			if( !object->get_path(mBACKGROUND).empty() )
			{
				imageblink->set_image_path(object->get_path(mBACKGROUND));
				imageblink->set_image2_path(object->get_path(mode));
			}
			else
			{
				if( !object->get_path(mode,true).empty() )
				{
					imageblink->set_image_path(object->get_path(mode,true));
					imageblink->set_image2_path(object->get_path(mode));
				}
				else
				{
					imageblink->set_image_path(object->get_path(mOFF));
					imageblink->set_image2_path(object->get_path(mode));
				}
			}
		}
		else
		{
			image->set_image_path(object->get_path(mode));
		}

		image->set_mode(mode);
		image->set_priority(object->get_priority(mode));
    object->add_child(image, typeView);
	}
};
// -------------------------------------------------------------------------
void TypicalMultiState::constructor()
{
	set_priority(mWARNING_HIGH, 5);
	set_priority(mWARNING_LOW, 5);
	set_priority(mRESERV, 2);
	set_priority(mALARM_HIGH, 6);
	set_priority(mALARM_LOW, 6);

	add_child(&statemultilogic_firststate_, typeLogic);
	add_child(&statemultilogic_secondstate_, typeLogic);
	add_child(&statemultilogic_thirdstate_, typeLogic);
	add_child(&statemultilogic_fourthstate_, typeLogic);
  showlogic_ = new ShowLogic();
  put(*showlogic_,0,0);
	add_child(showlogic_, typeLogic);
#if GTK_VERSION_GE(2,18)
	statemultilogic_firststate_.set_visible(false);
	statemultilogic_secondstate_.set_visible(false);
	statemultilogic_thirdstate_.set_visible(false);
	statemultilogic_fourthstate_.set_visible(false);
	showlogic_->set_visible(false);
#else
	statemultilogic_firststate_.hide();
	statemultilogic_secondstate_.hide();
	statemultilogic_thirdstate_.hide();
	statemultilogic_fourthstate_.hide();
  showlogic_->hide();
#endif
}
// -------------------------------------------------------------------------
TypicalMultiState::TypicalMultiState() :
	Glib::ObjectBase("typicalgdgfourstate")
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalMultiState::TypicalMultiState(AbstractTypical::BaseObjectType* gobject) :
	AbstractTypical(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalMultiState::~TypicalMultiState()
{
  delete showlogic_;
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_rect(const Gdk::Rectangle rect)
{
	rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_path(const long mode, const Glib::ustring& path)
{
	pair<ModePathMap::iterator, bool> ret;
	ret = paths_.insert(ModePathPair(mode, path));

	assert(ret.second == true);
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_path2(const long mode, const Glib::ustring& path)
{
	pair<ModePathMap::iterator, bool> ret;
	ret = paths_2.insert(ModePathPair(mode, path));

	assert(ret.second == true);
}
// -------------------------------------------------------------------------
void TypicalMultiState::insert_mode(const long mode, const long priority, const long blink)
{
	ModeImageMap::iterator it = images_.find(mode);
	if ( it != images_.end() )
	{
		delete it->second;
		it->second = ( blink ? new ImageBlink():new Image() );
	}
	else
		images_.insert(ModeImagePair(mode, blink ? new ImageBlink():new Image()));
	if(priority != -1)
		set_priority(mode, priority);

	assert( !images_.empty() );
}
// -------------------------------------------------------------------------
void TypicalMultiState::configure()
{
	set_property_disconnect_effect(0);//Отключаем рисование серой области.
	/* TODO: Use TypicalMultiState non static method or make
	 * the ConfigureImage functor friend of TypicalMultiState class  */
	for_each(images_.begin(),
			images_.end(),
			bind2nd(ConfigureGDGImageFourState(), this));
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_lock_view(const bool lock)
{
	statemultilogic_firststate_.set_lock_view(lock);
	statemultilogic_secondstate_.set_lock_view(lock);
	statemultilogic_thirdstate_.set_lock_view(lock);
	statemultilogic_fourthstate_.set_lock_view(lock);
}
// -------------------------------------------------------------------------
Glib::ustring TypicalMultiState::get_path(const long mode,bool is_back)
{
	ModePathMap::iterator it;

	if( !is_back )
	{
		it = paths_.find(mode);
		if( it != paths_.end() )
 			return it->second;
	}
	else
	{
		it = paths_2.find(mode);
		if( it != paths_2.end() )
			return it->second;
	}
	Glib::ustring found;
	return found;
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalMultiState::get_rect()
{
	return &rect_;
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_state_obj(const UniWidgetsTypes::ObjectId sensor)
{
	statemultilogic_firststate_.set_state_obj_ai(sensor);
	statemultilogic_thirdstate_.set_state_obj_ai(sensor);
	statemultilogic_secondstate_.set_state_obj_ai(sensor);
	statemultilogic_fourthstate_.set_state_obj_ai(sensor);
}
// -------------------------------------------------------------------------
// firststate
void TypicalMultiState::set_firststate_ai(const UniWidgetsTypes::ObjectId sensor)
{
	statemultilogic_firststate_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_firststate_node(const UniWidgetsTypes::ObjectId node)
{
	statemultilogic_firststate_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_firststate_mode(const long mode)
{
	statemultilogic_firststate_.set_mode(mode);
}
// -------------------------------------------------------------------------
// secondstate
void TypicalMultiState::set_secondstate_ai(const UniWidgetsTypes::ObjectId sensor)
{
	statemultilogic_secondstate_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_secondstate_node(const UniWidgetsTypes::ObjectId node)
{
	statemultilogic_secondstate_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_secondstate_mode(const long mode)
{
	statemultilogic_secondstate_.set_mode(mode);
}
// -------------------------------------------------------------------------
// thirdstate
void TypicalMultiState::set_thirdstate_ai(const UniWidgetsTypes::ObjectId sensor)
{
	statemultilogic_thirdstate_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_thirdstate_node(const UniWidgetsTypes::ObjectId node)
{
	statemultilogic_thirdstate_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_thirdstate_mode(const long mode)
{
	statemultilogic_thirdstate_.set_mode(mode);
}
// -------------------------------------------------------------------------
// fourthstate
void TypicalMultiState::set_fourthstate_ai(const UniWidgetsTypes::ObjectId sensor)
{
	statemultilogic_fourthstate_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_fourthstate_node(const UniWidgetsTypes::ObjectId node)
{
	statemultilogic_fourthstate_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalMultiState::set_fourthstate_mode(const long mode)
{
	statemultilogic_fourthstate_.set_mode(mode);
}
// -------------------------------------------------------------------------
// ignore modes
void TypicalMultiState::add_firststate_ignoremode(const long mode)
{
	statemultilogic_firststate_.add_ignoremode(mode);
}
// -------------------------------------------------------------------------
void TypicalMultiState::add_secondstate_ignoremode(const long mode)
{
	statemultilogic_secondstate_.add_ignoremode(mode);
}
// -------------------------------------------------------------------------
void TypicalMultiState::add_thirdstate_ignoremode(const long mode)
{
	statemultilogic_thirdstate_.add_ignoremode(mode);
}
// -------------------------------------------------------------------------
void TypicalMultiState::add_fourthstate_ignoremode(const long mode)
{
	statemultilogic_fourthstate_.add_ignoremode(mode);
}
// -------------------------------------------------------------------------
// blinkoff modes
void TypicalMultiState::add_firststate_blinkoff_mode(const long mode)
{
	statemultilogic_firststate_.add_blinkoff_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalMultiState::add_secondstate_blinkoff_mode(const long mode)
{
	statemultilogic_secondstate_.add_blinkoff_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalMultiState::add_thirdstate_blinkoff_mode(const long mode)
{
	statemultilogic_thirdstate_.add_blinkoff_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalMultiState::add_fourthstate_blinkoff_mode(const long mode)
{
	statemultilogic_fourthstate_.add_blinkoff_mode(mode);
}
// -------------------------------------------------------------------------
