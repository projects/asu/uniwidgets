#ifndef _TYPICALMULTISTATE_H
#define _TYPICALMULTISTATE_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <typical/AbstractTypical.h>
#include <global_macros.h>
#include <objects/StateMultiLogic.h>
#include <objects/ShowLogic.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class Image;
// class ShowLogic;
// class StateMultiLogic;
/*!
 * \brief Типовой контейнер SimpleObject сенсор для ГДГ.
 * \par
 * Типовой контейнер представляет собой рамку для ГДГ, отображающую состояние датчиков,
 * с возможностью игнорировать определенные состояния.
 */
class TypicalMultiState : public AbstractTypical
{
public:
	TypicalMultiState();
	explicit TypicalMultiState(AbstractTypical::BaseObjectType* gobject);
	virtual ~TypicalMultiState();

	/* FIXME: Move this types to private or into include/types.h */
	/* Types */
	typedef std::unordered_map<long, Image*> ModeImageMap;
	typedef std::pair<long, Image*> ModeImagePair;
	typedef std::unordered_map<long, Glib::ustring > ModePathMap;
	typedef std::pair<long, Glib::ustring > ModePathPair;

	/* Methods */
	void configure();

	void set_rect(const Gdk::Rectangle rect);
	void set_path(const long mode, const Glib::ustring& path);
	void set_path2(const long mode, const Glib::ustring& path);

	void set_firststate_ai(const UniWidgetsTypes::ObjectId sensor);
	void set_firststate_node(const UniWidgetsTypes::ObjectId node);
	void set_firststate_mode(const long mode);
	void add_firststate_ignoremode(const long mode);
	void add_firststate_blinkoff_mode(const long mode);

	void set_secondstate_ai(const UniWidgetsTypes::ObjectId sensor);
	void set_secondstate_node(const UniWidgetsTypes::ObjectId node);
	void set_secondstate_mode(const long mode);
	void add_secondstate_ignoremode(const long mode);
	void add_secondstate_blinkoff_mode(const long mode);

	void set_thirdstate_ai(const UniWidgetsTypes::ObjectId sensor);
	void set_thirdstate_node(const UniWidgetsTypes::ObjectId node);
	void set_thirdstate_mode(const long mode);
	void add_thirdstate_ignoremode(const long mode);
	void add_thirdstate_blinkoff_mode(const long mode);

	void set_fourthstate_ai(const UniWidgetsTypes::ObjectId sensor);
	void set_fourthstate_node(const UniWidgetsTypes::ObjectId node);
	void set_fourthstate_mode(const long mode);
	void add_fourthstate_ignoremode(const long mode);
	void add_fourthstate_blinkoff_mode(const long mode);

	virtual void set_lock_view(const bool lock);

	void set_state_obj(const UniWidgetsTypes::ObjectId sensor);
	void insert_mode(const long mode, const long priority, const long blink);	/*!< добавить режим с параметрами */

	Glib::ustring get_path(const long mode, bool is_back = false);

	Gdk::Rectangle* get_rect();

private:
	/* Variables */
	Gdk::Rectangle rect_;
	ShowLogic *showlogic_;
	StateMultiLogic statemultilogic_firststate_;
	StateMultiLogic statemultilogic_secondstate_;
	StateMultiLogic statemultilogic_thirdstate_;
	StateMultiLogic statemultilogic_fourthstate_;
	ModeImageMap images_;
	ModePathMap paths_;
	ModePathMap paths_2;

	/* Methods */
	void constructor();

	DISALLOW_COPY_AND_ASSIGN(TypicalMultiState);
};

}
#endif
