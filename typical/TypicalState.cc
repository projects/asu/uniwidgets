#include <iostream>
#include <components/ImageBlink.h>
#include <types.h>
#include "TypicalState.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
class ConfigureImage: public binary_function<TypicalState::ModeImagePair, TypicalState*, void>
{
public:
	void operator() (TypicalState::ModeImagePair pair, TypicalState* object) const
	{
		Image* image = pair.second;
		int mode = pair.first;
		if( object->get_path(mode).empty() )
			return;

		Gdk::Rectangle* rect = object->get_rect();
		image->set_size_request(rect->get_width(),
			rect->get_height());
		object->put(*image,0,0);

		ImageBlink* imageblink;
		imageblink = dynamic_cast<ImageBlink*>(image);
		if (imageblink != NULL)
		{
			if(!object->get_path(mBACKGROUND).empty() )
			{
				imageblink->set_image_path(object->get_path(mBACKGROUND));
				imageblink->set_image2_path(object->get_path(mode));
			}
			else
			{
				if( !object->get_path(mode,true).empty() )
				{
					imageblink->set_image_path(object->get_path(mode,true));
					imageblink->set_image2_path(object->get_path(mode));
				}
				else
				{
					imageblink->set_image_path(object->get_path(mOFF));
					imageblink->set_image2_path(object->get_path(mode));
				}
			}
		}
		else
		{
			image->set_image_path(object->get_path(mode));
		}
		image->set_mode(mode);
		image->set_priority(object->get_priority(mode));
    object->add_child(image, typeView);
	}
};
// -------------------------------------------------------------------------
void TypicalState::constructor()
{
	add_child(&statelogic_, typeLogic);
	add_child(&showlogic_, typeLogic);
#if GTK_VERSION_GE(2,18)
	statelogic_.set_visible(false);
	showlogic_.set_visible(false);
#else
	statelogic_.hide();
	showlogic_.hide();
#endif
}
// -------------------------------------------------------------------------
TypicalState::TypicalState() :
	Glib::ObjectBase("typicalstate")
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalState::TypicalState(AbstractTypical::BaseObjectType* gobject) :
	AbstractTypical(gobject)
{
	constructor();
}
// -------------------------------------------------------------------------
TypicalState::~TypicalState()
{
}
// -------------------------------------------------------------------------
void TypicalState::set_rect(const Gdk::Rectangle rect)
{
	rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalState::set_path(const long mode, const Glib::ustring& path)
{
	pair<ModePathMap::iterator, bool> ret;
	ret = paths_.insert(ModePathPair(mode, path));

	assert(ret.second == true);
}
// -------------------------------------------------------------------------
void TypicalState::set_path2(const long mode, const Glib::ustring& path)
{
	pair<ModePathMap::iterator, bool> ret;
	ret = paths_2.insert(ModePathPair(mode, path));

	assert(ret.second == true);
}
// -------------------------------------------------------------------------
void TypicalState::insert_mode(const long mode, const long priority, const long blink)
{
	ModeImageMap::iterator it = images_.find(mode);
	if ( it != images_.end() )
	{
		delete it->second;
		it->second = ( blink ? new ImageBlink():new Image() );
	}
	else
		images_.insert(ModeImagePair(mode, blink ? new ImageBlink():new Image()));
	if(priority != -1)
		set_priority(mode, priority);

	assert( !images_.empty() );
}
// -------------------------------------------------------------------------
void TypicalState::invert_mode_off(const int priority)
{
	ModeImageMap::iterator it = images_.find(mOFF);
	if ( it != images_.end() )
	{
		delete it->second;
		it->second = new ImageBlink();
		set_priority(mOFF, priority);
	}
	else
		insert_mode(mOFF,priority,true);

	assert( !images_.empty() );
}
// -------------------------------------------------------------------------
void TypicalState::invert_mode_off_state(UniWidgetsTypes::ThresholdType type)
{
	statelogic_.set_invert_mode_state(type);
}
// -------------------------------------------------------------------------
void TypicalState::configure()
{
	set_property_disconnect_effect(0);//Отключаем рисование серой области.
	/* TODO: Use TypicalState non static method or make
	 * the ConfigureImage functor friend of TypicalState class  */
	
	for_each(images_.begin(),
			images_.end(),
			bind2nd(ConfigureImage(), this));
}
// -------------------------------------------------------------------------
Glib::ustring TypicalState::get_path(const long mode, bool is_back)
{
	ModePathMap::iterator it;
	if( !is_back )
	{
		it = paths_.find(mode);
		if( it != paths_.end() )
 			return it->second;
	}
	else
	{
		it = paths_2.find(mode);
		if( it != paths_2.end() )
			return it->second;
	}
	Glib::ustring not_found;
	return not_found;
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalState::get_rect()
{
	return &rect_;
}
// -------------------------------------------------------------------------
void TypicalState::set_lock_view(const bool lock)
{
	statelogic_.set_lock_view(lock);
}
// -------------------------------------------------------------------------
void TypicalState::set_state_ai(const UniWidgetsTypes::ObjectId sensor)
{
	statelogic_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalState::set_state_obj(const UniWidgetsTypes::ObjectId sensor)
{
	statelogic_.set_state_obj_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalState::set_node(const UniWidgetsTypes::ObjectId node)
{
	statelogic_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalState::set_mode_state(const long mode)
{
	statelogic_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalState::set_detntr_state(const long mode)
{
	statelogic_.set_detntr(mode);
}
// -------------------------------------------------------------------------
void TypicalState::set_invert_mode(const bool state)
{
	statelogic_.set_invert_mode(state);
}
// -------------------------------------------------------------------------
