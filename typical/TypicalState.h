#ifndef _TYPICALSTATE_H
#define _TYPICALSTATE_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <typical/AbstractTypical.h>
#include <objects/StateLogic.h>
#include <objects/ShowLogic.h>
#include <components/Image.h>
#include <USignals.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой контейнер SimpleObject сенсор.
 * \par
 * Типовой контейнер представляет собой набор картинок, логику для их отображения
 * и логику для работы с датчиком. Логика работы с датчиком одна, поэтому можно
 * работать либо с аналоговым датчиком и несколькими состояниями или с дискретным, но
 * с одним состоянием. Также можно работать с аналоговым датчиком как с дискретным при
 * помощи параметра "детонатор".
 */
class TypicalState : public AbstractTypical
{
public:
	TypicalState();
	explicit TypicalState(AbstractTypical::BaseObjectType* gobject);
	virtual ~TypicalState();

	/* Types */
	typedef std::unordered_map<long, Image*> ModeImageMap;					/*!< тип stl контейнера для хранения картинок для режимов */
	typedef std::pair<long, Image*> ModeImagePair;					/*!< тип элемента для stl контейнера для хранения объектов Image */
	typedef std::unordered_map<long, Glib::ustring > ModePathMap;				/*!< тип stl контейнера для хранения картинок для режимов */
	typedef std::pair<long, Glib::ustring > ModePathPair;				/*!< тип элемента для stl контейнера для хранения картинок */

	/* Methods */
	void configure();								/*!< конфигурирование контейнера */

	void set_rect(const Gdk::Rectangle rect);
	void set_path(const long mode, const Glib::ustring& path);			/*!< задать путь для картинки определенного режима */
	void set_path2(const long mode, const Glib::ustring& path);			/*!< задать путь для картинки подложки определенного режима */
	void set_state_ai(const UniWidgetsTypes::ObjectId sensor);				/*!< задать id датчик состояния */
	void set_state_obj(const UniWidgetsTypes::ObjectId sensor);				/*!< задать id датчик внутреннего состояния */
	void set_node(const UniWidgetsTypes::ObjectId node);				/*!< задать id узла */
	void set_mode_state(const long mode);						/*!< задать режим для логики */
	void set_detntr_state(const long mode);						/*!< задать "детонатор" для логики */
	void set_invert_mode(const bool state);						/*!< задать инверсию режим выключен */
	virtual void set_lock_view(const bool lock);					/*!< задать блокировку экрана при срабатывании АПС */
	void insert_mode(const long mode, const long priority, const long blink);	/*!< добавить режим с параметрами */
	void invert_mode_off(const int priority=0);					/*!< задать мигающую картинку с приоритетом для режима выключен */
	void invert_mode_off_state(UniWidgetsTypes::ThresholdType);			/*!< задать тип инвертированного режима выключен */

	Glib::ustring get_path(const long, bool is_back = false);
	Gdk::Rectangle* get_rect();

private:
	/* Variables */
	Gdk::Rectangle rect_;
	ShowLogic showlogic_;
	StateLogic statelogic_;
	ModeImageMap images_;
	ModePathMap paths_; /*Пути для картинок*/
	ModePathMap paths_2; /*Пути для картинок "подложек" основного состояния*/

	/* Methods */
	void constructor();

	DISALLOW_COPY_AND_ASSIGN(TypicalState);
};

}
#endif
