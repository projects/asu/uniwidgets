#include <iostream>
#include <components/TextBlink.h>
#include <components/Text.h>
#include <types.h>
#include "TypicalText.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
class ConfigureText: public binary_function<TypicalText::ModeTextPair,TypicalText *, void>
{
public:
  void operator() (TypicalText::ModeTextPair pair,TypicalText* object) const
  {
    text_prop* property = pair.second;

    if(property->bstate)
    {
      TextBlink* tx = new TextBlink();
      tx->set_property_on_font_name_(property->fname);
      tx->set_property_on_font_color_(property->color_on);
      tx->set_property_on_abs_font_size_(property->fsize);
        
      object->put( *tx, 0, 0 );
  
      tx->set_mode(property->mode);


      int pr = object->get_priority(property->mode);
      if( property->priority == -1 )
        tx->set_priority(pr);
      else{
        object->set_priority( property->mode, property->priority );
        tx->set_priority( property->priority );
      }
      tx->set_property_text_(property->text);
      tx->set_property_font_color_(property->color_off);
      tx->set_property_drop_shadow_(property->shadow);
      tx->set_property_font_name_(property->fname);
      tx->set_property_abs_font_size_(property->fsize);
      tx->set_property_alignment_(property->al);
      tx->set_size_request(-1,-1);
      object->add_child(tx, typeView);
      object->texts_.push_back ( tx );
    }
    else
    {
      Text *tx = new Text();
        
      object->put( *tx, 0, 0 );
  
      tx->set_mode(property->mode);
      int pr = object->get_priority(property->mode);
      if( property->priority == -1 )
        tx->set_priority(pr);
      else{
        object->set_priority( property->mode, property->priority );
        tx->set_priority( property->priority );
      }
      tx->set_property_text_(property->text);
      tx->set_property_font_color_(property->color_on);
      tx->set_property_drop_shadow_(property->shadow);
      tx->set_property_font_name_(property->fname);
      tx->set_property_abs_font_size_(property->fsize);
      tx->set_property_alignment_(property->al);
      tx->set_size_request(-1,-1);
      object->add_child(tx, typeView);
      object->texts_.push_back ( tx );
    }
  }
};
// -------------------------------------------------------------------------
void TypicalText::constructor()
{
  set_size_request(-1,-1);
  add_child(&statelogic_, typeLogic);
  add_child(&showlogic_, typeLogic);
#if GTK_VERSION_GE(2,18)
  statelogic_.set_visible(false);
  showlogic_.set_visible(false);
#else
  statelogic_.hide();
  showlogic_.hide();
#endif
}
// -------------------------------------------------------------------------
TypicalText::TypicalText() :
  Glib::ObjectBase("typicaltext")
{
  constructor();
}
// -------------------------------------------------------------------------
TypicalText::TypicalText(AbstractTypical::BaseObjectType* gobject) :
  AbstractTypical(gobject)
{
  constructor();
}
// -------------------------------------------------------------------------
TypicalText::~TypicalText()
{
}
// -------------------------------------------------------------------------
void TypicalText::set_rect(Gdk::Rectangle *rect)
{
  rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalText::add_mode_text(text_prop *tp)
{
  ModeTextMap::iterator it = t_modes_.find(tp->mode);

  if( it == t_modes_.end() )
    t_modes_.insert(ModeTextPair(tp->mode, tp));
}
// -------------------------------------------------------------------------
void TypicalText::configure()
{
  set_property_disconnect_effect(0);//Отключаем рисование серой области.
  for_each(t_modes_.begin(),
      t_modes_.end(),
      bind2nd(ConfigureText(), this));
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalText::get_rect()
{
  return rect_;
}
// -------------------------------------------------------------------------
void TypicalText::set_textlogic_state_ai(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalText::set_state_obj(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_.set_state_obj_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalText::set_node(const UniWidgetsTypes::ObjectId node)
{
  statelogic_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalText::set_textlogic_mode(const long mode)
{
  statelogic_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalText::set_textlogic_detntr(const long mode)
{
  statelogic_.set_detntr(mode);
}
// -------------------------------------------------------------------------
void TypicalText::set_lock_view(const bool lock)
{
  statelogic_.set_lock_view(lock);
}
// -------------------------------------------------------------------------
void TypicalText::set_invert_mode(const bool state)
{
  statelogic_.set_invert_mode(state);
}
// -------------------------------------------------------------------------
