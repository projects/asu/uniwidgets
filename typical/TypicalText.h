#ifndef _TYPICALTEXT_H
#define _TYPICALTEXT_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <typical/AbstractTypical.h>
#include <objects/StateLogic.h>
#include <objects/ShowLogic.h>
#include <components/Text.h>
#include <USignals.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
class Text;
/*! \brief Вспомогательная структура.
 *  \par
 *  Cтруктура для описания объета отображения "Текст".
*/
struct text_prop{
  long mode;      /*!< режим(mode), т.е. уникальный идентификатор */
  long priority;      /*!< приоритет, служит для определения порядка отображения нескольких объектов */
  Glib::ustring text;    /*!< содержание строки */
  Glib::ustring fname;    /*!< шрифт текста */
  long fsize;      /*!< размер шрифта */
  Gdk::Color color_on;    /*!< цвет состояния включен */
  Gdk::Color color_off;    /*!< цвет состояния выключен */
  bool bstate;      /*!< мигание */
  bool shadow;      /*!< тень текста */
  Pango::Alignment al;    /*!< выравнивание */
};
/*!
 * \brief Типовой контейнер SimpleObject текст.
 * \par
 * Типовой контейнер представляет собой набор текстовых строк, логику для их отображения
 * и логику для работы с датчиком. Принцип работы такой же как у TypicalState, за исключением
 * объектов отображения - текст вместо картинок. Как и для картинок для текста задается режим
 * и приоритет, но для задания внешнего вида текста используется структура, в которой описывается
 * шрифт, цвет, выравнивание, размер, способность мигать, тень текста и сам текст.
 */
class TypicalText : public AbstractTypical
{
public:
  TypicalText();
  explicit TypicalText(AbstractTypical::BaseObjectType* gobject);
  virtual ~TypicalText();

  /* FIXME: Move this types to private or into include/types.h */
  /* Types */
  typedef std::unordered_map<long, text_prop*> ModeTextMap;        /*!< тип stl контейнера для хранения описания режимов текста для создания дочерних виджетов типа Text или TextBlink */
  typedef std::pair<long, text_prop*> ModeTextPair;      /*!< тип элемента для stl контейнера для хранения объектов text_prop */

  /* Methods */
  void configure();              /*!< конфигурирование контейнера */

  void set_rect(Gdk::Rectangle* rect);
  void set_textlogic_state_ai(const UniWidgetsTypes::ObjectId sensor);  /*!< задать id датчик состояния */
  void set_node(const UniWidgetsTypes::ObjectId node);      /*!< задать id узла */
  void set_textlogic_mode(const long);          /*!< задать режим для логики */
  void set_textlogic_detntr(const long);          /*!< задать "детонатор" для логики */
  void set_invert_mode(const bool state);          /*!< задать инверсию режим выключен */
  void set_state_obj(const UniWidgetsTypes::ObjectId sensor);      /*!< задать id датчик внутреннего состояния */
  virtual void set_lock_view(const bool lock);        /*!< задать блокировку экрана при срабатывании АПС */

  void add_mode_text(text_prop *);          /*!< добавить новый режим */

  Gdk::Rectangle* get_rect();

  /* Variables */
  std::vector<Text *> texts_;          /*!< дочение виджеты Text в контейнере */

private:
  /* Variables */
  void constructor();
  Gdk::Rectangle* rect_;
  ShowLogic showlogic_;
  StateLogic statelogic_;
  ModeTextMap t_modes_;

  /* Methods */
  void create_texts(long int num_state = 1);

  DISALLOW_COPY_AND_ASSIGN(TypicalText);
};

}
#endif
