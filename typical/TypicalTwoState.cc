#include <iostream>
#include <components/ImageBlink.h>
#include <types.h>
#include "TypicalTwoState.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
class ConfigureImageTwoState: public binary_function<TypicalTwoState::ModeImagePair, TypicalTwoState*, void>
{
public:
  void operator() (TypicalTwoState::ModeImagePair pair, TypicalTwoState* object) const
  {
    Image* image = dynamic_cast< Image* >(pair.second);
    int mode = pair.first;

    if( object->get_path(mode).empty() )
      return;

    Gdk::Rectangle* rect = object->get_rect();

    object->put(*image,0,0);

    image->set_size_request(rect->get_width(),
      rect->get_height());

    ImageBlink* imageblink;
    imageblink = dynamic_cast<ImageBlink*>(image);
    if (imageblink != NULL)
    {
      if( !object->get_path(mBACKGROUND).empty() )
      {
        imageblink->set_image_path(object->get_path(mBACKGROUND));
        imageblink->set_image2_path(object->get_path(mode));
      }
      else
      {
        if( !object->get_path(mode, true).empty() )
        {
          imageblink->set_image_path(object->get_path(mode, true));
          imageblink->set_image2_path(object->get_path(mode));
        }
        else
        {
          imageblink->set_image_path(object->get_path(mOFF));
          imageblink->set_image2_path(object->get_path(mode));
        }
      }
    }
    else
    {
      image->set_image_path(object->get_path(mode));
    }

    image->set_mode(mode);
    image->set_priority(object->get_priority(mode));
    object->add_child(image, typeView);
  }
};
// -------------------------------------------------------------------------
void TypicalTwoState::constructor()
{
  set_priority(mWARNING_HIGH, 5);
  set_priority(mWARNING_LOW, 5);
  set_priority(mALARM_HIGH, 6);
  set_priority(mALARM_LOW, 6);
  set_priority(mRESERV, 2);

  add_child(&statelogic_high_, typeLogic);
  add_child(&statelogic_low_, typeLogic);
  add_child(&showlogic_, typeLogic);
#if GTK_VERSION_GE(2,18)
  statelogic_high_.set_visible(false);
  statelogic_low_.set_visible(false);
  showlogic_.set_visible(false);
#else
  statelogic_high_.hide();
  statelogic_low_.hide();
  showlogic_.hide();
#endif
}
// -------------------------------------------------------------------------
TypicalTwoState::TypicalTwoState() :
  Glib::ObjectBase("typicaltwostate")
{
  constructor();
}
// -------------------------------------------------------------------------
TypicalTwoState::TypicalTwoState(AbstractTypical::BaseObjectType* gobject) :
  AbstractTypical(gobject)
{
  constructor();
}
// -------------------------------------------------------------------------
TypicalTwoState::~TypicalTwoState()
{
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_rect(const Gdk::Rectangle rect)
{
  rect_ = rect;
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_path(const long mode, const Glib::ustring& path)
{
  if(!path.empty())
  {
    pair<ModePathMap::iterator, bool> ret;
    ret = paths_.insert(ModePathPair(mode, path));

    assert(ret.second == true);
  }
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_path2(const long mode, const Glib::ustring& path)
{
  if(!path.empty())
  {
    pair<ModePathMap::iterator, bool> ret;
    ret = paths_2.insert(ModePathPair(mode, path));
  
    assert(ret.second == true);
  }
}
// -------------------------------------------------------------------------
void TypicalTwoState::insert_mode(const long mode, const long priority, const long blink)
{
  ModeImageMap::iterator it = images_.find(mode);
  if ( it != images_.end() )
  {
    delete it->second;
    it->second = ( blink ? new ImageBlink():new Image() );
  }
  else
    images_.insert(ModeImagePair(mode, blink ? new ImageBlink():new Image()));
  if(priority != -1)
    set_priority(mode, priority);

  assert( !images_.empty() );
}
// -------------------------------------------------------------------------
void TypicalTwoState::configure()
{
  set_property_disconnect_effect(0);//Отключаем рисование серой области.
  /* TODO: Use TypicalTwoState non static method or make
   * the ConfigureImage functor friend of TypicalTwoState class  */
  for_each(images_.begin(),
      images_.end(),
      bind2nd(ConfigureImageTwoState(), this));
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_lock_view(const bool lock)
{
  statelogic_high_.set_lock_view(lock);
  statelogic_low_.set_lock_view(lock);
}
// -------------------------------------------------------------------------
Glib::ustring TypicalTwoState::get_path(const long mode, bool is_back)
{
  ModePathMap::iterator it;

  if( !is_back )
  {
    it = paths_.find(mode);
    if( it != paths_.end() )
       return it->second;
  }
  else
  {
    it = paths_2.find(mode);
    if( it != paths_2.end() )
      return it->second;
  }
  Glib::ustring found;
  return found;
}
// -------------------------------------------------------------------------
Gdk::Rectangle* TypicalTwoState::get_rect()
{
  return &rect_;
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_state_ai_high(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_high_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_state_obj(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_high_.set_state_obj_ai(sensor);
  statelogic_low_.set_state_obj_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_node_high(const UniWidgetsTypes::ObjectId node)
{
  statelogic_high_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_mode_state_high(const long mode)
{
  statelogic_high_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_state_ai_low(const UniWidgetsTypes::ObjectId sensor)
{
  statelogic_low_.set_state_ai(sensor);
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_node_low(const UniWidgetsTypes::ObjectId node)
{
  statelogic_low_.set_node(node);
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_mode_state_low(const long mode)
{
  statelogic_low_.set_mode(mode);
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_state_blink_low(bool state)
{
  statelogic_low_.set_blinking(state);
}
// -------------------------------------------------------------------------
void TypicalTwoState::set_state_blink_high(bool state)
{
  statelogic_high_.set_blinking(state);
}
// -------------------------------------------------------------------------
