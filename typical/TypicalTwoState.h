#ifndef _TYPICALTWOSTATE_H
#define _TYPICALTWOSTATE_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <typical/AbstractTypical.h>
#include <objects/StateLogic.h>
#include <objects/ShowLogic.h>
#include <components/Image.h>
#include <USignals.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой контейнер SimpleObject сенсор с двумя логиками состояния.
 * \par
 * Это контейнер содержит четыре логики состояния датчика, логику отображения картинок
 * и, собственно, сами картинки. Контейнер применяется когда нужно на одном виджете
 * разместить до 2x различных состояния от различных датчиков.
 * Например, рамка индикатора с 2мя разными порогами(верхний и нижний).
 * При этом получается, что режимы могут совпадать по типу и нужно их различать.
 * Для этого ввели новые типы: mWARNING_HIGH,mWARNING_LOW,mALARM_HIGH,mALARM_LOW,
 * которые по сути попарно одикаковые, но различаются типом.
 */
class TypicalTwoState : public AbstractTypical
{
public:
  TypicalTwoState();
  explicit TypicalTwoState(AbstractTypical::BaseObjectType* gobject);
  virtual ~TypicalTwoState();

  /* FIXME: Move this types to private or into include/types.h */
  /* Types */
  typedef std::unordered_map<long, Image*> ModeImageMap;          /*!< тип stl контейнера для хранения картинок для режимов */
  typedef std::pair<long, Image*> ModeImagePair;          /*!< тип элемента для stl контейнера для хранения объектов Image */
  typedef std::unordered_map<long, Glib::ustring > ModePathMap;        /*!< тип stl контейнера для хранения картинок для режимов */
  typedef std::pair<long, Glib::ustring > ModePathPair;        /*!< тип элемента для stl контейнера для хранения картинок */

  /* Methods */
  void configure();                /*!< конфигурирование контейнера */

  void set_rect(const Gdk::Rectangle rect);          /*!< задать размеры контейнера */
  void set_path(const long mode, const Glib::ustring& path);      /*!< задать путь для картинки определенного режима */
  void set_path2(const long mode, const Glib::ustring& path);      /*!< задать путь для картинки подложки определенного режима */
  void set_state_ai_high(const UniWidgetsTypes::ObjectId sensor);      /*!< задать id датчик для логики(x_HIGH) */
  void set_state_obj(const UniWidgetsTypes::ObjectId sensor);        /*!< задать id датчик внутреннего состояния */
  void set_node_high(const UniWidgetsTypes::ObjectId node);        /*!< задать id узла для логики(x_HIGH) */
  void set_mode_state_high(const long mode);          /*!< задать режим для логики(x_HIGH) */
  void set_state_ai_low(const UniWidgetsTypes::ObjectId sensor);      /*!< задать id датчик для логики(x_LOW) */
  void set_node_low(const UniWidgetsTypes::ObjectId node);        /*!< задать id узла для логики(x_LOW) */
  void set_mode_state_low(const long mode);          /*!< задать режим для логики(x_LOW) */
  void set_state_blink_low(bool state);            /*!< задать мигание нижним порогом(нужно для некоторых индикаторов, когда верхний уровень АПС и должени мигать при срабатывании, а нижний нет)*/
  void set_state_blink_high(bool state);
  virtual void set_lock_view(const bool lock);          /*!< задать блокировку экрана при срабатывании АПС */
  void insert_mode(const long mode, const long priority, const long blink);  /*!< добавить режим с параметрами */

  Glib::ustring get_path(const long mode, bool is_back = false);      /*!< получить картинку для нужного режима */

  Gdk::Rectangle* get_rect();              /*!< получить размеры виджета */

private:
  /* Variables */
  Gdk::Rectangle rect_;
  ShowLogic showlogic_;
  StateLogic statelogic_high_;
  StateLogic statelogic_low_;
  ModeImageMap images_;
  ModePathMap paths_;/*Пути для картинок*/
  ModePathMap paths_2;/*Пути для картинок "подложек" основного состояния*/

  /* Methods */
  void constructor();

  DISALLOW_COPY_AND_ASSIGN(TypicalTwoState);
};

}
#endif
