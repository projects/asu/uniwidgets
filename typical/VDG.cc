#include <iostream>
#include <types.h>
#include "VDG.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define VDG_NODE        "node"
#define VDG_LINK_DI        "link-di"
#define VDG_NAME_PATH        "name-path"
#define VDG_G_OFF_PATH        "g-off-path"
#define VDG_G_ON_PATH        "g-on-path"
#define VDG_G_WARN_PATH        "g-warn-path"
#define VDG_G_ALARM_PATH      "g-alarm-path"
#define VDG_D_OFF_PATH        "d-off-path"
#define VDG_D_ON_PATH        "d-on-path"
#define VDG_D_ALARM_PATH      "d-alarm-path"
#define VDG_GENERATOR_AI      "generator-ai"
#define VDG_DIESEL_ON_DI      "diesel-on-di"
#define VDG_DIESEL_ALARM_DI      "diesel-alarm-di"
#define VDG_KEY_AI        "key-ai"
#define VDG_BACK_PROTECTION_AI      "back_protection-ai"
#define VDG_GEN_WORK_PATH      "gen-work-path"
#define VDG_GEN_OVERLOAD105_DI      "gen-overload105-di"
#define VDG_GEN_OVERLOAD105_PATH    "gen-overload105-path"
#define VDG_GEN_OVERLOAD110_DI      "gen-overload110-di"
#define VDG_GEN_OVERLOAD110_PATH    "gen-overload110-path"
#define VDG_INDICATOR_U_AI      "indicator-u-ai"
#define VDG_INDICATOR_P_AI      "indicator-p-ai"
#define VDG_INDICATOR_F_AI      "indicator-f-ai"
#define VDG_INDICATOR_I_AI      "indicator-i-ai"
#define VDG_INDICATOR_THRESHOLD_U_WARN    "indicator-threshold-u-warn"
#define VDG_INDICATOR_THRESHOLD_P_LOW_WARN  "indicator-threshold-p-low-warn"
#define VDG_INDICATOR_THRESHOLD_P_WARN    "indicator-threshold-p-warn"
#define VDG_INDICATOR_THRESHOLD_P_ALARM    "indicator-threshold-p-alarm"
#define VDG_INDICATOR_THRESHOLD_F_WARN    "indicator-threshold-f-warn"

#define VDG_CONTROL_DI        "control-di"
#define VDG_READY_DI        "vdg-ready-di"
#define VDG_READY_PATH        "vdg-ready-path"

#define X_CONTROL        "x-control"
#define Y_CONTROL        "y-control"
#define W_CONTROL        "w-control"
#define H_CONTROL        "h-control"

#define VDG_BACK_WIDTH        "back-width"
#define VDG_BACK_HEIGHT        "back-heigth"
#define X_DIESEL_GEN        "x-diesel-gen"
#define Y_DIESEL        "y-diesel"
#define W_H_DIESEL        "w-h-diesel"
#define Y_GEN          "y-gen"
#define W_H_GEN          "w-h-gen"
#define X_GEN_STATES        "x-gen-states"
#define Y_GEN_STATES        "y-gen-states"
#define W_GEN_STATES        "w-gen-states"
#define H_GEN_STATES        "h-gen-states"
#define X_INDICATORS_UP        "x-indicators-up"
#define X_INDICATORS_IF        "x-indicators-if"
#define Y_INDICATOR_U        "y-indicators-u"
#define Y_INDICATOR_P        "y-indicators-p"
#define Y_INDICATOR_F        "y-indicators-f"
#define Y_INDICATOR_I        "y-indicators-i"
#define W_INDICATORS_UP        "w-indicators-up"
#define H_INDICATORS_UP        "h-indicators-up"
#define W_INDICATORS_IF        "w-indicators-if"
#define H_INDICATORS_IF        "h-indicators-if"
#define X_INDICATOR_VALUE_U      "x-indicator-value-u"
#define X_INDICATOR_VALUE_P      "x-indicator-value-p"
#define X_INDICATOR_VALUE_F      "x-indicator-value-f"
#define X_INDICATOR_VALUE_I      "x-indicator-value-i"
#define X_KEY          "x-key"
#define Y_KEY          "y-key"
#define W_KEY          "w-key"
#define H_KEY          "h-key"
#define X_NAME          "x-name"
#define Y_NAME          "y-name"
#define W_NAME          "w-name"
#define H_NAME          "h-name"
#define SVG_PATH        "svg-path"
// -------------------------------------------------------------------------
#define INIT_VDG_PROPERTIES() \
  node(*this, VDG_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,property_link_di(*this, VDG_LINK_DI , UniWidgetsTypes::DefaultObjectId) \
  ,svg_path(*this, SVG_PATH ,"/usr/share/yauza/19/svg/ses/") \
  ,name_path(*this, VDG_NAME_PATH , "" ) \
  ,g_off_path(*this, VDG_G_OFF_PATH , "" ) \
  ,g_on_path(*this, VDG_G_ON_PATH , "" ) \
  ,g_warn_path(*this, VDG_G_WARN_PATH , "" ) \
  ,g_alarm_path(*this, VDG_G_ALARM_PATH , "" ) \
  ,d_off_path(*this, VDG_D_OFF_PATH , "" ) \
  ,d_on_path(*this, VDG_D_ON_PATH , "" ) \
  ,d_alarm_path(*this, VDG_D_ALARM_PATH , "" ) \
  ,generator_ai(*this, VDG_GENERATOR_AI , UniWidgetsTypes::DefaultObjectId) \
  ,diesel_on_di(*this, VDG_DIESEL_ON_DI , UniWidgetsTypes::DefaultObjectId) \
  ,diesel_alarm_di(*this, VDG_DIESEL_ALARM_DI , UniWidgetsTypes::DefaultObjectId) \
  ,key_ai(*this, VDG_KEY_AI , UniWidgetsTypes::DefaultObjectId) \
  ,back_protection_ai(*this, VDG_BACK_PROTECTION_AI , UniWidgetsTypes::DefaultObjectId) \
  ,gen_work_path(*this, VDG_GEN_WORK_PATH , "" ) \
  ,gen_overload105_di(*this, VDG_GEN_OVERLOAD105_DI , UniWidgetsTypes::DefaultObjectId) \
  ,gen_overload105_path(*this, VDG_GEN_OVERLOAD105_PATH , "" ) \
  ,gen_overload110_di(*this, VDG_GEN_OVERLOAD110_DI , UniWidgetsTypes::DefaultObjectId) \
  ,gen_overload110_path(*this, VDG_GEN_OVERLOAD110_PATH , "" ) \
  \
  ,indicator_u_ai(*this, VDG_INDICATOR_U_AI, UniWidgetsTypes::DefaultObjectId) \
  ,indicator_p_ai(*this, VDG_INDICATOR_P_AI, UniWidgetsTypes::DefaultObjectId) \
  ,indicator_f_ai(*this, VDG_INDICATOR_F_AI, UniWidgetsTypes::DefaultObjectId) \
  ,indicator_i_ai(*this, VDG_INDICATOR_I_AI, UniWidgetsTypes::DefaultObjectId) \
  ,indicator_threshold_u_warn(*this, VDG_INDICATOR_THRESHOLD_U_WARN , UniWidgetsTypes::DefaultObjectId) \
  ,indicator_threshold_p_low_warn(*this, VDG_INDICATOR_THRESHOLD_P_LOW_WARN , UniWidgetsTypes::DefaultObjectId) \
  ,indicator_threshold_p_warn(*this, VDG_INDICATOR_THRESHOLD_P_WARN , UniWidgetsTypes::DefaultObjectId) \
  ,indicator_threshold_p_alarm(*this, VDG_INDICATOR_THRESHOLD_P_ALARM , UniWidgetsTypes::DefaultObjectId) \
  ,indicator_threshold_f_warn(*this, VDG_INDICATOR_THRESHOLD_F_WARN , UniWidgetsTypes::DefaultObjectId) \
  \
  ,control_di(*this, VDG_CONTROL_DI , UniWidgetsTypes::DefaultObjectId) \
  \
  ,vdg_ready_di(*this, VDG_READY_DI , UniWidgetsTypes::DefaultObjectId) \
  ,vdg_ready_path(*this, VDG_READY_PATH , "" ) \
  ,back_width(*this, VDG_BACK_WIDTH, 212) \
  ,back_height(*this, VDG_BACK_HEIGHT, 173) \
  \
  ,x_control(*this, X_CONTROL, 46) \
  ,y_control(*this, Y_CONTROL, 26) \
  ,w_control(*this, W_CONTROL, 210) \
  ,h_control(*this, H_CONTROL, 18) \
  \
  ,x_diesel_gen(*this, X_DIESEL_GEN, 11) \
  ,y_diesel(*this, Y_DIESEL, 50) \
  ,w_h_diesel(*this, W_H_DIESEL, 51) \
  ,y_gen(*this, Y_GEN, 112) \
  ,w_h_gen(*this, W_H_GEN, 50) \
  ,x_gen_states(*this, X_GEN_STATES, 69) \
  ,y_gen_states(*this, Y_GEN_STATES, 19) \
  ,w_gen_states(*this, W_GEN_STATES, 136) \
  ,h_gen_states(*this, H_GEN_STATES, 39) \
  ,x_indicators_up(*this, X_INDICATORS_UP, 72) \
  ,x_indicators_if(*this, X_INDICATORS_IF, 72) \
  ,y_indicator_u(*this, Y_INDICATOR_U, 60) \
  ,y_indicator_p(*this, Y_INDICATOR_P, 96) \
  ,y_indicator_f(*this, Y_INDICATOR_F, 132) \
  ,y_indicator_i(*this, Y_INDICATOR_I, 132) \
  ,w_indicators_up(*this, W_INDICATORS_UP, 105) \
  ,h_indicators_up(*this, H_INDICATORS_UP, 32) \
  ,w_indicators_if(*this, W_INDICATORS_IF, 105) \
  ,h_indicators_if(*this, H_INDICATORS_IF, 32) \
  ,x_indicator_value_u(*this, X_INDICATOR_VALUE_U, 8 ) \
  ,x_indicator_value_p(*this, X_INDICATOR_VALUE_P, 8 ) \
  ,x_indicator_value_f(*this, X_INDICATOR_VALUE_F, 8 ) \
  ,x_indicator_value_i(*this, X_INDICATOR_VALUE_I, 8 ) \
  ,x_key(*this, X_KEY, 30) \
  ,y_key(*this, Y_KEY, 171) \
  ,w_key(*this, W_KEY, 26) \
  ,h_key(*this, H_KEY, 51) \
  ,x_name(*this, X_NAME, 8) \
  ,y_name(*this, Y_NAME, 17) \
  ,w_name(*this, W_NAME, 27) \
  ,h_name(*this, H_NAME, 10)
// -------------------------------------------------------------------------
/* Static definitions */
const string VDG::img_back_path = "vdg_back.svg";
const string VDG::img_back_protection_path = "vdg_back_alarm.svg";

/* Keys */
const string VDG::img_key_off_path = "gdg_key_off.svg";
const string VDG::img_key_on_path = "gdg_key_on.svg";
const string VDG::img_key_protection_path = "gdg_key_alarm.svg";
const string VDG::img_key_undef_path = "gdg_key_undef.svg";
const string VDG::img_key_undef_off_path = "gdg_key_undef_off.svg";

const string VDG::img_gen_off_path = "gdg_gen_off.svg";

/*Control*/
const string VDG::img_control_dist_path = "vdg_control_dist.svg";
const string VDG::img_control_hand_path = "vdg_control_hand.svg";

/* Indicator */
const string VDG::img_indicator_u_path = "gdg_indicator_u_off.svg";
const string VDG::img_indicator_u_warn_path = "gdg_indicator_u_warn.svg";
const string VDG::img_indicator_p_path = "gdg_indicator_p_off.svg";
const string VDG::img_indicator_p_warn_path = "gdg_indicator_p_warn.svg";
const string VDG::img_indicator_p_alarm_path = "gdg_indicator_p_alarm.svg";
const string VDG::img_indicator_f_path = "vdg_indicator_f_off.svg";
const string VDG::img_indicator_f_warn_path = "vdg_indicator_f_warn.svg";
const string VDG::img_indicator_i_path = "vdg_indicator_i.svg";
// -------------------------------------------------------------------------
void VDG::constructor()
{

  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback<VDG>;

  is_configured = false;
  /*Добавляем к родительскому контейнеру с типом*/
  add_child( &state_back,typeObject );
  add_child( &state_key,typeObject );
  add_child( &state_D,typeObject );
  add_child( &state_G,typeObject );
  add_child( &generator_states,typeObject );
  add_child( &indicator_u,typeObject );
  add_child( &indicator_p,typeObject );
  add_child( &indicator_f,typeObject );
  add_child( &state_control,typeObject );
  add_child(&link_, typeLogic);
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  back_rect = new Gdk::Rectangle(0, 0, get_back_width(), get_back_height());
  D_rect = new Gdk::Rectangle(get_x_diesel_gen(), get_y_diesel(), get_w_h_diesel(), get_w_h_diesel());
  G_rect = new Gdk::Rectangle(get_x_diesel_gen(), get_y_gen(), get_w_h_gen(), get_w_h_gen());
  gen_states_rect = new Gdk::Rectangle(get_x_gen_states(), get_y_gen_states(), get_w_gen_states(), get_h_gen_states());
  key_rect = new Gdk::Rectangle(get_x_key(), get_y_key(), get_w_key(), get_h_key());
  control_rect = new Gdk::Rectangle(get_x_control(), get_y_control(), get_w_control(), get_h_control());

  connect_property_changed("x-control", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("y-control", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("w-control", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("h-control", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("back_width", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("back_heigth", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("x_diesel_gen", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("y_diesel", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("w_h_diesel", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("y_gen", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("w_h_gen", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("x_gen_states", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("y_gen_states", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("w_gen_states", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("h_gen_states", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("x_key", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("y_key", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("w_key", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
  connect_property_changed("h_key", sigc::mem_fun(*this, &VDG::on_rectangle_changed));
}
// -------------------------------------------------------------------------
VDG::VDG() :
  Glib::ObjectBase("vdg")
  ,INIT_VDG_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
VDG::VDG(SimpleObject::BaseObjectType* gobject) :
  SimpleObject(gobject)
  ,INIT_VDG_PROPERTIES()
{
  constructor();
}
// -------------------------------------------------------------------------
VDG::~VDG()
{
}
// -------------------------------------------------------------------------
void VDG::on_rectangle_changed()
{
  set_size_request(-1,-1);
  back_rect->set_x(0);
  back_rect->set_y(0);
  back_rect->set_width(get_back_width());
  back_rect->set_height(get_back_height());

  control_rect->set_x(get_x_control());
  control_rect->set_y(get_y_control());
  control_rect->set_width(get_w_control());
  control_rect->set_height(get_h_control());

  D_rect->set_x(get_x_diesel_gen());
  D_rect->set_y(get_y_diesel());
  D_rect->set_width(get_w_h_diesel());
  D_rect->set_height(get_w_h_diesel());

  G_rect->set_x(get_x_diesel_gen());
  G_rect->set_y(get_y_gen());
  G_rect->set_width(get_w_h_gen());
  G_rect->set_height(get_w_h_gen());

  gen_states_rect->set_x(get_x_gen_states());
  gen_states_rect->set_y(get_y_gen_states());
  gen_states_rect->set_width(get_w_gen_states());
  gen_states_rect->set_height(get_h_gen_states());

  key_rect->set_x(get_x_key());
  key_rect->set_y(get_y_key());
  key_rect->set_width(get_w_key());
  key_rect->set_height(get_h_key());
  queue_resize();
}
// -------------------------------------------------------------------------
void VDG::on_realize()
{
  SimpleObject::on_realize();
  on_rectangle_changed();

  if (!is_configured )
  {
    is_configured = true;
    on_configure();
  }
}
// -------------------------------------------------------------------------
void VDG::on_connect() throw()
{
  SimpleObject::on_connect();
  on_rectangle_changed();

  if (!is_configured )
  {
    is_configured = true;
    on_configure();
  }
}
// -------------------------------------------------------------------------
void VDG::on_configure()
{
  link_.set_link_di( get_property_link_di() );
  link_.set_node( get_node() );
#if GTK_VERSION_GE(2,18)
  link_.set_visible(false);
#else
  link_.hide();
#endif
  /* Create background Object */
  state_back.set_rect(*back_rect);

  state_back.set_path(mOFF, get_svg_path() + img_back_path);
  state_back.set_path(mPROTECTION, get_svg_path() + img_back_protection_path);
  state_back.insert_mode(mOFF,-1,false);
  state_back.insert_mode(mPROTECTION,-1,true);

  state_back.set_state_ai(get_back_protection_ai());
  state_back.set_mode_state( mPROTECTION );
  state_back.set_detntr_state(mPROTECTION);
  state_back.set_node(get_node());
  state_back.set_lock_view(get_property_lock_view());

  state_back.configure();
  
  put(state_back,
      back_rect->get_x(),
      back_rect->get_y());


  /* Name */
  put(name,get_x_name(),get_y_name());
 
  name.set_size_request(get_w_name(),get_h_name());
  name.set_image_path(get_name_path());
  name.show();

  add_child(&name,  typeView);

  /* Control */
  if( get_control_di() != UniWidgetsTypes::DefaultObjectId )
  {
    state_control.set_rect(*control_rect);

    state_control.set_path(mON, get_svg_path() + img_control_dist_path);
    state_control.set_path(mOFF, get_svg_path() + img_control_hand_path);
    state_control.insert_mode(mOFF,-1,false);
    state_control.insert_mode(mON,-1,false);

    state_control.set_state_ai(get_control_di());
    state_control.set_node(get_node());
  
    state_control.configure();
  
    /* The SimpleObject::on_realize will be called after this put */
    put(state_control,
        control_rect->get_x(),
        control_rect->get_y());
    state_control.show();
  }

  /*Key*/
  state_key.set_rect(*key_rect);

  state_key.set_path(mOFF, get_svg_path() + img_key_off_path);
  state_key.set_path(mON, get_svg_path() + img_key_on_path);
  state_key.set_path(mPROTECTION, get_svg_path() + img_key_protection_path);
  state_key.set_path(mUNKNOWN, get_svg_path() + img_key_undef_path);
  state_key.set_path2(mUNKNOWN, get_svg_path() + img_key_undef_off_path);
  state_key.insert_mode(mOFF,-1,false);
  state_key.insert_mode(mON,-1,false);
  state_key.insert_mode(mPROTECTION,-1,true);
  state_key.insert_mode(mUNKNOWN,-1,true);

  state_key.set_state_ai(get_key_ai());
  state_key.set_node(get_node());

  state_key.set_lock_view(get_property_lock_view());

  state_key.configure();

  /* The SimpleObject::on_realize will be called after this put */
  put(state_key,
      key_rect->get_x(),
      key_rect->get_y());

  /* Diesel */
  state_D.set_rect(*D_rect);
  state_D.set_path(mOFF, get_d_off_path());
  state_D.set_path(mON, get_d_on_path());
  state_D.set_path(mALARM_HIGH, get_d_alarm_path());
  state_D.insert_mode(mOFF,-1,false);
  state_D.insert_mode(mON,-1,false);
  state_D.insert_mode(mALARM_HIGH,-1,true);

  state_D.set_mode_state_low(mON);
  state_D.set_state_ai_low(get_diesel_on_di());
  state_D.set_node_low(get_node());

  state_D.set_mode_state_high(mALARM_HIGH);
  state_D.set_state_ai_high(get_diesel_alarm_di());
  state_D.set_node_high(get_node());

  state_D.set_lock_view(get_property_lock_view());

  state_D.configure();

  /* The SimpleObject::on_realize will be called after this put */
  put(state_D,
      D_rect->get_x(),
      D_rect->get_y());

  /* Generator */
  state_G.set_rect(*G_rect);
  state_G.set_path(mOFF, get_g_off_path());
  state_G.set_path(mON, get_g_on_path());
  state_G.set_path(mALARM_HIGH, get_g_alarm_path());
  state_G.insert_mode(mOFF,-1,false);
  state_G.insert_mode(mON,-1,false);
  state_G.insert_mode(mALARM_HIGH,-1,true);

  state_G.set_mode_state_low(mON);
  state_G.set_state_ai_low(get_diesel_on_di());
  state_G.set_node_low(get_node());

  state_G.set_mode_state_high(mALARM_HIGH);
  state_G.set_state_ai_high(get_diesel_alarm_di());
  state_G.set_node_high(get_node());

  state_G.set_lock_view(get_property_lock_view());

  state_G.configure();
  
  /* The SimpleObject::on_realize will be called after this put */
  put(state_G,
      G_rect->get_x(),
      G_rect->get_y());

  
  /* Indicator U */
  indicator_u.set_svg_path(get_svg_path());
  indicator_u.set_value_ai(get_indicator_u_ai());
  indicator_u.set_node(get_node());

  indicator_u.set_off_path(get_svg_path() + img_indicator_u_path);
  indicator_u.set_warn_path(get_svg_path() + img_indicator_u_warn_path);

  indicator_u.set_thresholdlow_warn(get_indicator_threshold_u_warn());
  indicator_u.set_width(get_w_indicators_up());
  indicator_u.set_height(get_h_indicators_up());
  indicator_u.set_ind_rect_x(get_x_indicator_value_u());
  indicator_u.set_property_precision(1);
  indicator_u.set_property_digits(5);
  indicator_u.set_indicator_lock_view_high(get_property_lock_view());
  indicator_u.set_indicator_lock_view_low(get_property_lock_view());

  put(indicator_u,get_x_indicators_up(),get_y_indicator_u());


  /* Indicator P */
  indicator_p.set_svg_path(get_svg_path());
  indicator_p.set_value_ai(get_indicator_p_ai());
  indicator_p.set_node(get_node());

  indicator_p.set_off_path(get_svg_path() + img_indicator_p_path);
  indicator_p.set_warn_path(get_svg_path() + img_indicator_p_warn_path);
  indicator_p.set_alarm_path(get_svg_path() + img_indicator_p_alarm_path);

  indicator_p.set_thresholdhigh_warn(get_indicator_threshold_p_warn());
  indicator_p.set_thresholdhigh_alarm(get_indicator_threshold_p_alarm());
  indicator_p.set_thresholdlow_warn(get_indicator_threshold_p_low_warn());
  indicator_p.set_width(get_w_indicators_up());
  indicator_p.set_height(get_h_indicators_up());
  indicator_p.set_ind_rect_x(get_x_indicator_value_p());
  indicator_p.set_blinking_low(false);
  indicator_p.set_property_precision(1);
  indicator_p.set_property_digits(5);
  indicator_p.set_indicator_lock_view_high(get_property_lock_view());
  indicator_p.set_indicator_lock_view_low(get_property_lock_view());

  put(indicator_p,get_x_indicators_up(),get_y_indicator_p());

  /* Indicator F */
  indicator_f.set_svg_path(get_svg_path());
  indicator_f.set_value_ai(get_indicator_f_ai());
  indicator_f.set_node(get_node());

  indicator_f.set_off_path(get_svg_path() + img_indicator_f_path);
  indicator_f.set_warn_path(get_svg_path() + img_indicator_f_warn_path);

  indicator_f.set_thresholdlow_warn(get_indicator_threshold_f_warn());
  indicator_f.set_width(get_w_indicators_if());
  indicator_f.set_height(get_h_indicators_if());
  indicator_f.set_ind_rect_x(get_x_indicator_value_f());
  indicator_f.set_property_precision(2);
  indicator_f.set_property_digits(5);
  indicator_f.set_indicator_lock_view_high(get_property_lock_view());
  indicator_f.set_indicator_lock_view_low(get_property_lock_view());
  
  put(indicator_f,get_x_indicators_if(),get_y_indicator_f());

  /* Indicator I */
  if(get_indicator_i_ai() != UniWidgetsTypes::DefaultObjectId)
  {
    IndicatorTwoState *new_indicator_i = new IndicatorTwoState();
    new_indicator_i->set_svg_path(get_svg_path());
    new_indicator_i->set_value_ai(get_indicator_i_ai());
    new_indicator_i->set_node(get_node());

    new_indicator_i->set_off_path(get_svg_path() + img_indicator_i_path);

    new_indicator_i->set_width(get_w_indicators_if());
    new_indicator_i->set_height(get_h_indicators_if());
    new_indicator_i->set_ind_rect_x(get_x_indicator_value_i());
    new_indicator_i->set_property_precision(0);
    new_indicator_i->set_property_digits(4);

    indicator_i = new_indicator_i;
    add_child( indicator_i,typeObject );
    put(*indicator_i,get_x_indicators_if(),get_y_indicator_i());
  }

  /* Generator states*/
  generator_states.set_rect(*gen_states_rect);

  generator_states.set_path(mOFF, get_svg_path() + img_gen_off_path);
  generator_states.set_path(mON, get_gen_work_path());
  generator_states.set_path(mWARNING_HIGH, get_gen_overload105_path());
  generator_states.set_path(mALARM_HIGH, get_gen_overload110_path());
  generator_states.insert_mode(mOFF,-1,false);
  generator_states.insert_mode(mON,-1,false);
  generator_states.insert_mode(mWARNING_HIGH,-1,true);
  generator_states.insert_mode(mALARM_HIGH,-1,true);

  generator_states.set_mode_state_low_warn(mON);
  generator_states.set_state_ai_low_warn(get_diesel_on_di());
  generator_states.set_node_low_warn(get_node());

  generator_states.set_mode_state_high_warn(mWARNING_HIGH);
  generator_states.set_state_ai_high_warn(get_gen_overload105_di());
  generator_states.set_node_high_warn(get_node());

  generator_states.set_mode_state_high_alarm(mALARM_HIGH);
  generator_states.set_state_ai_high_alarm(get_gen_overload110_di());
  generator_states.set_node_high_alarm(get_node());

  if( get_vdg_ready_di() != UniWidgetsTypes::DefaultObjectId )
  {
    generator_states.set_path(mREADY, get_vdg_ready_path());
        generator_states.insert_mode(mREADY,-1,false);
    generator_states.set_mode_state_low_alarm(mREADY);
    generator_states.set_state_ai_low_alarm(get_vdg_ready_di());
    generator_states.set_node_low_alarm(get_node());
  }
  generator_states.set_lock_view(get_property_lock_view());

  generator_states.configure();
  
  /* The SimpleObject::on_realize will be called after this put */
  put(generator_states,
      gen_states_rect->get_x(),
      gen_states_rect->get_y());


  /* Show all objects */
  show();
  state_back.show();
  state_key.show();
  state_D.show();
  state_G.show();
  generator_states.show();
}
// -------------------------------------------------------------------------
