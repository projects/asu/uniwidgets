#ifndef _VDG_H
#define _VDG_H
// -------------------------------------------------------------------------
#include <string>
#include <map>
#include <components/Image.h>
#include <objects/SimpleObject.h>
#include <objects/LinkLogic.h>
#include <typical/TypicalState.h>
#include <typical/IndicatorTwoState.h>
#include <typical/IndicatorFourState.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Типовой виджет ВДГ.
 * \par
 * Виджет отображает работу Вспомогательного Дизель Генератора. На виджете отображается
 * состояние ВДГ, рамка, ГА, состояния дизеля и генератора, и четыре цифровых индикатора.
 * 
 * \n Пример ВДГ: \n \n
 * \image html vdg.png
*/
class VDG : public SimpleObject
{
public:
  VDG();
  explicit VDG(SimpleObject::BaseObjectType* gobject);
  virtual ~VDG();

  /* Constants */
  Gdk::Rectangle *back_rect;        /*!< координаты и размер рамки и фона виджета */
  Gdk::Rectangle *D_rect;          /*!< координаты и размер области отображения дизеля */
  Gdk::Rectangle *G_rect;          /*!< координаты и размер области отображения генератора */
  Gdk::Rectangle *gen_states_rect;      /*!< координаты и размер области отображения состояния виджета */
  Gdk::Rectangle *key_rect;        /*!< координаты и размер области отображения ГА */
  Gdk::Rectangle *control_rect;        /*!< координаты и размер области отображения состояния управления */
  static const std::string img_back_path;      /*!< путь к картинке рамки с фоном(отключен) */
  static const std::string img_back_protection_path;    /*!< путь к картинке рамки с фоном(защита) */

  static const std::string img_key_off_path;      /*!< путь к картинке ГА(отключен) */
  static const std::string img_key_on_path;      /*!< путь к картинке ГА(включен) */
  static const std::string img_key_protection_path;    /*!< путь к картинке ГА(сработала защита) */
  static const std::string img_key_undef_path;      /*!< путь к картинке ГА(неопределенное состояние) */
  static const std::string img_key_undef_off_path;    /*!< путь к картинке ГА(неопределенное состояние подложка) */

  static const std::string img_gen_off_path;      /*!< путь к картинке состояния виджета(отключен) */

  static const std::string img_indicator_u_path;    /*!< путь к картинке индикатора напряжения */
  static const std::string img_indicator_u_warn_path;    /*!< путь к картинке индикатора напряжения(предупреждение) */
  static const std::string img_indicator_p_path;    /*!< путь к картинке индикатора мощности */
  static const std::string img_indicator_p_warn_path;    /*!< путь к картинке индикатора мощности(предупреждение) */
  static const std::string img_indicator_p_alarm_path;    /*!< путь к картинке индикатора мощности(авария) */
  static const std::string img_indicator_f_path;    /*!< путь к картинке индикатора частоты */
  static const std::string img_indicator_f_warn_path;    /*!< путь к картинке индикатора частоты(предупреждение) */
  static const std::string img_indicator_i_path;    /*!< путь к картинке индикатора силы тока */

  static const std::string img_control_dist_path;    /*!< путь к картинке управление(удаленное) */
  static const std::string img_control_hand_path;    /*!< путь к картинке управление(ручное) */

  sigc::connection button_release_conn_;      /*!< ссылка на коннектор для события signal_button_release_event */

protected:
  /* Handlers */
  virtual void on_realize();
  virtual void on_connect() throw();

private:
  /* Variables */
  Image name;
  TypicalState state_back;
  TypicalState state_key;
  TypicalTwoState state_D;
  TypicalTwoState state_G;
  TypicalFourState generator_states;
  IndicatorTwoState indicator_u;
  IndicatorFourState indicator_p;
  IndicatorTwoState indicator_f;
  IndicatorTwoState *indicator_i;
  TypicalState state_control;
  LinkLogic link_;

  bool is_configured;

  /* Methods */
  void constructor();
  void on_rectangle_changed();
  void on_configure();

  DISALLOW_COPY_AND_ASSIGN(VDG);

  /* Properties */
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )        /*!< свойство: id узла */
  ADD_PROPERTY( property_link_di, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика состояния связи с SharedMemory */
  ADD_PROPERTY( svg_path, Glib::ustring )          /*!< свойство: путь к директории с картинкам */
  ADD_PROPERTY( name_path, Glib::ustring )        /*!< свойство: картинка для знака низвания виджета */
  ADD_PROPERTY( g_off_path, Glib::ustring )        /*!< свойство: картинка для генератора(выключен) */
  ADD_PROPERTY( g_on_path, Glib::ustring )        /*!< свойство: картинка для генератора(включен) */
  ADD_PROPERTY( g_warn_path, Glib::ustring )        /*!< свойство: картинка для генератора(предупреждение) */
  ADD_PROPERTY( g_alarm_path, Glib::ustring )        /*!< свойство: картинка для генератора(авария) */
  ADD_PROPERTY( d_off_path, Glib::ustring )        /*!< свойство: картинка для дизеля(выключен) */
  ADD_PROPERTY( d_on_path, Glib::ustring )        /*!< свойство: картинка для дизеля(включен) */
  ADD_PROPERTY( d_alarm_path, Glib::ustring )        /*!< свойство: картинка для дизеля(авария) */
  ADD_PROPERTY( generator_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика состояние генератора */
  ADD_PROPERTY( diesel_on_di, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика состояние дизеля(включен) */
  ADD_PROPERTY( diesel_alarm_di, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика состояние дизеля(авария) */
  ADD_PROPERTY( key_ai, UniWidgetsTypes::ObjectId )        /*!< свойство: id датчика состояние ГА */
  ADD_PROPERTY( back_protection_ai, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика режим защиты */
  ADD_PROPERTY( gen_work_path, Glib::ustring )        /*!< свойство: картинка для для надписи состояния(ГДГ работает) */
  ADD_PROPERTY( gen_overload105_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика перегрузка 105% */
  ADD_PROPERTY( gen_overload105_path, Glib::ustring )      /*!< свойство: картинка для для надписи состояния(перегрузка 105%) */
  ADD_PROPERTY( gen_overload110_di, UniWidgetsTypes::ObjectId )    /*!< свойство: id датчика перегрузка 110% */
  ADD_PROPERTY( gen_overload110_path, Glib::ustring )      /*!< свойство: картинка для для надписи состояния(перегрузка 110%) */
  ADD_PROPERTY( indicator_u_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика для индикатора напряжения */
  ADD_PROPERTY( indicator_p_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика для индикатора мощности */
  ADD_PROPERTY( indicator_f_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика для индикатора частоты тока */
  ADD_PROPERTY( indicator_i_ai, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика для индикатора силы тока */
  ADD_PROPERTY( indicator_threshold_u_warn, UniWidgetsTypes::ObjectId )  /*!< свойство: id датчика для порога напряжения(предупреждение) */
  ADD_PROPERTY( indicator_threshold_p_low_warn, UniWidgetsTypes::ObjectId )  /*!< свойство: id датчика для нижнего порога мощности(предупреждение) */
  ADD_PROPERTY( indicator_threshold_p_warn, UniWidgetsTypes::ObjectId )  /*!< свойство: id датчика для порога мощности(предупреждение) */
  ADD_PROPERTY( indicator_threshold_p_alarm, UniWidgetsTypes::ObjectId )  /*!< свойство: id датчика для порога мощности(авария) */
  ADD_PROPERTY( indicator_threshold_f_warn, UniWidgetsTypes::ObjectId )  /*!< свойство: id датчика для порога частоты(предупреждение) */
  ADD_PROPERTY( control_di, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика состояние управления */
  ADD_PROPERTY( vdg_ready_di, UniWidgetsTypes::ObjectId )      /*!< свойство: id датчика готовность к пуску */
  ADD_PROPERTY( vdg_ready_path, Glib::ustring )        /*!< свойство: картинка для надписи состояния(готов к пуску) */
  ADD_PROPERTY( back_width, long )          /*!< свойство: ширина виджета */
  ADD_PROPERTY( back_height, long )          /*!< свойство: высота виджета */
  ADD_PROPERTY( x_control, long )            /*!< свойство: x-координата надписи состояния управления */
  ADD_PROPERTY( y_control, long )            /*!< свойство: y-координата надписи состояния управления */
  ADD_PROPERTY( w_control, long )            /*!< свойство: ширина надписи состояния управления */
  ADD_PROPERTY( h_control, long )            /*!< свойство: высота надписи состояния управления */
  ADD_PROPERTY( x_diesel_gen, long )          /*!< свойство: x-координата знака для дизеля и генератора */
  ADD_PROPERTY( y_diesel, long )            /*!< свойство: y-координата знака для дизеля */
  ADD_PROPERTY( w_h_diesel, long )          /*!< свойство: размеры знака для дизеля */
  ADD_PROPERTY( y_gen, long )            /*!< свойство: y-координата знака для генератора */
  ADD_PROPERTY( w_h_gen, long )            /*!< свойство: размеры знака для генератора */
  ADD_PROPERTY( x_gen_states, long )          /*!< свойство: x-координата надписи состояния ГДГ */
  ADD_PROPERTY( y_gen_states, long )          /*!< свойство: y-координата надписи состояния ГДГ */
  ADD_PROPERTY( w_gen_states, long )          /*!< свойство: ширина надписи состояния ГДГ */
  ADD_PROPERTY( h_gen_states, long )          /*!< свойство: высота надписи состояния ГДГ */
  ADD_PROPERTY( x_indicators_up, long )          /*!< свойство: x-координата индикаторов напряжения и мощности */
  ADD_PROPERTY( x_indicators_if, long )          /*!< свойство: x-координата индикаторов силы тока и частоты */
  ADD_PROPERTY( y_indicator_u, long )          /*!< свойство: y-координата индикатора напряжения */
  ADD_PROPERTY( y_indicator_p, long )          /*!< свойство: y-координата индикатора мощности */
  ADD_PROPERTY( y_indicator_f, long )          /*!< свойство: y-координата индикатора силы тока */
  ADD_PROPERTY( y_indicator_i, long )          /*!< свойство: y-координата индикатора частоты */
  ADD_PROPERTY( w_indicators_up, long )          /*!< свойство: ширина индикаторов напряжения и мощности */
  ADD_PROPERTY( h_indicators_up, long )          /*!< свойство: высота индикаторов напряжения и мощности */
  ADD_PROPERTY( w_indicators_if, long )          /*!< свойство: ширина индикаторов силы тока и частоты */
  ADD_PROPERTY( h_indicators_if, long )          /*!< свойство: высота индикаторов силы тока и частоты */
  ADD_PROPERTY( x_indicator_value_u, long )        /*!< свойство: x-координата индикаторов напряжения */
  ADD_PROPERTY( x_indicator_value_p, long )        /*!< свойство: x-координата индикаторов мощности */
  ADD_PROPERTY( x_indicator_value_f, long )        /*!< свойство: x-координата индикаторов частоты */
  ADD_PROPERTY( x_indicator_value_i, long )        /*!< свойство: x-координата индикаторов силы тока */
  ADD_PROPERTY( x_key, long )            /*!< свойство: x-координата знака ГА */
  ADD_PROPERTY( y_key, long )            /*!< свойство: y-координата знака ГА */
  ADD_PROPERTY( w_key, long )            /*!< свойство: ширина знака ГА */
  ADD_PROPERTY( h_key, long )            /*!< свойство: высота знака ГА */
  ADD_PROPERTY( x_name, long )            /*!< свойство: x-координата картинки названия виджета */
  ADD_PROPERTY( y_name, long )            /*!< свойство: y-координата картинки названия виджета */
  ADD_PROPERTY( w_name, long )            /*!< свойство: ширина картинки названия виджета */
  ADD_PROPERTY( h_name, long )            /*!< свойство: высота картинки названия виджета */
  
};

}
#endif
