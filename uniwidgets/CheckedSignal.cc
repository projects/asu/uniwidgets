#include "CheckedSignal.h"
#include <iostream>
// -------------------------------------------------------------------------
using namespace UniWidgetsTypes;
using namespace std;
// -------------------------------------------------------------------------
CheckConnection CheckedSignal::connect( const sigc::slot<void,ObjectId>& check_slot, ObjectId sensorId )
{
	CheckBackContainer::iterator it = cb.begin();
	while ( it != cb.end() )
	{
		if ( (*it).first == sensorId ) break;
		it++;
	}

	if ( it == cb.end() )
	{
		cb.push_back( CheckBackEntry(sensorId, new sigc::signal<void,ObjectId>) );
		it == cb.rend().base();
	}
	sigc::connection conn = (*it).second->connect(check_slot);
	return CheckConnection(this, conn, sensorId);
}
// -------------------------------------------------------------------------
CheckedSignal::~CheckedSignal()
{
	for(CheckBackContainer::iterator it = cb.begin(); it != cb.end(); it++)
	{
		delete (*it).second;
	}
}
// -------------------------------------------------------------------------
bool CheckedSignal::emit()
{
	if (cb.empty()) return false;
	sigc::signal<void,ObjectId>* check_sig = cb.front().second;
	ObjectId id = cb.front().first;
	cb.pop_front();
	check_sig->emit(id);
	delete check_sig;
	if (cb.empty()) return false;
	return true;
}
// -------------------------------------------------------------------------
void CheckConnection::disconnect()
{
	if (signal == NULL)
		return;
	if (signal->cb.empty())
		return;
	CheckBackContainer::reverse_iterator it = signal->cb.rbegin();
	while ( it < signal->cb.rend() )
	{
		if ( (*it).first == sid )
		{
			connection.disconnect();
			if ( (*it).second->size() == 0 )
			{
				delete (*it).second;
				signal->cb.erase(it.base() - 1 );
				return;
			}
		}
		it++;
	}
}
// -------------------------------------------------------------------------
