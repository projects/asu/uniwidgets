#ifndef _CHECKEDSIGNAL_H
#define _CHECKEDSIGNAL_H
// -------------------------------------------------------------------------
#include "types.h"
#include <sigc++/sigc++.h>
#include <deque>
// -------------------------------------------------------------------------
class CheckConnection;

typedef std::pair< UniWidgetsTypes::ObjectId,sigc::signal< void,UniWidgetsTypes::ObjectId >* > CheckBackEntry;
typedef std::deque< CheckBackEntry > CheckBackContainer;
/*!
 * \brief Управление квитированием.
 * \par
 * Предназначен для управления квитированием.(В новых виджетах не используется т.к. есть
 * ConfirmSignal)
*/
class CheckedSignal
{
friend class CheckConnection;
public:
	CheckConnection connect( const sigc::slot< void,UniWidgetsTypes::ObjectId >&, UniWidgetsTypes::ObjectId sensorId );
	bool emit();
	~CheckedSignal();
private:
	CheckBackContainer cb;
};

class CheckConnection
{
public:
	CheckConnection(CheckedSignal* sig, sigc::connection& conn, UniWidgetsTypes::ObjectId id)
		:sid (id), signal(sig), connection(conn) {}
//	CheckConnection(const CheckConnection& chconn);
	CheckConnection():signal(NULL) {}
	~CheckConnection() {}
	void disconnect();

private:
	UniWidgetsTypes::ObjectId sid;
	CheckedSignal* signal;
	sigc::connection connection;
};
#endif
