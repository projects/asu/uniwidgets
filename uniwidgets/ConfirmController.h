#ifndef CONFIRM_CONTROLLER_H
#define CONFIRM_CONTROLLER_H
// -------------------------------------------------------------------------
#include <iostream>
#include "types.h"
#include <sigc++/sigc++.h>
#include <map>
#include <cassert>
// -------------------------------------------------------------------------
namespace ConfirmCtl
{

struct def_StateKey
{
	UniWidgetsTypes::ObjectId id;
	long value;

	bool operator<(const def_StateKey& y) const
	{ return (id < y.id || (id == y.id && value < y.value)); }
};
// -------------------------------------------------------------------------
struct StateInfo
{
	StateInfo () : on(false), confirmed(true), priority(0),
		need_confirm(false), blinkable(false) {}

	bool on;
	bool confirmed;

	int priority;
	bool need_confirm;
	bool blinkable;

	sigc::slot<void> start_blink;
	sigc::slot<void> stop_blink;
	sigc::slot<void> switch_on;
	sigc::slot<void, sigc::slot<void> > connect_confirm;
};
// -------------------------------------------------------------------------
/*!
 * \brief Управление квитированием.
 * \par
 * Предназначен для управления квитированием.(В новых виджетах не используется т.к. есть
 * ConfirmSignal)
*/
template <class T = def_StateKey, class Compare = std::less<T> >
class ConfirmController
{
public:
	typedef T StateKey;
	typedef std::map<StateKey, StateInfo, Compare> StatesMap;

	ConfirmController() : current_it_(states_map_.end()), undefined_(false) {}
	~ConfirmController() {}

	void update(StateKey state_key, bool on);
	void confirm(StateKey state_key);
	void add(StateKey state_key, StateInfo& state_info);
	void remove(const StateKey& state_key);
	void reset();

	void setBlinkable(const StateKey& state_key, bool blinkable);
	void setPriority(const StateKey& state_key, int priority);
	void setNeedConfirm(const StateKey& state_key, bool need_confirm);

	StateInfo copyStateInfo(const StateKey& state_key) const;
	bool hasState(const StateKey& state_key) const;
	const StatesMap& getStatesMap() const { return states_map_; }
	const StateInfo* getStateInfo(const StateKey& state_key) const;
	bool undefined() const;

	sigc::signal<void> signal_switch_to_undefined;

protected:
	void Error(const char* message) {}

private:
	StatesMap states_map_;
	//StateKey current_;
	typename StatesMap::iterator current_it_;

	bool undefined_;

	void switch_to_next();
	void switch_to(StateKey new_state);
	bool is_state_to_show(const StateInfo& si) const;
};
// -------------------------------------------------------------------------
template<class T,class C>
void ConfirmController<T,C>::update(StateKey sk, bool on)
{
	typename StatesMap::iterator it = states_map_.find(sk);
	if (it == states_map_.end()) {
		Error("State not found. Can't update\n");
		return;
	}

	StateInfo* si = &it->second;

	//TODO: confirm processing
	if (on == true && si->on == false) {
//		if ( !state_i->on && state_i->need_confirm && state_i->confirmed ) {
//			sigc::slot<void, StateKey> tmp_slot;
//			tmp_slot = sigc::mem_fun(*this,
//					&ConfirmController::confirm);
//			state_i->connect_confirm(sigc::bind(tmp_slot, state_k));
//			if ( state_k == current_ ) state_i->start_blink(); state_i->confirmed = false;
//		}
		si->on = true;
		si->confirmed = false;
		if (current_it_ == states_map_.end() || si->priority > current_it_->second.priority)
			switch_to(sk);
	}
	else if (on == false && si->on == true) { 
		si->on = false;
		if (it == current_it_)// && si->confirmed )
			switch_to_next();
	}
	else if (on == true && si->on == true)
	{
		if (it == current_it_)
			si->stop_blink();
		si->confirmed = true;
	}
}
// -------------------------------------------------------------------------
template<class T, class C>
void ConfirmController<T,C>::switch_to(StateKey sk)
{
	typename StatesMap::iterator it = states_map_.find(sk);

	if (it == states_map_.end()) {
		Error("(!INTERNAL ERROR!): Trying to switch to unknown state.\n");
		return;
	}

	//TODO: what is better to use?:
	//	current_it_ != states_map.end()
	//	or
	//	undefined() {return undefined_;}

	if (current_it_ != states_map_.end() &&
			current_it_->second.blinkable)
		current_it_->second.stop_blink();

	current_it_ = it;
	it->second.switch_on();

	if (it->second.blinkable && !it->second.confirmed)
		it->second.start_blink();

	//TODO: process confirming
	//const bool not_confirmed =
	//	!current_it_->second.confirmed || !current_it_->second.need_confirm;
	//if ( not_confirmed && current_it_->second.blinkable )
	//	current_it_->second.start_blink();
}
// -------------------------------------------------------------------------
template<class T, class C>
void ConfirmController<T,C>::switch_to_next()
{
	int max_priority = INT_MIN;
	StateKey next_state;
	for (typename StatesMap::iterator it = states_map_.begin();
			it != states_map_.end(); ++it)
	{
		StateInfo* si= &(*it).second;
		assert( si->priority != INT_MIN );
		//TODO: decide < or <= and write in documentation
		//TODO: process confiming
		//if ( (si->on || !si->confirmed) && max_priority <= si->priority) {
		if ( si->on && max_priority <= si->priority) {
			max_priority = si->priority;
 			next_state = (*it).first;
		}
	}
	if ( max_priority != INT_MIN )
		switch_to( next_state );
	else if ( current_it_ != states_map_.end() ) {
		if ( current_it_->second.blinkable )
			current_it_->second.stop_blink();
		current_it_ = states_map_.end();
		undefined_ = true;
		signal_switch_to_undefined.emit();
	}
}
// -------------------------------------------------------------------------
template<class T, class C>
void ConfirmController<T,C>::confirm(StateKey sk)
{
	//TODO: process confirming
//	assert( states_map_.count(sk) > 0 );
//	states_map_[sk].confirmed = true;
//	if ( sk != current_ )
//		return;	
//	if ( states_map_[sk].on && states_map_[sk].blinkable )
//		states_map_[sk].stop_blink();
//	else
//		switch_to_next();
}
// -------------------------------------------------------------------------
template<class T, class C>
void ConfirmController<T,C>::add(StateKey sk, StateInfo& si)
{
	if (states_map_.count(sk) > 0) {
		Error("Trying to add existing state.\n");
		return;
	}

	states_map_[sk] = si;

	//TODO: confirming
	if (si.on && is_state_to_show(si))
		switch_to(sk);
}
// -------------------------------------------------------------------------
template<class T, class C>
void ConfirmController<T,C>::remove(const StateKey& sk)
{
	typename StatesMap::iterator it = states_map_.find(sk);
	if (it == states_map_.end()) {
		Error("Trying to remove unknown state.\n");
		return;
	}

	if (current_it_ == it)
		update(sk, false);

	states_map_.erase(it);
}
// -------------------------------------------------------------------------
template<class T, class C>
StateInfo ConfirmController<T,C>::copyStateInfo( const StateKey& sk) const
{
	if (states_map_.count(sk) <= 0) {
		Error("Trying to get information from unknown state.\n");
		throw std::exception();
	}

	return states_map_[sk];
}
// -------------------------------------------------------------------------
template<class T, class C>
const StateInfo* ConfirmController<T,C>::getStateInfo( const StateKey& sk ) const
{
	typename StatesMap::iterator it = states_map_.find(sk);
	if (it == states_map_.end())
		return NULL;

	return &(*it).second;
}
// -------------------------------------------------------------------------
template<class T, class C>
void ConfirmController<T,C>::setPriority( const StateKey& sk, int priority )
{
	typename StatesMap::iterator it = states_map_.find(sk);
	assert( it != states_map_.end() );

	it->second.priority = priority;
	switch_to_next();
}
// -------------------------------------------------------------------------
template<class T, class C>
void ConfirmController<T,C>::setBlinkable( const StateKey& sk, bool blinkable )
{
	typename StatesMap::iterator it = states_map_.find(sk);
	assert(it != states_map_.end());

	std::swap(it->second.blinkable, blinkable);
	if (it == current_it_) {
		if (!it->second.blinkable && blinkable) // values were swapped!
			it->second.stop_blink();
		else if (it->second.blinkable && !blinkable) // values were swapped!
			it->second.start_blink();
	}
	return;
}
// -------------------------------------------------------------------------
template<class T, class C>
void ConfirmController<T,C>::setNeedConfirm( const StateKey& sk, bool need_confirm )
{
	typename StatesMap::iterator it = states_map_.find(sk);
	assert( it != states_map_.end() );

	std::swap(it->second.need_confirm, need_confirm);
	if (it != current_it_)
		return;
	//wait confirm if need_confirm enabled on curent state
	if (it->second.need_confirm && !need_confirm)
		update( it->first, true );
	return;
}
// -------------------------------------------------------------------------
template<class T, class C>
bool ConfirmController<T,C>::hasState(const StateKey& sk) const
{
	return states_map_.count(sk);
}
// -------------------------------------------------------------------------
template<class T, class C>
bool ConfirmController<T,C>::undefined() const
{
	return current_it_ == states_map_.end();
}
// -------------------------------------------------------------------------
template<class T, class C>
bool ConfirmController<T,C>::is_state_to_show(const StateInfo& si) const
{ 
	return current_it_ == states_map_.end() || current_it_->second.priority < si.priority;
}
// -------------------------------------------------------------------------
template<class T, class C>
void ConfirmController<T,C>::reset()
{
	for (typename StatesMap::iterator it = states_map_.begin();
			it != states_map_.end(); ++it)
	{
		it->second.on = false;
		it->second.confirmed = true;
	}

	current_it_ = states_map_.end();
	signal_switch_to_undefined.emit();
}

} //namespace ConfirmCtl
#endif
