#include "ConfirmSignal.h"
#include <iostream>
#include <glibmm/main.h>
// -------------------------------------------------------------------------
using namespace UniWidgetsTypes;
using namespace std;
using UMessages::MessageId;
// -----------------------------------------------------------------------------
ConfirmSignal::ConfirmSignal( Connector* connector_ ) :
	connector(connector_)
{
}
// -------------------------------------------------------------------------
MsgConfirmConnection
ConfirmSignal::connect( const MsgConfirmSlot& confirm_slot, MessageId id )
{
	if (!connector->connected())
		return MsgConfirmConnection();

  	if (_auto_confirm_time > 0 && mcc.empty())
  		Glib::signal_timeout().connect(sigc::mem_fun(this, &ConfirmSignal::auto_confirm), _auto_confirm_time);

	if ( id._node == -1 )
		id._node = connector->getLocalNode();
	MsgConfirmedContainer::iterator cit = mcedc.find( id );
	if ( cit != mcedc.end() )
		cerr << "\n*ERROR* ConfirmSignal Connecting confirm on CONFIRMED signal.\n" << endl;
	MsgConfirmContainer::iterator it = mcc.begin();
	MsgConfirmContainer::iterator end = mcc.end();
	while (it != end && (*it)._id != id) ++it;
	if (it == end)
		it = mcc.insert(end, MsgConfirmEntry(id, new MsgConfirmSignal));

	sigc::connection conn = (*it)._signal->connect(confirm_slot);

	return MsgConfirmConnection(this, conn, id);
}
// -----------------------------------------------------------------------------
sigc::connection
ConfirmSignal::connect_msg_wait_confirm( sigc::slot<void, MessageId>& slot )
{
	return signal_msg_wait_confirm.connect( slot );	
}
// -----------------------------------------------------------------------------
ConfirmSignal::~ConfirmSignal()
{
	MsgConfirmContainer::iterator end = mcc.end();
	for(MsgConfirmContainer::iterator it = mcc.begin(); it != end; ++it)
	{
		delete (*it)._signal;
	}
}
// -----------------------------------------------------------------------------
bool 
ConfirmSignal::emit(time_t sec)
{
	//TODO: think if it's normal
	//don't emit if using autoconfirm
	if (_auto_confirm_time <= 0)
		confirm(sec);
	if (mcc.empty())
		return false;
	return true;
}
// -------------------------------------------------------------------------
void 
ConfirmSignal::confirm(time_t sec)
{
	if (mcc.empty()) return;

	/**TODO: Need smth better.
	 * We forget about all messages
	 * that doesn't have any one to process.
	 */
	MsgConfirmContainer::iterator it = mcc.begin();
	while (it != mcc.end() && (*it)._signal->empty())
		it++;

	it = mcc.erase(mcc.begin(), it);

	if (mcc.empty()) return;

	MsgConfirmSignal* signal = (*it)._signal;
	MessageId id = (*it)._id;
	mcc.pop_front();

	long value;
	try {
		value = connector->get_value_directly(id._id, id._node);
	} catch ( UniWidgetsTypes::Exception& ex )
	{
		cerr << "\n*ERROR* get sensor value in ConfirmSignal\n" << endl;
	}
	/* Проверка,что значение датчика соответствует значению датчика в привязываемом сообщениии.
	   Привязка сообщений в логиках происходит не на все подряд значения,а только на те
	   которые являются АПС, соответственно и квитируются только они.
	*/
	if ( (!id._bits_msg && value == id._value) || (id._bits_msg && (value&(id.get_long_value()))) )
	{
		mcedc.insert( id );
		USignals::VConn* conn = new USignals::VConn;
		sigc::slot<void, ObjectId, ObjectId, long> drop =
		sigc::hide(sigc::hide(sigc::hide(
			sigc::bind(sigc::mem_fun( this, &ConfirmSignal::drop_message ), id, conn))));
		*conn  = connector->signals().connect_value_out( drop, id._id, id._node, id._value);
	}
	
	signal->emit(id, sec);
	delete signal;
	signal_msg_wait_confirm(mcc.front()._id);
}
// -------------------------------------------------------------------------
bool
ConfirmSignal::auto_confirm()
{
	//time processing
	time_t rawtime;
	time(&rawtime);

	confirm(rawtime);
	if (mcc.empty())
		return false;
	else
		return true;
}
// -------------------------------------------------------------------------
MessageStatus
ConfirmSignal::get_message_status(MessageId id)
{
	if ( id._node == -1 )
		id._node = connector->getLocalNode();

	MsgConfirmContainer::iterator it = mcc.begin();
	while (  it != mcc.end() && (*it)._id != id ) ++it;
	if ( it != mcc.end() )
		return WAITS_CONFIRM;
	MsgConfirmedContainer::iterator it2 = mcedc.find(id);
	if ( it2 != mcedc.end() )
		return CONFIRMED;

	return DROPPED;
}
// -------------------------------------------------------------------------
void
ConfirmSignal::drop_message(MessageId id, USignals::VConn* conn )
{
	MsgConfirmedContainer::iterator it = mcedc.find(id);
	//assert( it != mcedc.end() );
	if ( it != mcedc.end() ) mcedc.erase( it );
	conn->disconnect();
	delete conn;
}
// -------------------------------------------------------------------------
void
MsgConfirmConnection::disconnect()
{
	if (_signal == NULL)
		return;
	if (_signal->mcc.empty())
		return;

	_connection.disconnect();
}
// -------------------------------------------------------------------------
