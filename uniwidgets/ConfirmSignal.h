#ifndef _MSGCONFIRMSIGNAL_H
#define _MSGCONFIRMSIGNAL_H
// -------------------------------------------------------------------------
#include <sigc++/sigc++.h>
#include <USignals.h>
#include <deque>
#include <set>
#include <USignals.h>
#include <time.h>
#include "Connector.h"
// -------------------------------------------------------------------------
class MsgConfirmConnection;
// -------------------------------------------------------------------------
typedef sigc::slot< void, UMessages::MessageId, time_t> MsgConfirmSlot;
typedef sigc::signal< void, UMessages::MessageId, time_t> MsgConfirmSignal;
/*! \enum MessageStatus
 * Состояние квитирования сообщений.
 */
enum MessageStatus {
		     WAITS_CONFIRM 	/*!< ожидает квитирования */
		     ,CONFIRMED		/*!< квитировано */
		     ,DROPPED		/*!< сброшено, т.е. не ожидает квитирования и не находится в списке заквитированных */
		   };
// -------------------------------------------------------------------------
/*! \struct MsgConfirmEntry
 * Структура описывающая сообщения в очереди для квитирования.
 */
struct MsgConfirmEntry
{
	MsgConfirmEntry( UMessages::MessageId id, MsgConfirmSignal* signal ):
		_id( id ), _signal( signal ) {}
	UMessages::MessageId _id;		/*!< id сообщения */
	MsgConfirmSignal* _signal;		/*!< сигнал о квитировании данного сообщеия */
};
// -------------------------------------------------------------------------
typedef std::deque<MsgConfirmEntry> MsgConfirmContainer;
typedef std::set<UMessages::MessageId> MsgConfirmedContainer;
// -------------------------------------------------------------------------
/*!
 * \brief Итератор для доступа к элементам очереди сообщений на квитирование.
 * \todo check if it's normal inheritance
*/
class WaitsQueue_ConstIterator : public MsgConfirmContainer::const_iterator
{
public:
	typedef MsgConfirmContainer::const_iterator BaseIt;
	typedef MsgConfirmContainer::iterator NonConstBaseIt;
	WaitsQueue_ConstIterator( const BaseIt& iter ) : BaseIt(iter) {}
	WaitsQueue_ConstIterator( const NonConstBaseIt& iter ) : BaseIt(iter) {}
	const UMessages::MessageId& operator*() const 
	{
		return BaseIt::operator->()->_id;
	}
	const UMessages::MessageId* operator->() const
	{
		return &operator*();
	}
};
// -------------------------------------------------------------------------
/*!
 * \brief Класс реализующий очередь сообщений на квитирования.
 * \par
 * Кратко о работе этого класса:
 *     АПС сигналы, сообщения о которых были обработаны менеджером объектов GuiPM, помещаются в
 * очередь для квитирования с помощью вызова метода "connect" в логике. У каждого такого
 * сообщения в очереди есть сигнал с привязанным к нему слотом (пользователь сам его
 * соединяет в логике). Таким образом формируется очередь сообщений ожидающих квитирования.
 * Когда происходит событие квитирования(оператор нажимает кнопку), приходит сообщение с id
 * датчика квитирования. По этому id вызывается обработчик метод "confirm",где происходит
 * обработка сообщения ожидающего квитирования и перенос его в список заквитированных сообщений,
 * также вызывается пользовательский слот(функция привязанная в логике) для какой-то обработки
 * этого события и привязывается коннектор к сигналу на сброс значения датчика(здесь уже сообщение
 * удаляется и из списка заквитированных сообщений).
*/
class ConfirmSignal
{
friend class MsgConfirmConnection;
public:
	typedef WaitsQueue_ConstIterator const_iterator;
	ConfirmSignal( Connector* connector );
	~ConfirmSignal();
	/*! добавить собщение в очередь на квитирование.
	    \param confirm_slot пользовательский слот.
	    \param id сообщение, которое требует квитирования.
	    \return объект MsgConfirmConnection.
	*/
	MsgConfirmConnection connect( const MsgConfirmSlot&, UMessages::MessageId id);
	sigc::connection connect_msg_wait_confirm( sigc::slot<void, UMessages::MessageId>& slot );
	/*! получить статус сообщения.
	    \param id id сообщения.
	    \return состояние сообщения MessageStatus.
	*/
	MessageStatus get_message_status(UMessages::MessageId id);

	/*! вернуть итератор на начало очереди на квитирование */
	const_iterator begin()
	{
		return mcc.begin(); 
	}
	/*! вернуть итератор на следующий за последним элементом очереди на квитирование */
	const_iterator end()
	{
		return mcc.end();
	}
	/*! обработчик поступления сигнала о квитировании */
	bool emit(time_t sec);

	/*! установить время для автоматического квитирования */
	void set_autoconfirm_time(int new_time)
	{ _auto_confirm_time = new_time; }

private:
	void confirm(time_t);
	void drop_message(UMessages::MessageId id, USignals::VConn* conn);

	MsgConfirmContainer mcc;
	MsgConfirmedContainer mcedc;
	sigc::signal<void, UMessages::MessageId> signal_msg_wait_confirm;
	Connector* connector;

	int _auto_confirm_time;
	bool auto_confirm();
};
// -------------------------------------------------------------------------
/*!
 * \brief Вспомогательный класс для хранения сообщения и сигнала квитирования.
*/
class MsgConfirmConnection
{
public:
	MsgConfirmConnection(ConfirmSignal* sig, sigc::connection& conn, UMessages::MessageId id)
		:_id (id), _signal(sig), _connection(conn) {}
	MsgConfirmConnection():_signal(NULL) {}
	~MsgConfirmConnection() {}
	void disconnect();

private:
	UMessages::MessageId _id;
	ConfirmSignal* _signal;
	sigc::connection _connection;

public:

};
#endif
