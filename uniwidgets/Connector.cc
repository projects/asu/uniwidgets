#include "Connector.h"
#include <iostream>
#include <glibmm/main.h>
#include <glibmm/ustring.h>
// -------------------------------------------------------------------------
/*!
 * \brief Коннектор.
 * \par
 * Класс реализует коннектор к SharedMemory.
*/
// -------------------------------------------------------------------------
ConnectorRef Connector::create_connector(const Glib::ustring& type, const Glib::ustring& manager_name
							,const Glib::ustring& alive_sensor
							,const Glib::ustring& confirm_sensor
							,const Glib::ustring& auto_confirm_time_str)
{
	ConnectorRef ref = ConnectorRef();
	try
	{
		ref = ConnectorRef(DynamicLib::open(std::string("libUniWidgets2-") + type + std::string(".so")));
		ref->init_connector(manager_name, alive_sensor, confirm_sensor, auto_confirm_time_str);
	}
	catch ( UniWidgetsTypes::Exception& ex) {
		std::cerr << ex.what() << std::endl;
	}
	return ref;
}
// -------------------------------------------------------------------------
void Connector::destroy_connector(ConnectorRef& connector)
{
	if (!connector)
	{
		std::cerr << "***ERROR trying to delent empty connector" << std::endl;
		return;
	}
	
	connector.pointer_ = std::shared_ptr<Connector>();
	connector.connector_lib_ = std::shared_ptr<DynamicLib>();
}
// -------------------------------------------------------------------------
