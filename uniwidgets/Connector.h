#ifndef _CONNECTOR_H
#define _CONNECTOR_H
// -------------------------------------------------------------------------
#include <memory>
//#include <UInterface.h>
//#include <MessageType.h>
#include <USignals.h>
#include <SmartSignal.h>
#include <sigc++/sigc++.h>
#include <dynamic_libs.h>
// -------------------------------------------------------------------------
class CheckedSignal;
class ConfirmSignal;
class ConnectorRef;
// -------------------------------------------------------------------------
/*!
 * \brief Класс интерфейс коннектора.
 * \par
 * Коннестор(Connector) предназначен для обработки сигналов подсоединения к и отсоединения
 * от SharedMemory, посылки сообщений объектам SharedMemory, обработки сигналов квитирвания,
 * опроса датчиков и поступления сообщения от SharedMemory.
*/
class Connector
{
public:
	virtual ~Connector(){}

	/* Signals */
	virtual sigc::signal<void>& signal_connected() = 0;					/*!< сигнал о том что произошло соединение с SharedMemory */
	virtual sigc::signal<void>& signal_disconnected() = 0;					/*!< сигнал о том что произошло отсоединение от SharedMemory */
	virtual sigc::signal<void>& signal_startup() = 0;					/*!< сигнал старта SharedMemory */
	//virtual sigc::signal<void, UniversalIO::UIOCommand>& signal_ask_sensors() = 0;		/*!< сигнал опроса датчиков при старте */
	//virtual SmartSignal<void, const UniWidgetsTypes::SensorMessage*>& signal_sensor_info() = 0;	/*!< сигнал получения сообщений от датчиков */

	virtual CheckedSignal& signal_checked() = 0;
	virtual ConfirmSignal& signal_confirm() = 0;		/*!< сигнал квитирования */
	virtual USignals::SigController& signals() = 0;		/*!< сигналы для работы с сообщениями о датчиках */

	/* Methods */
	virtual bool connected() = 0;				/*!< получить состояние соединения с SharedMemory */

	virtual void set_process_signal_confirm(UMessages::MessageId id) = 0;		/*!< установить обработку квитирования коннектором*/
	/*! получить инфо о датчике */
	virtual UniWidgetsTypes::ObjectInfo get_info(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) = 0;
	/*! заказать значение датчика */
	virtual long get_value(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) = 0;
	/*! получить значение датчика из SharedMemory */
	virtual long get_value_directly(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) = 0;
	/*! заказать значение аналогового датчика */
	virtual float get_analog_value(const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) = 0;
	/*! сохранить значение датчика */
	virtual void save_value(const long value, const UniWidgetsTypes::ObjectId id, const UniWidgetsTypes::ObjectId node) = 0;
	/*! получить тип коннектора(на данный момент используется только тип "uniset") */
	virtual Glib::ustring get_type() const = 0;
	/*! послать сообщение объекту */
	//virtual void send(UniWidgetsTypes::ObjectId name, UniWidgetsTypes::TransportMessage& msg) = 0;
	/*! получить секцию настроек в xml формате */
	virtual xmlNode* get_xml_node( const std::string& node, const std::string& name = "" ) = 0;
	virtual UniWidgetsTypes::ObjectId getLocalNode() = 0;
	virtual UniWidgetsTypes::ObjectId getSensorID( const std::string& name ) = 0;
	virtual UniWidgetsTypes::ObjectId getNodeID( const std::string& name ) = 0;
	/*! создать коннектор.
	    \param type тип коннектора.
	    \param manager_name имя объекта(Описывается в секции <objects ... > configure.xml ).
	    \param alive_sensor id датчика "живости" SharedMemory.
	    \param confirm_sensor id датчика квитирования АПС сигналов.
	    \param auto_confirm_time_str время автоматического квитирования АПС сигналов(если "0",то квитируются сигналы вручную).
	    \return коннектор ConnectorRef.
	*/
	static ConnectorRef create_connector(const Glib::ustring& type, const Glib::ustring& manager_name
									,const Glib::ustring& alive_sensor
									,const Glib::ustring& confirm_sensor
									,const Glib::ustring& auto_confirm_time_str);
	virtual void init_connector(const Glib::ustring& manager_name
									,const Glib::ustring& alive_sensor
									,const Glib::ustring& confirm_sensor
									,const Glib::ustring& auto_confirm_time_str) = 0;
	/*! уничтожить коннектор */
	static void destroy_connector(ConnectorRef& connector);
};
/*!
 * \brief Вспомогаельный структура и тип фабрики коннектора.
 * \par
 * Класс предоставляет средства для создания и удаления коннектора при подключении динамической библиотеки
*/
// ----------------------------------------------------------------------------------
struct ConnectorDeleter
{
	void operator()(Connector* p) const
	{
		try{ delete p; } catch(...) {}
	}
};
// ----------------------------------------------------------------------------------
// the types of the class factories
typedef std::shared_ptr<Connector> create_connector_t();
// -------------------------------------------------------------------------
/*!
 * \brief Вспомогаельный класс для работы с коннектором.
 * \par
 * Класс предоставляет интерфейс для доступа к коннектору через указатель. Это
 * различные операторы(доступа, сравнения, проверки на NULL).
 * @todo this must be safe and easy to use
*/
class ConnectorRef
{
public:
	ConnectorRef() : pointer_(nullptr), connector_lib_(nullptr) {}
	ConnectorRef(const ConnectorRef& target);
	ConnectorRef(const std::shared_ptr<DynamicLib>& lib);
	ConnectorRef& operator=(const ConnectorRef& target);
	std::shared_ptr<Connector> operator->();
	bool operator==(const ConnectorRef& target) const;
	operator bool() const;

	friend class Connector;
	
private:
	std::shared_ptr<Connector> pointer_;
	std::shared_ptr<DynamicLib> connector_lib_;
};
// -------------------------------------------------------------------------
inline ConnectorRef::ConnectorRef(const ConnectorRef& target)
{
	*this = target;
}
// -------------------------------------------------------------------------
inline ConnectorRef::ConnectorRef(const std::shared_ptr<DynamicLib>& lib) : pointer_(nullptr), connector_lib_(nullptr)
{
	connector_lib_ = lib;
	if(connector_lib_)
	{
		create_connector_t* create_connector_func = (create_connector_t*) connector_lib_->load("create_connector");
		if( !create_connector_func )
		{
			//cerr << "Cannot load symbols: " << dlerror() << endl;
			throw UniWidgetsTypes::Exception(std::string("'create_connector' symbol does not correspond with 'create_connector_t' type"));
		}
		pointer_ = create_connector_func();
	}
}
// -------------------------------------------------------------------------
inline ConnectorRef&
ConnectorRef::operator=(const ConnectorRef& target)
{
	if (this != &target)
	{
		pointer_ = target.pointer_;
		connector_lib_ = target.connector_lib_;
	}
	return *this;
}
// -------------------------------------------------------------------------
inline std::shared_ptr<Connector>
ConnectorRef::operator->()
{
	return pointer_;
}
// -------------------------------------------------------------------------
inline bool
ConnectorRef::operator==(const ConnectorRef& target) const
{
	return (pointer_ == target.pointer_);
}
// -------------------------------------------------------------------------
inline
ConnectorRef::operator bool() const
{
	if (pointer_ != nullptr)
		return true;

	return false;
}
// -------------------------------------------------------------------------
#endif
