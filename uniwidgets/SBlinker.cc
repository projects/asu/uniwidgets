#include "SBlinker.h"
#include <gtkmm/main.h>
#include <iostream>
// -------------------------------------------------------------------------
Blinker::Blinker() : blink_time(DEFAULT_BLINK_TIME),state(false)
{
//	connPoll = Glib::signal_timeout().connect(sigc::mem_fun(*this, &Blinker::blink),blink_time );
	sigc::slot<bool> time_slot = sigc::bind(sigc::mem_fun(*this, &Blinker::blink),blink_time);
	connPoll[blink_time] = Glib::signal_timeout().connect(time_slot,blink_time);
}
// -------------------------------------------------------------------------
Blinker::~Blinker()
{
	for( auto&& it: connPoll )
		it.second.disconnect();
}
// -------------------------------------------------------------------------
bool Blinker::blink(int time)
{
//	std::cout<<"time="<<time<<std::endl;
//	if(connPoll.find(time)!=connPoll.end())
//	{
//		std::cout<<"time="<<time<<std::endl;
		state = !state;
		signal_blink[time].emit( state,time);
//	}
	return true;
}
// -------------------------------------------------------------------------
void Blinker::set_blink_time(int sec)
{
//	std::cout<<"Blinker::set_blink_time sec="<<sec<<std::endl;
	if ( sec <= 0 || connPoll.find(sec)!=connPoll.end())
		return; //Не хватает вывода ошибки

	blink_time = sec;
	sigc::slot<bool> time_slot = sigc::bind(sigc::mem_fun(*this, &Blinker::blink),sec);
	connPoll[blink_time] = Glib::signal_timeout().connect(time_slot,sec);
// 	std::map<int, sigc::connection>::iterator it = connPoll.begin();
//	for(it;it!=connPoll.end();it++)
//		std::cout<<"Blinker::set_blink_time map sec="<<it->first<<std::endl;
}
// -------------------------------------------------------------------------
