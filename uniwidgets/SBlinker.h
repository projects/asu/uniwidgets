#ifndef _BLINKER_H_
#define _BLINKER_H_
// -------------------------------------------------------------------------
#include <set>
#include <sigc++/sigc++.h>
#include <unordered_map>
// -------------------------------------------------------------------------
const int DEFAULT_BLINK_TIME = 500;
// -------------------------------------------------------------------------
/*! 
 * \brief Универсальный класс мигания.
 * \par
 * Реализует мигание объектов отображения.
 *	Пример использования:
 * \code
 * 	class MyClass
 * 	{
 * 	public:
 * 	    ....
 * 		void start_blink();
 * 		void stop_blink();
 * 	    ....
 * 	private:
 * 	    ....
 * 		static Blinker blinker;
 * 		sigc::connection blink_connection_;
 * 		void blink(bool state,int time = DEFAULT_BLINK_TIME);
 * 	    ....
 * 	}
 * 
 * 
 * 	Blinker CisternImageBlink::blinker;
 * 	MyClass::MyClass()
 * 	{
 * 	    ....
 * 	}
 * 	void MyClass::start_blink()
 * 	{
 * 		blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].
 * 			connect(sigc::mem_fun(this, &MyClass::blink));
 * 	    ....
 * 	}
 * 	void MyClass::stop_blink()
 * 	{
 * 		blink_connection_.disconnect();
 * 	    ....
 * 	}
 * 	void MyClass::blink(bool state,int time)
 * 	{
 * 	    ....
 * 		//DO something
 * 	    ....
 * 	}
 * \endcode
 */
class Blinker
{
private:
	int blink_time;
public:
	Blinker();
//	~Blinker(){connPoll.disconnect();}
	~Blinker();

//	sigc::signal<void,bool> signal_blink;
//	sigc::connection connPoll;

	std::unordered_map< int, sigc::signal<void,bool,int> > signal_blink;	/*!< сигналы мигания,к которому привязывается обработчик в классе объекта отображения(картинка, текст и т.п.) */
	std::unordered_map< int, sigc::connection> connPoll;			/*!< соединения к сигналам мигания различной частоты */
	bool state;							/*!< переменная состояния, переключаемая при мигании с true в false и обратно, с заданной частотой */

	bool blink(int time=DEFAULT_BLINK_TIME);			/*!< испускание сигнала мигания со сменой состояния state на противоположное */
	void set_blink_time(int sec);					/*!< установить частоту мигания */

};
#endif
