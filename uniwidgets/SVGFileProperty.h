#ifndef _SVGFILEPROPERTY_H
#define _SVGFILEPROPERTY_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <SVGLoader.h>
#include <iostream>
#include <cassert>
// -------------------------------------------------------------------------
typedef Glib::Property<Glib::ustring> PType;
// -------------------------------------------------------------------------
/*!
 * \brief Дополнительное свойство для работы с svg-картинками.
 * \par
 * Класс добавляет к родительскому виджету свойство "name"-svg-file
 * \code
 * 	Glib::Property<Glib::ustring> m_property;
 * \endcode
*/
class SVGFileProperty
{
protected:

	Glib::Property<Glib::ustring> m_property;	/*!< свойство картинки для родительского виджета */
	Gtk::Widget* m_owner;				/*!< родительский виджет */
	Glib::ustring m_name;				/*!< имя виджета */
	SVGLoader loader;				/*!< загрузчик картинок */

	sigc::connection err_idle_connection;		/*!< коннектор к сигналу об ошибке */
	Glib::ustring err_msg;				/*!< сообщение об ошибке */

	/*! обработчик изменения свойства svg-файла */
	void on_filename_changed()
	{
		assert( m_owner!=NULL );
//		err_idle_connection.disconnect();
		try
		{
			loader.loadRsvgFile ( m_property.get_value() );
		}
		catch (std::string& err)
		{
			err_msg = err;
			std::cerr << m_owner->get_name() << ":"<< err_msg << std::endl;
//			err_msg = err;
//			err_idle_connection = Glib::signal_timeout().connect( sigc::mem_fun(*this, &SVGFileProperty::print_error_message), 1000 );
		}
	}
	/*! печать сообщений об ошибке */
	bool print_error_message()
	{
		std::cerr << m_owner->get_name() << ":"<< err_msg << std::endl;
		return false;
	}

public:
	SVGFileProperty(Gtk::Widget& owner, Glib::ustring name, Glib::ustring default_filename) :
		m_property( owner, name+"-svg-file", default_filename)
		,m_owner(&owner)
		,m_name(name)
	{
	}
	/*! установить обработчик изменения свойства svg-файла и вызвать принудительно обработчик */
	void connect_processing()
	{
		on_filename_changed();
		m_owner->connect_property_changed_with_return( m_name+"-svg-file", sigc::mem_fun(*this, &SVGFileProperty::on_filename_changed));
	}

	/*! получить указатель на загрузчик svg-файлов */
	SVGLoader* get_loader() {return &loader;}
	/*! перегруженный оператор "=" для SVGFileProperty */
	SVGFileProperty& operator=( Glib::ustring new_value ) { m_property.set_value(new_value); return *this; }

	operator Glib::Property<Glib::ustring>&() { return m_property; }
	virtual ~SVGFileProperty() { err_idle_connection.disconnect(); }
private:
	SVGFileProperty();
};
#endif
