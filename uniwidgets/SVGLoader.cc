#include "SVGLoader.h"
#include "Connector.h"
#include <iostream>
#include <types.h>
#include <glibmm/init.h>
#include <librsvg/rsvg.h>
#include <librsvg/rsvg-cairo.h>
// -------------------------------------------------------------------------
using namespace std;
// -------------------------------------------------------------------------
SVGLoader::SVGLoader() :
	handle(NULL)
	,filename("")
	,svg_width(50)
	,svg_height(50)
{
	Glib::init();
}
// -------------------------------------------------------------------------
SVGLoader::SVGLoader( std::string str_file ) :
	handle(NULL)
	,filename(str_file)
	,svg_width(50)
	,svg_height(50)
{
	Glib::init();
	loadRsvgFile(filename);
}
// -------------------------------------------------------------------------
SVGLoader::~SVGLoader()
{
	if ( handle!= NULL )
		rsvg_handle_free(handle);
}
// -------------------------------------------------------------------------
bool SVGLoader::loadRsvgFile(std::string str_file, bool throw_exception)
{
	if ( handle != NULL )
		rsvg_handle_free(handle);

	filename = str_file;
	path = get_path(str_file);

	if( !(handle = rsvg_handle_new_from_file( (path+filename).c_str(),NULL)) )
	{
		//если handle = NULL - то говорим, что ошибка

		svg_width = 50;
		svg_height = 50;
		//if ( !filename.empty() && throw_exception )
				//throw UniWidgetsTypes::Exception("*** SVGLoader: \'"+path+filename+"\' not found");
		return false;
	}

	RsvgDimensionData* svg_dd = new RsvgDimensionData;
	rsvg_handle_get_dimensions(handle, svg_dd);
	svg_width = svg_dd->width;
	svg_height = svg_dd->height;

	delete svg_dd;
	return true;
}
// -------------------------------------------------------------------------
std::string SVGLoader::get_path(std::string str_file)
{
	std::string ret_path;
	if ( str_file[0] == '/' || str_file[0] == '~' || str_file[0] == '.' )
	{
		ret_path = "";
	}
	else
	{
		ret_path = UniWidgetsTypes::getArgParam("--svg-path", "/usr/share/uniwidgets2/svg/");
	}

	return ret_path;
}
// -------------------------------------------------------------------------
bool SVGLoader::renderSurface(Cairo::RefPtr<Cairo::Surface> surf, int width, int height)
{
	Cairo::RefPtr<Cairo::Context> cr = Cairo::Context::create(surf);
	if (handle!=NULL)
	{

		cr->scale((float)width/svg_width, (float)height/svg_height );
		rsvg_handle_render_cairo(handle,cr->cobj());

		return true;
	} else 
	{
		//������ �����

		cr->set_source_rgb(1.0, 1.0, 1.0);
		cr->paint();
		cr->set_source_rgba(1.0, 0.0, 0.0, 1.0);
		cr->set_line_width(10.0);
		cr->move_to(0, 0);
		cr->line_to(width, height);
		cr->move_to(0, height);
		cr->line_to(width, 0);
		cr->stroke();

		cr->move_to(10,10);
		cr->set_line_width(1.0);
		cr->set_source_rgb(0, 0, 0);
		cr->show_text("Can't load : \'"+path+filename+"\'.");
		cr->fill();

		return false;
	}
}
// -------------------------------------------------------------------------
bool SVGLoader::renderWithCairoContext(Cairo::RefPtr<Cairo::Context> &cr)
{
	if (handle!=NULL)
	{
		rsvg_handle_render_cairo(handle,cr->cobj());

		return true;
	} else 
	{
		cr->set_source_rgb(0, 0, 0);
		cr->set_line_width(1.0);
		cr->line_to(10, 10);
		cr->show_text("Can't load : \'"+path+filename+"\'.");
		cr->fill();
		return false;
	}

}
// -------------------------------------------------------------------------
SVGLoader& SVGLoader::operator=(const SVGLoader& loader)
{
	if (this == &loader)
		return *this;

	if (&loader.handle == NULL)
	{
		handle = NULL;
		return *this;
	}

	loadRsvgFile( loader.filename , false );
	return *this;
}
// -------------------------------------------------------------------------
