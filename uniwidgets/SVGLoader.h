#ifndef _SVGLOADER_H_
#define _SVGLOADER_H_
// -------------------------------------------------------------------------
#include <cairomm/context.h>
//#include <librsvg/rsvg.h>
//#include <librsvg/rsvg-cairo.h>
#include <iostream>
//#include <UEventBox.h>
// -------------------------------------------------------------------------
static std::string svg_path = "/usr/share/uniwidgets/svg/";
static const std::string no_svg = "empty.svg";

static const std::string true_svg = "true.svg";
static const std::string false_svg = "false.svg";
// -------------------------------------------------------------------------
struct _RsvgHandle;
typedef _RsvgHandle RsvgHandle;
// -------------------------------------------------------------------------
/*!
 * \brief Загрузчик svg-картинок.
 * \par
 * Класс реализует загрузку и обработку svg-картинок.
 * \code
 *
 * \endcode
*/
class SVGLoader
{
private:
	RsvgHandle* handle;		/*!< svg объект */
	std::string path;		/*!< путь к svg-файлу */
	std::string filename;		/*!< имя svg-файла */
	int svg_width;			/*!< ширина изображения */
	int svg_height;			/*!< высота изображения */

public:
	SVGLoader();
	SVGLoader(std::string path);
	~SVGLoader();
	/*! загрузить svg-объект */
	bool loadRsvgFile(std::string filename, bool throw_exception = true );
	SVGLoader& operator=(const SVGLoader&);
	/*! получить ширину изображения */
	int get_svg_width() {return svg_width;}
	/*! получить высоту изображения */
	int get_svg_height() {return svg_height;}
	/*! получить имя svg-файла */
	std::string get_filename() {return filename;}
	/*! получить путь к svg-файлу */
	static std::string get_path(std::string str_file="");
	/*! отрисовка картинки на экране */
	bool renderSurface(Cairo::RefPtr<Cairo::Surface> surf, int width, int height);
	/*! отрисовка картинки на экране */
	bool renderWithCairoContext(Cairo::RefPtr<Cairo::Context> &cr);

	bool isLoaded() { return handle?true:false;}

};
#endif
