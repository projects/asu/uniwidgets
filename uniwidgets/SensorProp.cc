#include <SensorProp.h>
#include <iostream>
#include <assert.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void SensorProp::set_sens_name( const std::string& txt )
{
	sens_name = txt;
}
// -------------------------------------------------------------------------
std::string SensorProp::get_sens_name()
{
	if ( ((string)sens_name).empty())
		return "DefaultSensor";
	else
		return sens_name;
}
// -------------------------------------------------------------------------
void SensorProp::set_node_name( const std::string& txt )
{
	node_name.set_value(txt);
}
// -------------------------------------------------------------------------
std::string SensorProp::get_node_name()
{
	if ( ((string)node_name).empty())
		return "LocalhostNode";
	else
		return node_name;
}
// -------------------------------------------------------------------------
void SensorProp::set_sens_id(ObjectId id)
{
	sens_id = id;
  set_sensor_handler();
}
// -------------------------------------------------------------------------
ObjectId SensorProp::get_sens_id()
{
	if ( sens_id == DefaultObjectId && connector_ref_ )
		return connector_ref_->getSensorID( sens_name );
	return sens_id;
}
// -------------------------------------------------------------------------
void SensorProp::set_node_id(ObjectId id)
{
	node_id = id;
  set_sensor_handler();
}
// -------------------------------------------------------------------------
ObjectId SensorProp::get_node_id()
{
	if( node_id == DefaultObjectId && connector_ref_)
		node_id = connector_ref_->getLocalNode();
	return node_id;
}
// -------------------------------------------------------------------------
void SensorProp::set_stype( const std::string& t )
{
	stype = t;
}
// -------------------------------------------------------------------------
std::string  SensorProp::get_stype()
{
	return stype;
}
// -------------------------------------------------------------------------
bool SensorProp::get_state()
{
//	if (!guipm_check_ready("get state"))
//		return false;

	if (!check_connector("get state"))
		return false;

	bool rv = false;
	try
	{
//		rv = (*guipm_ptr_)->uin->getState( get_sens_id(), get_node_id() );
		rv = (bool)connector_ref_->get_value_directly( get_sens_id(), get_node_id() );
	}
	catch (Exception& ex)
	{
		cerr << "(" << owner->get_name() << "): error while getting state of " 
			<< get_sens_id() << "," << get_node_id() << endl;
	}
	return rv;
}
// -------------------------------------------------------------------------
long SensorProp::get_value()
{
//	if (!guipm_check_ready("get value"))
//		return 0;

	if (!check_connector("get value"))
		return 0;

	long rv = 0;
	try
	{
//		rv = (*guipm_ptr_)->getValue( get_sens_id(), get_node_id() );
		rv = connector_ref_->get_value_directly( get_sens_id(), get_node_id() );
	}
	catch (Exception& ex)
	{
		cerr << "(" << owner->get_name() 
		     << "): error while getting value of " 
		     << get_sens_id() << "," << get_node_id() 
		     << ex << endl;
	}
	return rv;
}
// -------------------------------------------------------------------------
void SensorProp::save_value(long value)
{
//	if (!guipm_check_ready("save value"))
//		return;
	if (!check_connector("save value"))
		return;
	try
	{
//		(*guipm_ptr_)->saveValue( value, get_sens_id(), get_node_id() );
		connector_ref_->save_value( value, get_sens_id(), get_node_id() );
	}
	catch (Exception& ex)
	{
		cerr << "(" << owner->get_name() 
		     << "): error while saving value of " 
		     << get_sens_id() << "," << get_node_id() 
		     << ex << endl;
	}
}
// -------------------------------------------------------------------------
bool
SensorProp::check_connector( const Glib::ustring& operation)
{
	if ( !connector_ref_ ) {
		cerr <<  "(" + owner->get_name() + "): ProxyManager not found to " << operation << "." << endl;
		return false;
	}

	if ( !connector_ref_->connected() ) {
		cerr <<  "(" + owner->get_name() + "): ProxyManager is not active to " << operation << "." << endl;
		return false;
	}

	return true;
}
// -------------------------------------------------------------------------
void SensorProp::set_connector(ConnectorRef connector_ref)
{
	if(connector_ref_ == connector_ref)
		return;
	connector_ref_ = connector_ref;
  set_sensor_handler();
}
// -------------------------------------------------------------------------
void SensorProp::set_sensor_handler()
{
  connection.disconnect();
  if (connector_ref_) {
    sigc::slot<void, ObjectId, ObjectId, long> processSensorPropSlot =
        sigc::mem_fun(this, &SensorProp::process_sensor);
        connection = connector_ref_->signals().connect_value_changed(
                                             processSensorPropSlot,
                                             get_sens_id(),
                                                 get_node_id());
	
      info = connector_ref_->get_info( get_sens_id(), get_node_id() );
  }
}
// -------------------------------------------------------------------------
void SensorProp::process_sensor(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value)
{
	if (id == get_sens_id()  && value != current_value)
	{
		current_value = value;
		value_changed.emit( value );
	}
}
// -------------------------------------------------------------------------
