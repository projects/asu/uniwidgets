#ifndef _SENSORPROP_H
#define _SENSORPROP_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
//#include <UniWidgetsTypes.h>
//#include <MessageType.h>
#include <Connector.h>
#include <USignals.h>
#include <cstdlib>
// -------------------------------------------------------------------------
/*!
 * \brief Набор свойств для работы с датчиком.
 * \par
 * Основная задача класса - это добавить к виджету свойства и методы для работы с
 * датчиком. К родительскому классу виджета добавляются следующие свойства:
 * \code
 * 	Glib::Property<std::string> sens_name; // название датчика
 * 	Glib::Property<std::string> node_name; // название узла
 * 	Glib::Property<UniWidgetsTypes::ObjectId> sens_id; // id датчика
 * 	Glib::Property<UniWidgetsTypes::ObjectId> node_id; // id узла
 * 	Glib::Property<std::string> stype; // тип датчика
 * \endcode
 * А также добавляются различные методы для работы с датчиком.
*/
class SensorProp
{
protected:
	float current_value;
	Gtk::Widget* owner;

public:
//	SensorProp(Gtk::Widget* targ, Glib::ustring name, GuiPM** guipm_ptr, std::string uniset_name = "DefaultSensor")  :
	SensorProp(Gtk::Widget* targ, const Glib::ustring& name, ConnectorRef connector_ref, const std::string& uniset_name = "DefaultSensor")  :
	owner(targ)
	,sens_name(*targ, name+"-sensor-name", uniset_name)
	,node_name(*targ, name+"-node-name", "LocalhostNode")
	,sens_id(*targ, name+"-sensor-id", UniWidgetsTypes::DefaultObjectId)
	,node_id(*targ, name+"-node-id", UniWidgetsTypes::DefaultObjectId)
	,stype(*targ,name+"-stype","DI")
//	,guipm_ptr_(guipm_ptr)
	,connector_ref_(connector_ref)
	{}

	~SensorProp(){}

	void set_connector(ConnectorRef connector_ref);						/*!< установить коннектор */
	void set_sens_name(const std::string& txt);							/*!< установить имя для датчика */
	std::string get_sens_name();								/*!< получить имя датчика */

	void set_node_name( const std::string& txt );							/*!< установить имя узла датчика */
	std::string get_node_name();								/*!< получить имя узла датчика */

	void set_sens_id(UniWidgetsTypes::ObjectId id);						/*!< установить id датчика */
	void set_sens_id( const std::string& id ) { set_sens_id(atoi(id.c_str())); }		/*!< установить id датчика с помощью строки */
	UniWidgetsTypes::ObjectId get_sens_id();							/*!< получить id датчика */

	void set_node_id(UniWidgetsTypes::ObjectId id);						/*!< установить id узла */
	void set_node_id( const std::string& id) { set_node_id(atoi(id.c_str())); }		/*!< установить id узла с помощью строки */
	UniWidgetsTypes::ObjectId get_node_id();							/*!< получить id узла */

	void set_stype(const std::string& t);								/*!< установить тип датчика */
	std::string get_stype();								/*!< получить тип датчика */

	float get_current_value() {return current_value;}					/*!< получить текущее значение переменной current_value, выставляемой при обработке сигнала об изменении значении датчика */
	//void sensorInfo(UniWidgetsTypes::SensorMessage* sm);					/*!< обработка сообщений об изменении состояния датчика */
	sigc::signal<void, float> value_changed;						/*!< сигнал об изменении значения датчика */

	bool get_state();									/*!< получить состояние дискретного датчика */
	long get_value();									/*!< получить состояние аналогового датчика */
	void save_value(long value);								/*!< сохранить значение для датчика */
	void process_sensor(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value);	/*!< обработка сообщений об изменении состояния датчика */
	inline UniWidgetsTypes::ObjectInfo get_info(){return info;};				/*!< получить калибровочную информацию для значения датчике */

	Glib::Property<std::string> sens_name;
	Glib::Property<std::string> node_name;
	Glib::Property<UniWidgetsTypes::ObjectId> sens_id;
	Glib::Property<UniWidgetsTypes::ObjectId> node_id;
	Glib::Property<std::string> stype;


private:
	SensorProp();
//	bool guipm_check_ready(Glib::ustring operation);
	bool check_connector( const Glib::ustring& operation);
  void set_sensor_handler();
//	GuiPM** guipm_ptr_;
	ConnectorRef connector_ref_;
	USignals::Connection connection;
	UniWidgetsTypes::ObjectInfo info;
};

#endif
