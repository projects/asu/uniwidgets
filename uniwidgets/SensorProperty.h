#ifndef _SENSORPROPERTY_H
#define _SENSORPROPERTY_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include "types.h"
//#include <MessageType.h>
#include <SensorProp.h>
#include <Connector.h>
// -------------------------------------------------------------------------
class GuiPM;
class USensorProperty;
// -------------------------------------------------------------------------
namespace Glib
{
//TODO write documentation
template <>
class Property<USensorProperty>
{
public:
//	Property(Gtk::Widget& targ, const Glib::ustring& name, GuiPM **guipm_ptr, Glib::ustring uniset_name = "DefaultSensor") :
	Property(Gtk::Widget& targ, const Glib::ustring& name, ConnectorRef connector_ref, Glib::ustring uniset_name = "DefaultSensor") :
	m_property(&targ, name, connector_ref, uniset_name)
//	,sens_name(&targ, name+"-sensor-name")
//	,node_name(&targ, name+"-node-name")
//	,sens_id(&targ, name+"-sensor-id")
//	,node_id(&targ, name+"-node-id")
//	,stype(&targ,name+"-stype")
	{}

	~Property(){}

	void set_sens_name(std::string name) {m_property.set_sens_name(name); }
	void set_node_name(std::string name) {m_property.set_node_name(name); }
	void set_sens_id(UniWidgetsTypes::ObjectId id) {m_property.set_sens_id(id); }
	void set_node_id(UniWidgetsTypes::ObjectId id) {m_property.set_node_id(id); }
	void set_stype(std::string type) {m_property.set_stype(type); }
	UniWidgetsTypes::ObjectId get_sens_id() { return m_property.get_sens_id();}
	Property& operator= (const Property& property) 
	{
		if (this == &property) return *this;
		m_property.sens_name = property.m_property.sens_name.get_value();
		m_property.node_name = property.m_property.node_name.get_value();
		m_property.sens_id = property.m_property.sens_id.get_value();
		m_property.node_id = property.m_property.node_id.get_value();
		m_property.stype = property.m_property.stype.get_value();
		return *this;
	}

	float get_current_value() {return m_property.get_current_value();}

	SensorProp m_property;
/*	PropertyProxy<std::string> sens_name;
	PropertyProxy<std::string> node_name;
	PropertyProxy<UniWidgetsTypes::ObjectId> sens_id;
	PropertyProxy<UniWidgetsTypes::ObjectId> node_id;
	PropertyProxy<std::string> stype;*/
protected:

private:
	Property();

};

//temporary solution
template <>
class PropertyProxy<USensorProperty>
{
public:
	PropertyProxy(Property<USensorProperty> property) :
		property_(&property.m_property)
	{}

	~PropertyProxy() {}

	void set_sens_name(std::string name) {property_->set_sens_name(name); }
	void set_node_name(std::string name) {property_->set_node_name(name); }
	void set_sens_id(UniWidgetsTypes::ObjectId id) {property_->set_sens_id(id); }
	void set_node_id(UniWidgetsTypes::ObjectId id) {property_->set_node_id(id); }
	void set_stype(std::string type) {property_->set_stype(type); }
	UniWidgetsTypes::ObjectId get_sens_id() { return property_->get_sens_id();}

	PropertyProxy& operator=(const Property<USensorProperty>& property) 
	{
		//if (this == &property) return *this;
		property_->sens_name = property.m_property.sens_name.get_value();
		property_->node_name = property.m_property.node_name.get_value();
		property_->sens_id = property.m_property.sens_id.get_value();
		property_->node_id = property.m_property.node_id.get_value();
		property_->stype = property.m_property.stype.get_value();
		return *this;
	}

	float get_current_value() {return property_->get_current_value();}

	SensorProp* property_;
protected:

private:
	PropertyProxy();

};
}
#endif
