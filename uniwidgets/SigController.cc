#include "USignals.h"
#include "SigControllerImpl.h"
#include <glibmm/ustring.h>
#include <glibmm/convert.h>
#include <map>
#include <ctime>
#include <sys/time.h>
#include <math.h>
#include <bitset>
#include <unordered_map>
#undef atoi
// -------------------------------------------------------------------------
using namespace UniWidgetsTypes;
using namespace std;
using namespace UMessages;
// -------------------------------------------------------------------------
namespace UMessages
{

MessageId
Message::getMessageId() const
{
	return id_;
}
// -------------------------------------------------------------------------
time_t
Message::getLastTime() const
{
	return _item->_last_time;
}
// -------------------------------------------------------------------------
time_t
Message::getLastTimeNsec() const
{
	return _item->_time_nsec;
}
// -------------------------------------------------------------------------
const Glib::ustring
Message::getText() const
{
	return _item->get_first_text();
}
// -------------------------------------------------------------------------
const std::list<Glib::ustring>&
Message::getTextList() const
{
	return _item->_text;
}
// -------------------------------------------------------------------------
int
Message::getMessageType() const
{
	return _item->_mtype;
}
// -------------------------------------------------------------------------
bool
Message::valid() const
{
	return ( _item != NULL );
}

} //namespace Message
// -------------------------------------------------------------------------
namespace USignals
{

SigController::SigController(SigControllerImpl* impl)
{
	if(!impl)
		throw UniWidgetsTypes::Exception("Wrong SigControllerImpl ptr!");
	_impl = impl;
}
// -------------------------------------------------------------------------
SigController::~SigController()
{
	delete _impl;
}
// -------------------------------------------------------------------------
Connection
SigController::connect_value_changed( const ValueChangedSlot& slot,
		ObjectId id, ObjectId node)
{
	return	_impl->connect_value_changed(slot,id,node);
}
// -------------------------------------------------------------------------
Connection
SigController::connect_analog_value_changed( const AnalogValueChangedSlot& slot,
		ObjectId id, ObjectId node)
{
	return	_impl->connect_analog_value_changed(slot,id,node);
}
// -------------------------------------------------------------------------
VConn
SigController::connect_value_in( const ValueInOutSlot& slot,
		ObjectId id,
		ObjectId node,
		long value )

{
	return	_impl->connect_value_in(slot,id,node,value);
}
// -------------------------------------------------------------------------
VConn
SigController::connect_value_out( const ValueInOutSlot& slot,
		ObjectId id,
		ObjectId node,
		long value )

{
	return	_impl->connect_value_out(slot,id,node,value);
}
// -------------------------------------------------------------------------
Connection
SigController::connect_on_any_message( const MessageSlot& slot )
{
	return _impl->connect_on_any_message(slot);
}
// -------------------------------------------------------------------------
Connection
SigController::connect_on_any_message_full( const FullMessageSlot& slot )
{
	return _impl->connect_on_any_message_full(slot);
}
// -------------------------------------------------------------------------
Connection
SigController::connect_on_message( const MessageSlot& slot,
		MessageId id)
{
	return _impl->connect_on_message( slot, id);
}
// -------------------------------------------------------------------------
UMessages::Message
SigController::get_message( const UMessages::MessageId& id )
{
	return _impl->get_message( id );
}
// -------------------------------------------------------------------------
std::list<UMessages::Message >
SigController::get_message_list( const UMessages::MessageId& id )
{
	return _impl->get_message_list( id );
}
// -------------------------------------------------------------------------
std::list<UMessages::Message >
SigController::get_all_messages_list()
{
	return _impl->get_all_messages_list();
}
// -------------------------------------------------------------------------
long
SigController::get_value(ObjectId id, ObjectId node)
{
	return _impl->get_value(id, node);
}
// -------------------------------------------------------------------------
float
SigController::get_analog_value(ObjectId id, ObjectId node)
{
	return _impl->get_analog_value(id, node);
}
// -------------------------------------------------------------------------
std::string
SigController::get_info(int signal_num)
{
	return _impl->get_info(signal_num);
}
// -------------------------------------------------------------------------
void
SigController::connect_get_info_slot(const GetInfoSlot& sl, const std::string& c, const std::string& i)
{
	_impl->connect_get_info_slot(sl, c, i);
}
// -------------------------------------------------------------------------
void
SigController::on_connect()
{
	_impl->on_connect();
}
// -------------------------------------------------------------------------
void
VConn::disconnect()
{
	conn.disconnect();
}

} //namespace USignals
