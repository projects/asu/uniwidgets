#include "SigControllerImpl.h"
#include <glibmm/ustring.h>
#include <glibmm/convert.h>
#include <map>
#include <ctime>
#include <sys/time.h>
#include <math.h>
#include <bitset>
#include <unordered_map>
#undef atoi
// -------------------------------------------------------------------------
namespace USignals
{

void SensorMapItem::value_map_add_item(ValueKey key, ValueMapItem* item)
{
	ValueMap::iterator vit = _value_map.find(key);
	if(vit==_value_map.end())
	{
		std::cout<<"value_map_add_item() key.value1="<<key.value1<<" key.value2="<<key.value2<<std::endl;
		_value_map[key] = item;
	}
	else
	{
		while(vit!=_value_map.end())
		{
			std::cout<<"value_map_add_item() key.value1="<<key.value1<<" key.value2="<<key.value2<<" find.value1="<<vit->first.value1<<" find.value2="<<vit->first.value2;
			ValueMapItem* find_vitem = new ValueMapItem(*vit->second);
			long find_value1	 = vit->first.value1;
			long find_value2	 = vit->first.value2;
			long min_find_value	 = MIN(find_value1,find_value2);
			long max_find_value	 = MAX(find_value1,find_value2);
			long min_key_value	 = MIN(key.value1,key.value2);
			long max_key_value	 = MAX(key.value1,key.value2);
			
			//найденое множество полностью включает новое множество
			if(vit->first == key.value1 && vit->first == key.value2)
			{
				std::cout<<" if=1"<<std::endl;
				_value_map.erase(vit);
				
				if(min_key_value > min_find_value)
				{
					_value_map[ValueKey(min_find_value,min_key_value-1)] = find_vitem;
				}
				
				ValueMapItem* input_vitem = new ValueMapItem(*item);
				for(std::list<Glib::ustring>::iterator text_it = find_vitem->_text.begin();text_it != find_vitem->_text.end();++text_it)
					input_vitem->_text.push_back(*text_it);
				_value_map[key] = input_vitem;
				
				_value_map[ValueKey(max_key_value+1,max_find_value)] = new ValueMapItem(*find_vitem);
				return;
			}
			//новое множество полностью включает найденое множество
			else if(key == find_value1 && key == find_value2)
			{
				std::cout<<" if=2"<<std::endl;
				_value_map.erase(vit);
				
				if(min_find_value > min_key_value)
				{
					_value_map[ValueKey(min_key_value,min_find_value-1)] = new ValueMapItem(*item);
				}
				
				ValueMapItem* input_vitem = new ValueMapItem(*find_vitem);
				for(std::list<Glib::ustring>::iterator text_it = item->_text.begin();text_it != item->_text.end();++text_it)
					input_vitem->_text.push_back(*text_it);
				_value_map[ValueKey(min_find_value,max_find_value)] = input_vitem;
				
				key.value1 = max_find_value+1;
				key.value2 = max_key_value;
			}
			//новое множество частично включает найденое множество с начала
			else if(key == max_find_value && !(key == min_find_value))
			{
				std::cout<<" if=3"<<std::endl;
				_value_map.erase(vit);
				
				_value_map[ValueKey(min_find_value,min_key_value-1)] = new ValueMapItem(*find_vitem);

				ValueMapItem* input_vitem = new ValueMapItem(*item);
				for(std::list<Glib::ustring>::iterator text_it = find_vitem->_text.begin();text_it != find_vitem->_text.end();++text_it)
					input_vitem->_text.push_back(*text_it);
				_value_map[ValueKey(min_key_value,max_find_value)] = input_vitem;
				
				key.value1 = max_find_value+1;
				key.value2 = max_key_value;
			}
			//новое множество частично включает найденое множество с конца
			else if(!(key == max_find_value) && key == min_find_value)
			{
				std::cout<<" if=4"<<std::endl;
				_value_map.erase(vit);
				
				_value_map[ValueKey(min_key_value,min_find_value-1)] = new ValueMapItem(*item);
				
				ValueMapItem* input_vitem = new ValueMapItem(*item);
				for(std::list<Glib::ustring>::iterator text_it = find_vitem->_text.begin();text_it != find_vitem->_text.end();++text_it)
					input_vitem->_text.push_back(*text_it);
				_value_map[ValueKey(min_find_value,max_key_value)] = input_vitem;
				
				key.value1 = max_key_value+1;
				key.value2 = max_find_value;
			}
			vit = _value_map.find(key);
			delete find_vitem;
		};
		_value_map[key] = item;
	}
}
// -------------------------------------------------------------------------
SigControllerImpl::SigControllerImpl() : _signal_any_message("signal_any_message"), _signal_any_message_full("signal_any_message_full")
{
}
// -------------------------------------------------------------------------
SensorMapItem*
SigControllerImpl::addMessage( UMessages::MessageId id, Glib::ustring text, int mtype)
{
	const SensorKey sk(id._id, id._node);
	SensorMap::iterator it = _sensor_map.find(sk);
	SensorMapItem* item;
	if ( it != _sensor_map.end() )
		item = it->second;
	else {
		item = new SensorMapItem(id._id);
		_sensor_map[sk] = item;
	}
// 	ValueMap::iterator v_it = item->find_in_value_map( id._value );
// 	ValueMapItem* vitem;
// 	if ( v_it != item->get_value_map_end() )
// 		vitem = v_it->second;
// 	else
// 	{
// 		vitem = new ValueMapItem;
// 		item->value_map_add_item(id._value, vitem);
// 	}

	ValueMapItem* vitem = new ValueMapItem(id._id, id._value);
	vitem->_is_message = true;
	vitem->_text.push_back(text);
	vitem->_mtype = mtype;
	//TODO: check time
//	if(gpm_->connected()) {
//		IOController_i::ShortIOInfo inf = gpm_->uin->getChangedTime(id._id,id._node);
//		vitem->_last_time = inf.tv_sec;
//	}
//	else
		vitem->_last_time = 0;
	item->value_map_add_item(id._value, vitem);

	item->msg_for_bits = id._bits_msg;
	item->msg_groups = id._msg_groups;
	return item;
}

} //namespace USignals
