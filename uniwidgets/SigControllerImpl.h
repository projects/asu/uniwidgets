#ifndef _SIGCONTROLLERIMPL_H
#define _SIGCONTROLLERIMPL_H
// -------------------------------------------------------------------------
#include <sigc++/sigc++.h>
#include <types.h>
#include <glibmm/ustring.h>
#include <glibmm/convert.h>
#include <ctime>
#include "USignals.h"
// -------------------------------------------------------------------------
namespace USignals
{

typedef UMessages::ValueRange ValueKey;
typedef std::map<ValueKey, ValueMapItem*> ValueMap;
// -------------------------------------------------------------------------
//SensorMap
struct SensorKey
{
	SensorKey ()
		: _id(UniWidgetsTypes::DefaultObjectId),
		  _node(UniWidgetsTypes::DefaultObjectId) {}
	SensorKey ( UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node)
		: _id(id), _node(node) {}
	UniWidgetsTypes::ObjectId _id;
	UniWidgetsTypes::ObjectId _node;

	bool operator<(const SensorKey& y) const
	{ return (_id < y._id || (_id == y._id && _node < y._node)); }
};
// -------------------------------------------------------------------------
struct SensorMapItem
{
	SensorMapItem (UniWidgetsTypes::ObjectId id = UniWidgetsTypes::DefaultObjectId):
		_last_value(0),
		_last_analog_value(0),
		_value_changed("changed"),
		_analog_value_changed("ai_changed"),
		show_any_value(false),
		msg_for_bits(false),
		msg_groups(""),
		default_msg_text("")
		{
			std::ostringstream os;
			os << "changed(" << id << ")";
			_value_changed.set_name(os.str());
			_analog_value_changed.set_name("ai_" + os.str());
		}
	long _last_value;
	float _last_analog_value;
	ValueChangedSignal _value_changed;
	AnalogValueChangedSignal _analog_value_changed;
	bool show_any_value;
	bool msg_for_bits;
	Glib::ustring msg_groups;
	Glib::ustring default_msg_text;
	
	ValueMap::iterator get_value_map_end()
	{
		return _value_map.end();
	}
	ValueMap::iterator get_value_map_begin()
	{
		return _value_map.begin();
	}
	ValueMap::iterator find_in_value_map(ValueKey key)
	{
		return _value_map.find(key);
	}
	ValueMap::iterator find_in_value_map(long key)
	{
		return _value_map.find(key);
	}
	// ---обработка добавления нового множества---
	void value_map_add_item(ValueKey key, ValueMapItem* item);
	
private:
	ValueMap _value_map;
};
// -------------------------------------------------------------------------
typedef std::map<SensorKey, SensorMapItem*> SensorMap;
//end SensorMap
/*!
 * \brief Класс реализующий управление сигналами от датчиков.
 * \par
 * Данный класс реализует методы управления сигналами изменения значения, состояния датчиков
 * ,а также заказа датчиков из Sharedmemory.
*/
class SigControllerImpl
{
public:
	SigControllerImpl();
	/*! назначить обработчик сигнала об изменении значения датчика */
	virtual Connection connect_value_changed( const ValueChangedSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node ) = 0;
	/*! назначить обработчик сигнала об изменении аналогового значения датчика */
	virtual Connection connect_analog_value_changed(const AnalogValueChangedSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node) = 0;
	/*! назначить обработчик сигнала об изменении датчика в заданное значение */
	virtual VConn connect_value_in( const ValueInOutSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value) = 0;
	/*! назначить обработчик сигнала об изменении заданного значения датчика */
	virtual VConn connect_value_out( const ValueInOutSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value ) = 0;
	/*! назначить обработчик сигнала об изменении заданного значения датчика */
	virtual VConn connect_value_in_out( const ValueInOutSlot& slot,
			UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value, bool on) = 0;
	/*! назначить обработчик на приходящее сообщение от любого датчика */
	virtual Connection connect_on_any_message( const MessageSlot& slot) = 0;
	/*! назначить обработчик на приходящее сообщение от заданного датчика */
	virtual Connection connect_on_message( const MessageSlot& slot, UMessages::MessageId id) = 0;
	/*! назначить обработчик на приходящее сообщение от любого датчика(расширенное количество параметров) */
	virtual Connection connect_on_any_message_full ( const FullMessageSlot& slot ) = 0;
	/*! получить объект класса Message, в котором описан сигнал для id.
	    Если для сигнала задано поле mtype, то для этого сигнала хранится
	    описание с параметрами сигнала, временем срабатывания и различными
	    сигналами(см. USignals::ValueMapItem) т.е. это сигнал АПС и он должен
	    отображаться в журнале и квитироваться, если это предусмотрено. Этот
	    метод применяется, например, когда нужно повесить обработчик сообщения на
	    конкретный датчик и конкретное значение */
	virtual UMessages::Message get_message( const UMessages::MessageId& id) = 0;
	/*! получить объекты класса Message, в котором описаны сигнал для id.
	    Этот метод выполняет такую же задачу как и UMessages::Message get_message,
	    только в отличие от него работает с аналоговыми датчиками, у которых есть
	    "MessageList" в описании(configure.xml). Данная функция возвращает описание
	    для каждого пункта MessageList. Применяется метод, когда нужно повесить
	    обработчик сообщений для нескольких значений одного и того же датчика. */
	virtual std::list<UMessages::Message > get_message_list( const UMessages::MessageId& id) = 0;
	/*! запросить все датчики */
	virtual std::list<UMessages::Message > get_all_messages_list() = 0;
	/*! запросить статистику о сигналах*/
	virtual std::string get_info(int signal_num = 0) = 0;
	/*! подключить слот для выводу своей статистики*/
	inline void connect_get_info_slot( const GetInfoSlot& sl, const std::string& component, const std::string& info )
	{
		int i = _info_slots.size();
		GetInfoSlotItem& it = _info_slots[i];
		it.component = component;
		it.info = info;
		it.callback = sl;
	}
	virtual void on_connect() = 0;
	virtual void on_disconnect() throw() = 0;
	/*! получить значение для датчика */
	virtual long get_value(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node) throw() = 0;
	/*! получить значение для аналогового датчика */
	virtual float get_analog_value(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node) throw() = 0;

protected:
	SensorMapItem* addMessage(UMessages::MessageId id, Glib::ustring text, int mtype);
	SensorMap _sensor_map;
	MessageSignal _signal_any_message;
	FullMessageSignal _signal_any_message_full;
	GetInfoSlots _info_slots;
};

} //namespace USignals
#endif
