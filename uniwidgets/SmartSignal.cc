#include "SmartSignal.h"
#include "USignals.h"
#include <sys/time.h> // gor gettimeofday
// -------------------------------------------------------------------------
#define CALC_TIME(time, count) ( (count==0) ? 0 : (double)time / count / 1000000 )

// -------------------------------------------------------------------------
bool SignalInfoHolder::debug = true;
const unsigned long SignalInfoHolder::max_time = 60 * 60 * 1000 * 1000; // 60 минут
time_t SignalInfoHolder::long_time = 10 * 60; // 10 минут
time_t SignalInfoHolder::short_time = 1 * 60; // 1 минут
// -------------------------------------------------------------------------
SignalInfoHolder::Period::Period(time_t size):
	size_sec(size),
	size_usec(size * 1000000),
	start(0),
	counter(0),
	count(0),
	time(0)
{
}
// -------------------------------------------------------------------------
void SignalInfoHolder::Period::reset(time_t& current)
{
	start = current;
	counter = 0;
	count = 0;
	time = 0;
}
// -------------------------------------------------------------------------
void SignalInfoHolder::Period::update(time_t& current, unsigned long& eval_usec)
{
	counter++;
	time += eval_usec;
	time_t temp_time = current - start;
	if( temp_time > size_sec )
	{
		start = current;
		count = counter;
		counter = 0;
	}
}
// -------------------------------------------------------------------------
void SignalInfoHolder::get_current_time(struct timeval& tm)
{
	// текущее время
	tm.tv_sec = 0;
	tm.tv_usec = 0;
	gettimeofday(&tm,NULL);
}
// -------------------------------------------------------------------------
SignalInfoHolder::SignalInfoHolder():
	long_period(long_time),
	short_period(short_time)
{
	reset();
}
// -------------------------------------------------------------------------
bool SignalInfoHolder::overflow()
{
	return time > max_time;
}
// -------------------------------------------------------------------------
void SignalInfoHolder::reset()
{
	struct timeval tm;
	get_current_time(tm);
	start = tm.tv_sec;
	count = 0;
	time = 0;
	long_period.reset(tm.tv_sec);
	short_period.reset(tm.tv_sec);
}
// -------------------------------------------------------------------------
void SignalInfoHolder::update(time_t& sec, unsigned long& eval_usec)
{
	count++;
	time += eval_usec;
	long_period.update(sec, eval_usec);
	short_period.update(sec, eval_usec);
}
// -------------------------------------------------------------------------
std::string SignalInfoHolder::info()
{
	std::ostringstream tmpos;
	tmpos.setf(std::ios_base::fixed, std::ios_base::floatfield);
	tmpos.precision(6);
	struct timeval tm;
	get_current_time(tm);
	tmpos << "long_period = " << SignalInfoHolder::long_time  << "sec;  short_period = " << SignalInfoHolder::short_time << "sec;  elapsed_time = " << (tm.tv_sec - start) << "sec" << std::endl;
	tmpos << std::setw(90) << " | "  << std::setw(13) << "count";
	tmpos << " | "  << std::setw(16) << "evaluate time";
	tmpos << " | "  << std::setw(15) << "average time";
	tmpos << " | "  << std::setw(13) << "average time(" << std::setw(4) << SignalInfoHolder::long_time << ")";
	tmpos << " | "  << std::setw(13) << "average time(" << std::setw(4) << SignalInfoHolder::short_time << ")";
	tmpos << " | "  << std::setw(15) << "period";
	tmpos << " | "  << std::setw(13) << "period(" << std::setw(4) << SignalInfoHolder::long_time << ")";
	tmpos << " | "  << std::setw(13) << "period(" << std::setw(4) << SignalInfoHolder::short_time << ")";
	return tmpos.str();
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, const SignalInfoHolder& t )
{
	struct timeval tm;
	SignalInfoHolder::get_current_time(tm);
	unsigned long elapsed_time = (tm.tv_sec - t.start) * 1000000;
	std::ostringstream tmpos;
	tmpos.setf(std::ios_base::fixed, std::ios_base::floatfield);
	tmpos.precision(6);
	tmpos << " | c = " << std::setw(9) << t.count;
	tmpos << " | et = " << std::setw(11) << (double)t.time / 1000000;
	tmpos << " | at = " << std::setw(10) << CALC_TIME(t.time, t.count);
	tmpos << " | at(l) = " << std::setw(10) << CALC_TIME(t.long_period.time, t.long_period.count);
	tmpos << " | at(s) = " << std::setw(10) << CALC_TIME(t.short_period.time, t.short_period.count);
	tmpos << " | p = " << std::setw(11) << CALC_TIME(elapsed_time, t.count);
	tmpos << " | p(l) = " << std::setw(11) << CALC_TIME(t.long_period.size_usec, t.long_period.count);
	tmpos << " | p(s) = " << std::setw(11) << CALC_TIME(t.short_period.size_usec, t.short_period.count);
	return os << tmpos.str();
}
// -------------------------------------------------------------------------
std::size_t std::hash<UMessages::MessageId>::operator()(UMessages::MessageId const& s) const
{
	return std::hash<UniWidgetsTypes::ObjectId>()(s._id);
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, const UMessages::MessageId& t )
{
	std::ostringstream tmpos;
	//tmpos << UniWidgetsTypes::uniset_conf()->oind->getBaseName( UniWidgetsTypes::uniset_conf()->oind->getNameById(t._id) );
	tmpos << "(" << std::setw(6) << t._id << ")";
	return os << tmpos.str();
}
// -------------------------------------------------------------------------
template<>
std::ostream& operator<<( std::ostream& os, const SignalInfoKey<UniWidgetsTypes::ObjectId>& t )
{
	std::ostringstream tmpos;
	//tmpos << UniWidgetsTypes::uniset_conf()->oind->getBaseName( UniWidgetsTypes::uniset_conf()->oind->getNameById(t.key) );
	tmpos << "(" << std::setw(6) << t.key << ")";
	return os << tmpos.str();
}
// -------------------------------------------------------------------------
