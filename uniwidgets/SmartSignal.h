#ifndef _SMARTSIGNAL_H
#define _SMARTSIGNAL_H
// -------------------------------------------------------------------------
#include <sstream>
#include <sigc++/sigc++.h>
#include <iostream>
#include <iomanip>
#include <unordered_map>
#include "types.h"
//#include <UniWidgetsTypes.h>
//#include <MessageType.h>
// -------------------------------------------------------------------------
class SignalInfoHolder
{
	public:
		SignalInfoHolder();
		~SignalInfoHolder(){};
		static bool debug;
		inline static void set_long_time(int sec){ if( sec > 0 ) long_time = sec; };
		inline static void set_short_time(int sec){ if( sec > 0 ) short_time = sec; };
		static void get_current_time(struct timeval& tm);
		bool overflow();
		void reset();
		void update(time_t& sec, unsigned long& eval_usec);
		std::string info();
	private:
		friend std::ostream& operator<<( std::ostream& os, const SignalInfoHolder& t );
		static const unsigned long max_time; // контроль переполнения unsigned long в микросекундах
		static time_t long_time; // длительный период в секундах
		static time_t short_time; // короткий период в секундах
		time_t start; // время создания SignalInfoHolder в секундах
		unsigned long count; // количество вызовов emit()
		unsigned long time; // общее время обработки emit() в микросекундах
		struct Period
		{
			Period( time_t size );
			void reset(time_t& current);
			void update(time_t& current, unsigned long& eval_usec);
			time_t start; // метка начала периода в секундах
			time_t size_sec; // длительность периода в секундах
			unsigned long size_usec; // длительность периода в микросекундах
			unsigned long counter; // счетчик
			unsigned long count; // количество вызовов emit() за последний расчитанный период
			unsigned long time; // среднее время между emit() за последний расчитанный период в микросекундах;
		};
		Period long_period; // длительный период
		Period short_period; // короткий период
};
std::ostream& operator<<( std::ostream& os, const SignalInfoHolder& t );
// -------------------------------------------------------------------------
template<class T_arg1>
class SignalInfoKey
{
	public:
		SignalInfoKey(T_arg1 arg1):key(arg1){};
		~SignalInfoKey(){};
		bool operator<(const SignalInfoKey<T_arg1>& r) const{ return key < r.key; };
		bool operator==(const SignalInfoKey<T_arg1>& r) const{ return key == r.key; };
	private:
		friend class std::hash<SignalInfoKey<T_arg1> >;
		template<class T_func_arg1> friend std::ostream& operator<<(std::ostream& os, const SignalInfoKey<T_func_arg1>& t);
		T_arg1 key;
};
// -------------------------------------------------------------------------
namespace std
{
	template<class T_arg1>
	struct hash<SignalInfoKey<T_arg1> >
	{
		size_t operator()(const SignalInfoKey<T_arg1>& s) const
		{
			return std::hash<T_arg1>()(s.key);
		}
	};
}
// -------------------------------------------------------------------------
template<class T_arg1>
class SignalInfoKey<T_arg1*>
{
	public:
		SignalInfoKey(T_arg1* arg1):key(*arg1){};
		~SignalInfoKey(){};
		bool operator<(const SignalInfoKey<T_arg1*>& r) const{ return key < r.key; };
		bool operator==(const SignalInfoKey<T_arg1*>& r) const{ return key == r.key; };
	private:
		friend class std::hash<SignalInfoKey<T_arg1*> >;
		template<class T_func_arg1> friend std::ostream& operator<<(std::ostream& os, const SignalInfoKey<T_func_arg1>& t);
		T_arg1 key;
};
// -------------------------------------------------------------------------
namespace std
{
	template<class T_arg1>
	struct hash<SignalInfoKey<T_arg1*> >
	{
		size_t operator()(const SignalInfoKey<T_arg1*>& s) const
		{
			return std::hash<T_arg1>()(s.key);
		}
	};
}
// -------------------------------------------------------------------------
template<class T_arg1>
std::ostream& operator<<( std::ostream& os, const SignalInfoKey<T_arg1>& t )
{
	return os << t.key;
}
// -------------------------------------------------------------------------
template<class T_key >
class SmartSignalBase 
{
	public:
		SmartSignalBase(std::string n, sigc::signal_base* s_ptr) : name(n), signal_ptr(s_ptr) {};
		~SmartSignalBase(){};
		std::string get_info();
		std::string get_short_info();
		void set_name(std::string n){ name = n; };
	protected:
		void preemit();
		void postemit(const T_key&);
	private:
		SmartSignalBase(){};
		std::string name;
		sigc::signal_base* signal_ptr;
		SignalInfoHolder info;
		std::unordered_map<SignalInfoKey<T_key>, SignalInfoHolder> info_map;
		struct timeval emit_tm;
};
// -------------------------------------------------------------------------
template<class T_key>
void SmartSignalBase<T_key>::preemit()
{
	if( info.overflow() )
	{
		info.reset();
		for(auto&& it: info_map)
			it.second.reset();
	}
	SignalInfoHolder::get_current_time(emit_tm);
}
// -------------------------------------------------------------------------
template<class T_key>
void SmartSignalBase<T_key>::postemit(const T_key& key)
{
	struct timeval tm;
	SignalInfoHolder::get_current_time(tm);
	unsigned long eval_time = (tm.tv_sec - emit_tm.tv_sec) * 1000000 + tm.tv_usec - emit_tm.tv_usec;
	SignalInfoHolder& imap = info_map[key];
	info.update(tm.tv_sec, eval_time);
	imap.update(tm.tv_sec, eval_time);
}
// -------------------------------------------------------------------------
template<class T_key>
std::string SmartSignalBase<T_key>::get_info()
{
	std::ostringstream inf;
	if( SignalInfoHolder::debug )
	{
		inf << std::setw(25) << name << ":  slots=" << signal_ptr->size() << ";  " << info.info() << std::endl;
		if( signal_ptr->size() > 0)
		{
			if( info_map.size() == 0 )
				inf << std::setw(25) << name << "(" << std::setw(5) << signal_ptr->size() << "):  " << std::setw(50) << "no emitions" << ": " << info << std::endl;
			else if( info_map.size() > 1 )
				inf << std::setw(25) << name << "(" << std::setw(5) << signal_ptr->size() << "):  " << std::setw(50) << "all emitions" << ": " << info << std::endl;
			for(auto&& it: info_map)
				inf << std::setw(25) << name << "(" << std::setw(5) << signal_ptr->size() << "):  " << std::setw(50) << it.first << ": " << it.second << std::endl;
		}
	}
	else
	{
		inf << name << ": debug mode is off" << std::endl;
	}
	return inf.str();
}
// -------------------------------------------------------------------------
template<class T_key>
std::string SmartSignalBase<T_key>::get_short_info()
{
	if( !SignalInfoHolder::debug )
		return "";
	if( signal_ptr->size() == 0)
		return "";
	std::ostringstream inf;
	if( info_map.size() == 0 )
		inf << std::setw(25) << name << "(" << std::setw(5) << signal_ptr->size() << "):  " << std::setw(50) << "no emitions" << ": " << info << std::endl;
	else if( info_map.size() > 1 )
		inf << std::setw(25) << name << "(" << std::setw(5) << signal_ptr->size() << "):  " << std::setw(50) << "all emitions" << ": " << info << std::endl;
	for(auto&& it: info_map)
		inf << std::setw(25) << name << "(" << std::setw(5) << signal_ptr->size() << "):  " << std::setw(50) << it.first << ": " << it.second << std::endl;
	return inf.str();
}
#warning Сделать специализацию для сигналов возращающих void
// -------------------------------------------------------------------------
template<class T_return, class T_arg1>
class SmartSignal1 : public SmartSignalBase<T_arg1>
{
	public:
		SmartSignal1(std::string n) : SmartSignalBase<T_arg1>(n, &signal) {};
		~SmartSignal1(){};
		typedef typename sigc::signal<T_return, T_arg1>::result_type result_type;
		result_type emit(const T_arg1&);
		sigc::connection connect( const sigc::slot<T_return, T_arg1>& slot){ return signal.connect(slot); };
	private:
		SmartSignal1(){};
		sigc::signal<T_return, T_arg1> signal;
};
// -------------------------------------------------------------------------
template<class T_return, class T_arg1>
typename SmartSignal1<T_return, T_arg1>::result_type SmartSignal1<T_return, T_arg1>::emit(const T_arg1& arg1)
{
	if(SignalInfoHolder::debug)
	{
		SmartSignalBase<T_arg1>::preemit();
		signal.emit(arg1);
		SmartSignalBase<T_arg1>::postemit(arg1);
	}
	else
	{
		signal.emit(arg1);
	}
}
// -------------------------------------------------------------------------
template<class T_return, class T_arg1, class T_arg2>
class SmartSignal2 : public SmartSignalBase<T_arg1>
{
	public:
		SmartSignal2(std::string n) : SmartSignalBase<T_arg1>(n, &signal) {};
		~SmartSignal2(){};
		typedef typename sigc::signal<T_return, T_arg1, T_arg2>::result_type result_type;
		result_type emit(const T_arg1&, const T_arg2&);
		sigc::connection connect( const sigc::slot<T_return, T_arg1, T_arg2>& slot){ return signal.connect(slot); };
	private:
		SmartSignal2(){};
		sigc::signal<T_return, T_arg1, T_arg2> signal;
};
// -------------------------------------------------------------------------
template<class T_return, class T_arg1, class T_arg2>
typename SmartSignal2<T_return, T_arg1, T_arg2>::result_type SmartSignal2<T_return, T_arg1, T_arg2>::emit(const T_arg1& arg1, const T_arg2& arg2)
{
	if(SignalInfoHolder::debug)
	{
		SmartSignalBase<T_arg1>::preemit();
		signal.emit(arg1, arg2);
		SmartSignalBase<T_arg1>::postemit(arg1);
	}
	else
	{
		signal.emit(arg1, arg2);
	}
}
// -------------------------------------------------------------------------
template<class T_return, class T_arg1, class T_arg2, class T_arg3>
class SmartSignal3 : public SmartSignalBase<T_arg1>
{
	public:
		SmartSignal3(std::string n) : SmartSignalBase<T_arg1>(n, &signal) {};
		~SmartSignal3(){};
		typedef typename sigc::signal<T_return, T_arg1, T_arg2, T_arg3>::result_type result_type;
		result_type emit(const T_arg1&, const T_arg2&, const T_arg3&);
		sigc::connection connect( const sigc::slot<T_return, T_arg1, T_arg2, T_arg3>& slot){ return signal.connect(slot); };
	private:
		SmartSignal3(){};
		sigc::signal<T_return, T_arg1, T_arg2, T_arg3> signal;
};
// -------------------------------------------------------------------------
template<class T_return, class T_arg1, class T_arg2, class T_arg3>
typename SmartSignal3<T_return, T_arg1, T_arg2, T_arg3>::result_type SmartSignal3<T_return, T_arg1, T_arg2, T_arg3>::emit(const T_arg1& arg1, const T_arg2& arg2, const T_arg3& arg3)
{
	if(SignalInfoHolder::debug)
	{
		SmartSignalBase<T_arg1>::preemit();
		signal.emit(arg1, arg2, arg3);
		SmartSignalBase<T_arg1>::postemit(arg1);
	}
	else
	{
		signal.emit(arg1, arg2, arg3);
	}
}
// -------------------------------------------------------------------------
template<class T_return, class T_arg1, class T_arg2, class T_arg3, class T_arg4>
class SmartSignal4 : public SmartSignalBase<T_arg1>
{
	public:
		SmartSignal4(std::string n) : SmartSignalBase<T_arg1>(n, &signal) {};
		~SmartSignal4(){};
		typedef typename sigc::signal<T_return, T_arg1, T_arg2, T_arg3, T_arg4>::result_type result_type;
		result_type emit(const T_arg1&, const T_arg2&, const T_arg3&, const T_arg4&);
		sigc::connection connect( const sigc::slot<T_return, T_arg1, T_arg2, T_arg3, T_arg4>& slot){ return signal.connect(slot); };
	private:
		SmartSignal4(){};
		sigc::signal<T_return, T_arg1, T_arg2, T_arg3, T_arg4> signal;
};
// -------------------------------------------------------------------------
template<class T_return, class T_arg1, class T_arg2, class T_arg3, class T_arg4>
typename SmartSignal4<T_return, T_arg1, T_arg2, T_arg3, T_arg4>::result_type SmartSignal4<T_return, T_arg1, T_arg2, T_arg3, T_arg4>::emit(const T_arg1& arg1, const T_arg2& arg2, const T_arg3& arg3, const T_arg4& arg4)
{
	if(SignalInfoHolder::debug)
	{
		SmartSignalBase<T_arg1>::preemit();
		signal.emit(arg1, arg2, arg3, arg4);
		SmartSignalBase<T_arg1>::postemit(arg1);
	}
	else
	{
		signal.emit(arg1, arg2, arg3, arg4);
	}
}
// -------------------------------------------------------------------------
template<class T_return, class T_arg1 = sigc::nil, class T_arg2 = sigc::nil, class T_arg3 = sigc::nil, class T_arg4 = sigc::nil>
class SmartSignal : public SmartSignal4<T_return, T_arg1, T_arg2, T_arg3, T_arg4>
{
	public:
		SmartSignal(std::string n = "SmartSignal<T_return, T_arg1, T_arg2, T_arg3, T_arg4>") : SmartSignal4<T_return, T_arg1, T_arg2, T_arg3, T_arg4>(n) {};
};
// -------------------------------------------------------------------------
template<class T_return, class T_arg1, class T_arg2, class T_arg3>
class SmartSignal<T_return, T_arg1, T_arg2, T_arg3, sigc::nil> : public SmartSignal3<T_return, T_arg1, T_arg2, T_arg3>
{
	public:
		SmartSignal(std::string n = "SmartSignal<T_return, T_arg1, T_arg2, T_arg3>") : SmartSignal3<T_return, T_arg1, T_arg2, T_arg3>(n) {};
};
// -------------------------------------------------------------------------
template<class T_return, class T_arg1, class T_arg2>
class SmartSignal<T_return, T_arg1, T_arg2, sigc::nil, sigc::nil> : public SmartSignal2<T_return, T_arg1, T_arg2>
{
	public:
		SmartSignal(std::string n = "SmartSignal<T_return, T_arg1, T_arg2>") : SmartSignal2<T_return, T_arg1, T_arg2>(n) {};
};
// -------------------------------------------------------------------------
template<class T_return, class T_arg1>
class SmartSignal<T_return, T_arg1, sigc::nil, sigc::nil, sigc::nil> : public SmartSignal1<T_return, T_arg1>
{
	public:
		SmartSignal(std::string n = "SmartSignal<T_return, T_arg1>") : SmartSignal1<T_return, T_arg1>(n) {};
};
// -------------------------------------------------------------------------
namespace UMessages
{
	struct MessageId;
}
namespace std
{
	template<>
	struct hash<UMessages::MessageId>
	{
		size_t operator()(UMessages::MessageId const& s) const;
	};
}
std::ostream& operator<<( std::ostream& os, const UMessages::MessageId& t );
template<> std::ostream& operator<<( std::ostream& os, const SignalInfoKey<UniWidgetsTypes::ObjectId>& t );
#endif
