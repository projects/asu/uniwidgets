#ifndef _THEMELOADER_WRAPPER_H_
#define _THEMELOADER_WRAPPER_H_
// -------------------------------------------------------------------------
#include <glib.h>
// -------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

gchar ** get_theme_groups(gsize *size);

#ifdef __cplusplus
}
#endif
// -------------------------------------------------------------------------
#endif
