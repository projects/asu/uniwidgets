#include <ThemeLoader.h>
#include <SVGLoader.h>
#include <ThemeLoader-wrapper.h>
#include <types.h>
#include <string>
// -------------------------------------------------------------------------
/* Default theme if loaded no */
#define DEF_THEME "usvg_std.theme"
// -------------------------------------------------------------------------
using namespace UniWidgetsTypes;
using namespace std;
// -------------------------------------------------------------------------
bool ThemeLoader::theme_loaded = false;
bool ThemeLoader::loaded = false;
// -------------------------------------------------------------------------
string ThemeLoader::theme_path = "/usr/share/uniwidgets/svg/";
string ThemeLoader::theme_file = "usvg_std.theme";
// -------------------------------------------------------------------------
Glib::KeyFile ThemeLoader::kf;
// -------------------------------------------------------------------------
extern "C" gchar **
get_theme_groups(gsize *size)
{
	ThemeLoader::LoadTheme();
	return g_key_file_get_groups(ThemeLoader::kf.gobj(), size);
}
// -------------------------------------------------------------------------
void ThemeLoader::LoadTheme()
{
	theme_file = UniWidgetsTypes::getArgParam("--theme", theme_file);
	
	Glib::ustring fullpath = SVGLoader::get_path() + theme_file;

	try {
		loaded = kf.load_from_file(fullpath);
	}
	catch(Glib::Error& kf_err) {
		Glib::ustring err_msg = kf_err.what();
		g_print("Theme loading error: %s -- %s\n", fullpath.c_str(), err_msg.c_str());
	}
	theme_loaded = true;
}
// -------------------------------------------------------------------------
ThemeLoader::ThemeLoader(const Glib::ustring& str) :
	group(str)
{
	if (!theme_loaded )
		LoadTheme();
/*	
	if ( !kf.has_group(str) )
	{
		UniWidgetsTypes::Exception ex("Group \'"+str+"\' not found.");
		throw ex;
	}
*/
}
// -------------------------------------------------------------------------
void ThemeLoader::set_group(const Glib::ustring& str)
{
	if ( !kf.has_group(str) )
	{
		UniWidgetsTypes::Exception ex("Group \'"+str+"\' not found.");
		throw ex;
	}

	group = str;

}
// -------------------------------------------------------------------------
Glib::ustring ThemeLoader::get_string(const Glib::ustring& key, bool quiet)
{
	Glib::ustring result = "";
	
	if(loaded) 
	{
		try {
			result = kf.get_string(group, key);
		}
		catch(Glib::KeyFileError& kf_err) {
			if(!quiet) {
				Glib::ustring err_msg = kf_err.what();
				g_print("Invalid theme entry: %s\n", err_msg.c_str());
			}
		}
	}

	return result;
}
// -------------------------------------------------------------------------
gdouble ThemeLoader::get_double(const Glib::ustring& key, gdouble dflt, bool quiet)
{
	if(!loaded)
		return dflt;

	try {
		return kf.get_double(group, key);
	}
	catch(Glib::KeyFileError& kf_err) {
		if(!quiet) {
			Glib::ustring err_msg = kf_err.what();
			g_print("Invalid theme: %s\n", err_msg.c_str());
		}
		return dflt;
	}
}
// -------------------------------------------------------------------------
bool ThemeLoader::get_boolean(const Glib::ustring& key, bool dflt, bool quiet)
{
	if(!loaded)
		return dflt;

	try {
		return kf.get_boolean(group, key);
	}
	catch(Glib::KeyFileError& kf_err) {
		if(!quiet) {
			Glib::ustring err_msg = kf_err.what();
			g_print("Invalid theme: %s\n", err_msg.c_str());
		}
		return dflt;
	}
}
// -------------------------------------------------------------------------
std::vector<double> ThemeLoader::get_double_list(const Glib::ustring& key, bool quiet)
{
//	GError *error;

	if(!loaded)
		return std::vector<double>();

	try {
		return kf.get_double_list(group, key);
	}
	catch(Glib::KeyFileError& kf_err) {
		if(!quiet) {
			Glib::ustring err_msg = kf_err.what();
			g_print("Invalid theme: %s\n", err_msg.c_str());
		}
	}

	return std::vector<double>();
}
// -------------------------------------------------------------------------
Gdk::Color ThemeLoader::get_color(const Glib::ustring& key, const Glib::ustring& dflt, bool quiet)
{
//	GError *error;

	if(!loaded)
		return Gdk::Color(dflt);

	try {
		return Gdk::Color(kf.get_string(group, key));
	}
	catch(Glib::KeyFileError& kf_err) {
		if(!quiet) {
			Glib::ustring err_msg = kf_err.what();
			g_print("Invalid theme: %s\n", err_msg.c_str());
		}
	}

	return Gdk::Color(dflt);
}
// -------------------------------------------------------------------------
SVGLoader ThemeLoader::getSVG(const Glib::ustring& key, const Glib::ustring& dflt, bool quiet)
{
	Glib::ustring str = get_string(key, quiet);
	SVGLoader loader( str );

	if (!loader.isLoaded())
	{
		loader.loadRsvgFile( dflt );
	}
	return loader;
}
// -------------------------------------------------------------------------
