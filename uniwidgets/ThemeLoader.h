#ifndef _THEMELOADER_H_
#define _THEMELOADER_H_
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <librsvg/rsvg.h>
#include <SVGLoader.h>
// -------------------------------------------------------------------------
/*!
 * \brief Менеджер тем.
 * \par
 * Предназначен для загрузки тем из текстового файла.В файле для виджета можно
 * описать различные параметры такие как цвет, картинка, текст, шрифт и т.п.
*/
class ThemeLoader
{
public:
	/*! объект,который обрабатывает файл хранящие группы пар ключ-значение */
	static Glib::KeyFile kf;
	/*! загрузить тему */
	static void LoadTheme();

	ThemeLoader(const Glib::ustring& group);
	~ThemeLoader(){};
	/*! задать группу для поиска по файлу */
	void set_group(const Glib::ustring& group);
	/*! загрузить строку из файла темы */
	Glib::ustring get_string(const Glib::ustring& key, bool quiet = false);
	/*! загрузить значение типа double из файла темы */
	gdouble get_double(const Glib::ustring& key, gdouble dflt = 0.0, bool quiet = false);
	/*! загрузить значение типа boolean из файла темы */
	bool get_boolean(const Glib::ustring& key, bool dflt = false, bool quiet = false);
	/*! загрузить значение типа Gdk::Color из файла темы */
	Gdk::Color get_color(const Glib::ustring& key, const Glib::ustring& dflt = "#ffffff", bool quiet = false);
	/*! загрузить список значений типа gdouble из файла темы */
	std::vector<gdouble> get_double_list(const Glib::ustring& key, bool quiet = false);
	/*! загрузить SVG файл из темы */
	SVGLoader getSVG(const Glib::ustring& key, const Glib::ustring& dflt = "", bool quiet = false);

protected:
	static bool loaded;			/*!< если значение true, то тема загружена */
	static bool theme_loaded;		/*!< если значение true, то LoadTheme не нужно вызывать */
	static std::string theme_path;		/*!< путь до svg файлов */
	static std::string theme_file;		/*!< имя файла темы */

	Glib::ustring group;			/*!< название группы в файле темы */
	ThemeLoader();
};
#endif
