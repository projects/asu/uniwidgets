#ifndef _TRANSPROPERTY_H
#define _TRANSPROPERTY_H
// -------------------------------------------------------------------------
#include <SensorProperty.h>
#include <iostream>
// -------------------------------------------------------------------------
/*!
 * \brief Класс для работы со свойствами дочерних виджетов.
 * \par
 * Основная задача класса - это привязать свойства виджета к свойствам дочернего
 * виджета, так чтобы изменяя первые менялись и вторые.
 * Пример использования:
 * \code
 * class Widget : ...
 * {
 * ...
 * private:
 * 	SomeChildWidget ch;
 *	TransProperty<Type> property;
 * };
 * ...
 * #define WIDGET_INIT_PROPERTIES() \
 * property(*this, ch.property(), "some-property", default_value )
 * ...
 * Widget::Widget(...) :
 * 	...
 * 	,WIDGET_INIT_PROPERTIES()
 * {
 * 	...
 * }
 * \endcode
*/
template <class ValueType> class TransProperty
{
public:
	typedef Glib::Property<ValueType> PropertyType;
	typedef Glib::PropertyProxy<ValueType> PropertyProxyType;
	/*! задать новое значение для свойства.
	    \param data новое значение для свойства.
	*/
	void set_value(const ValueType& data)
	{ m_property = data; } 
	/*! конструктор.
	    \param owner виджет-владелец свойства.
	    \param name имя свойства.
	*/
	TransProperty( Gtk::Widget& owner, Glib::ustring name ) :
	m_owner(&owner),
	m_name(name),
	m_property(owner, name)
	{
	}
	/*! конструктор.
	    \param owner виджет-владелец свойства.
	    \param name имя свойства.
	    \param def_value значение свойства по-умолчанию.
	*/
	TransProperty( Gtk::Widget& owner, Glib::ustring name, ValueType def_value) :
	m_owner(&owner),
	m_name(name),
	m_property(owner, name, def_value)
	{
	}
  ~TransProperty()
  {
    for (typename std::list<PropertyProxyType*>::iterator it = connected_properties.begin(); it != connected_properties.end(); it++)
    {
      if(*it != NULL)
        delete *it;
    }
  }

/*	TransProperty( Gtk::Widget& owner, PropertyType &property, Glib::ustring name, ValueType def_value) :
	m_owner(&owner),
	m_name(name),
	m_property(owner, name, def_value)
	{
		connected_properties.push_back(&property.get_proxy());
		property.set_value (m_property);
	}

	TransProperty( Gtk::Widget& owner, PropertyType &property, Glib::ustring name ) :
	m_owner(&owner),
	m_name(name),
	m_property(owner, name)
	{
		connected_properties.push_back(&property.get_proxy());
		property.set_value (m_property);
	}*/
	/*! конструктор.
	    \param owner виджет-владелец свойства.
	    \param property_proxy свойство дочернего виджета,к которому подсоединяется свойство родителя.
	    \param name имя свойства.
	    \param def_value свойства значение по-умолчанию.
	*/
	TransProperty( Gtk::Widget& owner, PropertyProxyType property_proxy, Glib::ustring name, ValueType def_value) :
	m_owner(&owner),
	m_name(name),
	m_property(owner, name, def_value)
	{
		connected_properties.push_back
			(new PropertyProxyType(property_proxy));
	}
	/*! конструктор.
	    \param owner виджет-владелец свойства.
	    \param property_proxy свойство дочернего виджета,к которому подсоединяется свойство родителя.
	    \param name имя свойства.
	*/
	TransProperty( Gtk::Widget& owner, PropertyProxyType property_proxy, Glib::ustring name ) :
	m_owner(&owner),
	m_name(name),
	m_property(owner, name)
	{
		connected_properties.push_back
			(new PropertyProxyType(property_proxy));
	}
	/*! подсоединить обработчик изменения значения датчика.
	*/
	void connect_processing()
	{
		on_property_changed();
		m_owner->connect_property_changed( m_name, sigc::mem_fun(*this, &TransProperty::on_property_changed));
	}

	operator PropertyType& () { return m_property; }
	/*! получить текущее значение свойства.
	*/
	ValueType get_value() {return m_property.get_value();}
	/*! подсоединить к новому свойству(дочернего виджета).
	    \param property дочернее свойство.
	*/
	void connect_property( const PropertyType& property )
	{
		connected_properties.push_back(&property);
		property.set_value (m_property);
	}
  
  void connect_property( const PropertyProxyType& property )
  {
    connected_properties.push_back(new PropertyProxyType(property));
    typename std::list<PropertyProxyType*>::reverse_iterator it = connected_properties.rbegin();
    (*it)->set_value( m_property.get_value() );
  }
	/*! обработчик изменения состояние датчиков.
	*/
	void on_property_changed();


protected:
	TransProperty();
	Gtk::Widget* m_owner;					/*!< указатель на виджет-владелец свойства */
	Glib::ustring m_name;					/*!< имя свойства */
	PropertyType m_property;				/*!< свойство */
	std::list< PropertyProxyType* > connected_properties;	/*!< список подсоединенных свойств */
};
// -------------------------------------------------------------------------
template<> inline
void TransProperty<USensorProperty>::on_property_changed()
{
	for (std::list<PropertyProxyType*>::iterator it = connected_properties.begin(); it != connected_properties.end(); it++)
	{
		*(*it) = m_property;
	}
}
// -------------------------------------------------------------------------
template<class T> inline
void TransProperty<T>::on_property_changed()
{
	for (typename std::list<PropertyProxyType*>::iterator it = connected_properties.begin(); it != connected_properties.end(); it++)
	{
		(*it)->set_value( m_property.get_value() );
	}

}
// -------------------------------------------------------------------------
template<> inline
void TransProperty<USensorProperty>::connect_processing()
{
	m_owner->connect_property_changed( m_name + "-sensor-name", sigc::mem_fun(*this, &TransProperty::on_property_changed));
	m_owner->connect_property_changed( m_name + "-sensor-id", sigc::mem_fun(*this, &TransProperty::on_property_changed));
	m_owner->connect_property_changed( m_name + "-node-name", sigc::mem_fun(*this, &TransProperty::on_property_changed));
	m_owner->connect_property_changed( m_name + "-node-id", sigc::mem_fun(*this, &TransProperty::on_property_changed));
	m_owner->connect_property_changed( m_name + "-stype", sigc::mem_fun(*this, &TransProperty::on_property_changed));
}
// -------------------------------------------------------------------------
#endif
