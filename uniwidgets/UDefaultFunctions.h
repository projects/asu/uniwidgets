#ifndef _UDEFAULTFUNCTIONS_H
#define _UDEFAULTFUNCTIONS_H
// -------------------------------------------------------------------------
#include <UVoid.h>
#include <global_macros.h>
#include <UPixbufCache.h>
// -------------------------------------------------------------------------
class GuiPM;
// -------------------------------------------------------------------------
#define WRAPPED_TYPENAME( typename ) (std::string( g_type_name(typename) )+ "_uwrapped").c_str()
// -------------------------------------------------------------------------
/*!
 * \brief  Шаблонный базовый класс для логик и других виджетов, работающих с датчиками SharedMemory.
 * \par
 * Класс наследуется от класса Gtk виджета и UVoid. В нем реализуются методы
 * объявленные в UVoid и добавлены новые свойства: автоматическое соединение
 * с SharedMemory, эффект отсутствия связи, блокировка виджета при срабатывании АПС.
 * GtkParentClass - базовый Gtk виджет.
*/
template<class GtkParentClass>
class UDefaultFunctions : public GtkParentClass, public UVoid, public UPixbufCache
{
public:
  typedef GtkParentClass GtkmmBaseType;
  typedef UDefaultFunctions<GtkParentClass> BaseType;
  UDefaultFunctions() :
    Glib::ObjectBase(WRAPPED_TYPENAME(GtkParentClass::get_type()))
    ,UVoid(this)
	,is_glade_editor(false)
	,property_auto_connect(*this, "auto-connect", true)
    ,property_disconnect_effect(*this, "disconnect-effect", 1)
    ,property_lock_view(*this, "lock-view", false)
    ,property_cache_copy(*this, "cache-copy", false)
  {}

  explicit UDefaultFunctions(typename GtkParentClass::BaseObjectType* o) :
    GtkParentClass(o)
    ,UVoid(this)
	,is_glade_editor(false)
	,property_auto_connect(*this, "auto-connect", true)
    ,property_disconnect_effect(*this, "disconnect-effect", 1)
    ,property_lock_view(*this, "lock-view", false)
    ,property_cache_copy(*this, "cache-copy", false)
  {}
  
  UDefaultFunctions(const BaseType& p) :
    UVoid(this)
	,is_glade_editor(false)
    ,property_auto_connect(*this, "auto-connect", p.property_auto_connect)
    ,property_disconnect_effect(*this, "disconnect-effect", p.property_disconnect_effect)
    ,property_lock_view(*this, "lock-view", p.property_lock_view)
    ,property_cache_copy(*this, "cache-copy", p.property_cache_copy)
  {}

  virtual ~UDefaultFunctions() {}
  /*! обработчик сигнала изменения в иерархии виджета. Точнее, виджет закреплён когда его предок верхнего уровня 	       Gtk::Window. Этот сигнал издаётся когда виджет изменяется от закреплённого к не закреплённому и обратно
    \param *w предок верхнего уровня.
  */
  virtual void on_hierarchy_changed(Gtk::Widget* w);
  virtual void set_state(bool state);

  ConnectorRef get_connector_from_hierarchy();		/*!< получить коннетор виджета(см. Connector) */
  virtual void add_lock(const Gtk::Widget& w);		/*!< см. UVoid::add_lock */
  virtual void unlock_current();				/*!< см. UVoid::unlock_current */
  inline void set_is_glade_editor(bool flag){ is_glade_editor = flag;};
  
protected:
	bool is_glade_editor;
	
 	virtual Glib::RefPtr<Gdk::Pixbuf> get_pixbuf_from_cache(const std::string& path,int w=-1,int h=-1, bool make_copy=false)
 	{
		return UPixbufCache::get_pixbuf_from_cache(path, w, h, make_copy || property_cache_copy.get_value());
	}

	ADD_PROPERTY( property_auto_connect, bool )					/*!< свойство: включение автоматического подключения к SharedMemory*/
	ADD_PROPERTY( property_disconnect_effect, int )				/*!< свойство: включение отрисовки эффекта отсутствия связи с SharedMemory*/
	ADD_PROPERTY( property_lock_view, bool )					/*!< свойство: включение блокирования виджета при срабатывании АПС*/
	ADD_PROPERTY( property_cache_copy, bool )					/*!< свойство: копирование картинки pixbufcache*/

private:

};
// -------------------------------------------------------------------------
template<class T> ConnectorRef
UDefaultFunctions<T>::get_connector_from_hierarchy()
{
  Gtk::Widget* parent = reinterpret_cast<Gtk::Widget*>(T::get_parent());
  while (parent != NULL) {
    if(UVoid* uv = dynamic_cast<UVoid*>(parent)) {
      ConnectorRef new_connector = uv->get_connector();
      return new_connector;
    }
    parent = reinterpret_cast<Gtk::Widget*>(parent->get_parent());
  }
  return ConnectorRef();
}
// -------------------------------------------------------------------------
template<class T> void
UDefaultFunctions<T>::on_hierarchy_changed(Gtk::Widget* w)
{
  GtkmmBaseType::on_hierarchy_changed(w);
  if ( !property_auto_connect )
    return;
  ConnectorRef connector = get_connector_from_hierarchy();
  set_connector(connector);
}
// -------------------------------------------------------------------------
template<class T>
void UDefaultFunctions<T>::add_lock(const Gtk::Widget& w)
{
  Gtk::Widget* prev = this;
  Gtk::Widget* next = reinterpret_cast<Gtk::Widget*>(T::get_parent());
  while (next != NULL) {
    if (UVoid* uv = dynamic_cast<UVoid*>(next)) {
      uv->add_lock(*prev);
      break;
    }

    prev = next;
    next = reinterpret_cast<Gtk::Widget*>(next->get_parent());
  }
  UVoid::add_lock(w);
}
// -------------------------------------------------------------------------
template<class T>
void UDefaultFunctions<T>::unlock_current()
{
  Gtk::Widget* parent = reinterpret_cast<Gtk::Widget*>(T::get_parent());
  for (Gtk::Widget* p = parent; p != NULL; p = reinterpret_cast<Gtk::Widget*>(p->get_parent()))
    if (UVoid* uv = dynamic_cast<UVoid*>(p)) {
      uv->unlock_current();
      break;
    }
}
// -------------------------------------------------------------------------
template<class T>
void UDefaultFunctions<T>::set_state(bool state)
{
  Gtk::Container* conn = dynamic_cast<Gtk::Container*>(this);
  if( !conn )
    return;
  auto childs = conn->get_children();
  for( auto chit:childs )
  {
    if( !chit )
      continue;
    auto uvoid = dynamic_cast<UVoid*>(chit);
    if(uvoid)
      uvoid->set_state(state);
  }
}
// -------------------------------------------------------------------------
#endif
