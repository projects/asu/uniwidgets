#include "UPixbufCache.h"
#include <librsvg/rsvg.h>
#include <iostream>
#include <types.h>

// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------

Glib::RefPtr<Gdk::Pixbuf> UPixbufCache::empty = Glib::RefPtr<Gdk::Pixbuf>();
UPixbufCache::PixbufMap UPixbufCache::PixbufCache;
// -------------------------------------------------------------------------
void UPixbufCache::add_pixbuf(const string& parametr, const Glib::RefPtr<Gdk::Pixbuf> pixbuf)
{
	PixbufCache[parametr] = pixbuf;
}
// -------------------------------------------------------------------------
bool UPixbufCache::find_pixbuf(const string& parametr)
{
	PixbufMap::const_iterator It = PixbufCache.find(parametr);
	return It!=PixbufCache.end();
}
// -------------------------------------------------------------------------
Glib::RefPtr<Gdk::Pixbuf> UPixbufCache::get_pixbuf_from_cache(const string& path,int width,int height, bool make_copy)
{
//	cout << "path:" << (path.empty()?"empty":path)<<" file_exist="<<file_exist(path)<< endl;

	try
	{
		Glib::Dir subdir(path);
	}
	catch( Glib::FileError& ex )
	{
//		if( ex.code() == Glib::FileError::NOT_DIRECTORY)
		{
			if(path.empty() || !file_exist(path))
			{
				/*Создание картинки при инициализации виджета.
				Картинки создаются сначала со значениями по-умолчанию т.е.
				с пустыми путями картинок. Также обработка неверного пути.
				*/
				if( !empty )
					empty = resize(path, 1, 1);
				return make_copy ? empty->copy() : empty;
			}
			ostringstream parametr;
			parametr<<path << "_" << width << "x" << height;

			PixbufMap::const_iterator It = PixbufCache.find(parametr.str());
			if(It!=PixbufCache.end())
			{
				return make_copy ? It->second->copy():It->second;
			}
			else
			{
				Glib::RefPtr<Gdk::Pixbuf> pixbuf = resize(path, width, height);
				add_pixbuf(parametr.str(),pixbuf);
				return make_copy ? pixbuf->copy():pixbuf;
			}
		}
	}
	if( !empty )
		empty = resize(path, 1, 1);
	return make_copy ? empty->copy() : empty;
}
// -------------------------------------------------------------------------
Glib::RefPtr<Gdk::Pixbuf> UPixbufCache::resize(const string& imagefile, int w,int h)
{
	w = w>0 ? w:-1;
	h = h>0 ? h:-1;
	Glib::RefPtr<Gdk::Pixbuf> pixbuf;
	try
	{
		Glib::Dir subdir(imagefile);
	}
	catch( Glib::FileError& ex )
	{
		if( ((!imagefile.empty() && !file_exist(imagefile)) || imagefile.empty()) )//&& (w>0 && h>0) )
		{
			pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB,1, 8, w, h);
			pixbuf->fill(0);
			if( !pixbuf )
				pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB,1, 8, 1, 1);
			return pixbuf;
		}

		if(w==-1 && h==-1)
		{
			try
			{
				pixbuf = Gdk::Pixbuf::create_from_file(imagefile);
			}
			catch( Glib::FileError& ex )
			{
				pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB,1, 8, 1, 1);
			}
			if( !pixbuf )
				pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB,1, 8, 1, 1);
			return pixbuf;
		}

		Glib::ustring str = imagefile;
		str = str.uppercase();
		Glib::ustring svg	= ".svg";
		Glib::ustring svgz	= ".svgz";
		svg = svg.uppercase();
		svgz = svgz.uppercase();
		if( str.rfind(svg)== (str.size()-svg.size()) || str.rfind(svgz)== (str.size()-svgz.size()) )
		{
			try
			{
				pixbuf =  Glib::wrap(rsvg_pixbuf_from_file_at_size(imagefile.c_str(),w,h,NULL));
			}
			catch( Glib::FileError& ex )
			{
				pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB,1, 8, w, h);
			}
		}
		else
		{
			try
			{
				pixbuf = Gdk::Pixbuf::create_from_file(imagefile);
				w = w>0 ? w:pixbuf->get_width();
				h = h>0 ? h:pixbuf->get_height();
				pixbuf = pixbuf->scale_simple(w,h,Gdk::INTERP_HYPER);
			}
			catch( Glib::FileError& ex )
			{
				pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB,1, 8, w, h);
			}
		}
		if( !pixbuf )
			pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB,1, 8, 1, 1);
		return pixbuf;
	}

	pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB,1, 8, w>0 ? w:1, h>0 ? h:1);
	pixbuf->fill(0);
	if( !pixbuf )
		pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB,1, 8, 1, 1);
	return pixbuf;
}
// -------------------------------------------------------------------------
