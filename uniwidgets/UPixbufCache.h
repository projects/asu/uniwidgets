#ifndef _UPIXBUFCACHE_H
#define _UPIXBUFCACHE_H
// -------------------------------------------------------------------------
//#include <gtkmm.h>
#include <gdkmm.h>
#include <unordered_map>
// -------------------------------------------------------------------------
class UPixbufCache
{
public :
	UPixbufCache() {}
	virtual ~UPixbufCache() {}
	typedef std::unordered_map< std::string, Glib::RefPtr<Gdk::Pixbuf> > PixbufMap;

	/* Methods */
	void add_pixbuf(const std::string& parametr,const Glib::RefPtr<Gdk::Pixbuf> pixbuf);
	bool find_pixbuf(const std::string& parametr);
	virtual Glib::RefPtr<Gdk::Pixbuf> get_pixbuf_from_cache(const std::string& path,int w=-1,int h=-1, bool make_copy=false);

	/*! изменить размер картинки */
	static Glib::RefPtr<Gdk::Pixbuf> resize(const std::string& imagefile="", int width=-1,int height=-1);

protected:
	/* Variables */
	static Glib::RefPtr<Gdk::Pixbuf> empty;
	static PixbufMap PixbufCache;
private:
	/* Variables */
};
#endif
