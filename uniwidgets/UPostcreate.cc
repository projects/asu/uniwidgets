#include <gtkmm.h>
#include <typical/IndicatorTwoState.h>
#include <typical/IndicatorFourState.h>
#include <typical/GDG.h>
#include <typical/VDG.h>
#include "UPostcreate.h"
//----------------------------------------------------------
using namespace UniWidgets;
//----------------------------------------------------------
// template class UPostcreate<Gtk::Widget>;
// UPostcreate::Hook UPostcreate::_hook = 0;
template class UPostcreate<IndicatorTwoState>;
template <> UPostcreate<IndicatorTwoState>::Hook UPostcreate<IndicatorTwoState>::_hook = 0;
template class UPostcreate<IndicatorFourState>;
template <> UPostcreate<IndicatorFourState>::Hook UPostcreate<IndicatorFourState>::_hook = 0;
template class UPostcreate<GDG>;
template <> UPostcreate<GDG>::Hook UPostcreate<GDG>::_hook = 0;
template class UPostcreate<VDG>;
template <> UPostcreate<VDG>::Hook UPostcreate<VDG>::_hook = 0;
