#ifndef _UPOSTCREATE_H
#define _UPOSTCREATE_H
//----------------------------------------------------------
#include <gtkmm.h>
//----------------------------------------------------------
//Gtk::Widget -> W
/*!
 * \brief Класс содержащий функцию, которая выполняется при создании виджета.
 * \par
 * Данный класс содержит одну ссылку на статическую функцию и методы для работы
 * с этой ссылкой. Если назначить ссылке функцию через метод reg, то при создании
 * виджета будет вызван метод run и соответственно вызвана функция находящаяся по
 * ссылке.
 * Пример использования:
 * \code
 * ...
 * template<typename W>
 * void widget_connect(W* widget)
 * {
 * 	if(TypeWidget *w = dynamic_cast<TypeWidget *>(widget))
 *	{
 * 		//do something
 *	}
 * }
 * ...
 * int main(int argc, const char **argv)
 * {
 * ...
 * 	UPostcreate<TypeWidget>::reg(&widget_connect);
 * ...
 * 	return 0;
 * }
 * \endcode
*/
template <class W>
class UPostcreate
{
public:
	typedef void (*Hook)(W*);
private:
	static Hook _hook; /*!< ссылка на функцию */
public:
	/*! назначить ссылку на функцию.
	    \param hook ссылка на функицю,которую необходимо вызывать при создании виджета.
	*/
	static void reg (Hook hook)
	{
		_hook = hook;
	}
	/*! обнулить ссылку.
	*/
	static void unreg ()
	{
		_hook = 0;
	}
	/*! вызвать функцию по ссылке.
	    \param *w указатель на объект, который передается вызываемой функции.
	*/
	static void run(W* w)
	{
		if (_hook)
			_hook(w);
	}
};
#endif
