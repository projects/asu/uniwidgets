#include "USensor.h"
#include <iostream>
#include <sigc++/sigc++.h>
// -------------------------------------------------------------------------
using namespace UniWidgetsTypes;
using namespace std;
// -------------------------------------------------------------------------
USensor::USensor(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, ConnectorRef gpm)
{
	id_ = id;
	if ( node = -1 && gpm )
		node_ = gpm->getLocalNode();
	else
		node_ = node;
	connref = gpm;
}
// -------------------------------------------------------------------------
bool
USensor::connected() const
{
	return connected_;
}
// -------------------------------------------------------------------------
void
USensor::connect()
{
	/*
	 * TODO: need check if already conected.
	 */
	if ( connref == NULL ) {
		cerr << "USingnal: _gpm == NULL. Not connected." << endl;
		return;
	}
	connref->signals().connect_value_changed
		(sigc::mem_fun(this, &USensor::on_value_changed), id_, node_);
	connected_ = true;
}
// -------------------------------------------------------------------------
void
USensor::disconnect()
{
	/*
	 * TODO: maybe print warning if already disconnected.
	 */
	connection_.disconnect();
	connected_ = false;
}
// -------------------------------------------------------------------------
long
USensor::get_value() const
{
	return value_;
}
// -------------------------------------------------------------------------
/*float
USensor::get_value_with_precision() const
{

}*/
// -------------------------------------------------------------------------
void
USensor::save_value(long value) const
{
	/*
	 * TODO: need checking connref != NULL, is_connected() == true, etc.
	 */
	ConnectorRef constref = connref;
	constref->save_value(value, id_, node_);
}
// -------------------------------------------------------------------------
/*void
USensor::save_value_with_precision(float value) const
{

}*/
// -------------------------------------------------------------------------
void
USensor::set_id(UniWidgetsTypes::ObjectId id)
{
	bool save_connected = connected_;
	if ( save_connected )
		disconnect();

	id_ = id;

	if ( save_connected )
		connect();
}
// -------------------------------------------------------------------------
void
USensor::set_id(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node)
{
	bool save_connected = connected_;
	if ( save_connected )
		disconnect();

	id_ = id;
	node_ = node;

	if ( save_connected )
		connect();
}
// -------------------------------------------------------------------------
void
USensor::set_node(UniWidgetsTypes::ObjectId node)
{
	bool save_connected = connected_;
	if ( save_connected )
		disconnect();

	node_ = node;

	if ( save_connected )
		connect();
}
// -------------------------------------------------------------------------
void
USensor::on_value_changed(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long new_value)
{
	value_ = new_value;
	signal_value_changed_(*this);
}
// -------------------------------------------------------------------------
