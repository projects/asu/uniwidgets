#ifndef __USENSOR_H_
#define __USENSOR_H_
// -------------------------------------------------------------------------
#include <Connector.h>
#include <sigc++/sigc++.h>
#include "types.h"
// -------------------------------------------------------------------------
/*!
 * \brief Класс для работы с датчиком.
 * \par
 * Класс реализует работу с датчиком: его опрос,выставление и отслеживание состояния.
 * \todo В данный момент класс не используется виджетами, поэтому нужно определить нужен
 * ли класс вообще. А все методы и сигналы работы с датчиком доступны через Connector.
*/
class USensor
{
public:
	/*
	 * TODO: write documentation
	 */
	USensor(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, ConnectorRef gpm);

	/*
	 * TODO: write documentation
	 */
	bool connected() const;

	/*
	 * TODO: write documentation
	 */
	void connect();

	/*
	 * TODO: write documentation
	 */
	void disconnect();

	/*
	 * TODO: write documentation
	 */
	long get_value() const;

	/*
	 * TODO: write documentation
	 */
	//float get_value_with_precision() const;

	/*
	 * TODO: write documentation
	 */
	void save_value(long value) const;

	/*
	 * TODO: write documentation
	 */
	//void save_value_with_precision(float value) const;

	/*
	 * TODO: write documentation
	 */
	void set_id(UniWidgetsTypes::ObjectId id);

	/*
	 * TODO: write documentation
	 */
	void set_id(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node);

	/*
	 * TODO: write documentation
	 */
	void set_node(UniWidgetsTypes::ObjectId node);

	/*
	 * TODO: write documentation
	 */
	UniWidgetsTypes::ObjectId get_id() const;

	/*
	 * TODO: write documentation
	 */
	UniWidgetsTypes::ObjectId get_node() const;

	/*
	 * TODO: write documentation
	 */
	sigc::signal<void, const USensor&>& signal_value_changed();

protected:
	void on_value_changed(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long new_value);

private:
	UniWidgetsTypes::ObjectId id_;
	UniWidgetsTypes::ObjectId node_;
	ConnectorRef connref;
	sigc::signal<void, const USensor&> signal_value_changed_;
	USignals::VConn connection_;
	long value_;
	bool connected_;
};
// -------------------------------------------------------------------------
inline UniWidgetsTypes::ObjectId USensor::get_id() const
{
	return id_;
}
// -------------------------------------------------------------------------
inline UniWidgetsTypes::ObjectId USensor::get_node() const
{
	return node_;
}
// -------------------------------------------------------------------------
inline sigc::signal<void, const USensor&>& USensor::signal_value_changed() 
{
	return signal_value_changed_;
}
// -------------------------------------------------------------------------
#endif
