#ifndef _USIGNALS_H
#define _USIGNALS_H
// -------------------------------------------------------------------------
#include <sigc++/sigc++.h>
#include <glibmm/ustring.h>
#include <glibmm/convert.h>
#include <ctime>
#include <SmartSignal.h>
#include <types.h>
// -------------------------------------------------------------------------
namespace USignals
{
	typedef sigc::slot<std::string> GetInfoSlot;
	struct GetInfoSlotItem
	{
		std::string component;
		std::string info;
		GetInfoSlot callback;
	};
	typedef std::unordered_map<int, GetInfoSlotItem> GetInfoSlots;
	class ValueMapItem;
} //namespace USingnals

namespace UMessages
{
	struct ValueRange
	{
		ValueRange( long value1,long value2 ):
			value1(value1),
			value2(value2)
			{}
		ValueRange(long value) :
			value1(value),
			value2(value)
			{}
		ValueRange(const ValueRange &x) :
			value1(x.value1),
			value2(x.value2)
			{}
		ValueRange() :
			value1(0),
			value2(0)
			{}
		long value1;
		long value2;
		bool operator==(const long &x) const
		{
			if(value1 <= value2)
				return ( value1 <= x && value2 >= x );
			else
				return ( value2 <= x && value1 >= x );
		}
		bool operator<(const long &x)const
		{
			if(value1 <= value2)
				return ( value2 < x );
			else
				return ( value1 < x );
		}
		bool operator>(const long &x)const
		{
			if(value1 <= value2)
				return ( value1 > x );
			else
				return ( value2 > x );
		}
		bool operator==(const ValueRange &x) const
		{
			return ( (x == value1 || x == value2) || (*this == x.value1 || *this == x.value2) );
		}
		bool operator!=(const ValueRange& x) const
		{
			return !(*this==x);
		}
		bool operator<(const ValueRange &x)const
		{
			return ( MAX(value1,value2) < MIN(x.value1,x.value2) );
		}
		bool operator!() const
		{
			return !(this->value1 && this->value2);
		};
		operator long() const
		{
			if(this->value1 <= this->value2)
				return this->value1;
			else
				return this->value2;
		};
	};
	/*!
 * \brief Структура для работы с описанием датчика.
 * \par
 * В структуре описывается датчик в удобной форме т.е. объединены в структуру его id, id узла
 * ,с которого его снимают и значение датчика,а также определены вспомогательные операции.
*/
	struct MessageId
	{
		MessageId(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value ):
			_id(id),
			_node(node),
			_value(value),
			_bits_msg(false)
			{}
			MessageId(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, ValueRange value ):
			_id(id),
			_node(node),
			_value(value),
			_msg_groups(""),
			_bits_msg(false)
			{}
			MessageId(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value1, long value2 ):
			_id(id),
			_node(node),
			_value(value1,value2),
			_msg_groups(""),
			_bits_msg(false)
			{}
			MessageId():
			_id(UniWidgetsTypes::DefaultObjectId),
			_node(UniWidgetsTypes::DefaultObjectId),
			_msg_groups(""),
			_bits_msg(false)
			{}
			UniWidgetsTypes::ObjectId _id;
			UniWidgetsTypes::ObjectId _node;
			ValueRange _value;
			std::string _msg_groups;
			bool _bits_msg;
			bool operator==(const MessageId& x) const
			{
				return ( _id == x._id && _node == x._node && _value == x._value );
			}
			bool operator!=(const MessageId& x) const
			{
				return !(*this==x);
			}
			bool operator<(const MessageId& x) const
			{
				if (_id < x._id)
					return true;
				if (_id > x._id)
					return false;
				if (_node < x._node)
					return true;
				if (_node > x._node)
					return false;
				if (_value < x._value)
					return true;
				else
					return false;
			}
			long get_long_value()
			{
				if(_value.value1 <= _value.value2)
					return _value.value1;
				else
					return _value.value2;
			}
	};
	/*!
 * \brief Класс АПС сообщения.
 * \par
 * Класс описывает структуру АПС сообщения.
*/
class Message
{
public:
	Message(const USignals::ValueMapItem& item, const UMessages::MessageId& id) : _item(&item), id_(id)  {}

	/**
	 * Creates invalid message
	 */
	Message() : _item(NULL) {}

	MessageId getMessageId() const;		/*!< получить MessageId для датчика сообщения */

	time_t getLastTime() const;		/*!< получить время появления сообщения */
	time_t getLastTimeNsec() const;		/*!< получить время появления сообщения(количество наносекунд на момент появления сообщения) */

	const Glib::ustring getText() const;	/*!< получить текст 1-го сообщения */
	const std::list<Glib::ustring>& getTextList() const; /*!< получить список сообщений */
	
	int getMessageType() const;		/*!< получить mtype для сообщения(1-предупреждения,2-авария,3-информационное) */

	bool valid() const;			/*!< проверка что сообщение не пустое */

private:
	const USignals::ValueMapItem* _item;
	const UMessages::MessageId id_;
};
/*! Типы сообщений информационное, предупредительное и аварийное */
enum MessageType { INFO=1, WARNING=2, ALARM=3 };
} //namespace UMessages

namespace USignals
{
class SigControllerImpl;

typedef sigc::slot<void, UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, long>
	ValueChangedSlot;
typedef sigc::slot<void, UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, float>
	AnalogValueChangedSlot;
typedef sigc::slot<void, UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, long>
	ValueInOutSlot;
typedef sigc::slot<void, UMessages::MessageId, Glib::ustring> MessageSlot;
typedef sigc::slot<void, UMessages::MessageId, int, time_t, Glib::ustring> FullMessageSlot;

typedef SmartSignal<void, UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, long>
	ValueChangedSignal;
typedef SmartSignal<void, UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, float>
	AnalogValueChangedSignal;
typedef SmartSignal<void, UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, long>
	ValueInOutSignal;
typedef SmartSignal<void, UMessages::MessageId, Glib::ustring> MessageSignal;

typedef SmartSignal<void, UMessages::MessageId, int, time_t, Glib::ustring> FullMessageSignal;

//ValueMap
struct ValueMapItem
{
	ValueMapItem(UniWidgetsTypes::ObjectId id, long val):_value_in("value_in"), _value_out("value_out"), _signal_message("signal_message"), _is_message(false)
	{
		std::ostringstream os;
		os << "(" << id << "=" << val << ")";
		_value_in.set_name("value_in" + os.str());
		_value_out.set_name("value_out" + os.str());
		_signal_message.set_name("signal_message" + os.str());
	}
	ValueInOutSignal _value_in;
	ValueInOutSignal _value_out;
	MessageSignal _signal_message;
	bool _is_message;
	time_t _last_time;
	time_t _time_nsec;
	std::list<Glib::ustring> _text;
	int _mtype;
	
	Glib::ustring get_first_text() const
	{
		return (_text.empty() ? "":*(_text.begin()));
	}
};
//end ValueMap

/*! \brief Вспомогательная класс.
 *  \par
 *  Вспомогательный класс, унаследованный от sigc::connection(просто переопределено имя).
*/
class Connection : public sigc::connection
{
public:
	Connection( ) {}

	Connection( sigc::connection conn) :
		sigc::connection(conn)
	{}

    virtual ~Connection(){}

	virtual void disconnect()
	{
		sigc::connection::disconnect();
	}
};
/*! \brief Вспомогательная структура.
 *  \par
 *  Вспомогательная структура, объединяющая коннектор с датчиком, узлом и его значением.
*/
struct VConn
{
	sigc::connection conn;
	UniWidgetsTypes::ObjectId id;
	UniWidgetsTypes::ObjectId node;
	long value;
	void disconnect();
};
// -------------------------------------------------------------------------
/*!
 * \brief Класс управления сигналами.
 * \par
 * Основная задача - это управление сигналами изменения значения, состояния датчиков
 * ,а также заказ датчиков из Sharedmemory. Данный класс реализует все свои методы
 * посредством включения класса SigControllerImpl,в котором находится реализация всех функций.
*/
class SigController
{
public:
	SigController(SigControllerImpl* impl);
	~SigController();
	/*! см. USignals::SigControllerImpl::connect_value_changed */
	Connection connect_value_changed( const ValueChangedSlot& slot,
			UniWidgetsTypes::ObjectId id,
			UniWidgetsTypes::ObjectId node );
	/*! см. USignals::SigControllerImpl::connect_analog_value_changed */
	Connection connect_analog_value_changed( const AnalogValueChangedSlot& slot,
			UniWidgetsTypes::ObjectId id,
			UniWidgetsTypes::ObjectId node );
	/*! см. USignals::SigControllerImpl::connect_value_in */
	VConn connect_value_in( const ValueInOutSlot& slot,
			UniWidgetsTypes::ObjectId id,
			UniWidgetsTypes::ObjectId node,
			long value);
	/*! см. USignals::SigControllerImpl::connect_value_out */
	VConn connect_value_out( const ValueInOutSlot& slot,
			UniWidgetsTypes::ObjectId id,
			UniWidgetsTypes::ObjectId node,
			long value);
	/*! см. USignals::SigControllerImpl::connect_on_any_message */
	Connection connect_on_any_message( const MessageSlot& slot);
	/*! см. USignals::SigControllerImpl::connect_on_message */
	Connection connect_on_message( const MessageSlot& slot,
			UMessages::MessageId id);
	/*! см. USignals::SigControllerImpl::connect_on_any_message_full */
	Connection connect_on_any_message_full ( const FullMessageSlot& slot );
	/*! см. USignals::SigControllerImpl::get_value */
	long get_value(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node);
	/*! см. USignals::SigControllerImpl::get_analog_value */
	float get_analog_value(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node);
	/*! см. USignals::SigControllerImpl::get_message */
	UMessages::Message get_message( const UMessages::MessageId& id);
	/*! см. USignals::SigControllerImpl::get_message_list */
	std::list<UMessages::Message > get_message_list( const UMessages::MessageId& id);
	/*! см. USignals::SigControllerImpl::get_all_message_list */
	std::list<UMessages::Message > get_all_messages_list();
	/*! см. USignals::SigControllerImpl::getInfo */
	std::string get_info(int signal_num = 0);
	/*! см. USignals::SigControllerImpl::connect_get_info_slot */
	void connect_get_info_slot( const GetInfoSlot& sl, const std::string& component = "", const std::string& info = "" );
	void on_connect();
private:
	SigController();
	SigControllerImpl* _impl;
};

} //namespace USignals
#endif
