#include "UVoid.h"
// -------------------------------------------------------------------------
using namespace std;
Blinker UVoid::blinker;
// -------------------------------------------------------------------------
void UVoid::draw_disconnect_effect_1(Cairo::RefPtr<Cairo::Context> cr, const Gdk::Rectangle& rect)
{
	cr->save();
	cr->set_line_width(0.5);
	cr->set_source_rgb(1,1,1);
	//TODO: temporary solution
	int max = rect.get_width();
	if (max < rect.get_height())
		max = rect.get_height();
	for(int i = 0; i < max; ++i) {
		cr->move_to(rect.get_x()+i*3, rect.get_y());
		cr->line_to(rect.get_x(), rect.get_y()+i*3);
	}
	cr->stroke();
	cr->restore();
}
// -------------------------------------------------------------------------
std::list<Glib::ustring> UVoid::explodestr( const Glib::ustring str, char sep )
{
	std::list<Glib::ustring> l;
	
	string::size_type prev = 0;
	string::size_type pos = 0;
	do
	{
		pos = str.find(sep,prev);
		string s(str.substr(prev,pos-prev));
		if( !s.empty() )
		{
			l.push_back(s);
			prev=pos+1;
		}
	}
	while( pos!=string::npos );
	
	return l;
}
// -------------------------------------------------------------------------
inline void UVoid::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	if(get_connector()) {
		disconnect_connection_.disconnect();
		connect_connection_.disconnect();

		if(get_connector()->connected())
			on_disconnect();
	}

	connector_ = connector;

	if (get_connector()) {
		Gtk::Widget* w = dynamic_cast<Gtk::Widget*>(this);
		get_connector()->signals().connect_get_info_slot(sigc::mem_fun(*this, &UVoid::get_info), "Widgets", get_full_widget_name() );
		
		if(get_connector()->connected())
			on_connect();

		disconnect_connection_ =
			get_connector()->signal_disconnected().connect(
					sigc::mem_fun(this, &UVoid::on_disconnect));
		connect_connection_ =
			get_connector()->signal_connected().connect(
					sigc::mem_fun(this, &UVoid::on_connect));
	}

}
// -------------------------------------------------------------------------
ConnectorRef& UVoid::get_connector()
{
	return connector_;
}
// -------------------------------------------------------------------------
void UVoid::on_connect() throw()
{
	connected_ = true;
}
// -------------------------------------------------------------------------
void UVoid::on_disconnect() throw()
{
	connected_ = false;
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, const GdkColor& t )
{
	std::ostringstream tmp;
	tmp << std::hex << t.red << t.green << t.blue;
	return os << tmp.str();
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, const GdkColor* t )
{
	return os << *t;
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, const Gtk::Widget* t )
{
	return os << (t ? t->get_name() : "");
}
// -------------------------------------------------------------------------
#define IF_TRY_VALUE_TYPE(type, type_c, pspec, value) \
if( g_type_from_name(#type) == pspec->value_type) \
{ \
	Glib::Value<type_c>* pvb = reinterpret_cast<Glib::Value<type_c>* >(&value); \
	if(pvb) \
		inf << pvb->get(); \
}
#define ELSE_IF_TRY_VALUE_TYPE(type, type_c, pspec, value) else IF_TRY_VALUE_TYPE(type, type_c, pspec, value)
// -------------------------------------------------------------------------
std::string UVoid::get_info()
{
	std::ostringstream inf;
	inf << get_full_widget_name() << ":" << std::endl;
	
	Gtk::Widget* w = dynamic_cast<Gtk::Widget*>(this);
	if(w)
	{
		guint n=0;
		GParamSpec** list = g_object_class_list_properties(G_OBJECT_GET_CLASS(G_OBJECT(w->gobj())),&n);
		for(guint i = 0; i < n; ++i)
		{
			Glib::ValueBase vb;
			GParamSpec*& pspec = list[i];
			vb.init(pspec->value_type);
			if( !(pspec->flags & G_PARAM_READABLE) )
				continue;
			w->get_property_value(pspec->name, vb);
			inf << "	[property] " << pspec->name << " = ";
			IF_TRY_VALUE_TYPE(gboolean, bool, pspec, vb)
			ELSE_IF_TRY_VALUE_TYPE(gint, int, pspec, vb)
			ELSE_IF_TRY_VALUE_TYPE(guint, unsigned int, pspec, vb)
			ELSE_IF_TRY_VALUE_TYPE(glong, long, pspec, vb)
			ELSE_IF_TRY_VALUE_TYPE(gchararray, std::string, pspec, vb)
			//ELSE_IF_TRY_VALUE_TYPE(GdkColor, GdkColor, pspec, vb)
			ELSE_IF_TRY_VALUE_TYPE(GtkWidget, Gtk::Widget*, pspec, vb)
			ELSE_IF_TRY_VALUE_TYPE(GtkContainer, Gtk::Widget*, pspec, vb)
			else inf << "[" <<  g_type_name(pspec->value_type) << "]";
			inf << std::endl;
		}
		g_free(list);
	}
	
	for( auto& variable : get_info_variables )
		inf << "	[variable] " << variable.first << " = "<< variable.second->get() << std::endl;
	return inf.str();
}
// -------------------------------------------------------------------------
std::string UVoid::get_full_widget_name()
{
	Gtk::Widget* w = dynamic_cast<Gtk::Widget*>(this);
	if( !w )
		return "UVoid";
	
	std::string full_name = w->get_name();
	while(w = dynamic_cast<Gtk::Widget*>(w->get_parent()))
	{
		full_name = w->get_name() + "::" + full_name;
	}
	return full_name;
}
// -------------------------------------------------------------------------
