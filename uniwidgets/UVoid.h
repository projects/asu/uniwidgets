#ifndef _UVOID_H
#define _UVOID_H
// -------------------------------------------------------------------------
#include "types.h"
#include <gtkmm.h>
#include "SBlinker.h"
#include "Connector.h"
#include <iostream>
// -------------------------------------------------------------------------
class GuiPM;

class GetInfoVariableBase
{
public:
	GetInfoVariableBase() {}
	virtual ~GetInfoVariableBase() {}
	virtual std::string get() = 0;
};

template<class Type>
class GetInfoVariable : public GetInfoVariableBase
{
public:
	GetInfoVariable() : ptr(nullptr) {}
	virtual ~GetInfoVariable() {}
	void init(Type& p) { ptr = &p; }
	virtual std::string get() override
	{
		std::ostringstream os;
		if(ptr)
			os << *ptr;
		return os.str();
	}
private:
	Type* ptr;
};
#define ADD_VARIABLE(type, name) \
type name; \
GetInfoVariable<type> variable_ ## name

#define INIT_VARIABLE(name) \
variable_ ## name.init(name); \
add_get_info_variable(#name, dynamic_cast<GetInfoVariableBase*>(&variable_ ## name))
/*!
 * \brief Базовый класс для виджетов связывающий менеджер объектов SharedMemory с виджетом.
 * \par
 * Класс добавляет методы и свойства для состояния связи("коннектора") с SharedMemory.
 * Также в классе реализван метод отрисовки "серой"(неактивной) области поверх виджета,
 * означающей что связь с SharedMemory не установлена или пропала,а при восстановлении
 * связи область не отрисовывается.
*/
class UVoid
{
public :
	UVoid(Glib::Object* obj):
		gpm(NULL)
		,connected_(false)
		,locked_(false)
		,obj_(obj)
	{
		INIT_VARIABLE(connected_);
		INIT_VARIABLE(locked_);
	}
	virtual ~UVoid() {}
	
	/* Methods */
	/*! Получить инфо о виджете.
	 */
	virtual std::string get_info();
	/*! выставить состояние переменной connected_.
	    \param state новое состояние.
	 */
	virtual void set_state(bool state) {}
	/*! добавить блокировку экрана для виджета(для срабатывания АПС сигнала).
	    \param w блокируемый виджет.
	 */
 	virtual void add_lock(const Gtk::Widget& w) { locked_ = true; }
	/*! снять блокировку с виджета(когда АПС сигнал заквитирован).
	*/
 	virtual void unlock_current() { locked_ = false; }
	/*! установить коннектор к SharedMemory.
	    \param connector новый коннектор(см. Connector).
	*/
	virtual void set_connector(const ConnectorRef& connector) throw();
	/*! получить ссылку на текущий коннектор к SharedMemory.
	*/
	ConnectorRef& get_connector();
	/*! отрисовка эффекта отсутствия связи с SharedMemory("серая" область поверх виджета)
	*/
	static void draw_disconnect_effect_1(Cairo::RefPtr<Cairo::Context> cr, const Gdk::Rectangle& rect);
	
	/*! разбиение исходной строки разделительным символом на отдельные строки
	 */
	static std::list<Glib::ustring> explodestr( const Glib::ustring str, char sep );
	

	/* Handlers */
	virtual void on_connect() throw();					/*!< обработчик события появления связи с SharedMemory */
	virtual void on_disconnect() throw();					/*!< обработчик события пропадания связи с SharedMemory */

	/* Properties */
	virtual void set_property_auto_connect(bool state) = 0;			/*!< получить свойство для включения автоматического подключения к SharedMemory */
	virtual bool get_property_auto_connect() = 0;	/*!< получить свойство для включения автоматического подключения к SharedMemory(константный метод) */

	virtual void set_property_disconnect_effect(int state) = 0;			/*!< получить свойство для включения отрисовки эффекта отсутствия связи с SharedMemory */
	virtual int get_property_disconnect_effect() = 0;	/*!< получить свойство для включения  отрисовки эффекта отсутствия связи с SharedMemory(константный метод) */

	virtual void set_property_lock_view(bool state) = 0;				/*!< получить свойство для включения блокирования виджета при срабатывании АПС */
	virtual bool get_property_lock_view() = 0;		/*!< получить свойство для включения блокирования виджета при срабатывании АПС(константный метод) */
	
	virtual void set_property_cache_copy(bool state) = 0;			/*!< получить свойство для копирования картинки pixbufcache */
	virtual bool get_property_cache_copy() = 0;		/*!< получить свойство для копирования картинки pixbufcache(константный метод) */

	/* Variables */
	/*! указатель на менеджер объектов.
	    @todo Remove this public variables.
	*/
	GuiPM* gpm;
	inline void add_get_info_variable(const std::string& name, GetInfoVariableBase* var) { get_info_variables[name] = var; };

protected:
	/* Variables */
	static Blinker blinker;							/*!< мигатель */
	/* connected_ variable is used for local SharedMemory link state */
	ADD_VARIABLE(bool, connected_);						/*!< переменная состояния связи с SharedMemory */
	ADD_VARIABLE(bool, locked_);						/*!< переменная состояния locked */
	ConnectorRef connector_;						/*!< коннектор с SharedMemory */
	sigc::connection disconnect_connection_;				/*!< сигнал отсоединения от SharedMemory */
	sigc::connection connect_connection_;					/*!< сигнал соединения с SharedMemory */

private:
	std::string get_full_widget_name();
	std::unordered_map<std::string, GetInfoVariableBase*> get_info_variables;
	/* Variables */
	Glib::Object* obj_;
};
#endif
