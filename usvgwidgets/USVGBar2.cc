#include "USVGBar2.h"
#include <gdkmm.h>
#include <gtkmm.h>
using namespace std;
using namespace UniWidgetsTypes;


#define USVGBAR2_INIT_PROPERTIES() \
	 ai_                 ( this, "ai"               , get_connector()) \
	,property_value_     (*this, "value"            , 0) \
	\
	,prop_use_inert(*this, "use-inertia",false) \
	,inert(this) \
	\
	,property_min_value_ (*this, "min-value"        , 0) \
	,property_max_value_ (*this, "max-value"        , 100) \
	,property_horizontal_(*this, "horizontal"       , false) \
	,property_bar_start_ (*this, "bar-start"        , 0) \
	,property_bar_end_   (*this, "bar-end"          , 0) \
	,property_linefile_  (*this, "linefile-svg-file", "") \
	,property_topfile_   (*this, "topfile-svg-file" , "") \
	,property_backfile_  (*this, "backfile-svg-file", "") \
	\
	,last_value(0) \
	,value(0) \
	,temp_value(0) \
	
void USVGBar2::ctor()
{
	using sigc::slot;
	using sigc::bind;

	line.property_file_name = &property_linefile_;
	top .property_file_name = &property_topfile_;
	back.property_file_name = &property_backfile_;

	slot<void, PixbufHolder*> image_path_changed =
		sigc::mem_fun(*this, &USVGBar2::load_pixbuf_to_holder);

 	slot<void> topfile_changed  = bind(image_path_changed,  &top);
 	slot<void> backfile_changed = bind(image_path_changed, &back);
 	slot<void> linefile_changed = bind(image_path_changed, &line);

	slot<void> redraw = sigc::mem_fun(*this, &USVGBar2::queue_draw);

	connect_property_changed("linefile-svg-file", linefile_changed);
	connect_property_changed("linefile-svg-file", redraw);
	connect_property_changed("topfile-svg-file" , topfile_changed);
	connect_property_changed("topfile-svg-file" , redraw);
	connect_property_changed("backfile-svg-file", backfile_changed);
	connect_property_changed("backfile-svg-file", redraw);
 
	connect_property_changed("min-value" , redraw);
	connect_property_changed("max-value" , redraw);
	connect_property_changed("bar-start" , redraw);
	connect_property_changed("bar-end"   , redraw);
	connect_property_changed("value", sigc::mem_fun(*this, &USVGBar2::on_value_changed) );
	connect_property_changed("horizontal", redraw);

	missing_image_ref_ = render_icon(Gtk::Stock::MISSING_IMAGE, Gtk::ICON_SIZE_MENU);

	/* Disable UEventBox widget frame drawing */
	set_visible_window(false);
}

USVGBar2::USVGBar2() : 
	Glib::ObjectBase("usvgbar2")
	,USVGBAR2_INIT_PROPERTIES()
{
	ctor();
}

USVGBar2::~USVGBar2()
{
}

USVGBar2::USVGBar2(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,USVGBAR2_INIT_PROPERTIES()
{
	ctor();
}

void USVGBar2::set_sensor_ai(ObjectId sens_id, ObjectId node_id)
{
	/* Set new sens_id */
	ai_.set_sens_id(sens_id);

	/* Set new node_id */
	ai_.set_node_id(node_id);
}

void
USVGBar2::load_pixbuf_to_holder(PixbufHolder* holder)
{
	try {
		holder->origin_ref = Gdk::Pixbuf::create_from_file((*holder->property_file_name).get_value());
		make_resize(*holder, get_allocation());
	}
	catch (...) {
		cerr << "ERROR LOADING FILE:" << *holder->property_file_name << endl;
		holder-> origin_ref.reset();
		holder->resized_ref.reset();
	}
//	catch (const Gdk::PixbufError& er) {
//		cerr << er.what() << endl;
//	}
//	catch (const Glib::Exception& er ) {
//		//TODO::fix error when converting er.what() to locale
//		//cerr <<  er.what() << endl;
//		cerr << "(" << get_name() << "):" << "Error loading file \'" << *property_path << "\'." << endl;
//	}
}

void
USVGBar2::make_resize(PixbufHolder& ph, const Gtk::Allocation& alloc)
{
	const int width  = alloc.get_width();
	const int height = alloc.get_height();

	if ( ph.property_file_name != &property_linefile_) {
		ph.resized_ref = get_pixbuf_from_cache(ph.property_file_name->get_value(), width, height);
	}
	else {
		const int bar_max_size = abs(property_bar_start_ - property_bar_end_);
		if (property_horizontal_)
			ph.resized_ref = get_pixbuf_from_cache(ph.property_file_name->get_value(), bar_max_size, height);
		else
			ph.resized_ref = get_pixbuf_from_cache(ph.property_file_name->get_value(), width, bar_max_size);
	}
}

void
USVGBar2::on_size_allocate(Gtk::Allocation& alloc)
{
	UEventBox::on_size_allocate(alloc);
	if (top .origin_ref) make_resize(top,  alloc);
	if (back.origin_ref) make_resize(back, alloc);
	if (line.origin_ref) make_resize(line, alloc);
}
// -------------------------------------------------------------------------
void USVGBar2::on_inert_value_changed(double val, bool inert_is_stoped)
{
	//	cout<<get_name()<<"::on_inert_value_changed temp_value="<<val<< endl;
	temp_value = val;
	queue_draw();
}
// -------------------------------------------------------------------------
void USVGBar2::on_value_changed()
{
// 	cout<<get_name()<<"::on_value_changed value="<<property_value_.get_value()<< endl;
	long prec_value = property_value_.get_value();
	if(prop_use_inert)
	{
		last_value = temp_value;
		value = prec_value;
		if(prec_value < property_min_value_.get_value())
			value = property_min_value_.get_value();
		else if(prec_value > property_max_value_.get_value())
			value = property_max_value_.get_value();
		
		if (!inert_signal.connected())
		{
			inert_signal= inert.signal_inert_value_change().connect(sigc::mem_fun(*this, &USVGBar2::on_inert_value_changed));
		}
		inert.set_value( last_value, value );
	}
	else
	{
		if (inert_signal.connected())
			inert_signal.disconnect();
		last_value = value;
		value = prec_value;
		if(prec_value < property_min_value_.get_value())
			value = property_min_value_.get_value();
		else if(prec_value > property_max_value_.get_value())
			value = property_max_value_.get_value();
		temp_value = value;
	}
	//	cout<<get_name()<<"::on_value_changed value="<<value<<" temp_value="<<temp_value<< endl;
	
	queue_draw();
}
// -------------------------------------------------------------------------
bool
USVGBar2::on_expose_event(GdkEventExpose* event)
{
	typedef Glib::RefPtr<Gdk::Pixbuf> PixbufRef;
	bool rv= UEventBox::on_expose_event(event);

	long cur_value;
	if(prop_use_inert)
		cur_value = temp_value;
	else
		cur_value = value;
	
	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();

	const Gtk::Allocation alloc = get_allocation();
	const int x = alloc.get_x();
	const int y = alloc.get_y();
	const int width  = alloc.get_width();
	const int height = alloc.get_height();

	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	PixbufRef image_ref = back.resized_ref;
	if (image_ref)
	{
		Gdk::Cairo::set_source_pixbuf(cr, image_ref, x, y);
		cr->paint();
	}
/*	FIXME: Show missing image when back doesn't specified
	if (!image_ref) {
		Gdk::Cairo::rectangle(cr, alloc);
		cr->stroke();
		image_ref = missing_image_ref_;
	}
*/

    image_ref = line.resized_ref;
	if (!image_ref) {
		Gdk::Cairo::rectangle(cr, alloc);
		cr->stroke();
		image_ref = missing_image_ref_;
	}
	Gdk::Cairo::set_source_pixbuf(cr, image_ref, x, y);

	if (property_horizontal_)
	{
		const int bar_start    = x + property_bar_start_;
		const int bar_end      = x + property_bar_end_;
		int bar_max_size = bar_end - bar_start;
		if( bar_max_size == 0 )
			bar_max_size = width;

		float bar_size;

		if      (cur_value < property_min_value_) bar_size = 0;
		else if (cur_value > property_max_value_) bar_size = abs(bar_max_size);
		else bar_size = abs(bar_max_size) / (property_max_value_ - property_min_value_) * cur_value;
		
		if(bar_max_size >= 0)
			cr->rectangle(int(bar_start), y, int(bar_size), height);
		else
			cr->rectangle(int(bar_start - bar_size), y, int(bar_size), height);
		cr->clip();
		cr->paint();
	}
	else if (!property_horizontal_)
	{
		//when verticat we start from bottom
		const int bar_start = y + height - property_bar_start_;
		const int bar_end = y + height - property_bar_end_;
		int bar_max_size = bar_end - bar_start;
		if( bar_max_size == 0 )
			bar_max_size = -height;

		float bar_size;

		if      (cur_value < property_min_value_) bar_size = 0;
		else if (cur_value > property_max_value_) bar_size = abs(bar_max_size);
		else bar_size = abs(bar_max_size) / (property_max_value_ - property_min_value_) * cur_value;
		
		if(bar_max_size >= 0)
			cr->rectangle(x, int(bar_start), width, int(bar_size));
		else
			cr->rectangle(x, int(bar_start - bar_size), width, int(bar_size));
		cr->clip();
		cr->paint();
	}

	cr->reset_clip();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	image_ref = top.resized_ref;
	if (image_ref)
	{
		Gdk::Cairo::set_source_pixbuf(cr, image_ref, x, y);
		cr->paint();
	}
/*	FIXME: Show missing image when top doesn't specified
	if (!image_ref) {
		Gdk::Cairo::rectangle(cr, alloc);
		cr->stroke();
		image_ref = missing_image_ref_;
	}
*/
	if ( !connected_ && get_property_disconnect_effect() == 1) {
		cr->set_line_width(0.5);
		cr->set_source_rgb(1,1,1);
		//TODO: temporary solution
		int max = alloc.get_width();
		if (max < alloc.get_height())
			max = alloc.get_height();
		for(int i = 0; i < max; ++i) {
			cr->move_to(alloc.get_x()+i*3, alloc.get_y());
			cr->line_to(alloc.get_x(),alloc.get_y()+i*3);
		}
		cr->stroke();
	}

	return rv;
}

void 
USVGBar2::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UEventBox::set_connector(connector);

	ai_.set_connector(connector);

	sensor_connection_.disconnect();

	if (get_connector() == true) {
		sigc::slot<void, ObjectId, ObjectId, long> process_sensor_slot = 
				sigc::mem_fun(this, &USVGBar2::process_sensor);
		sensor_connection_ = get_connector()->signals().connect_value_changed(
				process_sensor_slot,
				ai_.get_sens_id(),
				ai_.get_node_id());
	}
}

void 
USVGBar2::on_connect() throw()
{
	UEventBox::on_connect();

	long val = get_connector()->
		get_value(ai_.get_sens_id(), ai_.get_node_id());

	process_sensor(ai_.get_sens_id(), ai_.get_node_id(), val);

	if(get_property_disconnect_effect() > 0)
		queue_draw();
}

void
USVGBar2::on_disconnect() throw()
{
	UEventBox::on_disconnect();

	if(get_property_disconnect_effect() > 0)
		queue_draw();
}

void
USVGBar2::on_realize()
{
	UEventBox::on_realize();
	if(get_connector())
	{
		process_sensor(ai_.get_sens_id(),ai_.get_node_id(),ai_.get_value());
		temp_value = ai_.get_value();
		value = ai_.get_value();
	}
	else
	{
		temp_value = property_value_.get_value();
		value = property_value_.get_value();
	}

}
// -------------------------------------------------------------------------
void
USVGBar2::process_sensor(ObjectId id, ObjectId node, long val)
{
	//TODO: process precision
	property_value_ = val;
}

