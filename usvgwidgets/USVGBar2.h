#ifndef _USVGBAR2_H
#define _USVGBAR2_H

#include <uwidgets/UEventBox.h>
#include <global_macros.h>
#include "uwidgets/UInertia.h"

struct PixbufHolder
{
	Glib::Property<Glib::ustring>* property_file_name;
	Glib::RefPtr<Gdk::Pixbuf> origin_ref;
	Glib::RefPtr<Gdk::Pixbuf> resized_ref;
};

class USVGBar2 : public UEventBox{
private:
	void ctor();

public:
	USVGBar2();
	explicit USVGBar2(GtkmmBaseType::BaseObjectType* gobject);
	virtual ~USVGBar2();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<float> property_value();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<float> property_value() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<float> property_max_value();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<float> property_max_value() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<float> property_min_value();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<float> property_min_value() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<bool> property_horizontal();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<bool> property_horizontal() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<int> property_bar_start();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<int> property_bar_start() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<int> property_bar_end();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<int> property_bar_end() const;

	Glib::PropertyProxy<Glib::ustring> property_linefile();
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_linefile() const;

	Glib::PropertyProxy<Glib::ustring> property_topfile();
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_topfile() const;

	Glib::PropertyProxy<Glib::ustring> property_backfile();
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_backfile() const;

	void set_sensor_ai(UniWidgetsTypes::ObjectId sens_id, UniWidgetsTypes::ObjectId node_id);
	virtual void set_connector(const ConnectorRef& connector) throw();

protected:
	void on_value_changed();

	virtual void on_realize();
	virtual bool on_expose_event(GdkEventExpose* event);
	virtual void on_size_allocate(Gtk::Allocation& alloc);

	virtual void on_connect() throw();
	virtual void on_disconnect() throw();

	double last_value;
	double value;
	double temp_value;
	sigc::connection inert_signal;
	virtual void on_inert_value_changed(double val, bool inert_is_stoped);
	
private:
	USignals::Connection sensor_connection_;

	SensorProp ai_;
	Glib::Property<float> property_value_;
	ADD_PROPERTY( prop_use_inert, bool )						/*!< свойство: использовать инерцию*/
	UInertia inert;
	Glib::Property<float> property_max_value_;
	Glib::Property<float> property_min_value_;
	Glib::Property<bool> property_horizontal_;
	Glib::Property<int> property_bar_start_;
	Glib::Property<int> property_bar_end_;

	Glib::Property<Glib::ustring> property_linefile_;
	Glib::Property<Glib::ustring> property_topfile_;
	Glib::Property<Glib::ustring> property_backfile_;

	Glib::RefPtr<Gdk::Pixbuf> missing_image_ref_;
	PixbufHolder line;
	PixbufHolder top;
	PixbufHolder back;

	void process_sensor(UniWidgetsTypes::ObjectId,UniWidgetsTypes::ObjectId,long);
	void load_pixbuf_to_holder(PixbufHolder* holder);
	void make_resize(PixbufHolder& ph, const Gtk::Allocation& alloc);
};


inline Glib::PropertyProxy<float>
USVGBar2::property_value()
				       {
	return property_value_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<float>
USVGBar2::property_value() const
{
	return Glib::PropertyProxy_ReadOnly<float>(this,"value");
}

inline Glib::PropertyProxy<float>
USVGBar2::property_max_value()
{
	return property_max_value_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<float>
USVGBar2::property_max_value() const
{
	return Glib::PropertyProxy_ReadOnly<float>(this,"max-value");
}

inline Glib::PropertyProxy<float>
USVGBar2::property_min_value()
{
	return property_min_value_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<float>
USVGBar2::property_min_value() const
{
	return Glib::PropertyProxy_ReadOnly<float>(this,"min-value");
}

inline Glib::PropertyProxy<bool>
USVGBar2::property_horizontal()
{
	return property_horizontal_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGBar2::property_horizontal() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"horizontal");
}

inline Glib::PropertyProxy<int>
USVGBar2::property_bar_start()
{
	return property_bar_start_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<int>
USVGBar2::property_bar_start() const
{
	return Glib::PropertyProxy_ReadOnly<int>(this,"bar-start");
}

inline Glib::PropertyProxy<int>
USVGBar2::property_bar_end()
{
	return property_bar_end_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<int>
USVGBar2::property_bar_end() const
{
	return Glib::PropertyProxy_ReadOnly<int>(this,"bar-end");
}

inline Glib::PropertyProxy<Glib::ustring>
USVGBar2::property_linefile()
{
	return property_linefile_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGBar2::property_linefile() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"linefile-svg-file");
}

inline Glib::PropertyProxy<Glib::ustring>
USVGBar2::property_topfile()
{
	return property_topfile_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGBar2::property_topfile() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"topfile-svg-file");
}

inline Glib::PropertyProxy<Glib::ustring>
USVGBar2::property_backfile()
{
	return property_backfile_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGBar2::property_backfile() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"backfile-svg-file");
}

#endif
