#include "USVGButton.h"

using namespace UniWidgetsTypes;

#define INIT_USVGButton_PROPERTIES() \
	property_page(*this, "page", 0) \
	,property_text(*this, "text", "") \
	,property_font_name(*this, "fontname", "Liberation Sans") \
	,property_font_size(*this, "fontsize", -1) \
	,property_alignment(*this, "alignment", Pango::ALIGN_CENTER) \
	,property_curve_radius(*this, "curve-radius", 2) \
	,property_border_width(*this, "border-thickness", 1) \
	,property_border_color(*this, "border-color", Gdk::Color("B3B3B3")) \
	,property_grad_color1(*this, "gradient-color-begin", Gdk::Color("A0A0A0")) \
	,property_grad_color4(*this, "gradient-color-end", Gdk::Color("050505")) \
	,property_grad_color2(*this, "gradient-color-separate1", Gdk::Color("787878")) \
	,property_grad_color3(*this, "gradient-color-separate2", Gdk::Color("232323")) \
	,property_active_grad_color1(*this, "gradient-active-color-begin", Gdk::Color("A0A0A0")) \
	,property_active_grad_color4(*this, "gradient-active-color-end", Gdk::Color("050505")) \
	,property_active_grad_color2(*this, "gradient-active-color-separate1", Gdk::Color("787878")) \
	,property_active_grad_color3(*this, "gradient-active-color-separate2", Gdk::Color("232323")) \
	,property_font_color(*this, "font-color", Gdk::Color("white")) \
	,property_grad_separate(*this, "gradient-separate", true) \
	,property_grad_separate_pos(*this, "gradient-separate-position", 0.30) \
	,property_draw_inactive(*this, "draw-inactive", true) \
	,property_drop_shadow(*this, "drop-shadow", true)
	
//----------------------------------------------------------
void USVGButton::ctor()
{
	connect_property_changed("text", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("fontname", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("fontsize", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("font-color", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("curve-radius", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("border-width", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("border-color", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-color-begin", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-color-end", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-color-separate1", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-color-separate2", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-active-color-begin", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-active-color-end", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-active-color-separate1", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-active-color-separate2", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-separate", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("gradient-separate-position", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("draw-inactive", sigc::mem_fun(*this, &USVGButton::queue_draw) );
	connect_property_changed("drop-shadow", sigc::mem_fun(*this, &USVGButton::queue_draw) );
}
//----------------------------------------------------------

//----------------------------------------------------------
USVGButton::USVGButton() : 
	Glib::ObjectBase("usvgbutton")
	,INIT_USVGButton_PROPERTIES()
{
	ctor();
}
//----------------------------------------------------------
//
//----------------------------------------------------------
USVGButton::USVGButton( const Glib::ustring str) :
	Glib::ObjectBase("usvgbutton")
	,INIT_USVGButton_PROPERTIES()
{
	ctor();
	set_property_text(str);
}
//----------------------------------------------------------

//----------------------------------------------------------
USVGButton::USVGButton(GtkmmBaseType::BaseObjectType* gobject) :
	BaseType(gobject)
	,INIT_USVGButton_PROPERTIES()
{
	ctor();
}
//----------------------------------------------------------

//----------------------------------------------------------
bool USVGButton::on_expose_event(GdkEventExpose *ev)
{
	Gtk::Allocation alloc = get_allocation();
	Cairo::RefPtr<Cairo::Context> cr = 
		get_window()->create_cairo_context();
	switch ( get_state() )
	{
		case Gtk::STATE_NORMAL:
			draw_normal(cr);
			break;
		case Gtk::STATE_ACTIVE:
			draw_active(cr);
			break;
		case Gtk::STATE_PRELIGHT :
			draw_normal(cr);
			break;
		case Gtk::STATE_SELECTED :
			draw_normal(cr);
			break;
		case Gtk::STATE_INSENSITIVE :
			draw_inactive(cr);
		break;
	}
	return true;
}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGButton::draw_normal(Cairo::RefPtr<Cairo::Context> &cr)
{
	cr->save();
	Gtk::Allocation alloc = get_allocation();
	int width = alloc.get_width() - 1 - property_border_width.get_value();
	int height = alloc.get_height() - 1 - property_border_width.get_value();
	double x = alloc.get_x() + 1 + (double)property_border_width.get_value() / 2.0;
	double y = alloc.get_y() + 1 + (double)property_border_width.get_value() / 2.0;

	unsigned int shift = property_curve_radius.get_value();
	if( shift > width / 2.0)
		shift = width / 2.0;
	if( shift > height / 2.0)
		shift = height / 2.0;
	
	cr->set_line_width(property_border_width.get_value());
	cr->move_to(x + shift, y );
	cr->arc( x + width - shift, y + shift , shift, -M_PI/2, 0);
	cr->arc( x + width - shift, y + height - shift, shift, 0, M_PI/2);	
	cr->arc( x + shift, y + height - shift, shift, M_PI/2, M_PI);
	cr->arc( x + shift, y + shift, shift, -M_PI,  -M_PI/2);
	cr->close_path();

	cr->set_source_rgb(0,0,0);
	cr->fill_preserve();
	cr->stroke();
	
	x=x-1;
	y=y-1;

	Cairo::RefPtr<Cairo::Gradient> grad = 
		Cairo::LinearGradient::create( x, y, x, y + height );

	cr->set_line_width(property_border_width.get_value());
	cr->move_to(x + shift, y );
	cr->arc( x + width - shift, y + shift , shift, -M_PI/2, 0);
	cr->arc( x + width - shift, y + height - shift, shift, 0, M_PI/2);	
	cr->arc( x + shift, y + height - shift, shift, M_PI/2, M_PI);
	cr->arc( x + shift, y + shift, shift, -M_PI,  -M_PI/2);
	cr->close_path();

	if(!property_grad_color1.get_value().to_string().empty())
		grad->add_color_stop_rgb(0., property_grad_color1.get_value().get_red_p(), property_grad_color1.get_value().get_green_p(), property_grad_color1.get_value().get_blue_p());
	if(!property_grad_color4.get_value().to_string().empty())
		grad->add_color_stop_rgb(1., property_grad_color4.get_value().get_red_p(), property_grad_color4.get_value().get_green_p(), property_grad_color4.get_value().get_blue_p());
	if( property_grad_separate )
	{
		double grad_pos = get_property_grad_separate_pos();
		if(grad_pos>1.0)
			grad_pos = 1.0;
		else if(grad_pos<0.0)
			grad_pos = 0.0;
		if(!property_grad_color2.get_value().to_string().empty())
			grad->add_color_stop_rgb(grad_pos, property_grad_color2.get_value().get_red_p(), property_grad_color2.get_value().get_green_p(), property_grad_color2.get_value().get_blue_p());
		if(!property_grad_color3.get_value().to_string().empty())
			grad->add_color_stop_rgb(grad_pos, property_grad_color3.get_value().get_red_p(), property_grad_color3.get_value().get_green_p(), property_grad_color3.get_value().get_blue_p());
	}
	cr->set_source(grad);
	cr->fill_preserve();

	Gdk::Cairo::set_source_color(cr, property_border_color.get_value());
	cr->stroke();
	
	cr->restore();

	Glib::RefPtr<Pango::Layout> txt;
	if (property_text.get_value().empty())
		txt = create_pango_layout(property_label());
	else
		txt = create_pango_layout(property_text);
	txt->set_alignment( property_alignment );

	Pango::FontDescription fd( property_font_name );
	if(property_font_size > 0)
		fd.set_absolute_size(property_font_size * Pango::SCALE);
	else
		fd.set_absolute_size(16 * Pango::SCALE);
	txt->set_font_description( fd);
	int w,h;
	txt->get_pixel_size(w,h);
	int text_x = x + (width - w)/2;
	int text_y = y + (height - h)/2;
	cr->save();
	if( property_drop_shadow )
	{
		cr->set_source_rgba(0,0,0,0.5);
		cr->move_to(text_x-1,text_y-1);
		txt->add_to_cairo_context(cr);
		cr->fill();
	}
	Gdk::Cairo::set_source_color(cr, property_font_color.get_value());
	cr->move_to(text_x, text_y);
	txt->add_to_cairo_context(cr);
	cr->fill();
}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGButton::draw_active(Cairo::RefPtr<Cairo::Context> &cr)
{
	cr->save();
	Gtk::Allocation alloc = get_allocation();
	int width = alloc.get_width() - 1 - property_border_width.get_value();
	int height = alloc.get_height() - 1 - property_border_width.get_value();
	double x = alloc.get_x() + 1 + (double)property_border_width.get_value() / 2.0;
	double y = alloc.get_y() + 1 + (double)property_border_width.get_value() / 2.0;

	unsigned int shift = property_curve_radius.get_value();
	if( shift > width / 2.0)
		shift = width / 2.0;
	if( shift > height / 2.0)
		shift = height / 2.0;
	
	Cairo::RefPtr<Cairo::Gradient> grad =
		Cairo::LinearGradient::create( x, y, x, y + height );
		
	cr->set_line_width(property_border_width.get_value());
	cr->move_to(x + shift, y );
	cr->arc( x + width - shift, y + shift , shift, -M_PI/2, 0);
	cr->arc( x + width - shift, y + height - shift, shift, 0, M_PI/2);	
	cr->arc( x + shift, y + height - shift, shift, M_PI/2, M_PI);
	cr->arc( x + shift, y + shift, shift, -M_PI,  -M_PI/2);
	cr->close_path();
	
	if(!property_active_grad_color1.get_value().to_string().empty())
		grad->add_color_stop_rgb(0., property_active_grad_color1.get_value().get_red_p(), property_active_grad_color1.get_value().get_green_p(), property_active_grad_color1.get_value().get_blue_p());
	if(!property_active_grad_color4.get_value().to_string().empty())
		grad->add_color_stop_rgb(1., property_active_grad_color4.get_value().get_red_p(), property_active_grad_color4.get_value().get_green_p(), property_active_grad_color4.get_value().get_blue_p());
	if( property_grad_separate )
	{
		double grad_pos = get_property_grad_separate_pos();
		if(grad_pos>1.0)
			grad_pos = 1.0;
		else if(grad_pos<0.0)
			grad_pos = 0.0;
		if(!property_active_grad_color2.get_value().to_string().empty())
			grad->add_color_stop_rgb(grad_pos, property_active_grad_color2.get_value().get_red_p(), property_active_grad_color2.get_value().get_green_p(), property_active_grad_color2.get_value().get_blue_p());
		if(!property_active_grad_color3.get_value().to_string().empty())
			grad->add_color_stop_rgb(grad_pos, property_active_grad_color3.get_value().get_red_p(), property_active_grad_color3.get_value().get_green_p(), property_active_grad_color3.get_value().get_blue_p());
	}
	cr->set_source(grad);
	cr->fill_preserve();

	Gdk::Cairo::set_source_color(cr, property_border_color.get_value());
	cr->stroke();
	
	cr->restore();

	Glib::RefPtr<Pango::Layout> txt;
	if (property_text.get_value().empty())
		txt = create_pango_layout(property_label());
	else
		txt = create_pango_layout(property_text);
	txt->set_alignment( property_alignment );

	Pango::FontDescription fd( property_font_name );
	if(property_font_size > 0)
		fd.set_absolute_size(property_font_size * Pango::SCALE);
	else
		fd.set_absolute_size(16 * Pango::SCALE);
	txt->set_font_description( fd);
	int w,h;
	txt->get_pixel_size(w,h);
	int text_x = x + (width - w)/2;
	int text_y = y + (height - h)/2;
	cr->save();
	if( property_drop_shadow )
	{
		cr->set_source_rgba(0,0,0,0.5);
		cr->move_to(text_x-1,text_y-1);
		txt->add_to_cairo_context(cr);
		cr->fill();
	}
	Gdk::Cairo::set_source_color(cr, property_font_color.get_value());
	cr->move_to(text_x, text_y);
	txt->add_to_cairo_context(cr);
	cr->fill();
}
//----------------------------------------------------------

void USVGButton::draw_inactive(Cairo::RefPtr<Cairo::Context> &cr)
{
	if( !property_draw_inactive )
		return draw_normal(cr);
	cr->save();
	Gtk::Allocation alloc = get_allocation();
	int width = alloc.get_width() - 1 - property_border_width.get_value();
	int height = alloc.get_height() - 1 - property_border_width.get_value();
	double x = alloc.get_x() + 1 + (double)property_border_width.get_value() / 2.0;
	double y = alloc.get_y() + 1 + (double)property_border_width.get_value() / 2.0;

	unsigned int shift = property_curve_radius.get_value();
	if( shift > width / 2.0)
		shift = width / 2.0;
	if( shift > height / 2.0)
		shift = height / 2.0;

	cr->set_line_width(property_border_width.get_value());
	cr->move_to(x + shift, y );
	cr->arc( x + width - shift, y + shift , shift, -M_PI/2, 0);
	cr->arc( x + width - shift, y + height - shift, shift, 0, M_PI/2);	
	cr->arc( x + shift, y + height - shift, shift, M_PI/2, M_PI);
	cr->arc( x + shift, y + shift, shift, -M_PI,  -M_PI/2);
	cr->close_path();

	cr->set_source_rgb(0,0,0);
	cr->fill_preserve();
	cr->stroke();
	
	x=x-1;
	y=y-1;

	Cairo::RefPtr<Cairo::Gradient> grad = 
		Cairo::LinearGradient::create( x, y, x, y + height );

	cr->set_line_width(property_border_width.get_value());
	cr->move_to(x + shift, y );
	cr->arc( x + width - shift, y + shift , shift, -M_PI/2, 0);
	cr->arc( x + width - shift, y + height - shift, shift, 0, M_PI/2);	
	cr->arc( x + shift, y + height - shift, shift, M_PI/2, M_PI);
	cr->arc( x + shift, y + shift, shift, -M_PI,  -M_PI/2);
	cr->close_path();

	grad->add_color_stop_rgb(0., 160./255,160./255,160./255);
	grad->add_color_stop_rgb(1., 75./255,75./255,75./255);
	if( property_grad_separate )
	{
		grad->add_color_stop_rgb(0.30, 120./255, 120./255, 120./255);
		grad->add_color_stop_rgb(0.31, 105./255, 105./255, 105./255);
	}
	cr->set_source(grad);
	cr->fill_preserve();

	cr->set_source_rgb( 200./255,200./255,200./255 );
	cr->stroke();
	
	cr->restore();

	Glib::RefPtr<Pango::Layout> txt;
	if (property_text.get_value().empty())
		txt = create_pango_layout(property_label());
	else
		txt = create_pango_layout(property_text);
	txt->set_alignment( property_alignment );

	Pango::FontDescription fd( property_font_name );
	if(property_font_size > 0)
		fd.set_absolute_size(property_font_size * Pango::SCALE);
	else
		fd.set_absolute_size(16 * Pango::SCALE);
	txt->set_font_description( fd);
	int w,h;
	txt->get_pixel_size(w,h);
	int text_x = x + (width - w)/2;
	int text_y = y + (height - h)/2;
	cr->save();
	if( property_drop_shadow )
	{
		cr->set_source_rgba(200./255,200./255,200./255,0.5);
		cr->move_to(text_x+1,text_y+1);
		txt->add_to_cairo_context(cr);
		cr->fill();
	}
	cr->move_to(text_x, text_y);
	txt->add_to_cairo_context(cr);
	cr->set_source_rgb(60./255,60./255,60./255);
	cr->fill();
}
//----------------------------------------------------------
USVGButton::~USVGButton()
{
}
//----------------------------------------------------------
void USVGButton::init_widget()
{
}
// ---------------------------------------------------------
