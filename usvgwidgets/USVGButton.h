#ifndef _USVGBUTTON_H
#define _USVGBUTTON_H

#include <gtkmm.h>
#include <UDefaultFunctions.h>
#include <SVGFileProperty.h>
#include <global_macros.h>

class USVGButton : public UDefaultFunctions<Gtk::Button> {
private:
	void ctor();

public:
	USVGButton();
	USVGButton( const Glib::ustring str );
	explicit USVGButton(GtkmmBaseType::BaseObjectType* gobject);
	~USVGButton();
	virtual void init_widget();
	void draw_active( Cairo::RefPtr<Cairo::Context> &cr);
	void draw_normal( Cairo::RefPtr<Cairo::Context> &cr);
	void draw_inactive(Cairo::RefPtr<Cairo::Context> &cr);

	virtual bool on_expose_event(GdkEventExpose *ev);
	int get_page() { return property_page; }
	
private:
	ADD_PROPERTY( property_page, int )						/*!< свойство:*/
	ADD_PROPERTY( property_text, Glib::ustring )			/*!< свойство:*/
	ADD_PROPERTY( property_font_name, Glib::ustring )		/*!< свойство:*/
	ADD_PROPERTY( property_font_size, int )					/*!< свойство:*/
	ADD_PROPERTY( property_alignment, Pango::Alignment )			/*!< свойство:*/
	ADD_PROPERTY( property_font_color, Gdk::Color )			/*!< свойство:*/
	ADD_PROPERTY( property_drop_shadow, bool )				/*!< свойство:*/
	ADD_PROPERTY( property_curve_radius, unsigned int )		/*!< свойство:*/
	ADD_PROPERTY( property_border_width, unsigned int )		/*!< свойство:*/
	ADD_PROPERTY( property_border_color, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_separate, bool )			/*!< свойство:*/
	ADD_PROPERTY( property_grad_separate_pos, double )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_color1, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_color2, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_color3, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_color4, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_active_grad_color1, Gdk::Color )	/*!< свойство:*/
	ADD_PROPERTY( property_active_grad_color2, Gdk::Color )	/*!< свойство:*/
	ADD_PROPERTY( property_active_grad_color3, Gdk::Color )	/*!< свойство:*/
	ADD_PROPERTY( property_active_grad_color4, Gdk::Color )	/*!< свойство:*/
	ADD_PROPERTY( property_draw_inactive, bool )			/*!< свойство:*/
	
};

#endif
