#include "USVGButtonIndicator.h"
#include <iostream>
#include <gtkmm.h>
#include <ConfirmSignal.h>

using namespace std;
using namespace UniWidgetsTypes;

#define USVGBUTTONINDICATOR_PROPERTY_INIT() \
	confirmed_(true) \
	,blinking_(false) \
	,sensor_prop_di_(this,"di",get_connector()) \
	,property_state_(*this,"state",false) \
	,property_blink_(*this, "blink" , false) \
	,property_confirm_(*this, "confirm", false) \
	,property_image0_path_(*this, "image0-path" ) \
	,property_image1_path_(*this, "image1-path" )

void USVGButtonIndicator::ctor()
{
	set_visible_window(false);

	sigc::slot<void, Glib::RefPtr<Gdk::Pixbuf>*,
		const Glib::Property<Glib::ustring>* > image_path_changed =
			sigc::mem_fun(*this, &USVGButtonIndicator::load_pixbuf);

 	sigc::slot<void> image0_path_changed =
		sigc::bind(sigc::bind(image_path_changed, &property_image0_path_), &image0_ref_);
 	sigc::slot<void> image1_path_changed =
		sigc::bind(sigc::bind(image_path_changed, &property_image1_path_), &image1_ref_);

	sigc::slot<void> redraw =
		sigc::mem_fun(*this, &USVGButtonIndicator::queue_draw);

	connect_property_changed("image0-path", image0_path_changed);
	connect_property_changed("image0-path", redraw);
	connect_property_changed("image1-path", image1_path_changed);
	connect_property_changed("image1-path", redraw);
	connect_property_changed("state",redraw);

	missing_image_ref_ = render_icon( Gtk::Stock::MISSING_IMAGE, Gtk::ICON_SIZE_MENU);
}

USVGButtonIndicator::USVGButtonIndicator() :
	Glib::ObjectBase("usvgbuttonindicator")
	,USVGBUTTONINDICATOR_PROPERTY_INIT()
{
	ctor();
}

USVGButtonIndicator::USVGButtonIndicator(const Glib::ustring& image0_path, const Glib::ustring& image1_path) :
	Glib::ObjectBase("usvgbuttonindicator")
	,USVGBUTTONINDICATOR_PROPERTY_INIT()
{
	property_image0_path_ = image0_path;
	load_pixbuf(&image0_ref_, &property_image0_path_);
	property_image1_path_ = image1_path;
	load_pixbuf(&image1_ref_, &property_image1_path_);
	ctor();
}

USVGButtonIndicator::USVGButtonIndicator(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,USVGBUTTONINDICATOR_PROPERTY_INIT()
{
	ctor();
}

bool USVGButtonIndicator::on_expose_event(GdkEventExpose* event)
{
	bool rv = UEventBox::on_expose_event(event);

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Glib::RefPtr<Gdk::Pixbuf> image_ref = property_state_ ? image1_ref_ : image0_ref_;

	const Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	if (!image_ref) {
		cr->set_line_width(1);
		Gdk::Cairo::rectangle(cr, alloc);
		cr->stroke();
		image_ref = missing_image_ref_;
	}

	const int image_width  = image_ref->get_width();
	const int image_height = image_ref->get_height();
	const int image_x = alloc.get_x() + (alloc.get_width()  - image_width )/2;
	const int image_y = alloc.get_y() + (alloc.get_height() - image_height)/2;

	if (!connected_ && get_property_disconnect_effect() == 2) {
		Gtk::IconSource source;
		source.set_pixbuf(image_ref);
		image_ref  = get_style()->
			render_icon(source,
					Gtk::TEXT_DIR_NONE,
					Gtk::STATE_INSENSITIVE,
					Gtk::IconSize((GtkIconSize)-1),
					*this, "");
	}

	// TODO: add property for difrent picture drawing or smth else
 	if (true) {
 		Glib::RefPtr<Gdk::Pixbuf> resized_image_ref =
 			image_ref->scale_simple(alloc.get_width(), alloc.get_height(), Gdk::INTERP_NEAREST);
 		Gdk::Cairo::set_source_pixbuf(cr, resized_image_ref, alloc.get_x(), alloc.get_y());
 	}
 	else
		Gdk::Cairo::set_source_pixbuf(cr, image_ref, image_x, image_y);

	cr->paint();

	if ( !connected_ && get_property_disconnect_effect() == 1)
		UVoid::draw_disconnect_effect_1(cr, alloc);

	return rv;
}

void USVGButtonIndicator::get_size(int* width, int* height)
{
	const int width_true = image1_ref_ ? image1_ref_->get_width() : 0;
	const int width_false = image0_ref_ ? image0_ref_->get_width() : 0;
	*width = (width_true > width_false) ? width_true : width_false;

	const int height_true = image1_ref_ ? image1_ref_->get_height() : 0;
	const int height_false = image0_ref_ ? image0_ref_->get_height() : 0;
	*height = (height_true > height_false) ? height_true : height_false;
}

USVGButtonIndicator::~USVGButtonIndicator()
{
}

void USVGButtonIndicator::init_widget()
{
}

void USVGButtonIndicator::blink( bool blink_state, int time)
{
	property_state_ = blink_state;
}

void
USVGButtonIndicator::set_state(bool val)
{
	property_state_.set_value(val);
}

void
USVGButtonIndicator::load_pixbuf(Glib::RefPtr<Gdk::Pixbuf>* image_ptr, const Glib::Property<Glib::ustring>* property_path )
{
	try {
		*image_ptr = Gdk::Pixbuf::create_from_file((*property_path).get_value());
	}
	catch (...) {
		cerr << "ERROR LOADING FILE:" << *property_path << endl;
	}
//	catch (const Gdk::PixbufError& er) {
//		cerr << er.what() << endl;
//	}
//	catch (const Glib::Exception& er ) {
//		//TODO::fix error when converting er.what() to locale
//		//cerr <<  er.what() << endl;
//		cerr << "(" << get_name() << "):" << "Error loading file \'" << *property_path << "\'." << endl;
//	}
}

void
USVGButtonIndicator::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UEventBox::set_connector(connector);

	sensor_connection_.disconnect();
	message_connection_.disconnect();
	confirm_connection_.disconnect();

	sensor_state_ = 0;
	stop_blink();
	property_state_ = false;

	if (get_connector() == true) {
		sigc::slot<void, ObjectId, ObjectId, long> process_sensor_slot =
				sigc::mem_fun(this, &USVGButtonIndicator::process_sensor);
		sensor_connection_ = get_connector()->signals().connect_value_changed(
				process_sensor_slot,
				sensor_prop_di_.get_sens_id(),
				sensor_prop_di_.get_node_id());
		if (property_confirm_) {
			sigc::slot<void, UMessages::MessageId, Glib::ustring> process_message_slot =
				sigc::mem_fun(this, &USVGButtonIndicator::process_message);
			UMessages::MessageId id(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id(), 1);
			message_connection_ = get_connector()->signals().connect_on_message(process_message_slot, id);
		}
	}
	sensor_prop_di_.set_connector(connector);
}

void
USVGButtonIndicator::on_connect() throw()
{
	UEventBox::on_connect();

	long value = get_connector()->
		get_value(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id());

	sensor_state_ = value;

	if (property_confirm_) {
		UMessages::MessageId id(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id(), 1);
		MessageStatus status = get_connector()->signal_confirm().
			get_message_status(id);

		if (value == 1)
			property_state_ = true;
		else if (status == WAITS_CONFIRM)
			process_message(id, Glib::ustring());
	}
	else
		process_sensor(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id(), value);

	if(get_property_disconnect_effect() > 0)
		queue_draw();
}

void
USVGButtonIndicator::on_disconnect() throw()
{
	UEventBox::on_disconnect();

	stop_blink();

	//TODO: reduce code duplication (see process_confirm())
	if (confirmed_ == false) {
		property_state_ = 1;
		confirmed_ = true;
		confirm_connection_.disconnect();
		if (get_property_lock_view())
			unlock_current();
	}

	queue_draw();
}

void
USVGButtonIndicator::process_sensor(ObjectId id, ObjectId node, long value)
{
	//TODO:use sensor_state_ from signals
	sensor_state_ = value;

	if (confirmed_)
		property_state_ = (value == 1);

	if (!property_confirm_ && property_blink_)
		if(value == 1)
			start_blink();
		else
			stop_blink();
}

void
USVGButtonIndicator::process_message(UMessages::MessageId id, Glib::ustring) {
	if (confirmed_) {
		confirmed_ = false;
		property_state_ = true;
		confirm_connection_ = get_connector()->signal_confirm().connect(
				sigc::mem_fun(this,&USVGButtonIndicator::process_confirm), id);
		if ( property_blink_ )
			start_blink();
		if (get_property_lock_view())
			add_lock(*this);
	}
}

void USVGButtonIndicator::process_confirm(UMessages::MessageId id, time_t)
{
	confirmed_ = true;
	if (blinking_)
		stop_blink();
	if (sensor_state_ != 1)
		property_state_ = false;
	if (get_property_lock_view())
		unlock_current();
}

void
USVGButtonIndicator::start_blink()
{
	if (blinking_)
		return;
	blinking_ = true;
	blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].
		connect(sigc::mem_fun(this, &USVGButtonIndicator::blink));
}

void
USVGButtonIndicator::stop_blink()
{
	if (!blinking_)
		return;
	blinking_ = false;
	blink_connection_.disconnect();
	//TODO:use sensor_state_ from signals
	property_state_ = (sensor_state_ == 1);
}

bool USVGButtonIndicator::on_button_press_event(GdkEventButton*event)
{
	bool rv = UEventBox::on_button_press_event(event);
	if (get_connector()->connected())
		sensor_prop_di_.save_value(true);
	return rv;
}

bool USVGButtonIndicator::on_button_release_event(GdkEventButton*event)
{
	bool rv = UEventBox::on_button_release_event(event);
	if (get_connector()->connected())
		sensor_prop_di_.save_value(false);
	return rv;
}

void USVGButtonIndicator::on_clicked()
{
    /*BaseType::on_clicked();*/
    if (get_connector()->connected())
        sensor_prop_di_.save_value(sensor_state_);
}
