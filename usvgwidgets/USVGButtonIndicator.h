#ifndef _USVGBUTTONINDICATOR_H
#define _USVGBUTTONINDICATOR_H

#include <ConfirmSignal.h>
#include <USignals.h>
#include <uwidgets/UEventBox.h>
#include <gdkmm.h>

class USVGButtonIndicator : public UEventBox{
public:
	USVGButtonIndicator();
	USVGButtonIndicator(const Glib::ustring& image0_path, const Glib::ustring& image1_path);
	explicit USVGButtonIndicator(GtkmmBaseType::BaseObjectType* gobject);
	~USVGButtonIndicator();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<bool> property_blink();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<bool> property_blink() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<bool> property_confirm();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<bool> property_confirm() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<Glib::ustring> property_image0_path();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_image0_path() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<Glib::ustring> property_image1_path();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_image1_path() const;


	/** TODO: write documentation
	 *
	 */
	bool get_state() const;

	/** TODO: write documentation
	 *
	 */
	virtual void set_state(bool state);

	/** TODO: write documentation
	 *
	 */
	void get_size(int* width, int* height);

	SensorProp* get_di() { return &sensor_prop_di_; }

	virtual void init_widget();
    virtual void on_clicked();

	virtual void set_connector(const ConnectorRef& connector) throw();
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();

protected:
	virtual bool on_expose_event(GdkEventExpose*);
	virtual bool on_button_release_event(GdkEventButton*event);
	virtual bool on_button_press_event(GdkEventButton*event);

private:
	void ctor();

	Glib::RefPtr<Gdk::Pixbuf> image0_ref_;
	Glib::RefPtr<Gdk::Pixbuf> image1_ref_;
	Glib::RefPtr<Gdk::Pixbuf> missing_image_ref_;
	
	bool confirmed_;
	bool blinking_;
	bool sensor_state_;
	sigc::connection blink_connection_;
	USignals::Connection sensor_connection_;
	MsgConfirmConnection confirm_connection_;
	USignals::Connection message_connection_;

	SensorProp sensor_prop_di_;
	Glib::Property<bool> property_state_;
	Glib::Property<bool> property_blink_;
	Glib::Property<bool> property_confirm_;
	Glib::Property<Glib::ustring> property_image0_path_;
	Glib::Property<Glib::ustring> property_image1_path_;

	void blink(bool state, int time=DEFAULT_BLINK_TIME);
	void start_blink();
	void stop_blink();

	void process_sensor(UniWidgetsTypes::ObjectId,UniWidgetsTypes::ObjectId,long);
	void process_message(UMessages::MessageId id, Glib::ustring);
	void process_confirm(UMessages::MessageId id, time_t);


	void load_pixbuf( Glib::RefPtr<Gdk::Pixbuf>* image_ptr, const Glib::Property<Glib::ustring>* property_path);
};

inline bool
USVGButtonIndicator::get_state() const
{
	return property_state_.get_value();
}

inline Glib::PropertyProxy<bool>
USVGButtonIndicator::property_blink()
{
	return property_blink_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGButtonIndicator::property_blink() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"blink");
}

inline Glib::PropertyProxy<bool>
USVGButtonIndicator::property_confirm()
{
	return property_confirm_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGButtonIndicator::property_confirm() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"confirm");
}

inline Glib::PropertyProxy<Glib::ustring>
USVGButtonIndicator::property_image0_path()
{
	return property_image0_path_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGButtonIndicator::property_image0_path() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"image0-path");
}

inline Glib::PropertyProxy<Glib::ustring>
USVGButtonIndicator::property_image1_path()
{
	return property_image1_path_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGButtonIndicator::property_image1_path() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"image1-path");
}
#endif
