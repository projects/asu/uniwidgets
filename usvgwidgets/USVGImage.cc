#include <USVGImage.h>
#include <iostream>
#include <gdkmm.h>
#include <gtkmm.h>

using namespace std;
using namespace UniWidgetsTypes;

void USVGImage::ctor()
{
	missing_image_ref_ = render_icon(Gtk::Stock::MISSING_IMAGE, Gtk::ICON_SIZE_MENU);
	set_visible_window(false);

	sigc::slot<void, Glib::RefPtr<Gdk::Pixbuf>*,
		const Glib::Property<Glib::ustring>* > image_path_changed =
			sigc::mem_fun(*this, &USVGImage::load_pixbuf);

 	sigc::slot<void> image0_path_changed =
		sigc::bind(sigc::bind(image_path_changed, &property_image0_path_), &image0_ref_);

	sigc::slot<void> redraw =
		sigc::mem_fun(*this, &USVGImage::queue_draw);

	image0_path_changed();
	connect_property_changed("svg-file", image0_path_changed);
	connect_property_changed("svg-file", redraw);
	connect_property_changed("state",    redraw);

}

USVGImage::USVGImage() :
	Glib::ObjectBase("usvgimage")
	,property_image0_path_(*this,"svg-file","")
{
	ctor();
}

USVGImage::USVGImage(Glib::ustring svgfile) :
	Glib::ObjectBase("usvgimage")
	,property_image0_path_(*this,"svg-file", svgfile)
{
	property_image0_path_ = svgfile;
	ctor();
}

USVGImage::USVGImage(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,property_image0_path_(*this,"svg-file","")
{
	ctor();
}

bool USVGImage::on_expose_event(GdkEventExpose* event)
{
	bool rv = UEventBox::on_expose_event(event);

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Glib::RefPtr<Gdk::Pixbuf> image_ref = image0_ref_;

	const Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	if (!image_ref) {
		cr->set_line_width(1);
		Gdk::Cairo::rectangle(cr, alloc);
		cr->stroke();
		image_ref = missing_image_ref_;
	}

	const int image_width  = image_ref->get_width();
	const int image_height = image_ref->get_height();
	const int image_x = alloc.get_x() + (alloc.get_width()  - image_width )/2;
	const int image_y = alloc.get_y() + (alloc.get_height() - image_height)/2;

	if (!connected_ && get_property_disconnect_effect() == 2) {
		Gtk::IconSource source;
		source.set_pixbuf(image_ref);
		image_ref  = get_style()->
			render_icon(source,
					Gtk::TEXT_DIR_NONE,
					Gtk::STATE_INSENSITIVE,
					Gtk::IconSize((GtkIconSize)-1),
					*this, "");
	}

	// TODO: add property for difrent picture drawing or smth else
	if (full_redraw) {
//		Glib::RefPtr<Gdk::Pixbuf> resized_image_ref;
		image_ref = get_pixbuf_from_cache(property_image0_path_.get_value(), width, height);
		Gdk::Cairo::set_source_pixbuf(cr, image_ref, alloc.get_x(), alloc.get_y());
		image0_ref_ = image_ref;
	}
 	else
		Gdk::Cairo::set_source_pixbuf(cr, image_ref, image_x, image_y);

	cr->paint();

	if ( !connected_ && get_property_disconnect_effect() == 1)
 		UVoid::draw_disconnect_effect_1(cr, alloc);

	return rv;
}

USVGImage::~USVGImage()
{
}

void
USVGImage::load_pixbuf(Glib::RefPtr<Gdk::Pixbuf>* image_ptr, const Glib::Property<Glib::ustring>* property_path )
{
	try {
		*image_ptr = Gdk::Pixbuf::create_from_file((*property_path).get_value());
	}
	catch (...) {
		cerr << "ERROR LOADING FILE:" << *property_path << endl;
	}
//	catch (const Gdk::PixbufError& er) {
//		cerr << er.what() << endl;
//	}
//	catch (const Glib::Exception& er ) {
//		//TODO::fix error when converting er.what() to locale
//		//cerr <<  er.what() << endl;
//		cerr << "(" << get_name() << "):" << "Error loading file \'" << *property_path << "\'." << endl;
//	}
}
