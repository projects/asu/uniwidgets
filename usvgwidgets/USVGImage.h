#ifndef _USVGIMAGE_H
#define _USVGIMAGE_H

#include <uwidgets/UEventBox.h>
#include <SVGLoader.h>
#include <SVGFileProperty.h>

class USVGImage : public UEventBox
{
public:
	USVGImage();
	USVGImage(Glib::ustring svgimage);
	explicit USVGImage(GtkmmBaseType::BaseObjectType* gobject);
	~USVGImage();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<Glib::ustring> property_image0_path();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_image0_path() const;

protected:
	virtual bool on_expose_event(GdkEventExpose*);
	void on_svg_file_changed(){};
	Glib::RefPtr<Gdk::Pixbuf> image0_ref_;

private:
	void ctor();
	Glib::Property<Glib::ustring> property_image0_path_;

	Glib::RefPtr<Gdk::Pixbuf> missing_image_ref_;

	virtual void load_pixbuf( Glib::RefPtr<Gdk::Pixbuf>* image_ptr, const Glib::Property<Glib::ustring>* property_path);
};

inline Glib::PropertyProxy<Glib::ustring>
USVGImage::property_image0_path()
{
	return property_image0_path_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGImage::property_image0_path() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"svg-file");
}

#endif
