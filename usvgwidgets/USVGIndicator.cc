#include <USVGIndicator.h>
#include <iostream>
#include <librsvg/rsvg-cairo.h>

using namespace std;
using namespace UniWidgetsTypes;

USVGIndicator::StaticPictures USVGIndicator::s_pics ("USVGIndicator_defaults");

//----------------------------------------------------------
void USVGIndicator::on_state_changed()
{
	if ( get_state() )
		curr_file = &s_pics.l_true;
	else 
		curr_file = &s_pics.l_false;
	queue_draw();
//	cerr << "state changed" << endl;
}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGIndicator::ctor()
{
	set_visible_window(false);
	curr_file = &s_pics.l_false;
	connect_property_changed("state", sigc::mem_fun(*this, &USVGIndicator::on_state_changed) );
}
//----------------------------------------------------------

//----------------------------------------------------------
USVGIndicator::USVGIndicator() : 
	Glib::ObjectBase("usvgindicator")
	,di(this,"di", get_connector())
	,state(*this,"state",false)
{
	ctor();
}
//----------------------------------------------------------

//----------------------------------------------------------
USVGIndicator::USVGIndicator(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,di(this,"di", get_connector())
	,state(*this,"state",false)
{
	ctor();
}
//----------------------------------------------------------

//----------------------------------------------------------
bool USVGIndicator::on_expose_event(GdkEventExpose* event)
{
	bool rv= UEventBox::on_expose_event(event);

	Gtk::Allocation alloc = get_allocation();

	surface = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, alloc.get_width(), alloc.get_height());
	if ( surface != 0 )
		curr_file->renderSurface(surface, alloc.get_width(), alloc.get_height()); // !!! �������� !!!


	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
//	cr->set_source_rgb(0.0,0.0,0.0);
	cr->set_source(surface, alloc.get_x(), alloc.get_y());
	cr->paint();
	return rv;
}
//----------------------------------------------------------

//----------------------------------------------------------
USVGIndicator::~USVGIndicator()
{
}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGIndicator::init_widget()
{
//	queue_draw();
}

// ---------------------------------------------------------
