#ifndef _USVGINDICATOR_H
#define _USVGINDICATOR_H

#include <uwidgets/UEventBox.h>
#include <SVGLoader.h>
#include <librsvg/rsvg.h>
#include <global_macros.h>
#include <types.h>

class USVGIndicator : public UEventBox{
public:
	class StaticPictures 
	{
	public:
		std::string name;
		SVGLoader l_true;
		SVGLoader l_false;

		StaticPictures(std::string n = std::string("unnnamed") ) : name(n)
		{
			load_file(l_true,true_svg);
			load_file(l_false,false_svg);
		}

		void load_file(SVGLoader &loader ,std::string path)
		{
			try 
			{ 
				l_true.loadRsvgFile(path);
			} 
			catch ( std::string err )
			{
				std::cerr << "(" << name <<")"<< err << std::endl;
			}
		}
	};


	USVGIndicator();
	explicit USVGIndicator(GtkmmBaseType::BaseObjectType* gobject);
	~USVGIndicator();
	virtual void init_widget();
	virtual bool on_expose_event(GdkEventExpose* );


private:
	void ctor();

	static StaticPictures s_pics;
	SVGLoader* curr_file;

	
protected:
	void on_state_changed();
	Cairo::RefPtr<Cairo::Surface> surface;
	
	SensorProp di;
	ADD_PROPERTY(state,bool)
};

#endif
