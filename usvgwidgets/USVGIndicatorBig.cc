#include "USVGIndicatorBig.h"

#include <gdkmm.h>
#include <iostream>

using namespace std;
using namespace UniWidgetsTypes;
using UMessages::MessageId;

#define USVGINDICATORBIG_INIT_PROPERTIES() \
	confirmed_(true) \
	,blinking_(false) \
	,di(this,"di", get_connector()) \
	,property_truefile(*this, m_indicator.property_image1_path(), "truefile", "true.svg") \
	,property_falsefile(*this, m_indicator.property_image0_path(), "falsefile", "false.svg") \
	\
	,property_vcode_text(*this, m_vcode.property_text(), "vcode", "00.00") \
	,property_vcode_font_name(*this, m_vcode.property_font_name(), "vcode-font-name", "Liberation Sans Bold 11") \
	,property_vcode_abs_font_size(*this, m_vcode.property_abs_font_size(), "vcode-abs-font-size", -1) \
	,property_vcode_off_color(*this, m_vcode.property_font_color(), "vcode-off-color", Gdk::Color("black") ) \
	,property_vcode_on_color(*this, m_vcode.property_on_font_color(), "vcode-on-color" , Gdk::Color("red") ) \
	,property_vcode_off_transparency(*this, m_vcode.property_transparency(), "vcode-off-transparency", 1.) \
	,property_vcode_position(*this, "vcode-position", Gtk::POS_BOTTOM) \
	\
	,property_text(*this, m_text.property_text(), "text", "Text") \
	,property_text_font_name(*this, m_text.property_font_name(), "text-font-name", "Liberation Sans Bold 12") \
	,property_text_abs_font_size(*this, m_text.property_abs_font_size(), "text-abs-font-size", -1) \
	,property_text_off_color(*this, m_text.property_font_color(), "text-off-color", Gdk::Color("black") ) \
	,property_text_on_color(*this, m_text.property_on_font_color(), "text-on-color" , Gdk::Color("red") ) \
	,property_text_position(*this, "text-position", Gtk::POS_BOTTOM) \
	\
	,property_pic_width(*this, "pic-width", 50) \
	,property_pic_height(*this, "pic-height", 37) \
	,property_vcode_width(*this, "vcode-width", 20) \
	,property_vcode_height(*this, "vcode-height", 5) \
	\
	,property_state_(*this, "state", false) \
	,property_blink_(*this, "blink" , false) \
	,property_confirm_(*this, "confirm", false)

//----------------------------------------------------------
void USVGIndicatorBig::ctor()
{
	push_composite_child(); {
		m_indicator.set_property_auto_connect(false);
		m_indicator.set_property_disconnect_effect(0);
		m_vbox.pack_start(m_indicator,Gtk::PACK_SHRINK);
		m_vcode.property_alignment() = Pango::ALIGN_CENTER;
		m_vcode.property_use_theme() = false;
		m_vcode.set_property_auto_connect(false);
		m_vcode.set_property_disconnect_effect(0);
		m_vbox.pack_start(m_vcode,Gtk::PACK_EXPAND_PADDING);
		m_hbox.pack_start(m_vbox, Gtk::PACK_SHRINK);

		m_text.property_use_theme() = false;
		m_text.property_alignment() = Pango::ALIGN_LEFT;
		m_text.set_property_auto_connect(false);
		m_text.set_property_disconnect_effect(0);
		m_align.set(Gtk::ALIGN_LEFT,Gtk::ALIGN_CENTER, 0.0, 0.0);
		m_align.add(m_text);
		m_hbox.pack_start(m_align, Gtk::PACK_EXPAND_WIDGET);

		add(m_hbox);
	}
	pop_composite_child();

	show_all();

	set_visible_window(false);

	property_truefile.connect_processing();
	property_falsefile.connect_processing();
	property_vcode_text.connect_processing();
	property_vcode_font_name.connect_processing();
	property_vcode_abs_font_size.connect_processing();
	property_vcode_on_color.connect_processing();
	property_vcode_off_color.connect_processing();
	property_vcode_off_transparency.connect_processing();

	property_text.connect_processing();
	property_text_font_name.connect_processing();
	property_text_abs_font_size.connect_processing();
	property_text_on_color.connect_processing();
	property_text_off_color.connect_processing();

	connect_property_changed("disconnect-effect", sigc::mem_fun(*this, &USVGIndicatorBig::on_disconnect_effect_changed));

	connect_property_changed("truefile", sigc::mem_fun(*this, &USVGIndicatorBig::resize_composits) );
	connect_property_changed("falsefile", sigc::mem_fun(*this, &USVGIndicatorBig::resize_composits) );
	connect_property_changed("state", sigc::mem_fun(*this, &USVGIndicatorBig::on_state_changed));
	connect_property_changed("vcode-position", sigc::mem_fun(*this, &USVGIndicatorBig::on_vcode_position_changed));
	connect_property_changed("text-position", sigc::mem_fun(*this, &USVGIndicatorBig::on_text_position_changed));
	connect_property_changed("pic-width", sigc::mem_fun(*this, &USVGIndicatorBig::queue_resize));
	connect_property_changed("pic-height", sigc::mem_fun(*this, &USVGIndicatorBig::queue_resize));
	connect_property_changed("vcode-width", sigc::mem_fun(*this, &USVGIndicatorBig::queue_resize));
	connect_property_changed("vcode-height", sigc::mem_fun(*this, &USVGIndicatorBig::queue_resize));
}

USVGIndicatorBig::USVGIndicatorBig(const Glib::ustring& image0_path, const Glib::ustring& image1_path) :
	Glib::ObjectBase("usvgindicatorbig")
	,m_indicator(image0_path, image1_path)
	,USVGINDICATORBIG_INIT_PROPERTIES()
{
	property_falsefile.set_value(image0_path);
	property_truefile.set_value(image1_path);
	ctor();
}

USVGIndicatorBig::USVGIndicatorBig() :
	Glib::ObjectBase("usvgindicatorbig")
	,USVGINDICATORBIG_INIT_PROPERTIES()
{
	ctor();
}

USVGIndicatorBig::USVGIndicatorBig(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,USVGINDICATORBIG_INIT_PROPERTIES()
{
	ctor();
}

USVGIndicatorBig::~USVGIndicatorBig()
{
}

bool
USVGIndicatorBig::on_expose_event(GdkEventExpose* event)
{
	UEventBox::on_expose_event(event);

	const Gtk::Allocation alloc = get_allocation();
	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();

	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	if ( !connected_ && get_property_disconnect_effect() == 1)
		UVoid::draw_disconnect_effect_1(cr, alloc);

	return true;
}

void
USVGIndicatorBig::on_size_allocate(Gtk::Allocation& allocation)
{
	UEventBox::on_size_allocate(allocation);
	resize_composits();
}
void
USVGIndicatorBig::on_hierarchy_changed(Gtk::Widget* top_level)
{
	if ( !get_property_auto_connect() )
		return;

	ConnectorRef connector = get_connector_from_hierarchy();
	set_connector(connector);
}

void
USVGIndicatorBig::resize_composits()
{
	m_indicator.set_size_request( property_pic_width, property_pic_height);
}

void
USVGIndicatorBig::on_state_changed()
{
	m_indicator.set_state( property_state_ );
	m_vcode.property_state() = property_state_;
	m_vcode.property_drop_shadow() = property_state_;
	m_text.property_state() =  property_state_;
	m_text.property_drop_shadow() = property_state_;
}

void
USVGIndicatorBig::on_vcode_position_changed()
{
	switch (property_vcode_position.get_value())
	{
		case Gtk::POS_TOP:
			m_vbox.reorder_child(m_vcode, 0);
			m_vbox.reorder_child(m_indicator, 1);
		break;
		case Gtk::POS_BOTTOM:
			m_vbox.reorder_child(m_indicator, 0);
			m_vbox.reorder_child(m_vcode, 1);
		break;
		default:
		break;
	}
}

void
USVGIndicatorBig::on_text_position_changed()
{
	switch (property_text_position.get_value())
	{
		case Gtk::POS_LEFT:
			m_align.set(Gtk::ALIGN_RIGHT, Gtk::ALIGN_CENTER, 0.0, 0.0);
			m_text.property_alignment() = Pango::ALIGN_RIGHT;
			m_hbox.reorder_child(m_align, 0);
			m_hbox.reorder_child(m_vbox,1);
		break;
		case Gtk::POS_RIGHT:
			m_align.set(Gtk::ALIGN_LEFT, Gtk::ALIGN_CENTER, 0.0, 0.0);
			m_text.property_alignment() = Pango::ALIGN_LEFT;
			m_hbox.reorder_child(m_vbox,0);
			m_hbox.reorder_child(m_align, 1);
		break;
		default:
		break;
	}
}

void
USVGIndicatorBig::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UEventBox::set_connector(connector);

	sensor_connection_.disconnect();
	message_connection_.disconnect();
	confirm_connection_.disconnect();

	sensor_state_ = 0;
	stop_blink();
	property_state_ = false;

	if (get_connector() == true) {
		sigc::slot<void, ObjectId, ObjectId, long> process_sensor_slot =
				sigc::mem_fun(this, &USVGIndicatorBig::process_sensor);
		sensor_connection_ = get_connector()->signals().connect_value_changed(
				process_sensor_slot,
				di.get_sens_id(),
				di.get_node_id());
		if (property_confirm_) {
			sigc::slot<void, UMessages::MessageId, Glib::ustring> process_message_slot =
				sigc::mem_fun(this, &USVGIndicatorBig::process_message);
			UMessages::MessageId id(di.get_sens_id(), di.get_node_id(), 1);
			message_connection_ = get_connector()->signals().connect_on_message(process_message_slot, id);
		}
		on_connect();
	}
}

void
USVGIndicatorBig::on_connect() throw()
{
	UEventBox::on_connect();

	long value = get_connector()->
		get_value(di.get_sens_id(), di.get_node_id());

	if (property_confirm_) {
		sensor_state_ = value;
		UMessages::MessageId id(di.get_sens_id(), di.get_node_id(), 1);
		MessageStatus status = get_connector()->signal_confirm().
			get_message_status(id);

		if (value == 1)
			property_state_ = true;
		else if (status == WAITS_CONFIRM)
			process_message(id, Glib::ustring());
	}
	else
		process_sensor(di.get_sens_id(), di.get_node_id(), value);

	if (get_property_disconnect_effect() > 0)
		queue_draw();

	if (get_property_disconnect_effect() == 2) {
		m_indicator.set_property_disconnect_effect(0);
		m_text.set_property_disconnect_effect(0);
		m_vcode.set_property_disconnect_effect(0);
	}
}

void
USVGIndicatorBig::on_disconnect() throw()
{
	UEventBox::on_disconnect();

	stop_blink();

	//TODO: reduce code duplication (see process_confirm())
	if (confirmed_ == false) {
		property_state_ = 1;
		confirmed_ = true;
		confirm_connection_.disconnect();
		if (get_property_lock_view())
			unlock_current();
	}

	if (get_property_disconnect_effect() == 2) {
		m_indicator.set_property_disconnect_effect(2);
		m_text.set_property_disconnect_effect(2);
		m_vcode.set_property_disconnect_effect(2);
	}

	queue_draw();
}

void
USVGIndicatorBig::process_sensor(ObjectId id, ObjectId node, long value)
{
	//TODO:use sensor_state_ from signals
	sensor_state_ = value;

	if (confirmed_)
		property_state_ = (value == 1);

	if (!property_confirm_ && property_blink_)
		if(value == 1)
			start_blink();
		else
			stop_blink();
}

void
USVGIndicatorBig::process_message(UMessages::MessageId id, Glib::ustring)
{
	if (confirmed_) {
		confirmed_ = false;
		property_state_ = true;
		confirm_connection_ = get_connector()->signal_confirm().connect(
				sigc::mem_fun(this,&USVGIndicatorBig::process_confirm), id);
		if ( property_blink_ )
			start_blink();
		if (get_property_lock_view())
			add_lock(*this);
	}
}

void
USVGIndicatorBig::process_confirm(UMessages::MessageId id, time_t)
{
	confirmed_ = true;
	if (blinking_)
		stop_blink();
	if (sensor_state_ != 1)
		property_state_ = false;
	if (get_property_lock_view())
		unlock_current();
}

void USVGIndicatorBig::blink( bool blink_state , int time)
{
	property_state_ = blink_state;
}

void
USVGIndicatorBig::start_blink()
{
	if (blinking_)
		return;
	blinking_ = true;
	blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].
		connect(sigc::mem_fun(this, &USVGIndicatorBig::blink));
}

void
USVGIndicatorBig::stop_blink()
{
	if (!blinking_)
		return;
	blinking_ = false;
	blink_connection_.disconnect();
	//TODO:use sensor_state_ from signals
	property_state_ = (sensor_state_ == 1);
}

void
USVGIndicatorBig::on_disconnect_effect_changed()
{
	if (get_property_disconnect_effect() == 2) {
		m_indicator.set_property_disconnect_effect(2);
		m_text.set_property_disconnect_effect(2);
		m_vcode.set_property_disconnect_effect(2);
	}
}
