#ifndef _USVGINDICATORBIG_H
#define _USVGINDICATORBIG_H

#include <gtkmm.h>
#include <uwidgets/UEventBox.h>
#include <usvgwidgets/USVGIndicatorPlus.h>
#include <usvgwidgets/USVGTextPlus.h>
#include <ConfirmSignal.h>
#include <TransProperty.h>
#include <iostream>


enum txt_pos { TOP_POS, BOTTOM_POS };

class USVGIndicatorBig : public UEventBox{

public:
	USVGIndicatorBig();
	USVGIndicatorBig(const Glib::ustring& image0_path, const Glib::ustring& image1_path);
	explicit USVGIndicatorBig(GtkmmBaseType::BaseObjectType* gobject);
	~USVGIndicatorBig();

	virtual void set_state(bool state) { property_state_ = state; }

	virtual void set_connector(const ConnectorRef& connector) throw();
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();

protected:
	virtual bool on_expose_event(GdkEventExpose* event);
	virtual void on_size_allocate(Gtk::Allocation& allocation);
	virtual void on_hierarchy_changed(Gtk::Widget* top_level);

private:
	bool confirmed_;
	bool blinking_;
	bool sensor_state_;

	Gtk::HBox m_hbox;
	Gtk::Alignment m_align;
	Gtk::VBox m_vbox;
	USVGIndicatorPlus m_indicator;
	USVGTextPlus m_vcode;
	USVGTextPlus m_text;

	sigc::connection blink_connection_;
	USignals::Connection sensor_connection_;
	MsgConfirmConnection confirm_connection_;
	USignals::Connection message_connection_;

	SensorProp di;
	TransProperty<Glib::ustring> property_truefile;
	TransProperty<Glib::ustring> property_falsefile;

	TransProperty<Glib::ustring> property_vcode_text;
	TransProperty<Glib::ustring> property_vcode_font_name;
	TransProperty<int> property_vcode_abs_font_size;
	TransProperty<Gdk::Color> property_vcode_off_color;
	TransProperty<Gdk::Color> property_vcode_on_color;
	TransProperty<double> property_vcode_off_transparency;
	Glib::Property<Gtk::PositionType> property_vcode_position;

	TransProperty<Glib::ustring> property_text;
	TransProperty<Glib::ustring> property_text_font_name;
	TransProperty<int> property_text_abs_font_size;
	TransProperty<Gdk::Color> property_text_on_color;
	TransProperty<Gdk::Color> property_text_off_color;
	Glib::Property<Gtk::PositionType> property_text_position;

	Glib::Property<int> property_pic_width;
	Glib::Property<int> property_pic_height;
	Glib::Property<int> property_vcode_width;
	Glib::Property<int> property_vcode_height;

	Glib::Property<bool> property_state_;
	Glib::Property<bool> property_confirm_;
	Glib::Property<bool> property_blink_;

	void ctor();

	void on_state_changed();

	void blink(bool state, int time=DEFAULT_BLINK_TIME);
	void start_blink();
	void stop_blink();

	void process_sensor(UniWidgetsTypes::ObjectId,UniWidgetsTypes::ObjectId,long);
	void process_message(UMessages::MessageId id, Glib::ustring);
	void process_confirm(UMessages::MessageId id, time_t);

	void resize_composits();

	void on_vcode_position_changed();
	void on_text_position_changed();
	void on_disconnect_effect_changed();
};

#endif
