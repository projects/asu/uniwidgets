#include "USVGIndicatorPlus.h"
#include <iostream>
#include <gtkmm.h>
#include <ConfirmSignal.h>

using namespace std;
using namespace UniWidgetsTypes;

#define USVGINDICATORPLUS_PROPERTY_INIT() \
	confirmed_(true) \
	,blinking_(false) \
	,widget_is_realised(false) \
	,width(0) \
	,height(0) \
	,opacity_value(255) \
	,state(false) \
	,sensor_prop_di_(this,"di",get_connector()) \
	,property_sensor_value_(*this,"sensor-value",1) \
	,property_state_(*this,"state",false) \
	,property_fade_state_(*this,"fade-state",false) \
	,property_fade_state_time_(*this,"fade-step-time",50) \
	,property_blink_(*this, "blink" , false) \
	,property_blink_time_(*this, "blink-time" , DEFAULT_BLINK_TIME) \
	,property_confirm_(*this, "confirm", false) \
	,property_image0_path_(*this, "image0-path" ) \
	,property_image0_on_top_(*this, "image0-on-top-in-fade-mode" , false) \
	,property_image1_path_(*this, "image1-path" ) \
	,property_image1_on_top_(*this, "image1-on-top-in-fade-mode" , false)

void USVGIndicatorPlus::ctor()
{
	widget_is_realised = false;
	signal_size_allocate().connect(sigc::bind(sigc::mem_fun(*this, &USVGIndicatorPlus::image_pixbuf_check),false));

	set_visible_window(false);

	sigc::slot<void> image_path_changed =
			sigc::mem_fun(*this, &USVGIndicatorPlus::image_check);

	sigc::slot<void> change_state =
			sigc::mem_fun(*this, &USVGIndicatorPlus::state_changed);

	sigc::slot<void> redraw =
		sigc::mem_fun(*this, &USVGIndicatorPlus::queue_draw);

	connect_property_changed("image0-path", image_path_changed);
	connect_property_changed("image0-path", redraw);
	connect_property_changed("image1-path", image_path_changed);
	connect_property_changed("image1-path", redraw);
	connect_property_changed("state",change_state);
	connect_property_changed("sensor-value",redraw);

	missing_image_ref_ = render_icon( Gtk::Stock::MISSING_IMAGE, Gtk::ICON_SIZE_MENU);
}

USVGIndicatorPlus::USVGIndicatorPlus() :
	Glib::ObjectBase("usvgindicatorplus")
	,USVGINDICATORPLUS_PROPERTY_INIT()
{
	ctor();
}

USVGIndicatorPlus::USVGIndicatorPlus(const Glib::ustring& image0_path, const Glib::ustring& image1_path) :
	Glib::ObjectBase("usvgindicatorplus")
	,USVGINDICATORPLUS_PROPERTY_INIT()
{
	property_image0_path_ = image0_path;
	property_image1_path_ = image1_path;
	ctor();
}

USVGIndicatorPlus::USVGIndicatorPlus(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,USVGINDICATORPLUS_PROPERTY_INIT()
{
	ctor();
}

bool USVGIndicatorPlus::on_expose_event(GdkEventExpose* event)
{
	bool rv = UEventBox::on_expose_event(event);

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Glib::RefPtr<Gdk::Pixbuf> image_ref = property_fade_state_ ? fade_image_ref_ : (property_state_ ? image1_ref_ : image0_ref_);

	const Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	if (!image_ref) {
		cr->set_line_width(1);
		Gdk::Cairo::rectangle(cr, alloc);
		cr->stroke();
		image_ref = missing_image_ref_;
	}

//	const int image_width  = image_ref->get_width();
//	const int image_height = image_ref->get_height();
//	const int image_x = alloc.get_x() + (alloc.get_width()  - image_width )/2;
//	const int image_y = alloc.get_y() + (alloc.get_height() - image_height)/2;

	if (!connected_ && get_property_disconnect_effect() == 2) {
		Gtk::IconSource source;
		source.set_pixbuf(image_ref);
		image_ref  = get_style()->
			render_icon(source,
					Gtk::TEXT_DIR_NONE,
					Gtk::STATE_INSENSITIVE,
					Gtk::IconSize((GtkIconSize)-1),
					*this, "");
	}

	Gdk::Cairo::set_source_pixbuf(cr, image_ref, alloc.get_x(), alloc.get_y());

	cr->paint();

	if ( !connected_ && get_property_disconnect_effect() == 1)
		UVoid::draw_disconnect_effect_1(cr, alloc);

	return rv;
}
//---------------------------------------------------------------------------------------
void USVGIndicatorPlus::state_changed()
{
	if(state != property_state_)
		state = property_state_;
	else
		return;

	if(property_fade_state_.get_value())
	{
		if(property_blink_time_.get_value()<=0)
			opacity_step=255;
		else if(property_fade_state_time_.get_value()<=0)
			opacity_step=255;
		else
			opacity_step = 255/(property_blink_time_.get_value()/property_fade_state_time_.get_value());
		if(opacity_step>255 || opacity_step<=0)
			opacity_step=255;
		opacity_value = 0;
		opacity_step_connector.disconnect();

		int time = (property_fade_state_time_.get_value()>property_blink_time_.get_value() || property_fade_state_time_.get_value()<=0) ? property_blink_time_.get_value():property_fade_state_time_.get_value();
		opacity_step_connector = Glib::signal_timeout().connect(sigc::mem_fun(*this, &USVGIndicatorPlus::opacity_step_timer),time);
		queue_draw();
	}
	else
		queue_draw();
}
//---------------------------------------------------------------------------------------
bool USVGIndicatorPlus::opacity_step_timer()
{
	if(opacity_value<0 || opacity_value>255 || !property_fade_state_.get_value() || !widget_is_realised)
		return false;

//	cout<<get_name()<<"::opacity_step_timer fadeState="<<property_fade_state_.get_value()<<" opacity_value="<<opacity_value<<" step="<<opacity_step<<" property_state_="<<property_state_<<endl;

	if( (opacity_value + opacity_step)<=255 || opacity_value==255)
		opacity_value += opacity_step;
	else
		opacity_value = 255;

	Glib::RefPtr<Gdk::Pixbuf> pixbuf;// = mask_pixbuf->copy();
	string path;
	if(get_state())
	{
		if(is_image1_on_top())
		{
			pixbuf = image0_ref_->copy();
			path = property_image0_path().get_value();
			if(path != property_image1_path().get_value())
				image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
			fade_image_ref_ = pixbuf->copy();
		}
		else
		{
			pixbuf = image1_ref_->copy();
			path = property_image1_path().get_value();
			if(path != property_image0_path().get_value())
				image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
			fade_image_ref_ = pixbuf->copy();
		}
	}
	else
	{
		if(is_image0_on_top())
		{
			pixbuf = image1_ref_->copy();
			path = property_image1_path().get_value();
			if(path != property_image0_path().get_value())
				image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
			fade_image_ref_ = pixbuf->copy();
		}
		else
		{
			pixbuf = image0_ref_->copy();
			path = property_image0_path().get_value();
			if(path != property_image1_path().get_value())
				image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
			fade_image_ref_ = pixbuf->copy();
		}
	}
	queue_draw();

	if(opacity_value>=255)
		return false;

	return true;
}
//---------------------------------------------------------------------------------------
void USVGIndicatorPlus::image_check()
{
	Gtk::Allocation alloc = get_allocation();
	image_pixbuf_check(alloc,true);
}
//---------------------------------------------------------------------------------------
void USVGIndicatorPlus::image_pixbuf_check(Gtk::Allocation& alloc, bool force)
{
	if(height == alloc.get_height() && width == alloc.get_width() && widget_is_realised && !force)
		return;
//	if(!force)
//	{
		height = alloc.get_height();
		width = alloc.get_width();
//	}
	image0_ref_ = get_pixbuf_from_cache(property_image0_path().get_value(), width, height);

	string	image1_path = property_image1_path().get_value();
	bool image1_exist = !((!image1_path.empty() && !UniWidgetsTypes::file_exist(image1_path)) || image1_path.empty());
	image1_path = ((!image1_path.empty() && !UniWidgetsTypes::file_exist(image1_path)) || image1_path.empty()) ? (string)property_image0_path().get_value() : image1_path;

	if(image1_path == property_image0_path().get_value())
		image1_ref_ = image0_ref_->copy();
	else
		image1_ref_ = get_pixbuf_from_cache(image1_path, width, height);

	mask_pixbuf = get_pixbuf_from_cache("", width, height);
	fade_image_ref_ = (property_state_ ? image1_ref_ : image0_ref_)->copy();
	widget_is_realised = true;
}
//---------------------------------------------------------------------------------------
void USVGIndicatorPlus::get_size(int* width, int* height)
{
	const int width_true = image1_ref_ ? image1_ref_->get_width() : 0;
	const int width_false = image0_ref_ ? image0_ref_->get_width() : 0;
	*width = (width_true > width_false) ? width_true : width_false;

	const int height_true = image1_ref_ ? image1_ref_->get_height() : 0;
	const int height_false = image0_ref_ ? image0_ref_->get_height() : 0;
	*height = (height_true > height_false) ? height_true : height_false;
}

USVGIndicatorPlus::~USVGIndicatorPlus()
{
}

void USVGIndicatorPlus::init_widget()
{
}
void USVGIndicatorPlus::on_realize()
{
	UEventBox::on_realize();
	if(property_blink_time_.get_value()>0 && property_blink_time_.get_value()!=DEFAULT_BLINK_TIME)
		blinker.set_blink_time(property_blink_time_.get_value());

//	image_check();
	Gtk::Allocation alloc = get_allocation();
	image_pixbuf_check(alloc);
}

void USVGIndicatorPlus::blink( bool blink_state , int time)
{
	if(time == property_blink_time_.get_value())
		property_state_ = blink_state;
}

void
USVGIndicatorPlus::set_state(bool val)
{
	property_state_.set_value(val);
	state_changed();
}

void
USVGIndicatorPlus::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;


	sensor_connection_.disconnect();
	message_connection_.disconnect();
	confirm_connection_.disconnect();

	sensor_state_ = 0;
	stop_blink();
	property_state_ = false;
	
	sensor_prop_di_.set_connector(connector);
	UEventBox::set_connector(connector);

	if (get_connector() == true) {
		sigc::slot<void, ObjectId, ObjectId, long> process_sensor_slot =
				sigc::mem_fun(this, &USVGIndicatorPlus::process_sensor);
		sensor_connection_ = get_connector()->signals().connect_value_changed(
				process_sensor_slot,
				sensor_prop_di_.get_sens_id(),
				sensor_prop_di_.get_node_id());
		if (property_confirm_) {
			sigc::slot<void, UMessages::MessageId, Glib::ustring> process_message_slot =
				sigc::mem_fun(this, &USVGIndicatorPlus::process_message);
			UMessages::MessageId id(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id(), 1);
			message_connection_ = get_connector()->signals().connect_on_message(process_message_slot, id);
		}
	}
}

void
USVGIndicatorPlus::on_connect() throw()
{
	UEventBox::on_connect();

	long value = get_connector()->
		get_value(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id());

	sensor_state_ = value;

	if (property_confirm_) {
		UMessages::MessageId id(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id(), 1);
		MessageStatus status = get_connector()->signal_confirm().
			get_message_status(id);

		if (value == property_sensor_value_)
			property_state_ = true;
		else if (status == WAITS_CONFIRM)
			process_message(id, Glib::ustring());
	}
	else
		process_sensor(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id(), value);

	if(get_property_disconnect_effect() > 0)
		queue_draw();
}

void
USVGIndicatorPlus::on_disconnect() throw()
{
	UEventBox::on_disconnect();

	stop_blink();

	//TODO: reduce code duplication (see process_confirm())
	if (confirmed_ == false) {
		property_state_ = 1;
		confirmed_ = true;
		confirm_connection_.disconnect();
		if (get_property_lock_view())
			unlock_current();
	}

	queue_draw();
}

void
USVGIndicatorPlus::process_sensor(ObjectId id, ObjectId node, long value)
{
	if(id == sensor_prop_di_.get_sens_id() && node == sensor_prop_di_.get_node_id())
	{
		sensor_state_ = value;

		if (confirmed_ && (get_state() != (value == property_sensor_value_)))
			property_state_ = (value == property_sensor_value_);

		if (!property_confirm_ && property_blink_)
			if(value == property_sensor_value_)
				start_blink();
		else
			stop_blink();
	}
}

void
USVGIndicatorPlus::process_message(UMessages::MessageId id, Glib::ustring) {
	if (confirmed_) {
		confirmed_ = false;
		property_state_ = true;
		confirm_connection_ = get_connector()->signal_confirm().connect(
				sigc::mem_fun(this,&USVGIndicatorPlus::process_confirm), id);
		if ( property_blink_ )
			start_blink();
		if (get_property_lock_view())
			add_lock(*this);
	}
}

void USVGIndicatorPlus::process_confirm(UMessages::MessageId id, time_t)
{
	confirmed_ = true;
	if (blinking_)
		stop_blink();
	if (sensor_state_ != property_sensor_value_)
		property_state_ = false;
	if (get_property_lock_view())
		unlock_current();
}

void
USVGIndicatorPlus::start_blink()
{
	if (blinking_)
		return;
	blinking_ = true;
	blink_connection_.disconnect();
	blink_connection_ = blinker.signal_blink[property_blink_time_.get_value()].
		connect(sigc::mem_fun(this, &USVGIndicatorPlus::blink));
}

void
USVGIndicatorPlus::stop_blink()
{
	if (!blinking_)
		return;
	blinking_ = false;
	blink_connection_.disconnect();
	//TODO:use sensor_state_ from signals
	property_state_ = (sensor_state_ == property_sensor_value_);
}
