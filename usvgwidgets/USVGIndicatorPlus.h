#ifndef _USVGINDICATORPLUS_H
#define _USVGINDICATORPLUS_H

#include <sigc++/sigc++.h>
#include <ConfirmSignal.h>
#include <USignals.h>
#include <uwidgets/UEventBox.h>
#include <gdkmm.h>
#include <global_macros.h>

class USVGIndicatorPlus : public UEventBox{
public:
	USVGIndicatorPlus();
	USVGIndicatorPlus(const Glib::ustring& image0_path, const Glib::ustring& image1_path);
	explicit USVGIndicatorPlus(GtkmmBaseType::BaseObjectType* gobject);
	~USVGIndicatorPlus();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<long> property_sensor_value() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<long> property_sensor_value();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<bool> property_blink();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<int> property_blink_time() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<int> property_blink_time();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<bool> property_blink() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<bool> property_confirm();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<bool> property_confirm() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<Glib::ustring> property_image0_path();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_image0_path() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<Glib::ustring> property_image1_path();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_image1_path() const;


	/** TODO: write documentation
	 *
	 */
	bool get_state() const;

	/** TODO: write documentation
	 *
	 */
	bool get_fade_state() const;

	/** TODO: write documentation
	 *
	 */
	bool is_image0_on_top() const;

	/** TODO: write documentation
	 *
	 */
	bool is_image1_on_top() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<int> property_fade_state_time() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<int> property_fade_state_time();

	/** TODO: write documentation
	 *
	 */
	virtual void set_state(bool state);

	/** TODO: write documentation
	 *
	 */
	void get_size(int* width, int* height);

	SensorProp* get_di() { return &sensor_prop_di_; }

	virtual void init_widget();

	virtual void set_connector(const ConnectorRef& connector) throw();
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void on_realize();

protected:
	virtual bool on_expose_event(GdkEventExpose*);
	void process_sensor(UniWidgetsTypes::ObjectId,UniWidgetsTypes::ObjectId,long);
	virtual void image_pixbuf_check(Gtk::Allocation& alloc, bool force=false);
	virtual void state_changed();
	virtual bool opacity_step_timer();
	int width, height;
	int opacity_value,opacity_step;
	sigc::connection opacity_step_connector;
	bool widget_is_realised;
	bool state;
	
	SensorProp sensor_prop_di_;
	ADD_PROPERTY(property_sensor_value_, long)
	ADD_PROPERTY(property_state_, bool)
	ADD_PROPERTY(property_fade_state_, bool)
	ADD_PROPERTY(property_fade_state_time_, int)
	ADD_PROPERTY(property_blink_, bool)
	ADD_PROPERTY(property_blink_time_, int)
	ADD_PROPERTY(property_confirm_, bool)
	ADD_PROPERTY(property_image0_path_, Glib::ustring)
	ADD_PROPERTY(property_image0_on_top_, bool)
	ADD_PROPERTY(property_image1_path_, Glib::ustring)
	ADD_PROPERTY(property_image1_on_top_, bool)
	
private:
	void ctor();

	Glib::RefPtr<Gdk::Pixbuf> image0_ref_;
	Glib::RefPtr<Gdk::Pixbuf> image1_ref_;
	Glib::RefPtr<Gdk::Pixbuf> fade_image_ref_;
	Glib::RefPtr<Gdk::Pixbuf> mask_pixbuf;
	Glib::RefPtr<Gdk::Pixbuf> missing_image_ref_;

	bool confirmed_;
	bool blinking_;
	long sensor_state_;
	sigc::connection blink_connection_;
	USignals::Connection sensor_connection_;
	MsgConfirmConnection confirm_connection_;
	USignals::Connection message_connection_;

	void blink(bool state, int time=DEFAULT_BLINK_TIME);
	void start_blink();
	void stop_blink();

	void process_message(UMessages::MessageId id, Glib::ustring);
	void process_confirm(UMessages::MessageId id, time_t);

	void image_check();

	DISALLOW_COPY_AND_ASSIGN(USVGIndicatorPlus);
};

inline bool
		USVGIndicatorPlus::get_state() const
{
	return property_state_.get_value();
}

inline bool
		USVGIndicatorPlus::is_image0_on_top() const
{
	return property_image0_on_top_.get_value();
}

inline bool
		USVGIndicatorPlus::is_image1_on_top() const
{
	return property_image1_on_top_.get_value();
}

inline Glib::PropertyProxy<bool>
USVGIndicatorPlus::property_blink()
{
	return property_blink_.get_proxy();
}

inline bool
USVGIndicatorPlus::get_fade_state() const
{
	return property_fade_state_.get_value();
}

inline Glib::PropertyProxy<int>
		USVGIndicatorPlus::property_fade_state_time()
{
	return property_fade_state_time_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<int>
		USVGIndicatorPlus::property_fade_state_time() const
{
	return Glib::PropertyProxy_ReadOnly<int>(this,"fade-step-time");
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGIndicatorPlus::property_blink() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"blink");
}

inline Glib::PropertyProxy<long>
		USVGIndicatorPlus::property_sensor_value()
{
	return property_sensor_value_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<long>
		USVGIndicatorPlus::property_sensor_value() const
{
	return Glib::PropertyProxy_ReadOnly<long>(this,"sensor-value");
}

inline Glib::PropertyProxy<int>
		USVGIndicatorPlus::property_blink_time()
{
	return property_blink_time_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<int>
		USVGIndicatorPlus::property_blink_time() const
{
	return Glib::PropertyProxy_ReadOnly<int>(this,"blink-time");
}

inline Glib::PropertyProxy<bool>
USVGIndicatorPlus::property_confirm()
{
	return property_confirm_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGIndicatorPlus::property_confirm() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"confirm");
}

inline Glib::PropertyProxy<Glib::ustring>
USVGIndicatorPlus::property_image0_path()
{
	return property_image0_path_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGIndicatorPlus::property_image0_path() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"image0-path");
}

inline Glib::PropertyProxy<Glib::ustring>
USVGIndicatorPlus::property_image1_path()
{
	return property_image1_path_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGIndicatorPlus::property_image1_path() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"image1-path");
}
#endif
