#include "USVGText.h"
#include <ThemeLoader.h>

#define COLOR_ENTRY "color"
#define FONT_ENTRY "font"

using namespace UniWidgetsTypes;

#define USVGTEXT_PROPERTIES_INIT \
	property_theme_(*this, "theme", "text_normal")\
	,property_use_theme_(*this, "use-theme", true)\
	,property_text_(*this,"text","Default Text")\
	,property_font_name_(*this, "font-name", "Liberation Sans 16")\
	,property_abs_font_size_(*this, "abs-font-size", -1)\
	,property_font_color_(*this, "font-color", Gdk::Color("black"))\
	,property_alignment_(*this, "alignment" , Pango::ALIGN_LEFT )\
	,property_wrapmode_(*this, "wrapmode", Pango::WRAP_WORD)\
	,property_drop_shadow_(*this, "drop-shadow", false)\
	,property_use_wrapmode_(*this, "use-wrapmode", false)\
	,property_transparency_(*this, "tranparency", 1)

#define USVGTEXT_PROPERTIES_COPY(obj) \
	property_theme_(*this, "theme", obj.property_theme_)\
	,property_use_theme_(*this, "use-theme", obj.property_use_theme_)\
	,property_text_(*this,"text",obj.property_text_)\
	,property_font_name_(*this, "font-name", obj.property_font_name_)\
	,property_abs_font_size_(*this, "abs-font-size", obj.property_abs_font_size_)\
	,property_font_color_(*this, "font-color", obj.property_font_color_)\
	,property_alignment_(*this, "alignment" , obj.property_alignment_ )\
	,property_wrapmode_(*this, "wrapmode", obj.property_wrapmode_)\
	,property_drop_shadow_(*this, "drop-shadow", obj.property_drop_shadow_)\
	,property_use_wrapmode_(*this, "use-wrapmode", obj.property_use_wrapmode_)\
	,property_transparency_(*this, "tranparency", obj.property_transparency_)

//----------------------------------------------------------
void USVGText::ctor()
{
	set_flags(Gtk::NO_WINDOW);
	layout = create_pango_layout("");
}
//----------------------------------------------------------

//----------------------------------------------------------
USVGText::USVGText() :
	Glib::ObjectBase("usvgtext")
	,USVGTEXT_PROPERTIES_INIT
{
	ctor();
}
//----------------------------------------------------------

//----------------------------------------------------------
USVGText::USVGText(GtkmmBaseType::BaseObjectType* gobject) :
	BaseType(gobject)
	,USVGTEXT_PROPERTIES_INIT
{
	ctor();
}
//----------------------------------------------------------
USVGText::USVGText(const USVGText& obj) :
	Glib::ObjectBase("usvgtext")
	,BaseType(obj)
	,USVGTEXT_PROPERTIES_COPY(obj)
{
	ctor();
}
//----------------------------------------------------------

//----------------------------------------------------------

//----------------------------------------------------------
void USVGText::on_realize()
{
	Gtk::Widget::on_realize();
	on_text_changed(); 
	connect_property_changed("theme", sigc::mem_fun(*this, &USVGText::on_theme_changed));
	connect_property_changed("text", sigc::mem_fun(*this, &USVGText::on_text_changed));
	connect_property_changed("font-name", sigc::mem_fun(*this, &USVGText::on_font_name_changed));
	connect_property_changed("font-color", sigc::mem_fun(*this, &USVGText::on_use_theme_changed));
	connect_property_changed("use-theme", sigc::mem_fun(*this, &USVGText::on_use_theme_changed));
	connect_property_changed("drop-shadow", sigc::mem_fun(*this, &USVGText::on_use_theme_changed));
	connect_property_changed("transparancy", sigc::mem_fun(*this, &USVGText::on_transparency_changed));
//	connect_property_changed("use-wrap", sigc::mem_fun(*this, &USVGText::on_use_wrap_)
	connect_property_changed("abs-font-size", sigc::mem_fun(*this, &USVGText::on_font_name_changed));

	layout->set_text(property_text_);
	transparency = property_transparency_;
	on_use_theme_changed();
}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGText::on_size_request(Gtk::Requisition* req)
{
	Gtk::Widget::on_size_request(req);
	*req = Gtk::Requisition();
	layout->get_pixel_size(req->width, req->height);
	if ( property_drop_shadow_ )
	{
		req->width += 1;
		req->height += 1;
	}
}
//----------------------------------------------------------

//USVGText::~USVGText()
//{
//}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGText::init_widget()
{
}
//----------------------------------------------------------
void USVGText::set_state(bool state)
{
	if(state)
		transparency = property_transparency_;
	else
		transparency = 0;
	queue_draw();
}
//----------------------------------------------------------
void USVGText::setText(Glib::ustring text)
{
	property_text() = text;
	queue_draw();
}
//----------------------------------------------------------

//----------------------------------------------------------
bool USVGText::on_expose_event(GdkEventExpose *event)
{
	widget_draw(layout, get_allocation());
	return true;
}
//----------------------------------------------------------

void USVGText::on_size_allocate(Gtk::Allocation& alloc)
{
	Gtk::Widget::on_size_allocate(alloc);
}

// ---------------------------------------------------------
void USVGText::widget_draw(const Glib::RefPtr<Pango::Layout>& layout, const Gtk::Allocation& alloc)
{
//	layout->set_wrap( property_wrapmode );
	cr = get_window()->create_cairo_context();

	int w,h;
	layout->get_pixel_size(w,h);


	layout->set_alignment( property_alignment_ );
		
	if (property_drop_shadow_)
	{
//		layout->set_width( (alloc.get_width()-1)*Pango::SCALE);

		cr->set_source_rgb(0,0,0);
		switch(property_alignment_)
		{
		  case Pango::ALIGN_LEFT:
			cr->move_to( alloc.get_x() + 1, alloc.get_y()+(alloc.get_height()-h)/2+1 );
			break;
		  case Pango::ALIGN_RIGHT:
			cr->move_to( alloc.get_x()+alloc.get_width()-w + 1, alloc.get_y()+(alloc.get_height()-h)/2+1 );
			break;
		  default:
			cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2+1, alloc.get_y()+(alloc.get_height()-h)/2+1 );
			break;
		}
		layout->add_to_cairo_context(cr);
		cr->fill();
	}
//	else
//		layout->set_width( alloc.get_width()*Pango::SCALE );
	

	/* Glade can set color to NULL so we need to check it.
	   May be later we will find another solution
	*/
	if (current_color.gobj() == NULL) current_color = Gdk::Color("black");

	cr->set_source_rgba( current_color.get_red_p(),
						current_color.get_green_p(),
						current_color.get_blue_p(),
						transparency );
	switch(property_alignment_)
	{
	  case Pango::ALIGN_LEFT:
		cr->move_to( alloc.get_x(), alloc.get_y()+(alloc.get_height()-h)/2 );
		break;
	  case Pango::ALIGN_RIGHT:
		cr->move_to( alloc.get_x()+alloc.get_width()-w, alloc.get_y()+(alloc.get_height()-h)/2 );
		break;
	  default:
		cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2, alloc.get_y()+(alloc.get_height()-h)/2 );
		break;
	}
	layout->add_to_cairo_context(cr);
	cr->fill();

}
// ---------------------------------------------------------

// ---------------------------------------------------------
void USVGText::load_theme_settings()
{
	ThemeLoader tl( property_theme_ );

	current_color = tl.get_color(COLOR_ENTRY);
	layout->set_font_description( Pango::FontDescription( tl.get_string(FONT_ENTRY) ) );

}
// ---------------------------------------------------------

// ---------------------------------------------------------
void USVGText::load_property_settings()
{
	current_color = property_font_color_;
	Pango::FontDescription fd(property_font_name_);
	if ( property_abs_font_size_ > 0)
		fd.set_absolute_size( property_abs_font_size_ * Pango::SCALE );
	layout->set_font_description(fd);
}
// ---------------------------------------------------------

// ---------------------------------------------------------
void USVGText::on_text_changed()
{
	layout->set_text(property_text_);
	int w,h;
	layout->get_pixel_size(w,h);
	//set_size_request(w,h);
}
// ---------------------------------------------------------

//----------------------------------------------------------
void USVGText::on_theme_changed()
{
	if ( property_use_theme_ )
	{
		load_theme_settings();
		queue_resize();
	}
}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGText::on_use_theme_changed()
{
	if ( property_use_theme_ )
		load_theme_settings();
	else
		load_property_settings();

	queue_resize();
}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGText::on_font_name_changed()
{
	if ( property_use_theme_ ) return;
	
	Pango::FontDescription fd(property_font_name_);
	if ( property_abs_font_size_ > 0)
		fd.set_absolute_size( property_abs_font_size_ * Pango::SCALE );
	layout->set_font_description(fd);
	queue_resize();
}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGText::on_font_color_changed()
{
	if ( property_use_theme_ ) return;
	queue_draw();
}
//----------------------------------------------------------

//----------------------------------------------------------
void USVGText::on_transparency_changed()
{
	transparency = property_transparency_;
	queue_draw();
}
//----------------------------------------------------------




//----------------------------------------------------------
//----------------------------------------------------------
//----------------------------------------------------------
USVGTextBase::USVGTextBase() :
	layout(NULL),
	cp(NULL),
	desc_set(false),
	text(""),
	jtype(0)
{
}

USVGTextBase::USVGTextBase(Glib::ustring newfont) :
	layout(NULL),
	cp(NULL),
	desc_set(true),
	text(""),
	font(newfont),
	jtype(0)
{
	desc = Pango::FontDescription(newfont);
}

void USVGTextBase::set_context(Cairo::RefPtr<Cairo::Context> cptr)
{
	cp = cptr;

	layout = Pango::Layout::create(cp);
	if(desc_set)
		layout->set_font_description(desc);
	if(!text.empty())
		layout->set_text(text);
}

// ---------------------------------------------------------
void USVGTextBase::set_font(Glib::ustring newfont)
{
	if (font != newfont) {
		font = newfont;
		desc = Pango::FontDescription(newfont);
		desc_set = true;
		if(layout)
			layout->set_font_description(desc);
	}
}
// ---------------------------------------------------------

// ---------------------------------------------------------
void USVGTextBase::set_justify(guint type)
{
	jtype = type;
}
// ---------------------------------------------------------

// ---------------------------------------------------------
void USVGTextBase::set_text(Glib::ustring newtext)
{
	text = newtext;
	if(layout)
		layout->set_text(newtext);
}
// ---------------------------------------------------------

// ---------------------------------------------------------
void USVGTextBase::set_color(Gdk::Color newcolor)
{
	color = newcolor;
}
// ---------------------------------------------------------

// ---------------------------------------------------------
void USVGTextBase::get_pixel_size(int &width, int&height)
{
	if(layout)
		layout->get_pixel_size(width, height);
	else
		g_critical("Cairo surface must be assigned before this operation!");
}
// ---------------------------------------------------------

// ---------------------------------------------------------
void USVGTextBase::render()
{
	int width, height;
	
	if(!cp) {
		g_critical("Cairo surface must be assigned before this operation!");
		return;
	}
	
	if(!layout) {
		g_critical("Pango layout must be assigned before this operation!");
		return;
	}
	
	cp->set_source_rgb(color.get_red_p(), color.get_green_p(), color.get_blue_p());
	switch(jtype) {
		case 1:
			get_pixel_size(width, height);
			cp->translate(-width, 0.0);
			break;
		case 2:
			get_pixel_size(width, height);
			cp->translate(-(float)width * 0.5, 0.0);
			break;
		default:
			break;
	}
	layout->add_to_cairo_context(cp);
	cp->fill();
}
// ---------------------------------------------------------

