#ifndef _USVGTEXT_H
#define _USVGTEXT_H

#include <gtkmm.h>
#include <UDefaultFunctions.h>
#include <SensorProp.h>

class USVGText : public UDefaultFunctions<Gtk::Widget> {
public:
	USVGText();
	explicit USVGText(GtkmmBaseType::BaseObjectType* gobject);
	USVGText(const USVGText& copy);
	virtual ~USVGText() {}
	
	void get_pixel_size(int& width, int& height) { layout->get_pixel_size(width, height); }
	
	void on_text_changed();
	void on_justify_changed();
	void widget_draw(const Glib::RefPtr<Pango::Layout>& layout, const Gtk::Allocation& alloc);
	void on_font_name_changed();
	void on_font_color_changed();
	void on_use_theme_changed();
	void on_transparency_changed();
	
	virtual void on_theme_changed();
	virtual void load_theme_settings();
	virtual void load_property_settings();
	
	virtual void init_widget();
	
	virtual bool on_expose_event(GdkEventExpose*);
	virtual void on_size_request(Gtk::Requisition* requisition);
	virtual void on_realize();
	virtual void on_size_allocate(Gtk::Allocation& alloc);
	virtual void set_state(bool state);
	
	void setText(Glib::ustring text);

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<Glib::ustring> property_theme();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_theme() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<bool> property_use_theme();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<bool> property_use_theme() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<Glib::ustring> property_text();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_text() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<Glib::ustring> property_font_name();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_font_name() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<gint> property_abs_font_size();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<gint> property_abs_font_size() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<Gdk::Color> property_font_color();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<Gdk::Color> property_font_color() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<Pango::Alignment> property_alignment();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<Pango::Alignment> property_alignment() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<Pango::WrapMode> property_wrapmode();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<Pango::WrapMode> property_wrapmode() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<bool> property_drop_shadow();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<bool> property_drop_shadow() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<bool> property_use_wrapmode();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<bool> property_use_wrapmode() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<double> property_transparency();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<double> property_transparency() const;

protected:
	void ctor();
	
	Glib::RefPtr<Pango::Layout> layout;
	Cairo::RefPtr<Cairo::Context> cr;
	
	Gdk::Color current_color;
	double transparency;

private:
	Glib::Property<Glib::ustring> property_theme_;
	Glib::Property<bool> property_use_theme_;
	Glib::Property<Glib::ustring> property_text_;
	Glib::Property<Glib::ustring> property_font_name_;
	Glib::Property<gint> property_abs_font_size_;
	Glib::Property<Gdk::Color> property_font_color_;
	Glib::Property<Pango::Alignment> property_alignment_;
	Glib::Property<Pango::WrapMode> property_wrapmode_;
	Glib::Property<bool> property_drop_shadow_;
	Glib::Property<bool> property_use_wrapmode_;
	Glib::Property<double> property_transparency_;
};


inline Glib::PropertyProxy<Glib::ustring>
USVGText::property_theme()
{
	return property_theme_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGText::property_theme() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"theme");
}

inline Glib::PropertyProxy<bool>
USVGText::property_use_theme()
{
	return property_use_theme_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGText::property_use_theme() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"use-theme");
}

inline Glib::PropertyProxy<Glib::ustring>
USVGText::property_text()
{
	return property_text_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGText::property_text() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"text");
}

inline Glib::PropertyProxy<Glib::ustring>
USVGText::property_font_name()
{
	return property_font_name_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGText::property_font_name() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"font-name");
}

inline Glib::PropertyProxy<gint>
USVGText::property_abs_font_size()
{
	return property_abs_font_size_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<gint>
USVGText::property_abs_font_size() const
{
	return Glib::PropertyProxy_ReadOnly<gint>(this,"abs-font-size");
}

inline Glib::PropertyProxy<Gdk::Color>
USVGText::property_font_color()
{
	return property_font_color_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Gdk::Color>
USVGText::property_font_color() const
{
	return Glib::PropertyProxy_ReadOnly<Gdk::Color>(this,"font-color");
}

inline Glib::PropertyProxy<Pango::Alignment>
USVGText::property_alignment()
{
	return property_alignment_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Pango::Alignment>
USVGText::property_alignment() const
{
	return Glib::PropertyProxy_ReadOnly<Pango::Alignment>(this,"alignment");
}

inline Glib::PropertyProxy<Pango::WrapMode>
USVGText::property_wrapmode()
{
	return property_wrapmode_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Pango::WrapMode>
USVGText::property_wrapmode() const
{
	return Glib::PropertyProxy_ReadOnly<Pango::WrapMode>(this,"wrapmode");
}

inline Glib::PropertyProxy<bool>
USVGText::property_drop_shadow()
{
	return property_drop_shadow_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGText::property_drop_shadow() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"drop-shadow");
}

inline Glib::PropertyProxy<bool>
USVGText::property_use_wrapmode()
{
	return property_use_wrapmode_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGText::property_use_wrapmode() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"use-wrapmode");
}

inline Glib::PropertyProxy<double>
USVGText::property_transparency()
{
	return property_transparency_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<double>
USVGText::property_transparency() const
{
	return Glib::PropertyProxy_ReadOnly<double>(this,"transparency");
}



/* I think we don't realy need it. We can use Pango::Layout instead of USVGTextBase*/

//#include <librsvg/rsvg.h>

class USVGTextBase {
public:
	USVGTextBase();
	USVGTextBase(Glib::ustring font);
	~USVGTextBase() {};
	void set_context(Cairo::RefPtr<Cairo::Context> cptr);
	void set_font(Glib::ustring newfont);
	void set_color(Gdk::Color color);
	void set_text(Glib::ustring text);
	void set_justify(guint jtype);
	void render();
	void get_pixel_size(int&, int&);
protected:
	Glib::RefPtr<Pango::Layout> layout;
	Cairo::RefPtr<Cairo::Context> cp;
	Pango::FontDescription desc;
	bool desc_set;
	Gdk::Color color;
	Glib::ustring font, text;
	guint jtype; /* 0 = Left, 1 = Right, 2 = Center... C++ suxxx! */
	/* use Pango::Alignmen & Pango::Justification (L)*/
};

#endif
