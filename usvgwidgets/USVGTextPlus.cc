#include "USVGTextPlus.h"

#include <Connector.h>
#include <ThemeLoader.h>
#include <ConfirmSignal.h>
#include <USignals.h>

#define ON_COLOR_ENTRY "on_color"
#define OFF_COLOR_ENTRY "off_color"
#define FONT_ENTRY "font"

using namespace UniWidgetsTypes;
using namespace std;

#define USVGTEXTPLUS_PROPERTIES_INIT \
	sensor_prop_di_(this, "di", get_connector()) \
	,property_on_font_name_(*this, "on-font-name", "Libertation Sans 16") \
	,property_on_font_color_(*this, "on-font-color", Gdk::Color("red")) \
	,property_on_abs_font_size_(*this, "on-abs-font-size", -1) \
	,property_on_transparency_(*this, "on-transparency", 1.0) \
	,property_state_(*this,"state",false) \
	,property_blink_(*this,"blink", false) \
	,property_confirm_(*this,"confirm", false)


void
USVGTextPlus::ctor()
{
	connect_property_changed("state", sigc::mem_fun(*this, &USVGTextPlus::on_state_changed));
	property_theme() = "textplus_sdt";
}

USVGTextPlus::USVGTextPlus() :
	Glib::ObjectBase("usvgtextplus")
	,USVGTEXTPLUS_PROPERTIES_INIT
	,confirmed_(true)
	,blinking_(false)
{
	ctor();
}

USVGTextPlus::USVGTextPlus(GtkmmBaseType::BaseObjectType* gobject) :
	USVGText(gobject)
	,USVGTEXTPLUS_PROPERTIES_INIT
	,confirmed_(true)
	,blinking_(false)
{
	ctor();
}

void
USVGTextPlus::on_realize()
{
	Gtk::Widget::on_realize();
	connect_property_changed("theme", sigc::mem_fun(*this, &USVGText::on_theme_changed));
	connect_property_changed("text", sigc::mem_fun(*this, &USVGText::on_text_changed));
	connect_property_changed("font-name", sigc::mem_fun(*this, &USVGText::on_font_name_changed));
	connect_property_changed("abs-font-size", sigc::mem_fun(*this, &USVGText::on_font_name_changed));
	connect_property_changed("font-color", sigc::mem_fun(*this, &USVGText::on_use_theme_changed));
	connect_property_changed("use-theme", sigc::mem_fun(*this, &USVGText::on_use_theme_changed));
	connect_property_changed("state", sigc::mem_fun(*this, &USVGTextPlus::on_state_changed));
	connect_property_changed("on-font-color", sigc::mem_fun(*this, &USVGTextPlus::on_use_theme_changed));

	layout->set_text(property_text());
	on_use_theme_changed();
	on_state_changed();
}

void
USVGTextPlus::init_widget()
{

}

bool
USVGTextPlus::on_expose_event(GdkEventExpose* event)
{
	USVGText::on_expose_event(event);

	if ( get_property_disconnect_effect() != 1 )
		return true;

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	const Gtk::Allocation alloc = get_allocation();

	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	if ( !connected_  && get_property_disconnect_effect() == 1)
		UVoid::draw_disconnect_effect_1(cr, alloc);
}

void USVGTextPlus::blink( bool blink_state, int time)
{
	property_state_ = blink_state;
}

void
USVGTextPlus::load_theme_settings()
{
	ThemeLoader tl( property_theme() );

	on_color = tl.get_color(ON_COLOR_ENTRY);
	off_color = tl.get_color(OFF_COLOR_ENTRY);
	Pango::FontDescription fd( tl.get_string(FONT_ENTRY) );
	if (property_abs_font_size() > 0)
		fd.set_absolute_size( property_abs_font_size() * Pango::SCALE );
	layout->set_font_description( fd );
	current_color = property_state_ ? on_color : off_color;
}

void
USVGTextPlus::load_property_settings()
{
	on_color = property_on_font_color_;
	off_color = property_font_color();
	Pango::FontDescription fd( property_font_name() );
	if (property_abs_font_size() > 0)
		fd.set_absolute_size( property_abs_font_size() * Pango::SCALE);
	layout->set_font_description( fd );
	current_color = property_state_.get_value() ? on_color : off_color;
}

void
USVGTextPlus::on_state_changed()
{
	current_color = property_state_.get_value() ? on_color : off_color;
	transparency = property_state_.get_value() ? property_on_transparency() : property_transparency();
	property_drop_shadow() = property_state_.get_value();
	queue_resize();
}

void
USVGTextPlus::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UVoid::set_connector(connector);

	sensor_connection_.disconnect();
	message_connection_.disconnect();
	confirm_connection_.disconnect();

	sensor_state_ = 0;
	stop_blink();
	property_state_ = false;

	if (get_connector() == true) {
		sigc::slot<void, ObjectId, ObjectId, long> process_sensor_slot =
				sigc::mem_fun(this, &USVGTextPlus::process_sensor);
		sensor_connection_ = get_connector()->signals().connect_value_changed(
				process_sensor_slot,
				sensor_prop_di_.get_sens_id(),
				sensor_prop_di_.get_node_id());
		if (property_confirm_) {
			sigc::slot<void, UMessages::MessageId, Glib::ustring> process_message_slot =
				sigc::mem_fun(this, &USVGTextPlus::process_message);
			UMessages::MessageId id(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id(), 1);
			message_connection_ = get_connector()->signals().connect_on_message(process_message_slot, id);
		}
		on_connect();
	}
}

void
USVGTextPlus::on_connect() throw()
{
	UVoid::on_connect();
	long value = get_connector()->
		get_value(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id());

	if (property_confirm_) {
		UMessages::MessageId id(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id(), 1);
		MessageStatus status = get_connector()->signal_confirm().
			get_message_status(id);

		if (value == 1)
			property_state_ = true;
		else if (status == WAITS_CONFIRM)
			process_message(id, Glib::ustring());
	}
	else
		process_sensor(sensor_prop_di_.get_sens_id(), sensor_prop_di_.get_node_id(), value);

	if (get_property_disconnect_effect() > 1)
		queue_draw();
}

void
USVGTextPlus::on_disconnect() throw()
{
	UVoid::on_disconnect();

	stop_blink();

	if (confirmed_ == false) {
		property_state_ = 1;
		confirmed_ = true;
		confirm_connection_.disconnect();
		if (get_property_lock_view())
			unlock_current();
	}
}

void
USVGTextPlus::process_sensor(ObjectId id, ObjectId node, long value)
{
	//TODO:use sensor_state_ from signals
	sensor_state_ = value;

	if (confirmed_)
		property_state_ = (value == 1);

	if (!property_confirm_ && property_blink_)
		if(value == 1)
			start_blink();
		else
			stop_blink();
}

void
USVGTextPlus::process_message(UMessages::MessageId id, Glib::ustring) {
	if (confirmed_) {
		confirmed_ = false;
		property_state_ = true;
		confirm_connection_ = get_connector()->signal_confirm().connect(
				sigc::mem_fun(this,&USVGTextPlus::process_confirm), id);
		if ( property_blink_ )
			start_blink();
		if (get_property_lock_view())
			add_lock(*this);
	}
}

void
USVGTextPlus::process_confirm(UMessages::MessageId id, time_t)
{
	confirmed_ = true;
	if (blinking_)
		stop_blink();
	if (sensor_state_ != 1)
		property_state_ = false;
	if (get_property_lock_view())
		unlock_current();
}

void
USVGTextPlus::start_blink()
{
	if (blinking_)
		return;
	blinking_ = true;
	blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].
		connect(sigc::mem_fun(this, &USVGTextPlus::blink));
}

void
USVGTextPlus::stop_blink()
{
	if (!blinking_)
		return;
	blinking_ = false;
	blink_connection_.disconnect();
	//TODO:use sensor_state_ from signals
	property_state_ = (sensor_state_ == 1);
}
