#ifndef _USVGTEXTPLUS_H
#define _USVGTEXTPLUS_H

#include <CheckedSignal.h>
#include <usvgwidgets/USVGText.h>
#include <ConfirmSignal.h>


class USVGTextPlus : public USVGText {

public:
	USVGTextPlus();
	explicit USVGTextPlus(GtkmmBaseType::BaseObjectType* gobject);
	~USVGTextPlus() {}

	void on_state_changed();
	void on_use_theme_changed_plus();
	virtual void load_theme_settings();
	virtual void load_property_settings();
	virtual void set_state( bool val) {property_state_.set_value(val);}

	virtual void on_realize();
	virtual void init_widget();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<Glib::ustring> property_on_font_name();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_on_font_name() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<Gdk::Color> property_on_font_color();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<Gdk::Color> property_on_font_color() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<gint> property_on_abs_font_size();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<gint> property_on_abs_font_size() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<double> property_on_transparency();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<double> property_on_transparency() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<bool> property_state();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<bool> property_state() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<bool> property_blink();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<bool> property_blink() const;

	/** TODO:write documentation
	*/
	Glib::PropertyProxy<bool> property_confirm();

	/** TODO:write documentation
	*/
	Glib::PropertyProxy_ReadOnly<bool> property_confirm() const;

	virtual void set_connector(const ConnectorRef& connector) throw();
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();

protected:
	Gdk::Color on_color;
	Gdk::Color off_color;

	virtual bool on_expose_event(GdkEventExpose *event);

private:
	bool confirmed_;
	bool blinking_;
	bool sensor_state_;
	sigc::connection blink_connection_;
	USignals::Connection sensor_connection_;
	MsgConfirmConnection confirm_connection_;
	USignals::Connection message_connection_;

	SensorProp sensor_prop_di_;
	Glib::Property<Glib::ustring> property_on_font_name_;
	Glib::Property<Gdk::Color> property_on_font_color_;
	Glib::Property<gint> property_on_abs_font_size_;
	Glib::Property<double> property_on_transparency_;
	Glib::Property<bool> property_state_;
	Glib::Property<bool> property_blink_;
	Glib::Property<bool> property_confirm_;

	void blink(bool state, int time=DEFAULT_BLINK_TIME);
	void start_blink();
	void stop_blink();

	void process_sensor(UniWidgetsTypes::ObjectId,UniWidgetsTypes::ObjectId,long);
	void process_message(UMessages::MessageId id, Glib::ustring);
	void process_confirm(UMessages::MessageId id, time_t);

	void ctor();
};

inline Glib::PropertyProxy<Glib::ustring>
USVGTextPlus::property_on_font_name()
{
	return property_on_font_name_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
USVGTextPlus::property_on_font_name() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"on-font-name");
}

inline Glib::PropertyProxy<Gdk::Color>
USVGTextPlus::property_on_font_color()
{
	return property_on_font_color_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<Gdk::Color>
USVGTextPlus::property_on_font_color() const
{
	return Glib::PropertyProxy_ReadOnly<Gdk::Color>(this,"on-font-color");
}

inline Glib::PropertyProxy<gint>
USVGTextPlus::property_on_abs_font_size()
{
	return property_on_abs_font_size_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<gint>
USVGTextPlus::property_on_abs_font_size() const
{
	return Glib::PropertyProxy_ReadOnly<gint>(this,"on-abs-font-size");
}

inline Glib::PropertyProxy<double>
USVGTextPlus::property_on_transparency()
{
	return property_on_transparency_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<double>
USVGTextPlus::property_on_transparency() const
{
	return Glib::PropertyProxy_ReadOnly<double>(this,"on-transparency");
}

inline Glib::PropertyProxy<bool>
USVGTextPlus::property_state()
{
	return property_state_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGTextPlus::property_state() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"state");
}

inline Glib::PropertyProxy<bool>
USVGTextPlus::property_blink()
{
	return property_blink_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGTextPlus::property_blink() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"blink");
}

inline Glib::PropertyProxy<bool>
USVGTextPlus::property_confirm()
{
	return property_confirm_.get_proxy();
}

inline Glib::PropertyProxy_ReadOnly<bool>
USVGTextPlus::property_confirm() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"confirm");
}

#endif
