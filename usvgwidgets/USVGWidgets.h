#ifndef _USVGWIDGETS_H
#define _USVGWIDGETS_H

#include <usvgwidgets/USVGBar2.h>
#include <usvgwidgets/USVGButton.h>
#include <usvgwidgets/USVGImage.h>
#include <usvgwidgets/USVGIndicator.h>
#include <usvgwidgets/USVGIndicatorBig.h>
#include <usvgwidgets/USVGIndicatorPlus.h>
#include <usvgwidgets/USVGButtonIndicator.h>
#include <usvgwidgets/USVGText.h>
#include <usvgwidgets/USVGTextPlus.h>
#include <usvgwidgets/UniSVGButton.h>
#include <usvgwidgets/UniSVGDialogButton.h>
#include <usvgwidgets/UniSVGScriptButton.h>
#include <usvgwidgets/UniSVGTabButton.h>
#include <usvgwidgets/UValueIndicatormb745.h>

#endif
