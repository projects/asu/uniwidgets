#include "UValueIndicatormb745.h"

#include <iomanip>

using namespace std;
using namespace UniWidgetsTypes;
using Glib::ustring;

const int BOX_HEIGHT = 24;
const int CIRCLE_D = BOX_HEIGHT;
const int LINE_WIDTH = 1;
const float sqr3 = 1.73205081;
const int TRI_HEIGHT = (BOX_HEIGHT - 5*LINE_WIDTH) / 2;
const int TRI_WIDTH = 2*TRI_HEIGHT / sqr3;

#define use_tri use_hi_warning_||use_lo_warning_||use_hi_alarm_||use_lo_alarm_

#define UVALUEINDICATORMB745_INIT_PROPERTIES() \
	ai_(this,"ai", get_connector()) \
	,hi_warn_sensor_(this, "hi_warn", get_connector()) \
	,hi_alarm_sensor_(this, "hi_alarm", get_connector()) \
	,lo_warn_sensor_(this, "lo_warn", get_connector()) \
	,lo_alarm_sensor_(this, "lo_alarm", get_connector()) \
	,property_precision_(*this, "precision", 2) \
	,property_digits_(*this, "digits", 2) \
	,property_fill_digits_(*this,"fill-digits",0) \
	,property_value_(*this, "value", 0.0) \
	,property_font_value_(*this,"font-value","Liberation Sans Bold") \
	,property_font_value_size_(*this, "font-value-size", 13) \
	,property_font_value_color_(*this,"font-value-color",Gdk::Color("#2c954d")) \
	,property_font_headname_(*this,"font-headname","Liberation Sans") \
	,property_font_headname_size_(*this, "font-headname-size", 13) \
	,property_font_headname_color_(*this,"font-headname-color",Gdk::Color("white")) \
	,property_font_units_(*this,"font-units","Liberation Sans") \
	,property_font_units_size_(*this, "font-units-size", 13) \
	,property_font_units_color_(*this,"font-units-color",Gdk::Color("white")) \
	,value_color_("#2c954d") \
	,lab_color_("white") \
	,property_state_(*this, "state", STATE_NORMAL) \
	,property_headname_(*this, "headname", "-") \
	,property_units_(*this, "units", "-") \
	,use_hi_warning_(*this, "use-hi-warning", false) \
	,use_lo_warning_(*this, "use-lo-warning", false) \
	,use_hi_alarm_(*this, "use-hi-alarm", false) \
	,use_lo_alarm_(*this, "use-lo-alarm", false) \
	\
	,hi_warning_enabled_(*this, "hi-warning-enabled", false) \
	,hi_warning_value_  (*this, "hi-warning-value",   80) \
	,lo_warning_enabled_(*this, "lo-warning-enabled", false) \
	,lo_warning_value_  (*this, "lo-warning-value",   20) \
	,hi_alarm_enabled_  (*this, "hi-alarm-enabled",   false) \
	,hi_alarm_value_    (*this, "hi-alarm-value",     95) \
	,lo_alarm_enabled_  (*this, "lo-alarm-enabled",   false) \
	,lo_alarm_value_    (*this, "lo-alarm-value",     5)

void
UValueIndicatormb745::ctor()
{
	set_flags(Gtk::NO_WINDOW);

	layout_head_ = create_pango_layout("-");
	Pango::FontDescription fd("Liberation Sans" );
	fd.set_absolute_size(13 * Pango::SCALE);
	layout_head_->set_font_description(fd);

	layout_units_ = create_pango_layout("-");
	fd = Pango::FontDescription( "Liberation Sans" );
	fd.set_absolute_size(13 * Pango::SCALE);
	layout_units_->set_font_description(fd);

	layout_value_ = create_pango_layout("0.0");
	fd = Pango::FontDescription( "Liberation Sans Bold" );
	fd.set_absolute_size(13 * Pango::SCALE);
	layout_value_->set_font_description(fd);

	connect_property_changed("font-value", bind(sigc::mem_fun(*this, &UValueIndicatormb745::on_font_changed),&layout_value_,&property_font_value_,&property_font_value_size_) );
	connect_property_changed("font-value-size", bind(sigc::mem_fun(*this, &UValueIndicatormb745::on_font_changed),&layout_value_,&property_font_value_,&property_font_value_size_) );
	connect_property_changed("font-headname", bind(sigc::mem_fun(*this, &UValueIndicatormb745::on_font_changed),&layout_head_,&property_font_headname_,&property_font_headname_size_) );
	connect_property_changed("font-headname-size", bind(sigc::mem_fun(*this, &UValueIndicatormb745::on_font_changed),&layout_head_,&property_font_headname_,&property_font_headname_size_) );
	connect_property_changed("font-units", bind(sigc::mem_fun(*this, &UValueIndicatormb745::on_font_changed),&layout_units_,&property_font_units_,&property_font_units_size_) );
	connect_property_changed("font-units-size", bind(sigc::mem_fun(*this, &UValueIndicatormb745::on_font_changed),&layout_units_,&property_font_units_,&property_font_units_size_) );

	connect_property_changed("font-value-color", sigc::mem_fun(*this, &UValueIndicatormb745::on_value_color_changed) );

	connect_property_changed("precision", sigc::mem_fun(*this, &UValueIndicatormb745::on_value_changed) );
	connect_property_changed("digits", sigc::mem_fun(*this, &UValueIndicatormb745::on_value_changed) );
	connect_property_changed("fill-digits", sigc::mem_fun(*this, &UValueIndicatormb745::on_value_changed) );
	connect_property_changed( "value",
			sigc::mem_fun(*this,
				&UValueIndicatormb745::on_value_changed));
	connect_property_changed( "state",
			sigc::mem_fun(*this,
				&UValueIndicatormb745::on_state_changed));
	connect_property_changed( "headname",
			sigc::mem_fun(*this,
				&UValueIndicatormb745::on_headname_changed));
	connect_property_changed( "units",
			sigc::mem_fun(*this,
				&UValueIndicatormb745::on_units_changed));
	connect_property_changed( "use-hi-warning",
			sigc::mem_fun(*this,
				&UValueIndicatormb745::on_use_hi_warning_changed));
	connect_property_changed( "use-hi-alarm",
			sigc::mem_fun(*this,
				&UValueIndicatormb745::on_use_hi_alarm_changed));
	connect_property_changed( "use-lo-warning",
			sigc::mem_fun(*this,
				&UValueIndicatormb745::on_use_lo_warning_changed));
	connect_property_changed( "use-lo-alarm",
			sigc::mem_fun(*this,
				&UValueIndicatormb745::on_use_lo_alarm_changed));
	up_tri_color_ = GREY;
	down_tri_color_ = GREY;
	blinking_up_color_ = GREY;
	blinking_down_color_ = GREY;

	//add up default state
	Confirmer::StateKey state_k;
	state_k.id = -2;
	state_k.value = 0;
	ConfirmCtl::StateInfo state_i;
	state_i.priority = 1;
	state_i.blinkable = false;
	state_i.need_confirm = false;
	state_i.on = true;
	sigc::slot<void, TriColorType> switch_to;
	switch_to = sigc::mem_fun(*this,
			&UValueIndicatormb745::set_up_tri_color);
	state_i.switch_on = sigc::bind( switch_to, GREY);
	confirm_ctl_up_.add( state_k, state_i );

	//add down default state
	state_k.id = -2;
	state_k.value = 0;
	state_i.priority = 1;
	state_i.blinkable = false;
	state_i.need_confirm = false;
	state_i.on = true;
	switch_to = sigc::mem_fun(*this,
			&UValueIndicatormb745::set_down_tri_color);
	state_i.switch_on = sigc::bind( switch_to, GREY);
	confirm_ctl_down_.add( state_k, state_i );
}

UValueIndicatormb745::UValueIndicatormb745() :
	Glib::ObjectBase("uvalueindicatormb745")
	,UVALUEINDICATORMB745_INIT_PROPERTIES()

{
	ctor();
}

UValueIndicatormb745::UValueIndicatormb745(GtkmmBaseType::BaseObjectType* gobject) :
	BaseType(gobject)
	,UVALUEINDICATORMB745_INIT_PROPERTIES()
{
	ctor();
}

UValueIndicatormb745::~UValueIndicatormb745()
{
}

void UValueIndicatormb745::on_font_changed(Glib::RefPtr<Pango::Layout> *layout, Glib::Property<Glib::ustring> *font, Glib::Property<int> *font_size)
{
	Pango::FontDescription fd(*font);
	fd.set_absolute_size((*font_size).get_value() * Pango::SCALE);
	(*layout)->set_font_description(fd);
}

void UValueIndicatormb745::on_value_color_changed()
{
	value_color_ = property_font_value_color_.get_value();
}

void
UValueIndicatormb745::on_size_request(Gtk::Requisition* req)
{
	int width;
	int height;
	req->width = CIRCLE_D + LINE_WIDTH;

	if(use_tri)
		req->width += TRI_WIDTH + LINE_WIDTH;

	layout_value_->get_pixel_size(width, height);
	req->width += width + LINE_WIDTH;

	layout_units_->get_pixel_size(width, height);
	req->width += width + 3*LINE_WIDTH;

	req->height = CIRCLE_D/2 + BOX_HEIGHT+1;
}

bool
UValueIndicatormb745::on_expose_event(GdkEventExpose* event)
{
	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();

	const Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	int new_BOX_HEIGHT = BOX_HEIGHT;
	int new_CIRCLE_D = CIRCLE_D;

	if(alloc.get_height()<=BOX_HEIGHT)
	{
		new_BOX_HEIGHT = alloc.get_height()-LINE_WIDTH;
		new_CIRCLE_D = alloc.get_height()-LINE_WIDTH;
	}
//	else if(alloc.get_height()<(BOX_HEIGHT*3)/2)
//	{
//		new_BOX_HEIGHT = (alloc.get_height()*2)/3;
//		new_CIRCLE_D = (alloc.get_height()*2)/3;
//	}
	else
	{
		new_BOX_HEIGHT = BOX_HEIGHT;
		new_CIRCLE_D = CIRCLE_D;
	}

	int box_width = alloc.get_width() - new_CIRCLE_D/2;

	int head_x, head_y;
	int head_box_width = 0;
	layout_head_->get_pixel_size(head_x, head_y);
	if(head_x>new_CIRCLE_D - 6*LINE_WIDTH)
	{
		head_box_width = head_x - new_CIRCLE_D + 6*LINE_WIDTH;
		head_x = 3*LINE_WIDTH ;
	}
	else
		head_x = new_CIRCLE_D/2 - head_x/2 ;
	head_y = new_CIRCLE_D/2 - head_y/2 ;

	int v_w, v_h, u_w, u_h;
	layout_value_->get_pixel_size(v_w, v_h);
	layout_units_->get_pixel_size(u_w, u_h);

	int value_x;
/*	if ( use_tri )
		value_x = (alloc.get_width() + new_CIRCLE_D + TRI_WIDTH + 2*LINE_WIDTH - v_w - u_w - 4*LINE_WIDTH)/2 + head_box_width/2;
	else
		value_x = (alloc.get_width() - v_w - u_w)/2 + new_CIRCLE_D/2 + head_box_width/2;
*/
	value_x = alloc.get_width() - v_w - u_w - LINE_WIDTH;

//	int value_y = CIRCLE_D/2 + ( BOX_HEIGHT - v_h )/2;
	int value_y = alloc.get_height()-new_BOX_HEIGHT - new_BOX_HEIGHT%2 + ( new_BOX_HEIGHT - v_h )/2;

	int layout_units_x = alloc.get_width() - u_w - LINE_WIDTH;

	if ( value_x + v_w > layout_units_x )
		layout_units_x = value_x + v_w;

	cr->save();
	cr->translate(alloc.get_x(), alloc.get_y());
	cr->set_line_width(LINE_WIDTH);

	//draw shadow
	cr->set_source_rgba( 0, 0, 0, 1 );
	cr->arc( new_CIRCLE_D/2 + new_CIRCLE_D%2 + 1, new_CIRCLE_D/2 + new_CIRCLE_D%2 + 1, new_CIRCLE_D/2 + new_CIRCLE_D%2, M_PI/2, -M_PI/2 );
	cr->arc( new_CIRCLE_D/2 + new_CIRCLE_D%2 + head_box_width + 1, new_CIRCLE_D/2 + new_CIRCLE_D%2 + 1, new_CIRCLE_D/2 + new_CIRCLE_D%2, -M_PI/2, M_PI/2 );
//	cr->rectangle( CIRCLE_D/2 + 1, CIRCLE_D/2 + 1, box_width, BOX_HEIGHT);
	cr->rectangle( new_CIRCLE_D/2 + 1, alloc.get_height()-new_BOX_HEIGHT, box_width, new_BOX_HEIGHT + new_BOX_HEIGHT%2);
	cr->fill();

	//draw box border
	cr->set_source_rgba( 1, 1, 1, 1 );
//	cr->rectangle( CIRCLE_D/2, new_CIRCLE_D/2, box_width, BOX_HEIGHT );
	cr->rectangle( new_CIRCLE_D/2, alloc.get_height()-new_BOX_HEIGHT-1, box_width, new_BOX_HEIGHT + new_BOX_HEIGHT%2);
	cr->fill();

	//draw box background
	cr->set_source_rgba( 0.21, 0.21, 0.21, 1 );
//	cr->rectangle( CIRCLE_D/2+LINE_WIDTH, CIRCLE_D/2+LINE_WIDTH, box_width-2*LINE_WIDTH, BOX_HEIGHT-2*LINE_WIDTH );
	cr->rectangle( new_CIRCLE_D/2+LINE_WIDTH, alloc.get_height()-new_BOX_HEIGHT-1+LINE_WIDTH, box_width-2*LINE_WIDTH, new_BOX_HEIGHT-2*LINE_WIDTH + new_BOX_HEIGHT%2);
	cr->fill();

	//draw circle border
	cr->set_source_rgba( 1, 1, 1, 1 );
	cr->arc( new_CIRCLE_D/2 + new_CIRCLE_D%2, new_CIRCLE_D/2 + new_CIRCLE_D%2, new_CIRCLE_D/2 + new_CIRCLE_D%2, M_PI/2, -M_PI/2 );
	cr->arc( new_CIRCLE_D/2 + new_CIRCLE_D%2 + head_box_width, new_CIRCLE_D/2 + new_CIRCLE_D%2, new_CIRCLE_D/2 + new_CIRCLE_D%2, -M_PI/2, M_PI/2 );
	cr->fill();

	//draw circle background
	cr->set_source_rgba( 0.21, 0.21, 0.21, 1 );
	cr->arc( new_CIRCLE_D/2 + new_CIRCLE_D%2, new_CIRCLE_D/2 + new_CIRCLE_D%2, new_CIRCLE_D/2 + new_CIRCLE_D%2 - LINE_WIDTH, M_PI/2, -M_PI/2 );
	cr->arc( new_CIRCLE_D/2 + new_CIRCLE_D%2 + head_box_width, new_CIRCLE_D/2 + new_CIRCLE_D%2, new_CIRCLE_D/2 + new_CIRCLE_D%2 - LINE_WIDTH, -M_PI/2, M_PI/2 );
	cr->fill();

	//draw value
	cr->set_source_rgb( value_color_.get_red_p(),
			    value_color_.get_green_p(),
			    value_color_.get_blue_p() );
	cr->move_to(value_x, value_y);
	layout_value_->add_to_cairo_context(cr);
	cr->fill();

	//draw text in circle (head)
	cr->set_source_rgb( property_font_headname_color_.get_value().get_red_p(),
			    property_font_headname_color_.get_value().get_green_p(),
			    property_font_headname_color_.get_value().get_blue_p() );
	cr->move_to(head_x, head_y);
	layout_head_->add_to_cairo_context(cr);
	cr->fill();

	//draw units
	cr->set_source_rgb( property_font_units_color_.get_value().get_red_p(),
			    property_font_units_color_.get_value().get_green_p(),
			    property_font_units_color_.get_value().get_blue_p() );
	cr->move_to(layout_units_x, value_y);
	layout_units_->add_to_cairo_context(cr);
	cr->fill();

	//draw top triangle
	if (use_hi_warning_ || use_hi_alarm_) {
		switch (up_tri_color_) {
			case RED:
				cr->set_source_rgb(1,0,0);
				break;
			case YELLOW:
				cr->set_source_rgb(1,208./255,0);
				break;
			case GREY:
				cr->set_source_rgb(77./255,77./255,77./255);
				break;
		}
		draw_up_tri(cr);
	}

	//draw down triangle
	if (use_lo_warning_ || use_lo_alarm_) {
		switch (down_tri_color_) {
			case RED:
				cr->set_source_rgb(1,0,0);
				break;
			case YELLOW:
				cr->set_source_rgb(1,208./255,0);
				break;
			case GREY:
				cr->set_source_rgb(77./255,77./255,77./255);
				break;
		}
		draw_down_tri(cr);
	}

	cr->restore();

	if (!connected_ && get_property_disconnect_effect() == 1)
		UVoid::draw_disconnect_effect_1(cr, alloc);

	return true;
}

/*// ---------------------------------------------------------
void UValueIndicatormb745::sensorInfo(SensorMessage* sm)
{
	if( ai.get_sens_id() == sm->id  )
		value = ( sm->value / pow10(sm->ci.precision) );
	else if ( sm->id == hi_warn_sensor.get_sens_id()
			&& use_hi_warning )
	{
		Confirmer::StateKey sk;
		sk.id = sm->id;
		sk.value = 1;
		_confirm_ctl_up.update(sk, sm->value);
	}
	else if ( sm->id == hi_alarm_sensor.get_sens_id()
			&& use_hi_alarm)
	{
		Confirmer::StateKey sk;
		sk.id = sm->id;
		sk.value = 1;
		_confirm_ctl_up.update(sk, sm->value);
	}
	else if ( sm->id == lo_warn_sensor.get_sens_id()
			&& use_lo_warning )
	{
		Confirmer::StateKey sk;
		sk.id = sm->id;
		sk.value = 1;
		_confirm_ctl_down.update(sk, sm->value);
	}
	else if ( sm->id == lo_alarm_sensor.get_sens_id()
			&& use_lo_alarm)
	{
		Confirmer::StateKey sk;
		sk.id = sm->id;
		sk.value = 1;
		_confirm_ctl_down.update(sk, sm->value);
	}
	return;
}
// ---------------------------------------------------------*/

/*//----------------------------------------------------------
void UValueIndicatormb745::askSensors(UniversalIO::UIOCommand cmd)
{
	if ( !gpm && !(gpm=search_gpm()))
	{
		Exception ex( "(" + get_name() + ")askSensors: No Active ProxyManager to make request.");
		throw ex;
	}
	ai.autoAsk(UniversalIO::UIONotifyChange);

	SensorMessage sm;
	sm.id = ai.get_sens_id();
	sm.state = sm.value = ai.get_value();
	sensorInfo(&sm);

	if ( use_hi_warning )
	{
		hi_warn_sensor.autoAsk(UniversalIO::UIONotifyChange);
		sm.id = hi_warn_sensor.get_sens_id();
		sm.state = sm.value = hi_warn_sensor.get_value();
		sensorInfo(&sm);
	}
	if ( use_hi_alarm )
	{
		hi_alarm_sensor.autoAsk(UniversalIO::UIONotifyChange);
		sm.id = hi_alarm_sensor.get_sens_id();
		sm.state = sm.value = hi_alarm_sensor.get_value();
		sensorInfo(&sm);
	}
	if ( use_lo_warning )
	{
		lo_warn_sensor.autoAsk(UniversalIO::UIONotifyChange);
		sm.id = lo_warn_sensor.get_sens_id();
		sm.state = sm.value = lo_warn_sensor.get_value();
		sensorInfo(&sm);
	}
	if ( use_lo_alarm )
	{
		lo_alarm_sensor.autoAsk(UniversalIO::UIONotifyChange);
		sm.id = lo_alarm_sensor.get_sens_id();
		sm.state = sm.value = lo_alarm_sensor.get_value();
		sensorInfo(&sm);
	}
}
// ---------------------------------------------------------*/

void
UValueIndicatormb745::switch_state(int state) //ValueIndicatorState state
{
	switch (state) {
		case 1: //STATE_NORMAL:
			value_color_ = property_font_value_color_.get_value();
			lab_color_ = Gdk::Color("white");
			break;
		case 2://STATE_HI_WARNING:
			value_color_ = Gdk::Color("yellow");
			lab_color_ = Gdk::Color("yellow");
			break;
		case 3://STATE_HI_ALARM:
			value_color_ = Gdk::Color("red");
			lab_color_ = Gdk::Color("red");
			break;
		case 4://STATE_LO_WARNING:
			value_color_ = Gdk::Color("yellow");
			lab_color_ = Gdk::Color("yellow");
			break;
		case 5://STATE_LO_ALARM:
			value_color_ = Gdk::Color("red");
			lab_color_ = Gdk::Color("red");
			break;
		default:
			break;
	}
}

void
UValueIndicatormb745::on_state_changed()
{
	switch_state(property_state_);
}

void
UValueIndicatormb745::on_value_changed()
{
	float value = property_value_;
	if      (hi_alarm_enabled_   && value/pow10(property_precision_) >= hi_alarm_value_)
			switch_state(3);
	else if (hi_warning_enabled_ && value/pow10(property_precision_) >= hi_warning_value_)
			switch_state(2);
	else if (lo_alarm_enabled_   && value/pow10(property_precision_) <= lo_alarm_value_)
			switch_state(5);
	else if (lo_warning_enabled_ && value/pow10(property_precision_) <= lo_warning_value_)
			switch_state(4);
	else
		switch_state(1);

	std::ostringstream title;
	value = property_value_/pow10(property_precision_);
	title<<(value>=0? "":"-");
	value = fabs(value);
	if(property_fill_digits_>0)
		title<<std::setw(property_fill_digits_)<<setfill('0')<<(long)value;
	else
		title<<(long)value;
	if(property_digits_>0)
		title<<","<<std::setw(property_digits_)<<setfill('0')<<(long)(fabs(((float)value-(long)value )*pow10(property_digits_)));

//	ustring value = ustring::format(std::fixed,
//			std::setprecision(property_precision_),
//			std::setw(property_digits_),
//			std::setfill(L'0'),
//			property_value_/pow10(property_precision_));
//	cout<<"!!!!!!!!!! new_value = "<<title.str()<<endl;
	layout_value_->set_text(title.str());

	queue_draw();
}

void
UValueIndicatormb745::on_headname_changed()
{
	layout_head_->set_text(property_headname_);
}

void
UValueIndicatormb745::on_units_changed()
{
	layout_units_->set_text(property_units_);
}

void
UValueIndicatormb745::update_mics()
{
/*	if( use_hi_warning || use_hi_alarm )
		_himic.set_child_visible(true);
	else
		_himic.set_child_visible(false);

	if ( use_lo_warning || use_lo_alarm )
		_lomic.set_child_visible(true);
	else
		_lomic.set_child_visible(false);
*/
	queue_resize();
}

void
UValueIndicatormb745::on_use_hi_warning_changed()
{
	if( !get_connector() )
		return;

	Confirmer::StateKey state_k;
	state_k.id = hi_warn_sensor_.get_sens_id();
	state_k.value = 1;
	if (use_hi_warning_) {
		ConfirmCtl::StateInfo state_i;
		state_i.on = false;
		state_i.need_confirm = true;
		state_i.blinkable = true;
		state_i.confirmed = true;
		state_i.priority = 2;

		sigc::slot<void, TriColorType> start_blink;
		start_blink = sigc::mem_fun(*this,
				&UValueIndicatormb745::start_blink_up);

		state_i.start_blink = sigc::bind(start_blink, YELLOW);

		state_i.stop_blink =	sigc::mem_fun(*this,
				&UValueIndicatormb745::stop_blink_up);

		sigc::slot<void, TriColorType> switch_to;
		switch_to = sigc::mem_fun(*this,
				&UValueIndicatormb745::set_up_tri_color);
		state_i.switch_on = sigc::bind( switch_to, YELLOW);

		sigc::slot<void, sigc::slot<void>, ObjectId> connect_confirm;
		connect_confirm = sigc::mem_fun(*this,
				&UValueIndicatormb745::connect_confirm);
		state_i.connect_confirm = sigc::bind(connect_confirm,
				hi_warn_sensor_.get_sens_id());

		confirm_ctl_up_.add(state_k, state_i);
	}
	else
		confirm_ctl_up_.remove(state_k);
}

void
UValueIndicatormb745::on_use_hi_alarm_changed()
{
	if( !get_connector() )
		return;

	Confirmer::StateKey state_key;
	state_key.id = hi_alarm_sensor_.get_sens_id();
	state_key.value = 1;
	if (use_hi_alarm_) {
		ConfirmCtl::StateInfo state_i;
		state_i.on = false;
		state_i.need_confirm = true;
		state_i.blinkable = true;
		state_i.confirmed = true;
		state_i.priority = 3;

		sigc::slot<void, TriColorType> start_blink;
		start_blink = sigc::mem_fun(*this,
				&UValueIndicatormb745::start_blink_up);
		state_i.start_blink = sigc::bind(start_blink, RED);

		state_i.stop_blink = sigc::mem_fun(*this,
				&UValueIndicatormb745::stop_blink_up);

		sigc::slot<void, TriColorType> switch_to;
		switch_to = sigc::mem_fun(*this,
				&UValueIndicatormb745::set_up_tri_color);
		state_i.switch_on = sigc::bind( switch_to, RED);

		sigc::slot<void, sigc::slot<void>, ObjectId> connect_confirm;
		connect_confirm = sigc::mem_fun(*this,
				&UValueIndicatormb745::connect_confirm);
		state_i.connect_confirm =sigc::bind(connect_confirm,
				hi_alarm_sensor_.get_sens_id());

		confirm_ctl_up_.add(state_key, state_i);
	}
	else
		confirm_ctl_up_.remove(state_key);
}

void
UValueIndicatormb745::on_use_lo_warning_changed()
{
	if( !get_connector() )
		return;

	Confirmer::StateKey state_key;
	state_key.id = lo_warn_sensor_.get_sens_id();
	state_key.value = 1;
	if (use_lo_warning_) {
		ConfirmCtl::StateInfo state_i;
		state_i.on = false;
		state_i.need_confirm = true;
		state_i.blinkable = true;
		state_i.confirmed = true;
		state_i.priority = 2;

		sigc::slot<void, TriColorType> start_blink;
		start_blink = sigc::mem_fun(*this,
				&UValueIndicatormb745::start_blink_down);
		state_i.start_blink = sigc::bind(start_blink, YELLOW);

		state_i.stop_blink = sigc::mem_fun(*this,
				&UValueIndicatormb745::stop_blink_down);

		sigc::slot<void, TriColorType> switch_to;
		switch_to = sigc::mem_fun(*this,
				&UValueIndicatormb745::set_down_tri_color);
		state_i.switch_on = sigc::bind( switch_to, YELLOW);

		sigc::slot<void, sigc::slot<void>, ObjectId> connect_confirm;
		connect_confirm = sigc::mem_fun(*this,
				&UValueIndicatormb745::connect_confirm);
		state_i.connect_confirm =sigc::bind(connect_confirm,
				lo_warn_sensor_.get_sens_id());

		confirm_ctl_down_.add(state_key, state_i);
	}
	else
		confirm_ctl_down_.remove(state_key);
}

void
UValueIndicatormb745::on_use_lo_alarm_changed()
{
	if( !get_connector() )
		return;

	Confirmer::StateKey state_key;
	state_key.id = lo_alarm_sensor_.get_sens_id();
	state_key.value = 1;
	if (use_lo_alarm_) {
		ConfirmCtl::StateInfo state_i;
		state_i.on = false;
		state_i.need_confirm = true;
		state_i.blinkable = true;
		state_i.confirmed = true;
		state_i.priority = 3;

		sigc::slot<void, TriColorType> start_blink;
		start_blink = sigc::mem_fun(*this,
				&UValueIndicatormb745::start_blink_down);
		state_i.start_blink = sigc::bind(start_blink, RED);

		state_i.stop_blink = sigc::mem_fun(*this,
				&UValueIndicatormb745::stop_blink_down);

		sigc::slot<void, TriColorType> switch_to;
		switch_to = sigc::mem_fun(*this,
				&UValueIndicatormb745::set_down_tri_color);
		state_i.switch_on = sigc::bind( switch_to, RED);

		sigc::slot<void, sigc::slot<void>, ObjectId> connect_confirm;
		connect_confirm = sigc::mem_fun(*this,
				&UValueIndicatormb745::connect_confirm);
		state_i.connect_confirm =sigc::bind(connect_confirm,
				lo_alarm_sensor_.get_sens_id());

		confirm_ctl_down_.add(state_key, state_i);
	}
	else
		confirm_ctl_down_.remove(state_key);
}

void
UValueIndicatormb745::connect_confirm(sigc::slot<void> confirm, ObjectId id)
{
 	get_connector()->signal_checked().connect(sigc::hide(confirm), id);
}

void
UValueIndicatormb745::start_blink_up(TriColorType type)
{
	blinking_up_color_ = type;
	blink_connection_.disconnect();
	blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].connect(sigc::mem_fun(this,
				&UValueIndicatormb745::blink));
}

void
UValueIndicatormb745::start_blink_down(TriColorType type)
{
	blinking_down_color_ = type;
	blink_connection_.disconnect();
	blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].connect(sigc::mem_fun(this,
				&UValueIndicatormb745::blink));
}

void
UValueIndicatormb745::stop_blink_up()
{
	up_tri_color_ = blinking_up_color_;
	blinking_up_color_ = GREY;
	blink_connection_.disconnect();
	queue_draw();
}

void
UValueIndicatormb745::stop_blink_down()
{
	down_tri_color_ = blinking_down_color_;
	blinking_down_color_ = GREY;
	blink_connection_.disconnect();
	queue_draw();
}

void UValueIndicatormb745::blink(bool state, int time)
{
	if ( state == true ) {
		up_tri_color_ = blinking_up_color_;
		down_tri_color_ = blinking_down_color_;
	}
	else {
		up_tri_color_ = GREY;
		down_tri_color_ = GREY;
	}
	queue_draw();
}

void
UValueIndicatormb745::set_up_tri_color(TriColorType color)
{
	up_tri_color_ = color;
	queue_draw();
}

void
UValueIndicatormb745::set_down_tri_color(TriColorType color)
{
	down_tri_color_ = color;
	queue_draw();
}

void
UValueIndicatormb745::draw_up_tri(Cairo::RefPtr<Cairo::Context>& cr)
{
	const Gtk::Allocation alloc = get_allocation();

	int new_BOX_HEIGHT = BOX_HEIGHT;
	int new_CIRCLE_D = CIRCLE_D;
	int new_TRI_HEIGHT = TRI_HEIGHT;
	int new_TRI_WIDTH = TRI_WIDTH;

	if(alloc.get_height()<=BOX_HEIGHT)
	{
		new_BOX_HEIGHT = alloc.get_height()-LINE_WIDTH-1;
		new_CIRCLE_D = alloc.get_height()-LINE_WIDTH-1;
		new_TRI_HEIGHT = (new_BOX_HEIGHT - 5*LINE_WIDTH) / 2;
		new_TRI_WIDTH = 2*new_TRI_HEIGHT / sqr3;
	}

	cr->save();
	cr->move_to(new_CIRCLE_D+LINE_WIDTH, new_CIRCLE_D/2 + new_TRI_HEIGHT + 2*LINE_WIDTH );
	cr->line_to(new_CIRCLE_D+LINE_WIDTH+new_TRI_WIDTH, new_CIRCLE_D/2 + new_TRI_HEIGHT + 2*LINE_WIDTH );
	cr->line_to(new_CIRCLE_D+LINE_WIDTH+new_TRI_WIDTH/2, new_CIRCLE_D/2+2*LINE_WIDTH);
	cr->close_path();
	cr->fill();
	cr->restore();
}

void
UValueIndicatormb745::draw_down_tri(Cairo::RefPtr<Cairo::Context>& cr)
{
	const Gtk::Allocation alloc = get_allocation();

	int new_BOX_HEIGHT = BOX_HEIGHT;
	int new_CIRCLE_D = CIRCLE_D;
	int new_TRI_HEIGHT = TRI_HEIGHT;
	int new_TRI_WIDTH = TRI_WIDTH;

	if(alloc.get_height()<=BOX_HEIGHT)
	{
		new_BOX_HEIGHT = alloc.get_height()-LINE_WIDTH-1;
		new_CIRCLE_D = alloc.get_height()-LINE_WIDTH-1;
		new_TRI_HEIGHT = (new_BOX_HEIGHT - 5*LINE_WIDTH) / 2;
		new_TRI_WIDTH = 2*new_TRI_HEIGHT / sqr3;
	}

	cr->save();
	cr->move_to(new_CIRCLE_D+LINE_WIDTH, new_CIRCLE_D/2 + new_TRI_HEIGHT + 4*LINE_WIDTH );
	cr->line_to(new_CIRCLE_D+LINE_WIDTH+new_TRI_WIDTH, new_CIRCLE_D/2 + new_TRI_HEIGHT + 4*LINE_WIDTH );
	cr->line_to(new_CIRCLE_D+LINE_WIDTH+new_TRI_WIDTH/2, new_CIRCLE_D/2+4*LINE_WIDTH+2*new_TRI_HEIGHT);
	cr->close_path();
	cr->fill();
	cr->restore();
}

/*/----------------------------------------------------------
void UValueIndicatormb745::start_blink_tri( TriType tri )
{
	if ( tri == TRI_UP_YELLOW || tri = TRI_UP_RED )
	{
		if (_up_blink_conn.empty() )
			up_blink_conn =
				blinker.signal_blink.connect(sigc::mem_fun(this,
						&UValueIndicatormb745::blink));
		_up_tri = tri;
	}
	else if ( tri == TRI_DOWN_YELLOW || tri == TRI_DOWN_RED )
	{
		if (_down_blink_conn.empty() )
			_down_blink_conn =
				blinker.signal_blink.connect(sigc::mem_fun(this,
						&UValueIndicatormb745::blink));
		_down_tri = tri;
	}
}
//--------------------------------------------------------*/

/*/----------------------------------------------------------
void UValueIndicatormb745::on_use_NN_changed( Glib::Property<bool>* NNprop,
		SensProperty* sprop, TriColorType blink_color, int priority )
{
	if( get_connector() == nullptr )
		return;

	Confirmer::StateKey state_k;
	state_k.id = sprop->get_sens_id();
	state_k.value = 1;
	if ( NNProp->get_value() )
	{
		ConfirmCtl::StateInfo state_i;
		state_i.on = false;
		state_i.need_confirm = true;
		state_i.blinkable = true;
		state_i.confirmed = true;
		state_i.priority = priority;

		sigc::slot<void, TriColorType> start_blink;
		start_blink = sigc::mem_fun(*this,
				&UValueIndicatormb745::start_blink_up);

		state_i.start_blink = sigc::bind(start_blink, blink_color);

		state_i.stop_blink =	sigc::mem_fun(*this,
				&UValueIndicatormb745::stop_blink_up);

		sigc::slot<void, TriColorType> switch_to;
		switch_to = sigc::mem_fun(*this,
				&UValueIndicatormb745::set_up_tri_color);
		state_i.switch_on = sigc::bind( switch_to, YELLOW);

		sigc::slot<void, sigc::slot<void>, ObjectId> connect_confirm;
		connect_confirm = sigc::mem_fun(*this,
				&UValueIndicatormb745::connect_confirm);
		state_i.connect_confirm = sigc::bind(connect_confirm,
				hi_warn_sensor.get_sens_id());

		_confirm_controller.add(state_k, state_i);
	}
	else
		_confirm_controller.remove(state_k);

}
//---------------------------------------------------------*/

void
UValueIndicatormb745::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UVoid::set_connector(connector);

	ai_.set_connector(connector);

	sensor_connection_.disconnect();

	if (get_connector()) {
		sigc::slot<void, ObjectId, ObjectId, float> process_sensor_slot =
				sigc::mem_fun(this, &UValueIndicatormb745::process_sensor);
		sensor_connection_ = get_connector()->signals().connect_analog_value_changed(
				process_sensor_slot,
				ai_.get_sens_id(),
				ai_.get_node_id());
		on_connect();
	}
}

void
UValueIndicatormb745::on_connect() throw()
{
	UVoid::on_connect();

	float new_value = get_connector()->
		get_analog_value(ai_.get_sens_id(), ai_.get_node_id());

	process_sensor(ai_.get_sens_id(), ai_.get_node_id(), new_value);

	if(get_property_disconnect_effect() > 0)
		queue_draw();
}

void
UValueIndicatormb745::on_disconnect() throw()
{
	UVoid::on_connect();
	UVoid::on_disconnect();

	if(get_property_disconnect_effect() > 0)
		queue_draw();
}

void
UValueIndicatormb745::on_realize()
{
	Gtk::Widget::on_realize();
	if(get_connector())
		process_sensor(ai_.get_sens_id(),ai_.get_node_id(),ai_.get_value());
}

void
UValueIndicatormb745::process_sensor(ObjectId id, ObjectId node, float value)
{
	property_value_ = value;
}
