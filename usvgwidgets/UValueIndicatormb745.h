#ifndef _UVALUEINDICATORMB745_H
#define _UVALUEINDICATORMB745_H

#include <gtkmm.h>
#include <gdkmm.h>
#include <UDefaultFunctions.h>
#include <SensorProp.h>
#include <ConfirmController.h>
#include <CheckedSignal.h>
#include <global_macros.h>

class UValueIndicatormb745 : public UDefaultFunctions<Gtk::Widget>
{
public:
	enum ValueIndicatorState
	{
		STATE_NORMAL     = 0,
		STATE_HI_WARNING = 1,
		STATE_HI_ALARM   = 2,
		STATE_LO_WARNING = 3,
		STATE_LO_ALARM   = 4
	};

	UValueIndicatormb745();
	explicit UValueIndicatormb745(GtkmmBaseType::BaseObjectType* gobject);
	~UValueIndicatormb745();

	void switch_state(int st);
	inline void set_value(double value) { property_value_.set_value(value); }
	
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void set_connector(const ConnectorRef& connector) throw();

	void set_fill_digits(const int digits);

protected:
	virtual void on_realize();
	virtual void on_size_request(Gtk::Requisition*);
	virtual bool on_expose_event(GdkEventExpose*);

private:
	enum TriColorType {RED, YELLOW, GREY};

	void ctor();

	void process_sensor(UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, float);
	void connect_confirm( sigc::slot<void>, UniWidgetsTypes::ObjectId );
	void on_value_changed();

	void on_font_changed(Glib::RefPtr<Pango::Layout> *layout, Glib::Property<Glib::ustring> *font, Glib::Property<int> *font_size);
	void on_value_color_changed();

	void on_state_changed();
	void on_headname_changed();
	void on_units_changed();
	void update_mics();
	void on_use_hi_warning_changed();
	void on_use_hi_alarm_changed();
	void on_use_lo_alarm_changed();
	void on_use_lo_warning_changed();

	void draw_up_tri(Cairo::RefPtr<Cairo::Context>&);
	void draw_down_tri(Cairo::RefPtr<Cairo::Context>&);

	void blink(bool state, int time=DEFAULT_BLINK_TIME);
	void start_blink_up(TriColorType type);
	void start_blink_down(TriColorType type);
	void stop_blink_up();
	void stop_blink_down();

	void set_up_tri_color(TriColorType type);
	void set_down_tri_color(TriColorType type);

	SensorProp ai_;
	Glib::Property<int> property_state_;

	Glib::Property<int> property_precision_;
	Glib::Property<int> property_digits_;
	Glib::Property<int> property_fill_digits_;
	Glib::Property<float> property_value_;
	Glib::Property<Glib::ustring> property_font_value_;
	Glib::Property<int> property_font_value_size_;
	Glib::Property<Gdk::Color> property_font_value_color_;

	Glib::Property<Glib::ustring> property_headname_;
	Glib::Property<Glib::ustring> property_font_headname_;
	Glib::Property<int> property_font_headname_size_;
	Glib::Property<Gdk::Color> property_font_headname_color_;
	Glib::Property<Glib::ustring> property_units_;
	Glib::Property<Glib::ustring> property_font_units_;
	Glib::Property<int> property_font_units_size_;
	Glib::Property<Gdk::Color> property_font_units_color_;

	SensorProp hi_warn_sensor_;
	SensorProp hi_alarm_sensor_;
	SensorProp lo_warn_sensor_;
	SensorProp lo_alarm_sensor_;

	Glib::Property<bool> use_hi_warning_;
	Glib::Property<bool> use_lo_warning_;
	Glib::Property<bool> use_hi_alarm_;
	Glib::Property<bool> use_lo_alarm_;

	Glib::Property<bool>  hi_warning_enabled_;
	Glib::Property<float> hi_warning_value_;

	Glib::Property<bool>  lo_warning_enabled_;
	Glib::Property<float> lo_warning_value_;

	Glib::Property<bool>  hi_alarm_enabled_;
	Glib::Property<float> hi_alarm_value_;

	Glib::Property<bool>  lo_alarm_enabled_;
	Glib::Property<float> lo_alarm_value_;

	typedef ConfirmCtl::ConfirmController<> Confirmer;
	Confirmer confirm_ctl_up_;
	Confirmer confirm_ctl_down_;

	Glib::RefPtr<Pango::Layout> layout_head_;
	Glib::RefPtr<Pango::Layout> layout_units_;
	Glib::RefPtr<Pango::Layout> layout_value_;

	Gdk::Color value_color_;
	Gdk::Color lab_color_;

	TriColorType up_tri_color_;
	TriColorType down_tri_color_;
	TriColorType blinking_up_color_;
	TriColorType blinking_down_color_;

	sigc::connection blink_connection_;
	sigc::connection sensor_connection_;
};

#endif
