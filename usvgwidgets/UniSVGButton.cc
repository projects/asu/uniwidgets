#include "UniSVGButton.h"
//#include <guiconfiguration.h>
//#include <sstream>
//#include <fstream>

using namespace std;
using namespace UniWidgetsTypes;

#define INIT_UniSVGButton_PROPERTIES() \
	on_push(false) \
	,in_focus(false) \
	,in_fade(false) \
	,in_push_fade(false) \
	,in_focus_fade(false) \
	,in_state_fade(false) \
	,fade_img_is_init(false) \
	,sensor(this,"push",get_connector()) \
	,property_push_value_(*this, "push-value" , 1) \
	,property_realize_value_(*this, "realize-value" , 0) \
	,property_init_sensor_by_realize_value_(*this, "init-push-sensor-by-realize-value" , false) \
	,property_fixed_button_(*this, "fixed-button" , true) \
	,property_image0_push_path_(*this, "image0-push-path" ) \
	,property_image0_push_on_top_(*this, "image0-push-on-top-in-fade-mode" , false) \
	,property_image1_push_path_(*this, "image1-push-path" ) \
	,property_image1_push_on_top_(*this, "image1-push-on-top-in-fade-mode" , false) \
	,property_image0_focus_path_(*this, "image0-focus-path" ) \
	,property_image0_focus_on_top_(*this, "image0-focus-on-top-in-fade-mode" , true) \
	,property_image1_focus_path_(*this, "image1-focus-path" ) \
	,property_image1_focus_on_top_(*this, "image1-focus-on-top-in-fade-mode" , true) \
	,property_image0_push_focus_path_(*this, "image0-push-focus-path" ) \
	,property_image0_push_focus_on_top_(*this, "image0-push-focus-on-top-in-fade-mode" , true) \
	,property_image1_push_focus_path_(*this, "image1-push-focus-path" ) \
	,property_image1_push_focus_on_top_(*this, "image1-push-focus-on-top-in-fade-mode" , true) \
	\
	,property_text_(*this, "text", "") \
	,property_text_on_(*this, "text-on", "") \
	,property_text_font_name_(*this, "text-font-name", "Liberation Sans Bold 12") \
	,property_text_abs_font_size_(*this, "text-abs-font-size", -1) \
	,property_text_off_color_(*this, "text-off-color", Gdk::Color("black") ) \
	,property_text_on_color_(*this, "text-on-color" , Gdk::Color("black") ) \
	,property_alignment_(*this, "text-alignment", Pango::ALIGN_CENTER) \
	,property_drop_shadow_(*this, "text-shadow", false) \


void UniSVGButton::constructor()
{
	layout = create_pango_layout("");
	alignment = CENTER;

	signal_event().connect(sigc::mem_fun(*this, &UniSVGButton::on_button_event),true);
//	signal_size_allocate().connect(sigc::mem_fun(*this, &UniSVGButton::image_pixbuf_check));

	set_property_auto_connect(true);

	sigc::slot<void> image_path_changed = sigc::bind(sigc::mem_fun(*this, &UniSVGButton::image_path_check),true);

	sigc::slot<void> redraw =
		sigc::mem_fun(*this, &USVGIndicatorPlus::queue_draw);

	connect_property_changed("image0-push-path", image_path_changed);
	connect_property_changed("image0-push-path", redraw);
	connect_property_changed("image1-push-path", image_path_changed);
	connect_property_changed("image1-push-path", redraw);
	connect_property_changed("image0-focus-path", image_path_changed);
	connect_property_changed("image0-focus-path", redraw);
	connect_property_changed("image1-focus-path", image_path_changed);
	connect_property_changed("image1-focus-path", redraw);
	connect_property_changed("image0-push-focus-path", image_path_changed);
	connect_property_changed("image0-push-focus-path", redraw);
	connect_property_changed("image1-push-focus-path", image_path_changed);
	connect_property_changed("image1-push-focus-path", redraw);
	connect_property_changed("state", sigc::mem_fun(*this, &UniSVGButton::state_changed));

	connect_property_changed("text", sigc::mem_fun(*this, &UniSVGButton::text_changed));
	connect_property_changed("text-on", sigc::mem_fun(*this, &UniSVGButton::text_changed));
	connect_property_changed("text-font-name", sigc::mem_fun(*this, &UniSVGButton::font_changed));
	connect_property_changed("text-abs-font-size", sigc::mem_fun(*this, &UniSVGButton::font_changed));
	connect_property_changed("text-off-color", redraw);
	connect_property_changed("text-on-color", redraw);
	connect_property_changed("text-alignment", sigc::mem_fun(*this, &UniSVGButton::alignment_changed));
	connect_property_changed("text-shadow", redraw);
}
//---------------------------------------------------------------------------------------
UniSVGButton::UniSVGButton() :
	Glib::ObjectBase("unisvgbutton")
	,USVGIndicatorPlus()
	,INIT_UniSVGButton_PROPERTIES()
{
	constructor();
}
//---------------------------------------------------------------------------------------
UniSVGButton::UniSVGButton(GtkmmBaseType::BaseObjectType* gobject) :
	USVGIndicatorPlus(gobject)
	,INIT_UniSVGButton_PROPERTIES()
{
	constructor();
}
//---------------------------------------------------------------------------------------
UniSVGButton::~UniSVGButton()
{
}
//---------------------------------------------------------------------------------------
void
UniSVGButton::on_value_changed(std::string prop)
{
}
//---------------------------------------------------------------------------------------
void UniSVGButton::configure()
{
	if( get_connector() )
		process_sensor(sensor.get_sens_id(),sensor.get_node_id(),sensor.get_value());

	layout->set_text(property_text_);
	alignment_changed();
}
//---------------------------------------------------------------------------------------
void UniSVGButton::on_connect() throw()
{
	USVGIndicatorPlus::on_connect();
	configure();
}
//---------------------------------------------------------------------------------------
void UniSVGButton::on_disconnect() throw()
{
	USVGIndicatorPlus::on_disconnect();
}
//---------------------------------------------------------------------------------------
void UniSVGButton::on_realize()
{
	UEventBox::on_realize();
	if(property_init_sensor_by_realize_value_.get_value())
		sensor.save_value(property_realize_value_.get_value());
	configure();
	state_changed();
	image_path_check(false);
}
//---------------------------------------------------------------------------------------
bool UniSVGButton::on_expose_event(GdkEventExpose* event)
{
//	USVGIndicatorPlus::on_expose_event(event);
	UEventBox::on_expose_event(event);

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	const Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	if( get_fade_state())
		Gdk::Cairo::set_source_pixbuf(cr, fade_image_ref_,alloc.get_x(), alloc.get_y());
	else
	{
		Gdk::Cairo::set_source_pixbuf(cr, get_state() ?
					(in_focus ?
						(on_push ? image1_push_focus_ref_ : image1_focus_ref_):
						(on_push ? image1_push_ref_ : image1_ref_)
					):
					(in_focus ?
						(on_push ? image0_push_focus_ref_ : image0_focus_ref_):
						(on_push ? image0_push_ref_ : image0_ref_)
					),
					 alloc.get_x(), alloc.get_y());
	}
	cr->paint();

	int w,h;
	layout->get_pixel_size(w,h);
	if (property_drop_shadow_)
	{
		cr->set_source_rgb(0,0,0);
		if(alignment == LEFT)
			cr->move_to( alloc.get_x()+1, alloc.get_y()+(alloc.get_height()-h)/2+1 );
		else if(alignment == RIGHT)
			cr->move_to( alloc.get_x()+(alloc.get_width()-w)+1, alloc.get_y()+(alloc.get_height()-h)/2+1 );
		else
			cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2+1, alloc.get_y()+(alloc.get_height()-h)/2+1 );
		layout->add_to_cairo_context(cr);
		cr->fill();
	}
	Gdk::Color current_color = get_state() ? property_text_on_color_ : property_text_off_color_;
	cr->set_source_rgb( current_color.get_red_p(),current_color.get_green_p(),current_color.get_blue_p());
	if(alignment == LEFT)
		cr->move_to( alloc.get_x(), alloc.get_y()+(alloc.get_height()-h)/2);
	else if(alignment == RIGHT)
		cr->move_to( alloc.get_x()+(alloc.get_width()-w), alloc.get_y()+(alloc.get_height()-h)/2);
	else
		cr->move_to( alloc.get_x()+(alloc.get_width()-w)/2, alloc.get_y()+(alloc.get_height()-h)/2);
	layout->add_to_cairo_context(cr);
	cr->fill();


	if ( !connected_ && get_property_disconnect_effect() == 1)
		UVoid::draw_disconnect_effect_1(cr, alloc);

	return true;
}
//---------------------------------------------------------------------------------------
void UniSVGButton::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	sensor.set_connector(connector);

	USVGIndicatorPlus::set_connector(connector);

	sensorPropConnection.disconnect();
	if (get_connector() == true) {
		sigc::slot<void, ObjectId, ObjectId, long> processSensorPropSlot =
				sigc::mem_fun(this, &UniSVGButton::process_sensor);
		sensorPropConnection = get_connector()->signals().connect_value_changed(
				processSensorPropSlot,
   				sensor.get_sens_id(),
			 sensor.get_node_id());
	}
}
//---------------------------------------------------------------------------------------
bool UniSVGButton::on_button_event(GdkEvent *event)
{
	GdkEventButton event_btn = event->button;
	GdkEventCrossing event_cross = event->crossing;
//	GdkEventExpose event_expose = event->expose;
//	cout<<get_name()<<": event->type="<<event->type<<" on_push="<<on_push<< endl;

	if(event_cross.type == GDK_ENTER_NOTIFY)
	{
		in_focus = true;
		in_focus_fade = true;
		if(!fade_img_is_init && get_fade_state())
		{
			in_state_fade = false;
			fade_img_is_init = true;
		}
		opacity_draw();
		return false;
	}
	else if(
		( (event_btn.type == GDK_BUTTON_RELEASE && !property_fixed_button_.get_value()) ||
		  (event_btn.type == GDK_UNMAP && !property_fixed_button_.get_value())	||
		  (event_btn.type == GDK_LEAVE_NOTIFY && !property_fixed_button_.get_value())
		) &&
		on_push
	       )
	{
		on_push = false;
		in_push_fade = true;
		if(event_cross.type == GDK_LEAVE_NOTIFY)
		{
			in_focus = false;
			in_focus_fade = true;
		}
		if(!fade_img_is_init && get_fade_state())
		{
			in_state_fade = false;
			fade_img_is_init = true;
		}
		sensor.save_value(property_realize_value_.get_value());
		opacity_draw();
		return false;
	}
	else if(event_cross.type == GDK_LEAVE_NOTIFY)
	{
		in_focus = false;
		in_focus_fade = true;
		if(!fade_img_is_init && get_fade_state())
		{
			in_state_fade = false;
			fade_img_is_init = true;
		}
		opacity_draw();
		return false;
	}
	else if((event_btn.type == Gdk::BUTTON_PRESS && event_btn.button == 1))
	{
		on_push = !on_push;
		in_push_fade = true;
		if(!fade_img_is_init && get_fade_state())
		{
			in_state_fade = false;
			fade_img_is_init = true;
		}
//		if(sensor.get_sens_id()!= DefaultObjectId)
			sensor.save_value(on_push? property_push_value_.get_value(): property_realize_value_.get_value());
		opacity_draw();
		return false;
	}
	return false;
}
//---------------------------------------------------------------------------------------
void UniSVGButton::process_sensor(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value)
{
	USVGIndicatorPlus::process_sensor(id,node,value);
	if(id == sensor.get_sens_id() && node == sensor.get_node_id())
	{
		bool push = property_push_value_.get_value() == value? true:false;
		if(push != on_push)
		{
			if(property_fixed_button_)
				on_push = push;
			in_push_fade = true;
			opacity_draw();
		}
	}
}
//---------------------------------------------------------------------------------------
void UniSVGButton::state_changed()
{
	layout->set_text(get_property_state_()? (get_property_text_on_()==""? get_property_text_():get_property_text_on_()):get_property_text_());
	if(state != property_state_)
		state = property_state_;
	else
		return;
	in_state_fade = true;
	opacity_draw();
}
//---------------------------------------------------------------------------------------
void UniSVGButton::opacity_draw()
{
	if(get_fade_state())
	{
		if(!in_fade)
		{
			if(property_blink_time().get_value()<=0)
				opacity_step=255;
			else if(property_fade_state_time().get_value()<=0)
				opacity_step=255;
			else
				opacity_step = 255/(property_blink_time().get_value()/property_fade_state_time().get_value());
			if(opacity_step>255 || opacity_step<=0)
				opacity_step=255;

			opacity_value = 0;
			opacity_step_connector.disconnect();

			int time = (property_fade_state_time().get_value()>property_blink_time().get_value() || property_fade_state_time().get_value()<=0) ? property_blink_time().get_value():property_fade_state_time().get_value();
			opacity_step_connector = Glib::signal_timeout().connect(sigc::mem_fun(*this, &UniSVGButton::opacity_step_timer),time);
//			queue_draw();
		}
		else if(opacity_value>190)
			opacity_value = 255-opacity_value;
	}
	else
		queue_draw();
}
//---------------------------------------------------------------------------------------
bool UniSVGButton::opacity_step_timer()
{
	if(opacity_value<0 || opacity_value>255 || !get_fade_state() || !widget_is_realised)
	{
		in_fade = false;
		return false;
	}

//	cout<<get_name()<<"::opacity_step_timer fadeState="<<property_fade_state_.get_value()<<" opacity_value="<<opacity_value<<" step="<<opacity_step<<" property_state_="<<property_state_<<endl;

	if( (opacity_value + opacity_step)<=255 || opacity_value==255)
		opacity_value += opacity_step;
	else
		opacity_value = 255;

	Glib::RefPtr<Gdk::Pixbuf> pixbuf;// = mask_pixbuf->copy();
	string path;
	if(get_state())
	{
		if(on_push)
		{
			if(in_focus)
			{
				if(is_image1_push_focus_on_top())
				{
					if(in_state_fade && in_push_fade && in_focus_fade)
					{
						pixbuf = image0_ref_->copy();
						path = property_image0_path().get_value();
					}
					else if(in_state_fade && in_push_fade)
					{
						pixbuf = image0_focus_ref_->copy();
						path = image0_focus_path;
					}
					else if(in_state_fade && in_focus_fade)
					{
						pixbuf = image0_push_ref_->copy();
						path = image0_push_path;
					}
					else if(in_push_fade && in_focus_fade)
					{
						pixbuf = image1_ref_->copy();
						path = property_image1_path().get_value();
					}
					else if(in_state_fade)
					{
						pixbuf = image0_push_focus_ref_->copy();
						path = image0_push_focus_path;
					}
					else if(in_push_fade)
					{
						pixbuf = image1_focus_ref_->copy();
						path = image1_focus_path;
					}
					else if(in_focus_fade)
					{
						pixbuf = image1_push_ref_->copy();
						path = image1_push_path;
					}

					if(path != image1_push_focus_path)
						image1_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
					fade_image_ref_ = pixbuf->copy();
				}
				else
				{
					pixbuf = image1_push_focus_ref_->copy();
					path = image1_push_focus_path;
					if(in_state_fade && in_push_fade && in_focus_fade && path != property_image0_path().get_value())
						image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_push_fade && path != image0_focus_path)
						image0_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_focus_fade && path != image0_push_path)
						image0_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && in_focus_fade && path != property_image1_path().get_value())
						image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && path != image0_push_focus_path)
						image0_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && path != image1_focus_path)
						image1_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_focus_fade && path != image1_push_path)
						image1_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);

					fade_image_ref_ = pixbuf->copy();
				}
			}
			else
			{
				if(is_image1_push_on_top())
				{
					if(in_state_fade && in_push_fade && in_focus_fade)
					{
						pixbuf = image0_focus_ref_->copy();
						path = image0_focus_path;
					}
					else if(in_state_fade && in_push_fade)
					{
						pixbuf = image0_ref_->copy();
						path = property_image0_path().get_value();
					}
					else if(in_state_fade && in_focus_fade)
					{
						pixbuf = image0_push_focus_ref_->copy();
						path = image0_push_focus_path;
					}
					else if(in_push_fade && in_focus_fade)
					{
						pixbuf = image1_focus_ref_->copy();
						path = image1_focus_path;
					}
					else if(in_state_fade)
					{
						pixbuf = image0_push_ref_->copy();
						path = image0_push_path;
					}
					else if(in_push_fade)
					{
						pixbuf = image1_ref_->copy();
						path = property_image1_path().get_value();
					}
					else if(in_focus_fade)
					{
						pixbuf = image1_push_focus_ref_->copy();
						path = image1_push_focus_path;
					}

					if(path != image1_push_path)
						image1_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
					fade_image_ref_ = pixbuf->copy();
				}
				else
				{
					pixbuf = image1_push_ref_->copy();
					path = image1_push_path;
					if(in_state_fade && in_push_fade && in_focus_fade && path != image0_focus_path)
						image0_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_push_fade && path != property_image0_path().get_value())
						image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_focus_fade && path != image0_push_focus_path)
						image0_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && in_focus_fade && path != image1_focus_path)
						image1_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && path != image0_push_path)
						image0_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && path != property_image1_path().get_value())
						image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_focus_fade && path != image1_push_focus_path)
						image1_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);

					fade_image_ref_ = pixbuf->copy();
				}
			}
		}
		else
		{
			if(in_focus)
			{
				if(is_image1_focus_on_top())
				{
					if(in_state_fade && in_push_fade && in_focus_fade)
					{
						pixbuf = image0_push_ref_->copy();
						path = image0_push_path;
					}
					else if(in_state_fade && in_push_fade)
					{
						pixbuf = image0_push_focus_ref_->copy();
						path = image0_push_focus_path;
					}
					else if(in_state_fade && in_focus_fade)
					{
						pixbuf = image0_ref_->copy();
						path = property_image0_path().get_value();
					}
					else if(in_push_fade && in_focus_fade)
					{
						pixbuf = image1_push_ref_->copy();
						path = image1_push_path;
					}
					else if(in_state_fade)
					{
						pixbuf = image0_focus_ref_->copy();
						path = image0_focus_path;
					}
					else if(in_push_fade)
					{
						pixbuf = image1_push_focus_ref_->copy();
						path = image1_push_focus_path;
					}
					else if(in_focus_fade)
					{
						pixbuf = image1_ref_->copy();
						path = property_image1_path().get_value();
					}

					if(path != image1_focus_path)
						image1_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
					fade_image_ref_ = pixbuf->copy();
				}
				else
				{
					pixbuf = image1_focus_ref_->copy();
					path = image1_focus_path;
					if(in_state_fade && in_push_fade && in_focus_fade && path != image0_push_path)
						image0_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_push_fade && path != image0_push_focus_path)
						image0_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_focus_fade && path != property_image0_path().get_value())
						image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && in_focus_fade && path != image1_push_path)
						image1_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && path != image0_focus_path)
						image0_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && path != image1_push_focus_path)
						image1_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_focus_fade && path != property_image1_path().get_value())
						image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);

					fade_image_ref_ = pixbuf->copy();
				}
			}
			else
			{
				if(is_image1_on_top())
				{
					if(in_state_fade && in_push_fade && in_focus_fade)
					{
						pixbuf = image0_push_focus_ref_->copy();
						path = image0_push_focus_path;
					}
					else if(in_state_fade && in_push_fade)
					{
						pixbuf = image0_push_ref_->copy();
						path = image0_push_path;
					}
					else if(in_state_fade && in_focus_fade)
					{
						pixbuf = image0_focus_ref_->copy();
						path = image0_focus_path;
					}
					else if(in_push_fade && in_focus_fade)
					{
						pixbuf = image1_push_focus_ref_->copy();
						path = image1_push_focus_path;
					}
					else if(in_state_fade)
					{
						pixbuf = image0_ref_->copy();
						path = property_image0_path().get_value();
					}
					else if(in_push_fade)
					{
						pixbuf = image1_push_ref_->copy();
						path = image1_push_path;
					}
					else if(in_focus_fade)
					{
						pixbuf = image1_focus_ref_->copy();
						path = image1_focus_path;
					}

					if(path != property_image1_path().get_value())
						image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
					fade_image_ref_ = pixbuf->copy();
				}
				else
				{
					pixbuf = image1_ref_->copy();
					path = property_image1_path().get_value();
					if(in_state_fade && in_push_fade && in_focus_fade && path != image0_push_focus_path)
						image0_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_push_fade && path != image0_push_path)
						image0_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_focus_fade && path != image0_focus_path)
						image0_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && in_focus_fade && path != image1_push_focus_path)
						image1_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && path != property_image0_path().get_value())
						image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && path != image1_push_path)
						image1_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_focus_fade && path != image1_focus_path)
						image1_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);

					fade_image_ref_ = pixbuf->copy();
				}
			}
		}
	}
	else
	{
		if(on_push)
		{
			if(in_focus)
			{
				if(is_image0_push_focus_on_top())
				{
					if(in_state_fade && in_push_fade && in_focus_fade)
					{
						pixbuf = image1_ref_->copy();
						path = property_image1_path().get_value();
					}
					else if(in_state_fade && in_push_fade)
					{
						pixbuf = image1_focus_ref_->copy();
						path = image1_focus_path;
					}
					else if(in_state_fade && in_focus_fade)
					{
						pixbuf = image1_push_ref_->copy();
						path = image1_push_path;
					}
					else if(in_push_fade && in_focus_fade)
					{
						pixbuf = image0_ref_->copy();
						path = property_image0_path().get_value();
					}
					else if(in_state_fade)
					{
						pixbuf = image1_push_focus_ref_->copy();
						path = image1_push_focus_path;
					}
					else if(in_push_fade)
					{
						pixbuf = image0_focus_ref_->copy();
						path = image0_focus_path;
					}
					else if(in_focus_fade)
					{
						pixbuf = image0_push_ref_->copy();
						path = image0_push_path;
					}

					if(path != image0_push_focus_path)
						image0_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
					fade_image_ref_ = pixbuf->copy();
				}
				else
				{
					pixbuf = image0_push_focus_ref_->copy();
					path = image0_push_focus_path;
					if(in_state_fade && in_push_fade && in_focus_fade && path != property_image1_path().get_value())
						image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_push_fade && path != image1_focus_path)
						image1_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_focus_fade && path != image1_push_path)
						image1_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && in_focus_fade && path != property_image0_path().get_value())
						image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && path != image1_push_focus_path)
						image1_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && path != image0_focus_path)
						image0_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_focus_fade && path != image0_push_path)
						image0_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);

					fade_image_ref_ = pixbuf->copy();
				}
			}
			else
			{
				if(is_image0_push_on_top())
				{
					if(in_state_fade && in_push_fade && in_focus_fade)
					{
						pixbuf = image1_focus_ref_->copy();
						path = image1_focus_path;
					}
					else if(in_state_fade && in_push_fade)
					{
						pixbuf = image1_ref_->copy();
						path = property_image1_path().get_value();
					}
					else if(in_state_fade && in_focus_fade)
					{
						pixbuf = image1_push_focus_ref_->copy();
						path = image1_push_focus_path;
					}
					else if(in_push_fade && in_focus_fade)
					{
						pixbuf = image0_focus_ref_->copy();
						path = image0_focus_path;
					}
					else if(in_state_fade)
					{
						pixbuf = image1_push_ref_->copy();
						path = image1_push_path;
					}
					else if(in_push_fade)
					{
						pixbuf = image0_ref_->copy();
						path = property_image0_path().get_value();
					}
					else if(in_focus_fade)
					{
						pixbuf = image0_push_focus_ref_->copy();
						path = image0_push_focus_path;
					}

					if(path != image0_push_path)
						image0_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
					fade_image_ref_ = pixbuf->copy();
				}
				else
				{
					pixbuf = image0_push_ref_->copy();
					path = image0_push_path;
					if(in_state_fade && in_push_fade && in_focus_fade && path != image1_focus_path)
						image1_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_push_fade && path != property_image1_path().get_value())
						image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_focus_fade && path != image1_push_focus_path)
						image1_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && in_focus_fade && path != image0_focus_path)
						image0_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && path != image1_push_path)
						image1_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && path != property_image0_path().get_value())
						image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_focus_fade && path != image0_push_focus_path)
						image0_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);

					fade_image_ref_ = pixbuf->copy();
				}
			}
		}
		else
		{
			if(in_focus)
			{
				if(is_image0_focus_on_top())
				{
					if(in_state_fade && in_push_fade && in_focus_fade)
					{
						pixbuf = image1_push_ref_->copy();
						path = image1_push_path;
					}
					else if(in_state_fade && in_push_fade)
					{
						pixbuf = image1_push_focus_ref_->copy();
						path = image1_push_focus_path;
					}
					else if(in_state_fade && in_focus_fade)
					{
						pixbuf = image1_ref_->copy();
						path = property_image1_path().get_value();
					}
					else if(in_push_fade && in_focus_fade)
					{
						pixbuf = image0_push_ref_->copy();
						path = image0_push_path;
					}
					else if(in_state_fade)
					{
						pixbuf = image1_focus_ref_->copy();
						path = image1_focus_path;
					}
					else if(in_push_fade)
					{
						pixbuf = image0_push_focus_ref_->copy();
						path = image0_push_focus_path;
					}
					else if(in_focus_fade)
					{
						pixbuf = image0_ref_->copy();
						path = property_image0_path().get_value();
					}

					if(path != image0_focus_path)
						image0_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
					fade_image_ref_ = pixbuf->copy();
				}
				else
				{
					pixbuf = image0_focus_ref_->copy();
					path = image0_focus_path;
					if(in_state_fade && in_push_fade && in_focus_fade && path != image1_push_path)
						image1_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_push_fade && path != image1_push_focus_path)
						image1_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_focus_fade && path != property_image1_path().get_value())
						image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && in_focus_fade && path != image0_push_path)
						image0_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && path != image1_focus_path)
						image1_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && path != image0_push_focus_path)
						image0_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_focus_fade && path != property_image0_path().get_value())
						image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);

					fade_image_ref_ = pixbuf->copy();
				}
			}
			else
			{
				if(is_image0_on_top())
				{
					if(in_state_fade && in_push_fade && in_focus_fade)
					{
						pixbuf = image1_push_focus_ref_->copy();
						path = image1_push_focus_path;
					}
					else if(in_state_fade && in_push_fade)
					{
						pixbuf = image1_push_ref_->copy();
						path = image1_push_path;
					}
					else if(in_state_fade && in_focus_fade)
					{
						pixbuf = image1_focus_ref_->copy();
						path = image1_focus_path;
					}
					else if(in_push_fade && in_focus_fade)
					{
						pixbuf = image0_push_focus_ref_->copy();
						path = image0_push_focus_path;
					}
					else if(in_state_fade)
					{
						pixbuf = image1_ref_->copy();
						path = property_image1_path().get_value();
					}
					else if(in_push_fade)
					{
						pixbuf = image0_push_ref_->copy();
						path = image0_push_path;
					}
					else if(in_focus_fade)
					{
						pixbuf = image0_focus_ref_->copy();
						path = image0_focus_path;
					}

					if(path != property_image0_path().get_value())
						image0_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,opacity_value);
					fade_image_ref_ = pixbuf->copy();
				}
				else
				{
					pixbuf = image0_ref_->copy();
					path = property_image0_path().get_value();
					if(in_state_fade && in_push_fade && in_focus_fade && path != image1_push_focus_path)
						image1_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_push_fade && path != image1_push_path)
						image1_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && in_focus_fade && path != image1_focus_path)
						image1_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && in_focus_fade && path != image0_push_focus_path)
						image0_push_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_state_fade && path != property_image1_path().get_value())
						image1_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_push_fade && path != image0_push_path)
						image0_push_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);
					else if(in_focus_fade && path != image0_focus_path)
						image0_focus_ref_->composite(pixbuf, 0, 0, width, height, 0, 0, 1, 1, Gdk::INTERP_NEAREST,255-opacity_value);

					fade_image_ref_ = pixbuf->copy();
				}
			}
		}
	}
	in_fade = true;
	queue_draw();

	if(opacity_value>=255)
	{
		in_fade = false;
		in_focus_fade = false;
		in_push_fade = false;
		in_state_fade = false;
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------------------
void UniSVGButton::image_path_check(bool force)
{
	Glib::ustring	imagefile = property_image0_push_path_.get_value();
	bool image0_push_exist = !((!imagefile.empty() && !UniWidgetsTypes::file_exist(imagefile)) || imagefile.empty());
	image0_push_path = ((!imagefile.empty() && !UniWidgetsTypes::file_exist(imagefile)) || imagefile.empty()) ? property_image0_path().get_value() : imagefile;

	imagefile = property_image1_push_path_.get_value();
	bool image1_push_exist = !((!imagefile.empty() && !UniWidgetsTypes::file_exist(imagefile)) || imagefile.empty());
	image1_push_path = ((!imagefile.empty() && !UniWidgetsTypes::file_exist(imagefile)) || imagefile.empty()) ? property_image1_path().get_value() : imagefile;

	imagefile = property_image0_focus_path_.get_value();
	image0_focus_path = ((!imagefile.empty() && !UniWidgetsTypes::file_exist(imagefile)) || imagefile.empty()) ? property_image0_path().get_value() : imagefile;

	imagefile = property_image1_focus_path_.get_value();
	image1_focus_path = ((!imagefile.empty() && !UniWidgetsTypes::file_exist(imagefile)) || imagefile.empty()) ? property_image1_path().get_value() : imagefile;

	imagefile = property_image0_push_focus_path_.get_value();
	image0_push_focus_path = ((!imagefile.empty() && !UniWidgetsTypes::file_exist(imagefile)) || imagefile.empty()) ? (image0_push_exist ? image0_push_path : image0_focus_path) : imagefile;

	imagefile = property_image1_push_focus_path_.get_value();
	image1_push_focus_path = ((!imagefile.empty() && !UniWidgetsTypes::file_exist(imagefile)) || imagefile.empty()) ? (image1_push_exist ? image1_push_path : image1_focus_path) : imagefile;

	Gtk::Allocation alloc = get_allocation();
	image_pixbuf_check(alloc,force);
}
//---------------------------------------------------------------------------------------
void UniSVGButton::image_pixbuf_check(Gtk::Allocation& alloc, bool force)
{
	if(height == alloc.get_height() && width == alloc.get_width() && widget_is_realised && !force)
		return;
//	if(!force)
//	{
		height = alloc.get_height();
		width = alloc.get_width();
//	}
	image0_ref_ = get_pixbuf_from_cache(property_image0_path().get_value(), width, height);

	if(property_image0_path().get_value() == property_image1_path().get_value())
		image1_ref_ = image0_ref_->copy();
	else
		image1_ref_ = get_pixbuf_from_cache(property_image1_path().get_value(), width, height);

	if(image0_push_path == property_image0_path().get_value())
		image0_push_ref_ = image0_ref_->copy();
	else if(image0_push_path == property_image1_path().get_value())
		image0_push_ref_ = image1_ref_->copy();
	else
		image0_push_ref_ = get_pixbuf_from_cache(image0_push_path, width, height);

	if(image1_push_path == property_image0_path().get_value())
		image1_push_ref_ = image0_ref_->copy();
	else if(image1_push_path == property_image1_path().get_value())
		image1_push_ref_ = image1_ref_->copy();
	else if(image1_push_path == image0_push_path)
		image1_push_ref_ = image0_push_ref_->copy();
	else
		image1_push_ref_ = get_pixbuf_from_cache(image1_push_path, width, height);

	if(image0_focus_path == property_image0_path().get_value())
		image0_focus_ref_ = image0_ref_->copy();
	else if(image0_focus_path == property_image1_path().get_value())
		image0_focus_ref_ = image1_ref_->copy();
	else if(image0_focus_path == image0_push_path)
		image0_focus_ref_ = image0_push_ref_->copy();
	else if(image0_focus_path == image1_push_path)
		image0_focus_ref_ = image1_push_ref_->copy();
	else
		image0_focus_ref_ = get_pixbuf_from_cache(image0_focus_path, width, height);

	if(image1_focus_path == property_image0_path().get_value())
		image1_focus_ref_ = image0_ref_->copy();
	else if(image1_focus_path == property_image1_path().get_value())
		image1_focus_ref_ = image1_ref_->copy();
	else if(image1_focus_path == image0_push_path)
		image1_focus_ref_ = image0_push_ref_->copy();
	else if(image1_focus_path == image1_push_path)
		image1_focus_ref_ = image1_push_ref_->copy();
	else if(image1_focus_path == image0_focus_path)
		image1_focus_ref_ = image0_focus_ref_->copy();
	else
		image1_focus_ref_ = get_pixbuf_from_cache(image1_focus_path, width, height);

	if(image0_push_focus_path == property_image0_path().get_value())
		image0_push_focus_ref_ = image0_ref_->copy();
	else if(image0_push_focus_path == property_image1_path().get_value())
		image0_push_focus_ref_ = image1_ref_->copy();
	else if(image0_push_focus_path == image0_push_path)
		image0_push_focus_ref_ = image0_push_ref_->copy();
	else if(image0_push_focus_path == image1_push_path)
		image0_push_focus_ref_ = image1_push_ref_->copy();
	else if(image0_push_focus_path == image0_focus_path)
		image0_push_focus_ref_ = image0_focus_ref_->copy();
	else if(image0_push_focus_path == image1_focus_path)
		image0_push_focus_ref_ = image1_focus_ref_->copy();
	else
		image0_push_focus_ref_ = get_pixbuf_from_cache(image0_push_focus_path, width, height);

	if(image1_push_focus_path == property_image0_path().get_value())
		image1_push_focus_ref_ = image0_ref_->copy();
	else if(image1_push_focus_path == property_image1_path().get_value())
		image1_push_focus_ref_ = image1_ref_->copy();
	else if(image1_push_focus_path == image0_push_path)
		image1_push_focus_ref_ = image0_push_ref_->copy();
	else if(image1_push_focus_path == image1_push_path)
		image1_push_focus_ref_ = image1_push_ref_->copy();
	else if(image1_push_focus_path == image0_focus_path)
		image1_push_focus_ref_ = image0_focus_ref_->copy();
	else if(image1_push_focus_path == image1_focus_path)
		image1_push_focus_ref_ = image1_focus_ref_->copy();
	else if(image1_push_focus_path == image0_push_focus_path)
		image1_push_focus_ref_ = image0_push_focus_ref_->copy();
	else
		image1_push_focus_ref_ = get_pixbuf_from_cache(image1_push_focus_path, width, height);

	mask_pixbuf = get_pixbuf_from_cache("", width, height);
	init_fade_img();
	widget_is_realised = true;
}
//---------------------------------------------------------------------------------------
void UniSVGButton::init_fade_img()
{
//	cout<<get_name()<<"::init_fade_img get_state()="<<get_state()<<" in_focus="<<in_focus<<" on_push="<<on_push<<endl;
	fade_image_ref_ = (get_state() ?
				(in_focus ?
					(on_push ? image1_push_focus_ref_ : image1_focus_ref_):
				(on_push ? image1_push_ref_ : image1_ref_)
				):
				(in_focus ?
					(on_push ? image0_push_focus_ref_ : image0_focus_ref_):
				(on_push ? image0_push_ref_ : image0_ref_)
				)
			  )->copy();
}
//---------------------------------------------------------------------------------------
void UniSVGButton::text_changed()
{
	layout->set_text(get_state()? (property_text_on_==""? property_text_:property_text_on_):property_text_);
//	int w,h;
//	layout->get_pixel_size(w,h);
//	set_size_request(w,h);
	queue_draw();
}
//---------------------------------------------------------------------------------------
void UniSVGButton::font_changed()
{
	Pango::FontDescription fd(property_text_font_name_);
	if ( property_text_abs_font_size_ > 0)
		fd.set_absolute_size( property_text_abs_font_size_ * Pango::SCALE );
	layout->set_font_description(fd);
	queue_resize();
}
//---------------------------------------------------------------------------------------
void UniSVGButton::alignment_changed()
{
	layout->set_alignment( property_alignment_);
	if(property_alignment_ == Pango::ALIGN_CENTER)
		alignment = CENTER;
	else if(property_alignment_ == Pango::ALIGN_LEFT)
		alignment = LEFT;
	else if(property_alignment_ == Pango::ALIGN_RIGHT)
		alignment = RIGHT;

	queue_resize();
}
