#ifndef _UniSVGButton_H
#define _UniSVGButton_H

#include <UDefaultFunctions.h>
#include <SensorProp.h>
#include <USignals.h>
#include <usvgwidgets/USVGIndicatorPlus.h>
#include <global_macros.h>
static int mynum = 0;

class UniSVGButton : public USVGIndicatorPlus
{
public:
	UniSVGButton();
	explicit UniSVGButton(GtkmmBaseType::BaseObjectType* gobject);
	virtual ~UniSVGButton();

	inline bool get_on_push(){return on_push;};
	inline void set_on_push(bool value){on_push=value; queue_draw();};
	virtual void set_connector(const ConnectorRef& connector) throw();

	bool is_image0_push_on_top() const;
	bool is_image1_push_on_top() const;
	bool is_image0_focus_on_top() const;
	bool is_image1_focus_on_top() const;
	bool is_image0_push_focus_on_top() const;
	bool is_image1_push_focus_on_top() const;

protected:

	virtual void on_value_changed(std::string prop);
	virtual void state_changed();
	/* Handlers */
	virtual bool on_expose_event(GdkEventExpose* event);
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void on_realize();
	virtual bool on_button_event(GdkEvent *event);

	virtual void opacity_draw();
	virtual bool opacity_step_timer();
	virtual void init_fade_img();
	virtual void image_path_check(bool force=true);
	virtual void image_pixbuf_check(Gtk::Allocation& alloc,bool force=false);
	void text_changed();
	void font_changed();
	void alignment_changed();

	Glib::RefPtr<Gdk::Pixbuf> image0_ref_;
	Glib::RefPtr<Gdk::Pixbuf> image1_ref_;
	Glib::RefPtr<Gdk::Pixbuf> image0_push_ref_;
	Glib::RefPtr<Gdk::Pixbuf> image1_push_ref_;
	Glib::RefPtr<Gdk::Pixbuf> image0_focus_ref_;
	Glib::RefPtr<Gdk::Pixbuf> image1_focus_ref_;
	Glib::RefPtr<Gdk::Pixbuf> image0_push_focus_ref_;
	Glib::RefPtr<Gdk::Pixbuf> image1_push_focus_ref_;
	Glib::RefPtr<Gdk::Pixbuf> fade_image_ref_;
	Glib::RefPtr<Gdk::Pixbuf> mask_pixbuf;

	bool	on_push,in_focus,
 		in_push_fade,in_focus_fade,in_state_fade,in_fade;
	bool	fade_img_is_init;
	Glib::ustring	image0_push_path
			,image1_push_path
			,image0_focus_path
			,image1_focus_path
			,image0_push_focus_path
			,image1_push_focus_path;

	SensorProp sensor;
	Glib::RefPtr<Pango::Layout> layout;

	ADD_PROPERTY(property_push_value_, long)
	ADD_PROPERTY(property_realize_value_, long)
	ADD_PROPERTY(property_init_sensor_by_realize_value_, bool)
	ADD_PROPERTY(property_fixed_button_, bool)
	ADD_PROPERTY(property_image0_push_path_, Glib::ustring)
	ADD_PROPERTY(property_image0_push_on_top_, bool)
	ADD_PROPERTY(property_image1_push_path_, Glib::ustring)
	ADD_PROPERTY(property_image1_push_on_top_, bool)
	ADD_PROPERTY(property_image0_focus_path_, Glib::ustring)
	ADD_PROPERTY(property_image0_focus_on_top_, bool)
	ADD_PROPERTY(property_image1_focus_path_, Glib::ustring)
	ADD_PROPERTY(property_image1_focus_on_top_, bool)
	ADD_PROPERTY(property_image0_push_focus_path_, Glib::ustring)
	ADD_PROPERTY(property_image0_push_focus_on_top_, bool)
	ADD_PROPERTY(property_image1_push_focus_path_, Glib::ustring)
	ADD_PROPERTY(property_image1_push_focus_on_top_, bool)

	enum text_alignment{LEFT=1,CENTER=2,RIGHT=3};
	int alignment;
	ADD_PROPERTY(property_text_, Glib::ustring)
	ADD_PROPERTY(property_text_on_, Glib::ustring)
	ADD_PROPERTY(property_text_font_name_, Glib::ustring)
	ADD_PROPERTY(property_text_abs_font_size_, int)
	ADD_PROPERTY(property_text_off_color_, Gdk::Color)
	ADD_PROPERTY(property_text_on_color_, Gdk::Color)
	ADD_PROPERTY(property_alignment_, Pango::Alignment)
	ADD_PROPERTY(property_drop_shadow_, bool)

private:
	void constructor();
	void configure();
	void process_sensor(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value);

	USignals::Connection sensorPropConnection;

	DISALLOW_COPY_AND_ASSIGN(UniSVGButton);
};

inline bool UniSVGButton::is_image0_push_on_top() const
{
	return property_image0_push_on_top_.get_value();
}

inline bool UniSVGButton::is_image1_push_on_top() const
{
	return property_image1_push_on_top_.get_value();
}

inline bool UniSVGButton::is_image0_focus_on_top() const
{
	return property_image0_focus_on_top_.get_value();
}

inline bool UniSVGButton::is_image1_focus_on_top() const
{
	return property_image1_focus_on_top_.get_value();
}

inline bool UniSVGButton::is_image0_push_focus_on_top() const
{
	return property_image0_push_focus_on_top_.get_value();
}

inline bool UniSVGButton::is_image1_push_focus_on_top() const
{
	return property_image1_push_focus_on_top_.get_value();
}

#endif // _UniSVGButton_H
