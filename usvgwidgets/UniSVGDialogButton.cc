#include "UniSVGDialogButton.h"
//#include <guiconfiguration.h>
//#include <sstream>
//#include <fstream>

using namespace std;
using namespace UniWidgetsTypes;

#define INIT_UniSVGDialogButton_PROPERTIES() \
	on_dialog_run(false) \
	,property_dialog_xml_path_(*this, "glade-xml-path", "./gui.glade" ) \
	,property_dialog_name_(*this, "dialog-window-name", "" ) \
	,property_dialog_pos_(*this, "dialog-position", CENTER ) \
	,property_dialog_pos_from_widget(*this, "dialog-castom-position-from-widget", nullptr ) \
	,property_dialog_pos_x(*this, "dialog-castom-position-x", 0 ) \
	,property_dialog_pos_y(*this, "dialog-castom-position-y", 0 ) \
	,property_dialog_focus_out_hide_(*this, "dialog-focus-out-hide", true ) \
	,property_exit_button_name_(*this, "exit-button-name", "" ) \
	,own_xml_path("") \
	,own_parent_window_name("") \
	,own_parent_window(NULL) \
	,dialog_pos_from_widget_name("") \
	,block_hide_dialog_by_out_focus("") \
	
void UniSVGDialogButton::constructor()
{
	dialog = 0;
	exit_button = 0;

//	gxml = Gnome::Glade::Xml::create("");
//	signal_event().connect(sigc::mem_fun(*this, &UniSVGDialogButton::on_button_event));
	
	set_property_auto_connect(true);

	connect_property_changed("glade-xml-path", sigc::mem_fun(*this, &UniSVGDialogButton::xml_path_changed));
	connect_property_changed("dialog-window-name", sigc::mem_fun(*this, &UniSVGDialogButton::dialog_name_changed));
//	connect_property_changed("dialog-position", sigc::mem_fun(*this, &UniSVGDialogButton::dialog_pos_changed));
// 	connect_property_changed("dialog-castom-position-x", sigc::mem_fun(*this, &UniSVGDialogButton::dialog_change_castom_position));
// 	connect_property_changed("dialog-castom-position-y", sigc::mem_fun(*this, &UniSVGDialogButton::dialog_change_castom_position));
	connect_property_changed("exit-button-name", sigc::mem_fun(*this, &UniSVGDialogButton::exit_button_name_changed));
}
//---------------------------------------------------------------------------------------
UniSVGDialogButton::UniSVGDialogButton() :
	Glib::ObjectBase("unisvgdialogbutton")
	,UniSVGButton()
	,INIT_UniSVGDialogButton_PROPERTIES()
{
	constructor();
}
//---------------------------------------------------------------------------------------
UniSVGDialogButton::UniSVGDialogButton(GtkmmBaseType::BaseObjectType* gobject) :
	UniSVGButton(gobject)
	,INIT_UniSVGDialogButton_PROPERTIES()
{
	constructor();
}
//---------------------------------------------------------------------------------------
UniSVGDialogButton::~UniSVGDialogButton()
{
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::configure()
{
	dialog = 0;
	exit_button = 0;
//	gxml = Gnome::Glade::Xml::create("");
	xml_path_changed();
//	dialog_pos_changed();
}
//---------------------------------------------------------------------------------------
GType postype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { UniSVGDialogButton::CENTER, "CENTER", "В центре" },{ UniSVGDialogButton::CENTER_OF_PARENT, "CENTER_OF_PARENT", "В центре родительского окна" },{ UniSVGDialogButton::MOUSE, "MOUSE", "В положении указателя мыши" },{ UniSVGDialogButton::TOP_LEFT, "TOP_LEFT", "В верхнем-левом углу" },{ UniSVGDialogButton::CASTOM_FROM_TOP_WINDOW, "CASTOM_FROM_TOP_WINDOW", "В заданной позиции от верхнего окна" },{ UniSVGDialogButton::CASTOM_FROM_PARENT, "CASTOM_FROM_PARENT", "В заданной позиции от родителя" },{ UniSVGDialogButton::CASTOM_FROM_CASTOM_WIDGET, "CASTOM_FROM_CASTOM_WIDGET", "В заданной позиции от выбраного виджета" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("DialogPositionType"), values);
	}
	return etype;
}
//---------------------------------------------------------------------------------------
// static
GType Glib::Value<UniSVGDialogButton::DialogPositionType>::value_type()
{
	return postype_get_type();
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::on_connect() throw()
{
	UniSVGButton::on_connect();
	configure();
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::on_disconnect() throw()
{
	UniSVGButton::on_disconnect();
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::on_realize()
{
	UniSVGButton::on_realize();
	configure();
}
//---------------------------------------------------------------------------------------
bool UniSVGDialogButton::on_expose_event(GdkEventExpose* event)
{
	return UniSVGButton::on_expose_event(event);
}
//---------------------------------------------------------------------------------------
bool UniSVGDialogButton::on_button_event(GdkEvent *event)
{
	UniSVGButton::on_button_event(event);
	GdkEventButton event_btn = event->button;
//	cout<<": event->type="<<event->type<< endl;

	if((event_btn.type == Gdk::BUTTON_PRESS && event_btn.button == 1))
	{
		if(on_dialog_run && dialog)
			dialog->hide();

		dialog_change_castom_position();
		on_dialog_run = true;
//		cout<<get_name()<<"::run dialog="<<dialog<<endl;
		if(dialog)
			dialog->show();
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------------------
bool UniSVGDialogButton::on_exit_button_event(GdkEvent *event)
{
	GdkEventButton event_btn = event->button;
//	cout<<": event->type="<<event->type<< endl;

	if(event_btn.type == Gdk::BUTTON_PRESS)
	{
		on_dialog_run = false;
//		cout<<get_name()<<""::exit dialog="<<dialog<<endl;
		if(dialog)
			dialog->hide();
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------------------
bool UniSVGDialogButton::on_dialog_focus_out_event(GdkEventFocus *event)
{
	if(block_hide_dialog_by_out_focus)
		return false;
	
	if(dialog && property_dialog_focus_out_hide_.get_value())
	{
		int p_x,p_y;
		Gdk::ModifierType mtype;
		dialog->get_root_window()->get_pointer(p_x,p_y,mtype);

		int dialog_p_x,dialog_p_y;
		dialog->get_window()->get_root_origin(dialog_p_x,dialog_p_y);
		int dialog_width = dialog->get_allocation().get_width();
		int dialog_height = dialog->get_allocation().get_height();
		if(p_x<dialog_p_x || p_x>dialog_p_x+dialog_width || p_y<dialog_p_y || p_y>dialog_p_y+dialog_height)
			dialog->hide();
	}
	return false;
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::exit_button_name_changed()
{
	if(gxml)
	{
		gxml->get_widget(property_exit_button_name_.get_value(), exit_button);
//		cout<<get_name()<<"::exit_button_name_changed Popitka zagruzit exit_button="<<exit_button<<endl;
	}
	if(exit_button)
		exit_button->signal_event().connect(sigc::mem_fun(*this, &UniSVGDialogButton::on_exit_button_event));

}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::dialog_name_changed()
{
	load_xml();
	if(gxml)
	{
		gxml->get_widget(property_dialog_name_.get_value(), dialog);
//		cout<<get_name()<<"::dialog_name_changed Popitka zagruzit dialog="<<dialog<<" property_dialog_name_="<<property_dialog_name_.get_value()<<endl;
	}
	if(dialog)
	{
		dialog->set_decorated(false);
//		dialog->set_modal(true);
		dialog_pos_changed();
		dialog->signal_focus_out_event().connect(sigc::mem_fun(*this, &UniSVGDialogButton::on_dialog_focus_out_event));
//		dialog_connect();
//		dialog->signal_realize().connect(sigc::mem_fun(*this, &UniSVGDialogButton::dialog_connect));
		set_connect_to_add_signal(dialog);
	}
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::set_connect_to_add_signal(Gtk::Container* w)
{
	w->signal_add().connect(sigc::mem_fun(*this, &UniSVGDialogButton::on_dialog_set_connector_to_hierarchy));

	Glib::ListHandle<Gtk::Widget*> childlist = w->get_children();
	for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); it++)
		on_dialog_set_connector_to_hierarchy(*it);
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::on_dialog_set_connector_to_hierarchy(Gtk::Widget* w)
{
	UVoid* uv = dynamic_cast<UVoid*>(w);
	if (uv)
		uv->set_connector(get_connector());

	Gtk::Container* cont = dynamic_cast<Gtk::Container*>(w);
	if (cont)
	{
		cont->signal_add().connect(sigc::mem_fun(*this, &UniSVGDialogButton::on_dialog_set_connector_to_hierarchy));
		set_connect_to_add_signal(cont);
	}
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::dialog_pos_changed()
{
	if(dialog)
	{
		if(get_property_dialog_pos_() == CENTER)
			dialog->set_position(Gtk::WIN_POS_CENTER);
		else if(get_property_dialog_pos_() == CENTER_OF_PARENT)
			dialog->set_position(Gtk::WIN_POS_CENTER_ON_PARENT);
		else if(get_property_dialog_pos_() == MOUSE)
			dialog->set_position(Gtk::WIN_POS_MOUSE);
		else if(get_property_dialog_pos_() == TOP_LEFT)
			dialog->set_position(Gtk::WIN_POS_NONE);
		else if(get_property_dialog_pos_() == CASTOM_FROM_TOP_WINDOW || get_property_dialog_pos_() == CASTOM_FROM_PARENT || get_property_dialog_pos_() == CASTOM_FROM_CASTOM_WIDGET)
		{
			dialog->set_position(Gtk::WIN_POS_NONE);
			dialog_change_castom_position();
		}
	}
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::dialog_change_castom_position()
{
	if(dialog && (get_property_dialog_pos_() == CASTOM_FROM_TOP_WINDOW || get_property_dialog_pos_() == CASTOM_FROM_PARENT || get_property_dialog_pos_() == CASTOM_FROM_CASTOM_WIDGET))
	{
		int root_x,root_y;
		if(get_toplevel() && get_property_dialog_pos_() == CASTOM_FROM_TOP_WINDOW)
		{
			if(get_toplevel()->get_window())
				get_toplevel()->get_window()->get_position(root_x,root_y);
		}
		if(get_parent() && get_property_dialog_pos_() == CASTOM_FROM_PARENT)
		{
			if(get_parent()->get_window())
				get_parent()->get_window()->get_position(root_x,root_y);
			if(!get_parent()->get_has_window())
			{
				root_x += get_parent()->get_allocation().get_x();
				root_y += get_parent()->get_allocation().get_y();
			}
//			cout<< get_name() << "::dialog_change_castom_position() w_name["<<get_parent()->get_name()<<"] root_x="<<root_x<<" root_y="<<root_y<<endl;
		}
		if(get_property_dialog_pos_from_widget() && get_property_dialog_pos_() == CASTOM_FROM_CASTOM_WIDGET)
		{
			if(get_property_dialog_pos_from_widget()->get_window())
				get_property_dialog_pos_from_widget()->get_window()->get_position(root_x,root_y);
			if(!get_property_dialog_pos_from_widget()->get_has_window())
			{
				root_x += get_property_dialog_pos_from_widget()->get_allocation().get_x();
				root_y += get_property_dialog_pos_from_widget()->get_allocation().get_y();
			}
//			cout<< get_name() << "::dialog_change_castom_position() w_name["<<get_property_dialog_pos_from_widget()->get_name()<<"] root_x="<<root_x<<" root_y="<<root_y<<endl;
		}
//		cout<< get_name() << "::dialog_change_castom_position() win_name["<<get_toplevel()->get_name()<<"] root_x="<<root_x<<" root_y="<<root_y<<endl;
		dialog->move(root_x+get_property_dialog_pos_x(),root_y+get_property_dialog_pos_y());
	}
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::init_dialog_pos_from_widget(Gtk::Widget* widget)
{
//	cout<<get_name()<<"::init_dialog_pos_from_widget widget_name="<<widget->get_name()<<endl;
//	cout<< get_name() << "::init_dialog_pos_from_widget() widget="<<widget<<endl;
	if(widget)
	{
		cout<<get_name()<<"::init_dialog_pos_from_widget widget_name="<<widget->get_name()<<" dialog_pos_from_widget_name="<<dialog_pos_from_widget_name<<endl;
		Gtk::Container* conteiner = dynamic_cast<Gtk::Container*>(widget);
		if(conteiner)
			conteiner->signal_add().connect(sigc::mem_fun(*this, &UniSVGDialogButton::init_dialog_pos_from_widget));
		
		if(!get_property_dialog_pos_from_widget() && !dialog_pos_from_widget_name.empty() && dialog_pos_from_widget_name == widget->get_name())
		{
			set_property_dialog_pos_from_widget(widget);
		}
	}
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::dialog_connect()
{
	cout<<get_name()<<" Connect to proxy... to connector="<<get_connector()<<endl;
	set_connector_to_hierarchy(dialog,get_connector());
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::xml_path_changed()
{
	load_xml();
	dialog_name_changed();
	exit_button_name_changed();
}
//---------------------------------------------------------------------------------------
void UniSVGDialogButton::load_xml()
{
	cout<<get_name()<<" Popitka otkrit` konfiguracionniyi fayil '"<<property_dialog_xml_path_.get_value()<<"'"<<endl;
	if( !property_dialog_xml_path_.get_value().empty() && UniWidgetsTypes::file_exist(property_dialog_xml_path_.get_value()) )
	{
		string full_path = property_dialog_xml_path_.get_value();
		full_path = realpath(full_path.c_str(),NULL);
		cout<<get_name()<<" Popitka zagruzit` konfiguracionniyi fayil '"<<full_path<<"' from node '"<<get_property_dialog_name_()<<"'"<<endl;
		if(is_glade_editor)
		{
			if(own_xml_path == full_path && (get_property_dialog_name_().empty() || own_parent_window->get_name() == get_property_dialog_name_()))
			{
				cout<<get_name()<<" Ne udalos` zagruzit` konfiguracionniyi fayil '"<<property_dialog_xml_path_.get_value()<<"'"<<endl;
				cout<<get_name()<<" Path to load xml == own_xml_path ("<<own_xml_path<<") && load dialog name == own_parent_window_name or empty ("<<get_property_dialog_name_()<<")"<<endl;
			}
			else if(own_xml_path == full_path)
			{
				gxml = Gnome::Glade::Xml::get_widget_tree(*static_cast<Gtk::Widget*>(this));
			}
			else
			{
				try
				{
					gxml = Gnome::Glade::Xml::create(property_dialog_xml_path_.get_value(), get_property_dialog_name_());
				}
				catch(...)
				{
					cout<<get_name()<<" Ne udalos` otkrit` konfiguracionniyi fayil '"<<property_dialog_xml_path_.get_value()<<"'"<<endl;
				}
			}
		}
		else
		{
			if(own_xml_path == full_path && (get_property_dialog_name_().empty() || own_parent_window_name == get_property_dialog_name_()))
			{
				cout<<get_name()<<" Ne udalos` zagruzit` konfiguracionniyi fayil '"<<property_dialog_xml_path_.get_value()<<"'"<<endl;
				cout<<get_name()<<" Path to load xml == own_xml_path ("<<own_xml_path<<") && load dialog name == own_parent_window_name or empty ("<<get_property_dialog_name_()<<")"<<endl;
			}
			else
			{
				try
				{
					gxml = Gnome::Glade::Xml::create(property_dialog_xml_path_.get_value(), get_property_dialog_name_());
				}
				catch(...)
				{
					cout<<get_name()<<" Ne udalos` otkrit` konfiguracionniyi fayil '"<<property_dialog_xml_path_.get_value()<<"'"<<endl;
				}
			}
		}
	}
}
//---------------------------------------------------------------------------------------
