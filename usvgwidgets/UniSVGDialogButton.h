#ifndef _UniSVGDialogButton_H
#define _UniSVGDialogButton_H

#include <libglademm.h>
#include <UDefaultFunctions.h>
#include <SensorProp.h>
#include <USignals.h>
#include <usvgwidgets/UniSVGButton.h>
#include <global_macros.h>

class UniSVGDialogButton : public UniSVGButton
{
public:
	UniSVGDialogButton();
	explicit UniSVGDialogButton(GtkmmBaseType::BaseObjectType* gobject);
	virtual ~UniSVGDialogButton();

//	virtual void set_connector(const ConnectorRef& connector) throw();

//	inline void set_dialog(Gtk::Dialog* d){ dialog = d; }
//	inline Gtk::Dialog* get_dialog(){ return dialog; }
	enum DialogPositionType
	{
		CENTER = 0
		,CENTER_OF_PARENT
		,MOUSE
		,TOP_LEFT
		,CASTOM_FROM_TOP_WINDOW
		,CASTOM_FROM_PARENT
		,CASTOM_FROM_CASTOM_WIDGET
	};
	
	inline void set_own_xml_path(std::string path){ own_xml_path = path;};
	inline void set_own_parent_window(Gtk::Window* win){ own_parent_window = win;};
	inline void set_own_parent_window_name(std::string name){ own_parent_window_name = name;};
	inline void set_dialog_pos_from_widget_name(Glib::ustring name) { dialog_pos_from_widget_name = name; }
	inline Gtk::Window* get_dialog() { return dialog; }
	void init_dialog_pos_from_widget(Gtk::Widget* widget);
	inline void set_block_hide_dialog_by_out_focus(bool flag) { block_hide_dialog_by_out_focus = flag; }
	
protected:

	/* Handlers */
	virtual bool on_expose_event(GdkEventExpose* event);
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void on_realize();
	virtual bool on_button_event(GdkEvent *event);
	virtual bool on_exit_button_event(GdkEvent *event);
	virtual bool on_dialog_focus_out_event(GdkEventFocus *event);
	virtual void on_dialog_set_connector_to_hierarchy(Gtk::Widget* w);

	void load_xml();
	void xml_path_changed();
	void dialog_name_changed();
	void dialog_pos_changed();
	void dialog_change_castom_position();
	void exit_button_name_changed();

	std::string own_xml_path;
	std::string own_parent_window_name;
	Gtk::Window *own_parent_window;
	Glib::RefPtr<Gnome::Glade::Xml> gxml;
	Gtk::Window *dialog;
	bool on_dialog_run;
	bool block_hide_dialog_by_out_focus;
	Gtk::Widget *exit_button;
	Glib::ustring dialog_pos_from_widget_name;
	
	ADD_PROPERTY(property_dialog_xml_path_, Glib::ustring)
	ADD_PROPERTY(property_dialog_name_, Glib::ustring)
	ADD_PROPERTY(property_dialog_focus_out_hide_, bool)
	ADD_PROPERTY(property_dialog_pos_, DialogPositionType)
	ADD_PROPERTY(property_dialog_pos_from_widget, Gtk::Widget*)
	ADD_PROPERTY(property_dialog_pos_x, int)
	ADD_PROPERTY(property_dialog_pos_y, int)
	ADD_PROPERTY(property_exit_button_name_, Glib::ustring)
	
private:
	void constructor();
	void configure();
	void dialog_connect();
	void set_connect_to_add_signal(Gtk::Container* w);
//	void process_sensor(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value);

//	USignals::Connection sensorPropConnection;

	DISALLOW_COPY_AND_ASSIGN(UniSVGDialogButton);
};
namespace Glib
{
	template <>
	class Value<UniSVGDialogButton::DialogPositionType> : public Value_Enum<UniSVGDialogButton::DialogPositionType>
	{
	public:
		static GType value_type() G_GNUC_CONST;
	};
} // namespace Glib

#endif // _UniSVGDialogButton_H
