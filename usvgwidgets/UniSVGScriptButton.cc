#include "UniSVGScriptButton.h"
//#include <guiconfiguration.h>
//#include <sstream>
//#include <fstream>
#include <types.h>

using namespace std;
using namespace UniWidgetsTypes;

#define INIT_UniSVGScriptButton_PROPERTIES() \
	property_script_path_(*this, "script-path", "" ) \

void UniSVGScriptButton::constructor()
{
	set_property_auto_connect(true);

	connect_property_changed("script-path", sigc::mem_fun(*this, &UniSVGScriptButton::script_path_changed));
}
//---------------------------------------------------------------------------------------
UniSVGScriptButton::UniSVGScriptButton() :
	Glib::ObjectBase("unisvgscriptbutton")
	,UniSVGButton()
	,INIT_UniSVGScriptButton_PROPERTIES()
{
	constructor();
}
//---------------------------------------------------------------------------------------
UniSVGScriptButton::UniSVGScriptButton(GtkmmBaseType::BaseObjectType* gobject) :
	UniSVGButton(gobject)
	,INIT_UniSVGScriptButton_PROPERTIES()
{
	constructor();
}
//---------------------------------------------------------------------------------------
UniSVGScriptButton::~UniSVGScriptButton()
{
}
//---------------------------------------------------------------------------------------
void UniSVGScriptButton::configure()
{
	script_path_changed();
}
//---------------------------------------------------------------------------------------
void UniSVGScriptButton::on_connect() throw()
{
	UniSVGButton::on_connect();
	configure();
}
//---------------------------------------------------------------------------------------
void UniSVGScriptButton::on_disconnect() throw()
{
	UniSVGButton::on_disconnect();
}
//---------------------------------------------------------------------------------------
void UniSVGScriptButton::on_realize()
{
	UniSVGButton::on_realize();
	configure();
}
//---------------------------------------------------------------------------------------
bool UniSVGScriptButton::on_expose_event(GdkEventExpose* event)
{
	return UniSVGButton::on_expose_event(event);
}
//---------------------------------------------------------------------------------------
bool UniSVGScriptButton::on_button_event(GdkEvent *event)
{
	UniSVGButton::on_button_event(event);
	GdkEventButton event_btn = event->button;
//	cout<<": event->type="<<event->type<< endl;

	if((event_btn.type == Gdk::BUTTON_PRESS && event_btn.button == 1))
	{
//		Glib::spawn_command_line_async(property_script_path_.get_value());//,cout,cerr,ret);
		try
		{
			Glib::spawn_command_line_async(property_script_path_.get_value());
		}
		catch(...)
		{
//			cerr << get_name()<<" :ERROR run script:" << property_script_path_<< endl;
			Gtk::MessageDialog msg(get_name()+" :ERROR run script:\n"+property_script_path_.get_value(),false, Gtk::MESSAGE_WARNING);
			msg.set_modal( true );
			msg.set_position( Gtk::WIN_POS_CENTER );
			msg.run();
		}
	}
	return false;
}
//---------------------------------------------------------------------------------------
void UniSVGScriptButton::script_path_changed()
{
	if(property_script_path_.get_value().empty())
	{
		cout<<get_name()<<": Put` k skriptu pust..."<<endl;
	}
	else if( !UniWidgetsTypes::file_exist(property_script_path_.get_value()) )
	{
		cout<<get_name()<<": fayil scripta ne sushestvuet po ukazannomu puti '"<<property_script_path_.get_value()<<"'..."<<endl;
	}
	else
	{
		cout<<get_name()<<": fayil scripta sushestvuet '"<<property_script_path_.get_value()<<"'..."<<endl;
	}
}