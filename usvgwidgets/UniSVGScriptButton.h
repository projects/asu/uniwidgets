#ifndef _UniSVGScriptButton_H
#define _UniSVGScriptButton_H

#include <libglademm.h>
#include <UDefaultFunctions.h>
#include <SensorProp.h>
#include <USignals.h>
#include <usvgwidgets/UniSVGButton.h>
#include <global_macros.h>

class UniSVGScriptButton : public UniSVGButton
{
public:
	UniSVGScriptButton();
	explicit UniSVGScriptButton(GtkmmBaseType::BaseObjectType* gobject);
	virtual ~UniSVGScriptButton();

//	virtual void set_connector(const ConnectorRef& connector) throw();
protected:

	/* Handlers */
	virtual bool on_expose_event(GdkEventExpose* event);
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void on_realize();
	virtual bool on_button_event(GdkEvent *event);

	void script_path_changed();

	ADD_PROPERTY(property_script_path_, Glib::ustring)

private:
	void constructor();
	void configure();
//	void process_sensor(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value);

//	USignals::Connection sensorPropConnection;

	DISALLOW_COPY_AND_ASSIGN(UniSVGScriptButton);
};

#endif // _UniSVGScriptButton_H
