#include "UniSVGTabButton.h"
//#include <guiconfiguration.h>
//#include <sstream>
//#include <fstream>

using namespace std;
using namespace UniWidgetsTypes;

#define INIT_UniSVGTabButton_PROPERTIES() \
	prop_tab_type(*this, "tab-type", TAB_FREE ) \
	,prop_notebook_type(*this, "notebook-type", HI_NOTEBOOK ) \
	,prop_notebook(*this, "notebook-widget", nullptr ) \
	,prop_hide_history_btn(*this, "hide-history-button", false ) \
	,prop_on_top_if_active(*this, "on-top-if-active", false ) \
	,prop_state_by_page(*this, "state-by-page", false ) \
	,notebook(nullptr) \
	,notebook_widget_name("") \
	,parent(nullptr) \
	,widget_x(0) \
	,widget_y(0) \
	,is_realized(false) \
	
void UniSVGTabButton::constructor()
{
	++tab_bt_id;
	own_number = tab_bt_id;
// 	cout<<get_name()<<"::constructor() UniSVGTabButton own_number="<<own_number<<endl;
	
	set_property_auto_connect(true);

	connect_property_changed("notebook-type", sigc::mem_fun(*this, &UniSVGTabButton::notebook_type_changed));
	connect_property_changed("notebook-widget", sigc::mem_fun(*this, &UniSVGTabButton::notebook_changed));
	connect_property_changed("tab-type", sigc::mem_fun(*this, &UniSVGTabButton::check_hide_history_btn));
	connect_property_changed("hide-history-button", sigc::mem_fun(*this, &UniSVGTabButton::check_hide_history_btn));
//	connect_property_changed("on-top-if-active", sigc::mem_fun(*this, &UniSVGTabButton::prop_on_top_if_active_changed));

	signal_size_allocate().connect(sigc::mem_fun(*this, &UniSVGTabButton::button_allocate_changed));
}
//---------------------------------------------------------------------------------------
UniSVGTabButton::UniSVGTabButton() :
	Glib::ObjectBase("unisvgtabbutton")
	,UniSVGButton()
	,INIT_UniSVGTabButton_PROPERTIES()
{
	constructor();
}
//---------------------------------------------------------------------------------------
UniSVGTabButton::UniSVGTabButton(GtkmmBaseType::BaseObjectType* gobject) :
	UniSVGButton(gobject)
	,INIT_UniSVGTabButton_PROPERTIES()
{
	constructor();
}
//---------------------------------------------------------------------------------------
UniSVGTabButton::~UniSVGTabButton()
{
}
// -------------------------------------------------------------------------
GType tabtype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { UniSVGTabButton::TAB_BACK, "TAB_BACK", "назад по истории" },{ UniSVGTabButton::TAB_FORWARD, "TAB_FORWARD", "вперёд по истории" },{ UniSVGTabButton::TAB_FREE, "TAB_FREE", "свободное (номер закладки)" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("TabType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UniSVGTabButton::TabType>::value_type()
{
	return tabtype_get_type();
}
// -------------------------------------------------------------------------
GType notebooktype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { UniSVGTabButton::HI_NOTEBOOK, "HI_NOTEBOOK", "вверх по иерархии" },{ UniSVGTabButton::LOW_NOTEBOOK, "LOW_NOTEBOOK", "вниз по иерархии" },{ UniSVGTabButton::ROOT_NOTEBOOK, "ROOT_NOTEBOOK", "вниз от главного окна" },{ UniSVGTabButton::CASTOM_NOTEBOOK, "CASTOM_NOTEBOOK", "выбрать Notebook" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("NotebookType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UniSVGTabButton::NotebookType>::value_type()
{
	return notebooktype_get_type();
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::init_notebook_widget(Gtk::Widget* widget)
{
	//	cout<<get_name()<<"::init_notebook_widget widget_name="<<widget->get_name()<<endl;
	//	cout<< get_name() << "::init_notebook_widget() widget="<<widget<<endl;
	if(widget)
	{
// 		cout<<get_name()<<"::init_notebook_widget widget_name="<<widget->get_name()<<" notebook_widget_name="<<notebook_widget_name<<endl;
		Gtk::Container* conteiner = dynamic_cast<Gtk::Container*>(widget);
		if(conteiner)
			conteiner->signal_add().connect(sigc::mem_fun(*this, &UniSVGTabButton::init_notebook_widget));
		
		if(!get_prop_notebook() && !notebook_widget_name.empty() && notebook_widget_name == widget->get_name())
		{
			Gtk::Notebook* nbook = dynamic_cast<Gtk::Notebook*>(widget);
			if(nbook)
				set_prop_notebook(nbook);
		}
	}
}
// -------------------------------------------------------------------------
void UniSVGTabButton::button_allocate_changed(Gtk::Allocation& alloc)
{
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::configure()
{
	parent = dynamic_cast<Gtk::Fixed*>(get_parent());
	if(parent && !is_realized)
	{
		is_realized = true;
		widget_x = get_allocation().get_x() - parent->get_allocation().get_x();
		widget_y = get_allocation().get_y() - parent->get_allocation().get_y();
// 		cout<< get_name() << "::configure() widget_x="<<widget_x<<" widget_y="<<widget_y<<endl;
	}
	
	notebook_type_changed();
	notebook_changed();
	if( (get_prop_tab_type()==TAB_BACK || get_prop_tab_type()==TAB_FORWARD) && notebook && prop_hide_history_btn)
		hide();
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::on_connect() throw()
{
	UniSVGButton::on_connect();
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::on_disconnect() throw()
{
	UniSVGButton::on_disconnect();
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::on_realize()
{
	UniSVGButton::on_realize();
	configure();
}
//---------------------------------------------------------------------------------------
bool UniSVGTabButton::on_expose_event(GdkEventExpose* event)
{
	bool ev = UniSVGButton::on_expose_event(event);
	return ev;
}
//---------------------------------------------------------------------------------------
bool UniSVGTabButton::on_button_event(GdkEvent *event)
{
	UniSVGButton::on_button_event(event);
	GdkEventButton event_btn = event->button;
// 	cout<< get_name() << "::on_button_event() event->type="<<event->type<< endl;

	if( event->type == GDK_VISIBILITY_NOTIFY )
	{
		if( !init_on_top )
		{
			init_on_top = true;
			lock_priority = true;
			for(NotebookPriorityMap::iterator npIt = priority.begin(); npIt!=priority.end();npIt++)
			{
				for(ParentPriorityMap::iterator ppIt = (npIt->second).begin(); ppIt!=(npIt->second).end();ppIt++)
				{
					for(PriorityMap::iterator pIt = (ppIt->second).begin(); pIt!=(ppIt->second).end();pIt++)
						(pIt->second)->set_on_top();
				}
			}
			lock_priority = false;
		}
	}
	
	if( event_btn.type == Gdk::BUTTON_PRESS )// && event_btn.button == 1 )
	{
		if( !prop_state_by_page )
			set_state(true);
	}
	else if(event_btn.type == Gdk::BUTTON_RELEASE )// && event_btn.button == 1 )
	{
		if(get_prop_tab_type()==TAB_BACK)
		{
			if( !prop_state_by_page )
				set_state(false);
			
			if(!history.empty() && notebook)
			{
				NotebookMap::iterator nit = history.find(notebook);
				if(nit!=history.end())
				{
					if(!nit->second.empty())
					{
						HistoryList::reverse_iterator pageIt = nit->second.rbegin();
						HistoryList::reverse_iterator last_page = nit->second.rbegin();
						bool find_current_page = false;
						for(pageIt; pageIt!=nit->second.rend();++pageIt)
						{
							if(!(*pageIt).current_tab && !find_current_page)
								continue;
							if(!find_current_page)
							{
								find_current_page = true;
								last_page = pageIt;
								continue;
							}
							
							last_pushed_tab_button = this;
							
							(*last_page).current_tab = false;
							(*pageIt).current_tab = true;
							notebook->set_current_page((*pageIt).page_num);
							last_pushed_tab_button = nullptr;
							break;
						}
					}
				}
			}
		}
		else if(get_prop_tab_type()==TAB_FORWARD)
		{
			if( !prop_state_by_page )
				set_state(false);

			if(!history.empty() && notebook)
			{
				NotebookMap::iterator nit = history.find(notebook);
				if(nit!=history.end())
				{
					if(!nit->second.empty())
					{
						HistoryList::iterator pageIt = nit->second.begin();
						HistoryList::iterator last_page = nit->second.begin();
						bool find_current_page = false;
						for(pageIt; pageIt!=nit->second.end();++pageIt)
						{
							if(!(*pageIt).current_tab && !find_current_page)
								continue;
							if(!find_current_page)
							{
								find_current_page = true;
								last_page = pageIt;
								continue;
							}
							
							last_pushed_tab_button = this;
							
							(*last_page).current_tab = false;
							(*pageIt).current_tab = true;
							notebook->set_current_page((*pageIt).page_num);
							last_pushed_tab_button = nullptr;
							break;
						}
					}
				}
			}
		}
		else if(get_prop_tab_type()==TAB_FREE)
		{
			if(notebook)
			{
				last_pushed_tab_button = this;
				notebook->set_current_page(property_sensor_value());
			}
		}
	}
	return false;
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::on_notebook_switch_page(GtkNotebookPage *page, guint page_num)
{
//	cout<<get_name()<<"::on_notebook_switch_page page="<<page<<" notebook="<<notebook<< endl;
	  
	if(last_pushed_tab_button)
		switched_to_page(page_num, last_pushed_tab_button->get_prop_tab_type()==TAB_FREE && last_pushed_tab_button->get_notebook()==notebook);
	else
		switched_to_page(page_num, true);
	
	if(notebook)
	{
		NotebookBtnMap::iterator nIt = notebook_btn.find(notebook);
		if(nIt!=notebook_btn.end())
		{
			BtnMap::iterator bIt = nIt->second.begin();
			for(bIt;bIt != nIt->second.end();++bIt)
				bIt->first->check_hide_history_btn();
		}
	}
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::switched_to_page(int page_num, bool write_to_history)
{
//	cout<<get_name()<<"::switched_to_page page_num="<<page_num<< endl;
	if (!notebook || get_prop_tab_type()!=TAB_FREE)
		return;
	
	if(write_to_history && !lock_priority)
	{
		if(history.empty())
		{
			HistoryList historylist;
			historylist.push_back(TabHistoryItem(page_num,true));
			history[notebook] = historylist;
		}
		else
		{
			NotebookMap::iterator nit = history.find(notebook);
			if(nit==history.end())
			{
				HistoryList historylist;
				historylist.push_back(TabHistoryItem(page_num,true));
				history[notebook] = historylist;
			}
			else
			{
				HistoryList::iterator pageIt = nit->second.begin();
				if(pageIt != nit->second.end())
				{
					bool find_current_page = false;
					for(pageIt;pageIt != nit->second.end();++pageIt)
					{
						if(!(*pageIt).current_tab && !find_current_page)
							continue;
						if(!find_current_page)
						{
							find_current_page = true;
							continue;
						}

						nit->second.erase(pageIt,nit->second.end());
						break;
					}
					if( (*(nit->second.rbegin())).page_num != page_num)
					{
						(*(nit->second.rbegin())).current_tab = false;
						nit->second.push_back(TabHistoryItem(page_num,true));
					}
				}
				else
					nit->second.push_back(TabHistoryItem(page_num,true));
			}
		}
	}
	
	if(notebook->get_current_page()  == property_sensor_value())
	{
		set_state(true);
		set_on_top();
		sensor_prop_di_.save_value(property_sensor_value());
	}
	else
		set_state(false);
}
// -------------------------------------------------------------------------
void UniSVGTabButton::to_hi_erarchy(Gtk::Widget* w)
{
	if(!w)
		return;
// 	cout<<get_name()<<"::to_hi_erarchy w_name="<<w->get_name()<<" w="<<w<< endl;
	if(Gtk::Notebook* n = dynamic_cast<Gtk::Notebook*>(w))
		notebook = n;
	else if(get_toplevel() != dynamic_cast<Gtk::Window*>(w))
		to_hi_erarchy(w->get_parent());
}
// -------------------------------------------------------------------------
void UniSVGTabButton::to_lo_erarchy(Gtk::Widget* w)
{
	if(!w)
		return;
// 	cout<<get_name()<<"::to_lo_erarchy w_name="<<w->get_name()<<" w="<<w<< endl;
	Glib::ListHandle<Gtk::Widget*> childlist = (dynamic_cast<Gtk::Container*>(w))->get_children();
	for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); ++it)
	{
		if(Gtk::Notebook* n = dynamic_cast<Gtk::Notebook*>(w))
			notebook = n;
		else if(Gtk::Container* cont = dynamic_cast<Gtk::Container*>(*it))
			to_lo_erarchy(cont);
		
		if(notebook)
			return;
	}
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::notebook_type_changed()
{
	
	if (pageChangeConnector.connected())
		pageChangeConnector.disconnect();
	
	if(get_prop_notebook_type()==HI_NOTEBOOK)
	{
		remove_from_notebook_maps(notebook);
		notebook = nullptr;
		to_hi_erarchy(get_parent());
		add_to_notebook_maps(notebook);
	}
	else if(get_prop_notebook_type()==LOW_NOTEBOOK)
	{
		remove_from_notebook_maps(notebook);
		notebook = nullptr;
		to_lo_erarchy(get_parent());
		add_to_notebook_maps(notebook);
	}
	else if(get_prop_notebook_type()==ROOT_NOTEBOOK)
	{
		remove_from_notebook_maps(notebook);
		notebook = nullptr;
		to_lo_erarchy(get_toplevel());
		add_to_notebook_maps(notebook);
	}
	else if(get_prop_notebook_type()==CASTOM_NOTEBOOK)
	{
		notebook_changed();
		return;
	}

	if(notebook)
	{
		pageChangeConnector = notebook->signal_switch_page().connect(sigc::mem_fun(*this, &UniSVGTabButton::on_notebook_switch_page));
		switched_to_page(notebook->get_current_page(), get_prop_tab_type()==TAB_FREE);
	}
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::notebook_changed()
{
	if(get_prop_notebook_type()==CASTOM_NOTEBOOK)
	{
		remove_from_notebook_maps(notebook);
		if (pageChangeConnector.connected())
			pageChangeConnector.disconnect();
		if(get_prop_notebook())
		{
			notebook = get_prop_notebook();
			add_to_notebook_maps(notebook);
			pageChangeConnector = notebook->signal_switch_page().connect(sigc::mem_fun(*this, &UniSVGTabButton::on_notebook_switch_page));
			switched_to_page(notebook->get_current_page(), get_prop_tab_type()==TAB_FREE);
		}
		else
			notebook = nullptr;
	}
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::check_hide_history_btn()
{
	if( (get_prop_tab_type()==TAB_BACK || get_prop_tab_type()==TAB_FORWARD) && notebook )
	{
		if (prop_hide_history_btn)
		{
			if(!history.empty())
			{
				NotebookMap::iterator nit = history.find(notebook);
				if(nit!=history.end())
				{
// 					cout<<get_name()<<"::check_hide_history_btn() begin().page_num="<<(*(nit->second.begin())).page_num<<" begin().current_tab="<<(*(nit->second.begin())).current_tab<<" rbegin().page_num="<<(*(nit->second.rbegin())).page_num<<" rbegin().current_tab="<<(*(nit->second.rbegin())).current_tab<< endl;
					if( get_prop_tab_type()==TAB_BACK && (*(nit->second.begin())).current_tab )
						hide();
					else if( get_prop_tab_type()==TAB_FORWARD && (*(nit->second.rbegin())).current_tab )
						hide();
					else
						show();
				}
			}
		}
		else
			show();
	}
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::prop_on_top_if_active_changed()
{
	set_on_top();
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::set_on_top()
{
	if( !get_mapped() || !get_prop_on_top_if_active() || is_glade_editor)
		return;
	
	parent = dynamic_cast<Gtk::Fixed*>(get_parent());
	if(parent && notebook && is_realized)
	{
		if(notebook->get_current_page() != property_sensor_value() && get_prop_tab_type()==TAB_FREE)
			return;
		
		NotebookPriorityMap::iterator npIt = priority.find(notebook);
		if(npIt!=priority.end())
		{
			ParentPriorityMap::iterator ppIt = (npIt->second).find(parent);
			if(ppIt!=(npIt->second).end())
			{
				for(PriorityMap::iterator pIt = (ppIt->second).begin(); pIt!=(ppIt->second).end();pIt++)
				{
					lock_priority = true;
					
					if(pIt->first == own_number)
						continue;
					
					if((pIt->second)->get_is_realized() && (pIt->second)->get_mapped())
					{
// 						cout<< get_name() << "::set_on_top() widget="<<(pIt->second)->get_name()<<" widget_x="<<(pIt->second)->get_widget_x()<<" widget_y="<<(pIt->second)->get_widget_y()<<endl;
						get_parent()->Gtk::Container::remove(*(pIt->second));
						parent->put(*(pIt->second),(pIt->second)->get_widget_x(),(pIt->second)->get_widget_y());
					}
				}
// 				cout<< get_name() << "::set_on_top() widget_x="<<widget_x<<" widget_y="<<widget_y<<endl;
				get_parent()->Gtk::Container::remove(*this);
				parent->put(*this,widget_x,widget_y);
				
				lock_priority = false;
			}
		}
	}
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::remove_from_notebook_maps(Gtk::Notebook* n)
{
	if(n && !lock_priority)
	{
		NotebookBtnMap::iterator nIt = notebook_btn.find(n);
		if(nIt!=notebook_btn.end())
		{
			BtnMap::iterator bIt = nIt->second.find(this);
			if(bIt!=nIt->second.end())
				nIt->second.erase(bIt);
		}

		parent = dynamic_cast<Gtk::Fixed*>(get_parent());
		if(parent)
		{
			NotebookPriorityMap::iterator npIt = priority.find(n);
			if(npIt!=priority.end())
			{
				ParentPriorityMap::iterator ppIt = npIt->second.find(parent);
				if(ppIt!=npIt->second.end())
				{
					PriorityMap::iterator pIt = ppIt->second.find(own_number);
					if(pIt!=ppIt->second.end())
						ppIt->second.erase(pIt);
				}
			}
		}
	}
}
//---------------------------------------------------------------------------------------
void UniSVGTabButton::add_to_notebook_maps(Gtk::Notebook* n)
{
	if(n && !lock_priority)
	{
		NotebookBtnMap::iterator nIt = notebook_btn.find(n);
		if(nIt!=notebook_btn.end())
		{
			BtnMap::iterator bIt = nIt->second.find(this);
			if(bIt==nIt->second.end())
				(nIt->second)[this] = true;
		}
		else
			notebook_btn[n][this] = true;
		
		parent = dynamic_cast<Gtk::Fixed*>(get_parent());
		if(parent)
		{
			priority[n][parent][own_number] = this;
		}
	}
}