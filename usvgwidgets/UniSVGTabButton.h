#ifndef _UniSVGTabButton_H
#define _UniSVGTabButton_H

#include <libglademm.h>
#include <UDefaultFunctions.h>
#include <SensorProp.h>
#include <USignals.h>
#include <usvgwidgets/UniSVGButton.h>
#include <global_macros.h>
#include <unordered_map>

static long tab_bt_id=0;

struct TabHistoryItem
{
	TabHistoryItem():
	page_num(0),
	current_tab(false)
	{
	};
	TabHistoryItem(int new_page_num,bool new_current_tab):
	page_num(new_page_num),
	current_tab(new_current_tab)
	{
	};
	TabHistoryItem(const TabHistoryItem &item)
	{
		*this = item;
	};
	TabHistoryItem& operator=(const TabHistoryItem &item)
	{
		page_num = item.page_num;
		current_tab = item.current_tab;
		
		return (*this);
	};
	int page_num;
	bool current_tab;
}__attribute__((packed));
typedef std::list<TabHistoryItem> HistoryList;
typedef std::unordered_map<Gtk::Notebook*,HistoryList> NotebookMap;
static NotebookMap history;

class UniSVGTabButton : public UniSVGButton
{
public:
	UniSVGTabButton();
	explicit UniSVGTabButton(GtkmmBaseType::BaseObjectType* gobject);
	virtual ~UniSVGTabButton();

	enum TabType
	{
		TAB_BACK=0
		,TAB_FORWARD
		,TAB_FREE
	};
	enum NotebookType
	{
		HI_NOTEBOOK=0
		,LOW_NOTEBOOK
		,ROOT_NOTEBOOK
		,CASTOM_NOTEBOOK
	};

	inline Gtk::Notebook* get_notebook(){ return notebook;};
	inline void set_notebook_widget_name(Glib::ustring name) { notebook_widget_name = name; }
	void init_notebook_widget(Gtk::Widget* widget);

	inline bool get_is_realized(){ return is_realized;};
	inline int get_widget_x(){ return widget_x;};
	inline int get_widget_y(){ return widget_y;};
	
//	virtual void set_connector(const ConnectorRef& connector) throw();
protected:

	/* Handlers */
	virtual bool on_expose_event(GdkEventExpose* event);
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void on_realize();
	virtual bool on_button_event(GdkEvent *event);
	virtual void button_allocate_changed(Gtk::Allocation& alloc);
	
	Gtk::Notebook *notebook;
	std::string notebook_widget_name;
	sigc::connection pageChangeConnector;
	void on_notebook_switch_page(GtkNotebookPage *page, guint page_num);
	virtual void switched_to_page(int page_num, bool write_to_history);
	
	void notebook_type_changed();
	void notebook_changed();
	void check_hide_history_btn();
	void prop_on_top_if_active_changed();
	
	void to_hi_erarchy(Gtk::Widget* w);
	void to_lo_erarchy(Gtk::Widget* w);
	void remove_from_notebook_maps(Gtk::Notebook* n);
	void add_to_notebook_maps(Gtk::Notebook* n);
	virtual void set_on_top();
	
	ADD_PROPERTY( prop_tab_type, TabType )					/*!< свойство: тип перехода по вкладкам Notebook*/
	ADD_PROPERTY( prop_notebook_type, NotebookType )		/*!< свойство: тип Notebook (где искать Notebook)*/
	ADD_PROPERTY( prop_notebook, Gtk::Notebook* )			/*!< свойство: Notebook в котором переключаем закладки*/
	ADD_PROPERTY( prop_hide_history_btn, bool )				/*!< свойство: скрывать кнопки истории если переход по ней не возможен*/
	ADD_PROPERTY( prop_on_top_if_active, bool )				/*!< свойство: отрисовывать поверх других кнопок привязанных к томуже prop_notebook,если кнопка активирована*/
	ADD_PROPERTY( prop_state_by_page, bool )				/*!< свойство: выставлять кнопку нажатой только по текущей странице Notebook*/

	bool is_realized;
	
private:
	long own_number;
	Gtk::Fixed* parent;
	int widget_x;
	int widget_y;
	
	void constructor();
	void configure();
//	void process_sensor(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value);

//	USignals::Connection sensorPropConnection;

	DISALLOW_COPY_AND_ASSIGN(UniSVGTabButton);

	
};
static UniSVGTabButton* last_pushed_tab_button = 0;

typedef std::unordered_map<UniSVGTabButton*,bool> BtnMap;
typedef std::unordered_map<Gtk::Notebook*,BtnMap> NotebookBtnMap;
static NotebookBtnMap notebook_btn;

typedef std::map<int,UniSVGTabButton*> PriorityMap;
typedef std::unordered_map<Gtk::Fixed*,PriorityMap> ParentPriorityMap;
typedef std::unordered_map<Gtk::Notebook*,ParentPriorityMap> NotebookPriorityMap;
static NotebookPriorityMap priority;
static bool init_on_top = false;
static bool lock_priority = false;

namespace Glib
{
	template <>
	class Value<UniSVGTabButton::TabType> : public Value_Enum<UniSVGTabButton::TabType>
	{
	public:
		static GType value_type() G_GNUC_CONST;
	};
	template <>
	class Value<UniSVGTabButton::NotebookType> : public Value_Enum<UniSVGTabButton::NotebookType>
	{
	public:
		static GType value_type() G_GNUC_CONST;
	};
} // namespace Glib

#endif // _UniSVGTabButton_H
