# Example for project building with local uniwidgets

USERNAME=$1

# Path to uniwidgets directory
export UWPATH=/srv/$USERNAME/Projects/uniwidgets
export UNIWIDGETS_CFLAGS="-I$UWPATH"
export UNIWIDGETS_LIBS="-L$UWPATH/lib -l uniwidgets -L$UWPATH/plugins/libglade -l UniWidgets"

./autogen.sh
make
