# Script for start glade with local uniwidgets

# Path to uniwidgets directory
export UWPATH=`pwd`/../..
 # $(dirname $0)/../..
export GLADE_CATALOG_PATH=$UWPATH/plugins/glade
export GLADE_MODULE_PATH=$GLADE_CATALOG_PATH/.libs
export UNIWIDGETS_MODULE_PATH=$UWPATH/plugins/uniset2/.libs

#gdb --args
glade-3 $*
