#!/bin/sh

export LIBGLADE_MODULE_PATH=../../plugins/libglade/.libs

START=uniset-start.sh

${START} -f ./test  --svg-path ./svg/ \
		    --gtkthemedir ./theme \
		    --cbid 52 \
		    --uniset-port 54613 \
		    --localNode localhost \
		    --confile ../../../yauza/conf/configure.xml \
		    --unideb-add-levels info,warn,crit,system --dlog-add-levels info,warn,crit \
		    "$*"
#
#cbid 105
#--confile ../../../yauza/conf/configure.xml
#--localNode LocalhostNode \
#		    --heartbeat-node localhost \