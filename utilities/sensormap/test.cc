#include <iostream>
#include <gtkmm.h>
#include <libglademm.h>
#include <Configuration.h>
//#include "UJournal.h"
//#include "UAPSJournal.h"
//#include "Image.h"
#include <uwidgets/UProxyWidget.h>
//#include "SimpleOscillograph.h"
//#include "OscillographLogic.h"
#include <uwidgets/ULockNotebook.h>

using namespace std;
using namespace UniSetTypes;
//using namespace UniWidgets;

static gboolean alpha_channel_support = false;
static const string GLADE_DIR = "glade/";
static const string GLADE_FILE = "objects.glade";
ConnectorRef connector_;

class MainWindow
{
	public:
		MainWindow(string gladedir, string guifile, string svgdir);
		~MainWindow() {delete w;};
		Gtk::Window *w;

};

MainWindow::MainWindow (string gladedir, string guifile, string svgdir)
{
	Gtk::Window splash;
	splash.modify_bg(Gtk::STATE_NORMAL, Gdk::Color("#000000"));

	Gtk::Alignment alignment(0.5,0.5,0,0);
	Gtk::ProgressBar bar;
	alignment.add(bar);
	splash.add(alignment);
	splash.set_default_size(800,600);
	splash.show_all();

	while(Gtk::Main::events_pending())
		Gtk::Main::iteration();

  Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::create(gladedir + guifile);

  gxml->get_widget("window1", w);

	bar.set_fraction(1);
	splash.hide();
}

main (int argc, char **argv)
{
	Glib::RefPtr<Gdk::Screen> screen;
	Glib::RefPtr<Gdk::Colormap> colormap;

	if (!g_thread_supported())
		g_thread_init(NULL);

	/* Secure gtk */
	gdk_threads_init();
	
	/* Obtain gtk's global lock */
	gdk_threads_enter();

	Gtk::Main Kit(argc, (char**&)argv);

	string confile = getArgParam( "--confile", argc, argv, "configure.xml" );
	conf = new Configuration(argc, argv, confile);

	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );

	MainWindow mw(GLADE_DIR, GLADE_FILE, "./svg");

// 	gtk_main();
	Kit.run(*mw.w);

	/* Release gtk's global lock */
	gdk_threads_leave();
}
