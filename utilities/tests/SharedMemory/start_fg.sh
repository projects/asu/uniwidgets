#!/bin/sh

ulimit -Sc 10000000000
START=uniset-start.sh

${START} -f uniset-smemory \
	--smemory-id SharedMemory1 \
	--e-filter evnt_gui \
	--pulsar-id pulsar \
	--pulsar-msec 3000 \
	--heartbeat-node localhost \
	$*

# Parameters for logging

#--dlog-add-levels info,warn,crit
#--unideb-add-levels info,warn,crit
#--enable-db-logging 1
