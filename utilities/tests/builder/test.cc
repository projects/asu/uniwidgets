#include <iostream>
#include <gtkmm.h>
#include <Configuration.h>
#include "UJournal.h"
#include "UAPSJournal.h"
#include "Image.h"
#include "UProxyWidget.h"
//#include "SimpleOscillograph.h"
//#include "OscillographLogic.h"
#include "ULockNotebook.h"
#include "Sensor1.h"

#include "init.h"
#include "RIndicatingInstrument.h"

using namespace std;
using namespace UniSetTypes;
using namespace UniWidgets;

static const string GLADE_DIR = "glade/";
static const string GLADE_FILE = "objects.ui";
static gboolean alpha_channel_support = false;
ConnectorRef connector_;

static bool on_window_expose_event(GdkEventExpose *event,GdkWindow* window)
{
	cairo_t *cr;
// 	Cairo::RefPtr<Cairo::Context> cr_ = widget->get_window()->create_cairo_context();

   	cr = gdk_cairo_create(window); // create cairo context

	if(alpha_channel_support)
	// set source to RGB-Alpha
	cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.7);
	else
	// set source to RGB-Alpha
	cairo_set_source_rgb(cr, 1.0, 1.0, 1.0); // set color RGB
	
	cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE); // set drawing compositing operator
							// SOURCE -> replace destination
	
	cairo_paint(cr); // paint source
	
	cairo_destroy(cr);
	g_message("expose event\n");
	
	return false;
}

#if 0
void show_oscillograph(OscillographLogic *w, const long id, Glib::ustring *cn)
{
	w->add_new_oscil_id(id,cn);
}
#endif 

class MainWindow
{
	public:
		MainWindow(string gladedir, string guifile, string svgdir, string weblog_addr);
		~MainWindow() {delete w;};
		Gtk::Window *w;
	private:
		Gtk::Notebook *jb;
		bool full_state, is_pressed;

		/* ипспользовалось при отладке */
// 		bool on_key_event(GdkEventKey *e);
		/* ипспользовалось при отладке */
// 		bool pressed(GdkEventButton *eb);
		/* ипспользовалось при отладке */
// 		bool released(GdkEventButton *eb);

// 		void process_message(UMessages::MessageId id, int wtype, time_t sec, Glib::ustring msg);
// 		void process_confirm(UMessages::MessageId id, time_t sec);

		void switch_lock();

		guint page;

		Gtk::CheckButton* check_button;
		ULockNotebook* lock_notebook;
};

MainWindow::MainWindow (string gladedir, string guifile, string svgdir, string weblog_addr) :
	full_state(false),
	is_pressed(false),
	page(1)
{
	Gtk::Window splash;
	splash.modify_bg(Gtk::STATE_NORMAL, Gdk::Color("#000000"));

	Gtk::Alignment alignment(0.5,0.5,0,0);
	Gtk::ProgressBar bar;
	alignment.add(bar);
	splash.add(alignment);
	splash.set_default_size(800,600);
	splash.show_all();
	while(Gtk::Main::events_pending())
		Gtk::Main::iteration();
	Glib::RefPtr<Gtk::Builder> builder;
	builder = Gtk::Builder::create();
	try
	{
		builder->add_from_file(gladedir + guifile);
	}catch(Gtk::BuilderError& ex){
		cerr << "MW builder create: " << ex.what() << endl;
		return;
	}
	/*Получить connector для опроса датчиков*/
	UProxyWidget *pw=0;
	builder->get_widget("uproxywidget1", pw);
	if(pw)
		connector_ = pw->get_connector();
	/*--------- Processing confirm signals ----------*/
// 	connector_->signals().connect_on_any_message_full(
// 			sigc::mem_fun(this, &MainWindow::process_message));
	builder->get_widget("window1", w);
	
	if( !w )
		throw SystemError("Not found widget 'window1'");
	
	builder->get_widget("ulocknotebook1", lock_notebook);

	//UJournal* Journal_;
	//UAPSJournal* APSJournal_;
	//builder->get_widget("ujournal1", Journal_);
	//builder->get_widget("uapsjournal1", APSJournal_);
	//Journal_->property_auto_connect() = false;
	//Journal_->set_connector(connector_);
	//APSJournal_->property_auto_connect() = false;
	//APSJournal_->set_connector(connector_);

	builder->get_widget("checkbutton1", check_button);
	check_button->signal_toggled().connect(sigc::mem_fun(*this, &MainWindow::switch_lock));
	lock_notebook->enable_lock(!check_button->get_active());

// 	WebKitWebView* webview;
// 	webview = WEBKIT_WEB_VIEW(webkit_web_view_new());
// 	webkit_web_view_load_uri(WEBKIT_WEB_VIEW(webview), weblog_addr.c_str());
// 	Gtk::Widget* browser = Glib::wrap(GTK_WIDGET(webview));

// 	g_signal_connect(webview, "download-requested", G_CALLBACK
// 		(download_requested_cb), fw);
// 	packer.append_widget(browser,"database");
// 	browser->show();

// 	w->signal_key_press_event().connect( sigc::mem_fun(*this, &MainWindow::on_key_event) );

	bar.set_fraction(1);
	splash.hide();
}

void MainWindow::switch_lock()
{
	lock_notebook->enable_lock(!check_button->get_active());
}

main (int argc, char **argv)
{
	Glib::RefPtr<Gdk::Screen> screen;
	Glib::RefPtr<Gdk::Colormap> colormap;

	if (!g_thread_supported())
		g_thread_init(NULL);

	/* Secure gtk */
	gdk_threads_init();
	
	/* Obtain gtk's global lock */
	gdk_threads_enter();

	Gtk::Main Kit(argc, (char**&)argv);

	UniWidgets::init(argc, (char**&)argv);
	uniset_init(argc, argv);

	string theme = getArgParam( "--gtkthemedir", argc, argv, "");
	if ( !theme.empty() )
		gtk_rc_parse( (theme + "/gtkrc").c_str() );
	cout<<"creating MW"<<endl;
	MainWindow mw(GLADE_DIR, GLADE_FILE, "./svg", "http://127.0.0.1:80");
	cout<<"created MW"<<endl;
// 	gtk_main();
	Kit.run(*mw.w);

	/* Release gtk's global lock */
	gdk_threads_leave();
}
