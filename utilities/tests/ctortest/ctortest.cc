#include <gtkmm.h>
#include <Configuration.h>
#include <uniwidgets/Connector.h>
#include <uwidgets/UWidgets.h>
#include <usvgwidgets/USVGIndicatorPlus.h>
#include <iostream>

using namespace std;
using namespace UniSetTypes;

int main(int argc, char **argv)
{
	Gtk::Main kit(argc, argv);

	string confile = UniSetTypes::getArgParam( "--confile", argc, argv, "configure.xml" );
	conf = new Configuration(argc, argv, confile);

	Gtk::Window w;
	UProxyWidget p;
	USVGIndicatorPlus i("../../svg/lamp_red.svg", "../../svg/lamp_green.svg");

	i.get_di()->set_sens_id(101);
	i.get_di()->set_node_id(3001);
	p.add(i);

	p.property_parameters() = "demoGUI;10;105";
	p.property_module() = "uniset";
	w.add(p);
	w.show_all_children();

	cerr << "###SHOW###" << endl;
	kit.run(w);
//	ConnectorRef connector = Connector::create_connector("uniset", "demoGUI;51");
//	pause();
//	Gtk::Window w;
//	Gtk::Widget* wid;
//	string type(*(argv+1));
//	if (type == "UEventBox")
//			wid = new UEventBox;
//	else if (type == "UIndicator")
//			wid = new UIndicator;
//	else if (type =="UBar")
//			wid = new UBar;
//	else if (type =="UButton")
//			wid = new UButton;
//	else if (type =="UHScale")
//			wid = new UHScale;
//	else if (type =="UTable")
//			wid = new UHScale;
//	else if (type =="UProxyTable")
//			wid = new UHScale;
//	else if (type =="UProxyWidget")
//			wid = new UProxyWidget;
//	else {
//		cerr<<"Unknown widget type:"<<*argv<<endl;
//		return 1;
//	}
//	wid = new ( UProxyWidgetNew );
//	w.add(*wid);
//	UProxyWidgetNew proxy;
//	proxy.property_module() = "uniset";
//	w.show_all();
//	kit.run(w);
//	cout<<"Test of:"<<type<<endl;
  return 0;
}
