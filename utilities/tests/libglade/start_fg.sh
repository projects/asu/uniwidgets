#!/bin/sh

export LIBGLADE_MODULE_PATH=../../../plugins/libglade/.libs

START=uniset-start.sh

${START} -g ./test  --svg-path ./svg/ \
		    --gtkthemedir ./theme \
		    --cbid 51 \
		    --confile configure.xml \
		    --localNode LocalhostNode \
		    --heartbeat-node localhost \
		    --unideb-add-levels info,warn,crit,system --dlog-add-levels info,warn,crit
#cbid 105
