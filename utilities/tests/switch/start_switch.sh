#!/bin/sh

#export ulimit -Sc 10000000
START=uniset-start.sh

#nice -20 
${START} -f ./switch --confile configure.xml --localNode K1Node --delay 10 --maxval 10000 --minval -10000 --step 100 --unideb-add-levels info,warn,crit,system --dlog-add-levels info,warn,crit
