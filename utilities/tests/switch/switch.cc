#include <iostream>
#include <UniversalInterface.h>
#include <UniSetTypes.h>

using namespace UniSetTypes;
using namespace std;
int main (int argc, char** argv)
{
	string confile = getArgParam( "--confile", argc, argv, "configure.xml" );
	int delay = getArgInt( "--delay", argc, argv, "configure.xml" );
	long maxval = getArgInt( "--maxval", argc,argv,"configure.xml");
	long minval = getArgInt( "--minval", argc,argv,"configure.xml");
	long step = getArgInt( "--step", argc,argv,"configure.xml");
	conf = new Configuration(argc, argv, confile);
	UniversalInterface* ui = new UniversalInterface;
	long val = minval;
	long i = -step;

	try
	{
		for(;;)
		{
//			ui->saveState(7777701,true,UniversalIO::DigitalInput,3001);
//			msleep(delay);
//			cerr<<"+";
//			ui->saveState(7777701,false,UniversalIO::DigitalInput,3001);
			
			ui->saveValue(7777704,val,UniversalIO::AnalogInput,3001);
			if (val >= maxval) i = -i;
			if (val <= minval) i = -i;
			val+=i;
			msleep(delay);
		}
	}
	catch (Exception& ex)
	{
			cout<<"Error:"<<ex<<endl;
	}

	return 0;
}
