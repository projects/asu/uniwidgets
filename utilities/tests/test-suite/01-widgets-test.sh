#!/bin/sh

START=/usr/bin/uniset-start.sh
tmp=./all_tmp.xml;
#sensors=( "102" "105" "109"
#	 "123" "1205" "QM1_Protection_S"
#	 "Ctl_QG1_On_S" "GEU1_FQC_Ready1_S" "GEU2_FQC_Ready2_S"
#	 "GEU1_Q1_Protection_S" "GEU2_Q3_Protection_S" "GEU1_NPUS_CtlKPUS_DI"
#	 "GEU2_NPUS_CtlKPUS_DI" "GEU1_KPUS_AlarmBreak_S" "GEU2_KPUS_AlarmBreak_S"
#	 "TSCPU_SecondMech_S" "GEU1_CPU_Reset_S" "GEU2_CPU_AlarmBreak_S")
# sensors=( "102" "1421" "2301" "QM1_Protection_S"
# 	 "Ctl_QG1_On_S" "GEU1_FQC_Ready1_S" "GEU2_FQC_Ready2_S"
# 	 "GEU1_Q1_Protection_S" "GEU2_Q3_Protection_S" "GEU1_NPUS_CtlKPUS_S"
# 	 "GEU2_NPUS_CtlKPUS_S" "GEU1_KPUS_AlarmBreak_S" "GEU2_KPUS_AlarmBreak_S"
# 	 "TSCPU_SecondMech_S" "GEU1_CPU_Reset_S" "GEU2_CPU_AlarmBreak_S")

# old_pattern="sensor"

# for str in ${sensors[@]}
# do
# 	sed -e "s/${old_pattern}/${str}/g" ${tmp} > all_${str}.xml
# done

${START} -f uniset-testsuite-xmlplayer --confile configure.xml --testfile unet2.xml --show-result-report --show-actions-log --show-test-log $*
# 
# for str in ${sensors[@]}
# do
# 	rm all_${str}.xml
# done