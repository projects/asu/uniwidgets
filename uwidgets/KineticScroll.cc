#include <sstream>
#include <iostream>
#include <iomanip>
#include <math.h>
#include "KineticScroll.h"
// -------------------------------------------------------------------------
using namespace std;
// -------------------------------------------------------------------------
KineticScroll::KineticScroll():
	orient(Gtk::ORIENTATION_VERTICAL),
	vadj(0),
	widget(0),
	prev_x(0),
	prev_y(0),
	prev_v(0),
	prev_t(0),
	but_pressed(false),
	kinetic(0),
	min_kinetic(0.012),
	max_kinetic(0.022),
	k_sliding(0.9975),
	k_timer_step(50),
	max_a(4),
	min_dz(10),
	kinetic_on(true),
	scroll_on(true),
	k_target_sliding(0.9975),
	target_pos(0),
	target_sign(1)
{
	k_const = (double)k_timer_step*(double)k_timer_step/2.0;
}
// -------------------------------------------------------------------------
KineticScroll::~KineticScroll()
{
}
// -------------------------------------------------------------------------
void KineticScroll::set_orientation( Gtk::Orientation o )
{
	orient = o;
}
// -------------------------------------------------------------------------
void KineticScroll::set_adjustment( Gtk::Adjustment* v )
{
	vadj = v;
}
// -------------------------------------------------------------------------
void KineticScroll::set_motion_event( Gtk::Widget* w )
{
	w->signal_event().connect(sigc::mem_fun(*this, &KineticScroll::on_motion_event));
	widget = w;
}
// -------------------------------------------------------------------------
static float sign( float v )
{
	if( v < 0 )
		return -1.0;
	if( v > 0 )
		return 1.0;
	
	return 0.0;
}
// -------------------------------------------------------------------------
bool KineticScroll::on_motion_event( GdkEvent *event )
{
	if( !event || !scroll_on )
		return false;

	if( event->type == GDK_FOCUS_CHANGE || event->type == GDK_VISIBILITY_NOTIFY )
	{
		but_pressed = false;
		k_timer.disconnect();
		return false;
	}

	if( event->type == GDK_BUTTON_PRESS )
	{
		// cerr << "kinetick: but press... but_pressed=" << but_pressed << endl;
		if( but_pressed )
			return false;

		k_target_timer.disconnect();
		if( kinetic_on )
		{
			k_timer.disconnect();
			k_timer = Glib::signal_timeout().connect(sigc::mem_fun(*this, &KineticScroll::kinetic_timer),k_timer_step);
		}

		but_pressed = true;
		// запоминаем значения
		Gdk::Event ev(event);
		ev.get_coords(prev_x,prev_y); // ev.get_root_coords();
		prev_t = ev.get_time();
		prev_v = 0;
		kinetic = 0;
		prev_dz = 0;
		k_event(keBeginScroll,vadj->get_value());
	}
	else if( event->type == GDK_BUTTON_RELEASE )
	{
		but_pressed = false;
#if 0
		if( fabs(kinetic) < min_kinetic )
			kinetic = sign(kinetic)*min_kinetic;
		else if( fabs(kinetic) > max_kinetic )
			kinetic = sign(kinetic)*max_kinetic;
#endif
		if( !kinetic_on )
			k_event(keEndScroll,vadj->get_value());

		return false;
	}
	else if( !but_pressed || event->type != GDK_MOTION_NOTIFY )
		return false;

	Gdk::Event ev(event);
	guint32 t = ev.get_time();
	if( t == 0 )
		return false; // видимо другое событие
		
	guint32 dt = t - prev_t;
	prev_t = t;

	double x=0,y=0;
	ev.get_coords(x,y); //	ev.get_root_coords(x_r,y_r);
	
	double dz = 0;
	if( orient == Gtk::ORIENTATION_HORIZONTAL )
	{	
		dz = x - prev_x;
		prev_x = x;
	}
	else //	if( orient == Gtk::ORIENTATION_VERTICAL )
	{	
		dz = y - prev_y;
		prev_y = y;
	}

	if( dz > 0 && (dz - prev_dz) < 0 )
		return false;

	if( dz < 0 && (dz - prev_dz) > 0 )
		return false;

	prev_dz = dz;

	if( fabs(dz) < min_dz )
	{
		if( but_pressed )
		{
			double next_pos =  vadj->get_value() - dz;
			set_current_position( next_pos );
		}
		return false;
	}

	double v = dt ? dz/dt : 0;
	double dv = v - prev_v;
	prev_v = v;

	if( v == 0 )
		return false;

	// ускорение
	double a = dt ? dv/dt : 0;

	kinetic += a;

	if( fabs(kinetic) > max_kinetic )
		kinetic = sign(kinetic) * max_kinetic;
	else if( fabs(kinetic) < min_kinetic  )
		kinetic = sign(kinetic) * min_kinetic;

#if 0
	std::cout << "(event2): t=" << t
		<< " dt=" << setw(3) << dt
		<< " a=" << setw(9) << a
		<< " dz=" << setw(3) << dz
		<< " V=" << setw(7) << v
		<< " dV=" << setw(7) << dv
		<< " kinetic=" << setw(7) << kinetic
		<< endl;
#endif

	double next_pos =  vadj->get_value() - dz;
	set_current_position( next_pos );
	return false;
}
// -------------------------------------------------------------------------
void KineticScroll::set_current_position( int pos )
{
	if( pos < 0 )
	{
		vadj->set_value(0);
		kinetic = 0;
		prev_v = 0;
		if( kinetic_on )
		{
			k_timer.disconnect();
			k_event(keEndScroll,0);
		}
	}
	else if( pos > (vadj->get_upper() - vadj->get_page_size()) )
	{
		vadj->set_value( vadj->get_upper() - vadj->get_page_size() );
		kinetic = 0;
		prev_v = 0;
		if( kinetic_on )
		{
			k_timer.disconnect();
			k_event(keEndScroll,vadj->get_value());
		}
	}
	else
		vadj->set_value(pos);
}
// -------------------------------------------------------------------------
bool KineticScroll::kinetic_timer()
{
	if( !widget->is_mapped() ) // || !widget->is_focus() )
	{
		but_pressed = false;
		k_event(keEndScroll,vadj->get_value());
		return false;
	}
	
	if( !but_pressed && fabs(kinetic) <= min_kinetic )
	{
	  	prev_v = 0;
		kinetic = 0;
		k_event(keEndScroll,vadj->get_value());
		return false;
	}

	if( fabs(kinetic) < min_kinetic )
		return true;

	// расход энергии
	kinetic *= k_sliding;

	// пока нажата кнопка список перемещается
	// ровно по указателю мыши..(см. on_page_event)
	// соотвественно здесь игнорируем..
	if( but_pressed )
		return true;

	double next_pos = vadj->get_value() - kinetic*k_const;

	if( next_pos <= 0 )
	{
		vadj->set_value(0);
		prev_v = 0;
		kinetic = 0;
		k_event(keEndScroll,0);
		return false;
	}
	else if( next_pos >= ( vadj->get_upper() - vadj->get_page_size()) )
	{
		vadj->set_value( vadj->get_upper() - vadj->get_page_size() );
		prev_v = 0;
		kinetic = 0;
		k_event(keEndScroll,vadj->get_value());
		return false;
	}

	vadj->set_value(next_pos);
	k_event(keScroll,next_pos);

	return true;
}
// -------------------------------------------------------------------------
void KineticScroll::set_k_timer( int msec )
{
	k_timer_step = msec;
	k_const = (double)k_timer_step*(double)k_timer_step/2.0;
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, KineticScroll::KineticEventID i )
{
	switch(i)
	{
		case KineticScroll::keBeginScroll:	
			os << "keBeginScroll";
		break;
		case KineticScroll::keEndScroll:	
			os << "keEndScroll";
		break;
		case KineticScroll::keScroll:	
			os << "keScroll";
		break;
		default:
			os << "UnknownKineticID";
		break;
	}

	return os;
}
// -------------------------------------------------------------------------
KineticScroll::KineticEvent_Signal KineticScroll::signal_kinetic_event()
{
	return k_event;
}
// -------------------------------------------------------------------------
void KineticScroll::set_target( int pos, double s, double k )
{
	if( fabs(vadj->get_value() - pos) <= 20.0 )
	{
		vadj->set_value(pos);
		return;
	}

	if( pos < 0 )
	{
		vadj->set_value(0);
		return;
	}

	if( pos >= ( vadj->get_upper() - vadj->get_page_size()) )
	{
		vadj->set_value( vadj->get_upper() - vadj->get_page_size() );
		return;
	}

	target_pos = pos;
	k_target_sliding = s;
	bool sign = pos < vadj->get_value() ? true : false;
	kinetic = k;
	target_sign = 1;
	if( (sign && k<0) || (!sign && k>0) )
	{
		kinetic *= -1.0;
		target_sign = -1;
	}

	k_target_timer.disconnect();
	k_target_timer = Glib::signal_timeout().connect(sigc::mem_fun(*this, &KineticScroll::target_kinetic_timer),k_timer_step);
}
// -------------------------------------------------------------------------
bool KineticScroll::target_kinetic_timer()
{
//	while(Gtk::Main::events_pending())
//    		Gtk::Main::iteration();

	if( !widget->is_mapped() )
	{
		k_event(keEndScroll,vadj->get_value());
		kinetic = 0;
		return false;
	}
	
	// расход энергии
	kinetic *= k_target_sliding;
	if( fabs(kinetic) < min_kinetic )
		kinetic = sign(kinetic)*min_kinetic;
	else if( fabs(kinetic) > max_kinetic )
		kinetic = sign(kinetic)*max_kinetic;
	
	double next_pos = vadj->get_value() - kinetic*k_const;


//	cerr << "kinetic: next_pos=" << next_pos << " target_pos=" << target_pos 
//		<< " kinetic=" << kinetic 
//		<< endl;

	if( target_sign > 0 && (next_pos + 20.0 >= target_pos) )
	{
		vadj->set_value(target_pos);
		kinetic = 0;
		k_event(keEndScroll,vadj->get_value());
		return false;
	}
	else if( target_sign < 0 && ( target_pos > (next_pos - 20.0 ) )  )
	{
		vadj->set_value(target_pos);
		kinetic = 0;
		k_event(keEndScroll,vadj->get_value());
		return false;
	}
	else if( next_pos <= 0 )
	{
		vadj->set_value(0);
		kinetic = 0;
		k_event(keEndScroll,0);
		return false;
	}
	else if( next_pos >= ( vadj->get_upper() - vadj->get_page_size()) )
	{
		vadj->set_value( vadj->get_upper() - vadj->get_page_size() );
		kinetic = 0;
		k_event(keEndScroll,vadj->get_value());
		return false;
	}

	vadj->set_value(next_pos);
	k_event(keScroll,next_pos);

//	while(Gtk::Main::events_pending())
//    		Gtk::Main::iteration();

	return true;
}
// -------------------------------------------------------------------------