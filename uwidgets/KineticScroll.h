#ifndef KineticScroll_H_
#define KineticScroll_H_
// -------------------------------------------------------------------------
/*! \author Pavel Vaynerman <pv@etersoft.ru> */
// -------------------------------------------------------------------------
#include <gtkmm.h>
// -------------------------------------------------------------------------
/*! Универсальный кинетический скролл. Реализует "кинетическую" прокрутку (вертикальную или горизонтальную).
По умолчанию - вертикальную.

	Один из примеров использования:
\code
	class MyClass
	{
	    ....
	    Gtk::TreeView* tv;
		Gtk::ScrolledWindow* scwin;
		KineticScroll kscroll;
	}
	
	MyClass::MyClass()
	{
		...
		kscroll.set_adjustment(scwin->get_vadjustment());
		kscroll.set_motion_event(tv);
	}
\endcode
*/
class KineticScroll
{
	public:

		KineticScroll();
		~KineticScroll();


		/*! плавная прокрутка до заданного места */
 		void set_target( int pos, double sliding=1.1, double start_kinetic=1.2 );

		void set_orientation( Gtk::Orientation o );

		void set_adjustment( Gtk::Adjustment* adj );
		void set_motion_event( Gtk::Widget* w );
		
		inline Gtk::Adjustment* get_adjustment(){ return vadj; }
		
		/*! минимальное ускорение, движение после которого прекращается */
		inline void set_min_kinetic( double s ){ min_kinetic = s; }

		/*! максимально допустимое ускорение */
		inline void set_max_kinetic( double s ){ max_kinetic = s; }

		/*! коэффициент скольжения [0...1] / <1 - торможение, >1 - разгон */
		inline void set_k_sliding( double s ){ k_sliding = s; }

		/*! мсек, шаг для кинетического таймера */
		void set_k_timer( int msec );

		enum KineticEventID
		{
			keBeginScroll,
			keEndScroll,
			keScroll
		};
		friend std::ostream& operator<<( std::ostream& os, KineticEventID i );

		typedef sigc::signal<void,KineticEventID,int> KineticEvent_Signal;

		KineticEvent_Signal signal_kinetic_event();

		inline void set_kinetic_scroll( bool set ){ kinetic_on = set; }
		inline void set_scroll( bool set ){ scroll_on = set; }

	protected:

		bool on_motion_event(GdkEvent *event);
		bool kinetic_timer();
		bool target_kinetic_timer();
		void set_current_position( int pos );

		Gtk::Orientation orient;
		Gtk::Adjustment* vadj;
		Gtk::Widget* widget;
		double prev_x;		
		double prev_y;
		double prev_v;
		gint32 prev_t;
		double prev_dz;
		bool but_pressed;
		double kinetic;		/*!< накопленное ускорение */
		double min_kinetic; /*!< минимальное ускорение, движение после которого прекращается */
		double max_kinetic; /*!< максимально допустимое ускорение */
		double k_sliding;   /*!< коэффициент скольжения [0...1] / <1 - торможение, >1 - разгон */
		sigc::connection k_timer;
		int k_timer_step;	/*!< мсек, шаг для кинетического таймера */
		double k_const;  	/*!< постоянный коэффициент в расчёте перемещения (оптимизация) */
		
		double max_a; /*!< максимальное ускорение */
		double min_dz;
		bool kinetic_on;
		bool scroll_on;

		KineticEvent_Signal k_event;

		sigc::connection k_target_timer;
		double k_target_sliding;
		double target_pos;
		int target_sign;

	private:

};
// -------------------------------------------------------------------------
#endif // KineticScroll_H_
// -------------------------------------------------------------------------
