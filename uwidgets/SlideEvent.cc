#include <iomanip>
#include <iostream>
#include <cmath>
#include "SlideEvent.h"
// -------------------------------------------------------------------------
using namespace std;
// -------------------------------------------------------------------------
SlideEvent::SlideEvent():
	widget(0),
	prev_x(0),
	prev_y(0),
	prev_t(0),
	but_pressed(false),
	w_zone(10),
	way_length(70)
{
}
// -------------------------------------------------------------------------
SlideEvent::~SlideEvent()
{
}
// -------------------------------------------------------------------------
std::ostream& operator<<( std::ostream& os, SlideEvent::SlideEventID i )
{
	switch(i)
	{
		case SlideEvent::toLeftSlideEvent:	
			os << "toLeftSlideEvent";
		break;
		case SlideEvent::toRightSlideEvent:	
			os << "toRightSlideEvent";
		break;
		case SlideEvent::toUpSlideEvent:	
			os << "toUpSlideEvent";
		break;
		case SlideEvent::toDownSlideEvent:	
			os << "toDownSlideEvent";
		break;
		default:
			os << "UnknownSlideID";
		break;
	}

	return os;
}
// -------------------------------------------------------------------------
SlideEvent::SlideEvent_Signal SlideEvent::signal_slide_event()
{
	return s_event;
}
// -------------------------------------------------------------------------
SlideEvent::SlideVoidEvent_Signal SlideEvent::signal_toleft_slide_event()
{
	return l_event;
}
// -------------------------------------------------------------------------
SlideEvent::SlideVoidEvent_Signal SlideEvent::signal_toright_slide_event()
{
	return r_event;
}
// -------------------------------------------------------------------------
SlideEvent::SlideVoidEvent_Signal SlideEvent::signal_toup_slide_event()
{
	return u_event;
}
// -------------------------------------------------------------------------
SlideEvent::SlideVoidEvent_Signal SlideEvent::signal_todown_slide_event()
{
	return d_event;
}
// -------------------------------------------------------------------------
void SlideEvent::set_widget( Gtk::Widget& w )
{
	w.signal_event().connect(sigc::mem_fun(*this, &SlideEvent::on_motion_event));
	widget = &w;
}
// -------------------------------------------------------------------------
bool SlideEvent::on_motion_event( GdkEvent *event )
{
	if( !event )
		return false;

	if( event->type == GDK_FOCUS_CHANGE || event->type == GDK_VISIBILITY_NOTIFY )
	{
		but_pressed = false;
		return false;
	}

	if( event->type == GDK_BUTTON_PRESS )
	{
		if( but_pressed )
			return false;

		but_pressed = true;
		// запоминаем значения
		Gdk::Event ev(event);
		ev.get_coords(prev_x,prev_y); // ev.get_root_coords();
		prev_t = ev.get_time();
	}
	else if( event->type == GDK_BUTTON_RELEASE )
	{
		but_pressed = false;
		return false;
	}
	else if( !but_pressed || event->type != GDK_MOTION_NOTIFY )
		return false;

	Gdk::Event ev(event);
	guint32 t = ev.get_time();
	if( t == 0 )
		return false; // видимо другое событие

#if 0		
	guint32 dt = (t - prev_t);
	prev_t = t;
	
//	if( fabs(dt) > 40 )
//		return false;
#endif
	
	double x=0,y=0;
	ev.get_coords(x,y); //	ev.get_root_coords(x_r,y_r);
		
	double dy = y - prev_y;
	double dx = x - prev_x;

#if 0
	cerr << "x=" << setw(5) << x 
		<< " dx=" << setw(5) << dx 
		<< " y=" << setw(5) << y 
		<< " dy=" << setw(5) << dy 
		<< endl;
#endif

	// Ловим движения Left и Rigth
	if( fabs(dx) > way_length )
	{
		// "путь пройден" пора действовать

		// сперва сбросим "обработку"
		but_pressed = false;

		// а теперь обработаем
		if( fabs(dy) < w_zone )
		{
			if( dx > 0 )
			{
				s_event.emit(toRightSlideEvent);
				r_event.emit();
			}
			else if( dx < 0 )
			{
				s_event.emit(toLeftSlideEvent);
				l_event.emit();
			}
		}
	}

	// Ловим движения Left и Rigth
	if( fabs(dy) > way_length )
	{
		// "путь пройден" пора действовать

		// сперва сбросим "обработку"
		but_pressed = false;

		// а теперь обработаем
		if( fabs(dx) < w_zone )
		{
			if( dy > 0 )
			{
				s_event.emit(toDownSlideEvent);
				d_event.emit();
			}
			else if( dy < 0 )
			{
				s_event.emit(toUpSlideEvent);
				u_event.emit();
			}
		}
	}

	return false;
}
// -------------------------------------------------------------------------
