/*! \author Pavel Vaynerman <pv@etersoft.ru> */
// -------------------------------------------------------------------------
#ifndef SlideEvent_H_
#define SlideEvent_H_
// -------------------------------------------------------------------------
#include <ostream>
#include <gtkmm.h>
// -------------------------------------------------------------------------
/*! Реализация событий типа slide (вертикальный и горизонтальный, влево и вправо).
	Это универсальная, подключающаяся к любым Gtk::Widget обёртка, которая генерирует события
"Slide в лево", "Slide в право", "Slide в верх", "Slide вниз", позволяющая подключится,
к соответствующему сигналу и уже обрабатывать по своему усмотрению...
	\par Пример использования
\code
	class MyWidget
	{
		MyWidget()
		{
			...
			sev.set_widget(*this);
			sev.signal_slide_event().connect( sigc::mem_fun(*this, &MyWidget::slide_event) );
			...
		}

		protected:
			SlideEvent sev;

			void slide_event( SlideEvent::SlideEventID i )
			{
				cout << "SlideEvent: " << i << endl;
			}
		
	} 
\endcode

	\par Пример использования (подключение к стороннему виджету )
\code
	class MyWidget
	{
		MyWidget()
		{
			...
			sev.set_widget(w);
			sev.signal_slide_event().connect( sigc::mem_fun(*this, &MyWidget::slide_event) );
			...
		}

		protected:
			OtherWidget w;
			SlideEvent sev;

			void slide_event( SlideEvent::SlideEventID i )
			{
				cout << "SlideEvent: " << i << endl;
			}
	}
\endcode
*/
class SlideEvent
{
	public:

		SlideEvent();
		~SlideEvent();

		void set_widget( Gtk::Widget& w );
		
		/*! разрешённая ширина зоны в которой движение будет считаться корректным	
			(по сути насколько разрешено отклонится при движении "указателя")
		*/
		inline void set_w_zone( double w ){ w_zone = w; }

		/*! длина "пути", который надо провести ""указателем", чтобы сработало событие */
		inline void set_way_length( double l ){ way_length = l; }

		enum SlideEventID
		{
			toLeftSlideEvent,  /*!< горизонтальный slide (rigth to left) */
			toRightSlideEvent, /*!< горизонтальный slide (left to right) */
			toDownSlideEvent,  /*!< вертикальный slide ( up to down ) */
			toUpSlideEvent     /*!< вертикальный slide ( down to up ) */
		};
		friend std::ostream& operator<<( std::ostream& os, SlideEventID i );

		typedef sigc::signal<void,SlideEventID> SlideEvent_Signal;

		SlideEvent_Signal signal_slide_event();

		// для удобства можно подключится к конкретному событию
		typedef sigc::signal<void> SlideVoidEvent_Signal;
		SlideVoidEvent_Signal signal_toleft_slide_event();
		SlideVoidEvent_Signal signal_toright_slide_event();
		SlideVoidEvent_Signal signal_toup_slide_event();
		SlideVoidEvent_Signal signal_todown_slide_event();

	protected:

		bool on_motion_event(GdkEvent *event);
		Gtk::Widget* widget;
		
		double prev_x;
		double prev_y;
		gint32 prev_t;

		bool but_pressed;
		double w_zone;
		double way_length;

		SlideEvent_Signal s_event;
		SlideVoidEvent_Signal l_event;
		SlideVoidEvent_Signal r_event;
		SlideVoidEvent_Signal u_event;
		SlideVoidEvent_Signal d_event;

	private:
};
// -------------------------------------------------------------------------
#endif // SlideEvent_H_
// -------------------------------------------------------------------------
