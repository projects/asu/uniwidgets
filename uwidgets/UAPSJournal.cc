#include <CheckedSignal.h>
#include <ConfirmSignal.h>
#include <ports.h>
#include <types.h>
#include "UAPSJournal.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
using namespace UMessages;
using Glib::ustring;
// -------------------------------------------------------------------------
#define UAPSJOURNAL_PROPERTY_INIT() \
	sensor_prop_newAPS(this,"newAPS",get_connector()) \
	,property_newAPS_timeout( *this, "newAPS-reset-timeout", 500) \
	,sensor_prop_haveAPS(this,"haveAPS",get_connector()) \
	,property_pic_title( *this, "pic-title", "Pic") \
	,property_time_title( *this, "time-title", "Time") \
	,property_text_title( *this, "text-title", "Text") \
	,property_confirm_title( *this, "confirm-title", "Confirm") \
	,property_pic_width( *this, "pic-width", 32 ) \
	,property_time_width( *this, "time-width", 74 ) \
	,property_text_width( *this, "text-width", 500 ) \
	,property_confirm_width( *this, "confirm-width", 74 ) \
	,property_info_pic( *this, "info-pic", "info.svg") \
	,property_warn_pic( *this, "warn-pic", "warning.svg") \
	,property_alarm_pic( *this, "alarm-pic", "alarm.svg") \
	,property_confirm1_pic( *this, "confirm1-pic", "confirm1.svg") \
	,property_confirm2_pic( *this, "confirm2-pic", "confirm2.svg") \
	,property_confirmed_pic( *this, "confirmed-pic", "confirmed.svg") \
	,property_msgFont(*this, "msg-font", "Liberation Sans 14") \
	,property_info_color( *this, "info-color", Gdk::Color("white")  ) \
	,property_warn_color( *this, "warn-color", Gdk::Color("yellow") ) \
	,property_alarm_color( *this, "alarm-color", Gdk::Color("red") ) \
	,property_bg_first_color   (*this, "bg-first-color",Gdk::Color("#5a5a5a") ) \
	,property_bg_second_color   (*this, "bg-second-color",Gdk::Color("#373737") ) \
	,property_blinkNewMsg       (*this, "blink-new-msg",  true ) \
	,property_blinkNewMsgCount  (*this, "blink-new-msg-count",  10 ) \
	,property_blinkNewMsgTime   (*this, "blink-new-msg-time",  500 ) \
	,prop_enableGroups(*this, "enable-group-of-msg", true) \
	,prop_groupOfMsg(*this, "group-of-msg", "") \
	
// -------------------------------------------------------------------------
void UAPSJournal::ctor()
{

	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback<UAPSJournal>;

	current_id = DefaultObjectId;
	scrolled_window_.add(tree_view_);
	scrolled_window_.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	add(scrolled_window_);

	m_refTreeModel = Gtk::ListStore::create(m_Columns);
	tree_view_.set_model(m_refTreeModel);
	tree_view_.get_selection()->set_mode(Gtk::SELECTION_NONE);

	//Picture
	{
	Gtk::CellRendererPixbuf* pRenderer = Gtk::manage( new Gtk::CellRendererPixbuf() );
	tree_view_.append_column("Pic", *pRenderer);
	Gtk::TreeViewColumn* pColumn = tree_view_.get_column(0);
	pColumn->add_attribute(pRenderer->property_pixbuf(), m_Columns.icon);
	pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_FIXED);
	pColumn->set_fixed_width(32);
	pColumn->set_cell_data_func(*pRenderer, sigc::mem_fun(*this, &UAPSJournal::on_set_pix_value) );
	}

	//Time
	{
	Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
	tree_view_.append_column("Time", *pRenderer);
	Gtk::TreeViewColumn* pColumn = tree_view_.get_column(1);
	pColumn->add_attribute(pRenderer->property_text(), m_Columns.timestring);
	pColumn->add_attribute(pRenderer->property_background_gdk(),m_Columns.bgcolor);
	pColumn->add_attribute(pRenderer->property_foreground_gdk(),m_Columns.fgcolor);
	pColumn->add_attribute(pRenderer->property_font(),m_Columns.msg_font);
	pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_FIXED);
	pColumn->set_fixed_width(84);
	}

	//Text
	{
	Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
	pRenderer->property_wrap_mode() = Pango::WRAP_WORD;
	pRenderer->property_wrap_width() = property_text_width;
	tree_view_.append_column("Text", *pRenderer);
	Gtk::TreeViewColumn* pColumn = tree_view_.get_column(2);
	pColumn->add_attribute(pRenderer->property_text(), m_Columns.textmsg);
	pColumn->add_attribute(pRenderer->property_background_gdk(),m_Columns.bgcolor);
	pColumn->add_attribute(pRenderer->property_foreground_gdk(),m_Columns.fgcolor);
	pColumn->add_attribute(pRenderer->property_font(),m_Columns.msg_font);
	pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
	pColumn->set_expand(true);
// 	pRenderer->property_ellipsize() = Pango::ELLIPSIZE_END;
// 	pRenderer->property_ellipsize_set() = true;
	}

	//Confirm Time
	{
	Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
	tree_view_.append_column("Confirm", *pRenderer);
	Gtk::TreeViewColumn* pColumn = tree_view_.get_column(3);
	pColumn->add_attribute(pRenderer->property_text(), m_Columns.confirm_timestring);
	pColumn->add_attribute(pRenderer->property_background_gdk(),m_Columns.bgcolor);
	pColumn->add_attribute(pRenderer->property_foreground_gdk(),m_Columns.fgcolor);
	pColumn->add_attribute(pRenderer->property_font(),m_Columns.msg_font);
	pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_FIXED);
	pColumn->set_fixed_width(84);
	}

	on_info_pic_changed();
	on_warn_pic_changed();
	on_alarm_pic_changed();
	on_confirm1_pic_changed();
	on_confirm2_pic_changed();
	on_confirmed_pic_changed();
	on_text_width_changed();

	connect_property_changed("pic-title", sigc::mem_fun(*this, &UAPSJournal::on_pic_title_changed) );
	connect_property_changed("time-title", sigc::mem_fun(*this, &UAPSJournal::on_time_title_changed) );
	connect_property_changed("text-title", sigc::mem_fun(*this, &UAPSJournal::on_text_title_changed) );
	connect_property_changed("confirm-title", sigc::mem_fun(*this, &UAPSJournal::on_confirm_title_changed) );
	connect_property_changed("pic-width", sigc::mem_fun(*this, &UAPSJournal::on_pic_width_changed) );
	connect_property_changed("text-width", sigc::mem_fun(*this, &UAPSJournal::on_text_width_changed));
	connect_property_changed("time-width", sigc::mem_fun(*this, &UAPSJournal::on_time_width_changed) );
	connect_property_changed("confirm-width", sigc::mem_fun(*this, &UAPSJournal::on_confirm_width_changed) );
	connect_property_changed("info-pic", sigc::mem_fun(*this, &UAPSJournal::on_info_pic_changed) );
	connect_property_changed("warn-pic", sigc::mem_fun(*this, &UAPSJournal::on_warn_pic_changed) );
	connect_property_changed("alarm-pic", sigc::mem_fun(*this, &UAPSJournal::on_alarm_pic_changed) );
	connect_property_changed("confirm1-pic", sigc::mem_fun(*this, &UAPSJournal::on_confirm1_pic_changed) );
	connect_property_changed("confirm2-pic", sigc::mem_fun(*this, &UAPSJournal::on_confirm2_pic_changed) );
	connect_property_changed("confirmed-pic", sigc::mem_fun(*this, &UAPSJournal::on_confirmed_pic_changed) );
	connect_property_changed("msg-font", sigc::mem_fun(*this, &UAPSJournal::on_msg_font_changed) );
	connect_property_changed("blink-new-msg-time", sigc::mem_fun(*this, &UAPSJournal::on_blink_new_msg_time_changed) );
	connect_property_changed("group-of-msg", sigc::mem_fun(*this, &UAPSJournal::on_group_of_msg_changed) );
	
	if (get_property_disconnect_effect() == 2) {
		scrolled_window_.set_sensitive(false);
		tree_view_.set_sensitive(false);
	}

    kscroll.set_adjustment(scrolled_window_.get_vadjustment());
    kscroll.set_motion_event(&tree_view_);

	show_all_children();
}
// -------------------------------------------------------------------------
UAPSJournal::UAPSJournal() :
	Glib::ObjectBase("uapsjournal")
	,UAPSJOURNAL_PROPERTY_INIT()
{
	ctor();
}
// -------------------------------------------------------------------------
UAPSJournal::UAPSJournal(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,UAPSJOURNAL_PROPERTY_INIT()
{
	ctor();
}
// -------------------------------------------------------------------------
UAPSJournal::~UAPSJournal()
{
}
// -------------------------------------------------------------------------
void UAPSJournal::on_realize()
{
	UEventBox::on_realize();
	on_msg_font_changed();
	on_blink_new_msg_time_changed();
}
// -------------------------------------------------------------------------
bool UAPSJournal::on_search_noconfiredID(const Gtk::TreeIter& it,
				    const Gtk::TreeRow& row,
				    const UMessages::Message& message)
{
	const Glib::ustring ctm = (*it)[m_Columns.confirm_timestring];
	if ( ctm.empty() && (*it)[m_Columns.wtype] != 0 && (*it)[m_Columns.wtype] != 3 && it != row ) {
		has_any_not_confirmed_id = true;
		if(MessageId((*it)[m_Columns.id]) == message.getMessageId())
			return true;
	}
	return false;
}
// -------------------------------------------------------------------------
bool UAPSJournal::newAPSReset()
{
	sensor_prop_newAPS.save_value(false);
	return false;
}
// -------------------------------------------------------------------------
void UAPSJournal::process_message(const UMessages::Message& message)
{
	if (message.getMessageType() != 1 &&
		message.getMessageType() !=2) //don't process info messages
		return;

	bool show_new_msg = false;
	if(prop_enableGroups && !msg_groups.empty())
	{
		std::list<Glib::ustring> msg_group_list = explodestr(message.getMessageId()._msg_groups,',');
		for(std::list<Glib::ustring>::iterator group_it = msg_group_list.begin(); group_it != msg_group_list.end(); ++group_it)
		{
			if(msg_groups.find(*group_it)!=msg_groups.end())
			{
				show_new_msg = true;
				break;
			}
		}
	}
	else
		show_new_msg = true;
	
	if(!show_new_msg)
		return;
	
	if(property_newAPS_timeout.get_value() > 0)
	{
		newAPS_conn.disconnect();
		newAPS_conn = Glib::signal_timeout().connect(sigc::mem_fun(*this, &UAPSJournal::newAPSReset),property_newAPS_timeout.get_value());
	}
	sensor_prop_newAPS.save_value(true);
	sensor_prop_haveAPS.save_value(2);
	
	Gtk::TreeRow row = *(m_refTreeModel->append());

	ustring dtime, ttime;
	get_time_str(dtime,ttime,message.getLastTime());
	row[m_Columns.time] = message.getLastTime();
	row[m_Columns.timestring] = ttime;

	row[m_Columns.id] = message.getMessageId();
	row[m_Columns.textmsg] = message.getText();
	row[m_Columns.wtype] = message.getMessageType();
	row[m_Columns.msg_font] = get_property_msgFont();
	int num = int(m_refTreeModel->children().size());
	if(num%2)
		row[m_Columns.bgcolor] = get_property_bg_first_color();
	else
		row[m_Columns.bgcolor] = get_property_bg_second_color();

	if (message.getMessageType() == 2 ) {
//		row[m_Columns.bgcolor] = Gdk::Color("#282828");
		row[m_Columns.fgcolor] = property_alarm_color.get_value();
		iconPixbufMap[APSIconKey(row,m_Columns)] = refPixAlarm;
	} else if (message.getMessageType() == 1) {
//		row[m_Columns.bgcolor] = Gdk::Color("#282828");
		row[m_Columns.fgcolor] = property_warn_color.get_value();
		iconPixbufMap[APSIconKey(row,m_Columns)] = refPixWarn;
	}

	if(property_blinkNewMsg)
	{
		row[m_Columns.blinkState] = blinkState;
		row[m_Columns.blinkCount] = 1;
		
		if(blinkState)
		{
			Gdk::Color bg,fg;
			bg = row[m_Columns.fgcolor];
			fg = row[m_Columns.bgcolor];
			row[m_Columns.fgcolor] = fg;
			row[m_Columns.bgcolor] = bg;
		}
		
		if(property_blinkNewMsgTime.get_value()>0 && !blink_msg_connection_.connected())
		{
			blink_msg_connection_= Glib::signal_timeout().connect(sigc::mem_fun(*this, &UAPSJournal::on_blink_msg_timer),property_blinkNewMsgTime.get_value());
		}
	}
	else
	{
		row[m_Columns.blinkState] = false;
		row[m_Columns.blinkCount] = 0;
	}

	//Search if we have same messages are waiting confirm.
	//If string with confirm time empty, we decide messages
	//wasn't confirmed
	has_any_not_confirmed_id = false;
	Gtk::TreeIter it = std::find_if(m_refTreeModel->children().begin(),
					m_refTreeModel->children().end(),
					sigc::bind(sigc::mem_fun(this,&UAPSJournal::on_search_noconfiredID),row,message));

	if (it == m_refTreeModel->children().end()) {
		if (!has_any_not_confirmed_id)
			set_pointer(message.getMessageId());
		connect_confirm(message.getMessageId());
	}

 	tree_view_.scroll_to_row( Gtk::TreePath(row) );
}
// -------------------------------------------------------------------------
bool UAPSJournal::on_search_noconfired(const Gtk::TreeIter& it)
{
	const Glib::ustring ctm = (*it)[m_Columns.confirm_timestring];
	if ( ctm.empty() && (*it)[m_Columns.wtype] != 0 && (*it)[m_Columns.wtype] != 3 )
	{
		has_any_not_confirmed_aps = true;
		return true;
	}

	has_any_not_confirmed_aps = false;
	return false;
}
// -------------------------------------------------------------------------
void
UAPSJournal::confirm(UMessages::MessageId id, time_t sec)
{
	sensor_prop_newAPS.save_value(false);
	
	Gtk::TreeIter found_row = m_refTreeModel->children().end();
	if (get_connector()->signal_confirm().get_message_status(id) == DROPPED)
	{
		Gtk::TreeIter it = m_refTreeModel->children().begin();
		remove_messages(id,it);
	}
	else {
		/**
		 * TODO: this block needs optimisation.
		 */
		Gtk::TreeIter it = std::find_if( m_refTreeModel->children().begin(),
							 m_refTreeModel->children().end(),
							sigc::bind(sigc::mem_fun(this,&UAPSJournal::on_search_ID),id));
		Gtk::TreeIter found_row = it++;
		remove_messages(id,it);//Удаляем все сообщения с id начинаю со следующего после найденного

		ustring dtime,ttime;
		time_t diff = sec - (*found_row)[m_Columns.time];
		get_gmtime_str(dtime,ttime,diff);
		(*found_row)[m_Columns.confirm_timestring] = ttime;
		iconPixbufMap[APSIconKey(*found_row,m_Columns)] = refPixConfirmed;

		(*found_row)[m_Columns.blinkCount] = 0;
		if(property_blinkNewMsg)
		{
			if((*found_row)[m_Columns.blinkState])
			{
				Gdk::Color bg,fg;
				bg = (*found_row)[m_Columns.fgcolor];
				fg = (*found_row)[m_Columns.bgcolor];
				(*found_row)[m_Columns.fgcolor] = fg;
				(*found_row)[m_Columns.bgcolor] = bg;
				
				(*found_row)[m_Columns.blinkState] = false;
			}
		}
		
		USignals::VConn* conn = new USignals::VConn;
		sigc::slot<void, ObjectId, ObjectId, long> drop =
			sigc::hide(sigc::hide(sigc::hide(
				sigc::bind(sigc::mem_fun( this, &UAPSJournal::value_out_proc ), id, conn))));
		*conn  = get_connector()->signals().connect_value_out( drop, id._id, id._node, id._value);
	}

	/* Here we looking for next entry need confirm */
	Gtk::TreeIter next = std::find_if(m_refTreeModel->children().begin(),
					  m_refTreeModel->children().end(),
					  sigc::mem_fun(this,&UAPSJournal::on_search_noconfired));
	if ( next != m_refTreeModel->children().end() ) {
		MessageId m_id = (*next)[m_Columns.id];
		set_pointer(m_id);
	 	tree_view_.scroll_to_row( Gtk::TreePath(next));
	}
	if(has_any_not_confirmed_aps)
		sensor_prop_haveAPS.save_value(true);
	else
		sensor_prop_haveAPS.save_value(false);
}
// -------------------------------------------------------------------------
bool UAPSJournal::on_search_ID( const Gtk::TreeIter& it,
				                const MessageId& id)
{
  if( MessageId((*it)[m_Columns.id]) == id )
		return true;
	return false;
}
// -------------------------------------------------------------------------
void UAPSJournal::remove_messages(MessageId id,Gtk::TreeIter& start)
{
    while ( start != m_refTreeModel->children().end() )
        if ( MessageId((*start)[m_Columns.id]) == id )
	{
		Gdk::Color bg_1,bg_2;
		Gdk::Color bg,fg;
		if(property_blinkNewMsg)
		{
			bg = (*start)[m_Columns.bgcolor];
			fg = (*start)[m_Columns.fgcolor];
			if( !(*start)[m_Columns.blinkState] )
			{
				if(bg == get_property_bg_first_color())
				{
					bg_1 = get_property_bg_first_color();
					bg_2 = get_property_bg_second_color();
				}
				else if(bg == get_property_bg_second_color())
				{
					bg_1 = get_property_bg_second_color();
					bg_2 = get_property_bg_first_color();
				}
			}
			else
			{
				if(fg == get_property_bg_first_color())
				{
					bg_1 = get_property_bg_first_color();
					bg_2 = get_property_bg_second_color();
				}
				else if(fg == get_property_bg_second_color())
				{
					bg_1 = get_property_bg_second_color();
					bg_2 = get_property_bg_first_color();
				}
			}
		}
		else
		{
			if((*start)[m_Columns.bgcolor] == get_property_bg_first_color())
			{
				bg_1 = get_property_bg_first_color();
				bg_2 = get_property_bg_second_color();
			}
			else if((*start)[m_Columns.bgcolor] == get_property_bg_second_color())
			{
				bg_1 = get_property_bg_second_color();
				bg_2 = get_property_bg_first_color();
			}
		}

		start = m_refTreeModel->erase((*start));
		
		int num=1;
		for( Gtk::TreeIter it = start; it != m_refTreeModel->children().end(); ++it )
		{
			if(num%2)
			{
				if(property_blinkNewMsg)
				{
					bg = (*it)[m_Columns.bgcolor];
					fg = (*it)[m_Columns.fgcolor];
					if( !(*it)[m_Columns.blinkState] )
					{
						(*it)[m_Columns.bgcolor] = bg_1;
					}
					else
						(*it)[m_Columns.fgcolor] = bg_1;
				}
				else
					(*it)[m_Columns.bgcolor] = bg_1;
			}
			else
			{
				if(property_blinkNewMsg)
				{
					bg = (*it)[m_Columns.bgcolor];
					fg = (*it)[m_Columns.fgcolor];
					if( !(*it)[m_Columns.blinkState] )
					{
						(*it)[m_Columns.bgcolor] = bg_2;
					}
					else
						(*it)[m_Columns.fgcolor] = bg_2;
				}
				else
					(*it)[m_Columns.bgcolor] = bg_2;
			}
			++num;
		}
	}
        else
            start++;
}
// -------------------------------------------------------------------------
bool UAPSJournal::on_blink_msg_timer()
{
	if(!property_blinkNewMsg)
		return false;
	
	blinkState = !blinkState;
	
	//	cout<<get_name()<<"::on_blink_msg_timer()"<<endl;
	
//	//один из вариантов проверки на мигание
// 	std::for_each(m_refTreeModel->children().begin(), m_refTreeModel->children().end(),sigc::mem_fun(this,&UAPSJournal::on_foreach_blink));
// 	Gtk::TreeIter continue_blink = std::find_if(m_refTreeModel->children().begin(),m_refTreeModel->children().end(),sigc::mem_fun(this,&UAPSJournal::on_search_blinked));
// 	return continue_blink != m_refTreeModel->children().end();
	
	//другой вариант проверки на мигание
	bool continue_blink = false;
	for(Gtk::TreeIter it = m_refTreeModel->children().begin();it!=m_refTreeModel->children().end();++it)
	{
		const int count = (*it)[m_Columns.blinkCount];
		if(count > 0 && count <= get_property_blinkNewMsgCount()*2)
		{
			if( (*it)[m_Columns.blinkState] != blinkState )
			{
				Gdk::Color bg,fg;
				bg = (*it)[m_Columns.fgcolor];
				fg = (*it)[m_Columns.bgcolor];
				(*it)[m_Columns.fgcolor] = fg;
				(*it)[m_Columns.bgcolor] = bg;
				
				(*it)[m_Columns.blinkState] = blinkState;
				(*it)[m_Columns.blinkCount] = count+1;
			}
			
			if( count > get_property_blinkNewMsgCount()*2 )
			{
				(*it)[m_Columns.blinkCount] = 0;
				if(!(*it)[m_Columns.blinkState])
					continue_blink = continue_blink|false;
				else
					continue_blink = true;
			}
			else
				continue_blink = true;
		}
		else if((*it)[m_Columns.blinkState])
		{
			Gdk::Color bg,fg;
			bg = (*it)[m_Columns.fgcolor];
			fg = (*it)[m_Columns.bgcolor];
			(*it)[m_Columns.fgcolor] = fg;
			(*it)[m_Columns.bgcolor] = bg;
			
			(*it)[m_Columns.blinkState] = false;
			
			continue_blink = continue_blink|false;
		}
	}
	if(!continue_blink)
		blink_msg_connection_.disconnect();
	
	return continue_blink;
}
// -------------------------------------------------------------------------
void UAPSJournal::on_foreach_blink(const Gtk::TreeIter& it)
{
	const int count = (*it)[m_Columns.blinkCount];
	if(count > 0 && count <= get_property_blinkNewMsgCount()*2)
	{
		Gdk::Color bg,fg;
		
//		cout<<get_name()<<"::on_foreach_blink() it="<<it<<endl;
		
		bg = (*it)[m_Columns.fgcolor];
		fg = (*it)[m_Columns.bgcolor];
		
		(*it)[m_Columns.fgcolor] = fg;
		(*it)[m_Columns.bgcolor] = bg;
		
		(*it)[m_Columns.blinkCount] = count+1;
		
		if(count > get_property_blinkNewMsgCount()*2)
			(*it)[m_Columns.blinkCount] = 0;
	}
}
// -------------------------------------------------------------------------
bool UAPSJournal::on_search_blinked(const Gtk::TreeIter& it)
{
	const int count = (*it)[m_Columns.blinkCount];
	if ( count > 0 )
		return true;
	
	return false;
}
// -------------------------------------------------------------------------
void UAPSJournal::value_out_proc(MessageId id, USignals::VConn* conn)
{
	Gtk::TreeIter it = m_refTreeModel->children().begin();
	remove_messages(id,it);
	conn->disconnect();
	delete conn;
}
// -------------------------------------------------------------------------
void UAPSJournal::on_set_pix_value(Gtk::CellRenderer* renderer, const Gtk::TreeModel::iterator& iter)
{
	Gtk::CellRendererPixbuf* pRenderer = dynamic_cast<Gtk::CellRendererPixbuf*>( renderer );
	assert(pRenderer != nullptr);

	pRenderer->property_pixbuf() = iconPixbufMap[APSIconKey(*iter,m_Columns)];
	//cerr << "UJournal::on_set_pix_value() " << temp_id << endl;
}
// -------------------------------------------------------------------------
void UAPSJournal::on_blink_foreach(const Gtk::TreeIter& it,
                                const MessageId& id,
                                const Glib::RefPtr<Gdk::Pixbuf>& pix)
{
	const Glib::ustring ctm = (*it)[m_Columns.confirm_timestring];
	if ( MessageId((*it)[m_Columns.id]) == id && ctm.empty() )
	{
		iconPixbufMap[APSIconKey(*it,m_Columns)] = pix;
		Gdk::Rectangle  cell;
		Gtk::TreeViewColumn* pColumn = tree_view_.get_column(0);
		tree_view_.get_cell_area(m_refTreeModel->get_path(it), *pColumn, cell);
		//cerr << cell.get_x() << ":" << cell.get_y() << " " << cell.get_width() << ":" << cell.get_height() << endl;
#warning hack пока непонятно как расчитать высоту заголовка таблицы
		int height = tree_view_.get_headers_visible () ? ( cell.get_height() + cell.get_height() ) : cell.get_height();
		tree_view_.queue_draw_area (cell.get_x(), cell.get_y(), cell.get_width(), height);
	}
}
// -------------------------------------------------------------------------
void UAPSJournal::blink(bool blink_state, int time, MessageId id)
{
	if(!is_mapped())
		return;

	Gtk::TreePath path;
	Gtk::TreePath end_path;
	bool rv = tree_view_.get_visible_range(path, end_path);

	if (rv == false)
		return;

	Glib::RefPtr<Gdk::Pixbuf> current_pixbuf =
		blink_state ? refPixConfirm1 : refPixConfirm2;

	std::for_each(m_refTreeModel->get_iter(path),
                  ++m_refTreeModel->get_iter(end_path),
                  sigc::bind(sigc::mem_fun(this,&UAPSJournal::on_blink_foreach),id,current_pixbuf));
}
// -------------------------------------------------------------------------
void
UAPSJournal::set_pointer( MessageId id )
{
	blink_conn.disconnect();
	sigc::slot<void, bool, int> blinkslot=
		sigc::bind( sigc::mem_fun(this, &UAPSJournal::blink), id );
	blink_conn = blinker.signal_blink[DEFAULT_BLINK_TIME].connect(blinkslot);
}
// -------------------------------------------------------------------------
void
UAPSJournal::connect_confirm( MessageId id )
{
	sigc::slot<void, MessageId, time_t> confirm = sigc::mem_fun( this, &UAPSJournal::confirm );
	get_connector()->signal_confirm().connect( confirm, id );
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_pic_title_changed()
{
	tree_view_.get_column(0)->set_title( property_pic_title );
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_time_title_changed()
{
	tree_view_.get_column(1)->set_title( property_time_title );
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_text_title_changed()
{
	tree_view_.get_column(2)->set_title( property_text_title );
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_confirm_title_changed()
{
	tree_view_.get_column(3)->set_title( property_confirm_title );
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_pic_width_changed()
{
	tree_view_.get_column(0)->set_fixed_width( property_pic_width);
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_time_width_changed()
{
	tree_view_.get_column(1)->set_fixed_width( property_time_width);
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_text_width_changed()
{
	Gtk::CellRendererText* pCellRendererText = dynamic_cast<Gtk::CellRendererText*>(tree_view_.get_column_cell_renderer(2));
// 	pCellRendererText->property_wrap_mode() = Pango::WRAP_WORD;
	pCellRendererText->property_wrap_width() = property_text_width;
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_confirm_width_changed()
{
	tree_view_.get_column(3)->set_fixed_width( property_confirm_width);
}
// -------------------------------------------------------------------------
void
UAPSJournal::try_load_pic( Glib::RefPtr<Gdk::Pixbuf>& refPix, std::string pic )
{
	try {
// 		refPix = Gdk::Pixbuf::create_from_file( pic );
		refPix = get_pixbuf_from_cache( pic, property_pic_width, property_pic_width);
	} catch ( ... ) {
		cerr << "(" << get_name() << "): Error while loading " << pic << endl;
	}
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_info_pic_changed()
{
	try_load_pic(refPixInfo, property_info_pic);
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_warn_pic_changed()
{
	try_load_pic(refPixWarn, property_warn_pic);
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_alarm_pic_changed()
{
	try_load_pic(refPixAlarm, property_alarm_pic);
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_confirm1_pic_changed()
{
	try_load_pic(refPixConfirm1, property_confirm1_pic);
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_confirm2_pic_changed()
{
	try_load_pic(refPixConfirm2, property_confirm2_pic);
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_confirmed_pic_changed()
{
	try_load_pic(refPixConfirmed, property_confirmed_pic);
}
// -------------------------------------------------------------------------
void UAPSJournal::on_msg_font_changed()
{
	dynamic_cast<Gtk::CellRendererText*>(tree_view_.get_column_cell_renderer(1))->property_font() = property_msgFont;
	dynamic_cast<Gtk::CellRendererText*>(tree_view_.get_column_cell_renderer(2))->property_font() = property_msgFont;
	dynamic_cast<Gtk::CellRendererText*>(tree_view_.get_column_cell_renderer(3))->property_font() = property_msgFont;
}
// -------------------------------------------------------------------------
void UAPSJournal::on_blink_new_msg_time_changed()
{
	if(property_blinkNewMsg)
	{
		if (blink_msg_connection_.connected())
			blink_msg_connection_.disconnect();
		if(property_blinkNewMsgTime.get_value()>0)
			blink_msg_connection_= Glib::signal_timeout().connect(sigc::mem_fun(*this, &UAPSJournal::on_blink_msg_timer),property_blinkNewMsgTime.get_value());
	}
}
// -------------------------------------------------------------------------
void UAPSJournal::on_group_of_msg_changed()
{
	msg_groups.clear();
	std::list<Glib::ustring> msg_group_list = explodestr(get_prop_groupOfMsg(),',');
	for(std::list<Glib::ustring>::iterator group_it = msg_group_list.begin(); group_it != msg_group_list.end(); ++group_it)
		add_msg_group(*group_it);
}
// -------------------------------------------------------------------------
void
UAPSJournal::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
	return;

	UEventBox::set_connector(connector);

	get_connector()->signals().connect_on_any_message_full(
			sigc::mem_fun(this, &UAPSJournal::recieve_message));
	
	sensor_prop_newAPS.set_connector(connector);
	sensor_prop_haveAPS.set_connector(connector);
}
// -------------------------------------------------------------------------
void UAPSJournal::on_connect() throw()
{
	UEventBox::on_connect();

	typedef Gtk::TreeModel::Children Children;
	Children children = m_refTreeModel->children();
	for(Children::iterator it = children.begin(); it!=children.end();
			it = m_refTreeModel->erase(it));

	ConfirmSignal& cs = get_connector()->signal_confirm();

	//connect and show messages waits confirm
	for (ConfirmSignal::const_iterator it = cs.begin(); it != cs.end(); ++it ) {
		UMessages::Message message =
			get_connector()->signals().get_message(*it);
		if( !message.valid() )
			continue;
		process_message(message);
	}

	if (get_property_disconnect_effect() == 2) {
		scrolled_window_.set_sensitive();
		tree_view_.set_sensitive();
	}
}
// -------------------------------------------------------------------------
void
UAPSJournal::on_disconnect() throw()
{
	UEventBox::on_disconnect();

	if (get_property_disconnect_effect() == 2) {
		scrolled_window_.set_sensitive();
		tree_view_.set_sensitive();
	}
}
// -------------------------------------------------------------------------
//TODO: we must have signal with Message
void
UAPSJournal::recieve_message(UMessages::MessageId id, int wtype, time_t sec, Glib::ustring msg)
{
	UMessages::Message message =
		get_connector()->signals().get_message(id);

	if(message.valid())
		process_message(message);
}
// -------------------------------------------------------------------------
