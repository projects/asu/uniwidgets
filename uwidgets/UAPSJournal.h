#ifndef _UAPSJOURNAL_H
#define _UAPSJOURNAL_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <unordered_map>
#include <uwidgets/UEventBox.h>
#include <CheckedSignal.h>
#include <USignals.h>
#include <uwidgets/KineticScroll.h>
#include <global_macros.h>
#include <plugins.h>
// -------------------------------------------------------------------------
//struct msgItem;
// -------------------------------------------------------------------------
/*!
 * \brief Журнал для АПС сообщений.
 * \par
 * Журнал, в котором отображаются только АПС сообщения,а по сбросу АПС датчиков
 * сообщения также удаляются из журнала.
*/
/*! структура, описывающая строку в журнале(точнее колонки) */
class ModelColumns : public Gtk::TreeModel::ColumnRecord
{
	public:
		ModelColumns()
			{
				add(icon);
				add(timestring);
				add(textmsg);
				add(confirm_timestring);
				add(time);
				add(msg_font);
				add(id);
				add(wtype);
				add(bgcolor);
				add(fgcolor);
				add(state);
				add(checked);
				add(blinkCount);
				add(blinkState);
			}

			Gtk::TreeModelColumn< Glib::RefPtr<Gdk::Pixbuf> >  icon;
			Gtk::TreeModelColumn<Glib::ustring> timestring;
			Gtk::TreeModelColumn<Glib::ustring> textmsg;
			Gtk::TreeModelColumn<Glib::ustring> confirm_timestring;
			Gtk::TreeModelColumn<time_t> time;
			Gtk::TreeModelColumn<Glib::ustring> msg_font;
			Gtk::TreeModelColumn<UMessages::MessageId> id;
			Gtk::TreeModelColumn<int> wtype;
			Gtk::TreeModelColumn<Gdk::Color> bgcolor;
			Gtk::TreeModelColumn<Gdk::Color> fgcolor;
			Gtk::TreeModelColumn<bool> checked;
			Gtk::TreeModelColumn<int> state;
			Gtk::TreeModelColumn<int> blinkCount;
			Gtk::TreeModelColumn<bool> blinkState;
};

struct APSIconKey
{
	APSIconKey():
	id(),
	confirm_time("")
	{
	}
	APSIconKey(const UMessages::MessageId& id_, const Glib::ustring& time_):
	id(id_),
	confirm_time(time_)
	{
	}
	APSIconKey(const Gtk::TreeRow& row, const ModelColumns& columns_):
	id(row[columns_.id]),
	confirm_time(row[columns_.confirm_timestring])
	{
	}
	bool operator==(const APSIconKey& x) const
	{
		return ( id == x.id && confirm_time == x.confirm_time );
	}
	UMessages::MessageId id;
	Glib::ustring confirm_time;
};
namespace std
{
	template<>
	struct hash<APSIconKey>
	{
		size_t operator()(APSIconKey const& s) const
		{
			return std::hash<UMessages::MessageId>()(s.id);
		}
	};
}

class UAPSJournal : public UEventBox{
private:
	bool newAPSReset();
	void ctor();
	sigc::connection blink_conn;
	UniWidgetsTypes::ObjectId current_id;

	sigc::connection newAPS_conn;
	SensorProp sensor_prop_newAPS;
	Glib::Property<int> property_newAPS_timeout;
	SensorProp sensor_prop_haveAPS;
	
	Glib::Property<Glib::ustring> property_pic_title;
	Glib::Property<Glib::ustring> property_time_title;
	Glib::Property<Glib::ustring> property_text_title;
	Glib::Property<Glib::ustring> property_confirm_title;

	Glib::Property<int> property_pic_width;
	Glib::Property<int> property_time_width;
	Glib::Property<int> property_text_width;
	Glib::Property<int> property_confirm_width;

	Glib::Property<std::string> property_info_pic;
	Glib::Property<std::string> property_warn_pic;
	Glib::Property<std::string> property_alarm_pic;
	Glib::Property<std::string> property_confirm1_pic;
	Glib::Property<std::string> property_confirm2_pic;
	Glib::Property<std::string> property_confirmed_pic;

	ADD_PROPERTY( property_msgFont, Glib::ustring )			/*!< свойство: шрифт сообщений*/
	Glib::Property<Gdk::Color> property_info_color;
	Glib::Property<Gdk::Color> property_warn_color;
	Glib::Property<Gdk::Color> property_alarm_color;
	ADD_PROPERTY(property_bg_first_color, Gdk::Color)	/*!< свойство: цвет фона нечетных сообщений */
	ADD_PROPERTY(property_bg_second_color, Gdk::Color)	/*!< свойство: цвет фона четных сообщений */
	
	ADD_PROPERTY( property_blinkNewMsg, bool )				/*!< свойство: мигать новыми сообщениями*/
	ADD_PROPERTY( property_blinkNewMsgCount, int )			/*!< свойство: количество миганий*/
	ADD_PROPERTY( property_blinkNewMsgTime, int )			/*!< свойство: период мигания*/
	
	ADD_PROPERTY( prop_enableGroups, bool )				/*!< свойство: фильтровать сообщения по принадлежности к группам*/
	ADD_PROPERTY( prop_groupOfMsg, Glib::ustring)		/*!< свойство: группы отображаемых сообщений*/
	
	Glib::RefPtr<Gdk::Pixbuf> refPixInfo;
	Glib::RefPtr<Gdk::Pixbuf> refPixWarn;
	Glib::RefPtr<Gdk::Pixbuf> refPixAlarm;
	Glib::RefPtr<Gdk::Pixbuf> refPixConfirm1;
	Glib::RefPtr<Gdk::Pixbuf> refPixConfirm2;
	Glib::RefPtr<Gdk::Pixbuf> refPixConfirmed;
	std::unordered_map<APSIconKey, Glib::RefPtr<Gdk::Pixbuf> > iconPixbufMap;			/*!< map с картинками для мигания текущим квитируемым сообщением */

	void try_load_pic( Glib::RefPtr<Gdk::Pixbuf>& refPix, std::string pic );
	void value_out_proc(UMessages::MessageId, USignals::VConn* conn);

	bool on_search_noconfiredID(const Gtk::TreeIter& it,
				    const Gtk::TreeRow& row,
				    const UMessages::Message& message);

	bool on_search_noconfired(const Gtk::TreeIter& it);

	void on_foreach(const Gtk::TreeIter& it,const UMessages::MessageId& id,const time_t& sec);

	bool on_search_ID( const Gtk::TreeIter& it,
			   const UMessages::MessageId& id);

    void on_blink_foreach(const Gtk::TreeIter& it,
                          const UMessages::MessageId& id,
                          const Glib::RefPtr<Gdk::Pixbuf>& pix);

	bool has_any_not_confirmed_id;
	bool has_any_not_confirmed_aps;

	sigc::connection blink_msg_connection_;
	virtual bool on_blink_msg_timer();
	void on_foreach_blink(const Gtk::TreeIter& it);
	bool on_search_blinked(const Gtk::TreeIter& it);
	
public:

	UAPSJournal();
	explicit UAPSJournal(GtkmmBaseType::BaseObjectType* gobject);
	~UAPSJournal();

	ModelColumns m_Columns;				/*!< описание строки в журнале */

	Gtk::TreeIter current_row;			/*!< текущая строка в журнале */
	Gtk::ScrolledWindow scrolled_window_;		/*!< главное прокручиваемое окно журнала */
	Gtk::TreeView tree_view_;			/*!< виджет отображающий модель (Gtk::TreeModel) данных и позволяющий пользователю взаимодействовать с ними */
	Glib::RefPtr<Gtk::ListStore> m_refTreeModel;	/*!< модель списка для использования с виджетом Gtk::TreeView */
	KineticScroll kscroll;				/*!< кинетическая прокрутка */

	struct  msgItem
	{
		UniWidgetsTypes::ObjectId node_id;
		bool checked;
		int character;
		int current_value;
		Glib::ustring msg;
	};

	typedef std::unordered_map<UniWidgetsTypes::ObjectId, msgItem > MessagesList;
	MessagesList msgLst;

	typedef std::unordered_map<std::string,bool> MsgGroupNameMap;
	inline MsgGroupNameMap* get_msg_groups(){ return &msg_groups;};
	inline void add_msg_group(std::string group){ msg_groups[group] = true;};
	
	CheckConnection ch_conn;

	void confirm(UMessages::MessageId id, time_t sec);			/*!< обработчик квитирования */
	void blink(bool blink_state,int time, UMessages::MessageId id);		/*!< мигание иконкой в первом столбце первым на очередь к квитированию сообщением */

	virtual void on_realize();
	/*! обработчик полученияя нового сообщения */
	void recieve_message( UMessages::MessageId id, int wtype, time_t sec, Glib::ustring msg);
	/*! соединить сигнал квитирования с обработчиком */
	void connect_confirm( UMessages::MessageId id );
	/*! установить мигание данным сообщением */
	void set_pointer( UMessages::MessageId id );
	/*! удалить сообщение */
	void remove_messages(UMessages::MessageId id,Gtk::TreeIter& start);

	void on_pic_title_changed();
	void on_time_title_changed();
	void on_text_title_changed();
	void on_confirm_title_changed();
	void on_pic_width_changed();
	void on_time_width_changed();
	void on_text_width_changed();
	void on_confirm_width_changed();
	void on_info_pic_changed();
	void on_warn_pic_changed();
	void on_alarm_pic_changed();
	void on_confirm1_pic_changed();
	void on_confirm2_pic_changed();
	void on_confirmed_pic_changed();
	void on_msg_font_changed();
	void on_blink_new_msg_time_changed();
	void on_group_of_msg_changed();
	/*! обработка сообщения, определение его типа и внесение его в журнал */
	void process_message(const UMessages::Message& message);
//	/*! установить новый коннектор */
	virtual void set_connector(const ConnectorRef& connector) throw();
//	/*! обработчик события появления связи с SharedMemory */
	virtual void on_connect() throw();
//	/*! обработчик события пропадания связи с SharedMemory */
	virtual void on_disconnect() throw();

protected:
	void on_set_pix_value( Gtk::CellRenderer* renderer, const Gtk::TreeModel::iterator& iter);
	
	MsgGroupNameMap msg_groups;
	
	bool blinkState;
};
#endif
