#include <types.h>
#include <typical/TypicalImitatorLamp.h>
#include "UButton.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define TOGGLEOFF      "toggle-off"
#define STATELOGIC_NODE      "node"
#define ADD_LAMP      "add-lamp"
#define STATE_LAMP_AI      "state-lamp-ai"
#define LAMP_WIDTH      "lamp-width"
#define LAMP_HEIGHT      "lamp-height"
#define LAMP_X        "lamp-x"
#define LAMP_Y        "lamp-y"
#define IMAGELAMP_PATH      "imagelamp-path"
#define IMAGELAMP2_PATH      "imagelamp2-path"
#define BLINKTIME1      "blinktime1"
#define BLINKTIME2      "blinktime2"
// -------------------------------------------------------------------------
#define INIT_PROPERTIES() \
  property_invert_(*this, INVERT, false) \
  ,toggle_off(*this, TOGGLEOFF, false) \
  ,node(*this, STATELOGIC_NODE , UniWidgetsTypes::DefaultObjectId) \
  ,add_lamp(*this, ADD_LAMP, false) \
  ,state_lamp_ai(*this, STATE_LAMP_AI , UniWidgetsTypes::DefaultObjectId) \
  ,lamp_width(*this, LAMP_WIDTH , 68) \
  ,lamp_height(*this, LAMP_HEIGHT , 44) \
  ,lamp_x(*this, LAMP_X , 0) \
  ,lamp_y(*this, LAMP_Y , 0) \
  ,imagelamp_path(*this, IMAGELAMP_PATH, "" ) \
  ,imagelamp2_path(*this, IMAGELAMP2_PATH, "" ) \
  ,blinktime1(*this, BLINKTIME1, 500) \
  ,blinktime2(*this, BLINKTIME2, 500)
// -------------------------------------------------------------------------
enum Numbers
{
  RefreshCount = 0,   // Для сброса счётчика
  CheckedCount = 2   // Число для проверки счётчика
};
// -------------------------------------------------------------------------
void UButton::ctor()
{
//   add(frame);
//   frame.set_shadow_type(Gtk::SHADOW_NONE);
//   frame.show(); 
}
// -------------------------------------------------------------------------
UButton::UButton() :
  Glib::ObjectBase("ubutton")
  ,dout(this, "dout", get_connector())
  ,INIT_PROPERTIES()
{
  ctor();
}
// -------------------------------------------------------------------------
UButton::UButton(GtkmmBaseType::BaseObjectType* gobject) :
  BaseType(gobject)
  ,dout(this, "dout", get_connector())
  ,INIT_PROPERTIES()
{
  ctor();
}
// -------------------------------------------------------------------------
void UButton::on_realize()
{
  BaseType::on_realize();

  if(get_node() != UniWidgetsTypes::DefaultObjectId)
    dout.set_node_id(get_node());

  if(get_add_lamp())
  {
    refresh=true;
    count = NULL;

    lamp_rect = new Gdk::Rectangle(get_lamp_x(), get_lamp_y(), get_lamp_width(), get_lamp_height());
    state_lamp = new TypicalImitatorLamp();

    state_lamp->set_rect(*lamp_rect);
    state_lamp->set_path(lmpOFF, get_imagelamp_path());
    state_lamp->set_path(lmpON, get_imagelamp2_path());
    state_lamp->set_path(lmpBLINK, get_imagelamp2_path());
    state_lamp->set_path(lmpBLINK2, get_imagelamp2_path());
    state_lamp->set_path(lmpBLINK3, get_imagelamp2_path());
    state_lamp->set_blinking_time(lmpBLINK2, get_blinktime1());
    state_lamp->set_blinking_time(lmpBLINK3, get_blinktime2());

    state_lamp->set_state_ai(get_state_lamp_ai());
    state_lamp->set_node(get_node());

    state_lamp->configure();

    Gtk::Widget *w = dynamic_cast<Gtk::Widget*>(state_lamp);
    Gtk::Button::set_image(*w);
    w->signal_event().connect(sigc::mem_fun(this,&UButton::on_my_event));

    state_lamp->show();

  }
  show();
}
bool UButton::on_my_event(GdkEvent* event)
{
  BaseType::on_event(event);
  /*Так как внутренний виджет перехватил фокус, то on_clicked не может быть
    вызван. Значит вызываем его принудительно, так как стандарно он так и
    вызывается.*/
  if(event->type == GDK_BUTTON_RELEASE)
    UButton::on_clicked();
  return false;
}

// -------------------------------------------------------------------------
void UButton::on_enter()
{
  BaseType::on_enter();
//   if(refresh)
//   {
//     this->hide();
//     this->show();
//     refresh = false;
//   }
}
// -------------------------------------------------------------------------
void UButton::on_leave()
{
  BaseType::on_leave();
//   count++;
//   if(count == CheckedCount)
//   {
//     refresh = true;
//     count = RefreshCount;
//   }
}
// -------------------------------------------------------------------------
void UButton::on_clicked()
{
  BaseType::on_clicked();
  if(!get_toggle_off())
  {
    if (connector_->connected())
    {
      bool value;
      value = get_active();
      if (property_invert())
        value = !value;
      dout.save_value(value);
    }
  }
}
// -------------------------------------------------------------------------
void UButton::on_pressed()
{
  BaseType::on_pressed();

  if(get_toggle_off())
  {
    set_active(true);
    if (connector_->connected())
    {
      dout.save_value(true);
    }
  }
}
// -------------------------------------------------------------------------
void UButton::on_released()
{
  BaseType::on_released();

  if(get_toggle_off())
  {
    set_active(false);
    if (connector_->connected())
    {
      dout.save_value(false);
    }
  }
}
// -------------------------------------------------------------------------
void
UButton::set_connector(const ConnectorRef& connector) throw()
{
  if (connector == get_connector())
    return;

  BaseType::set_connector(connector);

  dout.set_connector(connector);

  dout.value_changed.connect(sigc::mem_fun(this,&UButton::process_sensor_changed));
}
// -------------------------------------------------------------------------
void UButton::process_sensor_changed(float value)
{
  if(dout.get_state())
    set_active(true);
  else
    set_active(false);
}
// -------------------------------------------------------------------------
void
UButton::on_connect() throw()
{
  BaseType::on_connect();

  if(!get_toggle_off())
  {
      if(dout.get_state())
        set_active(true);
      else
        set_active(false);
  }
}
// -------------------------------------------------------------------------
