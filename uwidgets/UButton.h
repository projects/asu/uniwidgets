#ifndef _UBUTTON_H
#define _UBUTTON_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <map>
#include <string>
#include <UDefaultFunctions.h>
#include <SensorProp.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
#define INVERT    "invert"
// ------------------------------------------------------------------------------------------
namespace UniWidgets
{
class TypicalImitatorLamp;
/*!\brief Расширенная "кнопка" на основе старой.
 * \par
 * Позволяет отключить "фиксацию кнопки", добавить "лампочку" на кнопку. Датчики для самой кнопки
 * и лампочки должны быть на одном узле. Это кнопка используется преимущественно для реализации
 * физических панелей (ГЭУ, ЦПУ).
 * Описание параметров:
 * - ADD_PROPERTY( add_lamp, bool ): при значении "true" добавляет лампу на кнопку
 * - ADD_PROPERTY( toggle_off, bool ): при значении "true" отключает "фиксацию" кнопки
 * - ADD_PROPERTY( state_lamp_ai, UniWidgetsTypes::ObjectId ): указывается датчик лампочки
 * - ADD_PROPERTY( node, UniWidgetsTypes::ObjectId ): указывается узел
 * параметры расположения на экране и размера картинки лампочки
 * - ADD_PROPERTY( lamp_width, long )
 * - ADD_PROPERTY( lamp_height, long )
 * - ADD_PROPERTY( lamp_x, long ) 
 * - ADD_PROPERTY( lamp_y, long )

 * - ADD_PROPERTY( imagelamp_path, ... )
 * - ADD_PROPERTY( imagelamp2_path, ... ) - путь, где находится картинка для лампочки
*/
class UButton : public UDefaultFunctions<Gtk::ToggleButton>
{
public:
  UButton();
  explicit UButton(GtkmmBaseType::BaseObjectType* gobject);
  ~UButton() {};

  /* Properties get/set methods */
  Glib::PropertyProxy<bool> property_invert();
  Glib::PropertyProxy_ReadOnly<bool> property_invert() const;
  Gdk::Rectangle *btn_lamp_rect;    /*!< координаты и размеры кнопки */

  virtual void set_connector(const ConnectorRef& connector) throw();

  void process_sensor_changed(float value);  /*!< обработка сообщений об изменении состояния датчика */

protected:
  /* Properties */
  SensorProp dout;      /*!< датчик для самой кнопки */

  /* Event handlers */
  virtual void on_clicked();
  virtual void on_enter();
  virtual void on_leave();
  virtual void on_pressed();
  virtual void on_released();
  virtual void on_realize();

  virtual void on_connect() throw();   /*!< обработчик события появления связи с SharedMemory */
  bool on_my_event(GdkEvent* event);

private:
  Gtk::Frame frame;
  TypicalImitatorLamp *state_lamp;
  Gdk::Rectangle *lamp_rect;

  bool refresh;
  int count;

  /* Properties */
  Glib::Property<bool> property_invert_;

  /* Methods */
  void ctor();

  ADD_PROPERTY( toggle_off, bool )
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( add_lamp, bool )
  ADD_PROPERTY( state_lamp_ai, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( lamp_width, long )
  ADD_PROPERTY( lamp_height, long )
  ADD_PROPERTY( lamp_x, long )
  ADD_PROPERTY( lamp_y, long )
  ADD_PROPERTY( imagelamp_path, Glib::ustring )
  ADD_PROPERTY( imagelamp2_path, Glib::ustring )
  ADD_PROPERTY( blinktime1, int )
  ADD_PROPERTY( blinktime2, int )
};
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<bool>
UButton::property_invert()
{
  return property_invert_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<bool>
UButton::property_invert() const
{
  return Glib::PropertyProxy_ReadOnly<bool>(this, INVERT);
}

}
// -------------------------------------------------------------------------
#endif
