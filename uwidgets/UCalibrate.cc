/*
класс реализующий инерцию изменения аналогового значения
*/
#include "UCalibrate.h"
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

// -------------------------------------------------------------------------
UCalibrate::UCalibrate(Gtk::Widget* w) :
owner(w)
,prop_enable(*w,"enable-calibrate",false)
,prop_limit(*w,"enable-calibrate-limit",false)
,prop_rmin(*w,"calibrate-rmin",0)
,prop_rmax(*w,"calibrate-rmax",100)
,prop_cmin(*w,"calibrate-cmin",0)
,prop_cmax(*w,"calibrate-cmax",100)
,rvalue(0)
,cvalue(0)
,rmin_value(0)
,rmax_value(100)
,cmin_value(0)
,cmax_value(100)
{
	owner->connect_property_changed("calibrate-rmin", sigc::mem_fun(*this, &UCalibrate::rmin_changed) );
	owner->connect_property_changed("calibrate-rmax", sigc::mem_fun(*this, &UCalibrate::rmax_changed) );
	owner->connect_property_changed("calibrate-cmin", sigc::mem_fun(*this, &UCalibrate::cmin_changed) );
	owner->connect_property_changed("calibrate-cmax", sigc::mem_fun(*this, &UCalibrate::cmax_changed) );
}
// -------------------------------------------------------------------------
UCalibrate::~UCalibrate()
{
}
// -------------------------------------------------------------------------
void UCalibrate::rmin_changed()
{
// 	cout<<owner->get_name()<<"::rmin_changed() rmin="<<get_prop_rmin()<< endl;
	set_rmin(get_prop_rmin());
}
// -------------------------------------------------------------------------
void UCalibrate::rmax_changed()
{
// 	cout<<owner->get_name()<<"::rmax_changed() rmax="<<get_prop_rmax()<< endl;
	set_rmax(get_prop_rmax());
}
// -------------------------------------------------------------------------
void UCalibrate::cmin_changed()
{
// 	cout<<owner->get_name()<<"::cmin_changed() cmin="<<get_prop_cmin()<< endl;
	set_cmin(get_prop_cmin());
}
// -------------------------------------------------------------------------
void UCalibrate::cmax_changed()
{
// 	cout<<owner->get_name()<<"::cmax_changed() cmax="<<get_prop_cmax()<< endl;
	set_cmax(get_prop_cmax());
}
// -------------------------------------------------------------------------
double UCalibrate::get_rvalue(double val)
{
	if(!prop_enable)
		return val;
	
	if( get_prop_rmin()==get_prop_rmax() || get_prop_cmin()==get_prop_cmax() )
	{
		cout<<owner->get_name()<<"::get_rvalue() ДЕЛЕНИЕ НА 0!!! value="<<val<<" rmin="<<get_prop_rmin()<<" rmax="<<get_prop_rmax()<<" cmin="<<get_prop_cmin()<<" cmax="<<get_prop_cmax()<< endl;
		return val;
	}
	
	cvalue = val;
	rvalue = get_prop_rmin() + (cvalue - get_prop_cmin())/(get_prop_cmax() - get_prop_cmin()) * (get_prop_rmax() - get_prop_rmin());
	if(get_prop_limit())
	{
		if(rvalue > get_prop_rmax())
			rvalue = get_prop_rmax();
		else if(rvalue < get_prop_rmin())
			rvalue = get_prop_rmin();
	}
	
	return rvalue;
}
// -------------------------------------------------------------------------
double UCalibrate::get_cvalue(double val)
{
	if(!prop_enable)
		return val;

	if( get_prop_rmin()==get_prop_rmax() || get_prop_cmin()==get_prop_cmax() )
	{
		cout<<owner->get_name()<<"::get_cvalue() ДЕЛЕНИЕ НА 0!!! value="<<val<<" rmin="<<get_prop_rmin()<<" rmax="<<get_prop_rmax()<<" cmin="<<get_prop_cmin()<<" cmax="<<get_prop_cmax()<< endl;
		return val;
	}
	
	rvalue = val;
	cvalue = get_prop_cmin() + (rvalue - get_prop_rmin())/(get_prop_rmax() - get_prop_rmin()) * (get_prop_cmax() - get_prop_cmin());
	if(get_prop_limit())
	{
		if(cvalue > get_prop_cmax())
			cvalue = get_prop_cmax();
		else if(cvalue < get_prop_cmin())
			cvalue = get_prop_cmin();
	}
	
	return cvalue;
}
// -------------------------------------------------------------------------
double UCalibrate::get_rvalue(double rmin, double rmax, double cmin, double cmax, double val)
{
//	cout<<owner->get_name()<<"::get_cvalue() value="<<val<<" rmin="<<rmin<<" rmax="<<rmax<<" cmin="<<cmin<<" cmax="<<cmax<< endl;
	set_r_range(rmin, rmax);
	set_c_range(cmin, cmax);
	return get_rvalue(val);
}
// -------------------------------------------------------------------------
double UCalibrate::get_cvalue(double rmin, double rmax, double cmin, double cmax, double val)
{
//	cout<<owner->get_name()<<"::get_cvalue() value="<<val<<" rmin="<<rmin<<" rmax="<<rmax<<" cmin="<<cmin<<" cmax="<<cmax<< endl;
	set_r_range(rmin, rmax);
	set_c_range(cmin, cmax);
	return get_cvalue(val);
}
// -------------------------------------------------------------------------
