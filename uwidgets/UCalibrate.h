/*
	класс реализующий квантование аналогового значения
*/

#ifndef UCALIBRATE_H
#define UCALIBRATE_H
#include <gtkmm.h>
#include <global_macros.h>

class UCalibrate : public Glib::Object
{
	public:
		UCalibrate(Gtk::Widget* w);
		virtual ~UCalibrate();

		virtual double get_rvalue(double cval);								/* */
		virtual double get_rvalue(double rmin, double rmax, double cmin, double cmax, double cval);		/* */
		inline double get_rvalue() { return rvalue;};
		virtual double get_cvalue(double rval);								/* */
		virtual double get_cvalue(double rmin, double rmax, double cmin, double cmax, double rval);		/* */
		inline double get_cvalue() { return cvalue;};
		inline void set_rmin(double min)
		{
			rmin_value = min;
			if(rvalue<min)
			{
				rvalue = min;
				cvalue = get_cvalue(rmin_value,rmax_value,cmin_value,cmax_value,rvalue);
			}
		}
		inline void set_rmax(double max)
		{
			rmax_value = max;
			if(rvalue>max)
			{
				rvalue = max;
				cvalue = get_cvalue(rmin_value,rmax_value,cmin_value,cmax_value,rvalue);
			}
		}
		inline void set_cmin(double min)
		{
			cmin_value = min;
			if(cvalue<min)
			{
				cvalue = min;
				rvalue = get_rvalue(rmin_value,rmax_value,cmin_value,cmax_value,cvalue);
			}
		}
		inline void set_cmax(double max)
		{
			cmax_value = max;
			if(cvalue>max)
			{
				cvalue = max;
				rvalue = get_rvalue(rmin_value,rmax_value,cmin_value,cmax_value,cvalue);
			}
		}
		inline void set_r_range(double min, double max)
		{
			set_rmin(min);
			set_rmax(max);
		}
		inline void set_c_range(double min, double max)
		{
			set_cmin(min);
			set_cmax(max);
		}
		
	protected:

		Gtk::Widget* owner;

		void rmin_changed();
		void rmax_changed();
		void cmin_changed();
		void cmax_changed();
		
		double rvalue;
		double cvalue;
		double cmin_value;
		double cmax_value;
		double rmin_value;
		double rmax_value;
		
		ADD_PROPERTY( prop_enable, bool )		/*!< свойство: использовать ли калибровку*/
		ADD_PROPERTY( prop_limit, bool )		/*!< свойство: использовать ли ограничитель по крайним значениям калибровки*/
		ADD_PROPERTY( prop_rmin, double )		/*!< свойство: сырое минимальное значение*/
		ADD_PROPERTY( prop_rmax, double )		/*!< свойство: сырое максимальное значение*/
		ADD_PROPERTY( prop_cmin, double )		/*!< свойство: калиброванное минимальное значение*/
		ADD_PROPERTY( prop_cmax, double )		/*!< свойство: калиброванное максимальное значение*/
		
	private:
};
#endif
