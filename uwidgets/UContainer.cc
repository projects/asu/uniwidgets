#include <iostream>
#include "UContainer.h"
#include "Connector.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void UContainer::ctor()
{
}
// -------------------------------------------------------------------------
UContainer::UContainer() :
	Glib::ObjectBase("ucontainer")
{
	ctor();
}
// -------------------------------------------------------------------------
UContainer::UContainer(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
{
	ctor();
}
// -------------------------------------------------------------------------
UContainer::~UContainer()
{
}
// -------------------------------------------------------------------------
void UContainer::init_widget()
{
//	std::cerr<<"UContainer::init_widget()"<<std::endl;
}
// -------------------------------------------------------------------------
// void UContainer::add(Gtk::Widget& w)
// {
// 	Gtk::EventBox::add(w);
//	activate_gpm( this, gpm );

//	ConnectorRef connector = get_connector();
//	set_connector_to_hierarchy(this,connector);
// }
// -------------------------------------------------------------------------
void UContainer::on_parent_changed(Gtk::Widget* prev_parent)
{
	BaseType::on_parent_changed(prev_parent);
//	gpm = search_gpm();
//	if ( gpm!=NULL )
//		activate_gpm( this, gpm ); //Send gpm to children

//	ConnectorRef new_connector = get_connector_from_hierarchy();
//	set_connector_to_hierarchy(this,new_connector);
}
// -------------------------------------------------------------------------
