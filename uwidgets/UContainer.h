#ifndef _UCONTAINER_H
#define _UCONTAINER_H
// -------------------------------------------------------------------------
#include "UEventBox.h"
// -------------------------------------------------------------------------
class ConnectorRef;
// -------------------------------------------------------------------------
/*!\brief Класс контейнера.
 * \par
 * Базовый класс контейнера.
*/
class UContainer : public UEventBox{
private:
	void ctor();
public:
	UContainer();
	explicit UContainer(GtkmmBaseType::BaseObjectType* gobject);
	~UContainer();
	virtual void init_widget();
	
// 	virtual void add(Gtk::Widget &w);
	virtual void on_parent_changed(Gtk::Widget *prev_parent);

};
#endif
