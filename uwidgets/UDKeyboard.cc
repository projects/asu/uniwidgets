#include <iostream>
#include <gtkmm.h>
#include "UDKeyboard.h"

using namespace std;
using namespace UniWidgetsTypes;

//----------------------------------------------------------
UDKeyboardButton::UDKeyboardButton() :
Gtk::Button()
,font_name("Liberation Sans")
,font_size(-1)
,alignment(Pango::ALIGN_CENTER)
,curve_radius(2)
,border_width(1)
,border_color(Gdk::Color("B3B3B3"))
,grad_color1(Gdk::Color("A0A0A0"))
,grad_color4(Gdk::Color("050505"))
,grad_color2(Gdk::Color("787878"))
,grad_color3(Gdk::Color("232323"))
,active_grad_color1(Gdk::Color("A0A0A0"))
,active_grad_color4(Gdk::Color("050505"))
,active_grad_color2(Gdk::Color("787878"))
,active_grad_color3(Gdk::Color("232323"))
,font_color(Gdk::Color("white"))
,grad_separate(true)
,grad_separate_pos(0.30)
,draw_inactive(true)
,drop_shadow(true)
{
}
//----------------------------------------------------------
bool UDKeyboardButton::on_expose_event(GdkEventExpose *ev)
{
	Gtk::Allocation alloc = get_allocation();
	Cairo::RefPtr<Cairo::Context> cr =
	get_window()->create_cairo_context();
	switch ( get_state() )
	{
		case Gtk::STATE_NORMAL:
			draw_normal(cr);
			break;
		case Gtk::STATE_ACTIVE:
			draw_active(cr);
			break;
		case Gtk::STATE_PRELIGHT :
			draw_normal(cr);
			break;
		case Gtk::STATE_SELECTED :
			draw_normal(cr);
			break;
	}
	return true;
}
//----------------------------------------------------------
void UDKeyboardButton::draw_normal(Cairo::RefPtr<Cairo::Context> &cr)
{
	cr->save();
	Gtk::Allocation alloc = get_allocation();
	int width = alloc.get_width() - 1 - border_width;
	int height = alloc.get_height() - 1 - border_width;
	double x = alloc.get_x() + 1 + (double)border_width / 2.0;
	double y = alloc.get_y() + 1 + (double)border_width / 2.0;

	unsigned int shift = curve_radius;
	if( shift > width / 2.0)
		shift = width / 2.0;
	if( shift > height / 2.0)
		shift = height / 2.0;

	cr->set_line_width(border_width);
	cr->move_to(x + shift, y );
	cr->arc( x + width - shift, y + shift , shift, -M_PI/2, 0);
	cr->arc( x + width - shift, y + height - shift, shift, 0, M_PI/2);
	cr->arc( x + shift, y + height - shift, shift, M_PI/2, M_PI);
	cr->arc( x + shift, y + shift, shift, -M_PI,  -M_PI/2);
	cr->close_path();

	cr->set_source_rgb(0,0,0);
	cr->fill_preserve();
	cr->stroke();

	x=x-1;
	y=y-1;

	Cairo::RefPtr<Cairo::Gradient> grad =
	Cairo::LinearGradient::create( x, y, x, y + height );

	cr->set_line_width(border_width);
	cr->move_to(x + shift, y );
	cr->arc( x + width - shift, y + shift , shift, -M_PI/2, 0);
	cr->arc( x + width - shift, y + height - shift, shift, 0, M_PI/2);
	cr->arc( x + shift, y + height - shift, shift, M_PI/2, M_PI);
	cr->arc( x + shift, y + shift, shift, -M_PI,  -M_PI/2);
	cr->close_path();

	if(!grad_color1.to_string().empty())
		grad->add_color_stop_rgb(0., grad_color1.get_red_p(), grad_color1.get_green_p(), grad_color1.get_blue_p());
	if(!grad_color4.to_string().empty())
		grad->add_color_stop_rgb(1., grad_color4.get_red_p(), grad_color4.get_green_p(), grad_color4.get_blue_p());
	if( grad_separate )
	{
		double grad_pos = get_grad_separate_pos();
		if(grad_pos>1.0)
			grad_pos = 1.0;
		else if(grad_pos<0.0)
			grad_pos = 0.0;
		if(!grad_color2.to_string().empty())
			grad->add_color_stop_rgb(grad_pos, grad_color2.get_red_p(), grad_color2.get_green_p(), grad_color2.get_blue_p());
		if(!grad_color3.to_string().empty())
			grad->add_color_stop_rgb(grad_pos, grad_color3.get_red_p(), grad_color3.get_green_p(), grad_color3.get_blue_p());
	}
	cr->set_source(grad);
	cr->fill_preserve();

	Gdk::Cairo::set_source_color(cr, border_color);
	cr->stroke();

	cr->restore();

	Glib::RefPtr<Pango::Layout> txt;
	txt = create_pango_layout(get_label());
	txt->set_alignment( alignment );

	Pango::FontDescription fd( font_name );
	if(font_size > 0)
		fd.set_absolute_size(font_size * Pango::SCALE);
	else
		fd.set_absolute_size(16 * Pango::SCALE);
	txt->set_font_description( fd);
	int w,h;
	txt->get_pixel_size(w,h);
	int text_x = x + (width - w)/2;
	int text_y = y + (height - h)/2;
	cr->save();
	if( drop_shadow )
	{
		cr->set_source_rgba(0,0,0,0.5);
		cr->move_to(text_x-1,text_y-1);
		txt->add_to_cairo_context(cr);
		cr->fill();
	}
	Gdk::Cairo::set_source_color(cr, font_color);
	cr->move_to(text_x, text_y);
	txt->add_to_cairo_context(cr);
	cr->fill();
}
//----------------------------------------------------------
void UDKeyboardButton::draw_active(Cairo::RefPtr<Cairo::Context> &cr)
{
	cr->save();
	Gtk::Allocation alloc = get_allocation();
	int width = alloc.get_width() - 1 - border_width;
	int height = alloc.get_height() - 1 - border_width;
	double x = alloc.get_x() + 1 + (double)border_width / 2.0;
	double y = alloc.get_y() + 1 + (double)border_width / 2.0;

	unsigned int shift = curve_radius;
	if( shift > width / 2.0)
		shift = width / 2.0;
	if( shift > height / 2.0)
		shift = height / 2.0;

	Cairo::RefPtr<Cairo::Gradient> grad =
	Cairo::LinearGradient::create( x, y, x, y + height );

	cr->set_line_width(border_width);
	cr->move_to(x + shift, y );
	cr->arc( x + width - shift, y + shift , shift, -M_PI/2, 0);
	cr->arc( x + width - shift, y + height - shift, shift, 0, M_PI/2);
	cr->arc( x + shift, y + height - shift, shift, M_PI/2, M_PI);
	cr->arc( x + shift, y + shift, shift, -M_PI,  -M_PI/2);
	cr->close_path();

	if(!active_grad_color1.to_string().empty())
		grad->add_color_stop_rgb(0., active_grad_color1.get_red_p(), active_grad_color1.get_green_p(), active_grad_color1.get_blue_p());
	if(!active_grad_color4.to_string().empty())
		grad->add_color_stop_rgb(1., active_grad_color4.get_red_p(), active_grad_color4.get_green_p(), active_grad_color4.get_blue_p());
	if( grad_separate )
	{
		double grad_pos = get_grad_separate_pos();
		if(grad_pos>1.0)
			grad_pos = 1.0;
		else if(grad_pos<0.0)
			grad_pos = 0.0;
		if(!active_grad_color2.to_string().empty())
			grad->add_color_stop_rgb(grad_pos, active_grad_color2.get_red_p(), active_grad_color2.get_green_p(), active_grad_color2.get_blue_p());
		if(!active_grad_color3.to_string().empty())
			grad->add_color_stop_rgb(grad_pos, active_grad_color3.get_red_p(), active_grad_color3.get_green_p(), active_grad_color3.get_blue_p());
	}
	cr->set_source(grad);
	cr->fill_preserve();

	Gdk::Cairo::set_source_color(cr, border_color);
	cr->stroke();

	cr->restore();

	Glib::RefPtr<Pango::Layout> txt;
	txt = create_pango_layout(get_label());
	txt->set_alignment( alignment );

	Pango::FontDescription fd( font_name );
	if(font_size > 0)
		fd.set_absolute_size(font_size * Pango::SCALE);
	else
		fd.set_absolute_size(16 * Pango::SCALE);
	txt->set_font_description( fd);
	int w,h;
	txt->get_pixel_size(w,h);
	int text_x = x + (width - w)/2;
	int text_y = y + (height - h)/2;
	cr->save();
	if( drop_shadow )
	{
		cr->set_source_rgba(0,0,0,0.5);
		cr->move_to(text_x-1,text_y-1);
		txt->add_to_cairo_context(cr);
		cr->fill();
	}
	Gdk::Cairo::set_source_color(cr, font_color);
	cr->move_to(text_x, text_y);
	txt->add_to_cairo_context(cr);
	cr->fill();
}
//----------------------------------------------------------

#define UDKEYBOARD_PROPERTY_INIT() \
keyboard(nullptr) \
,property_use_keyboard(*this, "use-keyboard", true) \
,property_replace_value(*this, "replace-value", false) \
,property_width_size(*this, "buttons-width", 100) \
,property_height_size(*this, "buttons-height", 50) \
,property_labelFrame(*this, "label-frame-type", Gtk::SHADOW_IN ) \
,property_label_font_name(*this, "label-fontname", "Liberation Sans Bold") \
,property_label_font_size(*this, "label-fontsize", 20) \
,property_label_font_color(*this, "label-font-color", Gdk::Color("#888888")) \
,property_font_name(*this, "button-fontname", "Liberation Sans Bold") \
,property_font_size(*this, "button-fontsize", 20) \
,property_font_color(*this, "button-font-color", Gdk::Color("#888888")) \
,property_curve_radius(*this, "curve-radius", 5) \
,property_border_width(*this, "border-width", 1) \
,property_border_color(*this, "border-color", Gdk::Color("#777777")) \
,property_grad_color1(*this, "gradient-color-begin", Gdk::Color("#555555")) \
,property_grad_color4(*this, "gradient-color-end", Gdk::Color("#000000")) \
,property_grad_color2(*this, "gradient-color-separate1", Gdk::Color("#444444")) \
,property_grad_color3(*this, "gradient-color-separate2", Gdk::Color("#353535")) \
,property_active_grad_color1(*this, "gradient-active-color-begin", Gdk::Color("#000000")) \
,property_active_grad_color4(*this, "gradient-active-color-end", Gdk::Color("#555555")) \
,property_active_grad_color2(*this, "gradient-active-color-separate1", Gdk::Color("#353535")) \
,property_active_grad_color3(*this, "gradient-active-color-separate2", Gdk::Color("#444444")) \
,property_grad_separate(*this, "gradient-separate", true) \
,property_grad_separate_pos(*this, "gradient-separate-position", 0.30) \
,property_draw_inactive(*this, "draw-inactive", true) \
,property_drop_shadow(*this, "drop-shadow", true) \
,consumer_widget(nullptr) \
,entry_parent(nullptr) \
,init_edit(false) \

//----------------------------------------------------------
void UDKeyboard::ctor()
{
	set_visible_window(false);
	signal_event().connect(sigc::mem_fun(*this, &UDKeyboard::on_area_event));
	signal_parent_changed().connect(sigc::mem_fun(*this, &UDKeyboard::on_add_to_new_parent));

	connect_property_changed("use-keyboard", sigc::mem_fun(*this, &UDKeyboard::on_connect_to_entry_changed) );
	connect_property_changed("label-frame-type", sigc::mem_fun(*this, &UDKeyboard::on_frame_changed) );
	connect_property_changed("label-fontname", sigc::mem_fun(*this, &UDKeyboard::on_label_font_changed) );
	connect_property_changed("label-fontsize", sigc::mem_fun(*this, &UDKeyboard::on_label_font_changed) );
	connect_property_changed("label-font-color", sigc::mem_fun(*this, &UDKeyboard::on_label_font_changed) );
	connect_property_changed("buttons-width", sigc::mem_fun(*this, &UDKeyboard::on_button_size_changed) );
	connect_property_changed("buttons-height", sigc::mem_fun(*this, &UDKeyboard::on_button_size_changed) );
	connect_property_changed("button-fontname", sigc::mem_fun(*this, &UDKeyboard::on_font_name_changed) );
	connect_property_changed("button-fontsize", sigc::mem_fun(*this, &UDKeyboard::on_font_size_changed) );
	connect_property_changed("button-font-color", sigc::mem_fun(*this, &UDKeyboard::on_font_color_changed) );
	connect_property_changed("curve-radius", sigc::mem_fun(*this, &UDKeyboard::on_curve_radius_changed) );
	connect_property_changed("border-width", sigc::mem_fun(*this, &UDKeyboard::on_border_width_changed) );
	connect_property_changed("border-color", sigc::mem_fun(*this, &UDKeyboard::on_border_color_changed) );
	connect_property_changed("gradient-color-begin", sigc::mem_fun(*this, &UDKeyboard::on_grad_color1_changed) );
	connect_property_changed("gradient-color-end", sigc::mem_fun(*this, &UDKeyboard::on_grad_color4_changed) );
	connect_property_changed("gradient-color-separate1", sigc::mem_fun(*this, &UDKeyboard::on_grad_color2_changed) );
	connect_property_changed("gradient-color-separate2", sigc::mem_fun(*this, &UDKeyboard::on_grad_color3_changed) );
	connect_property_changed("gradient-active-color-begin", sigc::mem_fun(*this, &UDKeyboard::on_active_grad_color1_changed) );
	connect_property_changed("gradient-active-color-end", sigc::mem_fun(*this, &UDKeyboard::on_active_grad_color4_changed) );
	connect_property_changed("gradient-active-color-separate1", sigc::mem_fun(*this, &UDKeyboard::on_active_grad_color2_changed) );
	connect_property_changed("gradient-active-color-separate2", sigc::mem_fun(*this, &UDKeyboard::on_active_grad_color3_changed) );
	connect_property_changed("gradient-separate", sigc::mem_fun(*this, &UDKeyboard::on_grad_separate_changed) );
	connect_property_changed("gradient-separate-position", sigc::mem_fun(*this, &UDKeyboard::on_grad_separate_pos_changed) );
	connect_property_changed("draw-inactive", sigc::mem_fun(*this, &UDKeyboard::on_draw_inactive_changed) );
	connect_property_changed("drop-shadow", sigc::mem_fun(*this, &UDKeyboard::on_drop_shadow_changed) );
	
	keyboard = manage(new class Gtk::Window());
	keyboard->set_position(Gtk::WIN_POS_CENTER);
	keyboard->signal_focus_out_event().connect(sigc::mem_fun(*this, &UDKeyboard::on_keyboard_focus_out_event));
	
	Gtk::VBox*vbox=manage(new Gtk::VBox());
	vbox->show();
	keyboard->add(*vbox);

	frame=manage(new Gtk::Frame());
	frame->show();
	vbox->add(*frame);
	
	text=manage(new Gtk::Label(""));
	text->show();
	frame->add(*text);
	
	Gtk::HBox*hbox1=manage(new Gtk::HBox());
	hbox1->show();
	vbox->add(*hbox1);
	Gtk::HBox*hbox2=manage(new Gtk::HBox());
	hbox2->show();
	vbox->add(*hbox2);
	Gtk::HBox*hbox3=manage(new Gtk::HBox());
	hbox3->show();
	vbox->add(*hbox3);
	Gtk::HBox*hbox4=manage(new Gtk::HBox());
	hbox4->show();
	vbox->add(*hbox4);
	Gtk::HBox*hbox5=manage(new Gtk::HBox());
	hbox5->show();
	vbox->add(*hbox5);

	button_c = manage( new UDKeyboardButton() );
	button_c->set_label("◄");
	button_c->set_size_request(get_property_width_size(),get_property_height_size());
	button_c->show();
	button_c->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"◄"));
	hbox5->pack_start(*button_c,1,1);

	button_ok = manage( new UDKeyboardButton() );
	button_ok->set_label("Принять");
	button_ok->set_image(*manage( new Gtk::Image(Gtk::Stock::OK,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
	button_ok->set_size_request(get_property_width_size(),get_property_height_size());
	button_ok->show();
	button_ok->signal_clicked().connect(sigc::mem_fun(*this, &UDKeyboard::clicked_ok));
	hbox5->pack_start(*button_ok,1,1);
	
	button_cancel = manage( new UDKeyboardButton() );
	button_cancel->set_label("Отмена");
	button_cancel->set_image(*manage( new Gtk::Image(Gtk::Stock::CANCEL,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
	button_cancel->set_size_request(get_property_width_size(),get_property_height_size());
	button_cancel->show();
	button_cancel->signal_clicked().connect(sigc::mem_fun(*this, &UDKeyboard::clicked_cancel));
	hbox5->pack_start(*button_cancel,1,1);
	
	button_1 = manage( new UDKeyboardButton() );
	button_1->set_label("1");
	button_1->set_size_request(get_property_width_size(),get_property_height_size());
	button_1->show();
	button_1->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"1"));
	hbox1->pack_start(*button_1,1,1);
	
	button_2 = manage( new UDKeyboardButton() );
	button_2->set_label("2");
	button_2->set_size_request(get_property_width_size(),get_property_height_size());
	button_2->show();
	button_2->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"2"));
	hbox1->pack_start(*button_2,1,1);

	button_3 = manage( new UDKeyboardButton() );
	button_3->set_label("3");
	button_3->set_size_request(get_property_width_size(),get_property_height_size());
	button_3->show();
	button_3->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"3"));
	hbox1->pack_start(*button_3,1,1);

	button_4 = manage( new UDKeyboardButton() );
	button_4->set_label("4");
	button_4->set_size_request(get_property_width_size(),get_property_height_size());
	button_4->show();
	button_4->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"4"));
	hbox2->pack_start(*button_4,1,1);

	button_5 = manage( new UDKeyboardButton() );
	button_5->set_label("5");
	button_5->set_size_request(get_property_width_size(),get_property_height_size());
	button_5->show();
	button_5->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"5"));
	hbox2->pack_start(*button_5,1,1);

	button_6 = manage( new UDKeyboardButton() );
	button_6->set_label("6");
	button_6->set_size_request(get_property_width_size(),get_property_height_size());
	button_6->show();
	button_6->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"6"));
	hbox2->pack_start(*button_6,1,1);

	button_7 = manage( new UDKeyboardButton() );
	button_7->set_label("7");
	button_7->set_size_request(get_property_width_size(),get_property_height_size());
	button_7->show();
	button_7->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"7"));
	hbox3->pack_start(*button_7,1,1);

	button_8 = manage( new UDKeyboardButton() );
	button_8->set_label("8");
	button_8->set_size_request(get_property_width_size(),get_property_height_size());
	button_8->show();
	button_8->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"8"));
	hbox3->pack_start(*button_8,1,1);

	button_9 = manage( new UDKeyboardButton() );
	button_9->set_label("9");
	button_9->set_size_request(get_property_width_size(),get_property_height_size());
	button_9->show();
	button_9->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"9"));
	hbox3->pack_start(*button_9,1,1);

	button_minus = manage( new UDKeyboardButton() );
	button_minus->set_label("±");
	button_minus->set_size_request(get_property_width_size(),get_property_height_size());
	button_minus->show();
	button_minus->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"±"));
	hbox4->pack_start(*button_minus,1,1);

	button_0 = manage( new UDKeyboardButton() );
	button_0->set_label("0");
	button_0->set_size_request(get_property_width_size(),get_property_height_size());
	button_0->show();
	button_0->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),"0"));
	hbox4->pack_start(*button_0,1,1);

	button_dot = manage( new UDKeyboardButton() );
	button_dot->set_label(",");
	button_dot->set_size_request(get_property_width_size(),get_property_height_size());
	button_dot->show();
	button_dot->signal_clicked().connect(bind(sigc::mem_fun(*this, &UDKeyboard::edit),","));
	hbox4->pack_start(*button_dot,1,1);
}
// -------------------------------------------------------------------------
UDKeyboard::UDKeyboard() :
Glib::ObjectBase("udkeyboard")
,UDKEYBOARD_PROPERTY_INIT()
{
	ctor();
}
// -------------------------------------------------------------------------
UDKeyboard::UDKeyboard(GtkmmBaseType::BaseObjectType* gobject) :
UEventBox(gobject)
,UDKEYBOARD_PROPERTY_INIT()
{
	ctor();
}
// -------------------------------------------------------------------------
bool UDKeyboard::on_expose_event(GdkEventExpose* event)
{
	bool rv = UEventBox::on_expose_event(event);
	return rv;
}
// -------------------------------------------------------------------------
UDKeyboard::~UDKeyboard()
{
}
// -------------------------------------------------------------------------
void UDKeyboard::init_widget()
{
}
// -------------------------------------------------------------------------
void UDKeyboard::on_button_size_changed()
{
	button_1->set_size_request(get_property_width_size(),get_property_height_size());
	button_2->set_size_request(get_property_width_size(),get_property_height_size());
	button_3->set_size_request(get_property_width_size(),get_property_height_size());
	button_4->set_size_request(get_property_width_size(),get_property_height_size());
	button_5->set_size_request(get_property_width_size(),get_property_height_size());
	button_6->set_size_request(get_property_width_size(),get_property_height_size());
	button_7->set_size_request(get_property_width_size(),get_property_height_size());
	button_8->set_size_request(get_property_width_size(),get_property_height_size());
	button_9->set_size_request(get_property_width_size(),get_property_height_size());
	button_0->set_size_request(get_property_width_size(),get_property_height_size());
	button_minus->set_size_request(get_property_width_size(),get_property_height_size());
	button_dot->set_size_request(get_property_width_size(),get_property_height_size());
	button_c->set_size_request(get_property_width_size(),get_property_height_size());
	button_cancel->set_size_request(get_property_width_size(),get_property_height_size());
	button_ok->set_size_request(get_property_width_size(),get_property_height_size());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_label_font_changed()
{
	Pango::FontDescription fd(property_label_font_name);
	fd.set_absolute_size( property_label_font_size * Pango::SCALE );
	text->modify_font(fd);
	text->modify_fg(Gtk::STATE_NORMAL,get_property_label_font_color());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_font_name_changed()
{
	button_1->set_font_name(get_property_font_name());
	button_2->set_font_name(get_property_font_name());
	button_3->set_font_name(get_property_font_name());
	button_4->set_font_name(get_property_font_name());
	button_5->set_font_name(get_property_font_name());
	button_6->set_font_name(get_property_font_name());
	button_7->set_font_name(get_property_font_name());
	button_8->set_font_name(get_property_font_name());
	button_9->set_font_name(get_property_font_name());
	button_0->set_font_name(get_property_font_name());
	button_dot->set_font_name(get_property_font_name());
	button_minus->set_font_name(get_property_font_name());
	button_c->set_font_name(get_property_font_name());
	button_ok->set_font_name(get_property_font_name());
	button_cancel->set_font_name(get_property_font_name());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_font_size_changed()
{
	button_1->set_font_size(get_property_font_size());
	button_2->set_font_size(get_property_font_size());
	button_3->set_font_size(get_property_font_size());
	button_4->set_font_size(get_property_font_size());
	button_5->set_font_size(get_property_font_size());
	button_6->set_font_size(get_property_font_size());
	button_7->set_font_size(get_property_font_size());
	button_8->set_font_size(get_property_font_size());
	button_9->set_font_size(get_property_font_size());
	button_0->set_font_size(get_property_font_size());
	button_dot->set_font_size(get_property_font_size());
	button_minus->set_font_size(get_property_font_size());
	button_c->set_font_size(get_property_font_size());
	button_ok->set_font_size(get_property_font_size());
	button_cancel->set_font_size(get_property_font_size());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_font_color_changed()
{
	button_1->set_font_color(get_property_font_color());
	button_2->set_font_color(get_property_font_color());
	button_3->set_font_color(get_property_font_color());
	button_4->set_font_color(get_property_font_color());
	button_5->set_font_color(get_property_font_color());
	button_6->set_font_color(get_property_font_color());
	button_7->set_font_color(get_property_font_color());
	button_8->set_font_color(get_property_font_color());
	button_9->set_font_color(get_property_font_color());
	button_0->set_font_color(get_property_font_color());
	button_dot->set_font_color(get_property_font_color());
	button_minus->set_font_color(get_property_font_color());
	button_c->set_font_color(get_property_font_color());
	button_ok->set_font_color(get_property_font_color());
	button_cancel->set_font_color(get_property_font_color());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_drop_shadow_changed()
{
	button_1->set_drop_shadow(get_property_drop_shadow());
	button_2->set_drop_shadow(get_property_drop_shadow());
	button_3->set_drop_shadow(get_property_drop_shadow());
	button_4->set_drop_shadow(get_property_drop_shadow());
	button_5->set_drop_shadow(get_property_drop_shadow());
	button_6->set_drop_shadow(get_property_drop_shadow());
	button_7->set_drop_shadow(get_property_drop_shadow());
	button_8->set_drop_shadow(get_property_drop_shadow());
	button_9->set_drop_shadow(get_property_drop_shadow());
	button_0->set_drop_shadow(get_property_drop_shadow());
	button_dot->set_drop_shadow(get_property_drop_shadow());
	button_minus->set_drop_shadow(get_property_drop_shadow());
	button_c->set_drop_shadow(get_property_drop_shadow());
	button_ok->set_drop_shadow(get_property_drop_shadow());
	button_cancel->set_drop_shadow(get_property_drop_shadow());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_curve_radius_changed()
{
	button_1->set_curve_radius(get_property_curve_radius());
	button_2->set_curve_radius(get_property_curve_radius());
	button_3->set_curve_radius(get_property_curve_radius());
	button_4->set_curve_radius(get_property_curve_radius());
	button_5->set_curve_radius(get_property_curve_radius());
	button_6->set_curve_radius(get_property_curve_radius());
	button_7->set_curve_radius(get_property_curve_radius());
	button_8->set_curve_radius(get_property_curve_radius());
	button_9->set_curve_radius(get_property_curve_radius());
	button_0->set_curve_radius(get_property_curve_radius());
	button_dot->set_curve_radius(get_property_curve_radius());
	button_minus->set_curve_radius(get_property_curve_radius());
	button_c->set_curve_radius(get_property_curve_radius());
	button_ok->set_curve_radius(get_property_curve_radius());
	button_cancel->set_curve_radius(get_property_curve_radius());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_border_width_changed()
{
	button_1->set_border_width(get_property_border_width());
	button_2->set_border_width(get_property_border_width());
	button_3->set_border_width(get_property_border_width());
	button_4->set_border_width(get_property_border_width());
	button_5->set_border_width(get_property_border_width());
	button_6->set_border_width(get_property_border_width());
	button_7->set_border_width(get_property_border_width());
	button_8->set_border_width(get_property_border_width());
	button_9->set_border_width(get_property_border_width());
	button_0->set_border_width(get_property_border_width());
	button_dot->set_border_width(get_property_border_width());
	button_minus->set_border_width(get_property_border_width());
	button_c->set_border_width(get_property_border_width());
	button_ok->set_border_width(get_property_border_width());
	button_cancel->set_border_width(get_property_border_width());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_border_color_changed()
{
	button_1->set_border_color(get_property_border_color());
	button_2->set_border_color(get_property_border_color());
	button_3->set_border_color(get_property_border_color());
	button_4->set_border_color(get_property_border_color());
	button_5->set_border_color(get_property_border_color());
	button_6->set_border_color(get_property_border_color());
	button_7->set_border_color(get_property_border_color());
	button_8->set_border_color(get_property_border_color());
	button_9->set_border_color(get_property_border_color());
	button_0->set_border_color(get_property_border_color());
	button_dot->set_border_color(get_property_border_color());
	button_minus->set_border_color(get_property_border_color());
	button_c->set_border_color(get_property_border_color());
	button_ok->set_border_color(get_property_border_color());
	button_cancel->set_border_color(get_property_border_color());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_grad_separate_changed()
{
	button_1->set_grad_separate(get_property_grad_separate());
	button_2->set_grad_separate(get_property_grad_separate());
	button_3->set_grad_separate(get_property_grad_separate());
	button_4->set_grad_separate(get_property_grad_separate());
	button_5->set_grad_separate(get_property_grad_separate());
	button_6->set_grad_separate(get_property_grad_separate());
	button_7->set_grad_separate(get_property_grad_separate());
	button_8->set_grad_separate(get_property_grad_separate());
	button_9->set_grad_separate(get_property_grad_separate());
	button_0->set_grad_separate(get_property_grad_separate());
	button_dot->set_grad_separate(get_property_grad_separate());
	button_minus->set_grad_separate(get_property_grad_separate());
	button_c->set_grad_separate(get_property_grad_separate());
	button_ok->set_grad_separate(get_property_grad_separate());
	button_cancel->set_grad_separate(get_property_grad_separate());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_grad_separate_pos_changed()
{
	button_1->set_grad_separate_pos(get_property_grad_separate_pos());
	button_2->set_grad_separate_pos(get_property_grad_separate_pos());
	button_3->set_grad_separate_pos(get_property_grad_separate_pos());
	button_4->set_grad_separate_pos(get_property_grad_separate_pos());
	button_5->set_grad_separate_pos(get_property_grad_separate_pos());
	button_6->set_grad_separate_pos(get_property_grad_separate_pos());
	button_7->set_grad_separate_pos(get_property_grad_separate_pos());
	button_8->set_grad_separate_pos(get_property_grad_separate_pos());
	button_9->set_grad_separate_pos(get_property_grad_separate_pos());
	button_0->set_grad_separate_pos(get_property_grad_separate_pos());
	button_dot->set_grad_separate_pos(get_property_grad_separate_pos());
	button_minus->set_grad_separate_pos(get_property_grad_separate_pos());
	button_c->set_grad_separate_pos(get_property_grad_separate_pos());
	button_ok->set_grad_separate_pos(get_property_grad_separate_pos());
	button_cancel->set_grad_separate_pos(get_property_grad_separate_pos());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_grad_color1_changed()
{
	button_1->set_grad_color1(get_property_grad_color1());
	button_2->set_grad_color1(get_property_grad_color1());
	button_3->set_grad_color1(get_property_grad_color1());
	button_4->set_grad_color1(get_property_grad_color1());
	button_5->set_grad_color1(get_property_grad_color1());
	button_6->set_grad_color1(get_property_grad_color1());
	button_7->set_grad_color1(get_property_grad_color1());
	button_8->set_grad_color1(get_property_grad_color1());
	button_9->set_grad_color1(get_property_grad_color1());
	button_0->set_grad_color1(get_property_grad_color1());
	button_dot->set_grad_color1(get_property_grad_color1());
	button_minus->set_grad_color1(get_property_grad_color1());
	button_c->set_grad_color1(get_property_grad_color1());
	button_ok->set_grad_color1(get_property_grad_color1());
	button_cancel->set_grad_color1(get_property_grad_color1());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_grad_color2_changed()
{
	button_1->set_grad_color2(get_property_grad_color2());
	button_2->set_grad_color2(get_property_grad_color2());
	button_3->set_grad_color2(get_property_grad_color2());
	button_4->set_grad_color2(get_property_grad_color2());
	button_5->set_grad_color2(get_property_grad_color2());
	button_6->set_grad_color2(get_property_grad_color2());
	button_7->set_grad_color2(get_property_grad_color2());
	button_8->set_grad_color2(get_property_grad_color2());
	button_9->set_grad_color2(get_property_grad_color2());
	button_0->set_grad_color2(get_property_grad_color2());
	button_dot->set_grad_color2(get_property_grad_color2());
	button_minus->set_grad_color2(get_property_grad_color2());
	button_c->set_grad_color2(get_property_grad_color2());
	button_ok->set_grad_color2(get_property_grad_color2());
	button_cancel->set_grad_color2(get_property_grad_color2());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_grad_color3_changed()
{
	button_1->set_grad_color3(get_property_grad_color3());
	button_2->set_grad_color3(get_property_grad_color3());
	button_3->set_grad_color3(get_property_grad_color3());
	button_4->set_grad_color3(get_property_grad_color3());
	button_5->set_grad_color3(get_property_grad_color3());
	button_6->set_grad_color3(get_property_grad_color3());
	button_7->set_grad_color3(get_property_grad_color3());
	button_8->set_grad_color3(get_property_grad_color3());
	button_9->set_grad_color3(get_property_grad_color3());
	button_0->set_grad_color3(get_property_grad_color3());
	button_dot->set_grad_color3(get_property_grad_color3());
	button_minus->set_grad_color3(get_property_grad_color3());
	button_c->set_grad_color3(get_property_grad_color3());
	button_ok->set_grad_color3(get_property_grad_color3());
	button_cancel->set_grad_color3(get_property_grad_color3());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_grad_color4_changed()
{
	button_1->set_grad_color4(get_property_grad_color4());
	button_2->set_grad_color4(get_property_grad_color4());
	button_3->set_grad_color4(get_property_grad_color4());
	button_4->set_grad_color4(get_property_grad_color4());
	button_5->set_grad_color4(get_property_grad_color4());
	button_6->set_grad_color4(get_property_grad_color4());
	button_7->set_grad_color4(get_property_grad_color4());
	button_8->set_grad_color4(get_property_grad_color4());
	button_9->set_grad_color4(get_property_grad_color4());
	button_0->set_grad_color4(get_property_grad_color4());
	button_dot->set_grad_color4(get_property_grad_color4());
	button_minus->set_grad_color4(get_property_grad_color4());
	button_c->set_grad_color4(get_property_grad_color4());
	button_ok->set_grad_color4(get_property_grad_color4());
	button_cancel->set_grad_color4(get_property_grad_color4());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_active_grad_color1_changed()
{
	button_1->set_active_grad_color1(get_property_active_grad_color1());
	button_2->set_active_grad_color1(get_property_active_grad_color1());
	button_3->set_active_grad_color1(get_property_active_grad_color1());
	button_4->set_active_grad_color1(get_property_active_grad_color1());
	button_5->set_active_grad_color1(get_property_active_grad_color1());
	button_6->set_active_grad_color1(get_property_active_grad_color1());
	button_7->set_active_grad_color1(get_property_active_grad_color1());
	button_8->set_active_grad_color1(get_property_active_grad_color1());
	button_9->set_active_grad_color1(get_property_active_grad_color1());
	button_0->set_active_grad_color1(get_property_active_grad_color1());
	button_dot->set_active_grad_color1(get_property_active_grad_color1());
	button_minus->set_active_grad_color1(get_property_active_grad_color1());
	button_c->set_active_grad_color1(get_property_active_grad_color1());
	button_ok->set_active_grad_color1(get_property_active_grad_color1());
	button_cancel->set_active_grad_color1(get_property_active_grad_color1());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_active_grad_color2_changed()
{
	button_1->set_active_grad_color2(get_property_active_grad_color2());
	button_2->set_active_grad_color2(get_property_active_grad_color2());
	button_3->set_active_grad_color2(get_property_active_grad_color2());
	button_4->set_active_grad_color2(get_property_active_grad_color2());
	button_5->set_active_grad_color2(get_property_active_grad_color2());
	button_6->set_active_grad_color2(get_property_active_grad_color2());
	button_7->set_active_grad_color2(get_property_active_grad_color2());
	button_8->set_active_grad_color2(get_property_active_grad_color2());
	button_9->set_active_grad_color2(get_property_active_grad_color2());
	button_0->set_active_grad_color2(get_property_active_grad_color2());
	button_dot->set_active_grad_color2(get_property_active_grad_color2());
	button_minus->set_active_grad_color2(get_property_active_grad_color2());
	button_c->set_active_grad_color2(get_property_active_grad_color2());
	button_ok->set_active_grad_color2(get_property_active_grad_color2());
	button_cancel->set_active_grad_color2(get_property_active_grad_color2());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_active_grad_color3_changed()
{
	button_1->set_active_grad_color3(get_property_active_grad_color3());
	button_2->set_active_grad_color3(get_property_active_grad_color3());
	button_3->set_active_grad_color3(get_property_active_grad_color3());
	button_4->set_active_grad_color3(get_property_active_grad_color3());
	button_5->set_active_grad_color3(get_property_active_grad_color3());
	button_6->set_active_grad_color3(get_property_active_grad_color3());
	button_7->set_active_grad_color3(get_property_active_grad_color3());
	button_8->set_active_grad_color3(get_property_active_grad_color3());
	button_9->set_active_grad_color3(get_property_active_grad_color3());
	button_0->set_active_grad_color3(get_property_active_grad_color3());
	button_dot->set_active_grad_color3(get_property_active_grad_color3());
	button_minus->set_active_grad_color3(get_property_active_grad_color3());
	button_c->set_active_grad_color3(get_property_active_grad_color3());
	button_ok->set_active_grad_color3(get_property_active_grad_color3());
	button_cancel->set_active_grad_color3(get_property_active_grad_color3());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_active_grad_color4_changed()
{
	button_1->set_active_grad_color4(get_property_active_grad_color4());
	button_2->set_active_grad_color4(get_property_active_grad_color4());
	button_3->set_active_grad_color4(get_property_active_grad_color4());
	button_4->set_active_grad_color4(get_property_active_grad_color4());
	button_5->set_active_grad_color4(get_property_active_grad_color4());
	button_6->set_active_grad_color4(get_property_active_grad_color4());
	button_7->set_active_grad_color4(get_property_active_grad_color4());
	button_8->set_active_grad_color4(get_property_active_grad_color4());
	button_9->set_active_grad_color4(get_property_active_grad_color4());
	button_0->set_active_grad_color4(get_property_active_grad_color4());
	button_dot->set_active_grad_color4(get_property_active_grad_color4());
	button_minus->set_active_grad_color4(get_property_active_grad_color4());
	button_c->set_active_grad_color4(get_property_active_grad_color4());
	button_ok->set_active_grad_color4(get_property_active_grad_color4());
	button_cancel->set_active_grad_color4(get_property_active_grad_color4());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_draw_inactive_changed()
{
	button_1->set_draw_inactive(get_property_draw_inactive());
	button_2->set_draw_inactive(get_property_draw_inactive());
	button_3->set_draw_inactive(get_property_draw_inactive());
	button_4->set_draw_inactive(get_property_draw_inactive());
	button_5->set_draw_inactive(get_property_draw_inactive());
	button_6->set_draw_inactive(get_property_draw_inactive());
	button_7->set_draw_inactive(get_property_draw_inactive());
	button_8->set_draw_inactive(get_property_draw_inactive());
	button_9->set_draw_inactive(get_property_draw_inactive());
	button_0->set_draw_inactive(get_property_draw_inactive());
	button_dot->set_draw_inactive(get_property_draw_inactive());
	button_minus->set_draw_inactive(get_property_draw_inactive());
	button_c->set_draw_inactive(get_property_draw_inactive());
	button_ok->set_draw_inactive(get_property_draw_inactive());
	button_cancel->set_draw_inactive(get_property_draw_inactive());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_frame_changed()
{
	frame->set_shadow_type(property_labelFrame.get_value());
}
// -------------------------------------------------------------------------
void UDKeyboard::on_connect_to_entry_changed()
{
	disconnect_consumer_widgets(consumer_widget_connection_list);
	if(property_use_keyboard && !is_glade_editor)
		to_lo_erarchy(get_toplevel(),NULL);
}
// -------------------------------------------------------------------------
void UDKeyboard::disconnect_consumer_widgets(ConnectionsList& list)
{
	if(list.empty())
		return;
	
	for(ConnectionsList::iterator it = list.begin(); it != list.end(); ++it)
		(*it).disconnect();
	list.clear();
}
// -------------------------------------------------------------------------
void UDKeyboard::on_realize()
{
	UEventBox::on_realize();
	keyboard->set_decorated(is_glade_editor); //Установить декорацию
	if(!is_glade_editor)
	{
		keyboard->set_modal(true);
		on_connect_to_entry_changed();
	}
	
	on_button_size_changed();
	on_font_name_changed();
	on_font_size_changed();
	on_font_color_changed();
	on_drop_shadow_changed();
	on_curve_radius_changed();
	on_border_width_changed();
	on_border_color_changed();
	on_grad_separate_changed();
	on_grad_separate_pos_changed();
	on_grad_color1_changed();
	on_grad_color2_changed();
	on_grad_color3_changed();
	on_grad_color4_changed();
	on_active_grad_color1_changed();
	on_active_grad_color2_changed();
	on_active_grad_color3_changed();
	on_active_grad_color4_changed();
	on_draw_inactive_changed();
	on_frame_changed();
}
// -------------------------------------------------------------------------
void UDKeyboard::to_lo_erarchy(Gtk::Widget* w, Gtk::Widget* parent)
{
	Glib::ListHandle<Gtk::Widget*> childlist = (dynamic_cast<Gtk::Container*>(w))->get_children();
	for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); ++it)
	{
		
		if(Gtk::Entry* entry = dynamic_cast<Gtk::Entry*>(*it))
		{
			if(entry->property_editable())
			{
				consumer_widget_connection_list.push_back(entry->signal_event().connect(sigc::bind(sigc::mem_fun(*this, &UDKeyboard::on_any_entry_event),(*it),parent)));
			}
		}
		else if(UniOscillograph* oscil = dynamic_cast<UniOscillograph*>(*it))
		{
			to_lo_erarchy(oscil->get_cs_dialog(),oscil);
			to_lo_erarchy(oscil->get_set_dialog(),oscil);
		}
		else if(UniSVGDialogButton* dialogbt = dynamic_cast<UniSVGDialogButton*>(*it))
			to_lo_erarchy(dialogbt->get_dialog(),dialogbt);
		else if(Gtk::Container* cont = dynamic_cast<Gtk::Container*>(*it))
			to_lo_erarchy(cont,parent);
	}
}
// -------------------------------------------------------------------------
bool UDKeyboard::on_area_event(GdkEvent* ev)
{
//	if(ev->type!=GDK_EXPOSE)
//		cout<<get_name()<<"::on_area_event event->type="<<ev->type<< endl;
	GdkEventButton event_btn = ev->button;
	
	if(event_btn.type == GDK_BUTTON_PRESS)
	{
		if(is_glade_editor)
			keyboard->show();
	}
	
	return false;
}
// -------------------------------------------------------------------------
bool UDKeyboard::on_any_entry_event(GdkEvent* ev, Gtk::Widget* consumer, Gtk::Widget* parent)
{
	GdkEventButton event_btn = ev->button;
	
	if(event_btn.type == GDK_BUTTON_PRESS)
	{
		start_edit(consumer, parent);
	}
	
	return false;
}
// -------------------------------------------------------------------------
bool UDKeyboard::start_edit(Gtk::Widget* consumer, Gtk::Widget* parent)
{
	if(!consumer)
		return false;
	
	consumer_widget = consumer;
	entry_parent = parent;
	
	if(UniOscillograph* oscil = dynamic_cast<UniOscillograph*>(entry_parent))
		oscil->set_block_hide_dialog_by_out_focus(true);
	else if(UniSVGDialogButton* dialogbt = dynamic_cast<UniSVGDialogButton*>(entry_parent))
		dialogbt->set_block_hide_dialog_by_out_focus(true);

	if(Gtk::Entry* entry = dynamic_cast<Gtk::Entry*>(consumer_widget))
	{
		text->set_text(entry->get_text());
	}
	
	keyboard->show();
	init_edit = true;
	
	return true;
}
// -------------------------------------------------------------------------
void UDKeyboard::clicked_ok()
{
	//	keyboard->response(Gtk::RESPONSE_OK);
	if(Gtk::Entry* entry = dynamic_cast<Gtk::Entry*>(consumer_widget))
	{
		entry->set_text(text->get_text());
		entry->activate();
	}
// 	if(Gtk::SpinButton* spinbt = dynamic_cast<Gtk::SpinButton*>(consumer_widget))
// 		spinbt->set_value(atof(text->get_text().c_str()));
	keyboard->hide();
	init_edit = false;
	
	if(UniOscillograph* oscil = dynamic_cast<UniOscillograph*>(entry_parent))
		oscil->set_block_hide_dialog_by_out_focus(false);
	else if(UniSVGDialogButton* dialogbt = dynamic_cast<UniSVGDialogButton*>(entry_parent))
		dialogbt->set_block_hide_dialog_by_out_focus(false);
}
// -------------------------------------------------------------------------
void UDKeyboard::clicked_cancel()
{
	//	keyboard->response(Gtk::RESPONSE_CANCEL);
	keyboard->hide();
	init_edit = false;
	
	if(UniOscillograph* oscil = dynamic_cast<UniOscillograph*>(entry_parent))
		oscil->set_block_hide_dialog_by_out_focus(false);
	else if(UniSVGDialogButton* dialogbt = dynamic_cast<UniSVGDialogButton*>(entry_parent))
		dialogbt->set_block_hide_dialog_by_out_focus(false);
}
// -------------------------------------------------------------------------
void UDKeyboard::edit(Glib::ustring symbol)
{
	Glib::ustring value = (init_edit && property_replace_value) ? "":text->get_text();
	//	cout<<myname<<": "<<value.str()<<""<<text<<endl;
	if(symbol == "◄")
	{
		if(value.size())
			value.resize(value.size()-1);
		if(value.size()==1 && value.find("-")==0)
			text->set_text("");
		else
			text->set_text(value);
	}
	else if(symbol == "±")
	{
		if(!value.empty() || (init_edit && property_replace_value))
		{
			double val = atof(value.c_str());
			if(val>0)
				text->set_text("-"+value);
			else if(val<0)
				text->set_text(value.substr(1));
			else
				text->set_text(value);
		}
	}
	else if(symbol == ",")
	{
		if(value.empty())
			text->set_text("0"+symbol);
		else if(value.find(symbol)>value.size())
			text->set_text(value+symbol);
		else
			text->set_text(value);
	}
	else
	{
		if(!(value.size()==1 && value.find("0")==0 && symbol=="0"))
			text->set_text(value+symbol);
		else
			text->set_text(value);
	}

	init_edit = false;
}
//---------------------------------------------------------------------------------------
bool UDKeyboard::on_keyboard_focus_out_event(GdkEventFocus *event)
{
	if(keyboard && !is_glade_editor)
		clicked_cancel();
	return false;
}
// -------------------------------------------------------------------------
void UDKeyboard::on_add_to_new_parent(Gtk::Widget* prev_parent)
{
}
// -------------------------------------------------------------------------
