#ifndef _UDKEYBOARD_H
#define _UDKEYBOARD_H

#include <unordered_map>
#include <gdkmm.h>
#include "UEventBox.h"
#include "UniOscillograph.h"
#include "usvgwidgets/UniSVGDialogButton.h"
#include "global_macros.h"

class UDKeyboardButton : public Gtk::Button{
public:
	UDKeyboardButton();
	~UDKeyboardButton(){};

	void draw_active( Cairo::RefPtr<Cairo::Context> &cr);
	void draw_normal( Cairo::RefPtr<Cairo::Context> &cr);

	inline Glib::ustring get_font_name(){ return font_name; };
	inline void set_font_name(Glib::ustring val){ font_name=val; };

	inline int get_font_size(){ return font_size; };
	inline void set_font_size(int val){ font_size=val; };

	inline Pango::Alignment get_alignment(){ return alignment; };
	inline void set_alignment(Pango::Alignment val){ alignment=val; };

	inline Gdk::Color get_font_color(){ return font_color; };
	inline void set_font_color(Gdk::Color val){ font_color=val; };

	inline bool get_drop_shadow(){ return drop_shadow; };
	inline void set_drop_shadow(bool val){ drop_shadow=val; };

	inline unsigned int get_curve_radius(){ return curve_radius; };
	inline void set_curve_radius(unsigned int val){ curve_radius=val; };

	inline unsigned int get_border_width(){ return border_width; };
	inline void set_border_width(unsigned int val){ border_width=val; };

	inline Gdk::Color get_border_color(){ return border_color; };
	inline void set_border_color(Gdk::Color val){ border_color=val; };

	inline bool get_grad_separate(){ return grad_separate; };
	inline void set_grad_separate(bool val){ grad_separate=val; };

	inline double get_grad_separate_pos(){ return grad_separate_pos; };
	inline void set_grad_separate_pos(double val){ grad_separate_pos=val; };

	inline Gdk::Color get_grad_color1(){ return grad_color1; };
	inline void set_grad_color1(Gdk::Color val){ grad_color1=val; };

	inline Gdk::Color get_grad_color2(){ return grad_color2; };
	inline void set_grad_color2(Gdk::Color val){ grad_color2=val; };

	inline Gdk::Color get_grad_color3(){ return grad_color3; };
	inline void set_grad_color3(Gdk::Color val){ grad_color3=val; };

	inline Gdk::Color get_grad_color4(){ return grad_color4; };
	inline void set_grad_color4(Gdk::Color val){ grad_color4=val; };

	inline Gdk::Color get_active_grad_color1(){ return active_grad_color1; };
	inline void set_active_grad_color1(Gdk::Color val){ active_grad_color1=val; };

	inline Gdk::Color get_active_grad_color2(){ return active_grad_color2; };
	inline void set_active_grad_color2(Gdk::Color val){ active_grad_color2=val; };

	inline Gdk::Color get_active_grad_color3(){ return active_grad_color3; };
	inline void set_active_grad_color3(Gdk::Color val){ active_grad_color3=val; };

	inline Gdk::Color get_active_grad_color4(){ return active_grad_color4; };
	inline void set_active_grad_color4(Gdk::Color val){ active_grad_color4=val; };

	inline bool get_draw_inactive(){ return draw_inactive; };
	inline void set_draw_inactive(bool val){ draw_inactive=val; };

protected:

	virtual bool on_expose_event(GdkEventExpose *ev);

	Glib::ustring font_name;
	int font_size;
	Pango::Alignment alignment;
	Gdk::Color font_color;
	bool drop_shadow;
	unsigned int curve_radius;
	unsigned int border_width;
	Gdk::Color border_color;
	bool grad_separate;
	double grad_separate_pos;
	Gdk::Color grad_color1;
	Gdk::Color grad_color2;
	Gdk::Color grad_color3;
	Gdk::Color grad_color4;
	Gdk::Color active_grad_color1;
	Gdk::Color active_grad_color2;
	Gdk::Color active_grad_color3;
	Gdk::Color active_grad_color4;
	bool draw_inactive;

};

class UDKeyboard : public UEventBox{
public:
	UDKeyboard();
	explicit UDKeyboard(GtkmmBaseType::BaseObjectType* gobject);
	~UDKeyboard();
	
	virtual void init_widget();
	virtual void on_realize();

	inline Gtk::Button* get_button_c(){return button_c;};
	inline Gtk::Button* get_button_ok(){return button_ok;};
	inline Gtk::Button* get_button_cancel(){return button_cancel;};
	inline Gtk::Button* get_button_1(){return button_1;};
	inline Gtk::Button* get_button_2(){return button_2;};
	inline Gtk::Button* get_button_3(){return button_3;};
	inline Gtk::Button* get_button_4(){return button_4;};
	inline Gtk::Button* get_button_5(){return button_5;};
	inline Gtk::Button* get_button_6(){return button_6;};
	inline Gtk::Button* get_button_7(){return button_7;};
	inline Gtk::Button* get_button_8(){return button_8;};
	inline Gtk::Button* get_button_9(){return button_9;};
	inline Gtk::Button* get_button_0(){return button_0;};
	inline Gtk::Button* get_button_minus(){return button_minus;};
	inline Gtk::Button* get_button_dot(){return button_dot;};
	
protected:
	virtual bool on_expose_event(GdkEventExpose*);
	virtual bool on_area_event(GdkEvent* ev);
	virtual bool on_keyboard_focus_out_event(GdkEventFocus *event);
	virtual void on_add_to_new_parent(Gtk::Widget* prev_parent);
	
	void to_lo_erarchy(Gtk::Widget* w, Gtk::Widget* parent);
	bool on_any_entry_event(GdkEvent* ev, Gtk::Widget* consumer, Gtk::Widget* parent);
	virtual bool start_edit(Gtk::Widget* entry, Gtk::Widget* parent);
	void edit(Glib::ustring symbol);
	bool init_edit;

	Gtk::Window* keyboard;
	Gtk::Frame* frame;
	Gtk::Label* text;
	Gtk::Widget* consumer_widget;
	Gtk::Widget* entry_parent;

	typedef std::list<sigc::connection> ConnectionsList;
	ConnectionsList consumer_widget_connection_list;
	void disconnect_consumer_widgets(ConnectionsList& list);
	
	UDKeyboardButton* button_c;
	UDKeyboardButton* button_ok;
	UDKeyboardButton* button_cancel;
	UDKeyboardButton* button_1;
	UDKeyboardButton* button_2;
	UDKeyboardButton* button_3;
	UDKeyboardButton* button_4;
	UDKeyboardButton* button_5;
	UDKeyboardButton* button_6;
	UDKeyboardButton* button_7;
	UDKeyboardButton* button_8;
	UDKeyboardButton* button_9;
	UDKeyboardButton* button_0;
	UDKeyboardButton* button_minus;
	UDKeyboardButton* button_dot;
	
private:
	void ctor();

	void clicked_ok();
	void clicked_cancel();
	
	void on_connect_to_entry_changed();
	void on_button_size_changed();
	void on_font_name_changed();
	void on_font_size_changed();
	void on_font_color_changed();
	void on_drop_shadow_changed();
	void on_curve_radius_changed();
	void on_border_width_changed();
	void on_border_color_changed();
	void on_grad_separate_changed();
	void on_grad_separate_pos_changed();
	void on_grad_color1_changed();
	void on_grad_color2_changed();
	void on_grad_color3_changed();
	void on_grad_color4_changed();
	void on_active_grad_color1_changed();
	void on_active_grad_color2_changed();
	void on_active_grad_color3_changed();
	void on_active_grad_color4_changed();
	void on_draw_inactive_changed();
	void on_frame_changed();
	
	void on_label_font_changed();
	
	ADD_PROPERTY( property_use_keyboard, bool )				/*!< свойство:*/
	ADD_PROPERTY( property_replace_value, bool )			/*!< свойство:*/
	ADD_PROPERTY( property_width_size, unsigned int )		/*!< свойство:*/
	ADD_PROPERTY( property_height_size, unsigned int )		/*!< свойство:*/
	ADD_PROPERTY( property_labelFrame, Gtk::ShadowType )	/*!< свойство: */
	ADD_PROPERTY( property_label_font_name, Glib::ustring )	/*!< свойство:*/
	ADD_PROPERTY( property_label_font_size, int )			/*!< свойство:*/
	ADD_PROPERTY( property_label_font_color, Gdk::Color )	/*!< свойство:*/
	ADD_PROPERTY( property_font_name, Glib::ustring )		/*!< свойство:*/
	ADD_PROPERTY( property_font_size, int )					/*!< свойство:*/
	ADD_PROPERTY( property_font_color, Gdk::Color )			/*!< свойство:*/
	ADD_PROPERTY( property_drop_shadow, bool )				/*!< свойство:*/
	ADD_PROPERTY( property_curve_radius, unsigned int )		/*!< свойство:*/
	ADD_PROPERTY( property_border_width, unsigned int )		/*!< свойство:*/
	ADD_PROPERTY( property_border_color, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_separate, bool )			/*!< свойство:*/
	ADD_PROPERTY( property_grad_separate_pos, double )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_color1, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_color2, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_color3, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_grad_color4, Gdk::Color )		/*!< свойство:*/
	ADD_PROPERTY( property_active_grad_color1, Gdk::Color )	/*!< свойство:*/
	ADD_PROPERTY( property_active_grad_color2, Gdk::Color )	/*!< свойство:*/
	ADD_PROPERTY( property_active_grad_color3, Gdk::Color )	/*!< свойство:*/
	ADD_PROPERTY( property_active_grad_color4, Gdk::Color )	/*!< свойство:*/
	ADD_PROPERTY( property_draw_inactive, bool )			/*!< свойство:*/
	
	DISALLOW_COPY_AND_ASSIGN(UDKeyboard);

	struct EntryData
	{
		EntryData():
		oscil(nullptr),
		dialog_button(nullptr),
		entry(nullptr)
		{
		};
		Gtk::Entry* entry;
		UniOscillograph* oscil;
		UniSVGDialogButton* dialog_button;
	}__attribute__((packed));
	typedef std::unordered_map<long,EntryData> EntryList;
	EntryList entry_list;
	
};
// -------------------------------------------------------------------------
#endif
