#include <gdkmm.h>
#include <iostream>
#include <fstream>
#include <errno.h>
#include <iomanip>
#include "UDiagram.h"
#include "ConfirmSignal.h"
#include "UniOscilChannel.h"

// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using UMessages::MessageId;
// -------------------------------------------------------------------------
#define INIT_UDiagram_PROPERTIES() \
	 averageSensor(this,"average",get_connector()) \
	 ,prop_staticAverage(*this, "static-average", false) \
	 ,prop_average_value(*this, "average-value", 0) \
	 ,prop_precision(*this, "precision" , 0) \
	 ,prop_refresh_timeout(*this, "refresh-timeout" , 0) \
	 ,prop_dynamicLevels(*this, "dynamic-levels" , false) \
	 ,prop_highWarnLevel(*this, "high-warn-level" , 70) \
	 ,prop_highCritLevel(*this, "high-crit-level" , 80) \
	 ,prop_disableHighLevels(*this, "disable-high-levels" , false) \
	 ,prop_lowWarnLevel(*this, "low-warn-level" , 35) \
	 ,prop_lowCritLevel(*this, "low-crit-level" , 25) \
	 ,prop_disableLowLevels(*this, "disable-low-levels" , false) \
	 ,prop_infoColor(*this, "info-color" , Gdk::Color("green")) \
	 ,prop_warnColor(*this, "warn-color" , Gdk::Color("orange")) \
	 ,prop_critColor(*this, "crit-color" , Gdk::Color("red")) \
	 ,prop_useGradientColor(*this, "use-gradient-color" , false) \
	 ,prop_insensColor(*this, "insens-color" , Gdk::Color("grey")) \
	 ,prop_enableWeb(*this, "enable-web" , true) \
	 ,prop_webMajorColor(*this, "web-major-color" , Gdk::Color("grey")) \
	 ,prop_enableWebMinor(*this, "enable-web-minor" , true) \
	 ,prop_webMinorColor(*this, "web-minor-color" , Gdk::Color("grey")) \
	 ,prop_WebLineType(*this, "web-line-type" , Web::LINE_LINE) \
	 ,prop_scaleColor(*this, "scale-color" , Gdk::Color("black")) \
	 ,prop_scaleLabelColor(*this, "scale-label-color", Gdk::Color("black") ) \
	 ,prop_scaleLabelFont(*this, "scale-label-font", "Liberation Sans Bold" ) \
	 ,prop_scaleLabelFontSize(*this, "scale-label-font-size", 12 ) \
	 ,prop_topScale(*this, "top-scale", true) \
	 ,prop_bottomScale(*this, "bottom-scale", true) \
	 ,prop_leftScale(*this, "left-scale", true) \
	 ,prop_rightScale(*this, "right-scale", true) \
	 ,prop_barWidth(*this, "bar-width", 0) \
	 ,prop_Min(*this, "scale-min" , 0) \
	 ,prop_Max(*this, "scale-max" , 100) \
	 ,prop_XmaxMin(*this, "max-x-scale-minor" , 10) \
	 ,prop_XmaxMaj(*this, "max-x-scale-major" , 10) \
	 ,prop_YmaxMin(*this, "max-y-scale-minor" , 10) \
	 ,prop_YmaxMaj(*this, "max-y-scale-major" , 10) \
	 ,prop_BGImagePath(*this, "bg-image") \
	 ,prop_expandBGImagePath(*this, "expand-bg-image") \
	 ,prop_bgBorderWidth(*this, "bg-border-width", 0 ) \
	 ,prop_enableMinValue(*this, "enable-min-value", false) \
	 ,prop_minValueNum(*this, "min-value-num", 1 ) \
	 ,prop_minValueTextLineNum(*this, "min-value-text-line-num", 1) \
	 ,prop_enableMinValueThreshold(*this, "enable-min-value-threshold", false) \
	 ,prop_minValueThreshold(*this, "min-value-threshold", 35 ) \
	 ,prop_minValueTextFixedPosition(*this, "min-value-text-fixed-position", false) \
	 ,prop_minValueTextColor(*this, "min-value-text-color", Gdk::Color("black") ) \
	 ,prop_minValueDynamicColor(*this, "min-value-dynamic-color", false ) \
	 ,prop_minValueTextFont(*this, "min-value-text-font", "Liberation Sans Bold" ) \
	 ,prop_minValueTextFontSize(*this, "min-value-text-font-size", 12 ) \
	 ,prop_minValueDashStep(*this, "min-value-dash-line-step", 6.0) \
	 ,prop_minValueDashOffset(*this, "min-value-dash-line-offset", 0.0) \
	 ,prop_enableTooltip(*this, "enable-tooltip", false) \
	 ,prop_tooltipImagePath(*this, "tooltip-image") \
	 ,prop_tooltipImageWidth(*this, "tooltip-image-width", 10 ) \
	 ,prop_tooltipImageHeight(*this, "tooltip-image-height", 10) \
	 ,prop_tooltipLabelColor(*this, "tooltip-label-color", Gdk::Color("black") ) \
	 ,prop_tooltipDynamicColor(*this, "tooltip-dynamic-color", false ) \
	 ,prop_tooltipLabelFont(*this, "tooltip-label-font", "Liberation Sans Bold" ) \
	 ,prop_tooltipLabelFontSize(*this, "tooltip-label-font-size", 12 ) \
	 ,prop_enableHisory(*this, "enable-history", false) \
	 ,prop_dir(*this, "history-dir", "/tmp/" ) \
	 ,prop_historyPointInMem(*this, "history-point-in-memory", 100 ) \
	 ,point_x(0) \
	 ,ch_num(0) \
	 ,average(0) \
	 ,last_average(0) \
	 ,show_tooltip(false) \
	 ,history_file_name("") \
	 ,history_file_fullname("") \
	 ,is_show(false) \
	 ,is_expand(false)
// -------------------------------------------------------------------------
void UDiagram::ctor()
{
	tooltip_layout= create_pango_layout("");
	tooltip_layout->set_alignment(Pango::ALIGN_CENTER);

	minValue_layout= create_pango_layout(" ");
	minValue_layout->set_alignment(Pango::ALIGN_CENTER);

	BGImage = manage(new Gtk::Image(get_pixbuf_from_cache("", 1, 1)));
	BGImage->show();
	Gtk::Fixed::on_add(BGImage);

	expandBGImage = manage(new Gtk::Image(get_pixbuf_from_cache("", 1, 1)));
	expandBGImage->hide();
	Gtk::Fixed::on_add(expandBGImage);

	push_composite_child();
	{
		table = manage(new Gtk::Table(3,3));
		Gtk::Fixed::on_add(table);

		plot_area = manage(new Web());
		plot_area->set_enabled(true);
		plot_area->set_h_range(0,100);
		plot_area->set_v_range(0,100);
		table->attach(*plot_area,1,2,1,2);

		topScaleBox = manage(new Gtk::VBox());
		HScaleLabels *topScaleLabel = manage(new HScaleLabels(Gtk::POS_TOP));
		topScale = manage(new HScale(Gtk::POS_TOP,topScaleLabel));
		topScale->set_enabled(true);
		topScale->set_range(0,100);
		topScaleBox->pack_start(*topScaleLabel,Gtk::PACK_SHRINK);
		topScaleBox->pack_start(*topScale,Gtk::PACK_SHRINK);
		table->attach(*topScaleBox,1,2,0,1,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);

		bottomScaleBox = manage(new Gtk::VBox());
		HScaleLabels *bottomScaleLabel = manage(new HScaleLabels(Gtk::POS_BOTTOM));
		bottomScale = manage(new HScale(Gtk::POS_BOTTOM,bottomScaleLabel));
		bottomScale->set_enabled(true);
		bottomScale->set_range(0,100);
		bottomScaleBox->pack_start(*bottomScale,Gtk::PACK_SHRINK);
		bottomScaleBox->pack_start(*bottomScaleLabel,Gtk::PACK_SHRINK);
		table->attach(*bottomScaleBox,1,2,2,3,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);

		leftScaleBox = manage(new Gtk::HBox());
		VScaleLabels *leftScaleLabel = manage(new VScaleLabels(Gtk::POS_LEFT));
		leftScale = manage(new VScale(Gtk::POS_LEFT,leftScaleLabel));
		leftScale->set_enabled(true);
		leftScale->set_range(0,100);
		leftScaleBox->pack_start(*leftScaleLabel,Gtk::PACK_SHRINK);
		leftScaleBox->pack_start(*leftScale,Gtk::PACK_SHRINK);
		table->attach(*leftScaleBox,0,1,1,2,Gtk::SHRINK,Gtk::FILL|Gtk::EXPAND);

		rightScaleBox = manage(new Gtk::HBox());
		VScaleLabels *rightScaleLabel = manage(new VScaleLabels(Gtk::POS_RIGHT));
		rightScale = manage(new VScale(Gtk::POS_RIGHT,rightScaleLabel));
		rightScale->set_enabled(true);
		rightScale->set_range(0,100);
		rightScaleBox->pack_start(*rightScale,Gtk::PACK_SHRINK);
		rightScaleBox->pack_start(*rightScaleLabel,Gtk::PACK_SHRINK);
		table->attach(*rightScaleBox,2,3,1,2,Gtk::SHRINK,Gtk::FILL|Gtk::EXPAND);

		show_all();
	}
	pop_composite_child();

	//popup menu:
	menu_popup = manage( new Gtk::Menu() );
	{
		Gtk::Menu::MenuList& menulist = menu_popup->items();

		mi_set_expand = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::FULLSCREEN,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Развернуть")));
		mi_set_expand->signal_activate().connect(sigc::mem_fun(*this, &UDiagram::diagram_expand));
		mi_set_expand->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_expand) );

		mi_set_web = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::PREFERENCES,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Показать сетку")));
		mi_set_web->signal_activate().connect(sigc::mem_fun(*this, &UDiagram::set_diagram_web));
		mi_set_web->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_web) );

		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );

		mi_save_channel = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::SAVE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Сохранить в файл CSV канал")));
		mi_save_channel->signal_activate().connect(sigc::mem_fun(*this, &UDiagram::save_channel));
		mi_save_channel->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_save_channel) );

		mi_save_all_channels = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::SAVE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Сохранить все каналы в отдельные файлы CSV")));
		mi_save_all_channels->signal_activate().connect(sigc::mem_fun(*this, &UDiagram::save_all_channels));
		mi_save_all_channels->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_save_all_channels) );

		mi_save_all_in_one = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::SAVE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Сохранить все каналы в один файл CSV")));
		mi_save_all_in_one->signal_activate().connect(sigc::mem_fun(*this, &UDiagram::save_all_channels_in_one_file));
		mi_save_all_in_one->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_save_all_in_one) );
	}
	menu_popup->accelerate(*plot_area);

	signal_size_allocate().connect(sigc::mem_fun(*this, &UDiagram::diagram_resize));

	connect_property_changed("enable-web", sigc::mem_fun(*this, &UDiagram::on_web_enable_changed) );
	connect_property_changed("enable-web-minor", sigc::mem_fun(*this, &UDiagram::on_web_minor_enable_changed) );
	connect_property_changed("web-major-color", sigc::mem_fun(*this, &UDiagram::on_web_major_color_changed) );
	connect_property_changed("web-minor-color", sigc::mem_fun(*this, &UDiagram::on_web_minor_color_changed) );
	connect_property_changed("web-line-type", sigc::mem_fun(*this, &UDiagram::on_web_line_type_changed) );
	connect_property_changed("top-scale", sigc::mem_fun(*this, &UDiagram::on_scales_enable_changed) );
	connect_property_changed("bottom-scale", sigc::mem_fun(*this, &UDiagram::on_scales_enable_changed) );
	connect_property_changed("left-scale", sigc::mem_fun(*this, &UDiagram::on_scales_enable_changed) );
	connect_property_changed("right-scale", sigc::mem_fun(*this, &UDiagram::on_scales_enable_changed) );
	connect_property_changed("scale-min", sigc::mem_fun(*this, &UDiagram::on_scales_range_changed) );
	connect_property_changed("scale-max", sigc::mem_fun(*this, &UDiagram::on_scales_range_changed) );
	connect_property_changed("scale-color", sigc::mem_fun(*this, &UDiagram::on_scale_color_changed) );
	connect_property_changed("scale-label-color", sigc::mem_fun(*this, &UDiagram::on_font_color_changed) );
	connect_property_changed("scale-label-font", sigc::mem_fun(*this, &UDiagram::on_font_changed) );
	connect_property_changed("scale-label-font-size", sigc::mem_fun(*this, &UDiagram::on_font_changed) );
	connect_property_changed("average-value", sigc::mem_fun(*this, &UDiagram::on_average_changed) );
	connect_property_changed("max-y-scale-minor", sigc::mem_fun(*this, &UDiagram::on_v_scale_changed) );
	connect_property_changed("max-y-scale-major", sigc::mem_fun(*this, &UDiagram::on_v_scale_changed) );
	connect_property_changed("max-x-scale-minor", sigc::mem_fun(*this, &UDiagram::on_h_scale_changed) );
	connect_property_changed("max-x-scale-major", sigc::mem_fun(*this, &UDiagram::on_h_scale_changed) );
	connect_property_changed("bg-image", sigc::mem_fun(*this, &UDiagram::on_bg_image_changed) );
	connect_property_changed("expand-bg-image", sigc::mem_fun(*this, &UDiagram::on_expand_bg_image_changed) );
	connect_property_changed("bg-border-width", sigc::mem_fun(*this, &UDiagram::on_bg_border_width_changed) );
	connect_property_changed("refresh-timeout", sigc::mem_fun(*this, &UDiagram::on_refresh_timeout_changed) );
	connect_property_changed("tooltip-image", sigc::mem_fun(*this, &UDiagram::on_tooltip_image_changed) );
	connect_property_changed("tooltip-image-width", sigc::mem_fun(*this, &UDiagram::on_tooltip_image_changed) );
	connect_property_changed("tooltip-image-height", sigc::mem_fun(*this, &UDiagram::on_tooltip_image_changed) );
	connect_property_changed("tooltip-label-font", sigc::mem_fun(*this, &UDiagram::on_tooltip_font_changed) );
	connect_property_changed("tooltip-label-font-size", sigc::mem_fun(*this, &UDiagram::on_tooltip_font_changed) );
	connect_property_changed("enable-min-value", sigc::mem_fun(*this, &UDiagram::on_enable_min_value_changed) );
	connect_property_changed("min-value-text-font", sigc::mem_fun(*this, &UDiagram::on_min_value_font_changed) );
	connect_property_changed("min-value-text-font-size", sigc::mem_fun(*this, &UDiagram::on_min_value_font_changed) );
	connect_property_changed("min-value-text-line-num", sigc::mem_fun(*this, &UDiagram::on_min_value_text_line_num_changed) );
	connect_property_changed("enable-history", sigc::mem_fun(*this, &UDiagram::on_history_enable_changed) );
	connect_property_changed("history-dir", sigc::mem_fun(*this, &UDiagram::on_history_dir_changed) );

	plot_area->set_events(~Gdk::ALL_EVENTS_MASK);
	plot_area->signal_event().connect(sigc::mem_fun(*this, &UDiagram::on_plot_area_event));
}
// -------------------------------------------------------------------------
UDiagram::UDiagram() :
		Glib::ObjectBase("udiagram")
		,INIT_UDiagram_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UDiagram::UDiagram(GtkmmBaseType::BaseObjectType* gobject) :
		BaseType(gobject)
		,INIT_UDiagram_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UDiagram::~UDiagram()
{
}
// -------------------------------------------------------------------------
bool UDiagram::on_expose_event(GdkEventExpose* ev)
{
	bool rv = Gtk::Fixed::on_expose_event(ev);
//	cout<<get_name()<<"::on_expose_event event->type="<<ev->type<< endl;
#if GTK_VERSION_GE(2,20)
	if(!get_mapped())
#else
	if(!is_show)
#endif
		return false;

	Cairo::RefPtr<Cairo::Context> cr = plot_area->get_window()->create_cairo_context();
	Gtk::Allocation alloc = plot_area->get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
//	cr->save();
	cr->translate(alloc.get_x(), alloc.get_y());

	double x_step = (double)width/ch_num;
	double x=0;
	double average_pos = abs((average - prop_Min.get_value())/range*height);

	//draw average line
	cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
	cr->set_line_width(1);
	if((!prop_disableHighLevels && average>=prop_highCritLevel.get_value()) ||
	    (!prop_disableLowLevels && average<=prop_lowCritLevel.get_value())
	  )
		Gdk::Cairo::set_source_color(cr, prop_critColor.get_value());
	else if((!prop_disableHighLevels && average>=prop_highWarnLevel.get_value()) ||
		(!prop_disableLowLevels && average<=prop_lowWarnLevel.get_value())
	       )
		Gdk::Cairo::set_source_color(cr, prop_warnColor.get_value());
	else
		Gdk::Cairo::set_source_color(cr, prop_infoColor.get_value());
	cr->move_to(0, height - average_pos);
	cr->line_to(width, height - average_pos);
	cr->stroke();

	if(prop_useGradientColor)
	{
		double highCrit_pos, highWarn_pos, midle_pos, lowWarn_pos, lowCrit_pos;
		highCrit_pos = abs((prop_highCritLevel.get_value() - prop_Max.get_value())/range);
		highWarn_pos = abs((prop_highWarnLevel.get_value() - prop_Max.get_value())/range);
		midle_pos = abs((abs(prop_highWarnLevel.get_value()/2 - prop_lowWarnLevel.get_value()/2) + prop_lowWarnLevel.get_value() - prop_Max.get_value())/range);
		lowWarn_pos = abs((prop_lowWarnLevel.get_value() - prop_Max.get_value())/range);
		lowCrit_pos = abs((prop_lowCritLevel.get_value() - prop_Max.get_value())/range);

		Cairo::RefPtr<Cairo::LinearGradient> gradient = Cairo::LinearGradient::create(0, 0, 0, height);
		if(!prop_disableHighLevels)
		{
			if(!prop_critColor.get_value().to_string().empty())
			{
				gradient->add_color_stop_rgb(0.0,	   prop_critColor.get_value().get_red_p(),  prop_critColor.get_value().get_green_p(),  prop_critColor.get_value().get_blue_p());
				gradient->add_color_stop_rgb(highCrit_pos, prop_critColor.get_value().get_red_p(),  prop_critColor.get_value().get_green_p(),  prop_critColor.get_value().get_blue_p());
			}
			if(!prop_warnColor.get_value().to_string().empty())
				gradient->add_color_stop_rgb(highWarn_pos, prop_warnColor.get_value().get_red_p(),  prop_warnColor.get_value().get_green_p(),  prop_warnColor.get_value().get_blue_p());
			if(!prop_infoColor.get_value().to_string().empty())
				gradient->add_color_stop_rgb(midle_pos,	   prop_infoColor.get_value().get_red_p(),  prop_infoColor.get_value().get_green_p(),  prop_infoColor.get_value().get_blue_p());
		}
		else if(!prop_infoColor.get_value().to_string().empty())
			gradient->add_color_stop_rgb(0.0,	   prop_infoColor.get_value().get_red_p(),  prop_infoColor.get_value().get_green_p(),  prop_infoColor.get_value().get_blue_p());
		if(!prop_disableLowLevels)
		{
			if(!prop_infoColor.get_value().to_string().empty())
				gradient->add_color_stop_rgb(midle_pos,	   prop_infoColor.get_value().get_red_p(),  prop_infoColor.get_value().get_green_p(),  prop_infoColor.get_value().get_blue_p());
			if(!prop_warnColor.get_value().to_string().empty())
				gradient->add_color_stop_rgb(lowWarn_pos,  prop_warnColor.get_value().get_red_p(),  prop_warnColor.get_value().get_green_p(),  prop_warnColor.get_value().get_blue_p());
			if(!prop_critColor.get_value().to_string().empty())
			{
				gradient->add_color_stop_rgb(lowCrit_pos,  prop_critColor.get_value().get_red_p(),  prop_critColor.get_value().get_green_p(),  prop_critColor.get_value().get_blue_p());
				gradient->add_color_stop_rgb(1.0,	   prop_critColor.get_value().get_red_p(),  prop_critColor.get_value().get_green_p(),  prop_critColor.get_value().get_blue_p());
			}
		}
		else if(!prop_infoColor.get_value().to_string().empty())
			gradient->add_color_stop_rgb(1.0,	   prop_infoColor.get_value().get_red_p(),  prop_infoColor.get_value().get_green_p(),  prop_infoColor.get_value().get_blue_p());
		cr->set_source(gradient);
	}

	//draw channels
	Gdk::Color color;
	cr->set_line_cap(Cairo::LINE_CAP_ROUND);
	for(ChannelsList::iterator it = channels.begin();it != channels.end();it++)
	{
		double ch_value = (*it)->get_fvalue();
/*
//		cout<<get_name()<<"::on_expose_event average="<<average<<" last_average="<<last_average<<" ch_value="<<ch_value<<" last_fvalue="<<(*it)->get_last_fvalue()<< endl;
		if( (ch_value == (*it)->get_last_fvalue() && average == last_average) ||
		     ch_value == average
		  )
		{
//			cout<<get_name()<<"::on_expose_event!!! average="<<average<<" last_average="<<last_average<<" ch_value="<<ch_value<<" last_fvalue="<<(*it)->get_last_fvalue()<< endl;
			x = x + x_step;
			continue;
		}
		(*it)->equate_value();
*/
		double size = abs((ch_value - average)/range*height);
//		cout<<get_name()<<"::on_expose_event average="<<prop_average_value.get_value()<<" x_step="<<x_step<<" size="<<size<<" average_pos="<<average_pos<< endl;

		if(!prop_useGradientColor)
		{
			Gdk::Cairo::set_source_color(cr, (*it)->get_channelcolor());
		}
		
		double width = prop_barWidth;
		if(width == 0)
			width = x_step;
		if(ch_value > average)
			cr->rectangle( x, height - size - average_pos, width, size);
		else if(ch_value < average)
			cr->rectangle( x, height - average_pos, width, size);
		cr->fill();

		x = x + x_step;
//		cr->save();
	}
//	cr->restore();
	draw_min_value();

	if(prop_enableTooltip && show_tooltip)
	{
		Glib::RefPtr<Gdk::Pixbuf> tmp_image = tooltip_image->copy();
		int x,y;
		Gdk::ModifierType mtype;
		plot_area->get_window()->get_pointer(x,y,mtype);
//		cout<<get_name()<<":on_plot_area_event... p_x="<<p_x<<" p_y="<<p_y<<" mtype="<<mtype<<endl;
		int num = (x - alloc.get_x())/x_step;
		int x_pos = x - alloc.get_x();
		int y_pos = y - alloc.get_y() - prop_tooltipImageHeight.get_value();

		if(x_pos > width - prop_tooltipImageWidth.get_value())
		{
			tmp_image = tmp_image->flip(true);
			x_pos = x_pos > width ? width - prop_tooltipImageWidth.get_value():x_pos - prop_tooltipImageWidth.get_value();
		}
		else if(x < alloc.get_x())
		{
			x_pos = 0;
		}
		if(y > height + alloc.get_y())
		{
			y_pos = height - prop_tooltipImageHeight.get_value();
		}
		else if(y < alloc.get_y() + prop_tooltipImageHeight.get_value())
		{
			tmp_image = tmp_image->flip(false);
			y_pos = y - alloc.get_y() > 0 ? y - alloc.get_y():0;
		}

		//draw bg image
		Gdk::Cairo::set_source_pixbuf(cr,tmp_image, x_pos, y_pos);
		cr->paint();

		//draw text
		if(num>ch_num-1)
			num = ch_num-1;
		else if(num<0)
			num = 0;
		int layw,layh;
		std::ostringstream tmp;
		string name = channels[num]->get_channelname();
		tmp<< name << endl << channels[num]->get_fvalue();
		tooltip_layout->set_text(tmp.str());
		tooltip_layout->context_changed();
		tooltip_layout->get_pixel_size(layw,layh);
		if(prop_tooltipDynamicColor)
			Gdk::Cairo::set_source_color(cr, channels[num]->get_channelcolor());
		else
			Gdk::Cairo::set_source_color(cr, prop_tooltipLabelColor.get_value());
		double lay_xpos = x_pos + (double(prop_tooltipImageWidth.get_value()) -layw) / 2;
		double lay_ypos = y_pos + (double(prop_tooltipImageHeight.get_value())-layh) / 2;
		cr->move_to(lay_xpos, lay_ypos);
		tooltip_layout->add_to_cairo_context(cr);
		cr->fill();
	}

	if ( !connected_ && get_property_disconnect_effect() == 1)
	{
		alloc.set_x(0);
		alloc.set_y(0);
		UVoid::draw_disconnect_effect_1(cr, alloc);
	}
	return false;
}
// -------------------------------------------------------------------------
void UDiagram::draw_min_value()
{
#if GTK_VERSION_GE(2,20)
	if(!get_mapped()|| !prop_enableMinValue)
#else
	if(!is_show || !prop_enableMinValue)
#endif
		return;
	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
	cr->translate(alloc.get_x(), alloc.get_y());

	int h = alloc.get_height();
	double x_step = (double)width/ch_num;
	double offset_w = plot_area->get_allocation().get_x() - alloc.get_x() + x_step/2;

	std::vector<double> dashes(2);
	dashes[0] = prop_minValueDashStep.get_value();
	dashes[1] = prop_minValueDashStep.get_value()/2;
	cr->set_dash(dashes, prop_minValueDashOffset.get_value());
	cr->set_line_width(1);

	typedef std::map<int,double> LayoutUsedSpace;
	LayoutUsedSpace lines;
	for(int i=0;i<prop_minValueTextLineNum.get_value();i++)
		lines[i]=0;

	for(MinValueChannelMap::iterator mvIt = min_value_channels.begin();mvIt != min_value_channels.end();mvIt++)
	{
//		cout<<get_name()<<":draw_min_value... num="<<mvIt->first<<" value="<<mvIt->second.value<<endl;
		int layw,layh;
		std::ostringstream tmp;
		tmp << mvIt->second.value;
		minValue_layout->set_text(tmp.str());
		minValue_layout->context_changed();
		minValue_layout->get_pixel_size(layw,layh);
		if(prop_minValueDynamicColor)
			Gdk::Cairo::set_source_color(cr, mvIt->second.color);
		else
			Gdk::Cairo::set_source_color(cr, prop_minValueTextColor.get_value());

		double line_xpos = offset_w + (mvIt->first)*x_step;
		double line_ypos;
		if(mvIt->second.value < average)
			line_ypos = plot_area->get_allocation().get_y() - alloc.get_y() + height - abs((mvIt->second.value - prop_Min.get_value())/range*height);
		else
			line_ypos = plot_area->get_allocation().get_y() - alloc.get_y() + height - abs((average - prop_Min.get_value())/range*height);
		double lay_xpos = offset_w + (mvIt->first)*x_step - double(layw)/2;

		if(prop_minValueTextFixedPosition)
		{
			int linenum = mvIt->first % prop_minValueTextLineNum.get_value();
			//draw line
			cr->move_to(line_xpos, line_ypos);
			cr->line_to(line_xpos, h-layh*(prop_minValueTextLineNum.get_value() - linenum));
			cr->stroke();
			//draw layout
			cr->move_to(lay_xpos, h-layh*(prop_minValueTextLineNum.get_value() - linenum));
		}
		else
		{
			for(int i=0;i<prop_minValueTextLineNum.get_value();i++)
			{
				if(lines[i]>lay_xpos)
					continue;
				//draw line
				cr->move_to(line_xpos, line_ypos);
				cr->line_to(line_xpos, h-layh*(prop_minValueTextLineNum.get_value() - i));
				cr->stroke();
				//draw layout
				lines[i] = lay_xpos+layw;
				cr->move_to(lay_xpos, h-layh*(prop_minValueTextLineNum.get_value() - i));
				break;
			}
		}
		minValue_layout->add_to_cairo_context(cr);
		cr->fill();
	}

}
// -------------------------------------------------------------------------
void UDiagram::on_refresh_timeout_changed()
{
	if (tmr.connected())
		tmr.disconnect();
	if(prop_refresh_timeout.get_value()>0)
		tmr= Glib::signal_timeout().connect(sigc::mem_fun(*this, &UDiagram::on_timer_tick),prop_refresh_timeout.get_value());
}
// -------------------------------------------------------------------------
bool UDiagram::on_timer_tick()
{
	double avg=0;
	int num = 0;
	time_t tm = time(0);
	min_value.clear();

	for(ChannelsList::iterator it = channels.begin();it != channels.end();it++)
	{
		double ch_value = (*it)->get_fvalue();
		Gdk::Color color;
		if((!prop_disableHighLevels && ch_value>=prop_highCritLevel.get_value()) ||
		   (!prop_disableLowLevels && ch_value<=prop_lowCritLevel.get_value())
		  )
			color = prop_critColor.get_value();
		else if((!prop_disableHighLevels && ch_value>=prop_highWarnLevel.get_value()) ||
			(!prop_disableLowLevels && ch_value<=prop_lowWarnLevel.get_value())
		       )
			color = prop_warnColor.get_value();
		else
			color = prop_infoColor.get_value();
		(*it)->set_channelcolor(color);

//		if(!prop_staticAverage)
			avg = avg + ch_value;


		if(prop_enableHisory && connected_)
		{
			ChannelHistoryData channel_data;
			channel_data.tm = tm;
			channel_data.value = ch_value;
			history[point_x][num] = channel_data;
		}

		if(prop_enableMinValue)
		{
			if( prop_enableMinValueThreshold && (ch_value > prop_minValueThreshold.get_value()) )
			{
				num++;
				continue;
			}
			if(min_value.size()<prop_minValueNum.get_value())
			{
				MinValueMap::iterator mvIt = min_value.find(ch_value);
				if(mvIt != min_value.end())
				{
					(mvIt->second)[num] = color;
				}
				else
					min_value[ch_value][num] = color;
			}
			else if(min_value.rbegin()->first >= ch_value)
			{
				MinValueMap::iterator mvIt = min_value.find(ch_value);
				if(mvIt != min_value.end())
				{
					(mvIt->second)[num] = color;
				}
				else
				{
					min_value.erase(--(min_value.rbegin().base()));
					min_value[ch_value][num] = color;
				}
			}
		}
		num++;
	}

	if(connected_ && averageSensor.get_sens_id()!=DefaultObjectId)
		averageSensor.save_value( lround(avg/channels.size()*pow10(prop_precision.get_value())) );

	if(prop_enableHisory && connected_)
	{
		if(prop_historyPointInMem.get_value()>0)
		{
			if( history.size() >= prop_historyPointInMem.get_value() )
			{
				FILE* outfile = 0;
				bool outfileIsOpen;
				if(history_file_fullname.empty())
					on_history_dir_changed();
				cout<< get_name() << " history_file_fullname="<<history_file_fullname<<endl;

				fstream fout( history_file_fullname.c_str(), ios::out | ios::app | ios::binary );
				if(!fout)
				{
					cout<< get_name() << " can't open file="<<history_file_fullname<<" for write!"<< endl;
					outfileIsOpen = false;
				}
				else
				{
					fout.close();
					outfile = fopen(history_file_fullname.c_str(),"a");
					outfileIsOpen = true;
				}

				if(outfileIsOpen)
				{
					for(HistoryMap::iterator it = history.begin(); it!=history.end(); it++ )
					{
						for(ChannelsDataMap::iterator chIt = it->second.begin(); chIt!=it->second.end(); chIt++ )
						{
							if(!outfile)
								cout<< get_name() << " can't open file="<<history_file_fullname<<" for write!"<< endl;
							else if(fwrite(&(chIt->second),sizeof(chIt->second),1,outfile) < 1)
								cout<< get_name() << " can't write to file="<<history_file_fullname<< endl;
						}
					}
					fclose(outfile);
					history.clear();
				}
			}
		}
	}

	if(min_value.size() && prop_enableMinValue)
	{
		min_value_channels.clear();
		for(MinValueMap::iterator mvIt = min_value.begin();mvIt != min_value.end();mvIt++)
		{
			for(MinValueLine::iterator it = (mvIt->second).begin();it != (mvIt->second).end();it++)
			{
				min_value_channels[it->first].value = mvIt->first;
				min_value_channels[it->first].color = it->second;
//				cout<<get_name()<<":on_timer_tick... num="<<it->first<<" value="<<mvIt->first<<endl;
			}
		}
	}

	if(channels.size() && !prop_staticAverage)
	{
		last_average = average;
		prop_average_value.set_value(avg/channels.size());
	}

	point_x = point_x + (double)prop_refresh_timeout.get_value()/1000;

	queue_draw();
	return true;
}// -------------------------------------------------------------------------
void UDiagram::on_realize()
{
	Gtk::Fixed::on_realize();
	on_web_minor_enable_changed();
	on_web_major_color_changed();
	on_web_minor_color_changed();
	on_tooltip_image_changed();
	on_tooltip_font_changed();
	on_font_changed();
	on_min_value_font_changed();
	on_enable_min_value_changed();
	on_history_enable_changed();
	on_bg_image_changed();
//	on_expand_bg_image_changed();
}
// -------------------------------------------------------------------------
void UDiagram::diagram_resize(Gtk::Allocation& alloc)
{
	on_enable_min_value_changed();

	if(!is_expand)
	{
		widget_width = alloc.get_width();
		widget_height = alloc.get_height();
		widget_x = alloc.get_x();
		widget_y = alloc.get_y();
	}

}
// -------------------------------------------------------------------------
void UDiagram::diagram_expand()
{
	Gtk::Fixed *parent=0;
	parent = dynamic_cast<Gtk::Fixed*>(get_parent());

	if(!parent)
		return;

	if(!is_expand)
	{
		expandBGImage->set(get_pixbuf_from_cache(prop_expandBGImagePath.get_value(), parent->get_allocation().get_width(), parent->get_allocation().get_height()));
		expandBGImage->show();
		BGImage->hide();
		parent->Gtk::Container::remove(*this);
		parent->add(*this);
		is_expand = true;
		set_size_request(parent->get_allocation().get_width(),parent->get_allocation().get_height());
	}
	else
	{
		BGImage->set(get_pixbuf_from_cache(prop_BGImagePath.get_value(), widget_width, widget_height));
		BGImage->show();
		expandBGImage->hide();
		parent->move(*this,widget_x - parent->get_allocation().get_x(),widget_y - parent->get_allocation().get_y());
		is_expand = false;
		set_size_request(widget_width,widget_height);
	}

//	queue_draw();
}
// -------------------------------------------------------------------------
void UDiagram::on_bg_border_width_changed()
{
	table->set_border_width(prop_bgBorderWidth.get_value());
}
// -------------------------------------------------------------------------
void UDiagram::on_bg_image_changed()
{
	BGImage->set(get_pixbuf_from_cache(prop_BGImagePath.get_value(), get_allocation().get_width(), get_allocation().get_height()));
	if(is_expand)
		BGImage->hide();
}
// -------------------------------------------------------------------------
void UDiagram::on_expand_bg_image_changed()
{
}
// -------------------------------------------------------------------------
void UDiagram::on_add(Gtk::Widget* w)
{
	if (UVoid* uv = dynamic_cast<UVoid*>(w)) {
		uv->set_property_disconnect_effect(0);
		uv->set_property_auto_connect(true);
	}

	if (UniOscilChannel* ch = dynamic_cast<UniOscilChannel*>(w))
	{
		Gtk::Fixed::on_add(w);
		w->set_child_visible(false);
		channels.push_back(ch);
		ch_num = channels.size();
		topScale->set_range(0,ch_num);
		bottomScale->set_range(0,ch_num);
		plot_area->set_h_range(0,ch_num);
	}
	else
		Gtk::Fixed::on_remove(w);
}
// -------------------------------------------------------------------------
void UDiagram::on_remove(Gtk::Widget* w)
{
	Gtk::Fixed::on_remove(w);
	for(ChannelsList::iterator it = channels.begin();it != channels.end();it++)
	{
		if((*it)==dynamic_cast<UniOscilChannel*>(w))
		{
			channels.erase(it);
			ch_num = channels.size();
			topScale->set_range(0,ch_num);
			bottomScale->set_range(0,ch_num);
			plot_area->set_h_range(0,ch_num);
			break;
		}
	}
}
// -------------------------------------------------------------------------
#define check_child( W ) \
	if ( childinfomap_.count(W) == 0 ) { \
		std::cerr << "(UDiagram):Error:" \
			  << W->get_name() << \
			     " is not in container" <<std::endl; \
		return; \
}
// -------------------------------------------------------------------------
void UDiagram::set_child_connector(Gtk::Widget& child)
{
	UVoid* voidChild = dynamic_cast<UVoid*>(&child);
	if(voidChild && get_connector())
	{
		voidChild->set_connector(get_connector());
	}

//	if (get_connector()->connected())
//		child_on_connect(child);
}
// -------------------------------------------------------------------------
void UDiagram::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UVoid::set_connector(connector);
	averageSensor.set_connector(connector);

	sigc::slot<void, Gtk::Widget&> set_child_connector = sigc::mem_fun(this, &UDiagram::set_child_connector);

	foreach(set_child_connector);
}
// -------------------------------------------------------------------------
void UDiagram::on_connect() throw()
{
	UVoid::on_connect();

	queue_draw();
}
// -------------------------------------------------------------------------
void UDiagram::on_disconnect() throw()
{
	UVoid::on_disconnect();
	queue_draw();
}
// -------------------------------------------------------------------------
void UDiagram::process_sensor(ObjectId id, ObjectId node, long value)
{
}
// -------------------------------------------------------------------------
void UDiagram::on_scales_enable_changed()
{
	topScale->set_enabled(prop_topScale);
	bottomScale->set_enabled(prop_bottomScale);
	leftScale->set_enabled(prop_leftScale);
	rightScale->set_enabled(prop_rightScale);
}
// -------------------------------------------------------------------------
void UDiagram::on_web_enable_changed()
{
	plot_area->set_enabled(prop_enableWeb);
}
// -------------------------------------------------------------------------
void UDiagram::on_web_major_color_changed()
{
	plot_area->set_web_major_color(get_prop_webMajorColor());
}
// -------------------------------------------------------------------------
void UDiagram::on_web_minor_enable_changed()
{
	plot_area->set_enabled_minor(prop_enableWebMinor);
}
// -------------------------------------------------------------------------
void UDiagram::on_web_minor_color_changed()
{
	plot_area->set_web_minor_color(get_prop_webMinorColor());
}
// -------------------------------------------------------------------------
void UDiagram::on_web_line_type_changed()
{
	plot_area->set_web_line_type(get_prop_WebLineType());
}
// -------------------------------------------------------------------------
void UDiagram::on_scales_range_changed()
{
	leftScale->set_range(prop_Min.get_value(),prop_Max.get_value());
	rightScale->set_range(prop_Min.get_value(),prop_Max.get_value());
	plot_area->set_v_range(prop_Min.get_value(),prop_Max.get_value());
	range = abs(prop_Max.get_value() - prop_Min.get_value());
	queue_draw();
}
// -------------------------------------------------------------------------
void UDiagram::on_h_scale_changed()
{
	topScale->set_max_major(prop_XmaxMaj.get_value());
	topScale->set_max_minor(prop_XmaxMin.get_value());
	topScale->set_range(0,ch_num);

	bottomScale->set_max_minor(prop_XmaxMin.get_value());
	bottomScale->set_max_major(prop_XmaxMaj.get_value());
	bottomScale->set_range(0,ch_num);

	plot_area->set_max_h_minor(prop_XmaxMin.get_value());
	plot_area->set_max_h_major(prop_XmaxMaj.get_value());
	plot_area->set_h_range(0,ch_num);
}
// -------------------------------------------------------------------------
void UDiagram::on_v_scale_changed()
{
	leftScale->set_max_major(prop_YmaxMaj.get_value());
	leftScale->set_max_minor(prop_YmaxMin.get_value());
	leftScale->set_range(prop_Min.get_value(),prop_Max.get_value());

	rightScale->set_max_major(prop_YmaxMaj.get_value());
	rightScale->set_max_minor(prop_YmaxMin.get_value());
	rightScale->set_range(prop_Min.get_value(),prop_Max.get_value());

	plot_area->set_max_v_minor(prop_YmaxMin.get_value());
	plot_area->set_max_v_major(prop_YmaxMaj.get_value());
	plot_area->set_v_range(prop_Min.get_value(),prop_Max.get_value());
}
// -------------------------------------------------------------------------
/*void UDiagram::on_scales_autoscale_changed()
{
	m_plot.scale(AXIS_LEFT)->set_autoscale(prop_dynamicAxe);
	m_plot.scale(AXIS_RIGHT)->set_autoscale(prop_dynamicAxe);
	m_plot.scale(AXIS_TOP)->set_autoscale(prop_dynamicAxe);
	m_plot.scale(AXIS_BOTTOM)->set_autoscale(prop_dynamicAxe);
}*/
// -------------------------------------------------------------------------
void UDiagram::on_font_changed()
{
	Pango::FontDescription fd(prop_scaleLabelFont);
	fd.set_absolute_size(prop_scaleLabelFontSize.get_value() * Pango::SCALE);
	topScale->get_labels()->set_labels_font(fd);
	bottomScale->get_labels()->set_labels_font(fd);
	leftScale->get_labels()->set_labels_font(fd);
	rightScale->get_labels()->set_labels_font(fd);
}
// -------------------------------------------------------------------------
void UDiagram::on_scale_color_changed()
{
	topScale->set_scale_color(prop_scaleColor);
	bottomScale->set_scale_color(prop_scaleColor);
	leftScale->set_scale_color(prop_scaleColor);
	rightScale->set_scale_color(prop_scaleColor);
}
// -------------------------------------------------------------------------
void UDiagram::on_font_color_changed()
{
	topScale->get_labels()->set_labels_color(prop_scaleLabelColor);
	bottomScale->get_labels()->set_labels_color(prop_scaleLabelColor);
	leftScale->get_labels()->set_labels_color(prop_scaleLabelColor);
	rightScale->get_labels()->set_labels_color(prop_scaleLabelColor);
}
// -------------------------------------------------------------------------
void UDiagram::on_average_changed()
{
	average = prop_average_value.get_value();
	if(prop_staticAverage)
		last_average = average;
}
// -------------------------------------------------------------------------
void UDiagram::on_plot_bg_color_changed()
{
//	plot_area->modify_bg(Gtk::STATE_NORMAL,prop_bgColor);
}
// -------------------------------------------------------------------------
void UDiagram::on_tooltip_image_changed()
{
	tooltip_image = get_pixbuf_from_cache(prop_tooltipImagePath.get_value(), prop_tooltipImageWidth.get_value(), prop_tooltipImageHeight.get_value());
}
// -------------------------------------------------------------------------
void UDiagram::on_tooltip_font_changed()
{
	Pango::FontDescription font(prop_tooltipLabelFont);
	font.set_absolute_size(prop_tooltipLabelFontSize.get_value() * Pango::SCALE);
	tooltip_layout->set_font_description(font);
}
// -------------------------------------------------------------------------
bool UDiagram::on_plot_area_event(GdkEvent* ev)
{
//	if(ev->type!=GDK_EXPOSE)
//		cout<<get_name()<<"::on_plot_area_event event->type="<<ev->type<< endl;
	GdkEventButton event_btn = ev->button;

	if(ev->type==GDK_MAP)
		is_show = true;
	else if(ev->type==GDK_UNMAP)
		is_show = false;

	if(event_btn.type == GDK_2BUTTON_PRESS)
	{
		double x_step = (double)width/ch_num;
		int x,y;
		Gdk::ModifierType mtype;
		plot_area->get_window()->get_pointer(x,y,mtype);
		save_channel_num = (x - plot_area->get_allocation().get_x())/x_step;
		if(save_channel_num>ch_num-1)
			save_channel_num = ch_num-1;
		else if(save_channel_num<0)
			save_channel_num = 0;
		string name = channels[save_channel_num]->get_channelname();
		mi_save_channel->set_label(Glib::ustring("Сохранить канал '")+name+Glib::ustring("' в файл CSV"));

		mi_set_expand->set_image(*manage( new Gtk::Image(is_expand ? Gtk::Stock::LEAVE_FULLSCREEN : Gtk::Stock::FULLSCREEN,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
		mi_set_expand->set_label(is_expand ? Glib::ustring("Свернуть"):Glib::ustring("Развернуть"));

		mi_set_web->set_label(prop_enableWeb ? Glib::ustring("Скрыть  сетку"):Glib::ustring("Показать сетку"));

		menu_popup->popup(event_btn.button, event_btn.time);
	}
	else if( (ev->type == GDK_MOTION_NOTIFY || ev->type == GDK_BUTTON_PRESS ) && prop_enableTooltip )
	{
		show_tooltip = true;
		queue_draw();
	}
	else if( (ev->type == GDK_LEAVE_NOTIFY || ev->type == GDK_BUTTON_RELEASE) && prop_enableTooltip )
	{
		show_tooltip = false;
		queue_draw();
	}
	return false;
}
// -------------------------------------------------------------------------
void UDiagram::on_min_value_font_changed()
{
	Pango::FontDescription font(prop_minValueTextFont);
	font.set_absolute_size(prop_minValueTextFontSize.get_value() * Pango::SCALE);
	minValue_layout->set_font_description(font);
	on_enable_min_value_changed();
}
// -------------------------------------------------------------------------
void UDiagram::on_min_value_text_line_num_changed()
{
	int layw,layh;
	minValue_layout->get_pixel_size(layw,layh);
	table->set_size_request(get_allocation().get_width(),get_allocation().get_height()-layh*prop_minValueTextLineNum.get_value());
	width = plot_area->get_allocation().get_width();
	height = plot_area->get_allocation().get_height();
}
// -------------------------------------------------------------------------
void UDiagram::on_enable_min_value_changed()
{
	if(prop_enableMinValue)
		on_min_value_text_line_num_changed();
	else
	{
		table->set_size_request(get_allocation().get_width(),get_allocation().get_height());
		width = plot_area->get_allocation().get_width();
		height = plot_area->get_allocation().get_height();
	}
}
// -------------------------------------------------------------------------
void UDiagram::on_history_dir_changed()
{
	if(prop_enableHisory && connected_)
	{
		if(createDir(prop_dir.get_value()))
		{
			if(history_file_name.empty())
				history_file_name = dateToString(time(0),"")+"_"+timeToString(time(0),"")+"_"+get_name()+".dat";
			history_file_fullname	= prop_dir.get_value()+"/"+history_file_name;
		}
		else
			cout<< get_name() << "::on_history_dir_changed() can't create Dir="<<prop_dir.get_value()<< endl;
	}
}
// -------------------------------------------------------------------------
void UDiagram::on_history_enable_changed()
{
	mi_save_channel->set_sensitive(prop_enableHisory && connected_);
	mi_save_all_channels->set_sensitive(prop_enableHisory && connected_);
	mi_save_all_in_one->set_sensitive(prop_enableHisory && connected_);
	on_history_dir_changed();
}
// -------------------------------------------------------------------------
void UDiagram::save_channel()
{
	string filename;
	string channel_name = channels[save_channel_num]->get_channelname();
	if(!prop_dir.get_value().empty())
		filename = prop_dir.get_value() + "/" + dateToString(time(0),"") + "_" + timeToString(time(0),"") + "_" + channel_name + ".csv";
	else
		filename = dateToString(time(0),"") + "_" + timeToString(time(0),"") + "_" + channel_name + ".csv";

	save_channel_in_file(save_channel_num, filename);
}
// -------------------------------------------------------------------------
void UDiagram::save_all_channels()
{
	time_t tm = time(0);
	for(int num = 0;num<ch_num;num++)
	{
		string filename;
		string channel_name = channels[num]->get_channelname();
		if(!prop_dir.get_value().empty())
			filename = prop_dir.get_value() + "/" + dateToString(tm,"") + "_" + timeToString(tm,"") + "_" + channel_name + ".csv";
		else
			filename = dateToString(tm,"") + "_" + timeToString(tm,"") + "_" + channel_name + ".csv";

		save_channel_in_file(num, filename);
	}
}
// -------------------------------------------------------------------------
void UDiagram::save_channel_in_file(int channel_num, string filename)
{
	if(channels.empty())
		return;
	if(channel_num<0)
		return;
	if(channel_num>ch_num-1)
		return;

//	cout<<"file: "<<filename<<endl;
	filename = Glib::convert(filename,"CP1251","UTF8");

	cout<<"try write to file: "<<filename<<endl;
	fstream fout( filename.c_str(), ios::out | ios::trunc );
	if(!fout)
	{
		cout<< get_name() << " can't open file="<<history_file_fullname<<" for write!"<< endl;
		return;
	}

	double point = 0;
	int prec = channels[channel_num]->get_prop_precision();
	stringstream s;
	string channel_name = channels[channel_num]->get_channelname();
	s<<"Время"<<"\tОтсчет [с]\t"<<channel_name;

	fstream fin( history_file_fullname.c_str(), ios::in);
	if(!fin)
	{
		cout<< get_name() << " can't open file="<<history_file_fullname<<" for read!"<< endl;
	}
	else
	{
		fin.close();
		ChannelHistoryData tmpHistory;
		FILE* infile = fopen(history_file_fullname.c_str(),"r");
		if(infile)
		{
			int num=0;
			while(fread(&tmpHistory,sizeof(tmpHistory),1,infile) > 0)
			{
				if(num!=channel_num)
				{
					num++;
					if( num>ch_num-1 )
						num=0;
					continue;
				}
				num++;
				if( num>ch_num-1 )
					num=0;
				string time_str = timeToString(tmpHistory.tm,":");
				point = point + (double)prop_refresh_timeout.get_value()/1000;
				float ch_value = tmpHistory.value;

				s<<"\n"<<time_str;
				s<<"\t"<<(long)point<<","<<setw(5)<<setfill('0')<<(long)((point-(long)point)*100000);
				s<<"\t"<< (ch_value>=0? "":"-") <<abs((long)ch_value)<<","<<abs((long)((ch_value-(long)ch_value)*pow10(prec)));
			};
			fclose(infile);
		}
	}

	for(HistoryMap::iterator it = history.begin(); it!=history.end(); it++ )
	{
		string time_str = timeToString((it->second.begin())->second.tm,":");
		point = point + (double)prop_refresh_timeout.get_value()/1000;
		float ch_value = it->second[channel_num].value;

		s<<"\n"<<time_str;
		s<<"\t"<<(long)point<<","<<setw(5)<<setfill('0')<<(long)((point-(long)point)*100000);
		s<<"\t"<< (ch_value>=0? "":"-") <<abs((long)ch_value)<<","<<abs(lround((ch_value-(long)ch_value)*pow10(prec)));
	}

	fout<<Glib::convert(s.str(),"UNICODE","UTF8");
//	fout<<s.str();
	fout.close();
	cout<< get_name() << "::save_channel '"<<channel_name<<"' save OK!"<< endl;
}
//--------------------------------------------------------------------------
void UDiagram::save_all_channels_in_one_file()
{
	if(channels.empty())
		return;

	string filename;
	if(!prop_dir.get_value().empty())
		filename = prop_dir.get_value() + "/" + dateToString(time(0),"") + "_" + timeToString(time(0),"") + "_" + get_name() + ".csv";
	else
		filename = dateToString(time(0),"") + "_" + timeToString(time(0),"") + "_" + get_name() + ".csv";

//	cout<<"file: "<<filename<<endl;
	filename = Glib::convert(filename,"CP1251","UTF8");

	cout<<"try write to file: "<<filename<<endl;
	fstream fout( filename.c_str(), ios::out | ios::trunc );
	if(!fout)
	{
		cout<< get_name() << " can't open file="<<history_file_fullname<<" for write!"<< endl;
		return;
	}

	double point = 0;
	stringstream s;
	s<<"Время"<<"\tОтсчет [с]";
	for( ChannelsList::iterator it=channels.begin(); it!=channels.end(); it++ )
	{
		string name = (*it)->get_channelname();
		s<<"\t"<<name;
	}
	s<<"\n";

	fstream fin( history_file_fullname.c_str(), ios::in);
	if(!fin)
	{
		cout<< get_name() << " can't open file="<<history_file_fullname<<" for read!"<< endl;
	}
	else
	{
		fin.close();
		ChannelHistoryData tmpHistory;
		FILE* infile = fopen(history_file_fullname.c_str(),"r");
		if(infile)
		{
			string time_str;
			int num=0;
			while(fread(&tmpHistory,sizeof(tmpHistory),1,infile) > 0)
			{
				if( num==0 )
				{
					time_str = timeToString(tmpHistory.tm,":");
					s<<time_str<<"\t"<<(long)point<<","<<setw(5)<<setfill('0')<<(long)((point-(long)point)*100000);
					point = point + (double)prop_refresh_timeout.get_value()/1000;
				}

				float ch_value = tmpHistory.value;
				int prec = channels[num]->get_prop_precision();
				s<<"\t"<< (ch_value>=0? "":"-") <<abs((long)ch_value)<<","<<abs(lround((ch_value-(long)ch_value)*pow10(prec)));

				num++;
				if(num>=ch_num) // проверка что пора переходить на новую строчку
				{
					num=0;
					s<<"\n";
				}
			};
			fclose(infile);
		}
	}

	for(HistoryMap::iterator it = history.begin(); it!=history.end(); it++ )
	{
		string time_str = timeToString((it->second.begin())->second.tm,":");
		s<<time_str<<"\t"<<(long)point<<","<<setw(5)<<setfill('0')<<(long)((point-(long)point)*100000);
		point = point + (double)prop_refresh_timeout.get_value()/1000;
		for(ChannelsDataMap::iterator chIt = it->second.begin(); chIt!=it->second.end(); chIt++ )
		{
			float ch_value = chIt->second.value;
			int prec = channels[chIt->first]->get_prop_precision();
			s<<"\t"<< (ch_value>=0? "":"-") <<abs((long)ch_value)<<","<<abs(lround((ch_value-(long)ch_value)*pow10(prec)));
		}
		s<<"\n";
	}

	fout<<Glib::convert(s.str(),"UNICODE","UTF8");
//	fout<<s.str();
	fout.close();
	cout<< get_name() << "::save_all_channels_in_one_file() save OK!"<< endl;
}
//--------------------------------------------------------------------------
string UDiagram::dateToString(time_t tm, string brk)
{
	struct tm *tms = localtime(&tm);
	ostringstream date;
	date << std::setw(4) << std::setfill('0') << tms->tm_year+1900 << brk;
	date << std::setw(2) << std::setfill('0') << tms->tm_mon+1 << brk;
	date << std::setw(2) << std::setfill('0') << tms->tm_mday;
	return date.str();
}
//--------------------------------------------------------------------------
string UDiagram::timeToString(time_t tm, string brk)
{
	struct tm *tms = localtime(&tm);
	ostringstream time;
	time << std::setw(2) << std::setfill('0') << tms->tm_hour << brk;
	time << std::setw(2) << std::setfill('0') << tms->tm_min << brk;
	time << std::setw(2) << std::setfill('0') << tms->tm_sec;
	return time.str();
}
//--------------------------------------------------------------------------
void UDiagram::timeToInt(long time,int &hour, int &min, int &sec)
{
/*	hour = tm_hour + time / 3600;
	while(hour>=24)
		hour = hour - 24;
	min = tm_min + (time % 3600) / 60;
	if(min>=60)
	{
		min = min - 60;
		hour++;
		if(hour>=24)
			hour = hour - 24;
	}
	sec = tm_sec + (time % 3600) % 60;
	if(sec>=60)
	{
		sec = sec - 60;
		min++;
		if(min>=60)
		{
			min = min - 60;
			hour++;
			if(hour>=24)
				hour = hour - 24;
		}
	}*/
}
//--------------------------------------------------------------------------
bool UDiagram::createDir( const std::string dir )
{
	if( mkdir(dir.c_str(), (S_IRWXO | S_IRWXU | S_IRWXG) ) == -1 )
	{
		if( errno != EEXIST )
		{
			cout<< " mkdir " << dir << " FAILED! " << strerror(errno) << endl;
			return false;
		}
	}

	return true;
}
//--------------------------------------------------------------------------
