#ifndef _UDIAGRAM_H
#define _UDIAGRAM_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <gtkmm.h>
#include "UniOscilChannel.h"
#include "UDefaultFunctions.h"
#include "USignals.h"
#include "UScale.h"
#include "plugins.h"
#include "global_macros.h"
// -------------------------------------------------------------------------
/*!\brief Класс контейнера.
 * \par
 * Класс предназначен для хранения виджетов UniOscilChannel отображающих состояние датчиков.
 * Класс основан на Gtk::Fixed
*/
class UDiagram : public UDefaultFunctions<Gtk::Fixed>
{
	public:
		UDiagram();
		explicit UDiagram(GtkmmBaseType::BaseObjectType* gobject);
		virtual ~UDiagram();

		virtual void process_sensor(UniWidgetsTypes::ObjectId,UniWidgetsTypes::ObjectId,long);
		void startup_init(void);

		static std::string timeToString(time_t tm=time(0), std::string brk=""); /*!< Preobrazovanie vremeni v stroku HH:MM:SS */
		static std::string dateToString(time_t tm=time(0), std::string brk=""); /*!< Preobrazovanie dati v stroku YYYY/MM/DD */
		static bool createDir( const std::string dir );
		void timeToInt(long time,int &hour, int &min, int &sec); /*!< Preobrazovanie vremeni v chisla*/

	protected:
		enum ScalePosition
		{
			LEFT = 0,
			RIGHT,
			TOP,
			BOTTOM
		};
		virtual bool on_plot_area_event(GdkEvent *event);
		virtual void on_add(Gtk::Widget* w);
		virtual void on_remove(Gtk::Widget* w);
		virtual void diagram_resize(Gtk::Allocation& alloc);
		virtual void on_realize();
		virtual bool on_expose_event(GdkEventExpose*);
		virtual void set_connector(const ConnectorRef& connector) throw();
		virtual void on_connect() throw();
		virtual void on_disconnect() throw();
		virtual void draw_min_value();

		bool on_timer_tick();

		Gtk::VBox *topScaleBox;
		Gtk::VBox *bottomScaleBox;
		Gtk::HBox *leftScaleBox;
		Gtk::HBox *rightScaleBox;
		HScale *topScale;
		HScale *bottomScale;
		VScale *leftScale;
		VScale *rightScale;
		Web *plot_area;
		Gtk::Table *table;
		Gtk::Image *BGImage;
		Gtk::Image *expandBGImage;

		Glib::RefPtr<Pango::Layout> tooltip_layout;
		Glib::RefPtr<Gdk::Pixbuf> tooltip_image;

		Glib::RefPtr<Pango::Layout> minValue_layout;

	private:
		void ctor();
		sigc::connection tmr;

		void connect_child(Gtk::Widget* child);
		void disconnect_child(Gtk::Widget& child);

		void set_child_connector(Gtk::Widget&);

		void on_web_enable_changed();
		void on_web_minor_enable_changed();
		void on_web_major_color_changed();
		void on_web_minor_color_changed();
		void on_web_line_type_changed();
		void on_scales_enable_changed();
		void on_scales_range_changed();
		void on_scales_autoscale_changed();
		void on_font_changed();
		void on_font_color_changed();
		void on_scale_color_changed();
		void on_v_scale_changed();
		void on_h_scale_changed();
		void on_average_changed();
		void on_plot_bg_color_changed();
		void on_bg_image_changed();
		void on_expand_bg_image_changed();
		void on_bg_border_width_changed();
		void on_refresh_timeout_changed();
		void on_tooltip_image_changed();
		void on_tooltip_font_changed();
		void on_enable_min_value_changed();
		void on_min_value_font_changed();
		void on_min_value_text_line_num_changed();
		void on_history_enable_changed();
		void on_history_dir_changed();

		void save_channel();
		void save_all_channels();
		void save_channel_in_file(int channel_num, std::string filename);
		void save_all_channels_in_one_file();
		void diagram_expand();
		inline void set_diagram_web(){ prop_enableWeb.set_value(!prop_enableWeb);};

		typedef std::vector<UniOscilChannel*> ChannelsList;
		ChannelsList channels;

		struct ChannelData{
			ChannelData():
			value(0),
//			name(""),
			color("black")
			{
			};
			float value;
			Gdk::Color color;
//			string name;
		};
		typedef std::unordered_map<int,ChannelData> MinValueChannelMap;
		MinValueChannelMap min_value_channels;

		typedef std::map<int,Gdk::Color> MinValueLine;
		typedef std::map<float,MinValueLine> MinValueMap;
		MinValueMap min_value;

		struct ChannelHistoryData{
			ChannelHistoryData():
			value(0),
			tm()
			{
			};
			float value;
			time_t tm;
		}__attribute__((packed));
		typedef std::map<int,ChannelHistoryData> ChannelsDataMap;
		typedef std::map<double,ChannelsDataMap> HistoryMap;
		HistoryMap history;

		int width;
		int height;
		double point_x;
		int ch_num;
		double range;
		double average;
		double last_average;
		bool show_tooltip;
		bool is_show;
		bool is_expand;
		int widget_width;
		int widget_height;
		int widget_x;
		int widget_y;

		std::string history_file_name;
		std::string history_file_fullname;
		Gtk::Menu* menu_popup;				/*!< контекстное меню */
		Gtk::ImageMenuItem* mi_set_expand;		/*!< вкл\откл расширение на весь родительский виджет */
		Gtk::ImageMenuItem* mi_set_web;			/*!< вкл\откл отображения сетки */
		Gtk::ImageMenuItem* mi_save_all_in_one;		/*!< сохранение всех каналов в один файл */
		Gtk::ImageMenuItem* mi_save_channel;		/*!< сохранение канала в файл */
		Gtk::ImageMenuItem* mi_save_all_channels;	/*!< сохранение всех каналов в отдельный файл */
		int save_channel_num;

		SensorProp averageSensor;
		ADD_PROPERTY( prop_staticAverage, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_average_value, float )		/*!< свойство: */
		ADD_PROPERTY( prop_precision, int )			/*!< свойство: */
		ADD_PROPERTY( prop_refresh_timeout, int )		/*!< свойство: */
		ADD_PROPERTY( prop_dynamicLevels, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_highWarnLevel, float )		/*!< свойство: */
		ADD_PROPERTY( prop_highCritLevel, float )		/*!< свойство: */
		ADD_PROPERTY( prop_disableHighLevels, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_lowWarnLevel, float )		/*!< свойство: */
		ADD_PROPERTY( prop_lowCritLevel, float )		/*!< свойство: */
		ADD_PROPERTY( prop_disableLowLevels, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_infoColor, Gdk::Color )		/*!< свойство: */
		ADD_PROPERTY( prop_warnColor, Gdk::Color )		/*!< свойство: */
		ADD_PROPERTY( prop_critColor, Gdk::Color )		/*!< свойство: */
		ADD_PROPERTY( prop_useGradientColor, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_insensColor, Gdk::Color )		/*!< свойство: */
		ADD_PROPERTY( prop_enableWeb, bool )					/*!< свойство: */
		ADD_PROPERTY( prop_webMajorColor, Gdk::Color )			/*!< свойство: */
		ADD_PROPERTY( prop_enableWebMinor, bool )				/*!< свойство: */
		ADD_PROPERTY( prop_webMinorColor, Gdk::Color )			/*!< свойство: */
		ADD_PROPERTY( prop_WebLineType, Web::WebLineType )		/*!< свойство: */
		ADD_PROPERTY( prop_scaleColor, Gdk::Color )		/*!< свойство: */
		ADD_PROPERTY( prop_scaleLabelColor, Gdk::Color )	/*!< свойство: */
		ADD_PROPERTY( prop_scaleLabelFont, Glib::ustring )	/*!< свойство: */
		ADD_PROPERTY( prop_scaleLabelFontSize, int )		/*!< свойство: */
		ADD_PROPERTY( prop_topScale, bool )			/*!< свойство: */
		ADD_PROPERTY( prop_bottomScale, bool )			/*!< свойство: */
		ADD_PROPERTY( prop_leftScale, bool )			/*!< свойство: */
		ADD_PROPERTY( prop_rightScale, bool )			/*!< свойство: */
		ADD_PROPERTY( prop_barWidth, float )			/*!< свойство: */
		ADD_PROPERTY( prop_Min, float )				/*!< свойство: */
		ADD_PROPERTY( prop_Max, float )				/*!< свойство: */
		ADD_PROPERTY( prop_XmaxMin, int )			/*!< свойство: */
		ADD_PROPERTY( prop_XmaxMaj, int )			/*!< свойство: */
		ADD_PROPERTY( prop_YmaxMin, int )			/*!< свойство: */
		ADD_PROPERTY( prop_YmaxMaj, int )			/*!< свойство: */
		ADD_PROPERTY( prop_BGImagePath, Glib::ustring)		/*!< свойство: */
		ADD_PROPERTY( prop_expandBGImagePath, Glib::ustring)	/*!< свойство: */
		ADD_PROPERTY( prop_bgBorderWidth, int)			/*!< свойство: */
		ADD_PROPERTY( prop_enableMinValue, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_minValueNum, int )			/*!< свойство: */
		ADD_PROPERTY( prop_minValueTextLineNum, int )		/*!< свойство: */
		ADD_PROPERTY( prop_enableMinValueThreshold, bool )	/*!< свойство: */
		ADD_PROPERTY( prop_minValueThreshold, float )		/*!< свойство: */
		ADD_PROPERTY( prop_minValueTextFixedPosition, bool )	/*!< свойство: */
		ADD_PROPERTY( prop_minValueTextColor, Gdk::Color )	/*!< свойство: */
		ADD_PROPERTY( prop_minValueDynamicColor, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_minValueTextFont, Glib::ustring )	/*!< свойство: */
		ADD_PROPERTY( prop_minValueTextFontSize, int )		/*!< свойство: */
		ADD_PROPERTY( prop_minValueDashStep, double )		/*!< свойство: */
		ADD_PROPERTY( prop_minValueDashOffset, double )		/*!< свойство: */
		ADD_PROPERTY( prop_enableTooltip, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_tooltipImagePath, Glib::ustring)	/*!< свойство: */
		ADD_PROPERTY( prop_tooltipImageWidth, int )		/*!< свойство: */
		ADD_PROPERTY( prop_tooltipImageHeight, int )		/*!< свойство: */
		ADD_PROPERTY( prop_tooltipLabelColor, Gdk::Color )	/*!< свойство: */
		ADD_PROPERTY( prop_tooltipDynamicColor, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_tooltipLabelFont, Glib::ustring )	/*!< свойство: */
		ADD_PROPERTY( prop_tooltipLabelFontSize, int )		/*!< свойство: */
		ADD_PROPERTY( prop_enableHisory, bool )			/*!< свойство: */
		ADD_PROPERTY( prop_dir, Glib::ustring )			/*!< свойство: */
		ADD_PROPERTY( prop_historyPointInMem, long )		/*!< свойство: */

		DISALLOW_COPY_AND_ASSIGN(UDiagram);
};
// -------------------------------------------------------------------------
template<>
		inline GType UObj_Get_Type<UDiagram>()
{
	static GType gtype = 0;
	if (gtype)
		return gtype;

	UDiagram* dummy = new UDiagram();
	GtkContainerClass* container_klass = GTK_CONTAINER_GET_CLASS( dummy->gobj() );

	gtype = G_OBJECT_TYPE(dummy->gobj());
	delete( dummy );
	Glib::wrap_register(gtype, &UObj_Wrap_New<UDiagram>);

	return gtype;
}
#endif
