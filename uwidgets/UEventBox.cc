#include <iostream>
#include "types.h"
#include "UEventBox.h"
// -------------------------------------------------------------------------
//Blinker UEventBox::blinker;
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
UEventBox::UEventBox() :
	Glib::ObjectBase("ueventbox")
{
	full_redraw = true;
	INIT_VARIABLE(width);
	INIT_VARIABLE(height);
}
// -------------------------------------------------------------------------
UEventBox::UEventBox(GtkmmBaseType::BaseObjectType* gobject) :
	BaseType(gobject)
{
	full_redraw = true;
	INIT_VARIABLE(width);
	INIT_VARIABLE(height);
}
// -------------------------------------------------------------------------
UEventBox::~UEventBox()
{
}
// -------------------------------------------------------------------------
ConnectorRef
UEventBox::get_connector_from_hierarchy()
{
	Gtk::Widget* parent = get_parent();
	while (parent != NULL) {
		if(UVoid* uv = dynamic_cast<UVoid*>(parent)) {
			ConnectorRef new_connector = uv->get_connector();
			return new_connector;

		}
		parent = parent->get_parent();
	}
	return ConnectorRef();
}
// -------------------------------------------------------------------------
void
UEventBox::set_connector_to_hierarchy(Gtk::Container* w, ConnectorRef connector)
{
	typedef Glib::ListHandle<Gtk::Widget*> ChList;
	ChList childlist = w->get_children();
	for(ChList::iterator it = childlist.begin(); it != childlist.end(); ++it) {
		UVoid* uv = dynamic_cast<UVoid*>(*it);
		Gtk::Container* cont = dynamic_cast<Gtk::Container*>(*it);
		if (uv)
			uv->set_connector(connector);
		else if (cont)
			set_connector_to_hierarchy(cont,connector);
	}
}
// -------------------------------------------------------------------------
bool
UEventBox::on_expose_event(GdkEventExpose* event)
{
	bool rv = Gtk::EventBox::on_expose_event(event);
	Gtk::Allocation alloc = get_allocation();
	guint tw = alloc.get_width(), th = alloc.get_height();

	if (width != tw) {
		full_redraw = true;
		width = tw;
	}
	if (height != th) {
		full_redraw = true;
		height = th;
	}
	return rv;
}
// -------------------------------------------------------------------------
// void
// UEventBox::add_lock(const Gtk::Widget& w)
// {
// 	Gtk::Container *c  =this;
// 	Gtk::Container *prev = this;
// 	while ( (c = c->get_parent()) != NULL )
// 	{
// 		if (UVoid* uv = dynamic_cast<UVoid*>(c))
// 		{
// 			uv->add_lock(*prev);
// 			break;
// 		}
// 		prev = c;
// 	}
// }
// -------------------------------------------------------------------------
// void
// UEventBox::unlock_current()
// {
// 	Gtk::Container *c = this;
// 	while ( (c = c->get_parent()) != NULL )
// 		if (UVoid* uv = dynamic_cast<UVoid*>(c))
// 		{
// 			uv->unlock_current();
// 			break;
// 		}
// }
// -------------------------------------------------------------------------
