#ifndef _UEVENTBOX_H
#define _UEVENTBOX_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <sstream>
#include "UDefaultFunctions.h"
#include "SBlinker.h"
#include "SensorProp.h"
// -------------------------------------------------------------------------
#define GTKMM_PARENT Gtk::EventBox
#define GTK_PARENT GtkEventBox
// -------------------------------------------------------------------------
/*!\brief Класс области событий.
 * \par
 * Класс предназначен для реализации виджета представляющего из себя область событий,
 * которая обычно не появляется, но позволяет получать различные события, которые
 * можно задавать пользователю(set_event). Помимо стандарного внешнего вида унаследованного от
 * Gtk::EventBox, наследуются методы объявленные в UVoid т.е. работа с событиями соединения
 * , отсоединения от SharedMemory и обработка отрисовки неактивного состояния.
*/
class UEventBox: public UDefaultFunctions<Gtk::EventBox> {

public:
	UEventBox();
	explicit UEventBox(GtkmmBaseType::BaseObjectType* gobject);
	virtual ~UEventBox();
	/*! полная перерисовка виджета */
	void queue_full_draw() { full_redraw=true; queue_draw(); }

protected:
	/*! получить коннектор по иерархии от родительского виджета */
	ConnectorRef get_connector_from_hierarchy();
	/*! установить новый коннектор по иерархии для родительского виджета */
	void set_connector_to_hierarchy(Gtk::Container* w, ConnectorRef connector);
	/*! обработчик события перерисовки области виджета */
	virtual bool on_expose_event(GdkEventExpose* event);
	/*! ширина области виджета */
	ADD_VARIABLE(guint, width);
	/*! высота области виджета */
	ADD_VARIABLE(guint, height);

	/* Ancestor widgets using full_redraw must set it to false themselves after the redraw was done */
	/*! флаг полной перерисовки виджета */
	bool full_redraw;

// 	virtual void add_lock(const Gtk::Widget& w);
// 	virtual void unlock_current();
};
#endif
