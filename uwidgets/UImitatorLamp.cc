#include <types.h>
#include <typical/TypicalImitatorLamp.h>
#include <components/Text.h>
#include "UImitatorLamp.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgets;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define STATE_LAMP_AI			"state-lamp-ai"
#define STATELOGIC_NODE    "node"
#define LAMP_WIDTH			  "lamp-width"
#define LAMP_HEIGHT			  "lamp-height"
#define TEXT_X				    "text-x"
#define TEXT_Y				    "text-y"
#define IMAGELAMP_PATH		"imagelamp-path"
#define IMAGELAMP2_PATH		"imagelamp2-path"
#define BLINKTIME1			  "blinktime1"
#define BLINKTIME2			  "blinktime2"
#define LABEL             "label"
// -------------------------------------------------------------------------
#define INIT_PROPERTIES() \
	state_lamp_ai(*this, STATE_LAMP_AI , UniWidgetsTypes::DefaultObjectId) \
  ,node(*this, STATELOGIC_NODE , UniWidgetsTypes::DefaultObjectId) \
	,lamp_width(*this, LAMP_WIDTH , 68) \
	,lamp_height(*this, LAMP_HEIGHT , 44) \
	,text_x(*this, TEXT_X , 0) \
	,text_y(*this, TEXT_Y , 0) \
	,imagelamp_path(*this, IMAGELAMP_PATH, "" ) \
	,imagelamp2_path(*this, IMAGELAMP2_PATH, "" ) \
	,blinktime1(*this, BLINKTIME1, 500) \
	,blinktime2(*this, BLINKTIME2, 500) \
  ,property_label(*this,LABEL,"")
// -------------------------------------------------------------------------
void UImitatorLamp::ctor()
{
  state_lamp = new TypicalImitatorLamp();
  add_child( state_lamp,typeObject );

  // Важен порядок вызова add_child(), логика должа быть последней
  _label = new Text();
}
// -------------------------------------------------------------------------
UImitatorLamp::UImitatorLamp() :
	Glib::ObjectBase("uimitatorlamp")
	,INIT_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UImitatorLamp::UImitatorLamp(GtkmmBaseType::BaseObjectType* gobject) :
    SimpleObject(gobject)
	,INIT_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UImitatorLamp::~UImitatorLamp()
{
}
// -------------------------------------------------------------------------
void UImitatorLamp::on_realize()
{
	BaseType::on_realize();

	lamp_rect = new Gdk::Rectangle(0, 0, get_lamp_width(), get_lamp_height());

	state_lamp->set_rect(*lamp_rect);
	state_lamp->set_path(lmpOFF, get_imagelamp_path());
	state_lamp->set_path(lmpON, get_imagelamp2_path());
	state_lamp->set_path(lmpBLINK, get_imagelamp2_path());
	state_lamp->set_path(lmpBLINK2, get_imagelamp2_path());
	state_lamp->set_path(lmpBLINK3, get_imagelamp2_path());
	state_lamp->set_blinking_time(lmpBLINK2, get_blinktime1());
	state_lamp->set_blinking_time(lmpBLINK3, get_blinktime2());

	state_lamp->set_state_ai(get_state_lamp_ai());
	state_lamp->set_node(get_node());

	state_lamp->configure();
  put( *state_lamp,
       lamp_rect->get_x(),
                        lamp_rect->get_y() );
  state_lamp->show();
  _label->set_property_text_(get_property_label());
  _label->set_property_abs_font_size_(16);
  _label->set_property_font_color_(Gdk::Color("black"));
  put(*_label,get_text_x(),get_text_y());
  add_child(_label, typeView);
  _label->show();
}
// -------------------------------------------------------------------------
