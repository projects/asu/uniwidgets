#ifndef _UIMITATORLAMP_H
#define _UIMITATORLAMP_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <objects/SimpleObject.h>
#include <global_macros.h>
// ------------------------------------------------------------------------------------------
namespace UniWidgets
{
class TypicalImitatorLamp;
class Text;
/*!\brief Лампочка.
 * \par
 * Отображение лампочки на экране имитатора.
 * Описание параметров:
 * - ADD_PROPERTY( state_lamp_ai, UniWidgetsTypes::ObjectId ): указывается датчик лампочки
 * - ADD_PROPERTY( node, UniWidgetsTypes::ObjectId ): указывается узел
 * параметры расположения на экране и размера картинки лампочки
 * - ADD_PROPERTY( lamp_width, long )
 * - ADD_PROPERTY( lamp_height, long )
 * - ADD_PROPERTY( text_x, long )
 * - ADD_PROPERTY( text_y, long )

 * - ADD_PROPERTY( imagelamp_path, ... )
 * - ADD_PROPERTY( imagelamp2_path, ... ) - путь, где находится картинка для лампочки
*/
class UImitatorLamp : public SimpleObject
{
public:
  UImitatorLamp();
  explicit UImitatorLamp(GtkmmBaseType::BaseObjectType* gobject);
  ~UImitatorLamp();

protected:

  /* Event handlers */
  virtual void on_realize();

private:
	TypicalImitatorLamp *state_lamp;
  Text *_label;
	Gdk::Rectangle *lamp_rect;

	/* Methods */
	void ctor();

	ADD_PROPERTY( state_lamp_ai, UniWidgetsTypes::ObjectId )
  ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )
	ADD_PROPERTY( lamp_width, long )
	ADD_PROPERTY( lamp_height, long )
	ADD_PROPERTY( text_x, long )
	ADD_PROPERTY( text_y, long )
	ADD_PROPERTY( imagelamp_path, Glib::ustring )
	ADD_PROPERTY( imagelamp2_path, Glib::ustring )
	ADD_PROPERTY( blinktime1, int )
	ADD_PROPERTY( blinktime2, int )
  ADD_PROPERTY( property_label, Glib::ustring )
};

}
// -------------------------------------------------------------------------
#endif
