#include <iostream>
#include "UIndicator.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void UIndicator::on_text_prop_changed()
{
	lbl.set_text(get_prop_text());
}
// -------------------------------------------------------------------------
void UIndicator::on_pango_prop_changed()
{
	Pango::FontDescription pd(get_prop_pango());
	lbl.modify_font(pd);
}
// -------------------------------------------------------------------------
void UIndicator::on_state_changed()
{
	draw_state(get_state());
	queue_draw();
}
// -------------------------------------------------------------------------
void UIndicator::on_line_wrap_changed()
{
	lbl.set_line_wrap( get_line_wrap() );
}
// -------------------------------------------------------------------------
void UIndicator::ctor()
{
	add(lbl);
	lbl.show();
	connect_property_changed("text", sigc::mem_fun(*this, &UIndicator::on_text_prop_changed) );
	connect_property_changed("pango", sigc::mem_fun(*this, &UIndicator::on_pango_prop_changed) );
	connect_property_changed("state", sigc::mem_fun(*this, &UIndicator::on_state_changed) );
	connect_property_changed("line_wrap", sigc::mem_fun(*this, &UIndicator::on_line_wrap_changed) );

}
// -------------------------------------------------------------------------
UIndicator::UIndicator() : 
	Glib::ObjectBase("uindicator")
	,lbl("Text")
	,bg_col_0("grey")
	,fg_col_0("black")
	,bg_col_1("green")
	,fg_col_1("black")
	,di(*this,"di", get_connector())
	,prop_text(*this,"text","Default Text")
	,prop_pango(*this,"pango","Arial Bold 12")
	,line_wrap(*this,"line_wrap",true)
	,bgcolor_true(*this,"bg_color_true","green")
	,bgcolor_false(*this,"bg_color_false","grey")
	,fgcolor_true(*this,"fg_color_true","black")
	,fgcolor_false(*this,"fg_color_false","black")
	,state(*this,"state",false)
{
//	std::cerr << "Creating a new UIndicator default." << std::endl;
	ctor();
}
// -------------------------------------------------------------------------
UIndicator::UIndicator(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,lbl("Text")
	,bg_col_0("grey")
	,fg_col_0("black")
	,bg_col_1("green")
	,fg_col_1("black")
	,di(*this,"di", get_connector())
	,prop_text(*this,"text","Default Text")
	,prop_pango(*this,"pango","Arial Bold 12")
	,line_wrap(*this,"line_wrap",true)
	,bgcolor_true(*this,"bg_color_true","green")
	,bgcolor_false(*this,"bg_color_false","black")
	,fgcolor_true(*this,"fg_color_true","black")
	,fgcolor_false(*this,"fg_color_false","black")
	,state(*this,"state",false)
{
//	std::cerr<<"Creatign a new UIndicator from gobject."<<std::endl;
	ctor();
}
// -------------------------------------------------------------------------
UIndicator::~UIndicator()
{
//	std::cerr << "Deleting a UIndicator." << std::endl;
}
// -------------------------------------------------------------------------
void UIndicator::init_widget()
{

}
// -------------------------------------------------------------------------
/*void UIndicator::sensorInfo(SensorMessage* sm)
{
	if( di.get_sens_id() != sm->id  )
		return;
	set_state(sm->value);
}*/
// -------------------------------------------------------------------------
void UIndicator::draw_state( bool smstate )
{
// Может просто сделать ещё одну функцию get_bgcolor( bool state ); (и fgcolor) ?
	Gdk::Color bg_col;
	Gdk::Color fg_col;
	if (smstate)
	{
		bg_col.set( get_bgcolor_true() );
		fg_col.set( get_fgcolor_true() );
	}
	else
	{
		bg_col.set( get_bgcolor_false() );
		fg_col.set( get_fgcolor_false() );
	}
	
	modify_bg(Gtk::STATE_NORMAL,bg_col);
	modify_bg(Gtk::STATE_ACTIVE,bg_col);
	modify_bg(Gtk::STATE_PRELIGHT,bg_col);

	lbl.modify_fg(Gtk::STATE_NORMAL,fg_col);
	lbl.modify_fg(Gtk::STATE_ACTIVE,fg_col);
	lbl.modify_fg(Gtk::STATE_PRELIGHT,fg_col);
}
// -------------------------------------------------------------------------
