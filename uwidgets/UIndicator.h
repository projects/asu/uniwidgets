#ifndef _UINDICATOR_H
#define _UINDICATOR_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <gdkmm.h>
#include "UEventBox.h"
#include "SensorProperty.h"
#include "global_macros.h"
// -------------------------------------------------------------------------
/*!\brief Класс индикатора.
 * \par
 * \todo написать об использовании и вообще необходимости в этом классе.
*/
class UIndicator : public UEventBox{
private:
	Gtk::Label lbl;
	
	void ctor();
	void draw_state(bool);

	Gdk::Color bg_col_0;
	Gdk::Color fg_col_0;
	Gdk::Color bg_col_1;
	Gdk::Color fg_col_1;
	

public:
	UIndicator();
	explicit UIndicator(GtkmmBaseType::BaseObjectType* gobject);
	~UIndicator();
	virtual void init_widget();

protected:
	void on_text_prop_changed();
	void on_pango_prop_changed();
	void on_state_changed();
	void on_line_wrap_changed();
	
	//SensorProp di;
public:
	Glib::Property<USensorProperty> di;
	
	ADD_PROPERTY( prop_text, Glib::ustring )
	ADD_PROPERTY( prop_pango, Glib::ustring )
	ADD_PROPERTY( line_wrap, bool )
	
	ADD_PROPERTY( bgcolor_true, Glib::ustring )
	ADD_PROPERTY( bgcolor_false, Glib::ustring )
	ADD_PROPERTY( fgcolor_true, Glib::ustring )
	ADD_PROPERTY( fgcolor_false, Glib::ustring )
	ADD_PROPERTY( state, bool )
};
#endif
