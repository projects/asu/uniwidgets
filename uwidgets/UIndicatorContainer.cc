#include <iostream>
#include <gdkmm.h>
#include "UIndicatorContainer.h"
//#include <gladeui/glade.h>

// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using UMessages::MessageId;
// -------------------------------------------------------------------------
#define INIT_UINDICATORCONTAINER_PROPERTIES() \
	property_page(*this, "page" , 0)
// -------------------------------------------------------------------------
void
UIndicatorContainer::ctor()
{
	connect_property_changed("page", sigc::mem_fun(this, &UIndicatorContainer::on_page_changed));
}
// -------------------------------------------------------------------------
UIndicatorContainer::UIndicatorContainer() :
	Glib::ObjectBase("uindicatorcontainer")
	,prev_page_(0)
	,INIT_UINDICATORCONTAINER_PROPERTIES()
	,current_child_(NULL)
{
	ctor();
}
// -------------------------------------------------------------------------
UIndicatorContainer::UIndicatorContainer(GtkmmBaseType::BaseObjectType* gobject) :
	BaseType(gobject)
	,prev_page_(0)
	,INIT_UINDICATORCONTAINER_PROPERTIES()
	,current_child_(NULL)
{
	ctor();
}
// -------------------------------------------------------------------------
UIndicatorContainer::~UIndicatorContainer()
{
}
// -------------------------------------------------------------------------
void UIndicatorContainer::blink(bool state, int time)
{
//	cout<<current_child_->get_name()<<" UIndicatorContainer::blink: blinktime="<<time<<endl;
	if(childinfomap_[current_child_]->blink_time == time)
		childinfomap_[current_child_]->set_state(state);
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::on_realize()
{
	Gtk::HBox::on_realize();
// 	on_page_changed();
}
// -------------------------------------------------------------------------
bool
UIndicatorContainer::on_expose_event(GdkEventExpose* ev)
{
	Gtk::HBox::on_expose_event(ev);

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	const Gtk::Allocation alloc = get_allocation();

	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();

	if ( !connected_ && get_property_disconnect_effect() == 1)
		UVoid::draw_disconnect_effect_1(cr, alloc);

	return true;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::on_map()
{
	Gtk::HBox::on_map();
	check_resize();
	Gtk::Allocation alloc = get_allocation();
	on_size_allocate(alloc);
}
// -------------------------------------------------------------------------
void
alloc_for_children(Gtk::Widget& w, Gtk::Allocation alloc)
{
	w.size_allocate(alloc);
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::on_size_allocate(Gtk::Allocation& alloc)
{
	if( !is_mapped() )
		return;
	set_allocation(alloc);

	foreach( sigc::bind( sigc::ptr_fun(alloc_for_children), alloc) );
}
// -------------------------------------------------------------------------
void UIndicatorContainer::on_add(Gtk::Widget* w)
{
	add_child(w);
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::add_child(Gtk::Widget* w)
{
// 	cout<<get_name()<<" on_add()..."<<endl;
	if (UVoid* uv = dynamic_cast<UVoid*>(w)) {
		uv->set_property_disconnect_effect(0);
		uv->set_property_auto_connect(false);
	}

	Gtk::HBox::on_add(w);
	ChildInfo* ci = new ChildInfo;

	ci->sensor_name = "DefaultObjectId";
	ci->node_name = "LocalhostNode";
	ci->sensor_id = UniWidgetsTypes::DefaultObjectId;
	ci->node_id = UniWidgetsTypes::DefaultObjectId;
	ci->value = 0;
	ci->blink_time = DEFAULT_BLINK_TIME;
	ci->blink = false;
	ci->confirm = false;
	ci->priority = 0;

	ci->wrong = true;
	ci->state = false;
	ci->confirmed = true;

	if( is_glade_editor )
	{
// 		GladeWidget* gw = glade_widget_get_from_gobject(G_OBJECT(w->gobj()));
// 		cout<< glade_widget_get_name(gw) <<"::add_child() дефолт в упаковочный пропертис"<<endl;
// 		GladeProperty* prop = glade_widget_get_pack_property(gw, "ch-sensor-name");
// 		if(GLADE_IS_PROPERTY(prop))
// 		{
// 			Glib::Value<string> prop_val;
// 			prop_val.init(prop->value);
// 			string sname(prop_val.get());
// 			cout<<get_name()<<"::add_child() prop_name = 'ch-sensor-name' prop_val = '"<<prop_val.get()<<"'"<<endl;
// 			if(sname.empty())
// 			{
// 				prop_val.set("DefaultObjectId");
// 				glade_property_set_value(prop, prop_val.gobj());
// 			}
// 		}
	}
	
	//TODO: check if we really need set_state in UVoid
	if (UVoid* uv = dynamic_cast<UVoid*>(w))
		ci->set_state = sigc::mem_fun(uv, &UVoid::set_state);

	childinfomap_[w] = ci;

	w->set_child_visible(false);
	on_page_changed();
}
// -------------------------------------------------------------------------
void UIndicatorContainer::on_remove(Gtk::Widget* w)
{
	remove_child(w);
}
// -------------------------------------------------------------------------
void UIndicatorContainer::remove_child(Gtk::Widget* w)
{
	Gtk::HBox::on_remove(w);
	delete childinfomap_[w];
	if (current_child_ = w)
		current_child_ = NULL;
}
// -------------------------------------------------------------------------
void  UIndicatorContainer::on_page_changed()
{
	BoxList ch = children();
// 	std::cout<<this<<"::on_page_changed() property_page = '"<<property_page<<"' prev_page_ = '"<<prev_page_<<"'"<<" ch.size() = '"<<ch.size()<<"'"<<std::endl;
	if (property_page >= 0 && property_page < ch.size())
	{
		if(property_page!=prev_page_)
			ch[prev_page_].get_widget()->set_child_visible(false);
		current_child_ = ch[property_page].get_widget();
		current_child_->set_child_visible(true);
		childinfomap_[current_child_]->set_state(true);
		prev_page_ = property_page;
	}
	queue_resize();
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_page(int page)
{
	property_page = page;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::connect_confirm(sigc::slot<void> slot, MessageId id)
{
	MsgConfirmSlot msg_slot = sigc::hide(sigc::hide(slot));
	get_connector()->signal_confirm().connect( msg_slot, id );
	return;
}
// -------------------------------------------------------------------------
ConfirmCtl::StateInfo
UIndicatorContainer::create_state_info(Gtk::Widget* child, StateId state_id)
{
	ChildInfo* ci = childinfomap_[child];

	ConfirmCtl::StateInfo sinfo;

	sinfo.priority = ci->priority;
	sinfo.blinkable = ci->blink;

	if(ci->blink)
	{
		//cout<<child->get_name()<<" UIndicatorContainer::create_state_info: blinktime="<<ci->blink_time<<endl;
		blinker.set_blink_time(ci->blink_time);
	}
	sinfo.start_blink = sigc::bind(sigc::mem_fun(*this, &UIndicatorContainer::start_blink),ci->blink_time);
	sinfo.stop_blink = sigc::bind(sigc::mem_fun(this, &UIndicatorContainer::stop_blink),child);

	sigc::slot<void, int> set_page =
		sigc::mem_fun(this, &UIndicatorContainer::set_page);

	sinfo.switch_on =
		bind(set_page, get_child_position(child));

	return sinfo;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_property_vfunc(GtkWidget *inchild, guint property_id, const GValue *value, GParamSpec   *pspec)
{
	Gtk::Widget* child = Glib::wrap(inchild);

// 	switch (property_id)
// 	{
// 		case 5: //SensorName
// 			g_assert(G_VALUE_HOLDS_STRING(value));
// 			set_child_sid(child, g_value_get_string(value));
// 			break;
// 		case 6: //NodeName
// 			g_assert(G_VALUE_HOLDS_STRING(value));
// 			set_child_nodeid(child, g_value_get_string(value));
// 			break;
// 		case 7: //SensorID
// 			g_assert(G_VALUE_HOLDS_LONG(value));
// 			set_child_sid(child, g_value_get_long(value));
// 			break;
// 		case 8: //NodeID
// 			g_assert(G_VALUE_HOLDS_LONG(value));
// 			set_child_nodeid(child, g_value_get_long(value));
// 			break;
// 		case 9: //value
// 			g_assert(G_VALUE_HOLDS_LONG(value));
// 			set_child_value(child, g_value_get_long(value));
// 			break;
// 		case 10: //priority
// 			g_assert(G_VALUE_HOLDS_INT(value));
// 			set_child_priority(child, g_value_get_int(value));
// 			break;
// 		case 11: //blink
// 			g_assert(G_VALUE_HOLDS_BOOLEAN(value));
// 			set_child_blink(child, g_value_get_boolean(value));
// 			break;
// 		case 12: //blink time
// 			g_assert(G_VALUE_HOLDS_INT(value));
// 			set_child_blink_time(child, g_value_get_int(value));
// 			break;
// 		case 13: //confirm
// 			g_assert(G_VALUE_HOLDS_BOOLEAN(value));
// 			set_child_confirm(child, g_value_get_boolean(value));
// 			break;
// 		default:
// 			Gtk::HBox::set_child_property_vfunc (inchild, property_id, value, pspec);
// 	}

	if( is_glade_editor )
	{
//		(*packing_changed_function)( inchild, pspec );
	}
	
	string prop_name(pspec->name);
	
	if( prop_name == "ch-sensor-name" )
	{
		g_assert(G_VALUE_HOLDS_STRING(value));
		Glib::Value<string> prop_val;
		prop_val.init(value);
// 		string wname(is_glade_editor? glade_widget_get_name(glade_widget_get_from_gobject(G_OBJECT(inchild))):child->get_name());
// 		cout<<wname<<"::set_child_property_vfunc() prop_name = '"<<prop_name<<"' prop_val = '"<<prop_val.get()<<"'"<<endl;
		set_child_sid(child, prop_val.get());
	}
	else if( prop_name == "ch-node-name" )
	{
		g_assert(G_VALUE_HOLDS_STRING(value));
		Glib::Value<string> prop_val;
		prop_val.init(value);
// 		string wname(is_glade_editor? glade_widget_get_name(glade_widget_get_from_gobject(G_OBJECT(inchild))):child->get_name());
// 		cout<<wname<<"::set_child_property_vfunc() prop_name = '"<<prop_name<<"' prop_val = '"<<prop_val.get()<<"'"<<endl;
		set_child_nodeid(child, prop_val.get());
	}
	else if( prop_name == "ch-sensor-id" )
	{
		g_assert(G_VALUE_HOLDS_LONG(value));
		set_child_sid(child, g_value_get_long(value));
	}
	else if( prop_name == "ch-node-id" )
	{
		g_assert(G_VALUE_HOLDS_LONG(value));
		set_child_nodeid(child, g_value_get_long(value));
	}
	else if( prop_name == "chvalue" )
	{
		g_assert(G_VALUE_HOLDS_LONG(value));
		set_child_value(child, g_value_get_long(value));
	}
	else if( prop_name == "chpriority" )
	{
		g_assert(G_VALUE_HOLDS_INT(value));
		set_child_priority(child, g_value_get_int(value));
	}
	else if( prop_name == "chblink" )
	{
		g_assert(G_VALUE_HOLDS_BOOLEAN(value));
		set_child_blink(child, g_value_get_boolean(value));
	}
	else if( prop_name == "chblinktime" )
	{
		g_assert(G_VALUE_HOLDS_INT(value));
		set_child_blink_time(child, g_value_get_int(value));
	}
	else if( prop_name == "chconfirm" )
	{
		g_assert(G_VALUE_HOLDS_BOOLEAN(value));
		set_child_confirm(child, g_value_get_boolean(value));
	}
	else
		Gtk::HBox::set_child_property_vfunc (inchild, property_id, value, pspec);
}
// -------------------------------------------------------------------------
#define check_child( W ) \
	if ( childinfomap_.count(W) == 0 ) { \
		std::cerr << "(UIndicatorContainer):Error:" \
			  << W->get_name() << \
			     " is not in container" <<std::endl; \
		return; \
	}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_sid(Gtk::Widget* child, std::string sensor_name)
{
	check_child(child);
	StateId id = get_child_state_id(child);
	ChildInfo *ci = childinfomap_[child];

	if (ci->sensor_name == sensor_name)
		return;

	ci->sensor_name = sensor_name;
	if (ci->sensor_id != UniWidgetsTypes::DefaultObjectId)
		return;
	
	if (!ci->wrong)
	{
		set_child_connector(*child, true);
		confirm_ctl_.remove(id);
	}

	id = get_child_state_id(child);
	if (confirm_ctl_.hasState(id) || id._id == UniWidgetsTypes::DefaultObjectId)
	{
		ci->wrong = true;
		return;
	}
	
	ConfirmCtl::StateInfo info = create_state_info(child, id);
	confirm_ctl_.add(id, info);
	ci->wrong = false;
	if (get_connector() && get_connector()->connected())
		set_child_connector(*child);
	return;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_sid(Gtk::Widget* child, UniWidgetsTypes::ObjectId sid)
{
	check_child(child);
	StateId id = get_child_state_id(child);
	ChildInfo *ci = childinfomap_[child];

	if (ci->sensor_id == sid)
		return;

	ci->sensor_id = sid;
	if (!ci->wrong)
	{
		set_child_connector(*child, true);
		confirm_ctl_.remove(id);
	}
	
	id = get_child_state_id(child);
	if (confirm_ctl_.hasState(id) || sid == UniWidgetsTypes::DefaultObjectId)
	{
		ci->wrong = true;
		return;
	}
	
	ConfirmCtl::StateInfo info = create_state_info(child, id);
	confirm_ctl_.add(id, info);
	ci->wrong = false;
	if (get_connector() && get_connector()->connected())
		set_child_connector(*child);
	return;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_nodeid(Gtk::Widget* child, std::string node_name)
{
	check_child(child);
	StateId id = get_child_state_id(child);
	ChildInfo *ci = childinfomap_[child];

	if (ci->node_name == node_name)
		return;
	
	ci->node_name = node_name;
	if (ci->node_id != UniWidgetsTypes::DefaultObjectId)
		return;
	
	if (!ci->wrong)
	{
		set_child_connector(*child, true);
		confirm_ctl_.remove(id);
	}

	id = get_child_state_id(child);
	if (confirm_ctl_.hasState(id) || id._id == UniWidgetsTypes::DefaultObjectId)
	{
		ci->wrong = true;
		return;
	}

	ConfirmCtl::StateInfo info = create_state_info(child, id);
	confirm_ctl_.add( id, info );
	ci->wrong = false;
	set_child_connector(*child);;
	return;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_nodeid(Gtk::Widget* child, UniWidgetsTypes::ObjectId nodeid)
{
	check_child(child);
	StateId id = get_child_state_id(child);
	ChildInfo *ci = childinfomap_[child];
	
	if (ci->node_id == nodeid)
		return;
	ci->node_id = nodeid;
	
	if (!ci->wrong)
	{
		set_child_connector(*child, true);
		confirm_ctl_.remove(id);
	}
	
	id = get_child_state_id(child);
	if (confirm_ctl_.hasState(id) || id._id == UniWidgetsTypes::DefaultObjectId)
	{
		ci->wrong = true;
		return;
	}
	ConfirmCtl::StateInfo info = create_state_info(child, id);
	confirm_ctl_.add( id, info );
	ci->wrong = false;
	set_child_connector(*child);;
	return;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_value(Gtk::Widget* child, long value)
{
	check_child(child);
	StateId id = get_child_state_id(child);

	ChildInfo* ci = childinfomap_[child];
	if (ci->value == value)
		return;
	ci->value = value;
	if ( !ci->wrong ) {
		set_child_connector(*child, true);
		confirm_ctl_.remove(id);
	}
	id._value = value;
	if ( confirm_ctl_.hasState(id) || id._id == -1 ) {
		ci->wrong = true;
		return;
	}
	ConfirmCtl::StateInfo info = create_state_info(child, id);
	confirm_ctl_.add( id, info );
	ci->wrong = false;
	set_child_connector(*child);;
	return;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_blink(Gtk::Widget* child, bool blink)
{
	check_child(child);
	ChildInfo *ci = childinfomap_[child];
	ci->blink = blink;
	if (ci->wrong)
		return;
	StateId id = get_child_state_id(child);
	confirm_ctl_.setBlinkable(id, blink);
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_confirm(Gtk::Widget* child, bool confirm)
{
	check_child(child);
	StateId id = get_child_state_id(child);

	ChildInfo* ci = childinfomap_[child];
	if (ci->confirm == confirm)
		return;
	ci->confirm = confirm;
	if (!ci->wrong) {
		set_child_connector(*child, true);
		confirm_ctl_.remove(id);
	}
	id._msg_type = confirm;
	if (confirm_ctl_.hasState(id)) {
		ci->wrong = true;
		return;
	}
	ConfirmCtl::StateInfo info = create_state_info(child, id);
	confirm_ctl_.add(id, info);
	ci->wrong = false;
	set_child_connector(*child);
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_blink_time(Gtk::Widget* child, int blink_time)
{
	StateId id = get_child_state_id(child);
	ChildInfo *ci = childinfomap_[child];
	ci->blink_time = blink_time>0? blink_time:DEFAULT_BLINK_TIME;
//	cout<<child->get_name()<<" UIndicatorContainer::set_child_blink_time: blinktime="<<ci->blink_time<<endl;
	if (ci->wrong)
		return;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_priority(Gtk::Widget* child, int priority)
{
	ChildInfo *ci = childinfomap_[child];
	ci->priority = priority;
	if (ci->wrong)
		return;
	StateId id = get_child_state_id(child);
	confirm_ctl_.setPriority( id, priority );
	return;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::get_child_property_vfunc(GtkWidget *inchild, guint property_id, GValue *value, GParamSpec *pspec)
{
	Gtk::Widget* child = Glib::wrap(inchild);

	if( !strcmp (pspec->name, "ch-sensor-name") )
	{
		g_assert(G_VALUE_HOLDS_STRING(value));
		g_value_set_string(value,childinfomap_[child]->sensor_name.c_str());
	}
	else if( !strcmp (pspec->name, "ch-node-name") )
	{
		g_assert(G_VALUE_HOLDS_STRING(value));
		g_value_set_string(value,childinfomap_[child]->node_name.c_str());
	}
	else if( !strcmp (pspec->name, "ch-sensor-id") )
	{
		g_assert(G_VALUE_HOLDS_LONG(value));
		g_value_set_long(value,childinfomap_[child]->sensor_id);
	}
	else if( !strcmp (pspec->name, "ch-node-id") )
	{
		g_assert(G_VALUE_HOLDS_LONG(value));
		g_value_set_long(value,childinfomap_[child]->node_id);
	}
	else if( !strcmp (pspec->name, "chvalue") )
	{
		g_assert(G_VALUE_HOLDS_LONG(value));
		g_value_set_long(value,childinfomap_[child]->value);
	}
	else if( !strcmp (pspec->name, "chpriority") )
	{
		g_assert(G_VALUE_HOLDS_INT(value));
		g_value_set_int(value,childinfomap_[child]->priority);
	}
	else if( !strcmp (pspec->name, "chblink") )
	{
		g_assert(G_VALUE_HOLDS_BOOLEAN(value));
		g_value_set_boolean(value,childinfomap_[child]->blink);
	}
	else if( !strcmp (pspec->name, "chblinktime") )
	{
		g_assert(G_VALUE_HOLDS_INT(value));
		g_value_set_int(value,childinfomap_[child]->blink_time);
	}
	else if( !strcmp (pspec->name, "chconfirm") )
	{
		g_assert(G_VALUE_HOLDS_BOOLEAN(value));
		g_value_set_boolean(value,childinfomap_[child]->confirm);
	}
	else
		Gtk::HBox::get_child_property_vfunc (inchild, property_id, value, pspec);
}
// -------------------------------------------------------------------------
UniWidgetsTypes::ObjectId
UIndicatorContainer::get_child_sid(Gtk::Widget* child)
{
	return childinfomap_[child]->sensor_id;
}
// -------------------------------------------------------------------------
UniWidgetsTypes::ObjectId
UIndicatorContainer::get_child_nodeid(Gtk::Widget* child)
{
	return childinfomap_[child]->node_id;
}
// -------------------------------------------------------------------------
long
UIndicatorContainer::get_child_value(Gtk::Widget* child)
{
	return childinfomap_[child]->value;
}
// -------------------------------------------------------------------------
bool
UIndicatorContainer::get_child_blink(Gtk::Widget* child)
{
	return childinfomap_[child]->blink;
}
// -------------------------------------------------------------------------
bool
	UIndicatorContainer::get_child_blink_time(Gtk::Widget* child)
{
	return childinfomap_[child]->blink_time;
}
// -------------------------------------------------------------------------
bool
UIndicatorContainer::get_child_confirm(Gtk::Widget* child)
{
	return childinfomap_[child]->confirm;
}
// -------------------------------------------------------------------------
bool
UIndicatorContainer::get_child_state(Gtk::Widget* child)
{
	cerr << get_name() << ": get_child_state: stab" << endl;
	return false;
}
// -------------------------------------------------------------------------
int
UIndicatorContainer::get_child_position(Gtk::Widget* child)
{
	GValue out = {0};
	g_value_init(&out, G_TYPE_INT);
	gtk_container_child_get_property( GTK_CONTAINER(gobj() ),
			GTK_WIDGET(child->gobj()),
			"position",
			&out);
	return g_value_get_int(&out);
}
// -------------------------------------------------------------------------
StateId
UIndicatorContainer::get_child_state_id(Gtk::Widget* child)
{
	//TODO: make normal error processing
	assert(childinfomap_.count(child) > 0);
	ChildInfo* ci = childinfomap_[child];
	StateId id;
	if ( ci->sensor_id == UniWidgetsTypes::DefaultObjectId && get_connector() )
		id._id = get_connector()->getSensorID(ci->sensor_name);
	else
		id._id = ci->sensor_id;
	if( ci->node_id == UniWidgetsTypes::DefaultObjectId && get_connector() )
		id._node = get_connector()->getNodeID(ci->node_name);
	else
		id._node = ci->node_id;
	id._value = ci->value;
	id._msg_type = ci->confirm;
	return id;
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::start_blink(int time)
{
	//cout<<"UIndicatorContainer::start_blink: blinktime="<<time<<endl;
	connection_blink_.disconnect();
	connection_blink_ = blinker.signal_blink[time].connect(
			sigc::mem_fun(this,&UIndicatorContainer::blink));
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::stop_blink(Gtk::Widget* child)
{
	if(child)
		childinfomap_[child]->set_state(true);
	else	
		childinfomap_[current_child_]->set_state(true);
	connection_blink_.disconnect();
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::update_msg_on( StateId id, bool* confirmed )
{
	if ( ! *confirmed )
		return;

	confirm_ctl_.update(id, true);

	if (get_property_lock_view() && !locked_)
		add_lock(*this);

	*confirmed = false;

	sigc::slot<void, MessageId, time_t> off =
		sigc::hide(sigc::hide
				(sigc::bind(sigc::mem_fun(this, &UIndicatorContainer::update_msg_off), id, confirmed)));
	MessageId mid = MessageId(id._id, id._node, id._value);

	get_connector()->signal_confirm().connect(off, mid);
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::update_msg_off(StateId id, bool* confirmed)
{	
	ChildInfo* ci = nullptr;
	for( auto& cit:childinfomap_ )
	{
		UniWidgetsTypes::ObjectId _id;
		UniWidgetsTypes::ObjectId _node;
		if ( cit.second->sensor_id == UniWidgetsTypes::DefaultObjectId && get_connector() )
			_id = get_connector()->getSensorID(cit.second->sensor_name);
		else
			_id = cit.second->sensor_id;
		if( cit.second->node_id == UniWidgetsTypes::DefaultObjectId && get_connector() )
			_node = get_connector()->getNodeID(cit.second->node_name);
		else
			_node = cit.second->node_id;
		if( id._id == _id && id._node == _node && id._value == cit.second->value )
		{
			ci = cit.second;
			break;
		}
	}
	MessageId mid(id._id, id._node, id._value);
	if( get_connector()->signal_confirm().get_message_status(mid) == DROPPED )
	{
		confirm_ctl_.update( id, false );
		if ( ci )
			ci->out_connection.disconnect();
	}
	else
	{
		confirm_ctl_.update( id, true );
		if ( ci && !ci->wrong )
		{
			ci->out_connection.disconnect();
			sigc::slot<void, ObjectId, ObjectId, long> off =
				sigc::hide(sigc::hide(sigc::hide(
					sigc::bind( sigc::mem_fun(this, &UIndicatorContainer::update_msg_off), id, confirmed) )));
			ci->out_connection =
				get_connector()->signals().connect_value_out(
					off, id._id, id._node, id._value);
		}
	}
	
	if( *confirmed )
		return;
	*confirmed = true;
	
	/* TODO: make check if locked */
	if (get_property_lock_view())
		unlock_current();
	
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_child_connector(Gtk::Widget& child, bool empty)
{
	UVoid* voidChild = dynamic_cast<UVoid*>(&child);
	if(voidChild && get_connector())
		voidChild->set_connector(get_connector());
	ChildInfo* ci = childinfomap_[&child];

	if (ci->wrong)
		return;

	if(empty == true || !get_connector()) {
		ci->in_connection.disconnect();
		ci->out_connection.disconnect();
		ci->message_connection.disconnect();
		return;
	}

	StateId id = get_child_state_id(&child);
	ObjectId true_nodeid = (id._node == UniWidgetsTypes::DefaultObjectId) ? get_connector()->getLocalNode() : id._node;

	if (ci->confirm) { //connect confirm message
		MessageId msgid(id._id, true_nodeid, id._value);

		sigc::slot<void, MessageId, std::string> on =
			sigc::hide(sigc::hide(
				sigc::bind(sigc::mem_fun(this, &UIndicatorContainer::update_msg_on), id, &ci->confirmed)));
		ci->message_connection =
			get_connector()->signals().connect_on_message( on, msgid);
	}
	else { //connect value_in signal

		sigc::slot<void,StateId,bool> update =
			sigc::mem_fun(&confirm_ctl_, &IndConfirmCtl::update);

		sigc::slot<void, ObjectId, ObjectId, long> on =
			sigc::hide(sigc::hide(sigc::hide(
				sigc::bind( update, id, true) )));

		sigc::slot<void, ObjectId, ObjectId, long> off =
			sigc::hide(sigc::hide(sigc::hide(
				sigc::bind( update, id, false) )));

		ci->in_connection =
			get_connector()->signals().connect_value_in(
				on, id._id, true_nodeid, id._value);

		ci->out_connection =
			get_connector()->signals().connect_value_out(
				off, id._id, true_nodeid, id._value);
	}

	if (get_connector()->connected())
		child_on_connect(child);
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::child_on_connect(Gtk::Widget& child)
{
	ChildInfo* ci = childinfomap_[&child];

	if ((ci == NULL) || (ci->wrong))
		return;

	StateId id = get_child_state_id(&child);
	ObjectId true_nodeid = (id._node == UniWidgetsTypes::DefaultObjectId) ? get_connector()->getLocalNode() : id._node;
	
	confirm_ctl_.update(id, false);
	ci->confirmed = true;

	if ( ci->confirm ) { //init confirm message
		MessageId msgid(id._id, true_nodeid, id._value);
		ConfirmSignal& cs = get_connector()->signal_confirm();
		//TODO: There my be a wrong logic, when states has the same priority.
		ConfirmSignal::const_iterator it =
			find(cs.begin(), cs.end(), msgid); //may be not optimal
		if (it != cs.end())
			update_msg_on(id, &ci->confirmed);
	}
	else { //init value_in signal
		long value = get_connector()->get_value(id._id, true_nodeid);
		confirm_ctl_.update(id, value == id._value);
	}
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::disconnect_child(Gtk::Widget& child)
{
	//TODO: remove
	return;
}
// -------------------------------------------------------------------------
// void
// UIndicatorContainer::add_lock(Gtk::Widget *w)
// {
// 	assert (w != NULL);
// 	Gtk::Container *c  =this;
// 	Gtk::Container *prev = this;
// 	while ( (c = c->get_parent()) != NULL ) {
// 		if (UVoid* uv = dynamic_cast<UVoid*>(c)) {
// 			uv->add_lock(*prev);
// 			break;
// 		}
// 		prev = c;
// 	}
// }
// -------------------------------------------------------------------------
// void UIndicatorContainer::unlock_current()
// {
// 	Gtk::Container *c = this;
// 	while ( (c = c->get_parent()) != NULL )
// 		if (UVoid* uv = dynamic_cast<UVoid*>(c)) {
// 			uv->unlock_current();
// 			break;
// 		}
// }
// -------------------------------------------------------------------------
void
UIndicatorContainer::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UVoid::set_connector(connector);

	sigc::slot<void, Gtk::Widget&> set_child_connector =
		sigc::bind(sigc::mem_fun(this, &UIndicatorContainer::set_child_connector),false);

	foreach(set_child_connector);
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::on_connect() throw()
{
	UVoid::on_connect();
	confirm_ctl_.reset();

	sigc::slot<void, Gtk::Widget&> child_on_connect =
		sigc::mem_fun(this, &UIndicatorContainer::child_on_connect);

	foreach(child_on_connect);

	queue_draw();
}
// -------------------------------------------------------------------------
void
UIndicatorContainer::on_disconnect() throw()
{
	UVoid::on_disconnect();
	stop_blink();
	//TODO: that may be wrong some times
	if(current_child_ && childinfomap_[current_child_]->blink)
		childinfomap_[current_child_]->set_state(false);
	queue_draw();
}
// -------------------------------------------------------------------------
