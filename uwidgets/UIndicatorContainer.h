#ifndef _UINDICATORCONTAINER_H
#define _UINDICATORCONTAINER_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <gtkmm.h>
#include "UDefaultFunctions.h"
#include "ConfirmSignal.h"
#include "ConfirmController.h"
#include "USignals.h"
#include "plugins.h"
#include "global_macros.h"
// -------------------------------------------------------------------------
struct ChildInfo
{
	ChildInfo ():
		sensor_name("DefaultObjectId"),
		node_name("LocalhostNode"),
		sensor_id(UniWidgetsTypes::DefaultObjectId),
		node_id(UniWidgetsTypes::DefaultObjectId),
		value(0),
		state(false),
		wrong(true),
		priority(0),
		blink_time(DEFAULT_BLINK_TIME),
		blink(false),
		confirm(false),
		confirmed(true)
		{}

	std::string sensor_name;
	std::string node_name;
	UniWidgetsTypes::ObjectId sensor_id;
	UniWidgetsTypes::ObjectId node_id;
	long value;
	bool state;

	bool wrong;

	int priority;

	int blink_time;
	bool blink;
	bool confirm;
	bool confirmed;

	sigc::slot<void, bool> set_state;

	USignals::VConn in_connection;
	USignals::VConn out_connection;
	USignals::Connection message_connection;
	MsgConfirmConnection confirm_connection;
};
// -------------------------------------------------------------------------
struct StateId
{
	StateId ():
		_id(UniWidgetsTypes::DefaultObjectId),
		_node(UniWidgetsTypes::DefaultObjectId),
		_value(0),
		_msg_type(false)
	{}
	StateId( UniWidgetsTypes::ObjectId id,
		UniWidgetsTypes::ObjectId node,
		long value) :
		_id(id), _node(node), _value(value), _msg_type(false)
	{}

	bool operator< (const StateId& y) const {
		if (_id < y._id) return true;
		if (_id > y._id) return false;
		if (_node < y._node) return true;
		if (_node > y._node) return false;
		if (_value < y._value) return true;
		if (_value > y._value) return false;
		if (_msg_type < y._msg_type ) return true;
		else return false;
	}

	UniWidgetsTypes::ObjectId _id;
	UniWidgetsTypes::ObjectId _node;
	long _value;
	bool _msg_type;
};
// -------------------------------------------------------------------------
typedef std::unordered_map<const Gtk::Widget*, ChildInfo*> ChildInfoMap;
// -------------------------------------------------------------------------
class IndConfirmCtl : public ConfirmCtl::ConfirmController<StateId>
{
public:
	IndConfirmCtl() {}
	virtual ~IndConfirmCtl() {}
protected:
	void Error(const char* msg)
	{ std::cerr << msg << std::endl; }
};
// -------------------------------------------------------------------------
/*!\brief Класс контейнера.
 * \par
 * Класс предназначен для хранения виджетов отображающих состояние датчиков.
 * Класс основан на Gtk::HBox и для каждой позиции контейнера соответствует
 * соответствующее состояние. При срабатывании датчика контейнер скрывает
 * предыдущее состояние и показывает новое(в кратце).
*/
class UIndicatorContainer : public UDefaultFunctions<Gtk::HBox>
{
public:
	UIndicatorContainer();
	explicit UIndicatorContainer(GtkmmBaseType::BaseObjectType* gobject);
	virtual ~UIndicatorContainer();

	virtual void add_child(Gtk::Widget* w);
	virtual void remove_child(Gtk::Widget* w);

	typedef void(*packingChangedFunction)(GtkWidget*, const GParamSpec*);
	inline void connect_packing_changed_function(packingChangedFunction func){ packing_changed_function = func; };
	
	virtual void set_child_property_vfunc(GtkWidget* child,
			guint property_id,
			const GValue* value,
			GParamSpec* pspec);
	virtual void get_child_property_vfunc(GtkWidget* child,
			guint property_id,
			GValue* value,
			GParamSpec* pspec);

	void startup_init(void);

	virtual void on_page_changed();
	void set_page(int page);

	void set_child_sid(Gtk::Widget* child, UniWidgetsTypes::ObjectId sid);
	void set_child_sid(Gtk::Widget* child, std::string sname);
	void set_child_nodeid(Gtk::Widget* child, UniWidgetsTypes::ObjectId nodeid);
	void set_child_nodeid(Gtk::Widget* child, std::string nodename);
	void set_child_value(Gtk::Widget* child, long value);

	void set_child_priority(Gtk::Widget* child, int priority);
	void set_child_blink_time(Gtk::Widget* child, int blink_time);
	void set_child_blink(Gtk::Widget* child, bool blink);
	void set_child_confirm(Gtk::Widget* child, bool confirm);

	UniWidgetsTypes::ObjectId get_child_sid(Gtk::Widget* child);
	UniWidgetsTypes::ObjectId get_child_nodeid(Gtk::Widget* child);
	long get_child_value(Gtk::Widget* child);

	bool get_child_blink(Gtk::Widget* child);
	bool get_child_blink_time(Gtk::Widget* child);
	bool get_child_confirm(Gtk::Widget* child);

	bool get_child_state(Gtk::Widget* child);
	int get_child_position(Gtk::Widget* child);

	void start_blink(int time);
	void stop_blink(Gtk::Widget* child = 0);
	void connect_confirm(sigc::slot<void>, UMessages::MessageId id);

	ADD_PROPERTY( property_page, int )				/*!< свойство:*/
	
protected:
	virtual void on_realize();
	virtual void on_add(Gtk::Widget* w);
	virtual void on_remove(Gtk::Widget* w);
	virtual bool on_expose_event(GdkEventExpose*);
	virtual void on_map();
	virtual void on_size_allocate(Gtk::Allocation& alloc);
	virtual void set_connector(const ConnectorRef& connector) throw();

	virtual void on_connect() throw();
	virtual void on_disconnect() throw();

	void child_on_connect(Gtk::Widget&);
	
	Gtk::Widget* current_child_;

private:
	void ctor();
	IndConfirmCtl confirm_ctl_;

	ChildInfoMap childinfomap_;

	int prev_page_;
	bool checked_;

	sigc::connection connection_blink_;

	packingChangedFunction packing_changed_function;
	
	StateId get_child_state_id( Gtk::Widget* child);
	ConfirmCtl::StateInfo create_state_info( Gtk::Widget* child, StateId state_id);
	void connect_child(Gtk::Widget* child);
	void disconnect_child(Gtk::Widget& child);
	void update_msg_on( StateId id, bool* confirmed);
	void update_msg_off( StateId id, bool* confirmed);

	void blink(bool state, int time=DEFAULT_BLINK_TIME);
	void set_child_connector(Gtk::Widget&, bool empty = false);

	DISALLOW_COPY_AND_ASSIGN(UIndicatorContainer);
};
// -------------------------------------------------------------------------
template<>
inline GType UObj_Get_Type<UIndicatorContainer>()
{
static GType gtype = 0;
	if (gtype)
		return gtype;

	UIndicatorContainer* dummy = new UIndicatorContainer();
	GtkContainerClass* container_klass = GTK_CONTAINER_GET_CLASS( dummy->gobj() );
	
	guint n_properties = 0;
	gtk_container_class_list_child_properties( G_OBJECT_GET_CLASS(dummy->gobj()), &n_properties);
// 	n_properties--;
	std::cout<<"UObj_Get_Type<UIndicatorContainer> n_properties="<<n_properties<<std::endl;
	GParamSpec* spec;
	spec = g_param_spec_string ("ch-sensor-name", "имя датчика", "'имя датчика' используется, если 'id датчика' = -1", "DefaultObjectId",
			GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
	gtk_container_class_install_child_property( container_klass, n_properties++,spec);
	
	spec = g_param_spec_string ("ch-node-name", "имя узла датчика", "'имя узла датчика' используется, если 'id узла датчика' = -1", "LocalhostNode",
			GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
	gtk_container_class_install_child_property( container_klass, n_properties++,spec);
	
	spec = g_param_spec_long ("ch-sensor-id", "id датчика", "id датчика, если -1 (DefaultObjectId), то используется 'имя датчика'", LONG_MIN, LONG_MAX, -1,
			GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
	gtk_container_class_install_child_property( container_klass, n_properties++,spec);

	spec = g_param_spec_long ("ch-node-id", "id узла датчика", "id узла датчика, если -1 (LocalhostNode), то используется 'имя узла датчика'", LONG_MIN, LONG_MAX, -1,
			GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
	gtk_container_class_install_child_property( container_klass, n_properties++, spec);

	spec = g_param_spec_long("chvalue", "значение датчика", "значение датчика", LONG_MIN, LONG_MAX, 0,
			GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
	gtk_container_class_install_child_property( container_klass, n_properties++, spec );
	
	spec = g_param_spec_int ("chpriority", "приоритет", "Приоритет при одновременном срабатывании нескольких страниц (отображаться будет та, у кого приоритет выше)", INT_MIN, INT_MAX, 0,
			GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
	gtk_container_class_install_child_property( container_klass, n_properties++, spec );
	
	spec =	g_param_spec_boolean ("chblink", "мигание", "Мигание при срабатывании датчика (если сам виджет поддерживает мигание, т.е. смену state)", false,
			GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
	gtk_container_class_install_child_property( container_klass, n_properties++, spec );
	
	spec = g_param_spec_int ("chblinktime", "время мигания", "Время мигания (мс)", INT_MIN, INT_MAX, DEFAULT_BLINK_TIME,
			GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
	gtk_container_class_install_child_property( container_klass, n_properties++, spec );

	spec = g_param_spec_boolean ("chconfirm", "требует квитирования", "требует ли квитирования данная страница", false,
			GParamFlags(G_PARAM_READABLE|G_PARAM_WRITABLE));
	gtk_container_class_install_child_property( container_klass, n_properties++, spec );

	gtype = G_OBJECT_TYPE(dummy->gobj());
	delete( dummy );
	Glib::wrap_register(gtype, &UObj_Wrap_New<UIndicatorContainer>);

	return gtype;
}
#endif
