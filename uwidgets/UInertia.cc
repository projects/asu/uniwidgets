/*
класс реализующий инерцию изменения аналогового значения
*/
#include "UInertia.h"
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

// -------------------------------------------------------------------------
UInertia::UInertia(Gtk::Widget* w) :
owner(w)
,prop_type(*w,"inert-func",TYPE_1)
,prop_total_time(*w,"inert-time-ms",1500)
,prop_pow(*w,"inert-pow-of-func",1.0001)
,last_value(0)
,value(0)
,temp_value(0)
,inert_time(0)
{
}
// -------------------------------------------------------------------------
UInertia::~UInertia()
{
}
// -------------------------------------------------------------------------
GType inerttype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { UInertia::TYPE_1, "TYPE_1", "S(t) = a*t^n - b*t^(n+1)" },{ UInertia::TYPE_2, "TYPE_2", "S(t) = a*t^(1/n) - b*t^n" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("InertType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UInertia::InertType>::value_type()
{
	return inerttype_get_type();
}
// -------------------------------------------------------------------------
UInertia::inertValueChangedSignal& UInertia::signal_inert_value_change()
{
	return signal;
}
// -------------------------------------------------------------------------
bool UInertia::on_inert_timer_tick()
{
	inert_time = inert_time + INERT_TIMER_TIME;
	if(inert_time >= get_prop_total_time())
	{
		temp_value = value;
		if (inert_tmr.connected())
			inert_tmr.disconnect();
		
//		cout<<owner->get_name()<<"::on_inert_timer_tick value="<<value<<" temp_value="<<temp_value<<" last_value="<<last_value<<" inert_time="<<inert_time<< endl;
		signal.emit(temp_value, true);
		return false;
	}
	else
	{
		double t = (double)inert_time/1000;
		double T = (double)get_prop_total_time()/1000;
		double s = value - last_value;
		double n = get_prop_pow()>1 ? get_prop_pow():1.0001;
		
		if(get_prop_type() == TYPE_1)
		{
/*
			за основу берется уровнение перемещения S(t) = a*t^n - b*t^(n+1)
			функция достигает максимума (экстремума) при скорости dS/dt=0, т.е. V(t)= dS/dt = a*n*t^(n-1) - b*(n+1)*t^n = 0
			скорость равна 0 при t=0 и t=T, где T - общее время за которое стрелка должна закончить движение к новому значению датчика
			общее перемещение стрелки - это площадь под кривой скорости тоесть определенный интеграл ∫V(t)=S(t) от 0 до T, a*T^n - b*T^(n+1) = S,
			где S - перемещение стрелки (разница между бывшим положением и тем что нужно достичь)
			решив систему уравнений можно выразить a и b через S и T
			┌								┌						┌
			│a*T^n - b*T^(n+1) = S			│a*T^n - b*T^(n+1) = S	│b = n*S/T^(n+1)
			┤								┤						┤
			│a*n*T^(n-1) - b*(n+1)*T^n = 0	│a = (b*(n+1)*T)/n		│a = S*(n+1)*T/T^(n+1)
			└								└						└
			подставив a и b в уравнение движения получим смещение для заданого времени относительно положения стрелки до начала движения
*/			
			double b = n*s / pow(T, n+1);
			double a = (n+1)*s*T / pow(T, n+1);
			
			double ds = a*pow(t, n) - b*pow(t, n+1);
			temp_value = last_value + ds;
//			cout<<owner->get_name()<<"::on_inert_timer_tick(TYPE_1) value="<<value<<" temp_value="<<temp_value<<" last_value="<<last_value<<" inert_time="<<inert_time<<" ds="<<ds<< endl;
		}
		else if(get_prop_type() == TYPE_2)
		{
/*
			за основу берется уровнение перемещения S(t) = a*t^(1/n) - b*t^n
			функция достигает максимума (экстремума) при скорости dS/dt=0, т.е. V(t)= dS/dt = a*t^(1/n-1) - b*n²*t^(n-1) = 0
			скорость равна 0 при t=0 и t=T, где T - общее время за которое стрелка должна закончить движение к новому значению датчика
			общее перемещение стрелки - это площадь под кривой скорости тоесть определенный интеграл ∫V(t)=S(t) от 0 до T, a*T^(1/n) - b*T^n = S,
			где S - перемещение стрелки (разница между бывшим положением и тем что нужно достичь)
			решив систему уравнений можно выразить a и b через S и T
			┌								┌							┌
			│a*T^(1/n) - b*T^n = S			│a*T^(1/n) - b*T^n = S		│b = S/(T^n(n²-1))
			┤								┤							┤
			│a*T^(1/n-1) - b*n²*T^(n-1)=0	│a = b*n²*T^(n-1/n)			│a = S*n²/(T^(1/n)(n²-1)
			└								└							└
			подставив a и b в уравнение движения получим смещение для заданого времени относительно положения стрелки до начала движения
*/
			double b = s / (pow(T,n) * (pow(n,2) - 1));
			double a = s * pow(n,2) / (pow(T, (double)1/n) * (pow(n,2) - 1));
			
			double ds = a*pow(t, (double)1/n) - b*pow(t, n);
			temp_value = last_value + ds;
//			cout<<owner->get_name()<<"::on_inert_timer_tick(TYPE_2) value="<<value<<" temp_value="<<temp_value<<" last_value="<<last_value<<" inert_time="<<inert_time<<" ds="<<ds<< endl;
		}
		
		signal.emit(temp_value, false);
		return true;
	}
}
// -------------------------------------------------------------------------
void UInertia::set_value(double val)
{
//	cout<<owner->get_name()<<"::set_value value="<<val<< endl;
	last_value = temp_value;
	value = val;
	
	inert_time = 0;
	if (!inert_tmr.connected())
	{
		inert_tmr= Glib::signal_timeout().connect(sigc::mem_fun(*this, &UInertia::on_inert_timer_tick),INERT_TIMER_TIME);
	}
}
// -------------------------------------------------------------------------
void UInertia::set_value(double start_val, double val)
{
//	cout<<owner->get_name()<<"::set_value value="<<val<<" start_val="<<start_val<< endl;
	temp_value = start_val;
	set_value(val);
}
// -------------------------------------------------------------------------
