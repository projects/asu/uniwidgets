/*
	класс реализующий инерцию изменения аналогового значения
*/

#ifndef UINERTIA_H
#define UINERTIA_H
#include <vector>
#include <map>
#include <sigc++/sigc++.h>
#include <gtkmm.h>
#include <global_macros.h>

/* Inert Timer`s time*/
#define INERT_TIMER_TIME	40

class UInertia : public Glib::Object
{
	public:
		UInertia(Gtk::Widget* w);
		virtual ~UInertia();

		enum InertType
		{
			TYPE_1=0,
			TYPE_2
		};

		typedef sigc::slot<void, double, bool> inertValueChangedSlot; // double - промежуточное инерциальное значение, bool - признак окончания инерции
		typedef sigc::signal<void, double, bool> inertValueChangedSignal;
		
		virtual inertValueChangedSignal& signal_inert_value_change();	/* */
		virtual void set_value(double val);								/* */
		virtual void set_value(double start_val, double val);			/* */
		
		inline Glib::PropertyProxy<UInertia::InertType> property_type()
		{
			return prop_type.get_proxy();
		}
		
		inline Glib::PropertyProxy_ReadOnly<UInertia::InertType> property_type() const
		{
			return Glib::PropertyProxy_ReadOnly<UInertia::InertType>(this,"inert-func");
		}
		inline Glib::PropertyProxy<int> property_total_time()
		{
			return prop_total_time.get_proxy();
		}
		
		inline Glib::PropertyProxy_ReadOnly<int> property_total_time() const
		{
			return Glib::PropertyProxy_ReadOnly<int>(this,"inert-time-ms");
		}
		
		inline Glib::PropertyProxy<double> property_pow()
		{
			return prop_pow.get_proxy();
		}
		
		inline Glib::PropertyProxy_ReadOnly<double> property_pow() const
		{
			return Glib::PropertyProxy_ReadOnly<double>(this,"inert-pow-of-func");
		}
		
	protected:

		Gtk::Widget* owner;
		
		inertValueChangedSignal signal;
		inertValueChangedSlot slot;
		
		double last_value;
		double value;
		double temp_value;
		sigc::connection inert_tmr;
		int inert_time;

		virtual bool on_inert_timer_tick();
		
		ADD_PROPERTY( prop_type, InertType )	/*!< свойство: функция инерционного движения*/
		ADD_PROPERTY( prop_total_time, int )	/*!< свойство: время инерционного движения (переходного процесса)*/
		ADD_PROPERTY( prop_pow, double )		/*!< свойство: коэфициент степени функции инерционного движения (переходного процесса) (больше 1)*/

	private:
};

namespace Glib
{
	template <>
	class Value<UInertia::InertType> : public Value_Enum<UInertia::InertType>
	{
	public:
		static GType value_type() G_GNUC_CONST;
	};
} // namespace Glib


#endif
