#include <iostream>
#include <fstream>
#include <errno.h>
#include <iomanip>
#include <gdkmm.h>
#include <types.h>
//#include <MessageType.h>
#include "UJournal.h"
#include "USignals.h"
#include "ConfirmSignal.h"
#include "SBlinker.h"
#include "ports.h"
// -------------------------------------------------------------------------
// using namespace std;
using namespace std;
using namespace UniWidgetsTypes;
using namespace UMessages;
using Glib::ustring;
// -------------------------------------------------------------------------
/*
struct msgItem
{
	ObjectId node_id;
	Glib::ustring msg;
	int character;
};
*/
// -------------------------------------------------------------------------
#define UJOURNAL_INIT_PROPERTIES() \
	sensor_prop_newAPS(this,"newAPS",get_connector()) \
	,sensor_prop_haveAPS(this,"haveAPS",get_connector()) \
	,prop_enableHisory(*this, "enable-history", false) \
	,prop_history_code_page(*this, "history-code-page", "CP1251" ) \
	,prop_dir(*this, "history-dir", "/tmp/" ) \
	,property_max_items         (*this, "max-items",  0) \
	,max_life_time              (*this, "max-life-time",  0) \
	,print_info_message         (*this, "print-info-message",  true) \
	,property_dbserver_on       (*this, "dbserver",  false) \
	,property_scroll_messages   (*this, "auto-scroll-messages",  true) \
	,property_pic_title          (*this, "pic-title",      "Pic") \
	,property_time_title        (*this, "time-title",     "Time") \
	,property_text_title        (*this, "text-title",     "Text") \
	,property_confirm_title     (*this, "confirm-title",  "Confirm") \
	,property_pic_width         (*this, "pic-width",      32 ) \
	,property_time_width        (*this, "time-width",     74 ) \
	,property_text_width        (*this, "text-width",     500 ) \
	,property_confirm_width     (*this, "confirm-width",  74 ) \
	,property_info_pic          (*this, "info-pic",       "info.svg") \
	,property_warn_pic          (*this, "warn-pic",       "warning.svg") \
	,property_alarm_pic         (*this, "alarm-pic",      "alarm.svg") \
	,property_attention_pic     (*this, "attention-pic",  "attention.svg") \
	,property_confirm1_pic      (*this, "confirm1-pic",   "confirm1.svg") \
	,property_confirm2_pic      (*this, "confirm2-pic",   "confirm2.svg") \
	,property_confirmed_pic     (*this, "confirmed-pic",  "confirmed.svg") \
	,property_msgFont           (*this, "msg-font",       "Liberation Sans 14") \
	,property_info_color        (*this, "info-color",     Gdk::Color("white")  ) \
	,property_warn_color        (*this, "warn-color",     Gdk::Color("yellow") ) \
	,property_alarm_color       (*this, "alarm-color",    Gdk::Color("red") ) \
	,property_attention_color   (*this, "attention-color",Gdk::Color("yellow") ) \
	,property_bg_first_color    (*this, "bg-first-color",Gdk::Color("#5a5a5a") ) \
	,property_bg_second_color   (*this, "bg-second-color",Gdk::Color("#373737") ) \
	,property_blinkNewMsg       (*this, "blink-new-msg",  true ) \
	,property_blinkNewMsgCount  (*this, "blink-new-msg-count",  10 ) \
	,property_blinkNewMsgTime   (*this, "blink-new-msg-time",  500 ) \
	,prop_enableGroups(*this, "enable-group-of-msg", true) \
	,prop_groupOfMsg(*this, "group-of-msg", "") \
	,blinkState(false) \

// -------------------------------------------------------------------------
void UJournal::ctor()
{

	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback<UJournal>;

	scrolled_window_.add(tree_view_);
	scrolled_window_.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

//	add(scrolled_window_);

	butHistory = manage(new class Gtk::CheckButton("Запись журнала в файл"));
	butHistory->signal_clicked().connect(sigc::mem_fun(*this, &UJournal::butHistory_clicked)) ;
	butHistory->set_active(prop_enableHisory);
	butHistory_clicked();
	butHistory->show();

	Gtk::VBox *vbox = manage(new Gtk::VBox());
	vbox->pack_start(*butHistory,Gtk::PACK_SHRINK);
	vbox->pack_start(scrolled_window_);
	add(*vbox);

	tree_model_ref_ = Gtk::ListStore::create(columns_);
	tree_view_.set_model(tree_model_ref_);
	tree_view_.get_selection()->set_mode(Gtk::SELECTION_NONE);

	{
		Gtk::CellRendererPixbuf* pRenderer = Gtk::manage( new Gtk::CellRendererPixbuf() );
		tree_view_.append_column("Pic", *pRenderer);
		Gtk::TreeViewColumn* pColumn = tree_view_.get_column(0);
		pColumn->add_attribute(pRenderer->property_pixbuf(), columns_.icon);
// 		pColumn->add_attribute(pRenderer->property_cell_background_gdk(),columns_.bgcolor);
		pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_FIXED);
		pColumn->set_cell_data_func(*pRenderer, sigc::mem_fun(*this, &UJournal::on_set_pix_value) );
	}

	{
		Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
		tree_view_.append_column("Time", *pRenderer);
		Gtk::TreeViewColumn* pColumn = tree_view_.get_column(1);
		pColumn->add_attribute(pRenderer->property_text(), columns_.time_string);
		pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
		pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
		pColumn->add_attribute(pRenderer->property_font(),columns_.msg_font);
		pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_FIXED);
	}

	//Text Message
	{
		Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
		pRenderer->property_wrap_mode() = Pango::WRAP_WORD;		
		pRenderer->property_wrap_width() = property_text_width;
		tree_view_.append_column("Text", *pRenderer);
		Gtk::TreeViewColumn* pColumn = tree_view_.get_column(2);
		pColumn->add_attribute(pRenderer->property_text(), columns_.text_message);
		pColumn->add_attribute(pRenderer->property_background_gdk(), columns_.bgcolor);
		pColumn->add_attribute(pRenderer->property_foreground_gdk(), columns_.fgcolor);
		pColumn->add_attribute(pRenderer->property_font(),columns_.msg_font);
		pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
		pColumn->set_expand(true);
	// 	pRenderer->property_ellipsize() = Pango::ELLIPSIZE_END;
	// 	pRenderer->property_ellipsize_set() = true;
	}

	//Confirm Time
	{
		Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
		tree_view_.append_column("Confirm", *pRenderer);
		Gtk::TreeViewColumn* pColumn = tree_view_.get_column(3);
		pColumn->add_attribute(pRenderer->property_text(), columns_.confirm_time);
		pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
		pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
		pColumn->add_attribute(pRenderer->property_font(),columns_.msg_font);
		pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_FIXED);
	}

	on_pic_width_changed();
	on_time_width_changed();
	on_text_width_changed();
	on_confirm_width_changed();
	on_info_pic_changed();
	on_warn_pic_changed();
	on_alarm_pic_changed();
	on_attention_pic_changed();
	on_confirm1_pic_changed();
	on_confirm2_pic_changed();
	on_confirmed_pic_changed();

	connect_property_changed("enable-history", sigc::mem_fun(*this, &UJournal::on_history_enable_changed) );
	connect_property_changed("history-dir", sigc::mem_fun(*this, &UJournal::on_history_dir_changed) );
	connect_property_changed("pic-title", sigc::mem_fun(*this, &UJournal::on_pic_title_changed) );
	connect_property_changed("time-title", sigc::mem_fun(*this, &UJournal::on_time_title_changed) );
	connect_property_changed("text-title", sigc::mem_fun(*this, &UJournal::on_text_title_changed) );
	connect_property_changed("confirm-title", sigc::mem_fun(*this, &UJournal::on_confirm_title_changed) );
	connect_property_changed("pic-width", sigc::mem_fun(*this, &UJournal::on_pic_width_changed) );
	connect_property_changed("text-width", sigc::mem_fun(*this, &UJournal::on_text_width_changed));
	connect_property_changed("time-width", sigc::mem_fun(*this, &UJournal::on_time_width_changed) );
	connect_property_changed("confirm-width", sigc::mem_fun(*this, &UJournal::on_confirm_width_changed) );
	connect_property_changed("info-pic", sigc::mem_fun(*this, &UJournal::on_info_pic_changed) );
	connect_property_changed("warn-pic", sigc::mem_fun(*this, &UJournal::on_warn_pic_changed) );
	connect_property_changed("alarm-pic", sigc::mem_fun(*this, &UJournal::on_alarm_pic_changed) );
	connect_property_changed("attention-pic", sigc::mem_fun(*this, &UJournal::on_attention_pic_changed) );
	connect_property_changed("confirm1-pic", sigc::mem_fun(*this, &UJournal::on_confirm1_pic_changed) );
	connect_property_changed("confirm2-pic", sigc::mem_fun(*this, &UJournal::on_confirm2_pic_changed) );
	connect_property_changed("confirmed-pic", sigc::mem_fun(*this, &UJournal::on_confirmed_pic_changed) );
	connect_property_changed("msg-font", sigc::mem_fun(*this, &UJournal::on_msg_font_changed) );
	connect_property_changed("blink-new-msg-time", sigc::mem_fun(*this, &UJournal::on_blink_new_msg_time_changed) );
	connect_property_changed("group-of-msg", sigc::mem_fun(*this, &UJournal::on_group_of_msg_changed) );
	
	if (get_property_disconnect_effect() == 2) {
		scrolled_window_.set_sensitive(false);
		tree_view_.set_sensitive(false);
	}

	kscroll.set_adjustment(scrolled_window_.get_vadjustment());
	kscroll.set_motion_event(&tree_view_);

	show_all_children();
}
// -------------------------------------------------------------------------
UJournal::UJournal() :
	Glib::ObjectBase("ujournal")
	,UJOURNAL_INIT_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UJournal::UJournal(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,UJOURNAL_INIT_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UJournal::~UJournal()
{
	delete butHistory;
}
//-----------------------------------------------------------------------------------------------------
/*std::string UJournal::get_time(time_t sec)
{
	stringstream sstr;
	tm *tms = localtime(&sec);
	sstr << setfill('0');
	sstr << setw(2) << tms->tm_hour << ":"
		<< setw(2) << tms->tm_min << ":"
		<< setw(2) << tms->tm_sec << endl;
}*/
//-----------------------------------------------------------------------------------------------------
void UJournal::on_realize()
{
	UEventBox::on_realize();
	on_history_enable_changed();
	on_msg_font_changed();
	on_blink_new_msg_time_changed();
}
// -------------------------------------------------------------------------
void
UJournal::enable_cleaner()
{
  cleaner_connection_.disconnect();
  /* Далее вызывается очистка истории сообщений. Из-за задержек Glib::signal_timeout(т.к. он запущен в main_loop и
    при возникновении других событий они обрабатываются,а в таймер вносится задержка, которая может быть
    существенной) делаем вызов в четверть времени жизни сообщений - это на случай если нет новых сообщений уже
    больше max_life_time. И при каждом добавлении пытаемся удалить старые сообщения т.к. приходим не по всему
    списку,а только по тем сообщениям которые нужно удалить следовательно удаляются сообщения небольшими порциями */
  cleaner_connection_ = Glib::signal_timeout().connect_seconds(sigc::mem_fun(*this, &UJournal::removeOldEntries), int(max_life_time/4) );
}
// -------------------------------------------------------------------------
bool
UJournal::removeOldEntries()
{
  typedef Gtk::TreeModel::Children Children;
  Children children = tree_model_ref_->children();

  Children::iterator it = children.begin();
  while (it != children.end()) {
    Gtk::TreeModel::Row row = *it;
    const time_t life_time = difftime(time(NULL), row[columns_.time]);
    if ( life_time >= max_life_time )
      it = tree_model_ref_->erase(it);
    else
      break;//if this message is not older than max_life_time then next messages are not older too!
            // Does,t need checking next messages!
  }
  //if children.empty() == TRUE then return false for disconnecting signal timeout
  return !children.empty();
}
// -------------------------------------------------------------------------
void
UJournal::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
	return;

	UEventBox::set_connector(connector);

	get_connector()->signals().connect_on_any_message_full(sigc::mem_fun(this, &UJournal::recieve_message));
	
	sensor_prop_newAPS.set_connector(connector);
	sensor_prop_haveAPS.set_connector(connector);
}
// -------------------------------------------------------------------------
void UJournal::on_connect() throw()
{
  UEventBox::on_connect();

  typedef Gtk::TreeModel::Children Children;
//  Children children = tree_model_ref_->children();
//  for(Children::iterator it = children.begin(); it!=children.end();
//      it = tree_model_ref_->erase(it));

  ConfirmSignal& cs = get_connector()->signal_confirm();

  //connect and show messages waits confirm
  for (ConfirmSignal::const_iterator it = cs.begin(); it != cs.end(); ++it ) {
    UMessages::Message message =
      get_connector()->signals().get_message(*it);
    if( !message.valid() )
      continue;
    process_message(message);
  }

//	get_connector()->signals().init_all_message();
	std::list<UMessages::Message> msg_list = get_connector()->signals().get_all_messages_list();
	for (std::list<UMessages::Message>::const_iterator it = msg_list.begin(); it != msg_list.end(); it++ )
	{
		if( !(*it).valid() )
			continue;
		process_message(*it);
	}

  if (get_property_disconnect_effect() == 2) {
    scrolled_window_.set_sensitive();
    tree_view_.set_sensitive();
  }
}
// -------------------------------------------------------------------------
void
UJournal::on_disconnect() throw()
{
	UEventBox::on_disconnect();
	if (get_property_disconnect_effect() == 2) {
		scrolled_window_.set_sensitive(false);
		tree_view_.set_sensitive(false);
	}
}
// -------------------------------------------------------------------------
//TODO: we must have signal with Message
void
UJournal::recieve_message(UMessages::MessageId id, int wtype, time_t sec, Glib::ustring msg)
{
//	cout<<"UJournal::recieve_message() id="<<id._id<<" wtype="<<wtype<<" msg="<<msg<<endl;
	UMessages::Message message = get_connector()->signals().get_message(id);

	if(message.valid())
		process_message(message);
	else
	{
		USignals::ValueMapItem vitem = USignals::ValueMapItem(id._id, id._value);
		vitem._is_message = true;
		vitem._text.push_back(msg);
		vitem._mtype = wtype;
		vitem._last_time = sec;
		process_message(UMessages::Message(vitem,id));
	}
}
// -------------------------------------------------------------------------
bool UJournal::on_search_noconfiredID(const Gtk::TreeIter& it,
				    const Gtk::TreeRow& row,
				    const UMessages::Message& message)
{
	const Glib::ustring ctm = (*it)[columns_.confirm_time];
	if ( ctm.empty() && (*it)[columns_.wtype] != 0 && (*it)[columns_.wtype] != 3 && it != row ) {
		has_any_not_confirmed_id = true;
		const MessageId row_id = (*it)[columns_.id];
		if(row_id == message.getMessageId())
			return true;
	}
	return false;
}
// -------------------------------------------------------------------------
void
UJournal::process_message(const UMessages::Message& message)
{
	bool show_new_msg = false;
	if(prop_enableGroups && !msg_groups.empty())
	{
		std::list<Glib::ustring> msg_group_list = explodestr(message.getMessageId()._msg_groups,',');
		for(std::list<Glib::ustring>::iterator group_it = msg_group_list.begin(); group_it != msg_group_list.end(); ++group_it)
		{
			if(msg_groups.find(*group_it)!=msg_groups.end())
			{
				show_new_msg = true;
				break;
			}
		}
	}
	else
		show_new_msg = true;
	
	if(!show_new_msg)
		return;
	
	std::list<Glib::ustring> msglist = message.getTextList();
	for(std::list<Glib::ustring>::iterator text_it = msglist.begin();text_it != msglist.end();++text_it)
	{
		Gtk::TreeRow row = *(tree_model_ref_->append());
		
		ustring dtime, ttime;
		get_time_str(dtime, ttime, message.getLastTime());
		row[columns_.time] = message.getLastTime();
		row[columns_.time_string] = ttime;
		row[columns_.id] = message.getMessageId();
		row[columns_.text_message] = *text_it;
		row[columns_.wtype] = message.getMessageType();
		row[columns_.msg_font] = get_property_msgFont();
		int num = int(tree_model_ref_->children().size());
		if(num%2)
			row[columns_.bgcolor] = get_property_bg_first_color();
		else
			row[columns_.bgcolor] = get_property_bg_second_color();
		
		Glib::ustring out_string = ttime + " " + *text_it;
		
		if (message.getMessageType() == msgINFO) {
			//row[columns_.bgcolor] = Gdk::Color("#282828");
			row[columns_.fgcolor] = property_info_color.get_value();
			iconPixbufMap[IconKey(row,columns_)] = refPixInfo;
			
			/* temporary solution */
			out_string = Glib::ustring("(И)") + out_string;
			/*Printing message*/
			if(print_info_message)
				print_.emit(out_string);
		} else if (message.getMessageType() == msgATTENTION){
			//row[columns_.bgcolor] = Gdk::Color("#282828");
			row[columns_.fgcolor] = property_attention_color.get_value();
			iconPixbufMap[IconKey(row,columns_)] = refPixAttention;
			
			/* temporary solution */
			out_string = Glib::ustring("(В)") + out_string;
		}
		else {
			
			if (message.getMessageType() == msgALARM ) {
				//row[columns_.bgcolor] = Gdk::Color("#282828");
				row[columns_.fgcolor] = property_alarm_color.get_value();
				iconPixbufMap[IconKey(row,columns_)] = refPixAlarm;
				
				/* temporary solution */
				out_string = Glib::ustring("(А)") + out_string;
			} else if (message.getMessageType() == msgWARNING) {
				//row[columns_.bgcolor] = Gdk::Color("#282828");
				row[columns_.fgcolor] = property_warn_color.get_value();
				iconPixbufMap[IconKey(row,columns_)] = refPixWarn;
				
				/* temporary solution */
				out_string = Glib::ustring("(П)") + out_string;
			}

			sensor_prop_newAPS.save_value(true);
			sensor_prop_haveAPS.save_value(2);
			
			/*Printing message*/
			print_.emit(out_string);
			//Search if we have same messages are waiting confirm.
			//If string with confirm time empty, we decide messages
			//wasn't confirmed
			has_any_not_confirmed_id = false;
			Gtk::TreeIter it = std::find_if(tree_model_ref_->children().begin(),
											tree_model_ref_->children().end(),
											sigc::bind(sigc::mem_fun(this,&UJournal::on_search_noconfiredID),row,message));
			if (it == tree_model_ref_->children().end()) {
				if (!has_any_not_confirmed_id)
					set_pointer(message.getMessageId());
				connect_confirm(message.getMessageId());
			}
		}
		
		if(property_blinkNewMsg)
		{
			row[columns_.blinkState] = blinkState;
			row[columns_.blinkCount] = 1;
			
			if(blinkState)
			{
				Gdk::Color bg,fg;
				bg = row[columns_.fgcolor];
				fg = row[columns_.bgcolor];
				row[columns_.fgcolor] = fg;
				row[columns_.bgcolor] = bg;
			}
			
			if(property_blinkNewMsgTime.get_value()>0 && !blink_msg_connection_.connected())
			{
 				blink_msg_connection_= Glib::signal_timeout().connect(sigc::mem_fun(*this, &UJournal::on_blink_msg_timer),property_blinkNewMsgTime.get_value());
			}
		}
		else
		{
			row[columns_.blinkState] = false;
			row[columns_.blinkCount] = 0;
		}

		if ( max_life_time > 0 )
		{
			removeOldEntries();
			if( cleaner_connection_.empty() )
				enable_cleaner();
		}
		if ( property_max_items > 0 )
			while( long(tree_model_ref_->children().size()) > property_max_items ) removeFirst();
			
			if(get_property_scroll_messages())
			{
				/* По первоначальной задумке если журнал перемотан пользователем в середину(например)
				 *		  то при поступлении нового сообщения журнал не должен был перематываться автоматически.
				 *		  Это работает, но когда приходит много сообщений в один момент не успевает отрабатывать
				 *		  сообщения и не перематывает. Поэтому сделал ручной checkbox на экране и добавил это
				 *		  свойство.
				 */
				// 		Gtk::TreePath path;
				// 		Gtk::TreePath end_path;
				// 		Gtk::TreePath new_path(row);
				// 		bool rv = tree_view_.get_visible_range(path, end_path);
				// 		
				// 		if ( rv )
				// 		{
				// 			Gtk::TreeModel::iterator it;
				// 			it = std::find(tree_model_ref_->get_iter(path), tree_model_ref_->get_iter(end_path), --tree_model_ref_->get_iter(new_path));
				// 			if( it == --tree_model_ref_->get_iter(new_path) )
				// 			{
				// 				tree_view_.scroll_to_row(new_path);
				// 				this->queue_draw();
				// 				cout<<"In visible"<<endl;
				// 			}
				// 			else
				// 				cout<<"Not in visible"<<endl;
				// 		}else
				Gtk::TreePath new_path(row);
				tree_view_.scroll_to_row(new_path);
			}
			
			if(butHistory->get_active() && prop_enableHisory)
			{
				if(history_file_fullname.empty())
					on_history_dir_changed();
				fstream fin( history_file_fullname.c_str(), ios::in);
				if(!fin)
				{
					cout<< get_name() << " can't open file="<<history_file_fullname<<" for read! generate new file..."<< endl;
					on_history_dir_changed();
				}
				fstream fout( history_file_fullname.c_str(), ios::out | ios::app );
				if(!fout)
				{
					cout<< get_name() << " NE SMOG OTKRIT` FAYIL " << history_file_fullname << endl;
					return;
				}
				string s = timeToString(message.getLastTime(),":")+"\t"+*text_it+"\n";
				s = Glib::convert(s,get_prop_history_code_page(),"UTF8");
				fout << s;
				fout.close();
			}
	}
}
// -------------------------------------------------------------------------
bool
UJournal::removeFirst()
{
       Gtk::TreeModel::Children children = tree_model_ref_->children();
       Gtk::TreeModel::Children::iterator it = children.begin();
       tree_model_ref_->erase(it);

       return !children.empty();
}
// -------------------------------------------------------------------------
bool UJournal::on_search_noconfired(const Gtk::TreeIter& it)
{
	const Glib::ustring ctm = (*it)[columns_.confirm_time];
	if ( ctm.empty() && (*it)[columns_.wtype] != 0 && (*it)[columns_.wtype] != 3 )
	{
		has_any_not_confirmed_aps = true;
		return true;
	}
		
		has_any_not_confirmed_aps = false;
		return false;
}
// -------------------------------------------------------------------------
void UJournal::on_foreach(const Gtk::TreeIter& it,
			  const MessageId& id,
			  const time_t& sec)
{
	const Glib::ustring ctm = (*it)[columns_.confirm_time];
	if ( ctm.empty() && MessageId((*it)[columns_.id]) == id)
	{
		ustring dtime, ttime;
		time_t delay = sec - (*it)[columns_.time];
		get_gmtime_str(dtime,ttime,delay);
		(*it)[columns_.confirm_time] = ttime;
		iconPixbufMap[IconKey(*it,columns_)] = refPixConfirmed;
		
		(*it)[columns_.blinkCount] = 0;
		if(property_blinkNewMsg)
		{
			if((*it)[columns_.blinkState])
			{
				Gdk::Color bg,fg;
				bg = (*it)[columns_.fgcolor];
				fg = (*it)[columns_.bgcolor];
				(*it)[columns_.fgcolor] = fg;
				(*it)[columns_.bgcolor] = bg;
				
				(*it)[columns_.blinkState] = false;
			}
		}
	}
}
// ----------------------------------------------------------------------------
void UJournal::confirm(MessageId id, time_t sec)
{
	blink_connection_.disconnect();

	sensor_prop_newAPS.save_value(false);
	
	std::for_each(tree_model_ref_->children().begin(),
		      tree_model_ref_->children().end(),
		      sigc::bind(sigc::mem_fun(this,&UJournal::on_foreach),id,sec));
	/* Here we looking for next entry need confirm */
	Gtk::TreeIter next = std::find_if(tree_model_ref_->children().begin(),
						tree_model_ref_->children().end(),
						sigc::mem_fun(this,&UJournal::on_search_noconfired));

	if ( next != tree_model_ref_->children().end() ) {
		MessageId m_id = (*next)[columns_.id];
		set_pointer( m_id );
	}

	if(has_any_not_confirmed_aps)
		sensor_prop_haveAPS.save_value(true);
	else
		sensor_prop_haveAPS.save_value(false);
}
// -------------------------------------------------------------------------
bool
UJournal::on_blink_msg_timer()
{
	if(!property_blinkNewMsg)
		return false;
	
	blinkState = !blinkState;

//	cout<<get_name()<<"::on_blink_msg_timer()"<<endl;

//	//один из вариантов проверки на мигание
// 	std::for_each(tree_model_ref_->children().begin(), tree_model_ref_->children().end(),sigc::mem_fun(this,&UJournal::on_foreach_blink));
// 	Gtk::TreeIter continue_blink = std::find_if(tree_model_ref_->children().begin(),tree_model_ref_->children().end(),sigc::mem_fun(this,&UJournal::on_search_blinked));
// 
// 	return continue_blink != tree_model_ref_->children().end();

	//другой вариант проверки на мигание
	bool continue_blink = false;
	for(Gtk::TreeIter it = tree_model_ref_->children().begin();it!=tree_model_ref_->children().end();++it)
	{
		const int count = (*it)[columns_.blinkCount];
		if(count > 0 && count <= get_property_blinkNewMsgCount()*2)
		{
			if( (*it)[columns_.blinkState] != blinkState )
			{
				Gdk::Color bg,fg;
				bg = (*it)[columns_.fgcolor];
				fg = (*it)[columns_.bgcolor];
				(*it)[columns_.fgcolor] = fg;
				(*it)[columns_.bgcolor] = bg;
				
				(*it)[columns_.blinkState] = blinkState;
				(*it)[columns_.blinkCount] = count+1;
			}
			
			if( count > get_property_blinkNewMsgCount()*2 )
			{
				(*it)[columns_.blinkCount] = 0;
				if(!(*it)[columns_.blinkState])
					continue_blink = continue_blink|false;
				else
					continue_blink = true;
			}
			else
				continue_blink = true;
		}
		else if((*it)[columns_.blinkState])
		{
			Gdk::Color bg,fg;
			bg = (*it)[columns_.fgcolor];
			fg = (*it)[columns_.bgcolor];
			(*it)[columns_.fgcolor] = fg;
			(*it)[columns_.bgcolor] = bg;

			(*it)[columns_.blinkState] = false;
			
			continue_blink = continue_blink|false;
		}
	}
	if(!continue_blink)
		blink_msg_connection_.disconnect();
	
	return continue_blink;
}
// -------------------------------------------------------------------------
void UJournal::on_foreach_blink(const Gtk::TreeIter& it)
{
	const int count = (*it)[columns_.blinkCount];
	if(count > 0 && count <= get_property_blinkNewMsgCount()*2)
	{
		Gdk::Color bg,fg;
		
//		cout<<get_name()<<"::on_foreach_blink() it="<<it<<endl;
		
		bg = (*it)[columns_.fgcolor];
		fg = (*it)[columns_.bgcolor];
		
		(*it)[columns_.fgcolor] = fg;
		(*it)[columns_.bgcolor] = bg;
		
		(*it)[columns_.blinkCount] = count+1;
		
		if(count > get_property_blinkNewMsgCount()*2)
			(*it)[columns_.blinkCount] = 0;
	}
}
// -------------------------------------------------------------------------
bool UJournal::on_search_blinked(const Gtk::TreeIter& it)
{
	const int count = (*it)[columns_.blinkCount];
	if ( count > 0 )
		return true;
	
	return false;
}
// -------------------------------------------------------------------------
void
UJournal::set_pointer( MessageId id )
{
	blink_connection_.disconnect();
	sigc::slot<void, bool, int> blinkslot=
	sigc::bind( sigc::mem_fun(this, &UJournal::blink), id );
	blink_connection_ = blinker.signal_blink[DEFAULT_BLINK_TIME].connect(blinkslot);
}
// -------------------------------------------------------------------------
void UJournal::on_set_pix_value(Gtk::CellRenderer* renderer, const Gtk::TreeModel::iterator& iter)
{
	Gtk::CellRendererPixbuf* pRenderer = dynamic_cast<Gtk::CellRendererPixbuf*>( renderer );
	assert(pRenderer != nullptr);

	pRenderer->property_pixbuf() = iconPixbufMap[IconKey(*iter,columns_)];
	//cerr << "UJournal::on_set_pix_value() " << temp_id << endl;
}
// -------------------------------------------------------------------------
void UJournal::on_blink_foreach(const Gtk::TreeIter& it,
                          const MessageId& id,
                          const Glib::RefPtr<Gdk::Pixbuf>& pix)
{
	const Glib::ustring ctm = (*it)[columns_.confirm_time];
	if ( MessageId((*it)[columns_.id]) == id && ctm.empty() )
	{
		iconPixbufMap[IconKey(*it,columns_)] = pix;
		Gdk::Rectangle  cell;
		Gtk::TreeViewColumn* pColumn = tree_view_.get_column(0);
		tree_view_.get_cell_area(tree_model_ref_->get_path(it), *pColumn, cell);
		//cerr << cell.get_x() << ":" << cell.get_y() << " " << cell.get_width() << ":" << cell.get_height() << endl;
#warning hack пока непонятно как расчитать высоту заголовка таблицы
		int height = tree_view_.get_headers_visible () ? ( cell.get_height() + cell.get_height() ) : cell.get_height();
		tree_view_.queue_draw_area (cell.get_x(), cell.get_y(), cell.get_width(), height);
	}
}
// -------------------------------------------------------------------------
void UJournal::blink(bool blink_state, int time, MessageId id)
{
	if(!is_mapped())
		return;

	Gtk::TreePath path;
	Gtk::TreePath end_path;
	bool rv = tree_view_.get_visible_range(path, end_path);

	if (rv == false)
		return;

	Glib::RefPtr<Gdk::Pixbuf> current_pixbuf =
		blink_state ? refPixConfirm1 : refPixConfirm2;

	std::for_each(tree_model_ref_->get_iter(path),
                  ++tree_model_ref_->get_iter(end_path),
                  sigc::bind(sigc::mem_fun(this,&UJournal::on_blink_foreach),id,current_pixbuf));
}
// -------------------------------------------------------------------------
void
UJournal::connect_confirm( MessageId id )
{
	sigc::slot<void, MessageId, time_t> confirm = sigc::mem_fun( this, &UJournal::confirm );
	get_connector()->signal_confirm().connect( confirm, id );
	/*Processing message for DBServer if property "property_dbserver_on" is on*/
	if( property_dbserver_on )
		get_connector()->set_process_signal_confirm( id );
}
// -------------------------------------------------------------------------
void
UJournal::on_pic_title_changed()
{
	tree_view_.get_column(0)->set_title( property_pic_title );
}
// -------------------------------------------------------------------------
void
UJournal::on_time_title_changed()
{
	tree_view_.get_column(1)->set_title( property_time_title );
}
// -------------------------------------------------------------------------
void
UJournal::on_text_title_changed()
{
	tree_view_.get_column(2)->set_title( property_text_title );
}
// -------------------------------------------------------------------------
void
UJournal::on_confirm_title_changed()
{
	tree_view_.get_column(3)->set_title( property_confirm_title );
}
// -------------------------------------------------------------------------
void
UJournal::on_pic_width_changed()
{
	tree_view_.get_column(0)->set_fixed_width( property_pic_width);
}
// -------------------------------------------------------------------------
void
UJournal::on_time_width_changed()
{
	tree_view_.get_column(1)->set_fixed_width( property_time_width);
}
// -------------------------------------------------------------------------
void
UJournal::on_text_width_changed()
{
	Gtk::CellRendererText* pCellRendererText = dynamic_cast<Gtk::CellRendererText*>(tree_view_.get_column_cell_renderer(2));
// 	pCellRendererText->property_wrap_mode() = Pango::WRAP_WORD;
	pCellRendererText->property_wrap_width() = property_text_width;
}
// -------------------------------------------------------------------------
void
UJournal::on_confirm_width_changed()
{
	tree_view_.get_column(3)->set_fixed_width( property_confirm_width);
}
// -------------------------------------------------------------------------
void
UJournal::try_load_pic( Glib::RefPtr<Gdk::Pixbuf>& refPix, std::string pic )
{
	try {
// 		refPix = Gdk::Pixbuf::create_from_file( pic );
		refPix = get_pixbuf_from_cache( pic, property_pic_width, property_pic_width);
	} catch ( ... ) {
		std::cerr << "(" << get_name() << "): Error while loading " << pic << std::endl;
	}
}
// -------------------------------------------------------------------------
void
UJournal::on_info_pic_changed()
{
	try_load_pic(refPixInfo, get_property_info_pic());
}
// -------------------------------------------------------------------------
void
UJournal::on_warn_pic_changed()
{
	try_load_pic(refPixWarn, get_property_warn_pic());
}
// -------------------------------------------------------------------------
void
UJournal::on_alarm_pic_changed()
{
	try_load_pic(refPixAlarm, get_property_alarm_pic());
}
// -------------------------------------------------------------------------
void
UJournal::on_attention_pic_changed()
{
	try_load_pic(refPixAttention, get_property_attention_pic());
}
// -------------------------------------------------------------------------
void
UJournal::on_confirm1_pic_changed()
{
	try_load_pic(refPixConfirm1, get_property_confirm1_pic());
}
// -------------------------------------------------------------------------
void
UJournal::on_confirm2_pic_changed()
{
	try_load_pic(refPixConfirm2, get_property_confirm2_pic());
}
// -------------------------------------------------------------------------
void
UJournal::on_confirmed_pic_changed()
{
	try_load_pic(refPixConfirmed, get_property_confirmed_pic());
}
// -------------------------------------------------------------------------
void
UJournal::on_blink_new_msg_time_changed()
{
	if(property_blinkNewMsg)
	{
		if (blink_msg_connection_.connected())
			blink_msg_connection_.disconnect();
		if(property_blinkNewMsgTime.get_value()>0)
			blink_msg_connection_= Glib::signal_timeout().connect(sigc::mem_fun(*this, &UJournal::on_blink_msg_timer),property_blinkNewMsgTime.get_value());
	}
}
// -------------------------------------------------------------------------
void UJournal::on_history_dir_changed()
{
	if(prop_enableHisory && connected_)
	{
		if(createDir(prop_dir.get_value()))
		{
			if(createDir(prop_dir.get_value()+"/"+dateToString(time(0),".")))
			{
				history_file_fullname	= prop_dir.get_value()+"/"+dateToString(time(0),".")+"/"+"Journal_"+dateToString(time(0),"")+"_"+timeToString(time(0),"")+".log";
			}
			else
				cout<< get_name() << "::on_history_dir_changed() can't create Dir="<<prop_dir.get_value()+"/"+dateToString(time(0),".")<< endl;
		}
		else
			cout<< get_name() << "::on_history_dir_changed() can't create Dir="<<prop_dir.get_value()<< endl;
	}
}
// -------------------------------------------------------------------------
void UJournal::on_history_enable_changed()
{
	butHistory->set_active(prop_enableHisory);
	if(prop_enableHisory)
		butHistory->show();
	else
		butHistory->hide();
	on_history_dir_changed();
}
// -------------------------------------------------------------------------
void UJournal::on_msg_font_changed()
{
	dynamic_cast<Gtk::CellRendererText*>(tree_view_.get_column_cell_renderer(1))->property_font() = property_msgFont;
	dynamic_cast<Gtk::CellRendererText*>(tree_view_.get_column_cell_renderer(2))->property_font() = property_msgFont;
	dynamic_cast<Gtk::CellRendererText*>(tree_view_.get_column_cell_renderer(3))->property_font() = property_msgFont;
}
// -------------------------------------------------------------------------
void UJournal::on_group_of_msg_changed()
{
	msg_groups.clear();
	std::list<Glib::ustring> msg_group_list = explodestr(get_prop_groupOfMsg(),',');
	for(std::list<Glib::ustring>::iterator group_it = msg_group_list.begin(); group_it != msg_group_list.end(); ++group_it)
		add_msg_group(*group_it);
}
// -------------------------------------------------------------------------
void UJournal::butHistory_clicked()
{
	on_history_dir_changed();
	cout<< get_name() << "::butHistory_clicked() Zapis` jurnala v fayil "<<history_file_fullname<<(butHistory->get_active()? " vklyuchina" : " viklyuchina")<< endl;
	if(butHistory->get_active())
	{
		fstream fin( history_file_fullname.c_str(), ios::in);
		if(!fin)
		{
			cout<< get_name() << " can't open file="<<history_file_fullname<<" for read! generate new file..."<< endl;
			on_history_dir_changed();
			fin.close();
		}
		fstream fout( history_file_fullname.c_str(), ios::out | ios::app );
		if(!fout)
		{
			cout<< get_name() << " NE SMOG OTKRIT` FAYIL " << history_file_fullname << endl;
			return;
		}
		fout.close();
	}
}
// -------------------------------------------------------------------------
string UJournal::dateToString(time_t tm, string brk)
{
	struct tm *tms = localtime(&tm);
	ostringstream date;
	date << std::setw(2) << std::setfill('0') << tms->tm_mday << brk;
	date << std::setw(2) << std::setfill('0') << tms->tm_mon+1 << brk;
	date << std::setw(4) << std::setfill('0') << tms->tm_year+1900;
	return date.str();
}
//--------------------------------------------------------------------------
string UJournal::timeToString(time_t tm, string brk)
{
	struct tm *tms = localtime(&tm);
	ostringstream time;
	time << std::setw(2) << std::setfill('0') << tms->tm_hour << brk;
	time << std::setw(2) << std::setfill('0') << tms->tm_min << brk;
	time << std::setw(2) << std::setfill('0') << tms->tm_sec;
	return time.str();
}
//--------------------------------------------------------------------------
// void UJournal::timeToInt(long time,int &hour, int &min, int &sec, time_t tm)
// {
// 	struct tm *tms = localtime(&tm);
// 	hour = tms->tm_hour + time / 3600;
// 	if(hour<0)
// 		hour = 24 + (hour % 24);
// 	else if(hour>=24)
// 		hour = hour % 24;
// 
// 	min = tms->tm_min + (time % 3600) / 60;
// 	if(min<0)
// 	{
// 		min = 60 + min;
// 		hour--;
// 		if(hour<0)
// 			hour = 24 + (hour % 24);
// 	}
// 	else if(min>=60)
// 	{
// 		min = min - 60;
// 		hour++;
// 		if(hour>=24)
// 			hour = hour % 24;
// 	}
// 
// 	sec = tms->tm_sec + (time % 3600) % 60;
// 	if(sec<0)
// 	{
// 		sec = 60 + sec;
// 		min--;
// 		if(min<0)
// 		{
// 			min = 60 + min;
// 			hour--;
// 			if(hour<0)
// 				hour = 24 + (hour % 24);
// 		}
// 	}
// 	if(sec>=60)
// 	{
// 		sec = sec - 60;
// 		min++;
// 		if(min>=60)
// 		{
// 			min = min - 60;
// 			hour++;
// 			if(hour>=24)
// 				hour = hour % 24;
// 		}
// 	}
// }
//--------------------------------------------------------------------------
bool UJournal::createDir( const std::string dir )
{
	if( mkdir(dir.c_str(), (S_IRWXO | S_IRWXU | S_IRWXG) ) == -1 )
	{
		if( errno != EEXIST )
		{
			cout<< " mkdir " << dir << " FAILED! " << strerror(errno) << endl;
			return false;
		}
	}

	return true;
}
//--------------------------------------------------------------------------
