#ifndef _UJOURNAL_H
#define _UJOURNAL_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <gtkmm.h>
#include <time.h>
#include "UEventBox.h"
#include "USignals.h"
#include "KineticScroll.h"
#include <global_macros.h>
#include <plugins.h>
// -------------------------------------------------------------------------
class msgItem
{
    UniWidgetsTypes::ObjectId node_id;
    Glib::ustring msg;
    int character;
};

/*! структура, описывающая строку в журнале(точнее колонки) */
class JournalColumnRecord : public Gtk::TreeModel::ColumnRecord
	{
	public:

		JournalColumnRecord()
		{
			add(icon);
			add(time_string);
			add(text_message);
			add(confirm_time);
			add(id);
			add(time);
			add(msg_font);
			add(bgcolor);
			add(fgcolor);
			add(wtype);
			add(blinkCount);
			add(blinkState);
		}

		Gtk::TreeModelColumn< Glib::RefPtr<Gdk::Pixbuf> > icon;
		Gtk::TreeModelColumn<Glib::ustring> time_string;
		Gtk::TreeModelColumn<Glib::ustring> text_message;
		Gtk::TreeModelColumn<Glib::ustring> confirm_time;
		Gtk::TreeModelColumn<UMessages::MessageId> id;
		Gtk::TreeModelColumn<time_t> time;
		Gtk::TreeModelColumn<Glib::ustring> msg_font;
		Gtk::TreeModelColumn<Gdk::Color> bgcolor;
		Gtk::TreeModelColumn<Gdk::Color> fgcolor;
		Gtk::TreeModelColumn<int> wtype;
		Gtk::TreeModelColumn<int> blinkCount;
		Gtk::TreeModelColumn<bool> blinkState;
	};
	struct IconKey
	{
		IconKey():
		id(),
		confirm_time("")
		{
		}
		IconKey(const UMessages::MessageId& id_, const Glib::ustring& time_):
		id(id_),
		confirm_time(time_)
		{
		}
		IconKey(const Gtk::TreeRow& row, const JournalColumnRecord& columns_):
		id(row[columns_.id]),
		confirm_time(row[columns_.confirm_time])
		{
		}
		bool operator==(const IconKey& x) const
		{
			return ( id == x.id && confirm_time == x.confirm_time );
		}
		UMessages::MessageId id;
		Glib::ustring confirm_time;
	};
namespace std
{
	template<>
	struct hash<IconKey>
	{
		size_t operator()(IconKey const& s) const
		{
			return std::hash<UMessages::MessageId>()(s.id);
		}
	};
}
// -------------------------------------------------------------------------
/*!
 * \brief Журнал сообщений.
 * \par
 * Журнал, в котором отображаются сообщения от датчиков. Для того чтобы сообщение
 * от датчика выводилось в журнале необходимо в настройках(файл configure.xml) задать
 * параметр mtype, который может принимать 4 значения:0 - сообщение информационное,
 * 1 - предупредительное, 2 - аварийное и 3 - внимание.
 * В отличие от журнала АПС при сбросе датчика и его квитироовании запись о сообщении не
 * удаляется из журнала, но чистка записей производится. Если задан параметр максимального
 * количества записей в журнале и/или максимальное время нахождения записи в журнале, то
 * записи подчищаются.
*/
class UJournal : public UEventBox
{
public:
	UJournal();
	explicit UJournal(GtkmmBaseType::BaseObjectType* gobject);
	virtual ~UJournal();

	typedef std::unordered_map<std::string,bool> MsgGroupNameMap;
	inline MsgGroupNameMap* get_msg_groups(){ return &msg_groups;};
	inline void add_msg_group(std::string group){ msg_groups[group] = true;};
	
	virtual void set_connector(const ConnectorRef& connector) throw();
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	/*! сигнал печати сообщений на принтере приходящих в журнал */
	sigc::signal< bool , Glib::ustring > print_;

	static std::string timeToString(time_t tm=time(0), std::string brk=""); /*!< Preobrazovanie vremeni v stroku HH:MM:SS */
	static std::string dateToString(time_t tm=time(0), std::string brk=""); /*!< Preobrazovanie dati v stroku YYYY/MM/DD */
	static bool createDir( const std::string dir );
//	void timeToInt(long time,int &hour, int &min, int &sec, time_t tm=time(0)); /*!< Preobrazovanie vremeni v chisla*/

protected:
	void on_set_pix_value( Gtk::CellRenderer* renderer, const Gtk::TreeModel::iterator& iter);
	/*! обработка сообщения, определение его типа и внесение его в журнал */
	virtual void process_message(const UMessages::Message& message);
	/*! активировать автоматическую очистку журнала */
	void enable_cleaner();
	/*! удалить записи старее max_life_time */
	bool removeOldEntries();
	/*! удалить первую запись журнала */
	bool removeFirst();
	/*! обработчик полученияя нового сообщения */
	void recieve_message(UMessages::MessageId id, int wtype, time_t sec, Glib::ustring msg);
	void process_confirm(UMessages::MessageId id, time_t);
	/*! обработчик квитирования */
	void confirm(UMessages::MessageId id, time_t sec);
	/*! мигание иконкой в первом столбце первым на очередь к квитированию сообщением */
	void blink(bool blink_state, int time, UMessages::MessageId id);
	/*! соединить сигнал квитирования с обработчиком */
	void connect_confirm( UMessages::MessageId id );
	/*! установить мигание данным сообщением */
	void set_pointer( UMessages::MessageId id );

	virtual void on_realize();
	sigc::connection cleaner_connection_;
	sigc::connection blink_connection_;

	typedef std::unordered_map<UniWidgetsTypes::ObjectId, msgItem> MessagesList;
	MessagesList msg_list_;

	MsgGroupNameMap msg_groups;
	
	bool blinkState;
	sigc::connection blink_msg_connection_;
	virtual bool on_blink_msg_timer();
	void on_foreach_blink(const Gtk::TreeIter& it);
	bool on_search_blinked(const Gtk::TreeIter& it);
	
	Gtk::TreeView tree_view_;				/*!< виджет отображающий модель (Gtk::TreeModel) данных и позволяющий пользователю взаимодействовать с ними */
	Glib::RefPtr<Gtk::ListStore> tree_model_ref_;		/*!< модель списка для использования с виджетом Gtk::TreeView */

	SensorProp sensor_prop_newAPS;
	SensorProp sensor_prop_haveAPS;
	
	/*Properties*/
	ADD_PROPERTY( prop_enableHisory, bool )					/*!< свойство: */
	ADD_PROPERTY( prop_history_code_page, Glib::ustring )	/*!< свойство: */
	ADD_PROPERTY( prop_dir, Glib::ustring )					/*!< свойство: */
	ADD_PROPERTY( property_max_items, double);				/*!< свойство: максимальное число сообщений в журнале */
	ADD_PROPERTY( max_life_time, double);					/*!< свойство: максимальное время нахождения записи в журнале */
	ADD_PROPERTY( print_info_message, bool);				/*!< свойство: печатать сообщения из журнала на принтере */
	ADD_PROPERTY( property_dbserver_on, bool);				/*!< свойство: посылка сообщения о квитировании в DBServer */
	ADD_PROPERTY( property_scroll_messages, bool )			/*!< свойство: Автоматическая перемотка сообщений*/

	ADD_PROPERTY( property_pic_title, Glib::ustring);		/*!< свойство: заголовок шапки для картинки в первой колонке, которая отображает состояние сообщения */
	ADD_PROPERTY( property_time_title, Glib::ustring);		/*!< свойство: заголовок шапки для поля времени поступления сообщения */
	ADD_PROPERTY( property_text_title, Glib::ustring);		/*!< свойство: заголовок шапки для поля текста сообщения */
	ADD_PROPERTY( property_confirm_title, Glib::ustring);	/*!< свойство: заголовок шапки для поля времени реакции на сообщение */

	ADD_PROPERTY( property_pic_width, int);					/*!< свойство: ширина поля картинки */
	ADD_PROPERTY( property_time_width, int);				/*!< свойство: ширина поля времени поступления */
	ADD_PROPERTY( property_text_width, int);				/*!< свойство: ширина поля текста сообщения */
	ADD_PROPERTY( property_confirm_width, int);				/*!< свойство: ширина поля времени реакции */

	ADD_PROPERTY( property_info_pic, Glib::ustring);		/*!< свойство: картинка для картинки информационного сообщения */
	ADD_PROPERTY( property_warn_pic, Glib::ustring);		/*!< свойство: картинка для картинки предупредительного сообщения */
	ADD_PROPERTY( property_alarm_pic, Glib::ustring);		/*!< свойство: картинка для картинки аварийного сообщения */
	ADD_PROPERTY( property_attention_pic, Glib::ustring);	/*!< свойство: картинка для картинки сообщения типа "внимание" */
	ADD_PROPERTY( property_confirm1_pic, Glib::ustring);	/*!< свойство: картинка для картинки сообщения требующего квитирования  */
	ADD_PROPERTY( property_confirm2_pic, Glib::ustring);	/*!< свойство: картинка для картинки сообщения требующего квитирования  */
	ADD_PROPERTY( property_confirmed_pic, Glib::ustring);	/*!< свойство: картинка для картинки заквитированного сообщения */

	ADD_PROPERTY( property_msgFont, Glib::ustring )			/*!< свойство: шрифт сообщений*/
	ADD_PROPERTY( property_info_color, Gdk::Color);			/*!< свойство: цвет информационного сообщения */
	ADD_PROPERTY( property_warn_color, Gdk::Color);			/*!< свойство: цвет предупредительного сообщения */
	ADD_PROPERTY( property_alarm_color, Gdk::Color);		/*!< свойство: цвет аварийного сообщения */
	ADD_PROPERTY( property_attention_color, Gdk::Color);	/*!< свойство: цвет сообщения типа "внимание" */
	ADD_PROPERTY(property_bg_first_color, Gdk::Color)		/*!< свойство: цвет фона нечетных сообщений */
	ADD_PROPERTY(property_bg_second_color, Gdk::Color)		/*!< свойство: цвет фона четных сообщений */

	ADD_PROPERTY( property_blinkNewMsg, bool )				/*!< свойство: мигать новыми сообщениями*/
	ADD_PROPERTY( property_blinkNewMsgCount, int )			/*!< свойство: количество миганий*/
	ADD_PROPERTY( property_blinkNewMsgTime, int )			/*!< свойство: период мигания*/
	
	Glib::RefPtr<Gdk::Pixbuf> refPixInfo;			/*!< картинка для картинки информационного сообщения */
	Glib::RefPtr<Gdk::Pixbuf> refPixWarn;			/*!< картинка для картинки предупредительного сообщения */
	Glib::RefPtr<Gdk::Pixbuf> refPixAlarm;			/*!< картинка для картинки аварийного сообщения */
	Glib::RefPtr<Gdk::Pixbuf> refPixAttention;		/*!< картинка для картинки сообщения типа "внимание" */
	Glib::RefPtr<Gdk::Pixbuf> refPixConfirm1;		/*!< картинка для картинки сообщения требующего квитирования  */
	Glib::RefPtr<Gdk::Pixbuf> refPixConfirm2;		/*!< картинка для картинки сообщения требующего квитирования  */
	Glib::RefPtr<Gdk::Pixbuf> refPixConfirmed;		/*!< картинка для картинки заквитированного сообщения */

	ADD_PROPERTY( prop_enableGroups, bool )				/*!< свойство: фильтровать сообщения по принадлежности к группам*/
	ADD_PROPERTY( prop_groupOfMsg, Glib::ustring)		/*!< свойство: группы отображаемых сообщений*/
	
	JournalColumnRecord columns_;				/*!< описание строки в журнале */
	std::unordered_map<IconKey, Glib::RefPtr<Gdk::Pixbuf> > iconPixbufMap;			/*!< map с картинками для мигания текущим квитируемым сообщением */

	Gtk::ScrolledWindow scrolled_window_;			/*!< главное прокручиваемое окно журнала */

	KineticScroll kscroll;					/*!< кинентическая прокрутка */

	std::string history_file_fullname;
	Gtk::CheckButton*butHistory;
	void butHistory_clicked();

private:
	void ctor();

	bool on_search_noconfiredID(const Gtk::TreeIter& it,
				    const Gtk::TreeRow& row,
				    const UMessages::Message& message);

	bool on_search_noconfired(const Gtk::TreeIter& it);

	void on_foreach(const Gtk::TreeIter& it,const UMessages::MessageId& id,const time_t& sec);

    void on_blink_foreach(const Gtk::TreeIter& it,
                          const UMessages::MessageId& id,
                          const Glib::RefPtr<Gdk::Pixbuf>& pix);
	bool has_any_not_confirmed_id;
	bool has_any_not_confirmed_aps;
	
	
	void on_history_enable_changed();
	void on_history_dir_changed();
	void on_pic_title_changed();
	void on_time_title_changed();
	void on_text_title_changed();
	void on_confirm_title_changed();
	void on_pic_width_changed();
	void on_time_width_changed();
	void on_text_width_changed();
	void on_confirm_width_changed();
	void on_info_pic_changed();
	void on_warn_pic_changed();
	void on_alarm_pic_changed();
	void on_attention_pic_changed();
	void on_confirm1_pic_changed();
	void on_confirm2_pic_changed();
	void on_confirmed_pic_changed();
	void on_msg_font_changed();
	void on_blink_new_msg_time_changed();
	void on_group_of_msg_changed();
	
	void try_load_pic( Glib::RefPtr<Gdk::Pixbuf>& refPix, std::string pic );

};
#endif
