#include "ULockNotebook.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void ULockNotebook::ctor()
{
	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback<ULockNotebook>;

	connect_property_changed("enable-lock", sigc::mem_fun(this,&ULockNotebook::on_property_enable_lock_changed));
}
// -------------------------------------------------------------------------
ULockNotebook::ULockNotebook() : 
	Glib::ObjectBase("ulocknotebook")
	,property_enable_lock(*this, "enable-lock", true)
{
	ctor();
}
// -------------------------------------------------------------------------
ULockNotebook::ULockNotebook(GtkNotebook* gobject) :
	BaseType(gobject)
	,property_enable_lock(*this, "enable-lock", true)
{
	ctor();
}
// -------------------------------------------------------------------------
ULockNotebook::~ULockNotebook()
{
}
// -------------------------------------------------------------------------
void ULockNotebook::add_lock(const Gtk::Widget& w)
{
	locks_list.push_back(&w);
	on_property_enable_lock_changed();
	/*нужен ли следующий вызов*/
	UDefaultFunctions<Gtk::Notebook>::add_lock(w);
}
// -------------------------------------------------------------------------
void ULockNotebook::unlock_current()
{
	locks_list.pop_front();
	on_property_enable_lock_changed();

	UDefaultFunctions<Gtk::Notebook>::unlock_current();
}
// -------------------------------------------------------------------------
void
ULockNotebook::on_switch_page(GtkNotebookPage* page, guint npage)
{
 	//TODO: this function can be better
	const bool locked = property_enable_lock && !locks_list.empty();

	if (!locked) {
		Gtk::Notebook::on_switch_page(page, npage);
		return;
	}
	const int current = get_current_page();
	const int locked_page = page_num(*locks_list.front());

	if (current == locked_page)
		return;

	if (locked_page != npage)
		return;

	Gtk::Notebook::on_switch_page(page, npage);
}
// -------------------------------------------------------------------------
bool ULockNotebook::on_button_press_event(GdkEventButton* event)
{
	const bool locked = property_enable_lock && !locks_list.empty();
	if(locked)
		return true;
	Gtk::Notebook::on_button_press_event(event);
}
// -------------------------------------------------------------------------
void
ULockNotebook::enable_lock(bool enable)
{
	property_enable_lock = enable;
}
// -------------------------------------------------------------------------
void
ULockNotebook::on_property_enable_lock_changed()
{
	if (property_enable_lock && !locks_list.empty())
	{
		const int page = page_num(*locks_list.front());
		Gtk::Notebook::set_current_page(page);
		return;
	}
}
// -------------------------------------------------------------------------
std::string ULockNotebook::get_info()
{
	std::ostringstream inf;
	inf << UVoid::get_info();
	inf << "	[variable] locks_list.size() = "<< locks_list.size() << std::endl;
	inf << "	[variable] locks_list.front() = "<< (!locks_list.empty() ? page_num(*locks_list.front()) : -1 ) << std::endl;
	return inf.str();
}
// -------------------------------------------------------------------------
