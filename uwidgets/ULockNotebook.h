#ifndef _ULOCKNOTEBOOK_H
#define _ULOCKNOTEBOOK_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <deque>
#include "UDefaultFunctions.h"
#include "plugins.h"
// -------------------------------------------------------------------------
/*!
 * \brief Класс блокнота с закладками.
 * \par
 * Класс предназначен для реализации виджета блокнота с блокирующимися вкладками.
 * Вкладки блокируются и автоматически переключаются при срабатывании АПС сигнала.
 * Блокированную вкладку нельзя переключить пока сигнал не заквитирован или
 * не выставлен блокировка автопереключения вкладок. Если срабатывает несколько
 * АПС сигналов, то экран блокируется первым пришедшим сигналом, а все последующие
 * сигналы(точнее виджеты соответствующие этим сигналам) помещаются в очередь
 * блокировки. Когда будет заквитирован первый сигнал он будет удален из очереди и
 * блокнот автоматически переключиться на следующий в очереди сигнал и так пока
 * не закончится очередь.
*/
class ULockNotebook : public UDefaultFunctions<Gtk::Notebook>
{
public:
	ULockNotebook();

	explicit ULockNotebook(GtkmmBaseType::BaseObjectType* gobject);
	~ULockNotebook();

	virtual std::string get_info();
// 	virtual bool set_current_page(int page);
	void lock_nbook(const Gtk::Widget *w);				/*!< не используется */

	virtual void add_lock(const Gtk::Widget& w);			/*!< добавить виджет в очередь блокировки */
	virtual void unlock_current();					/*!< разблокировать текущую вкладку и переключиться на следующую, если необходимо */

	void enable_lock(bool enable=true);				/*!< включить автопереключение вкладок */

protected:
	/*! обработчик переключения на заданную закладку.
	    \param page страница, на которую нужно переключиться.
	    \param page_num номер закладки, на которую нужно переключиться.
	*/
	virtual void on_switch_page(GtkNotebookPage* page, guint page_num);
	/*! обработчик надатия кнопки мыши */
	virtual bool on_button_press_event(GdkEventButton* event);

private:
	void ctor();
	sigc::connection lock_connection;
	Glib::Property<bool> property_enable_lock;

	typedef std::deque<const Gtk::Widget*> LockList;
	LockList locks_list;

	void on_property_enable_lock_changed();

};
#endif
