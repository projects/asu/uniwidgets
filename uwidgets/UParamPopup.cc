#include <UParamPopup.h>
#undef atoi
#include "include/ports.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define UNIXML_KOI8_TO_UTF_HACK(x) Glib::locale_to_utf8(x);
// -------------------------------------------------------------------------
CellRendererSensorValue::CellRendererSensorValue() :
	Glib::ObjectBase( typeid(CellRendererSensorValue) )
	,Gtk::CellRenderer()
	,property_value_( *this, "value", 0)
	,property_view_parameters_(*this, "view-parameters" )
{
}
// -------------------------------------------------------------------------
CellRendererSensorValue::~CellRendererSensorValue()
{
}
// -------------------------------------------------------------------------
void
CellRendererSensorValue::get_size_vfunc (Gtk::Widget& widget, const Gdk::Rectangle* cell_area,
		int* x_offset, int* y_offset, int* width, int* height) const
{
	int max_height = 0;
	int max_width = 0;
	const PPViewParameters& view_parameters = property_view_parameters();
	if ( view_parameters.view_type == 1 )
	{
		typedef PPViewParameters::MessagesMap::const_iterator iterator;
		for (iterator it = view_parameters.messages_map.begin();
			it != view_parameters.messages_map.end(); ++it) {

			Glib::RefPtr<Pango::Layout> layout_ptr =
				widget.create_pango_layout(it->second.text);
			Pango::Rectangle rect = layout_ptr->get_pixel_logical_extents();

			if ( rect.get_width() > max_width )
				max_width = rect.get_width();
			if ( rect.get_height() > max_height )
				max_height = rect.get_height();
			if ( it->second.image_ptr ) {
				if ( it->second.image_ptr->get_width() > max_width )
					max_width = it->second.image_ptr->get_width();
				if ( it->second.image_ptr->get_height() > max_height )
					max_height = it->second.image_ptr->get_height();
			}
		}
	}
	else {
		Glib::ustring str;
		usprintf(str, view_parameters.format, property_value_.get_value());

		Glib::RefPtr<Pango::Layout> layout_ptr = widget.create_pango_layout(str);
		Pango::Rectangle rect = layout_ptr->get_pixel_logical_extents();

		max_width = rect.get_width() + 10;
		max_height = rect.get_height();
	}

	*width = max_width;
	*height = max_height;
	return;
}
// -------------------------------------------------------------------------
void
CellRendererSensorValue::render_vfunc (const Glib::RefPtr<Gdk::Drawable>& window, Gtk::Widget& widget,
			const Gdk::Rectangle& background_area, const Gdk::Rectangle& cell_area,
			const Gdk::Rectangle& expose_area, Gtk::CellRendererState flags)
{
	Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();

	cr->rectangle(expose_area.get_x(),
		      expose_area.get_y(),
		      expose_area.get_width(),
		      expose_area.get_height());
	cr->clip();

	cr->set_source_rgb(1,0,0);

	const PPViewParameters& view_parameters = property_view_parameters();

	//TODO: check this, added after merging 
	int x, y;
	Glib::ustring str;
	usprintf(str, view_parameters.format, property_value_.get_value());

	if ( view_parameters.view_type == 1 )
	{
		/** TODO: check if something must be done with
		 * file format ( RGB/RGBA ).
		 */
		const float value = property_value_;
		PPViewParameters::MessagesMap::const_iterator it =
			view_parameters.messages_map.find(value);

		//draw background, plain color set by bg_color
		if( it == view_parameters.messages_map.end() ) {
			cr->set_source_rgb(0,0,0);
			cr->rectangle(background_area.get_x(), background_area.get_y(),
				      background_area.get_width(), background_area.get_height());
			cr->fill();
		}
		else {
			const Gdk::Color& bg_color = it->second.bg_color;
			cr->set_source_rgb(bg_color.get_red_p(),
					   bg_color.get_green_p(),
					   bg_color.get_blue_p());
			cr->rectangle(background_area.get_x(), background_area.get_y(),
				      background_area.get_width(), background_area.get_height());
			cr->fill();
		}

		//draw picture if any
		if( it != view_parameters.messages_map.end() ) {
			const Glib::RefPtr<Gdk::Pixbuf>& image_ptr = it->second.image_ptr;
			if (image_ptr) {
				const int image_x = cell_area.get_x() +
					(cell_area.get_width() - image_ptr->get_width())/2;
				const int image_y = cell_area.get_y() +
					(cell_area.get_height() - image_ptr->get_height())/2;
				Gdk::Cairo::set_source_pixbuf(cr, image_ptr, image_x, image_y);
				cr->paint();
			}
		}

		//draw text
		if( it == view_parameters.messages_map.end() ) { //value doesn't have entry in configure.xml
			cr->set_source_rgb(0,0,0);

			Glib::RefPtr<Pango::Layout> layout_ptr =
				widget.create_pango_layout("Error!");
			Pango::Rectangle rect = layout_ptr->get_pixel_logical_extents();
			const int text_x = cell_area.get_x() + (cell_area.get_width()  - rect.get_width()) /2;
			const int text_y = cell_area.get_y() + (cell_area.get_height() - rect.get_height())/2;
			cr->move_to(text_x, text_y);
			layout_ptr->add_to_cairo_context(cr);
			cr->fill();
		}
		else {
			const Gdk::Color color = it->second.color;
			cr->set_source_rgb( color.get_red_p(),
					    color.get_green_p(),
					    color.get_blue_p());

			Glib::RefPtr<Pango::Layout> layout_ptr =
				widget.create_pango_layout(it->second.text);
			Pango::Rectangle rect = layout_ptr->get_pixel_logical_extents();


			const int text_x = cell_area.get_x() + (cell_area.get_width()  - rect.get_width()) /2;
			const int text_y = cell_area.get_y() + (cell_area.get_height() - rect.get_height())/2;
			cr->move_to(text_x, text_y);
			layout_ptr->add_to_cairo_context(cr);
			cr->fill();
		}
	}
	else { // view_parameters.view_type != 1
		const Gdk::Color& bg_color = view_parameters.bg_color;
		cr->set_source_rgb( bg_color.get_red_p(),
				    bg_color.get_green_p(),
				    bg_color.get_blue_p());
		cr->rectangle(background_area.get_x(), background_area.get_y(),
			      background_area.get_width(), background_area.get_height());
		cr->fill();

		const Gdk::Color color = view_parameters.color;
		cr->set_source_rgb( color.get_red_p(),
				    color.get_green_p(),
				    color.get_blue_p());
		char buf[250];
		sprintf(buf, view_parameters.format.c_str(), property_value_.get_value());
		std::string str(buf);

		Glib::RefPtr<Pango::Layout> layout_ptr = widget.create_pango_layout(str);
		Pango::Rectangle rect = layout_ptr->get_pixel_logical_extents();

		const int text_x = cell_area.get_x() + (cell_area.get_width()  - rect.get_width()) /2;
		const int text_y = cell_area.get_y() + (cell_area.get_height() - rect.get_height())/2;
		cr->move_to(text_x, text_y);

		layout_ptr->add_to_cairo_context( cr);
		cr->fill();
	}
}
// -------------------------------------------------------------------------
#define UPARAMPOPUP_PROPERTIES_INIT() \
	property_attribut_(*this, "attribut", "ppfilter") \
	,property_value_(*this, "value", "1") \
	,property_window_width_(*this, "window-width", 240) \
	,property_window_height_( *this, "window-height", 320) \
// -------------------------------------------------------------------------
void
UParamPopup::ctor()
{
}
// -------------------------------------------------------------------------
UParamPopup::UParamPopup() : 
	Glib::ObjectBase("uparampopup")
	,UPARAMPOPUP_PROPERTIES_INIT()
	,popup_window(NULL)
{
	ctor();
}
// -------------------------------------------------------------------------
UParamPopup::UParamPopup(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,UPARAMPOPUP_PROPERTIES_INIT()
	,popup_window(NULL)
{
	ctor();
}
// -------------------------------------------------------------------------
UParamPopup::~UParamPopup()
{
}
// -------------------------------------------------------------------------
bool
UParamPopup::on_button_press_event( GdkEventButton* event )
{
	if ( popup_window != NULL )
		return true;

	popup_window = create_popup(); 
	popup_window->show_all();

	sigc::slot<bool, GdkEventFocus*> focus_out =
		sigc::hide(sigc::bind(sigc::mem_fun(this, &UParamPopup::on_focus_out),popup_window));
	popup_window->signal_focus_out_event().connect(focus_out);
	return true;
}
// -------------------------------------------------------------------------
bool
UParamPopup::on_focus_out(Gtk::Widget* w)
{
	delete popup_window;
	popup_window = NULL;
}
// -------------------------------------------------------------------------
PPViewParameters
UParamPopup::get_view_parameters_from_node(xmlNode* it)
{
	PPViewParameters view_parameters;
	view_parameters.precision =
		atoi(UniWidgetsTypes::xmlGetProp(it,"precision").c_str());

	//processing short entry
	std::string tmp = UniWidgetsTypes::xmlGetProp(it,"pptm");
	if (!tmp.empty()) { //processing as short entry
		view_parameters.view_type = 1;
		size_t spos = tmp.find("/"); //spos - separator position
		if(spos == string::npos) {
			view_parameters.messages_map[0].text = tmp;
			view_parameters.messages_map[0].color = Gdk::Color("white");
			view_parameters.messages_map[0].bg_color = Gdk::Color("black");
			view_parameters.messages_map[1].text = "";
			view_parameters.messages_map[1].color = Gdk::Color("white");
			view_parameters.messages_map[1].bg_color = Gdk::Color("black");
		}
		else {
			view_parameters.messages_map[0].text = tmp.substr(0, spos);
			view_parameters.messages_map[0].color = Gdk::Color("white");
			view_parameters.messages_map[0].bg_color = Gdk::Color("black");
			view_parameters.messages_map[1].text = tmp.substr(spos+1);
			view_parameters.messages_map[1].color = Gdk::Color("white");
			view_parameters.messages_map[1].bg_color = Gdk::Color("black");
		}
		return view_parameters;
	}

	// if not a short entry
	tmp = UniWidgetsTypes::xmlGetProp(it,"viewtype");
	if (tmp == "1") {
		view_parameters.view_type = 1;
		xmlNode* it2 = UniWidgetsTypes::xmlExtFindNode( it, 1, 2, "ParamPopupTextMessages");
		bool has_subsections = (it2 = it2->children);
		if ( it2 == NULL || !has_subsections ) {
			cerr << "(" << get_name() << ")"
			     << "Warning: viewtype set to 1, but attribut pptm not"
			     << "found or section ParamPopupTextMessages is empty "
			     << "for sensor " << UniWidgetsTypes::xmlGetProp(it,"id") << ". "
			     << "Using default." << endl;
			view_parameters.messages_map[0].text = "Off";
			view_parameters.messages_map[0].color = Gdk::Color("white");
			view_parameters.messages_map[0].bg_color = Gdk::Color("black");
			view_parameters.messages_map[1].text = "On";
			view_parameters.messages_map[1].color = Gdk::Color("white");
			view_parameters.messages_map[1].bg_color = Gdk::Color("black");
		}
		for (; it2; it2 = it2->next) {
			tmp = UniWidgetsTypes::xmlGetProp(it2,"value");
			long value = std::atol(tmp.c_str());
			view_parameters.messages_map[value].text = UNIXML_KOI8_TO_UTF_HACK( UniWidgetsTypes::xmlGetProp(it2,"text") );

			tmp = UniWidgetsTypes::xmlGetProp(it2,"color");
			if (tmp.empty())
				view_parameters.messages_map[value].color = Gdk::Color("white");
			else
				view_parameters.messages_map[value].color = Gdk::Color(tmp);

			tmp = UniWidgetsTypes::xmlGetProp(it2,"bg_color");
			if (tmp.empty())
				view_parameters.messages_map[value].bg_color = Gdk::Color("black");
			else
				view_parameters.messages_map[value].bg_color = Gdk::Color(tmp);

			tmp = UniWidgetsTypes::xmlGetProp(it2,"image");
			try {
				view_parameters.messages_map[value].image_ptr =
					Gdk::Pixbuf::create_from_file(tmp);
			}
			catch ( const Glib::Error& er ) {
			cerr << "(" << get_name() << "):"
				      << er.what() << endl;
			}

			tmp = UniWidgetsTypes::xmlGetProp(it2,"font");
			if ( tmp.empty() )
				view_parameters.messages_map[value].font = "Liberation Sans 16";
			else
				view_parameters.messages_map[value].font = tmp;
		}

	}
	else { // tmp != 1
		view_parameters.view_type = 0;

		tmp = UniWidgetsTypes::xmlGetProp(it,"fmt");
		if ( tmp.empty() )
			view_parameters.format = "%f";
		else
			view_parameters.format = tmp;

		tmp = UniWidgetsTypes::xmlGetProp(it,"color");
		if (tmp.empty())
			view_parameters.color = Gdk::Color("white");
		else
			view_parameters.color = Gdk::Color(tmp);

		tmp = UniWidgetsTypes::xmlGetProp(it,"bg_color");
		if (tmp.empty())
			view_parameters.bg_color = Gdk::Color("black");
		else
			view_parameters.bg_color = Gdk::Color(tmp);

		tmp = UniWidgetsTypes::xmlGetProp(it,"font");
		if (tmp.empty())
			view_parameters.font = "Liberation Sans 16";
		else
			view_parameters.font = tmp;
	}
	return view_parameters;
}
// -------------------------------------------------------------------------
Gtk::Window*
UParamPopup::create_popup()
{
	Gtk::Window* new_window = new Gtk::Window;

	new_window->set_transient_for( *dynamic_cast<Gtk::Window*>(get_toplevel()) );
	new_window->set_decorated(false);
	new_window->set_default_size( property_window_width_, property_window_height_ );
	new_window->set_position(Gtk::WIN_POS_CENTER_ON_PARENT);

	Gtk::ScrolledWindow* scrolled_window = Gtk::manage( new Gtk::ScrolledWindow );
	scrolled_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	
	Gtk::TreeView* tree_view = Gtk::manage( new Gtk::TreeView );
	Glib::RefPtr<Gtk::ListStore> tree_model = Gtk::ListStore::create(columns);
	tree_view->set_model(tree_model);

	scrolled_window->add(*tree_view);

	Gtk::VBox* vbox = Gtk::manage( new Gtk::VBox );
	Gtk::Button *close_button = Gtk::manage( new Gtk::Button("Close") );
	
	Gtk::Frame* frame = Gtk::manage( new Gtk::Frame );
	frame->add(*vbox);
	new_window->add(*frame);
	vbox->add(*scrolled_window);
	//vbox->add(*frame);
	//vbox->set_spacing(3);
	//vbox->pack_start(*close_button, Gtk::PACK_SHRINK);

	sigc::slot<void> focus_out =
		sigc::hide_return(sigc::bind(sigc::mem_fun(this, &UParamPopup::on_focus_out),popup_window));
	close_button->signal_clicked().connect(focus_out);

	{
	CellRendererSensorValue* renderer = Gtk::manage( new CellRendererSensorValue() );
	tree_view->append_column("Value", *renderer);
	Gtk::TreeViewColumn* column = tree_view->get_column(0);
	column->add_attribute(renderer->property_value(), columns.value);
	column->add_attribute(renderer->property_view_parameters(), columns.view_parameters);
	}

	{
	Gtk::CellRendererText* renderer = Gtk::manage( new Gtk::CellRendererText() );
	tree_view->append_column("Text", *renderer);
	Gtk::TreeViewColumn* column = tree_view->get_column(1);
	column->add_attribute(renderer->property_text(), columns.text_message);
	renderer->property_font() = "Liberation Sans 14";
	renderer->property_background_gdk() = Gdk::Color("black");
	renderer->property_foreground_gdk() = Gdk::Color("white");
	renderer->property_ellipsize_set() = true;
	renderer->property_ellipsize() = Pango::ELLIPSIZE_END;
	}

	if ( get_connector() && 
	     !property_attribut_.get_value().empty() &&
	     !property_value_.   get_value().empty() )
	{
		assert( !get_connector() );
		xmlNode* it = get_connector()->get_xml_node("sensors");
		if( it )
		{
			if ( !(it = it->children) ) {
				/** TODO: error processing needed */
			}
			
			for ( ; it; it = it->next ) {
				string tmp = UniWidgetsTypes::xmlGetProp(it, property_attribut_.get_value() );
				if ( tmp != property_value_.get_value() )
					continue;

				Gtk::TreeRow row = *(tree_model->append());
				Glib::ustring text = UniWidgetsTypes::xmlGetProp(it,"textname");
				row[columns.text_message] = text;
				row[columns.view_parameters] =
					get_view_parameters_from_node(it);
				ObjectId id = atoi(UniWidgetsTypes::xmlGetProp(it,"id").c_str());

				tmp = UniWidgetsTypes::xmlGetProp(it,"node");
				ObjectId node;
				if ( tmp.empty() )
					node = get_connector()->getLocalNode();
				else
					node = get_connector()->getNodeID( tmp );

				try {
					sigc::slot<void, ObjectId, ObjectId, long> value_changed =
						sigc::hide<0>(sigc::hide<0>(sigc::bind(
							sigc::mem_fun(this, &UParamPopup::sensor_value_changed),row)));

					get_connector()->signals().connect_value_changed(value_changed, id, node);
					long value = get_connector()->get_value_directly(id,node);
					row[columns.value] = value;
				} 
				catch ( UniWidgetsTypes::Exception& ex ) {
					cerr << "(" << get_name() 
						<< "): error while getting value of " 
						<< id << "," << node 
						<< ex << endl;
				}
			}
		}
	}
	
	return new_window;
}
// -------------------------------------------------------------------------
void
UParamPopup::sensor_value_changed( long value, Gtk::TreeRow row )
{
	const PPViewParameters& view_parameters = row[columns.view_parameters];
	const int& precision = view_parameters.precision;
	row[columns.value] = value / pow(double(10),int(precision));
}
// -------------------------------------------------------------------------
