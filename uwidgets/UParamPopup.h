#ifndef _UPARAMPOPUP_H
#define _UPARAMPOPUP_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <gdkmm.h>
#include <gtkmm.h>
#include "UEventBox.h"
#include "USensor.h"
// -------------------------------------------------------------------------
/** PPViewParameters stores all parameters
 * from xml file for cellrenderer.
 * It is not safe!
 */
// -------------------------------------------------------------------------
struct PPViewParameters
{
	int view_type;
	int precision;
	std::string format;
	Gdk::Color color;
	Gdk::Color bg_color;
	std::string font;
	struct Message
	{
		Glib::ustring text;
		Gdk::Color color;
		Gdk::Color bg_color;
		Glib::RefPtr<Gdk::Pixbuf> image_ptr;
		Glib::ustring font;
	};
	typedef std::unordered_map<int,Message> MessagesMap;
	MessagesMap messages_map;
};
// -------------------------------------------------------------------------
/** TODO: write documentation
 *
 */
class CellRendererSensorValue : public Gtk::CellRenderer
{
public:
	CellRendererSensorValue();
	virtual ~CellRendererSensorValue();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<float> property_value();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<float> property_value() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<PPViewParameters> property_view_parameters();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<PPViewParameters> property_view_parameters() const;

protected:
	Glib::Property<float> property_value_;
	Glib::Property<PPViewParameters> property_view_parameters_;

	virtual void get_size_vfunc (Gtk::Widget& widget, const Gdk::Rectangle* cell_area,
			int* x_offset, int* y_offset, int* width, int* height) const;

	virtual void render_vfunc (const Glib::RefPtr<Gdk::Drawable>& window, Gtk::Widget& widget,
			const Gdk::Rectangle& background_area, const Gdk::Rectangle& cell_area,
			const Gdk::Rectangle& expose_area, Gtk::CellRendererState flags);
};
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<float>
CellRendererSensorValue::property_value()
{
	return property_value_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<float>
CellRendererSensorValue::property_value() const
{
	return Glib::PropertyProxy_ReadOnly<float>(this,"value");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<PPViewParameters>
CellRendererSensorValue::property_view_parameters()
{
	return property_view_parameters_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<PPViewParameters>
CellRendererSensorValue::property_view_parameters() const
{
	return Glib::PropertyProxy_ReadOnly<PPViewParameters>(this,"view-parameters");
}
// -------------------------------------------------------------------------
/*!
 * \brief Класс всплывающего окна.
 * \par
 * Класс предназначен для формирование всплывающего окна с таблицей, содержащей датчики
 * и текстовое обозначение датчиков. Значения с датчиков считываются и отображаются в таблице.
*/
class UParamPopup : public UEventBox
{
private: 
	void ctor();
	Glib::Property<Glib::ustring> property_attribut_;
	Glib::Property<Glib::ustring> property_value_;
	Glib::Property<int> property_window_width_;
	Glib::Property<int> property_window_height_;

	Gtk::Window * popup_window;
	void sensor_value_changed(long value, Gtk::TreeRow row);
	PPViewParameters get_view_parameters_from_node(xmlNode* it);

protected:
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ModelColumns() {
			add(text_message);
			add(value);
			add(view_parameters);
			add(connection);
		}

		Gtk::TreeModelColumn<Glib::ustring> text_message;
		Gtk::TreeModelColumn<long> value;
		Gtk::TreeModelColumn<PPViewParameters> view_parameters;
		Gtk::TreeModelColumn<USignals::Connection> connection; 
	} columns;

	virtual bool on_button_press_event (GdkEventButton* event);
	virtual bool on_focus_out(Gtk::Widget* widget);
	virtual Gtk::Window* create_popup();

public:
	UParamPopup();
	explicit UParamPopup(GtkmmBaseType::BaseObjectType* gobject);

	~UParamPopup();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<Glib::ustring> property_attribut();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly <Glib::ustring> property_attribut() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<Glib::ustring> property_value();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_value() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<int> property_window_width();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<int> property_window_width() const;

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy<int> property_window_height();

	/** TODO: write documentation
	 *
	 */
	Glib::PropertyProxy_ReadOnly<int> property_window_height() const;
};
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<Glib::ustring>
UParamPopup::property_attribut()
{
	return property_attribut_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
UParamPopup::property_attribut() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"attribut");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<Glib::ustring>
UParamPopup::property_value()
{
	return property_value_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
UParamPopup::property_value() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"value");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<int>
UParamPopup::property_window_width()
{
	return property_window_width_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<int>
UParamPopup::property_window_width() const
{
	return Glib::PropertyProxy_ReadOnly<int>(this,"window-width");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<int>
UParamPopup::property_window_height()
{
	return property_window_height_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<int>
UParamPopup::property_window_height() const
{
	return Glib::PropertyProxy_ReadOnly<int>(this,"window-height");
}
#endif
