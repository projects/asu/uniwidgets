#include <gdkmm.h>
#include <iostream>
#include <fstream>
#include <errno.h>
#include <iomanip>
#include "UPopupBox.h"
#include "UniOscillograph.h"
#include "ConfirmSignal.h"
#include <libglademm.h>
#include <glade/glade.h>

// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using UMessages::MessageId;
// -------------------------------------------------------------------------
static const int resize_step_time=50;
// -------------------------------------------------------------------------
#define INIT_UPopupBox_PROPERTIES() \
	 prop_XMin(*this, "min-width" , 0) \
	 ,prop_YMin(*this, "min-height" , 0) \
	 ,prop_XMax(*this, "max-width" , 100) \
	 ,prop_YMax(*this, "max-height" , 100) \
	 ,prop_expand_type(*this, "expand-type" , TO_BOTTOM_RIGHT) \
	 ,prop_consumer_widget_1(*this, "consumer-widget-1" , nullptr) \
	 ,prop_consumer_widget_2(*this, "consumer-widget-2" , nullptr) \
	 ,prop_consumer_widget_3(*this, "consumer-widget-3" , nullptr) \
	 ,prop_consumer_widget_4(*this, "consumer-widget-4" , nullptr) \
	 ,prop_consumer_widget_5(*this, "consumer-widget-5" , nullptr) \
	 ,prop_consumer_minimize_widget(*this, "consumer-minimize-widget" , nullptr) \
	 ,prop_maximize_delay(*this, "maximize-delay" , 300) \
	 ,prop_minimize_delay(*this, "minimize-delay" , 300) \
	 ,prop_resize_full_time(*this, "resize-full-time" , 500) \
	 ,parent(NULL) \
	 ,widget_x(0) \
	 ,widget_y(0) \
	 ,is_show(false) \
	 ,is_expand(true) \
	 ,is_resized(false) \
	 ,is_realized(false) \
	 
// -------------------------------------------------------------------------
void UPopupBox::ctor()
{
//	set_flags(Gtk::NO_WINDOW);
	set_visible_window(true);
//	set_events(Gdk::ALL_EVENTS_MASK);
	signal_event_after().connect(sigc::bind(sigc::mem_fun(*this, &UPopupBox::on_area_event_after),this));
	signal_size_allocate().connect(sigc::mem_fun(*this, &UPopupBox::area_allocate_changed));
	
	connect_property_changed("min-width", sigc::mem_fun(*this, &UPopupBox::on_XMin_changed) );
	connect_property_changed("min-height", sigc::mem_fun(*this, &UPopupBox::on_YMin_changed) );
	connect_property_changed("max-width", sigc::mem_fun(*this, &UPopupBox::on_XMax_changed) );
	connect_property_changed("max-height", sigc::mem_fun(*this, &UPopupBox::on_YMax_changed) );
	connect_property_changed("resize-full-time", sigc::mem_fun(*this, &UPopupBox::on_resize_full_time_changed) );
	connect_property_changed("consumer-widget-1", sigc::mem_fun(*this, &UPopupBox::on_consumer_widget1_changed) );
	connect_property_changed("consumer-widget-2", sigc::mem_fun(*this, &UPopupBox::on_consumer_widget2_changed) );
	connect_property_changed("consumer-widget-3", sigc::mem_fun(*this, &UPopupBox::on_consumer_widget3_changed) );
	connect_property_changed("consumer-widget-4", sigc::mem_fun(*this, &UPopupBox::on_consumer_widget4_changed) );
	connect_property_changed("consumer-widget-5", sigc::mem_fun(*this, &UPopupBox::on_consumer_widget5_changed) );
	connect_property_changed("consumer-minimize-widget", sigc::mem_fun(*this, &UPopupBox::on_consumer_minimize_widget_changed) );
	
	signal_child_notify().connect(sigc::mem_fun(*this, &UPopupBox::on_packing_changed));
//	Glib::signal_timeout().connect(sigc::mem_fun(*this, &UPopupBox::init_timer), 10);
}
// -------------------------------------------------------------------------
UPopupBox::UPopupBox() :
		Glib::ObjectBase("upopupbox")
		,INIT_UPopupBox_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UPopupBox::UPopupBox(GtkmmBaseType::BaseObjectType* gobject) :
		BaseType(gobject)
		,INIT_UPopupBox_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UPopupBox::~UPopupBox()
{
}
// -------------------------------------------------------------------------
void UPopupBox::init_consumers(Gtk::Widget* widget)
{
//	cout<<get_name()<<"::init_consumers widget_name="<<widget->get_name()<<endl;
//	cout<< get_name() << "::init_consumers() widget="<<widget<<endl;
	if(widget)
	{
// 		cout<<get_name()<<"::init_consumers widget_name="<<widget->get_name()<<endl;
		Gtk::Container* conteiner = dynamic_cast<Gtk::Container*>(widget);
		if(conteiner)
		{
			conteiner->signal_add().connect(sigc::mem_fun(*this, &UPopupBox::init_consumers));
			Glib::ListHandle<Gtk::Widget*> childlist = conteiner->get_children();
			for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); ++it)
				init_consumers(*it);
		}
		
		if(!get_prop_consumer_widget_1() && !consumer_widget_1_name.empty() && consumer_widget_1_name == widget->get_name())
		{
			set_prop_consumer_widget_1(widget);
		}
		else if(!get_prop_consumer_widget_2() && !consumer_widget_2_name.empty() && consumer_widget_2_name == widget->get_name())
		{
			set_prop_consumer_widget_2(widget);
		}
		else if(!get_prop_consumer_widget_3() && !consumer_widget_3_name.empty() && consumer_widget_3_name == widget->get_name())
		{
			set_prop_consumer_widget_3(widget);
		}
		else if(!get_prop_consumer_widget_4() && !consumer_widget_4_name.empty() && consumer_widget_4_name == widget->get_name())
		{
			set_prop_consumer_widget_4(widget);
		}
		else if(!get_prop_consumer_widget_5() && !consumer_widget_5_name.empty() && consumer_widget_5_name == widget->get_name())
		{
			set_prop_consumer_widget_5(widget);
		}
		else if(!get_prop_consumer_minimize_widget() && !consumer_minimize_widget_name.empty() && consumer_minimize_widget_name == widget->get_name())
		{
			set_prop_consumer_minimize_widget(widget);
		}
	}

/*	
	try
	{
		Glib::RefPtr<Gnome::Glade::Xml> gxml = Gnome::Glade::Xml::get_widget_tree(*static_cast<Gtk::Widget*>(this));

		if(!get_prop_consumer_widget_1() && !consumer_widget_1_name.empty() && consumer_widget_1_name == widget->get_name())
		{
			set_prop_consumer_widget_1(gxml->get_widget(consumer_widget_1_name));
		}
		if(!get_prop_consumer_widget_2() && !consumer_widget_2_name.empty() && consumer_widget_2_name == widget->get_name())
		{
			set_prop_consumer_widget_2(gxml->get_widget(consumer_widget_2_name));
		}
		if(!get_prop_consumer_widget_3() && !consumer_widget_3_name.empty() && consumer_widget_3_name == widget->get_name())
		{
			set_prop_consumer_widget_3(gxml->get_widget(consumer_widget_3_name));
		}
		if(!get_prop_consumer_widget_4() && !consumer_widget_4_name.empty() && consumer_widget_4_name == widget->get_name())
		{
			set_prop_consumer_widget_4(gxml->get_widget(consumer_widget_4_name));
		}
		if(!get_prop_consumer_widget_5() && !consumer_widget_5_name.empty() && consumer_widget_5_name == widget->get_name())
		{
			set_prop_consumer_widget_5(gxml->get_widget(consumer_widget_5_name));
		}
	}
	catch( Gnome::Glade::XmlError& ex )
	{
	}
	*/

/*	
	bool rv = false;
	GtkWidget *w = GTK_WIDGET(gobj());
	GtkWidget *consumer_widget = 0;
	cout<< get_name() << "::init_timer() prop_consumer_widget_1="<<get_prop_consumer_widget_1()<<" consumer_widget_1_name="<<consumer_widget_1_name<<endl;
	cout<< get_name() << "::init_timer() prop_consumer_widget_2="<<get_prop_consumer_widget_2()<<" consumer_widget_2_name="<<consumer_widget_2_name<<endl;
	cout<< get_name() << "::init_timer() prop_consumer_widget_3="<<get_prop_consumer_widget_3()<<" consumer_widget_3_name="<<consumer_widget_3_name<<endl;
	cout<< get_name() << "::init_timer() prop_consumer_widget_4="<<get_prop_consumer_widget_4()<<" consumer_widget_4_name="<<consumer_widget_4_name<<endl;
	cout<< get_name() << "::init_timer() prop_consumer_widget_5="<<get_prop_consumer_widget_5()<<" consumer_widget_5_name="<<consumer_widget_5_name<<endl;
	if(!get_prop_consumer_widget_1() && !consumer_widget_1_name.empty())
	{
		consumer_widget = glade_xml_get_widget(glade_get_widget_tree(w),consumer_widget_1_name.c_str());
		if(consumer_widget)
		{
			set_prop_consumer_widget_1(Glib::wrap(consumer_widget));
			rv = rv|false;
		}
		else
			rv = true;
	}
	if(!get_prop_consumer_widget_2() && !consumer_widget_2_name.empty())
	{
		consumer_widget = glade_xml_get_widget(glade_get_widget_tree(w),consumer_widget_2_name.c_str());
		if(consumer_widget)
		{
			set_prop_consumer_widget_2(Glib::wrap(consumer_widget));
			rv = rv|false;
		}
		else
			rv = true;
	}
	if(!get_prop_consumer_widget_3() && !consumer_widget_3_name.empty())
	{
		consumer_widget = glade_xml_get_widget(glade_get_widget_tree(w),consumer_widget_3_name.c_str());
		if(consumer_widget)
		{
			set_prop_consumer_widget_3(Glib::wrap(consumer_widget));
			rv = rv|false;
		}
		else
			rv = true;
	}
	if(!get_prop_consumer_widget_4() && !consumer_widget_4_name.empty())
	{
		consumer_widget = glade_xml_get_widget(glade_get_widget_tree(w),consumer_widget_4_name.c_str());
		if(consumer_widget)
		{
			set_prop_consumer_widget_4(Glib::wrap(consumer_widget));
			rv = rv|false;
		}
		else
			rv = true;
	}
	if(!get_prop_consumer_widget_5() && !consumer_widget_5_name.empty())
	{
		consumer_widget = glade_xml_get_widget(glade_get_widget_tree(w),consumer_widget_5_name.c_str());
		if(consumer_widget)
		{
			set_prop_consumer_widget_5(Glib::wrap(consumer_widget));
			rv = rv|false;
		}
		else
			rv = true;
	}
	return rv;
	*/
}
// -------------------------------------------------------------------------
GType expanddirectiontype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { UPopupBox::TO_BOTTOM_RIGHT, "TO_BOTTOM_RIGHT", "В нижний-правый угол" },{ UPopupBox::TO_BOTTOM_LEFT, "TO_BOTTOM_LEFT", "В нижний-левый угол" },{ UPopupBox::TO_TOP_RIGHT, "TO_TOP_RIGHT", "В верхний-правый угол" },{ UPopupBox::TO_TOP_LEFT, "TO_TOP_LEFT", "В верхний-левый угол" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("ExpandDirectionType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UPopupBox::ExpandDirectionType>::value_type()
{
	return expanddirectiontype_get_type();
}
// -------------------------------------------------------------------------
void UPopupBox::on_realize()
{
	Gtk::EventBox::on_realize();
	if(!is_realized)
	{
		is_realized = true;
		
		width = get_prop_XMin();
		height = get_prop_YMin();
		if(parent)
		{
			widget_x = get_allocation().get_x() - parent->get_allocation().get_x();
			widget_y = get_allocation().get_y() - parent->get_allocation().get_y();
		}
		on_resize_full_time_changed();
		
		
		set_size_request(get_prop_XMin(),get_prop_YMin());
// 		width = get_allocation().get_width();
// 		height = get_allocation().get_height();
// 		cout<< get_name() << "::on_realize() widget_x="<<widget_x<<" widget_y="<<widget_y<<" width="<<width<<" height="<<height<<endl;
		minimize();
//  	maximize_delay_tmr.disconnect();
//  	minimize_delay_tmr= Glib::signal_timeout().connect(sigc::bind(sigc::mem_fun(*this, &UPopupBox::minimize_on_delay_tick), this), 100);
	}
}
// -------------------------------------------------------------------------
bool UPopupBox::on_expose_event(GdkEventExpose* ev)
{
	Gtk::EventBox::on_expose_event(ev);
	#if GTK_VERSION_GE(2,20)
	if(!get_mapped())
	#else
	if(!is_show)
	#endif
		return false;

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
		
	return false;
}
// -------------------------------------------------------------------------
void UPopupBox::on_parent_changed(Gtk::Widget* w)
{
	Gtk::EventBox::on_parent_changed(w);
	parent = dynamic_cast<Gtk::Fixed*>(get_parent());
//	cout<<get_name()<<"::on_parent_changed() parent="<<parent<< endl;
}
// -------------------------------------------------------------------------
void UPopupBox::on_add(Gtk::Widget* w)
{
	if (UVoid* uv = dynamic_cast<UVoid*>(w))
	{
//		uv->property_disconnect_effect() = 0;
		uv->set_property_auto_connect(true);
	}
	
	Gtk::EventBox::on_add(w);
	connect_to_lo_erarchy(w);
}
// -------------------------------------------------------------------------
void UPopupBox::connect_to_lo_erarchy(Gtk::Widget* w, ConnectionsList* list)
{
	if(!w)
		return;
//	cout<<get_name()<<"::connect_to_lo_erarchy() list="<<list<< endl;
	if(Gtk::Container* cont = dynamic_cast<Gtk::Container*>(w))
	{
		Glib::ListHandle<Gtk::Widget*> childlist = cont->get_children();
		for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); ++it)
			connect_to_lo_erarchy(*it, list);
	}
//	w->set_events(Gdk::ALL_EVENTS_MASK);
//	cout<<w->get_name()<<"::connect_to_lo_erarchy() list="<<list<< endl;
	if(list)
		list->push_back(w->signal_event_after().connect(sigc::bind(sigc::mem_fun(*this, &UPopupBox::on_area_event_after),w)));
	else
		w->signal_event_after().connect(sigc::bind(sigc::mem_fun(*this, &UPopupBox::on_area_event_after),w));
}
// -------------------------------------------------------------------------
void UPopupBox::connect_to_lo_erarchy_for_minimize(Gtk::Widget* w, ConnectionsList* list)
{
	if(!w)
		return;
//	cout<<get_name()<<"::connect_to_lo_erarchy_for_minimize() list="<<list<< endl;
	if(Gtk::Container* cont = dynamic_cast<Gtk::Container*>(w))
	{
		Glib::ListHandle<Gtk::Widget*> childlist = cont->get_children();
		for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); ++it)
			connect_to_lo_erarchy_for_minimize(*it, list);
	}
//	w->set_events(Gdk::ALL_EVENTS_MASK);
//	cout<<w->get_name()<<"::connect_to_lo_erarchy_for_minimize() list="<<list<< endl;
	if(list)
		list->push_back(w->signal_event().connect(sigc::mem_fun(*this, &UPopupBox::on_minimaze_widget_event)));
	else
		w->signal_event().connect(sigc::mem_fun(*this, &UPopupBox::on_minimaze_widget_event));
}
// -------------------------------------------------------------------------
void UPopupBox::disconnect_consumer_widgets(ConnectionsList& list)
{
	if(list.empty())
		return;

	for(ConnectionsList::iterator it = list.begin(); it != list.end(); ++it)
		(*it).disconnect();
	list.clear();
}
// -------------------------------------------------------------------------
void UPopupBox::on_remove(Gtk::Widget* w)
{
//	checkNotify->remove(w);
	Gtk::EventBox::on_remove(w);
}
// -------------------------------------------------------------------------
#define check_child( W ) \
	if ( childinfomap_.count(W) == 0 ) { \
		std::cerr << "(UPopupBox):Error:" \
			  << W->get_name() << \
			     " is not in container" <<std::endl; \
		return; \
}
// -------------------------------------------------------------------------
void UPopupBox::set_child_connector(Gtk::Widget& child)
{
	UVoid* voidChild = dynamic_cast<UVoid*>(&child);
	if(voidChild && get_connector())
	{
		voidChild->set_connector(get_connector());
	}
}
// -------------------------------------------------------------------------
void UPopupBox::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UVoid::set_connector(connector);

	sigc::slot<void, Gtk::Widget&> set_child_connector = sigc::mem_fun(this, &UPopupBox::set_child_connector);

	foreach(set_child_connector);
}
// -------------------------------------------------------------------------
void UPopupBox::on_connect() throw()
{
	UVoid::on_connect();

	queue_draw();
}
// -------------------------------------------------------------------------
void UPopupBox::on_disconnect() throw()
{
	UVoid::on_disconnect();
	queue_draw();
}
// -------------------------------------------------------------------------
void UPopupBox::process_sensor(ObjectId id, ObjectId node, long value)
{
}
// -------------------------------------------------------------------------
void UPopupBox::on_consumer_widget1_changed()
{
//	cout<<get_name()<<"::on_consumer_widget1_changed() name="<<(get_prop_consumer_widget_1() ? get_prop_consumer_widget_1()->get_name():"???")<< endl;
	disconnect_consumer_widgets(consumer_widget_connection_list_1);
	connect_to_lo_erarchy(get_prop_consumer_widget_1(),&consumer_widget_connection_list_1);
}
// -------------------------------------------------------------------------
void UPopupBox::on_consumer_widget2_changed()
{
//	cout<<get_name()<<"::on_consumer_widget2_changed() name="<<(get_prop_consumer_widget_2() ? get_prop_consumer_widget_2()->get_name():"???")<< endl;
	disconnect_consumer_widgets(consumer_widget_connection_list_2);
	connect_to_lo_erarchy(get_prop_consumer_widget_2(),&consumer_widget_connection_list_2);
}
// -------------------------------------------------------------------------
void UPopupBox::on_consumer_widget3_changed()
{
//	cout<<get_name()<<"::on_consumer_widget3_changed() name="<<(get_prop_consumer_widget_3() ? get_prop_consumer_widget_3()->get_name():"???")<< endl;
	disconnect_consumer_widgets(consumer_widget_connection_list_3);
	connect_to_lo_erarchy(get_prop_consumer_widget_3(),&consumer_widget_connection_list_3);
}
// -------------------------------------------------------------------------
void UPopupBox::on_consumer_widget4_changed()
{
//	cout<<get_name()<<"::on_consumer_widget4_changed() name="<<(get_prop_consumer_widget_4() ? get_prop_consumer_widget_4()->get_name():"???")<< endl;
	disconnect_consumer_widgets(consumer_widget_connection_list_4);
	connect_to_lo_erarchy(get_prop_consumer_widget_4(),&consumer_widget_connection_list_4);
}
// -------------------------------------------------------------------------
void UPopupBox::on_consumer_widget5_changed()
{
	//	cout<<get_name()<<"::on_consumer_widget5_changed() name="<<(get_prop_consumer_widget_5() ? get_prop_consumer_widget_5()->get_name():"???")<< endl;
	disconnect_consumer_widgets(consumer_widget_connection_list_5);
	connect_to_lo_erarchy(get_prop_consumer_widget_5(),&consumer_widget_connection_list_5);
}
// -------------------------------------------------------------------------
void UPopupBox::on_consumer_minimize_widget_changed()
{
	cout<<get_name()<<"::on_consumer_minimize_widget_changed() name="<<(get_prop_consumer_minimize_widget() ? get_prop_consumer_minimize_widget()->get_name():"???")<< endl;
	disconnect_consumer_widgets(consumer_minimize_widget_connection_list);
	if(get_prop_consumer_minimize_widget())
		connect_to_lo_erarchy_for_minimize(get_prop_consumer_minimize_widget(),&consumer_minimize_widget_connection_list);
}
// -------------------------------------------------------------------------
void UPopupBox::on_XMin_changed()
{
	double T = (double)get_prop_resize_full_time();
	accel_width =  ((double)(get_prop_XMax() - get_prop_XMin()))*2 / pow(T,2);
	set_size_request(get_prop_XMin(),get_prop_YMin());
}
// -------------------------------------------------------------------------
void UPopupBox::on_YMin_changed()
{
	double T = (double)get_prop_resize_full_time();
	accel_height =  ((double)(get_prop_YMax() - get_prop_YMin()))*2 / pow(T,2);
	set_size_request(get_prop_XMin(),get_prop_YMin());
}
// -------------------------------------------------------------------------
void UPopupBox::on_XMax_changed()
{
	double T = (double)get_prop_resize_full_time();
	accel_width =  ((double)(get_prop_XMax() - get_prop_XMin()))*2 / pow(T,2);
	set_size_request(get_prop_XMax(),get_prop_YMax());
}
// -------------------------------------------------------------------------
void UPopupBox::on_YMax_changed()
{
	double T = (double)get_prop_resize_full_time();
	accel_height =  ((double)(get_prop_YMax() - get_prop_YMin()))*2 / pow(T,2);
	set_size_request(get_prop_XMax(),get_prop_YMax());
}
// -------------------------------------------------------------------------
void UPopupBox::on_resize_full_time_changed()
{
	double T = (double)get_prop_resize_full_time();
	accel_width =  ((double)(get_prop_XMax() - get_prop_XMin()))*2 / pow(T,2);
	accel_height =  ((double)(get_prop_YMax() - get_prop_YMin()))*2 / pow(T,2);
}
// -------------------------------------------------------------------------
void UPopupBox::on_packing_changed(GParamSpec* pspec)
{
	if( is_glade_editor )
	{
		GtkWidget *w = GTK_WIDGET(gobj());
		(*packing_changed_function)( w, pspec );
	}
}
// -------------------------------------------------------------------------
void alloc_for_popoup_children(Gtk::Widget& w, Gtk::Allocation alloc)
{
	w.size_allocate(alloc);
}
// -------------------------------------------------------------------------
void UPopupBox::area_allocate_changed(Gtk::Allocation& alloc)
{
	if(get_child())
	{
		child_area_allocate_changed(get_child(), alloc);
		get_child()->set_size_request(get_allocation().get_width(), get_allocation().get_height());
	}
	set_allocation(alloc);
}
// -------------------------------------------------------------------------
void UPopupBox::child_area_allocate_changed(Gtk::Widget* w, Gtk::Allocation& alloc)
{
//	w->set_allocation(alloc);
// 	w->size_allocate(alloc);
	
	Gtk::Container* conteiner = dynamic_cast<Gtk::Container*>(w);
	if(conteiner)
	{
		Glib::ListHandle<Gtk::Widget*> childlist = conteiner->get_children();
		for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); ++it)
		{
// 			cout<< w->get_name()<<"::"<<(*it)->get_name()<< "::child_area_allocate_changed() widget_w="<<alloc.get_width()<<" widget_h="<<alloc.get_height()<<endl;
			child_area_allocate_changed(*it, alloc);
		}
	}
}
// -------------------------------------------------------------------------
void UPopupBox::on_area_event_after(GdkEvent* ev, Gtk::Widget* w)
{
//	if(ev->type!=GDK_EXPOSE)
//		cout<<get_name()<<"::on_plot_area_event event.type="<<ev->type<< endl;
	GdkEventButton event_btn = ev->button;
	
	if(ev->type==GDK_MAP)
		is_show = true;
	else if(ev->type==GDK_UNMAP)
		is_show = false;
	
	else if( ev->type == GDK_ENTER_NOTIFY )
	{
//		cout<<get_name()<<"::on_area_event_after::GDK_ENTER_NOTIFY event.type="<<ev->type<<" w="<<w<< endl;
		if (!maximize_delay_tmr.connected())
		{
			minimize_delay_tmr.disconnect();
			maximize_delay_tmr= Glib::signal_timeout().connect(sigc::bind(sigc::mem_fun(*this, &UPopupBox::maximize_on_delay_tick), w), get_prop_maximize_delay()>100? get_prop_maximize_delay():100);
		}
	}
	else if( ev->type == GDK_LEAVE_NOTIFY )
	{
//		cout<<get_name()<<"::on_area_event_after::GDK_LEAVE_NOTIFY event.type="<<ev->type<<" w="<<w<< endl;
		if (!minimize_delay_tmr.connected())
		{
			maximize_delay_tmr.disconnect();
			minimize_delay_tmr= Glib::signal_timeout().connect(sigc::bind(sigc::mem_fun(*this, &UPopupBox::minimize_on_delay_tick), w), get_prop_minimize_delay()>100? get_prop_minimize_delay():100);
		}
	}
}
// -------------------------------------------------------------------------
bool UPopupBox::on_minimaze_widget_event(GdkEvent* ev)
{
	//	if(ev->type!=GDK_EXPOSE)
	//		cout<<get_name()<<"::on_minimaze_widget_event event.type="<<ev->type<< endl;
	GdkEventButton event_btn = ev->button;
	
// 	cout<<get_name()<<"::on_minimaze_widget_event() get_prop_consumer_minimize_widget()="<<get_prop_consumer_minimize_widget()<<" ev->type="<<ev->type<<" GDK_BUTTON_PRESS="<<GDK_BUTTON_PRESS<< endl;
	if(get_prop_consumer_minimize_widget() && event_btn.type == GDK_BUTTON_PRESS)
	{
		if (!minimize_delay_tmr.connected())
		{
			maximize_delay_tmr.disconnect();
			minimize_delay_tmr= Glib::signal_timeout().connect(sigc::bind(sigc::mem_fun(*this, &UPopupBox::minimize_on_delay_tick), get_prop_consumer_minimize_widget()), get_prop_minimize_delay()>100? get_prop_minimize_delay():100);
		}
	}
	return false;
}
// -------------------------------------------------------------------------
bool UPopupBox::minimize_on_delay_tick(Gtk::Widget* w)
{
// 	cout<<get_name()<<"::minimize_on_delay_tick::GDK_LEAVE_NOTIFY" << endl;
	minimize_delay_tmr.disconnect();
	minimize();
	return false;
}
// -------------------------------------------------------------------------
void UPopupBox::minimize()
{
	if(is_expand)
	{
		is_expand = false;
		if(!is_resized)
		{
			is_resized = true;
			resize_timer_connector.disconnect();
			num_step=1;
//			cout<< get_name() << "::minimize() widget_x="<<widget_x<<" widget_y="<<widget_y<<endl;
			resize_timer_connector = Glib::signal_timeout().connect(sigc::mem_fun(*this, &UPopupBox::resize_timer),resize_step_time);
		}
	}
}
// -------------------------------------------------------------------------
bool UPopupBox::maximize_on_delay_tick(Gtk::Widget* w)
{
// 	cout<<get_name()<<"::maximize_on_delay_tick::GDK_LEAVE_NOTIFY" << endl;
	maximize_delay_tmr.disconnect();
	maximize();
	return false;
}
// -------------------------------------------------------------------------
void UPopupBox::maximize()
{
	if(!is_expand)
	{
		is_expand = true;
		if(!is_resized)
		{
			is_resized = true;
			resize_timer_connector.disconnect();
			num_step=1;
			x = widget_x;
			y = widget_y;
//			cout<< get_name() << "::maximize() widget_x="<<widget_x<<" widget_y="<<widget_y<<endl;
			resize_timer_connector = Glib::signal_timeout().connect(sigc::mem_fun(*this, &UPopupBox::resize_timer),resize_step_time);
		}
	}
}
// -------------------------------------------------------------------------
bool UPopupBox::resize_timer()
{
	is_resized = true;
	
	bool continue_resize = true;
	float t = (float)resize_step_time;
	float dw = (accel_width*num_step*t)*t;
	float dh = (accel_height*num_step*t)*t;
	
	Gtk::Allocation alloc = get_allocation();
	
	if(is_expand)
	{
		height = height + dh;
		width = width + dw;
		if(height >= get_prop_YMax())
		{
			height = get_prop_YMax();
			
			if(width >= get_prop_XMax())
			{
				width = get_prop_XMax();
				continue_resize = false;
			}
		}
		if(width >= get_prop_XMax())
		{
			width = get_prop_XMax();
			
			if(height >= get_prop_YMax())
			{
				height = get_prop_YMax();
				continue_resize = false;
			}
		}
//		cout<< get_name() << "::resize_timer(+) height="<<height<<" dh="<<dh<<" width="<<width<<" dw="<<dw<<endl;
		
		if(get_prop_expand_type()==TO_BOTTOM_RIGHT)
		{
			x = widget_x;
			y = widget_y;
		}
		else if(get_prop_expand_type()==TO_BOTTOM_LEFT)
		{
			if(parent)
			{
				if(continue_resize)
				{
					x = x - dw;
					if(x <= widget_x - abs(get_prop_XMax() - get_prop_XMin()))
						x = widget_x - abs(get_prop_XMax() - get_prop_XMin());
				}
				else
					x = widget_x - abs(get_prop_XMax() - get_prop_XMin());
//				cout<< get_name() << "::resize_timer(+) widget_x="<<widget_x<<" x="<<x<<" alloc.get_x()="<<alloc.get_x()<<" width="<<width<<" dw="<<dw<<endl;
			}
		}
		else if(get_prop_expand_type()==TO_TOP_RIGHT)
		{
			if(parent)
			{
				if(continue_resize)
				{
					y = y - dh;
//					cout<< get_name() << "::resize_timer() widget_x="<<widget_x<<" widget_y="<<widget_y<<" x="<<x<<" width="<<width<<" height="<<height<<endl;
					if(y <= widget_y - abs(get_prop_YMax() - get_prop_YMin()))
						y = widget_y - abs(get_prop_YMax() - get_prop_YMin());
				}
				else
					y = widget_y - abs(get_prop_YMax() - get_prop_YMin());
			}
		}
		else if(get_prop_expand_type()==TO_TOP_LEFT)
		{
			if(parent)
			{
				if(continue_resize)
				{
					x = x - dw;
					y = y - dh;
//					cout<< get_name() << "::resize_timer() widget_x="<<widget_x<<" widget_y="<<widget_y<<" x="<<x<<" width="<<width<<" height="<<height<<endl;
					if(x <= widget_x - abs(get_prop_XMax() - get_prop_XMin()))
						x = widget_x - abs(get_prop_XMax() - get_prop_XMin());
					if(y <= widget_y - abs(get_prop_YMax() - get_prop_YMin()))
						y = widget_y - abs(get_prop_YMax() - get_prop_YMin());
				}
				else
				{
					x = widget_x - abs(get_prop_XMax() - get_prop_XMin());
					y = widget_y - abs(get_prop_YMax() - get_prop_YMin());
				}
			}
		}
	}
	else
	{
		height = height - dh;
		width = width - dw;
		if(height <= get_prop_YMin())
		{
			height = get_prop_YMin();
			
			if(width <= get_prop_XMin())
			{
				width = get_prop_XMin();
				continue_resize = false;
			}
		}
		if(width <= get_prop_XMin())
		{
			width = get_prop_XMin();
			
			if(height <= get_prop_YMin())
			{
				height = get_prop_YMin();
				continue_resize = false;
			}
		}
//		cout<< get_name() << "::resize_timer(-) height="<<height<<" dh="<<dh<<" width="<<width<<" dw="<<dw<<endl;
		
		if(get_prop_expand_type()==TO_BOTTOM_RIGHT)
		{
			x = widget_x;
			y = widget_y;
		}
		else if(get_prop_expand_type()==TO_BOTTOM_LEFT)
		{
			if(parent)
			{
				if(continue_resize)
				{
					x = x + dw;
					if(x >= widget_x)
						x = widget_x;
				}
				else
					x = widget_x;
//				cout<< get_name() << "::resize_timer(-) widget_x="<<widget_x<<" x="<<x<<" alloc.get_x()="<<alloc.get_x()<<" width="<<width<<" dw="<<dw<<endl;
			}
		}
		else if(get_prop_expand_type()==TO_TOP_RIGHT)
		{
			if(parent)
			{
				if(continue_resize)
				{
					y = y + dh;
//					cout<< get_name() << "::resize_timer() widget_x="<<widget_x<<" widget_y="<<widget_y<<" x="<<x<<" width="<<width<<" height="<<height<<endl;
					if(y >= widget_y)
						y = widget_y;
				}
				else
					y = widget_y;
			}
		}
		else if(get_prop_expand_type()==TO_TOP_LEFT)
		{
			if(continue_resize)
			{
				x = x + dw;
				y = y + dh;
//				cout<< get_name() << "::resize_timer() x="<<x<<" widget_x="<<widget_x<<" widget_y="<<widget_y<<" width="<<width<<" height="<<height<<endl;
				if(x >= widget_x)
					x = widget_x;
				if(y >= widget_y)
					y = widget_y;
			}
			else
			{
				x = widget_x;
				y = widget_y;
			}
		}
	}
	num_step++;

	if(lround(width)<=0 || lround(height)<=0)
		hide();
	else
	{
		show();
		set_size_request(lround(width),lround(height));
	}
	if(parent)
		parent->move(*this,lround(x),lround(y));
//	queue_resize();
	
	is_resized = continue_resize;
// 	if(!continue_resize && !is_expand)
// 		set_size_request(widget_x,widget_y);

	return continue_resize;
}
// -------------------------------------------------------------------------
