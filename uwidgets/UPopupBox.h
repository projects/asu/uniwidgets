#ifndef _USCROLLFIXED_H
#define _USCROLLFIXED_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <map>
#include <gtkmm.h>
//#include <TransProperty.h>
//#include <uwidgets/UEventBox.h>
#include "UDefaultFunctions.h"
#include "USignals.h"
//#include <usvgwidgets/USVGImage.h>
#include "plugins.h"
#include "global_macros.h"
// -------------------------------------------------------------------------
/*!\brief Класс контейнера, меняющего свой размер если он в фокусе.
 * \par
 * Класс UPopupBox предназначен для хранения виджетов.
 * Класс основан на Gtk::EventBox
 */
class UPopupBox : public UDefaultFunctions<Gtk::EventBox>
{
	public:
		UPopupBox();
		explicit UPopupBox(GtkmmBaseType::BaseObjectType* gobject);
		virtual ~UPopupBox();

		typedef void(*packingChangedFunction)(GtkWidget*, const GParamSpec*);
		
		virtual void process_sensor(UniWidgetsTypes::ObjectId,UniWidgetsTypes::ObjectId,long);
		void startup_init(void);

		enum ExpandDirectionType
		{
			TO_BOTTOM_RIGHT = 0
			,TO_BOTTOM_LEFT
			,TO_TOP_RIGHT
			,TO_TOP_LEFT
		};
		
		inline void set_consumer_widget_1_name(Glib::ustring name) { consumer_widget_1_name = name; }
		inline void set_consumer_widget_2_name(Glib::ustring name) { consumer_widget_2_name = name; }
		inline void set_consumer_widget_3_name(Glib::ustring name) { consumer_widget_3_name = name; }
		inline void set_consumer_widget_4_name(Glib::ustring name) { consumer_widget_4_name = name; }
		inline void set_consumer_widget_5_name(Glib::ustring name) { consumer_widget_5_name = name; }
		inline void set_consumer_minimize_widget_name(Glib::ustring name) { consumer_minimize_widget_name = name; }
		void init_consumers(Gtk::Widget* widget);
		inline void connect_packing_changed_function(packingChangedFunction func){ packing_changed_function = func; };
		inline void set_widget_x(int x) { widget_x = x; }
		inline void set_widget_y(int y) { widget_y = y; }
		
protected:

		virtual void on_parent_changed(Gtk::Widget* w);
		virtual void on_add(Gtk::Widget* w);
		virtual void on_remove(Gtk::Widget* w);
		virtual void on_realize();
		virtual void area_allocate_changed(Gtk::Allocation& alloc);
		virtual void child_area_allocate_changed(Gtk::Widget* w, Gtk::Allocation& alloc);
		virtual bool on_expose_event(GdkEventExpose*);
		virtual void on_area_event_after(GdkEvent *event, Gtk::Widget* w);
		virtual bool on_minimaze_widget_event(GdkEvent *event);
		virtual void set_connector(const ConnectorRef& connector) throw();
		virtual void on_connect() throw();
		virtual void on_disconnect() throw();

		typedef std::list<sigc::connection> ConnectionsList;
		ConnectionsList consumer_widget_connection_list_1;
		ConnectionsList consumer_widget_connection_list_2;
		ConnectionsList consumer_widget_connection_list_3;
		ConnectionsList consumer_widget_connection_list_4;
		ConnectionsList consumer_widget_connection_list_5;
		ConnectionsList consumer_minimize_widget_connection_list;
		void disconnect_consumer_widgets(ConnectionsList& list);
		void connect_to_lo_erarchy(Gtk::Widget* w, ConnectionsList* list = NULL);
		void connect_to_lo_erarchy_for_minimize(Gtk::Widget* w, ConnectionsList* list = NULL);
		void minimize();
		void maximize();
		
		virtual bool maximize_on_delay_tick(Gtk::Widget* w);
		virtual bool minimize_on_delay_tick(Gtk::Widget* w);
		virtual bool resize_timer();
		
		void on_consumer_widget1_changed();
		void on_consumer_widget2_changed();
		void on_consumer_widget3_changed();
		void on_consumer_widget4_changed();
		void on_consumer_widget5_changed();
		void on_consumer_minimize_widget_changed();
		void on_XMin_changed();
		void on_XMax_changed();
		void on_YMin_changed();
		void on_YMax_changed();
		void on_resize_full_time_changed();
		void on_packing_changed(GParamSpec* pspec);
		
		Gtk::Fixed* parent;
		
	private:
		void ctor();
		sigc::connection maximize_delay_tmr;
		sigc::connection minimize_delay_tmr;
		sigc::connection resize_timer_connector;
		
		void connect_child(Gtk::Widget* child);
		void disconnect_child(Gtk::Widget& child);
		void set_child_connector(Gtk::Widget&);
		
		bool is_show;
		bool is_expand;
		bool is_resized;
		bool is_realized;
		int num_step;
		float accel_width;
		float accel_height;
		float width;
		float height;
		float widget_x;
		float widget_y;
		float x;
		float y;

		packingChangedFunction packing_changed_function;
		
		Glib::ustring consumer_widget_1_name;
		Glib::ustring consumer_widget_2_name;
		Glib::ustring consumer_widget_3_name;
		Glib::ustring consumer_widget_4_name;
		Glib::ustring consumer_widget_5_name;
		Glib::ustring consumer_minimize_widget_name;
		
		ADD_PROPERTY( prop_XMin, int )							/*!< свойство: уменьшать ширину виджета до...(в пикселях) */
		ADD_PROPERTY( prop_YMin, int )							/*!< свойство: уменьшать высоту виджета до...(в пикселях) */
		ADD_PROPERTY( prop_XMax, int )							/*!< свойство: увеличивать ширину виджета до...(в пикселях) */
		ADD_PROPERTY( prop_YMax, int )							/*!< свойство: увеличивать высоту виджета до...(в пикселях) */
		ADD_PROPERTY( prop_expand_type, ExpandDirectionType )	/*!< свойство: направление увеличения виджета */
		ADD_PROPERTY( prop_consumer_widget_1, Gtk::Widget* )	/*!< свойство: доп. виджет-инициатор разворота (увеличения) */
		ADD_PROPERTY( prop_consumer_widget_2, Gtk::Widget* )	/*!< свойство: доп. виджет-инициатор разворота (увеличения) */
		ADD_PROPERTY( prop_consumer_widget_3, Gtk::Widget* )	/*!< свойство: доп. виджет-инициатор разворота (увеличения) */
		ADD_PROPERTY( prop_consumer_widget_4, Gtk::Widget* )	/*!< свойство: доп. виджет-инициатор разворота (увеличения) */
		ADD_PROPERTY( prop_consumer_widget_5, Gtk::Widget* )	/*!< свойство: доп. виджет-инициатор разворота (увеличения) */
		ADD_PROPERTY( prop_consumer_minimize_widget, Gtk::Widget* )	/*!< свойство: доп. виджет-инициатор сворачивания (уменьшения) */
		ADD_PROPERTY( prop_maximize_delay, int )				/*!< свойство: задержка перед разворачиванием (увеличением) виджета */
		ADD_PROPERTY( prop_minimize_delay, int )				/*!< свойство: задержка перед сворачиванием (уменьшением) виджета */
		ADD_PROPERTY( prop_resize_full_time, int )				/*!< свойство: период (полное время) изменения размера от минимального до максимального */
		
		DISALLOW_COPY_AND_ASSIGN(UPopupBox);
};
// -------------------------------------------------------------------------
template<>
inline GType UObj_Get_Type<UPopupBox>()
{
	static GType gtype = 0;
	if (gtype)
		return gtype;

	UPopupBox* dummy = new UPopupBox();
	GtkContainerClass* container_klass = GTK_CONTAINER_GET_CLASS( dummy->gobj() );

	gtype = G_OBJECT_TYPE(dummy->gobj());
	delete( dummy );
	Glib::wrap_register(gtype, &UObj_Wrap_New<UPopupBox>);

	return gtype;
}
namespace Glib
{
	template <>
	class Value<UPopupBox::ExpandDirectionType> : public Value_Enum<UPopupBox::ExpandDirectionType>
	{
	public:
		static GType value_type() G_GNUC_CONST;
	};
} // namespace Glib
#endif
