#include "UPrinterInterface.h"
#include <fstream>
#include <cstring>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/lp.h>
#include <errno.h>
#include "types.h"
// -------------------------------------------------------------------------
#define PRINTER_DEV			"printer-dev"
#define PRINTER_LOCALE			"printer-locale"
#define TEXT_LOCALE			"text-locale"
#define PRINTER_CHECK_TIME		"printer-check-time"
#define SENSOR_PRINTER_ERROR		"printer-error"
#define PRINTER_NODE			"node"
// -------------------------------------------------------------------------
#define INIT_PRINTER_PROPERTIES() \
	property_sensor_printer_error(*this, SENSOR_PRINTER_ERROR , UniWidgetsTypes::DefaultObjectId) \
	,node(*this, PRINTER_NODE , UniWidgetsTypes::DefaultObjectId) \
	,property_printer_dev(*this, PRINTER_DEV,    "/dev/usb/lp0") \
	,property_printer_locale(*this, PRINTER_LOCALE, "CP866" ) \
	,property_text_locale(*this, TEXT_LOCALE, "KOI8-R" ) \
	,property_printer_check_time(*this, PRINTER_CHECK_TIME,0)
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using namespace UniWidgets;
// -------------------------------------------------------------------------
void
UPrinterInterface::constructor()
{

	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback_object<UPrinterInterface>;

	printer_status = (Status)1;
}
// -------------------------------------------------------------------------
UPrinterInterface::UPrinterInterface():
	Glib::ObjectBase("uprinterinterface")
	,INIT_PRINTER_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
UPrinterInterface::UPrinterInterface(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,INIT_PRINTER_PROPERTIES()
{
	constructor();
}
// -------------------------------------------------------------------------
UPrinterInterface::~UPrinterInterface()
{
}
// -------------------------------------------------------------------------
void
UPrinterInterface::on_connect() throw()
{
	UEventBox::on_connect();

	printer_check();
	if (property_printer_check_time > 0)
		Glib::signal_timeout().connect(sigc::mem_fun(*this, &UPrinterInterface::printer_check), get_property_printer_check_time());
}
// -------------------------------------------------------------------------
UPrinterInterface::Status
UPrinterInterface::getStatus()
{
	int fd = open( get_property_printer_dev().c_str(), O_RDONLY );
	if( fd < 0 )
	{
		if(printer_status != P_UNKNOWN)
			cerr << ": Unable to open device " << property_printer_dev << " with err: " << strerror(errno) << endl;
		return P_UNKNOWN;
	}
	int status;
	if( ioctl(fd, LPGETSTATUS, &status) != 0 )
	{
		cerr << ": Unable to get status " << strerror(errno) << endl;
		close(fd);
		return P_UNKNOWN;
	}

	switch(status)
	{
		case ML280_OK:
		case ML280_OUT_PAPER:
		case ML280_BUSY:
		break;

		default:
			status = P_UNKNOWN;
			break;
	}

	close(fd);
	return (Status)status;
}
// -------------------------------------------------------------------------
bool
UPrinterInterface::print_string(const Glib::ustring& text )
{
	printer_check();
	if( printer_status != UPrinterInterface::P_OK )
		return false;
	if( text.empty() ) {
		cerr << "(UPrinterInterface): ������� ���������� ������ ������ " << endl; 
		return false;
	}

	fstream fout( get_property_printer_dev().c_str(), ios::out );
	if( !fout ) {
		cerr << "(UPrinterInterface): �� ���� ������� " << property_printer_dev
			<< " ��������: << " << text << " >>" << endl;
		return false;
	}

	try {
		fout << Glib::convert(text, get_property_printer_locale(), get_property_text_locale()) << endl;
	}
	catch (const Glib::Error& er){
		cerr << "(UPrinterInterface):(ConverError):" <<  er.what() << endl;
	}

	fout.close();
	return true;
}
// -------------------------------------------------------------------------
bool
UPrinterInterface::printer_check()
{
	const Status new_status = getStatus();
	
	if (printer_status == new_status )
		return true;
	
	printer_status = new_status;
	if (!get_connector() || !get_connector()->connected())
		return true;

	const bool error = printer_status != P_OK;
	
	get_connector()->save_value(error,get_property_sensor_printer_error(),get_node());

//     if ( printer_status == UPrinterInterface::P_OK ) {
//             sensor_printer_ok.save_value(1);
//             sensor_printer_busy.save_value(0);
//             sensor_printer_out_paper.save_value(0);
//     }
//     else if ( printer_status != UPrinterInterface::P_BUSY ) {
//             sensor_printer_ok.save_value(0);
//             sensor_printer_busy.save_value(1);
//             sensor_printer_out_paper.save_value(0);
//     }
//     else if ( printer_status != UPrinterInterface::P_BUSY ) {
//             sensor_printer_ok.save_value(0);
//             sensor_printer_busy.save_value(0);
//             sensor_printer_out_paper.save_value(1);
//     }
       return true;
}
// -------------------------------------------------------------------------
