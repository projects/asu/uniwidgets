#ifndef UPrinterInterface_H_
#define UPrinterInterface_H_
// -------------------------------------------------------------------------
#include <glibmm/ustring.h>
#include <time.h>
#include <gdkmm.h>
#include <glibmm/main.h>
#include <glibmm/convert.h>
#include "UEventBox.h"
#include "global_macros.h"
#include "plugins.h"
// -------------------------------------------------------------------------
#define ML280_OK 0x18
#define ML280_BUSY 0x08 	// NOT READY
#define ML280_OUT_PAPER 0x20
// -------------------------------------------------------------------------
namespace UniWidgets
{
/*!
 * \brief Класс виджета для работы с принтером.
 * \par
 * Класс предназначен для реализации виджета принтер. Через интерфейс данного
 * класса другие виджеты могут выводить данные для печати на принтере.
*/
class UPrinterInterface : public UEventBox
{
public:

	UPrinterInterface();
	explicit UPrinterInterface(GtkmmBaseType::BaseObjectType* gobject);
	~UPrinterInterface();

	/*! состояние принтера */
	enum Status
	{
		P_UNKNOWN 	= -1,			/*!< неопределенное состояние */
		P_OK 		= ML280_OK,		/*!< принтер готов */
		P_BUSY 		= ML280_BUSY,		/*!< принтер занят */
		P_OUTPAPER 	= ML280_OUT_PAPER	/*!< в принтере закончилась бумага */
	};
	/*! получить состояние принтера */
	Status getStatus();
	/*! печать текстовой строки */
	virtual bool print_string(const Glib::ustring& text);
	/*! проверка готовности принтера иначе выставляется соответствующий АПС сигнал об неисправности принтера */
	bool printer_check();
private:
	void constructor();
protected:
	virtual void on_connect() throw();
	Status printer_status;							/*!< состояние принтера */
	/*Properties*/
	ADD_PROPERTY( property_sensor_printer_error, UniWidgetsTypes::ObjectId )	/*!< свойство: id датчика для отображения состояния принтера */
	ADD_PROPERTY( node, UniWidgetsTypes::ObjectId )				/*!< свойство: id узла датчика */
	ADD_PROPERTY( property_printer_dev, Glib::ustring )			/*!< свойство: путь к устройству принтера */
	ADD_PROPERTY( property_printer_locale, Glib::ustring )			/*!< свойство: кодировка принтера */
	ADD_PROPERTY( property_text_locale, Glib::ustring )			/*!< свойство: кодировка текста отправляемого на печать */
	ADD_PROPERTY( property_printer_check_time, long )			/*!< свойство: интервалы проверки состояния принтера */
};

}
#endif 
