#include <iostream>
#include "UProxyWidget.h"
// -------------------------------------------------------------------------
const int POLL_TIME = 100;
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void UProxyWidget::ctor()
{
	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback<UProxyWidget>;

	connect_property_changed("module-name", sigc::mem_fun(*this, &UProxyWidget::on_module_name_changed));
	connect_property_changed("manager-name", sigc::mem_fun(*this, &UProxyWidget::on_module_name_changed));
	connect_property_changed("alive-sensor", sigc::mem_fun(*this, &UProxyWidget::on_module_name_changed));
	connect_property_changed("confirm-sensor", sigc::mem_fun(*this, &UProxyWidget::on_module_name_changed));
	connect_property_changed("auto-confirm-time-str", sigc::mem_fun(*this, &UProxyWidget::on_module_name_changed));
}
// -------------------------------------------------------------------------
UProxyWidget::UProxyWidget() :
	Glib::ObjectBase("uproxywidget")
	,property_module_(*this,"module-name")
	,property_manager_name_(*this,"manager-name")
	,property_alive_sensor_(*this,"alive-sensor")
	,property_confirm_sensor_(*this,"confirm-sensor")
	,property_auto_confirm_time_str_(*this,"auto-confirm-time-str")
	,property_connect_(*this, "connect", false)
{
	ctor();
}
// -------------------------------------------------------------------------
UProxyWidget::UProxyWidget(const Glib::ustring& module_name, const Glib::ustring& manager_name
								,const Glib::ustring& alive_sensor
								,const Glib::ustring& confirm_sensor
								,const Glib::ustring& auto_confirm_time_str) :
	Glib::ObjectBase("uproxywidget")
	,property_module_(*this,"module-name")
	,property_manager_name_(*this,"manager-name")
	,property_alive_sensor_(*this,"alive-sensor")
	,property_confirm_sensor_(*this,"confirm-sensor")
	,property_auto_confirm_time_str_(*this,"auto-confirm-time-str")
	,property_connect_(*this, "connect", false)
{
	property_module_ = module_name;
	property_manager_name_ = manager_name;
	property_alive_sensor_ = alive_sensor;
	property_confirm_sensor_ = confirm_sensor;
	property_auto_confirm_time_str_ = auto_confirm_time_str;
	on_module_name_changed();
	ctor();
}
// -------------------------------------------------------------------------
UProxyWidget::UProxyWidget(GtkmmBaseType::BaseObjectType* gobject) :
	UContainer(gobject)
	,property_module_(*this,"module-name")
	,property_manager_name_(*this,"manager-name")
	,property_alive_sensor_(*this,"alive-sensor")
	,property_confirm_sensor_(*this,"confirm-sensor")
	,property_auto_confirm_time_str_(*this,"auto-confirm-time-str")
	,property_connect_(*this, "connect", false)
{
	ctor();
}
// -------------------------------------------------------------------------
UProxyWidget::~UProxyWidget()
{
}
// -------------------------------------------------------------------------
void
UProxyWidget::init_widget()
{
}
// -------------------------------------------------------------------------
void
UProxyWidget::on_module_name_changed()
{
	ConnectorRef old_connector = get_connector();
	ConnectorRef new_connector = ConnectorRef();

	//if (property_module_ != "uniset")
	//	return;

	try {
		new_connector = Connector::create_connector(property_module_, property_manager_name_
										,property_alive_sensor_
										,property_confirm_sensor_
										,property_auto_confirm_time_str_);
	}
	catch (UniWidgetsTypes::Exception& ex) {
		cerr << ex << endl;
	}
	catch (...) {
		cerr << "error creating connector" << endl;
	}
	
	set_connector(new_connector);
	set_connector_to_hierarchy(this,new_connector);
	
	if ( old_connector )
		Connector::destroy_connector(old_connector);
}
// -------------------------------------------------------------------------
void UProxyWidget::on_add(Gtk::Widget* w)
{
	BaseType::on_add(w);
	set_connector_to_hierarchy(this,get_connector());
}
// -------------------------------------------------------------------------
