#ifndef _UPROXYWIDGET_H
#define _UPROXYWIDGET_H
// -------------------------------------------------------------------------
#include "UContainer.h"
#include "plugins.h"
// -------------------------------------------------------------------------
/*!
 * \brief Класс контейнер.
 * \par
 * Класс контейнер обеспечивает связь дочерних виджетов с  SharedMemory. Виджет
 * является контейнером верхнего уровня, в нем создается коннектор к SharedMemory
 * и производится активация дочерних виджетов.
*/
class UProxyWidget : public UContainer{
private:
public:
	UProxyWidget();

	/*! конструктор с параметрами для создания коннектора (см.Connector::create_connector) */
	UProxyWidget(const Glib::ustring& module, const Glib::ustring& manager_name
						,const Glib::ustring& alive_sensor
						,const Glib::ustring& confirm_sensor
						,const Glib::ustring& auto_confirm_time_str);

	explicit UProxyWidget(GtkmmBaseType::BaseObjectType* gobject);
	~UProxyWidget();
	virtual void init_widget();
	
	/*! получить имя модуля */
	Glib::PropertyProxy<Glib::ustring> property_module();

	/*! получить имя модуля(только для чтения) */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_module() const;

	/*! получить имя объекта */
	Glib::PropertyProxy<Glib::ustring> property_manager_name();

	/*! получить имя объекта(только для чтения) */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_manager_name() const;

	/*! получить id датчика "живости" SharedMemory */
	Glib::PropertyProxy<Glib::ustring> property_alive_sensor();

	/*! получить id датчика "живости" SharedMemory(только для чтения) */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_alive_sensor() const;

	/*! получить id датчика квитирования АПС сигналов */
	Glib::PropertyProxy<Glib::ustring> property_confirm_sensor();

	/*! получить id датчика квитирования АПС сигналов(только для чтения) */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_confirm_sensor() const;

	/*! получить время автоматического квитирования АПС сигналов */
	Glib::PropertyProxy<Glib::ustring> property_auto_confirm_time_str();

	/*! получить время автоматического квитирования АПС сигналов(только для чтения) */
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_auto_confirm_time_str() const;

protected:
	//We don't need some actions that done in UEventBox
	virtual void on_parent_changed(Gtk::Widget* prev_parent)
		{ Gtk::EventBox::on_parent_changed(prev_parent); }
	virtual void on_hierarchy_changed(Gtk::Widget* prev_top)
		{ Gtk::EventBox::on_hierarchy_changed(prev_top); }
	sigc::connection connPoll;

	Glib::Property<Glib::ustring> property_module_;				/*!< свойство: название модуля(на данный момент используется только "uniset") */
	Glib::Property<Glib::ustring> property_manager_name_;			/*!< свойство: имя объекта(Описывается в секции <objects ... > configure.xml ) */
	Glib::Property<Glib::ustring> property_alive_sensor_;			/*!< свойство: id датчика "живости" SharedMemory */
	Glib::Property<Glib::ustring> property_confirm_sensor_;			/*!< свойство: id датчика квитирования АПС сигналов */
	Glib::Property<Glib::ustring> property_auto_confirm_time_str_;		/*!< свойство: время автоматического квитирования АПС сигналов(если "0",то квитируются сигналы вручную) */
	Glib::Property<bool> property_connect_;

	virtual void on_add(Gtk::Widget* w);

private:
	void on_module_name_changed();
	void ctor();
};
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<Glib::ustring>
UProxyWidget::property_module()
{
	return property_module_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
UProxyWidget::property_module() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"module-name");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<Glib::ustring>
UProxyWidget::property_manager_name()
{
	return property_manager_name_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
UProxyWidget::property_manager_name() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"manager-name");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<Glib::ustring>
UProxyWidget::property_alive_sensor()
{
	return property_alive_sensor_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
UProxyWidget::property_alive_sensor() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"alive-sensor");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<Glib::ustring>
UProxyWidget::property_confirm_sensor()
{
	return property_confirm_sensor_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
UProxyWidget::property_confirm_sensor() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"confirm-sensor");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<Glib::ustring>
UProxyWidget::property_auto_confirm_time_str()
{
	return property_auto_confirm_time_str_.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<Glib::ustring>
UProxyWidget::property_auto_confirm_time_str() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"auto-confirm-time-str");
}
#endif
