/*
класс реализующий инерцию изменения аналогового значения
*/
#include "UQuantization.h"
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

// -------------------------------------------------------------------------
UQuantization::UQuantization(Gtk::Widget* w) :
owner(w)
,prop_enable(*w,"enable-quantization",false)
,prop_countOfQuant(*w,"count-of-quant",100)
,prop_round(*w,"round",true)
,value(0)
,min_value(0)
,max_value(100)
{
}
// -------------------------------------------------------------------------
UQuantization::~UQuantization()
{
}
// -------------------------------------------------------------------------
double UQuantization::get_value(double val)
{
	if(!prop_enable)
		return val;

	int countOfQuant;
	if(val>max_value)
		value = max_value;
	else if(val<min_value)
		value = min_value;
	else
	{
		countOfQuant = get_prop_countOfQuant()>1 ? get_prop_countOfQuant():1;
		if(prop_round)
			value = lround((val - min_value)/((max_value - min_value)/countOfQuant))*((max_value - min_value)/countOfQuant) + min_value;
		else
			value = (long((val - min_value)/((max_value - min_value)/countOfQuant)))*((max_value - min_value)/countOfQuant) + min_value;
	}
//	cout<<owner->get_name()<<"::get_value value="<<value<<" val="<<val<<" countOfQuant="<<countOfQuant<<" min_value="<<min_value<<" max_value="<<max_value<<endl;
	return value;
}
// -------------------------------------------------------------------------
double UQuantization::get_value(double min, double max, double val)
{
//	cout<<owner->get_name()<<"::get_value value="<<value<<" min="<<min<<" max="<<max<< endl;
	set_range(min, max);
	return get_value(val);
}
// -------------------------------------------------------------------------
