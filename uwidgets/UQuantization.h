/*
	класс реализующий квантование аналогового значения
*/

#ifndef UQUANTIZATION_H
#define UQUANTIZATION_H
#include <gtkmm.h>
#include <global_macros.h>

class UQuantization : public Glib::Object
{
	public:
		UQuantization(Gtk::Widget* w);
		virtual ~UQuantization();

		virtual double get_value(double val);								/* */
		virtual double get_value(double min, double max, double val);		/* */
		inline double get_value()
		{
			return value;
		}
		inline void set_min(double min)
		{
			min_value = min;
			if(value<min)
				value = min;
		}
		inline void set_max(double max)
		{
			max_value = max;
			if(value>max)
				value = max;
		}
		inline void set_range(double min, double max)
		{
			set_min(min);
			set_max(max);
		}
		
		inline Glib::PropertyProxy<bool> property_enable()
		{
			return prop_enable.get_proxy();
		}
		inline Glib::PropertyProxy_ReadOnly<bool> property_enable() const
		{
			return Glib::PropertyProxy_ReadOnly<bool>(this,"enable-quantization");
		}

		inline Glib::PropertyProxy<int> property_count_of_quant()
		{
			return prop_countOfQuant.get_proxy();
		}
		inline Glib::PropertyProxy_ReadOnly<int> property_count_of_quant() const
		{
			return Glib::PropertyProxy_ReadOnly<int>(this,"count-of-quant");
		}

		inline Glib::PropertyProxy<bool> property_round()
		{
			return prop_round.get_proxy();
		}
		inline Glib::PropertyProxy_ReadOnly<bool> property_round() const
		{
			return Glib::PropertyProxy_ReadOnly<bool>(this,"round");
		}
		
	protected:

		Gtk::Widget* owner;
		
		double value;
		double min_value;
		double max_value;

		ADD_PROPERTY( prop_enable, bool )			/*!< свойство: использовать ли квантование*/
		ADD_PROPERTY( prop_countOfQuant, int )		/*!< свойство: количество квантов*/
		ADD_PROPERTY( prop_round, bool )			/*!< свойство: округление при квантовании*/

	private:
};
#endif
