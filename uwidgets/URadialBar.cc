#include "types.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <gdkmm.h>
#include <librsvg/rsvg.h>
//#include <librsvg/rsvg-cairo.h>
#include "URadialBar.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using Glib::ustring;
// -------------------------------------------------------------------------
#define URadialBar_INIT_PROPERTIES() \
    prop_externalBar(*this,"external-bar",true) \
    ,prop_enableBarThreshold(*this,"enable-bar-threshold",true) \
    ,prop_useThresholdGlossColor(*this,"use-bar-threshold-gloss-color",false) \
    ,prop_barWidth(*this,"bar-width",10) \
    ,prop_barColor(*this, "bar-color" , Gdk::Color("black")) \
    ,prop_useGloss(*this,"use-gloss",true) \
    ,prop_glossColor(*this, "gloss-color" , Gdk::Color("white")) \
    ,prop_posGlossGradient(*this, "position-gloss-gradient" , 0.33) \
    ,prop_glossTransparent(*this, "gloss-transparent" , 0.80) \
    \
    ,prop_strokeWidth(*this,"stroke-width", 0) \
    ,prop_strokeColor1(*this, "stroke-color-1" , Gdk::Color("black")) \
    ,prop_strokeColor2(*this, "stroke-color-2" , Gdk::Color("black")) \
    \
    ,prop_useImage(*this,"use-image",false) \
    ,prop_image(*this,"image","") \
    ,prop_hiWarningImage(*this,"hi-warning-image","") \
    ,prop_hiAlarmImage(*this,"hi-alarm-image","") \
    ,prop_loWarningImage(*this,"lo-warning-image","") \
    ,prop_loAlarmImage(*this,"lo-alarm-image","") \
    ,prop_cropImage(*this,"crop-image",true) \
    \
    ,prop_enableCentralLabel(*this,"enable-central-label",true) \
    ,prop_enableCentralLabelThreshold(*this,"enable-central-label-threshold",true) \
    ,prop_centralLabelUnits(*this, "central-label-units", "%" ) \
    ,prop_centralLabelColor(*this, "central-label-color", Gdk::Color("black") ) \
    ,prop_centralLabelFont(*this, "central-label-font", "Liberation Sans Bold" ) \
    ,prop_centralLabelFontSize(*this, "central-label-font-size", 12 ) \

// -------------------------------------------------------------------------
void URadialBar::ctor()
{
    set_visible_window(false);

	connect_property_changed("external-bar", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("bar-width", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("bar-color", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("use-gloss", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("gloss-color", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("position-gloss-gradient", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("gloss-transparent", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	
	connect_property_changed("enable-central-label", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("central-label-units", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("central-label-color", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("central-label-font", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("central-label-font-size", sigc::mem_fun(*this, &URadialBar::queue_draw) );

	connect_property_changed("use-image", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("image", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("hi-warning-image", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("hi-alarm-image", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("lo-warning-image", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	connect_property_changed("lo-alarm-image", sigc::mem_fun(*this, &URadialBar::queue_draw) );
	
//	on_value_changed();
    show_all();
}
// -------------------------------------------------------------------------
URadialBar::URadialBar() :
    Glib::ObjectBase("uradialbar")
	,URadialValueIndicator()
	,URadialBar_INIT_PROPERTIES()
{
    ctor();
}
// -------------------------------------------------------------------------
URadialBar::URadialBar(GtkmmBaseType::BaseObjectType* gobject) :
    URadialValueIndicator(gobject)
    ,URadialBar_INIT_PROPERTIES()
{
    ctor();
}
// -------------------------------------------------------------------------
void URadialBar::calculate_circle_param()
{
	URadialValueIndicator::calculate_circle_param();
}
// -------------------------------------------------------------------------
void URadialBar::calculate_circle_param_scale_offset()
{
	URadialValueIndicator::calculate_circle_param_scale_offset();
}
// -------------------------------------------------------------------------
void URadialBar::calculate_circle_param_label_offset()
{
	URadialValueIndicator::calculate_circle_param_label_offset();
}
// -------------------------------------------------------------------------
void URadialBar::calculate_circle_param_bar_offset()
{
	// вычисление поправки велечину радиуса дуги если индикатор рисуются наружу
	if(prop_externalBar)
	{
		R = R - get_prop_barWidth();
	}
}
// -------------------------------------------------------------------------
bool URadialBar::on_expose_event(GdkEventExpose* event)
{
#if GTK_VERSION_GE(2,20)
    if(!get_mapped())
#else
    if(!is_show)
#endif
        return false;

    Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
    Gtk::Allocation alloc = get_allocation();
    Gdk::Cairo::rectangle(cr,alloc);
    cr->clip();
    cr->translate(alloc.get_x(), alloc.get_y());

    calculate_circle_param();
	// вычисление поправки велечину радиуса дуги если деления дуги рисуются наружу
	calculate_circle_param_scale_offset();
	// вычисление поправки велечину радиуса дуги если индикатор снаружи
	calculate_circle_param_bar_offset();
	// вычисление поправки велечину радиуса дуги если подписи шкалы находятся снаружи
	calculate_circle_param_label_offset();
	
	//ресуем круговой индикатор
	draw_bar(cr,alloc);
	//красим сектора пороговых значений
    draw_threshold(cr,alloc);
    //чертим шкалу
    draw_scale(cr,alloc);
	//ресуем значение в центре
	draw_central_label(cr,alloc);
	
    if ( !connected_ && get_property_disconnect_effect() == 1)
    {
        alloc.set_x(0);
        alloc.set_y(0);
        UVoid::draw_disconnect_effect_1(cr, alloc);
    }

    return true;
}
// -------------------------------------------------------------------------
void URadialBar::draw_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	URadialValueIndicator::draw_scale(cr,alloc);
}
// -------------------------------------------------------------------------
void URadialBar::draw_threshold(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	URadialValueIndicator::draw_threshold(cr,alloc);
}
// -------------------------------------------------------------------------
void URadialBar::draw_bar(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	double cur_angle;
	if(prop_use_arrow_inert)
		cur_angle = temp_value_angle;
	else
		cur_angle = value_angle;
	cur_angle = uquant.get_value(maxDeg>minDeg? minDeg:(minDeg-2*M_PI),maxDeg,cur_angle);
	
	double prec_value=0;
	if(prop_enableBarThreshold)
	{
		if(prop_invert_rotate)
			prec_value = (prop_Max.get_value()-prop_Min.get_value())*(maxDeg - cur_angle)/angle/(M_PI/180) + prop_Min.get_value();
		else
			prec_value = (prop_Max.get_value()-prop_Min.get_value())*(cur_angle - minDeg)/angle/(M_PI/180) + prop_Min.get_value();
	}
	
	if(prop_useImage)
	{
		cr->save();
		cr->translate(pos_x, pos_y);
		// рисуем путь обрисовки индикатора, который послужит маской для картини
		cr->begin_new_sub_path();
		double R_for_bar;
		if(prop_externalBar)
		{
			if( get_prop_scaleMinor_height()<0 || get_prop_scaleMajor_height()<0)
				R_for_bar = R + get_prop_barWidth() - MIN(get_prop_scaleMinor_height(),get_prop_scaleMajor_height());
			else
				R_for_bar = R + get_prop_barWidth();
		}
		else
		{
			if( get_prop_scaleMinor_height()>0 || get_prop_scaleMajor_height()>0)
				R_for_bar = R - MAX(get_prop_scaleMinor_height(),get_prop_scaleMajor_height());
			else
				R_for_bar = R;
		}
		if(prop_invert_rotate)
		{
			cr->arc( 0, 0, R_for_bar, cur_angle - M_PI, maxDeg - M_PI );
			if(prop_cropImage)
				cr->arc_negative( 0, 0, R_for_bar - get_prop_barWidth(), maxDeg - M_PI, cur_angle - M_PI );
			else
				cr->arc( 0, 0, R_for_bar - get_prop_barWidth(), maxDeg - M_PI, cur_angle - M_PI );
		}
		else
		{
			cr->arc( 0, 0, R_for_bar, minDeg - M_PI, cur_angle - M_PI );
			if(prop_cropImage)
				cr->arc_negative( 0, 0, R_for_bar - get_prop_barWidth(), cur_angle - M_PI, minDeg - M_PI );
			else
				cr->arc( 0, 0, R_for_bar - get_prop_barWidth(), cur_angle - M_PI, minDeg - M_PI );
		}
		cr->close_path();
		cr->clip();

		//рисуем картинку
		ustring imagefile;
		if(prop_enableBarThreshold)
		{
			if (property_hi_alarm_on_ && prec_value >= property_hi_alarm_)
				imagefile = get_prop_hiAlarmImage().empty()? get_prop_image():get_prop_hiAlarmImage();
			else if (property_hi_warning_on_ && prec_value >= property_hi_warning_)
				imagefile = get_prop_hiWarningImage().empty()? get_prop_image():get_prop_hiWarningImage();
			else if (property_lo_alarm_on_ && prec_value <= property_lo_alarm_)
				imagefile = get_prop_loAlarmImage().empty()? get_prop_image():get_prop_loAlarmImage();
			else if (property_lo_warning_on_ && prec_value <= property_lo_warning_)
				imagefile = get_prop_loWarningImage().empty()? get_prop_image():get_prop_loWarningImage();
			else
				imagefile = get_prop_image();
		}
		else
			imagefile = get_prop_image();
		
		Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(imagefile, 2*R_for_bar, 2*R_for_bar);
		Gdk::Cairo::set_source_pixbuf(cr,image, -R_for_bar, -R_for_bar);
		cr->paint();
		cr->restore();
	}
	else
	{
		cr->save();
		cr->translate(pos_x, pos_y);
		// рисуем путь индикатора
		cr->begin_new_sub_path();
		double strokeWidth = get_prop_strokeWidth()>0 ? get_prop_strokeWidth():0;
		cr->set_line_width(strokeWidth);
		double R_for_arc;
		double R_for_arc_negative;
		if(prop_externalBar)
		{
			if( get_prop_scaleMinor_height()<0 || get_prop_scaleMajor_height()<0)
				R_for_arc = R + get_prop_barWidth() - MIN(get_prop_scaleMinor_height(),get_prop_scaleMajor_height());
			else
				R_for_arc = R + get_prop_barWidth();
		}
		else
		{
			if( get_prop_scaleMinor_height()>0 || get_prop_scaleMajor_height()>0)
				R_for_arc = R - MAX(get_prop_scaleMinor_height(),get_prop_scaleMajor_height());
			else
				R_for_arc = R;
		}
		R_for_arc = R_for_arc - strokeWidth/2;
		R_for_arc_negative = R_for_arc - get_prop_barWidth() + get_prop_strokeWidth();
		double stroke_delta = strokeWidth/2/R_for_arc;
		stroke_delta = strokeWidth/2/R_for_arc_negative;
		if(prop_invert_rotate)
		{
			cr->arc( 0, 0, R_for_arc, cur_angle - M_PI - stroke_delta, maxDeg - M_PI + stroke_delta);
			cr->arc_negative( 0, 0, R_for_arc_negative, maxDeg - M_PI + stroke_delta, cur_angle - M_PI - stroke_delta);
		}
		else
		{
			cr->arc( 0, 0, R_for_arc, minDeg - M_PI + stroke_delta, cur_angle - M_PI - stroke_delta);
			cr->arc_negative( 0, 0, R_for_arc_negative, cur_angle - M_PI - stroke_delta, minDeg - M_PI + stroke_delta);
		}
		cr->close_path();
		Cairo::Path *bar = cr->copy_path();

		Gdk::Color color;
		if(prop_enableBarThreshold)
		{
			if (property_hi_alarm_on_ && prec_value >= property_hi_alarm_)
				color = property_hi_alarm_color_1;
			else if (property_hi_warning_on_ && prec_value >= property_hi_warning_)
				color = property_hi_warning_color_1;
			else if (property_lo_alarm_on_ && prec_value <= property_lo_alarm_)
				color = property_lo_alarm_color_1;
			else if (property_lo_warning_on_ && prec_value <= property_lo_warning_)
				color = property_lo_warning_color_1;
			else
				color = prop_barColor;
		}
		else
			color = prop_barColor;
		
		Gdk::Cairo::set_source_color(cr, color);
		cr->fill();
		
		// рисуем глянец на индикаторе
		if(prop_useGloss)
		{
			Cairo::RefPtr<Cairo::RadialGradient> gradient;
			gradient = Cairo::RadialGradient::create(0, 0, R_for_arc, 0, 0, R_for_arc_negative);

			if(prop_enableBarThreshold && prop_useThresholdGlossColor)
			{
				if (property_hi_alarm_on_ && prec_value >= property_hi_alarm_)
					color = property_hi_alarm_color_2;
				else if (property_hi_warning_on_ && prec_value >= property_hi_warning_)
					color = property_hi_warning_color_2;
				else if (property_lo_alarm_on_ && prec_value <= property_lo_alarm_)
					color = property_lo_alarm_color_2;
				else if (property_lo_warning_on_ && prec_value <= property_lo_warning_)
					color = property_lo_warning_color_2;
				else
					color = prop_glossColor;
			}
			else
				color = prop_glossColor;
			
			double pos_grad = prop_posGlossGradient.get_value();
			pos_grad = pos_grad>1? 1:(pos_grad<0? 0:pos_grad);
			gradient->add_color_stop_rgba(0, color.get_red_p(), color.get_green_p(), color.get_blue_p(),0.0);
			gradient->add_color_stop_rgba(pos_grad, color.get_red_p(), color.get_green_p(), color.get_blue_p(),get_prop_glossTransparent());
			gradient->add_color_stop_rgba(1.0, color.get_red_p(), color.get_green_p(), color.get_blue_p(),0.0);
			cr->set_source(gradient);

			cr->append_path(*bar);
			cr->fill();
		}

		// рисуем обводку индикатора
		Cairo::RefPtr<Cairo::RadialGradient> gradient;
		gradient = Cairo::RadialGradient::create(0, 0, R_for_arc, 0, 0, R_for_arc_negative);
		
		gradient->add_color_stop_rgba(0, get_prop_strokeColor1().get_red_p(), get_prop_strokeColor1().get_green_p(), get_prop_strokeColor1().get_blue_p(),1.0);
		gradient->add_color_stop_rgba(1.0, get_prop_strokeColor2().get_red_p(), get_prop_strokeColor2().get_green_p(), get_prop_strokeColor2().get_blue_p(),1.0);
		cr->set_source(gradient);
		cr->append_path(*bar);
		cr->stroke();
		
		cr->restore();
	}
}
// -------------------------------------------------------------------------
void URadialBar::draw_central_label(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	if(!prop_enableCentralLabel)
		return;
	
	double cur_angle;
	if(prop_use_arrow_inert)
		cur_angle = temp_value_angle;
	else
		cur_angle = value_angle;
	cur_angle = uquant.get_value(maxDeg>minDeg? minDeg:(minDeg-2*M_PI),maxDeg,cur_angle);
	
	double prec_value;
	if(prop_invert_rotate)
		prec_value = (prop_Max.get_value()-prop_Min.get_value())*(maxDeg - cur_angle)/angle/(M_PI/180) + prop_Min.get_value();
	else
		prec_value = (prop_Max.get_value()-prop_Min.get_value())*(cur_angle - minDeg)/angle/(M_PI/180) + prop_Min.get_value();
	
	std::ostringstream title;
	title<<(prec_value>=0? "":"-");
	prec_value = fabs(prec_value);
	if(property_fill_digits>0)
		title<<std::setw(property_fill_digits)<<setfill('0')<<(long)prec_value;
	else
		title<<(long)prec_value;
	if(property_digits>0)
		title<<","<<std::setw(property_digits)<<setfill('0')<<(long)(fabs(((float)prec_value-(long)prec_value )*pow10(property_digits)));
	title<<get_prop_centralLabelUnits();
	
	Glib::RefPtr<Pango::Layout> layout_value;
	layout_value = create_pango_layout("0");
	layout_value->set_alignment(Pango::ALIGN_CENTER);
	layout_value->set_text(title.str());
	
	cr->save();
//	cr->translate(pos_x, pos_y);

	Gdk::Color color;
	if (property_hi_alarm_on_ && prec_value >= property_hi_alarm_)
		color = property_hi_alarm_color_1;
	else if (property_hi_warning_on_ && prec_value >= property_hi_warning_)
		color = property_hi_warning_color_1;
	else if (property_lo_alarm_on_ && prec_value <= property_lo_alarm_)
		color = property_lo_alarm_color_1;
	else if (property_lo_warning_on_ && prec_value <= property_lo_warning_)
		color = property_lo_warning_color_1;
	else
		color = prop_centralLabelColor;
	Gdk::Cairo::set_source_color(cr, color);

	Pango::FontDescription fd(prop_centralLabelFont.get_value());
	fd.set_absolute_size(prop_centralLabelFontSize.get_value() * Pango::SCALE);
	layout_value->set_font_description(fd);
	
	int tw, th;
	layout_value->get_pixel_size(tw,th);

	double text_x = pos_x - ((float)tw)/2;
	double text_y = pos_y - ((float)th)/2;
	if(pos_x + ((float)tw)/2 > W)
		text_x = W - tw;
	else if(pos_x - ((float)tw)/2 < 0)
		text_x = 0;
	if(pos_y + ((float)th)/2 > H)
		text_y = H - th;
	else if(pos_y - ((float)th)/2 < 0)
		text_y = 0;
	
	cr->move_to(text_x, text_y);
	layout_value->add_to_cairo_context(cr);
	cr->fill();
	
	cr->restore();
}
// -------------------------------------------------------------------------
void URadialBar::on_realize()
{
//	cout<<get_name()<<"::on_realize..."<<endl;
	URadialValueIndicator::on_realize();
}
// -------------------------------------------------------------------------
