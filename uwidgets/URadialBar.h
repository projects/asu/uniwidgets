#ifndef _URADIALBAR_H
#define _URADIALBAR_H
// -------------------------------------------------------------------------
#include "URadialValueIndicator.h"
// -------------------------------------------------------------------------
/*!
 * \brief Класс цифрового индикатора.
 * \par
 * Класс предназначен для реализации виджета "цифровой индикатор".
*/
class URadialBar : public URadialValueIndicator
{
public:
	URadialBar();
	explicit URadialBar(GtkmmBaseType::BaseObjectType* gobject);
	~URadialBar() {}

protected:
	virtual void on_realize();
	virtual bool on_expose_event(GdkEventExpose*);
	virtual void calculate_circle_param();
	virtual void calculate_circle_param_scale_offset();
	virtual void calculate_circle_param_label_offset();
	virtual void calculate_circle_param_bar_offset();
	virtual void draw_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_threshold(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_bar(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_central_label(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	
	ADD_PROPERTY( prop_externalBar, bool )     		/*!< свойство: индикатор снаружи шкалы */
	ADD_PROPERTY( prop_enableBarThreshold, bool )	/*!< свойство: отрисовывать пороги на индикаторе*/
	ADD_PROPERTY( prop_useThresholdGlossColor, bool )	/*!< свойство: использовать градиент порогов для цвета глянца*/
	ADD_PROPERTY( prop_barWidth, int )     			/*!< свойство: толщина индикатора */
	ADD_PROPERTY( prop_barColor, Gdk::Color )		/*!< свойство: цвет индикатора*/
	ADD_PROPERTY( prop_useGloss, bool )				/*!< свойство: использовать градиент для заливки цвета */
	ADD_PROPERTY( prop_glossColor, Gdk::Color )		/*!< свойство: цвет глянца*/
	ADD_PROPERTY( prop_posGlossGradient, double )	/*!< свойство: положение градиента глянца (от 0.0 до 1.0)*/
	ADD_PROPERTY( prop_glossTransparent, double )	/*!< свойство: прозрачность градиента глянца (от 0.0 до 1.0)*/
	
	ADD_PROPERTY( prop_strokeWidth, int )			/*!< свойство: ширина обводки индикатора*/
	ADD_PROPERTY( prop_strokeColor1, Gdk::Color )	/*!< свойство: цвет обводки*/
	ADD_PROPERTY( prop_strokeColor2, Gdk::Color )	/*!< свойство: цвет */
	
	ADD_PROPERTY( prop_useImage, bool )					/*!< свойство: использовать картинку*/
	ADD_PROPERTY( prop_image, Glib::ustring )			/*!< свойство: путь к файлу картинки*/
	ADD_PROPERTY( prop_hiWarningImage, Glib::ustring)	/*!< свойство: путь к файлу картинки для верхнего предупредительного уровня*/
	ADD_PROPERTY( prop_hiAlarmImage, Glib::ustring)		/*!< свойство: путь к файлу картинки для верхнего аварийного уровня*/
	ADD_PROPERTY( prop_loWarningImage, Glib::ustring)	/*!< свойство: путь к файлу картинки для нижнего предупредительного уровня*/
	ADD_PROPERTY( prop_loAlarmImage, Glib::ustring)		/*!< свойство: путь к файлу картинки для нижнего аварийного уровня*/
	ADD_PROPERTY( prop_cropImage, bool )				/*!< свойство: обрезать по ширене индикатора картинку*/
	
	ADD_PROPERTY( prop_enableCentralLabel, bool )		/*!< свойство: включить центральную подпись значения индикатора*/
	ADD_PROPERTY( prop_enableCentralLabelThreshold, bool )/*!< свойство: отрисовывать пороги на центральной подписи значения индикатора*/
	ADD_PROPERTY( prop_centralLabelUnits, Glib::ustring )/*!< свойство: единицы измерения центральной подписи значения индикатора*/
	ADD_PROPERTY( prop_centralLabelColor, Gdk::Color )	/*!< свойство: цвет центральной подписи значения индикатора*/
	ADD_PROPERTY( prop_centralLabelFont, Glib::ustring )/*!< свойство: шрифт центральной подписи значения индикатора*/
	ADD_PROPERTY( prop_centralLabelFontSize, int )		/*!< свойство: размер шрифта центральной подписи значения индикатора*/
	
private:
	void ctor();
};
#endif
