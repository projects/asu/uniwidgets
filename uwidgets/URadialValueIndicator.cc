#include "types.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <gdkmm.h>
#include <librsvg/rsvg.h>
//#include <librsvg/rsvg-cairo.h>
#include "URadialValueIndicator.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using Glib::ustring;
// -------------------------------------------------------------------------
#define URadialValueIndicator_INIT_PROPERTIES() \
    sensor_ai_(this,"ai", get_connector()) \
    ,calibrate(this) \
    ,property_value(*this,"value",0) \
    ,property_precision(*this,"precision",0) \
    \
    ,uquant(this) \
    \
    ,prop_passiveMode(*this, "passive-mode",true) \
    ,prop_setValuePermanent(*this, "set-value-permanent",false) \
    \
    ,prop_Min(*this,"min-scale-value",0) \
    ,prop_Max(*this,"max-scale-value",100) \
    ,prop_maxMinor(*this, "max-scale-minor" , 10) \
    ,prop_maxMajor(*this, "max-scale-major" , 10) \
    ,prop_MinDeg(*this,"start-scale-radial-value",0) \
    ,prop_MaxDeg(*this,"end-scale-radial-value",180) \
    ,prop_enableScaleLabel(*this,"enable-scale-label",true) \
    ,prop_scalemark_width(*this,"scale-width",1) \
    ,prop_scaleMinor_height(*this,"scale-minor-height",2) \
    ,prop_scaleMajor_height(*this, "scale-major-height" , 3) \
    ,prop_scaleMinorColor(*this, "scale-minor-color" , Gdk::Color("black")) \
    ,prop_scaleMajorColor(*this, "scale-major-color" , Gdk::Color("red")) \
    \
    ,prop_enableScale(*this,"enable-scale",true) \
    ,property_digits(*this,"scale-label-digits",0) \
    ,property_fill_digits(*this,"scale-label-fill-digits",0) \
    ,prop_scaleLabelColor(*this, "scale-label-color", Gdk::Color("black") ) \
    ,prop_scaleLabelFont(*this, "scale-label-font", "Liberation Sans Bold" ) \
    ,prop_scaleLabelFontSize(*this, "scale-label-font-size", 12 ) \
    ,prop_scaleLabelOffset(*this, "scale-label-offset", 5 ) \
    \
    ,prop_enableArrow(*this,"enable-arrow",true) \
    ,prop_enableArrowThreshold(*this,"enable-arrow-threshold",true) \
    ,prop_arrow_useThresholdGradientColor(*this,"use-arrow-threshold-gradient-color",false) \
    ,prop_arrowOnTop(*this,"arrow-on-top",false) \
    ,prop_ratio_arrowWidth_and_arrowHeight(*this,"ratio-arrowWidth-and-arrowHeight",0.15) \
    ,prop_ratio_arrowHeight_and_arrowPeakHeight(*this,"ratio-arrowHeight-and-arrowPeakHeight",1.0) \
    ,prop_ratio_arrowWidth_and_arrowPeakWidht(*this,"ratio-arrowWidth-and-arrowPeakWidht",0.25) \
    ,prop_ratio_arrowBaseHeight_and_arrowHeight(*this,"ratio-arrowBaseHeigh-and-arrowHeight",0.15) \
    ,prop_arrow_stroke_color(*this, "arrow-stroke-color", Gdk::Color("#444444") ) \
    ,prop_arrow_stroke_width(*this,"arrow-stroke-width",1) \
    ,prop_arrow_fill_color1(*this, "arrow-fill-color1", Gdk::Color("red") ) \
    ,prop_arrow_fill_color2(*this, "arrow-fill-color2", Gdk::Color("black") ) \
    ,prop_arrow_useGradientColor(*this,"use-arrow-gradient-color",true) \
    ,prop_arrow_gradient_color1_pos(*this,"arrow-gradient-color1-pos",0.0) \
    ,prop_arrow_gradient_color2_pos(*this,"arrow-gradient-color2-pos",1.0) \
    \
    ,prop_use_arrow_svgfile(*this,"use-arrow-svgfile",false) \
    ,prop_arrow_svgfile(*this,"arrow-svgfile","") \
    ,prop_hi_warning_svgfile(*this,"hi-warning-arrow-svgfile","") \
    ,prop_hi_alarm_svgfile(*this,"hi-alarm-arrow-svgfile","") \
    ,prop_lo_warning_svgfile(*this,"lo-warning-arrow-svgfile","") \
    ,prop_lo_alarm_svgfile(*this,"lo-alarm-arrow-svgfile","") \
    ,prop_svg_auto_scale(*this,"arrow-svg-auto-scale",false) \
    ,prop_arrow_width(*this,"arrow-width",10) \
    ,prop_arrow_height(*this,"arrow-height",10) \
    ,prop_arrow_rotate_x(*this,"arrow-rotate-center-x-pos",0) \
    ,prop_arrow_rotate_y(*this,"arrow-rotate-center-y-pos",0) \
    \
    ,prop_invert_rotate(*this,"invert-rotate",false) \
    ,prop_use_arrow_inert(*this,"use-arrow-inertia",false) \
    ,inert(this) \
    \
    ,prop_enableScaleThreshold(*this,"enable-scale-threshold",true) \
    ,prop_threshold_useGradientColor(*this,"use-threshold-gradient-color",false) \
    ,prop_threshold_offset_gradient(*this,"threshold-gradient-offset",0.400) \
    \
    ,property_hi_warning_on_(*this,"hi_warning_on",false) \
    ,property_hi_warning_(*this,"hi_warning",0) \
    ,property_hi_warning_color_1(*this,"hi_warning_color1") \
    ,property_hi_warning_color_1_transparent(*this,"hi_warning_color1_transparent",1.0) \
    ,property_hi_warning_color_2(*this,"hi_warning_color2") \
    ,property_hi_warning_color_2_transparent(*this,"hi_warning_color2_transparent",1.0) \
    \
    ,property_hi_alarm_on_(*this,"hi_alarm_on",false) \
    ,property_hi_alarm_(*this,"hi_alarm",0) \
    ,property_hi_alarm_color_1(*this,"hi_alarm_color1") \
    ,property_hi_alarm_color_1_transparent(*this,"hi_alarm_color1_transparent",1.0) \
    ,property_hi_alarm_color_2(*this,"hi_alarm_color2") \
    ,property_hi_alarm_color_2_transparent(*this,"hi_alarm_color2_transparent",1.0) \
    \
    ,property_lo_warning_on_(*this,"lo_warning_on",false) \
    ,property_lo_warning_(*this,"lo_warning",0) \
    ,property_lo_warning_color_1(*this,"lo_warning_color1") \
    ,property_lo_warning_color_1_transparent(*this,"lo_warning_color1_transparent",1.0) \
    ,property_lo_warning_color_2(*this,"lo_warning_color2") \
    ,property_lo_warning_color_2_transparent(*this,"lo_warning_color2_transparent",1.0) \
    \
    ,property_lo_alarm_on_(*this,"lo_alarm_on",false) \
    ,property_lo_alarm_(*this,"lo_alarm",0) \
    ,property_lo_alarm_color_1(*this,"lo_alarm_color1") \
    ,property_lo_alarm_color_1_transparent(*this,"lo_alarm_color1_transparent",1.0) \
    ,property_lo_alarm_color_2(*this,"lo_alarm_color2") \
    ,property_lo_alarm_color_2_transparent(*this,"lo_alarm_color2_transparent",1.0) \
    \
    ,is_show(false) \
    ,action_start(false) \
    \
    ,last_value_angle(0) \
    ,value_angle(0) \
    ,temp_value_angle(0) \

// -------------------------------------------------------------------------
void URadialValueIndicator::ctor()
{
    set_visible_window(false);

    connect_property_changed("value", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
	connect_property_changed("enable-calibrate", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
	connect_property_changed("enable-calibrate-limit", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
	connect_property_changed("calibrate-rmin", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
	connect_property_changed("calibrate-rmax", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
	connect_property_changed("calibrate-cmin", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
	connect_property_changed("calibrate-cmax", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
	connect_property_changed("precision", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );

    connect_property_changed("min-scale-value", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
    connect_property_changed("max-scale-value", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
    connect_property_changed("max-scale-minor", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
    connect_property_changed("max-scale-major", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
    connect_property_changed("start-scale-radial-value", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
    connect_property_changed("end-scale-radial-value", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
	connect_property_changed("enable-scale", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
	connect_property_changed("scale-width", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-minor-height", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-major-height", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-minor-color", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-major-color", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );

    connect_property_changed("enable-scale-label", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-label-digits", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-label-fill-digits", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-label-color", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-label-font", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-label-font-size", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("scale-label-offset", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );

	connect_property_changed("invert-rotate", sigc::mem_fun(*this, &URadialValueIndicator::on_value_changed) );
	connect_property_changed("use-arrow-svgfile", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("arrow-svgfile", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("arrow-width", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("arrow-height", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("arrow-rotate-center-x-pos", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );
    connect_property_changed("arrow-rotate-center-y-pos", sigc::mem_fun(*this, &URadialValueIndicator::queue_draw) );

    signal_size_allocate().connect(sigc::mem_fun(*this, &URadialValueIndicator::allocate_changed));
    set_events(~Gdk::ALL_EVENTS_MASK);
    signal_event().connect(sigc::mem_fun(*this, &URadialValueIndicator::on_indicator_event));

//	on_value_changed();
    show_all();
}
// -------------------------------------------------------------------------
URadialValueIndicator::URadialValueIndicator() :
    Glib::ObjectBase("uradialvalueindicator")
    ,URadialValueIndicator_INIT_PROPERTIES()
{
    ctor();
}
// -------------------------------------------------------------------------
URadialValueIndicator::URadialValueIndicator(GtkmmBaseType::BaseObjectType* gobject) :
    UEventBox(gobject)
    ,URadialValueIndicator_INIT_PROPERTIES()
{
    ctor();
}
// -------------------------------------------------------------------------
void URadialValueIndicator::on_value_changed()
{
//	cout<<get_name()<<"::on_value_changed value="<<property_value.get_value()<< endl;
	double prec_value = calibrate.get_cvalue(property_value.get_value())/pow10(get_property_precision());
	if(prop_use_arrow_inert)
    {
        last_value_angle = temp_value_angle;
		if(prop_invert_rotate)
		{
			value_angle = maxDeg - (prec_value - prop_Min.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
			if(prec_value < prop_Min.get_value())
				value_angle = maxDeg;
			else if(prec_value > prop_Max.get_value())
				value_angle = minDeg<maxDeg ? minDeg:minDeg+2*M_PI;
		}
		else
		{
			value_angle = minDeg + (prec_value - prop_Min.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
			if(prec_value < prop_Min.get_value())
				value_angle = minDeg;
			else if(prec_value > prop_Max.get_value())
				value_angle = minDeg<maxDeg ? maxDeg:maxDeg+2*M_PI;
		}
		
		if (!inert_signal.connected())
		{
			inert_signal= inert.signal_inert_value_change().connect(sigc::mem_fun(*this, &URadialValueIndicator::on_inert_value_changed));
		}
		inert.set_value( last_value_angle, value_angle );
	}
    else
    {
		if (inert_signal.connected())
			inert_signal.disconnect();
		last_value_angle = value_angle;
		if(prop_invert_rotate)
		{
			value_angle = maxDeg - (prec_value - prop_Min.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
			if(prec_value < prop_Min.get_value())
				value_angle = maxDeg;
			else if(prec_value > prop_Max.get_value())
				value_angle = minDeg<maxDeg ? minDeg:minDeg+2*M_PI;
		}
		else
		{
			value_angle = minDeg + (prec_value - prop_Min.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
			if(prec_value < prop_Min.get_value())
				value_angle = minDeg;
			else if(prec_value > prop_Max.get_value())
				value_angle = minDeg<maxDeg ? maxDeg:maxDeg+2*M_PI;
		}
		temp_value_angle = value_angle;
    }
//	cout<<get_name()<<"::on_value_changed value_angle="<<value_angle<<" temp_value_angle="<<temp_value_angle<< endl;

    queue_draw();
}
// -------------------------------------------------------------------------
void URadialValueIndicator::on_inert_value_changed(double val, bool inert_is_stoped)
{
	//	cout<<get_name()<<"::on_inert_value_changed temp_value="<<val<< endl;
	temp_value_angle = val;
	queue_draw();
}
// -------------------------------------------------------------------------
void URadialValueIndicator::allocate_changed(Gtk::Allocation& alloc)
{
    queue_draw();
}
// -------------------------------------------------------------------------
bool URadialValueIndicator::on_indicator_event(GdkEvent* ev)
{
	if(ev->type==GDK_MAP)
	{
//		cout<<get_name()<<"::on_indicator_event event->type="<<ev->type<< endl;
		is_show = true;
	}
	else if(ev->type==GDK_UNMAP)
		is_show = false;

	if(prop_passiveMode)
		return false;
	
	GdkEventButton event_btn = ev->button;
	if(event_btn.type == GDK_BUTTON_PRESS)
	{
		action_start=true;
		float val = calculation_value_in_push_point();
		if(prop_setValuePermanent)
		{
			signal.emit(val);
			sensor_ai_.save_value(val);
		}
		queue_draw();
	}
	else if( ev->type == GDK_MOTION_NOTIFY && action_start)
	{
		float val = calculation_value_in_push_point();
		if(prop_setValuePermanent)
		{
			signal.emit(val);
			sensor_ai_.save_value(val);
		}
		queue_draw();
	}
	else if( ev->type == GDK_BUTTON_RELEASE && action_start)
	{
		action_start=false;
		float val = calculation_value_in_push_point();
// 		cout<<get_name()<<"::on_indicator_event() val="<<val<< endl;
		signal.emit(val);
		sensor_ai_.save_value(val);
	}
	return false;
}
// -------------------------------------------------------------------------
float URadialValueIndicator::calculation_value_in_push_point()
{
	Glib::RefPtr<Gdk::Window> win = get_window();
	if(!win)
		return 0;
	
	int p_x,p_y;
	Gdk::ModifierType mtype;
	win->get_pointer(p_x,p_y,mtype);
	
	Gtk::Allocation alloc = get_allocation();
	//calculation value X & Y in push point
	float x_value = p_x - alloc.get_x();
	float y_value = p_y - alloc.get_y();

	double set_angle = 0;
	double delta_pos_x = pos_x - x_value;
	double delta_pos_y = pos_y - y_value;
	double averageAngle = (minDeg+maxDeg)/2;
	if(maxDeg>minDeg)
	{
		if(averageAngle - M_PI < 0)
			averageAngle += M_PI;
		else
			averageAngle -= M_PI;
	}
	
	if(delta_pos_x >= 0 && delta_pos_y >= 0)
		set_angle = atan(abs(pos_y - y_value)/(pos_x - x_value));
	else if(delta_pos_x < 0 && delta_pos_y >= 0)
		set_angle = M_PI + atan(abs(pos_y - y_value)/(pos_x - x_value));
	else if(delta_pos_x < 0 && delta_pos_y < 0)
		set_angle = M_PI - atan(abs(pos_y - y_value)/(pos_x - x_value));
	else if(delta_pos_x >= 0 && delta_pos_y < 0)
		set_angle = 2*M_PI - atan(abs(pos_y - y_value)/(pos_x - x_value));
	
	if(maxDeg<minDeg && set_angle>averageAngle)
		set_angle -= 2*M_PI;
	else if(set_angle<averageAngle && averageAngle<minDeg && averageAngle<maxDeg)
		set_angle += 2*M_PI;
	else if(set_angle>averageAngle && averageAngle>minDeg && averageAngle>maxDeg)
		set_angle -= 2*M_PI;
	
	float prec_value = 0;
	if(prop_invert_rotate)
	{
		prec_value = (prop_Max.get_value()-prop_Min.get_value())*(maxDeg - set_angle)/angle/(M_PI/180) + prop_Min.get_value();
		
		if(prec_value < prop_Min.get_value())
		{
			set_angle = maxDeg;
			prec_value = prop_Min.get_value();
		}
		else if(prec_value > prop_Max.get_value())
		{
			set_angle = minDeg<maxDeg ? minDeg:minDeg+2*M_PI;
			prec_value = prop_Max.get_value();
		}
	}
	else
	{
		double minAngle = (maxDeg>minDeg? minDeg:(minDeg-2*M_PI));
		prec_value = (prop_Max.get_value()-prop_Min.get_value())*(set_angle - minAngle)/angle/(M_PI/180) + prop_Min.get_value();
		
		if(prec_value < prop_Min.get_value())
		{
			set_angle = minDeg;
			prec_value = prop_Min.get_value();
		}
		else if(prec_value > prop_Max.get_value())
		{
			set_angle = minDeg<maxDeg ? maxDeg:maxDeg+2*M_PI;
			prec_value = prop_Max.get_value();
		}
	}
	
	if(prop_use_arrow_inert)
		temp_value_angle = set_angle;
	else
		value_angle = set_angle;
	return calibrate.get_rvalue(prec_value)*pow10(get_property_precision());
}
// -------------------------------------------------------------------------
void URadialValueIndicator::calculate_circle_param()
{
    Gtk::Allocation alloc = get_allocation();

    minDeg = ((360 + prop_MinDeg.get_value()%360)%360)*(M_PI/180);
    maxDeg = ((360 + prop_MaxDeg.get_value()%360)%360)*(M_PI/180);
    H = (double)alloc.get_height();
    W = (double)alloc.get_width();
    cosA = cos(minDeg);
    cosB = cos(maxDeg);
    sinA = sin(minDeg);
    sinB = sin(maxDeg);

    // вычисление координат смещения по X и Y и велечину радиуса дуги в зависимости от размеров виджета
	if( maxDeg == minDeg )
	{//если полная окружность
		R=MIN(W,H)/2;
		pos_x=R;
		pos_y=R;

		angle = 360;
		maxDeg = minDeg + 2*M_PI;
	}
	else
	{
		if(cosA>=0)
		{
			if(sinA>=0)
			{
				if(cosB>=0)
				{
					if(sinB>=0)
					{
						if(cosA<=cosB)
						{
							R=MIN(W,H)/2;
							pos_x=R;
							pos_y=R;
						}
						else
						{
							R=MIN(W/cosA,H/sinB);
							pos_x=R*cosA;
							pos_y=R*sinB;
						}
					}
					else
					{
						R=MIN(W/(1+MAX(cosA,cosB)),H/2);
						pos_x=R*MAX(cosA,cosB);
						pos_y=R;
					}
				}
				else
				{
					if(sinB>=0)
					{
						R=MIN(W/(cosA-cosB),H);
						pos_x=R*cosA;
						pos_y=R;
					}
					else
					{
						R=MIN(W/(1+cosA),H/(1-sinB));
						pos_x=R*cosA;
						pos_y=R;
					}
				}
			}
			else
			{
				if(cosB>=0)
				{
					if(sinB>=0)
					{
						R=MIN(W,H/(sinB-sinA));
						pos_x=R;
						pos_y=R*sinB;
					}
					else
					{
						if(sinA>=sinB)
						{
							R=MIN(W,H)/2;
							pos_x=R;
							pos_y=R;
						}
						else
						{
							R=MIN(W/cosB,-H/sinA);
							pos_x=R*cosB;
							pos_y=0;
						}
					}
				}
				else
				{
					if(sinB>=0)
					{
						R=MIN(W/(1-cosB),H/(1-sinA));
						pos_x=R;
						pos_y=R;
					}
					else
					{
						R=MIN(W/2,H/(1-MIN(sinA,sinB)));
						pos_x=R;
						pos_y=R;
					}
				}
			}
		}
		else
		{
			if(sinA>=0)
			{
				if(cosB>=0)
				{
					if(sinB>=0)
					{
						R=MIN(W/2,H/(1+MAX(sinA,sinB)));
						pos_x=R;
						pos_y=R*MAX(sinA,sinB);
					}
					else
					{
						R=MIN(W/(1+cosB),H/(1+sinA));
						pos_x=R*cosB;
						pos_y=R*sinA;
					}
				}
				else
				{
					if(sinB>=0)
					{
						if(cosA<=cosB)
						{
							R=MIN(W,H)/2;
							pos_x=R;
							pos_y=R;
						}
						else
						{
							R=MIN(-W/cosB,H/sinA);
							pos_x=0;
							pos_y=MAX(R*sinA,R*sinB);
						}
					}
					else
					{
						R=MIN(W,H/(sinA-sinB));
						pos_x=0;
						pos_y=R*sinA;
					}
				}
			}
			else
			{
				if(cosB>=0)
				{
					if(sinB>=0)
					{
						R=MIN(W/(1-cosA),H/(1+sinB));
						pos_x=R;
						pos_y=R*sinB;
					}
					else
					{
						R=MIN(W/(cosB-cosA),H);
						pos_x=R*cosB;
						pos_y=0;
					}
				}
				else
				{
					if(sinB>=0)
					{
						R=MIN(W/(1-MIN(cosA,cosB)),H/2);
						pos_x=R;
						pos_y=R;
					}
					else
					{
						if(sinA<=sinB)
						{
							R=MIN(W,H)/2;
							pos_x=R;
							pos_y=R;
						}
						else
						{
							R=MIN(-W/cosA,-H/sinB);
							pos_x=0;
							pos_y=0;
						}
					}
				}
			}
		}
		angle = (360 + prop_MaxDeg.get_value()%360 - prop_MinDeg.get_value()%360)%360;
	}
//	cout<<get_name()<<"::on_expose_event R="<<R<<" pos_x="<<pos_x<<" pos_y="<<pos_y<<" W="<<W<<" H="<<H<<" cosA="<<cosA<<" sinA="<<sinA<<" cosB="<<cosB<<" sinB="<<sinB<<endl;
}
// -------------------------------------------------------------------------
void URadialValueIndicator::calculate_circle_param_scale_offset()
{
	// вычисление поправки велечину радиуса дуги если деления дуги рисуются наружу
	if(get_prop_scaleMinor_height()<0 || get_prop_scaleMajor_height()<0)
	{
		R = R + MIN(get_prop_scaleMinor_height(),get_prop_scaleMajor_height());
	}
}
// -------------------------------------------------------------------------
void URadialValueIndicator::calculate_circle_param_label_offset()
{
	if(prop_enableScaleLabel && prop_scaleLabelOffset.get_value()<0)
	{
		int numDashMajor = prop_maxMajor.get_value()>0? prop_maxMajor.get_value():1;
		for(int i = 0;i<=numDashMajor;++i)
		{
			float value = prop_Min.get_value() + i*(prop_Max.get_value()-prop_Min.get_value())/numDashMajor;
			std::ostringstream title;
			title<<(value>=0? "":"-");
			value = fabs(value);
			if(property_fill_digits>0)
				title<<std::setw(property_fill_digits)<<setfill('0')<<(long)value;
			else
				title<<(long)value;
			if(property_digits>0)
				title<<","<<std::setw(property_digits)<<setfill('0')<<(long)(fabs(((float)value-(long)value )*pow10(property_digits)));
			//cout<<get_name()<<"::draw_scale titles["<<i<<"]="<<title.str()<<endl;
			
			Glib::RefPtr<Pango::Layout> layout_value;
			layout_value = create_pango_layout("0");
			layout_value->set_text(title.str());
			
			Pango::FontDescription fd(prop_scaleLabelFont);
			fd.set_absolute_size(prop_scaleLabelFontSize.get_value() * Pango::SCALE);
			layout_value->set_font_description(fd);
			
			int tx, ty;
			layout_value->get_pixel_size(tx,ty);
			double radial_value;
			if(prop_invert_rotate)
				radial_value = maxDeg - i*(double)(angle*(M_PI/180))/numDashMajor;
			else
				radial_value = minDeg + i*(double)(angle*(M_PI/180))/numDashMajor;
			double cosV = cos(radial_value);
			double sinV = sin(radial_value);
			double tanV = tan(radial_value);
			double title_offset = R - (double)prop_scaleMajor_height.get_value() - (double)prop_scaleLabelOffset.get_value();
			double x_layout_center,y_layout_center,x,y;
			
			double a = pow(tanV,2)+1;
			double b;
			double c = pow(ty/2,2) + pow(tx/2,2) - pow(title_offset,2);
			if(prop_scaleMajor_height.get_value()>=0)
			{
				if(cosV>=0 && sinV>=0)
				{
					b = -tanV*ty - tx;
					x_layout_center = (-b-pow((pow(b,2)-4*a*c),0.5))/(2*a);
				}
				else if(cosV<0 && sinV>=0)
				{
					b = -tanV*ty + tx;
					x_layout_center = (-b+pow((pow(b,2)-4*a*c),0.5))/(2*a);
				}
				else if(cosV<0 && sinV<0)
				{
					b = tanV*ty + tx;
					x_layout_center = (-b+pow((pow(b,2)-4*a*c),0.5))/(2*a);
				}
				else if(cosV>=0 && sinV<0)
				{
					b = tanV*ty - tx;
					x_layout_center = (-b-pow((pow(b,2)-4*a*c),0.5))/(2*a);
				}
			}
			else
			{
				if(cosV>=0 && sinV>=0)
				{
					b = tanV*ty + tx;
					x_layout_center = (-b-pow((pow(b,2)-4*a*c),0.5))/(2*a);
				}
				else if(cosV<0 && sinV>=0)
				{
					b = tanV*ty - tx;
					x_layout_center = (-b+pow((pow(b,2)-4*a*c),0.5))/(2*a);
				}
				else if(cosV<0 && sinV<0)
				{
					b = -tanV*ty - tx;
					x_layout_center = (-b+pow((pow(b,2)-4*a*c),0.5))/(2*a);
				}
				else if(cosV>=0 && sinV<0)
				{
					b = -tanV*ty + tx;
					x_layout_center = (-b-pow((pow(b,2)-4*a*c),0.5))/(2*a);
				}
			}
			y_layout_center = tanV * x_layout_center;
			x = x_layout_center - tx/2 + pos_x;
			y = y_layout_center - ty/2 + pos_y;
			//			cout<<get_name()<<"::draw_scale a="<<a<<" b="<<b<<" c="<<c<<" x_layout_center="<<x_layout_center<<" y_layout_center="<<y_layout_center<<" x="<<x<<" y="<<y<<endl;
			
			double new_Rx = R;
			double new_Ry = R;
			if(x+tx/2<0 && cosV!=0)
				new_Rx = R - fabs(x/cosV);
			else if(x>W-tx/2 && cosV!=0)
				new_Rx = R - fabs((x-(W-tx/2))/cosV);
			if(y+ty/2<0 && sinV!=0)
				new_Ry = R - fabs(y/sinV);
			else if(y>H-ty/2 && sinV!=0)
				new_Ry = R - fabs((y-(H-ty/2))/sinV);
			
			R = MIN(new_Rx,new_Ry);
		}
	}
}
// -------------------------------------------------------------------------
void URadialValueIndicator::calculate_circle_param_arrow_offset()
{
	if(prop_use_arrow_svgfile)
	{
	}
	else
	{
		double k_rarius = prop_ratio_arrowWidth_and_arrowHeight.get_value();
		k_rarius = k_rarius>1? 1:(k_rarius<0? 0:k_rarius);
		double arrow_rotate_radius = R - (get_prop_scaleMinor_height()>0 ? get_prop_scaleMinor_height():0);
		double arrow_h = k_rarius * arrow_rotate_radius;

		double delta_pos_x = 0;
		double delta_pos_y = 0;
		if(pos_x + (arrow_h/2 + ((double)get_prop_arrow_stroke_width())/2) > W)
		{
			delta_pos_x = (W - arrow_h/2 - ((double)get_prop_arrow_stroke_width())/2) - pos_x;
		}
		else if(pos_x - (arrow_h/2 + ((double)get_prop_arrow_stroke_width())/2) < 0)
		{
			delta_pos_x = (arrow_h/2 + ((double)get_prop_arrow_stroke_width())/2) - pos_x;
		}
		if(pos_y + (arrow_h/2 + ((double)get_prop_arrow_stroke_width())/2) > H)
		{
			delta_pos_y = (H - arrow_h/2 - ((double)get_prop_arrow_stroke_width())/2) - pos_y;
		}
		else if(pos_y - (arrow_h/2 + ((double)get_prop_arrow_stroke_width())/2) < 0)
		{
			delta_pos_y = (arrow_h/2 + ((double)get_prop_arrow_stroke_width())/2) - pos_y;
		}
		
		double delta = fabs(delta_pos_x)>fabs(delta_pos_y) ? fabs(delta_pos_x):fabs(delta_pos_y);
// 		cout<<get_name()<<"::calculate_circle_param_arrow_offset delta_pos_x="<<delta_pos_x<<" delta_pos_y="<<delta_pos_y<<" delta="<<delta<<" pos_x="<<pos_x<<" pos_y="<<pos_y<<endl;
		R = R - delta;
		pos_x = pos_x + delta_pos_x;
		pos_y = pos_y + delta_pos_y;
	}
}
// -------------------------------------------------------------------------
bool URadialValueIndicator::on_expose_event(GdkEventExpose* event)
{
#if GTK_VERSION_GE(2,20)
    if(!get_mapped())
#else
    if(!is_show)
#endif
        return false;

    Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
    Gtk::Allocation alloc = get_allocation();
    Gdk::Cairo::rectangle(cr,alloc);
    cr->clip();
    cr->translate(alloc.get_x(), alloc.get_y());

    calculate_circle_param();
	// вычисление поправки велечину радиуса дуги если деления дуги рисуются наружу
	if(prop_enableScale)
		calculate_circle_param_scale_offset();
	// вычисление поправки велечину радиуса дуги если подписи шкалы находятся снаружи
	calculate_circle_param_label_offset();
	// вычисление поправки велечину радиуса дуги если стрелка вылезает за границы виджета
	calculate_circle_param_arrow_offset();
	
   //ресуем стрелку под шкалой
    if(prop_enableArrow && !prop_arrowOnTop)
		draw_arrow(cr,alloc);
    //красим сектора пороговых значений
    draw_threshold(cr,alloc);
    //чертим шкалу
    draw_scale(cr,alloc);
	//ресуем стрелку над шкалой
	if(prop_enableArrow && prop_arrowOnTop)
		draw_arrow(cr,alloc);
	
    if ( !connected_ && get_property_disconnect_effect() == 1)
    {
        alloc.set_x(0);
        alloc.set_y(0);
        UVoid::draw_disconnect_effect_1(cr, alloc);
    }

    return true;
}
// -------------------------------------------------------------------------
void URadialValueIndicator::draw_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	if(prop_enableScale)
	{
		//отрисовка малых делений
		double dashMinor = (R-(double)prop_scaleMinor_height.get_value()/2)*angle*(M_PI/180);
		int numDashMinor = (prop_maxMinor.get_value()>0? prop_maxMinor.get_value():1)*(prop_maxMajor.get_value()>0? prop_maxMajor.get_value():1);
		std::vector<double> dashesMinor(2);
		dashesMinor[0] = prop_scalemark_width.get_value()<0? 0:prop_scalemark_width.get_value();
		dashesMinor[1] = (double)(dashMinor - prop_scalemark_width.get_value())/numDashMinor - prop_scalemark_width.get_value();
		dashesMinor[1] = dashesMinor[1]<0? 0:dashesMinor[1];
		cr->save();
		Gdk::Cairo::set_source_color(cr, prop_scaleMinorColor.get_value());
		cr->set_dash(dashesMinor,0);
		cr->set_line_width(abs(prop_scaleMinor_height.get_value()));
		cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2, minDeg-M_PI,maxDeg-M_PI);
		cr->stroke();
		cr->restore();
		
		//отрисовка больших делений
		double dashMajor = (R-(double)prop_scaleMajor_height.get_value()/2)*angle*(M_PI/180);
		int numDashMajor = prop_maxMajor.get_value()>0? prop_maxMajor.get_value():1;
		std::vector<double> dashesMajor(2);
		dashesMajor[0] = dashesMinor[0];
		dashesMajor[1] = (double)(dashMajor - prop_scalemark_width.get_value())/numDashMajor - prop_scalemark_width.get_value();
		dashesMajor[1] = dashesMajor[1]<0? 0:dashesMajor[1];
		cr->save();
		Gdk::Cairo::set_source_color(cr, prop_scaleMajorColor.get_value());
		cr->set_dash(dashesMajor,0);
		cr->set_line_width(abs(prop_scaleMajor_height.get_value()));
		cr->arc( pos_x, pos_y, R-(double)prop_scaleMajor_height.get_value()/2, minDeg-M_PI,maxDeg-M_PI);
		cr->stroke();
		cr->restore();
		/*
		 *    cout<<get_name()<<"::draw_scale angle="<<angle<<endl;
		 *    cout<<get_name()<<"::draw_scale dashMinor="<<dashMinor<<endl;
		 *    cout<<get_name()<<"::draw_scale dashesMinor[0]="<<dashesMinor[0]<<" dashesMinor[1]="<<dashesMinor[1]<<endl;
		 *    cout<<get_name()<<"::draw_scale dashMajor="<<dashMajor<<endl;
		 *    cout<<get_name()<<"::draw_scale dashesMajor[0]="<<dashesMinor[0]<<" dashesMajor[1]="<<dashesMajor[1]<<endl;
		 */
		//отрисовка окружности
		cr->save();
		//	cr->unset_dash();
		Gdk::Cairo::set_source_color(cr, prop_scaleMinorColor.get_value());
		cr->set_line_width(prop_scalemark_width.get_value());
		cr->arc( pos_x, pos_y, R-(double)prop_scalemark_width.get_value()/2, minDeg-M_PI,maxDeg-M_PI);
		cr->stroke();
		cr->restore();
	}

    //отрисовка подписей значений под большими делениями
    if(prop_enableScaleLabel)
    {
		int numDashMajor = prop_maxMajor.get_value()>0? prop_maxMajor.get_value():1;
		int i=0;
		//если полная окружность - не рисуем первое значение шкалы (на его месте отрисуется последнее)
		if( maxDeg==(minDeg + 2*M_PI) )
			i=1;
		for(i;i<=numDashMajor;++i)
        {
            float value;
			std::ostringstream title;
			if( maxDeg==(minDeg + 2*M_PI) && i==numDashMajor && !prop_invert_rotate )
			{//если полная окружность и рисуем последнее значение шкалы, то вместе с ним рисуем первое значение шкалы
				value = prop_Min.get_value();
				title<<(value>=0? "":"-");
				value = fabs(value);
				if(property_fill_digits>0)
					title<<std::setw(property_fill_digits)<<setfill('0')<<(long)value;
				else
					title<<(long)value;
				if(property_digits>0)
					title<<","<<std::setw(property_digits)<<setfill('0')<<(long)(fabs(((float)value-(long)value )*pow10(property_digits)));
				title<<endl;
			}

			value = prop_Min.get_value() + i*(prop_Max.get_value()-prop_Min.get_value())/numDashMajor;
			title<<(value>=0? "":"-");
            value = fabs(value);
            if(property_fill_digits>0)
                title<<std::setw(property_fill_digits)<<setfill('0')<<(long)value;
            else
                title<<(long)value;
            if(property_digits>0)
                title<<","<<std::setw(property_digits)<<setfill('0')<<(long)(fabs(((float)value-(long)value )*pow10(property_digits)));

			if( maxDeg==(minDeg + 2*M_PI) && i==numDashMajor && prop_invert_rotate )
			{//если полная окружность и рисуем последнее значение шкалы, то вместе с ним рисуем первое значение шкалы
				value = prop_Min.get_value();
				title<<endl;
				title<<(value>=0? "":"-");
				value = fabs(value);
				if(property_fill_digits>0)
					title<<std::setw(property_fill_digits)<<setfill('0')<<(long)value;
				else
					title<<(long)value;
				if(property_digits>0)
					title<<","<<std::setw(property_digits)<<setfill('0')<<(long)(fabs(((float)value-(long)value )*pow10(property_digits)));
			}
			//cout<<get_name()<<"::draw_scale titles["<<i<<"]="<<title.str()<<endl;

            Glib::RefPtr<Pango::Layout> layout_value;
            layout_value = create_pango_layout("0");
			layout_value->set_alignment(Pango::ALIGN_CENTER);
			layout_value->set_text(title.str());

            cr->save();
            Gdk::Cairo::set_source_color(cr, prop_scaleLabelColor.get_value());
            Pango::FontDescription fd(prop_scaleLabelFont);
            fd.set_absolute_size(prop_scaleLabelFontSize.get_value() * Pango::SCALE);
            layout_value->set_font_description(fd);

            int tx, ty;
            layout_value->get_pixel_size(tx,ty);
            double radial_value;
			if(prop_invert_rotate)
				radial_value = maxDeg - i*(double)(angle*(M_PI/180))/numDashMajor;
			else
				radial_value = minDeg + i*(double)(angle*(M_PI/180))/numDashMajor;
			double cosV = cos(radial_value);
            double sinV = sin(radial_value);
            double tanV = tan(radial_value);
            double title_offset = R - (double)prop_scaleMajor_height.get_value() - (double)prop_scaleLabelOffset.get_value();
            double x_layout_center,y_layout_center,x,y;

/*
            Положим что центр текста подписи должен лежать на прямой  проходящей через центр окружности и точку на окружности соответствующую значению шкалы.
            уравнение такой прямой y=tg(V)*x; где A - угол в радианах соответствующий значению шкалы, а y и x координаты на плоскости.
            Также  положение текста должно удовлетворять условию, чтоб угол плошадки текста лежал на окружности (для каждой четверти окружности свой угол,
            например, для 1й четверти это левый-верхний угол площадки, если текст внутри оружности, и нижний-правый, если текст снаружи).
            уравнение окружности x²+y²=R²
            для определения положения центра площадки текста нужно решить систему уравнений
            ┌					┌							┌											┌
            │y=tg(V)*x			│y=tg(V)*x					│y=tg(V)*x									│y=tg(V)*x
            ┤					┤							┤											┤
            │(x+∆x)²+(y+∆y)²=R²	│(x+∆x)²+(tg(V)x+∆y)²=R²	│x²+2x∆x+∆x²+tg(V)²x²+2tg(V)x∆y+∆y²-R²=0	│x²(tg(V)²+1)+x(2∆x+2tg(V)∆y)+(∆x²+∆y²-R²)=0
            └					└							└											└
            решаем квадратное уравнение
            ax²+bx+c=0;
            D=b²-4ac;
            x=(-b±√D)/2a;
            и подставляем x в уравнение y=tg(V)*x;
            полученные координаты в системе координат с началом в центре окружности...
            поэтому
            x=x-∆x+x0; и y=y-∆y+y0;
*/
            double a = pow(tanV,2)+1;
            double b;
            double c = pow(ty/2,2) + pow(tx/2,2) - pow(title_offset,2);
            if(prop_scaleMajor_height.get_value()>=0)
            {
                if(cosV>=0 && sinV>=0)
                {
                    b = -tanV*ty - tx;
                    x_layout_center = (-b-pow((pow(b,2)-4*a*c),0.5))/(2*a);
                }
                else if(cosV<0 && sinV>=0)
                {
                    b = -tanV*ty + tx;
                    x_layout_center = (-b+pow((pow(b,2)-4*a*c),0.5))/(2*a);
                }
                else if(cosV<0 && sinV<0)
                {
                    b = tanV*ty + tx;
                    x_layout_center = (-b+pow((pow(b,2)-4*a*c),0.5))/(2*a);
                }
                else if(cosV>=0 && sinV<0)
                {
                    b = tanV*ty - tx;
                    x_layout_center = (-b-pow((pow(b,2)-4*a*c),0.5))/(2*a);
                }
            }
            else
            {
                if(cosV>=0 && sinV>=0)
                {
                    b = tanV*ty + tx;
                    x_layout_center = (-b-pow((pow(b,2)-4*a*c),0.5))/(2*a);
                }
                else if(cosV<0 && sinV>=0)
                {
                    b = tanV*ty - tx;
                    x_layout_center = (-b+pow((pow(b,2)-4*a*c),0.5))/(2*a);
                }
                else if(cosV<0 && sinV<0)
                {
                    b = -tanV*ty - tx;
                    x_layout_center = (-b+pow((pow(b,2)-4*a*c),0.5))/(2*a);
                }
                else if(cosV>=0 && sinV<0)
                {
                    b = -tanV*ty + tx;
                    x_layout_center = (-b-pow((pow(b,2)-4*a*c),0.5))/(2*a);
                }
            }
            y_layout_center = tanV * x_layout_center;
            x = x_layout_center - tx/2 + pos_x;
            y = y_layout_center - ty/2 + pos_y;
//			cout<<get_name()<<"::draw_scale a="<<a<<" b="<<b<<" c="<<c<<" x_layout_center="<<x_layout_center<<" y_layout_center="<<y_layout_center<<" x="<<x<<" y="<<y<<endl;
/*
            cr->rectangle( x<0?0:(x>W-tx?W-tx:x), y<0?0:(y>H-ty?H-ty:y), tx, ty);
            cr->fill();
            cr->move_to(x<0?0:(x>W-tx?W-tx:x), y<0?0:(y>H-ty?H-ty:y));
            Gdk::Cairo::set_source_color(cr, prop_scaleMinorColor.get_value());
*/
//			cout<<get_name()<<"::draw_scale  x="<<x<<" y="<<y<<" pos_x="<<pos_x<<" pos_y="<<pos_y<<" W="<<W<<" tx="<<tx<<" H="<<H<<" ty="<<ty<<endl;
            cr->move_to(x<0?0:(x>W-tx?W-tx:x), y<0?0:(y>H-ty?H-ty:y));
            layout_value->add_to_cairo_context(cr);
            cr->fill();
            cr->restore();
        }
    }
}
// -------------------------------------------------------------------------
void URadialValueIndicator::draw_arrow(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
    double cur_angle;
    if(prop_use_arrow_inert)
        cur_angle = temp_value_angle;
    else
        cur_angle = value_angle;
	cur_angle = uquant.get_value(maxDeg>minDeg? minDeg:(minDeg-2*M_PI),maxDeg,cur_angle);
	
	double prec_value=0;
	if(prop_enableArrowThreshold)
	{
		if(prop_invert_rotate)
			prec_value = (prop_Max.get_value()-prop_Min.get_value())*(maxDeg - cur_angle)/angle/(M_PI/180) + prop_Min.get_value();
		else
			prec_value = (prop_Max.get_value()-prop_Min.get_value())*(cur_angle - minDeg)/angle/(M_PI/180) + prop_Min.get_value();
	}
	
	if(prop_use_arrow_svgfile)
    {
		ustring svgfile;
		if(prop_enableArrowThreshold)
		{
			if (property_hi_alarm_on_ && prec_value >= property_hi_alarm_)
				svgfile = get_prop_hi_alarm_svgfile().empty()? get_prop_arrow_svgfile():get_prop_hi_alarm_svgfile();
			else if (property_hi_warning_on_ && prec_value >= property_hi_warning_)
				svgfile = get_prop_hi_warning_svgfile().empty()? get_prop_arrow_svgfile():get_prop_hi_warning_svgfile();
			else if (property_lo_alarm_on_ && prec_value <= property_lo_alarm_)
				svgfile = get_prop_lo_alarm_svgfile().empty()? get_prop_arrow_svgfile():get_prop_lo_alarm_svgfile();
			else if (property_lo_warning_on_ && prec_value <= property_lo_warning_)
				svgfile = get_prop_lo_warning_svgfile().empty()? get_prop_arrow_svgfile():get_prop_lo_warning_svgfile();
			else
				svgfile = get_prop_arrow_svgfile();
		}
		else
			svgfile = get_prop_arrow_svgfile();
		
		RsvgHandle *handle = rsvg_handle_new_from_file (svgfile.c_str(),NULL);
        if(handle!=NULL)
        {
            RsvgDimensionData dimension_data;
            rsvg_handle_get_dimensions(handle, &dimension_data);
            double kx,ky,x_center,y_center;
            if(prop_svg_auto_scale)
            {
                kx = ((double)prop_arrow_width.get_value())/dimension_data.width;
                if(kx==0)
                    kx=1;
                ky = kx;
                x_center = pos_x - ((prop_arrow_rotate_x.get_value()*cos(cur_angle) - prop_arrow_rotate_y.get_value()*sin(cur_angle)))*kx;
                y_center = pos_y -((prop_arrow_rotate_x.get_value()*sin(cur_angle) + prop_arrow_rotate_y.get_value()*cos(cur_angle)))*ky;
            }
            else
            {
                kx = ((double)prop_arrow_width.get_value())/dimension_data.width;
                if(kx==0)
                    kx=1;
                ky = ((double)prop_arrow_height.get_value())/dimension_data.height;
                if(ky==0)
                    ky=1;
                x_center = pos_x - (prop_arrow_rotate_x.get_value()*cos(cur_angle) - prop_arrow_rotate_y.get_value()*sin(cur_angle));
                y_center = pos_y - (prop_arrow_rotate_x.get_value()*sin(cur_angle) + prop_arrow_rotate_y.get_value()*cos(cur_angle));
            }
//			cout<<get_name()<<"::draw_arrow SVG.width="<<dimension_data.width<<" SVG.height="<<dimension_data.height<<" x_center="<<x_center<<" y_center="<<y_center<<endl;

//			cr->push_group();
            cr->save();
            cr->scale(kx, ky);
            cr->translate(x_center/kx, y_center/ky);
            cr->rotate(cur_angle);
            rsvg_handle_render_cairo(handle, cr->cobj());
//			rsvg_handle_free (handle);
			g_object_unref(handle);
//			g_free(handle);
            cr->close_path();
            cr->stroke();
            cr->restore();

//			Cairo::RefPtr<Cairo::Pattern> arrow = cr->pop_group();
//			arrow->unreference();
//			Cairo::Matrix matrix = arrow->get_matrix();
//			matrix.scale(kx, ky);
//			matrix.translate(x_center/kx, y_center/ky);
//			matrix.rotate(cur_angle-M_PI);
//			matrix.translate(alloc.get_x() - x_center/* - alloc.get_x()*/, alloc.get_y() - y_center/* - alloc.get_y()*/);
//			arrow->set_matrix(matrix);
//			cr->set_source(arrow);
//			cr->paint();
        }
        else
            cout<<get_name()<<"::draw_arrow can`t open SVG="<<prop_arrow_svgfile.get_value()<<endl;
    }
    else
    {
        double k_rarius = prop_ratio_arrowWidth_and_arrowHeight.get_value();
        k_rarius = k_rarius>1? 1:(k_rarius<0? 0:k_rarius);
        double k_peak_h = prop_ratio_arrowHeight_and_arrowPeakHeight.get_value();
        k_peak_h = k_peak_h>1? 1:(k_peak_h<0? 0:k_peak_h);
        double k_peak_w = prop_ratio_arrowWidth_and_arrowPeakWidht.get_value();
        k_peak_w = k_peak_w>1? 1:(k_peak_w<0? 0:k_peak_w);
        double k_angle = prop_ratio_arrowBaseHeight_and_arrowHeight.get_value();
        k_angle = k_angle>1? 1:(k_angle<0? 0:k_angle);
        double arrow_rotate_radius = R - (get_prop_scaleMinor_height()>0 ? get_prop_scaleMinor_height():0);
        double arrow_h = k_rarius * arrow_rotate_radius;
        double arrow_w = arrow_h + arrow_rotate_radius;
        double x_center = pos_x - (arrow_rotate_radius*cos(cur_angle) - arrow_h/2*sin(cur_angle));
        double y_center = pos_y - (arrow_rotate_radius*sin(cur_angle) + arrow_h/2*cos(cur_angle));

        cr->save();
        cr->translate(x_center, y_center);
        cr->rotate(cur_angle);

        cr->begin_new_sub_path();
        cr->set_line_width(prop_arrow_stroke_width.get_value());
        cr->arc( arrow_rotate_radius, arrow_h/2, arrow_h/2, (k_angle*M_PI)-M_PI, -(k_angle*M_PI)-M_PI );
        cr->line_to( k_peak_w * arrow_rotate_radius, arrow_h/2*(1+k_peak_h) );
        cr->line_to(0, arrow_h/2);
        cr->line_to( k_peak_w * arrow_rotate_radius, arrow_h/2*(1-k_peak_h) );
        cr->close_path();
        Cairo::Path *arrow = cr->copy_path();

		Gdk::Color color1, color2;
		if(prop_enableArrowThreshold)
		{
			if (property_hi_alarm_on_ && prec_value >= property_hi_alarm_)
			{
				color1 = property_hi_alarm_color_1;
				if(prop_arrow_useThresholdGradientColor)
					color2 = property_hi_alarm_color_2;
				else
					color2 = prop_arrow_fill_color2;
			}
			else if (property_hi_warning_on_ && prec_value >= property_hi_warning_)
			{
				color1 = property_hi_warning_color_1;
				if(prop_arrow_useThresholdGradientColor)
					color2 = property_hi_warning_color_2;
				else
					color2 = prop_arrow_fill_color2;
			}
			else if (property_lo_alarm_on_ && prec_value <= property_lo_alarm_)
			{
				color1 = property_lo_alarm_color_1;
				if(prop_arrow_useThresholdGradientColor)
					color2 = property_lo_alarm_color_2;
				else
					color2 = prop_arrow_fill_color2;
			}
			else if (property_lo_warning_on_ && prec_value <= property_lo_warning_)
			{
				color1 = property_lo_warning_color_1;
				if(prop_arrow_useThresholdGradientColor)
					color2 = property_lo_warning_color_2;
				else
					color2 = prop_arrow_fill_color2;
			}
			else
			{
				color1 = prop_arrow_fill_color1;
				color2 = prop_arrow_fill_color2;
			}
		}
		else
		{
			color1 = prop_arrow_fill_color1;
			color2 = prop_arrow_fill_color2;
		}
		
		if(prop_arrow_useGradientColor)
        {
//			cout<<get_name()<<"::draw_arrow prop_arrow_useGradientColor="<<prop_arrow_useGradientColor<<endl;
            double color1_pos = prop_arrow_gradient_color1_pos.get_value();
            color1_pos = color1_pos>1? 1:(color1_pos<0? 0:color1_pos);
            double color2_pos = prop_arrow_gradient_color2_pos.get_value();
            color2_pos = color2_pos>1? 1:(color2_pos<0? 0:color2_pos);
//			cout<<get_name()<<"::draw_arrow color1_pos="<<color1_pos<<" color2_pos="<<color2_pos<<endl;
            Cairo::RefPtr<Cairo::LinearGradient> gradient = Cairo::LinearGradient::create(0, 0, arrow_w, 0);
            gradient->add_color_stop_rgba(color1_pos, color1.get_red_p(), color1.get_green_p(), color1.get_blue_p(),1.0);
            gradient->add_color_stop_rgba(color2_pos, color2.get_red_p(), color2.get_green_p(), color2.get_blue_p(),1.0);
            cr->set_source(gradient);
        }
        else
            Gdk::Cairo::set_source_color(cr, color1);
        cr->fill();
        Gdk::Cairo::set_source_color(cr, prop_arrow_stroke_color.get_value());
        cr->append_path(*arrow);
        cr->stroke();
        cr->restore();
    }
}
// -------------------------------------------------------------------------
void URadialValueIndicator::draw_threshold(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	if(!prop_enableScaleThreshold)
		return;
    if(prop_threshold_useGradientColor)
    {
        int num_minor_step = get_prop_maxMinor()*get_prop_maxMajor();
        double minor_angle_step = ((maxDeg-minDeg)>0? (maxDeg-minDeg):(2*M_PI+(maxDeg-minDeg)))/num_minor_step;

        if(property_lo_warning_on_ && get_property_lo_warning_()>get_property_lo_alarm_())
        {
            double threshold_start_angle;
			double threshold_angle;
			if(prop_invert_rotate)
			{
				if(property_lo_alarm_on_)
				{
					threshold_start_angle = maxDeg - (fabs(prop_Min.get_value()) + property_lo_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
					if(property_lo_alarm_.get_value() < prop_Min.get_value())
						threshold_start_angle = maxDeg;
					else if(property_lo_alarm_.get_value() > prop_Max.get_value())
						threshold_start_angle = minDeg;
				}
				else
					threshold_start_angle = maxDeg;
				threshold_angle = maxDeg - (fabs(prop_Min.get_value()) + property_lo_warning_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_lo_warning_.get_value() < prop_Min.get_value())
					threshold_angle = maxDeg;
				else if(property_lo_warning_.get_value() > prop_Max.get_value())
					threshold_angle = minDeg;
			}
			else
			{
				if(property_lo_alarm_on_)
				{
					threshold_start_angle = minDeg + (fabs(prop_Min.get_value()) + property_lo_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
					if(property_lo_alarm_.get_value() < prop_Min.get_value())
						threshold_start_angle = minDeg;
					else if(property_lo_alarm_.get_value() > prop_Max.get_value())
						threshold_start_angle = maxDeg;
				}
				else
					threshold_start_angle = minDeg;
				threshold_angle = minDeg + (fabs(prop_Min.get_value()) + property_lo_warning_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_lo_warning_.get_value() < prop_Min.get_value())
					threshold_angle = minDeg;
				else if(property_lo_warning_.get_value() > prop_Max.get_value())
					threshold_angle = maxDeg;
			}

            cr->save();
            cr->set_line_width(abs(prop_scaleMinor_height.get_value()));
			for(double i=threshold_start_angle;prop_invert_rotate? i>threshold_angle:i<threshold_angle;)
            {
                double k;
                double red;
                double green;
                double blue;
                double transparent;
                double i_first = i;
				if(prop_invert_rotate? i_first<threshold_start_angle:i_first>threshold_start_angle && prop_invert_rotate? i_first>threshold_angle:i_first<threshold_angle)
					i_first -= prop_invert_rotate? -get_prop_threshold_offset_gradient()/R:get_prop_threshold_offset_gradient()/R;//2*M_PI/720;
				i += prop_invert_rotate? -minor_angle_step:minor_angle_step;
				if(prop_invert_rotate? i>threshold_angle:i<threshold_angle)
                    i = threshold_angle;

                double grad_x1 = pos_x - (R-(double)prop_scaleMinor_height.get_value()/2)*cos(i_first);
                double grad_y1 = pos_y - (R-(double)prop_scaleMinor_height.get_value()/2)*sin(i_first);
                double grad_x2 = pos_x - (R-(double)prop_scaleMinor_height.get_value()/2)*cos(i);
                double grad_y2 = pos_y - (R-(double)prop_scaleMinor_height.get_value()/2)*sin(i);
                Cairo::RefPtr<Cairo::LinearGradient> gradient = Cairo::LinearGradient::create(grad_x1, grad_y1, grad_x2, grad_y2);

				if(prop_invert_rotate)
					k = (threshold_start_angle - threshold_angle)!=0 ? ((threshold_start_angle - i_first)/(threshold_start_angle - threshold_angle)) : 1;
				else
					k = (threshold_angle-threshold_start_angle)!=0 ? ((i_first-threshold_start_angle)/(threshold_angle-threshold_start_angle)) : 1;
				red = get_property_lo_warning_color_2().get_red_p() + (get_property_lo_warning_color_1().get_red_p()-get_property_lo_warning_color_2().get_red_p())*k;
                green = get_property_lo_warning_color_2().get_green_p() + (get_property_lo_warning_color_1().get_green_p()-get_property_lo_warning_color_2().get_green_p())*k;
                blue = get_property_lo_warning_color_2().get_blue_p() + (get_property_lo_warning_color_1().get_blue_p()-get_property_lo_warning_color_2().get_blue_p())*k;
                transparent = get_property_lo_warning_color_2_transparent() + (get_property_lo_warning_color_1_transparent() - get_property_lo_warning_color_2_transparent())*k;
                gradient->add_color_stop_rgba(0.0,red,green,blue,transparent);

				if(prop_invert_rotate)
					k = (threshold_start_angle - threshold_angle)!=0 ? ((threshold_start_angle - i)/(threshold_start_angle - threshold_angle)) : 1;
				else
					k = (threshold_angle-threshold_start_angle)!=0 ? ((i-threshold_start_angle)/(threshold_angle-threshold_start_angle)) : 1;
                red = get_property_lo_warning_color_2().get_red_p() + (get_property_lo_warning_color_1().get_red_p()-get_property_lo_warning_color_2().get_red_p())*k;
                green = get_property_lo_warning_color_2().get_green_p() + (get_property_lo_warning_color_1().get_green_p()-get_property_lo_warning_color_2().get_green_p())*k;
                blue = get_property_lo_warning_color_2().get_blue_p() + (get_property_lo_warning_color_1().get_blue_p()-get_property_lo_warning_color_2().get_blue_p())*k;
                transparent = get_property_lo_warning_color_2_transparent() + (get_property_lo_warning_color_1_transparent() - get_property_lo_warning_color_2_transparent())*k;
                gradient->add_color_stop_rgba(1.0,red,green,blue,transparent);

//				cout<<get_name()<<"::draw_threshold::lo_warning i_first="<<i_first<<" i="<<i<<" threshold_start_angle="<<threshold_start_angle<<" threshold_angle="<<threshold_angle<<" minDeg="<<minDeg<<" maxDeg="<<maxDeg<<endl;
                cr->set_source(gradient);
				if(prop_invert_rotate)
					cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2,i-M_PI, i_first-M_PI);
				else
					cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2, i_first-M_PI,i-M_PI);
				cr->stroke();
            }
            cr->restore();
        }
        if(property_hi_warning_on_ && get_property_hi_warning_()<get_property_hi_alarm_())
        {
			double threshold_start_angle;
			double threshold_angle;
			double maxAngle;
			if(prop_invert_rotate)
			{
				maxAngle = (maxDeg>minDeg? minDeg:(minDeg-2*M_PI));
				if(property_hi_alarm_on_)
				{
					threshold_start_angle = maxDeg - (fabs(prop_Min.get_value()) + property_hi_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
					if(property_hi_alarm_.get_value() < prop_Min.get_value())
						threshold_start_angle = maxDeg;
					else if(property_hi_alarm_.get_value() > prop_Max.get_value())
						threshold_start_angle = maxAngle;
				}
				else
					threshold_start_angle = maxAngle;
				threshold_angle = maxDeg - (fabs(prop_Min.get_value()) + property_hi_warning_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_hi_warning_.get_value() < prop_Min.get_value())
					threshold_angle = maxDeg;
				else if(property_hi_warning_.get_value() > prop_Max.get_value())
					threshold_angle = maxAngle;
			}
			else
			{
				maxAngle = (maxDeg>minDeg? maxDeg:(maxDeg+2*M_PI));
				if(property_hi_alarm_on_)
				{
					threshold_start_angle = minDeg + (fabs(prop_Min.get_value()) + property_hi_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
					if(property_hi_alarm_.get_value() < prop_Min.get_value())
						threshold_start_angle = minDeg;
					else if(property_hi_alarm_.get_value() > prop_Max.get_value())
						threshold_start_angle = maxAngle;
				}
				else
					threshold_start_angle = maxAngle;
				threshold_angle = minDeg + (fabs(prop_Min.get_value()) + property_hi_warning_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_hi_warning_.get_value() < prop_Min.get_value())
					threshold_angle = minDeg;
				else if(property_hi_warning_.get_value() > prop_Max.get_value())
					threshold_angle = maxAngle;
			}

            cr->save();
            cr->set_line_width(abs(prop_scaleMinor_height.get_value()));
			for(double i=threshold_start_angle;prop_invert_rotate? i<threshold_angle:i>threshold_angle;)
            {
                double k;
                double red;
                double green;
                double blue;
                double transparent;
                double i_first = i;
				if(prop_invert_rotate)
				{
					if(i_first<threshold_angle && i_first>threshold_start_angle)
						i_first -= get_prop_threshold_offset_gradient()/R;//2*M_PI/720;
					i += minor_angle_step;
					if(i>threshold_angle)
						i = threshold_angle;
				}
				else
				{
					if(i_first>threshold_angle && i_first<threshold_start_angle)
						i_first += get_prop_threshold_offset_gradient()/R;//2*M_PI/720;
					i -= minor_angle_step;
					if(i<threshold_angle)
						i = threshold_angle;
				}

                double grad_x1 = pos_x - (R-(double)prop_scaleMinor_height.get_value()/2)*cos(i_first);
                double grad_y1 = pos_y - (R-(double)prop_scaleMinor_height.get_value()/2)*sin(i_first);
                double grad_x2 = pos_x - (R-(double)prop_scaleMinor_height.get_value()/2)*cos(i);
                double grad_y2 = pos_y - (R-(double)prop_scaleMinor_height.get_value()/2)*sin(i);
                Cairo::RefPtr<Cairo::LinearGradient> gradient = Cairo::LinearGradient::create(grad_x1, grad_y1, grad_x2, grad_y2);

				if(prop_invert_rotate)
					k = (threshold_angle - threshold_start_angle)!=0 ? ((i_first - threshold_start_angle)/(threshold_angle - threshold_start_angle)) : 1;
				else
					k = (threshold_start_angle-threshold_angle)!=0 ? ((threshold_start_angle-i_first)/(threshold_start_angle-threshold_angle)) : 1;
				red = get_property_hi_warning_color_2().get_red_p() + (get_property_hi_warning_color_1().get_red_p()-get_property_hi_warning_color_2().get_red_p())*k;
                green = get_property_hi_warning_color_2().get_green_p() + (get_property_hi_warning_color_1().get_green_p()-get_property_hi_warning_color_2().get_green_p())*k;
                blue = get_property_hi_warning_color_2().get_blue_p() + (get_property_hi_warning_color_1().get_blue_p()-get_property_hi_warning_color_2().get_blue_p())*k;
                transparent = get_property_hi_warning_color_2_transparent() + (get_property_hi_warning_color_1_transparent() - get_property_hi_warning_color_2_transparent())*k;
                gradient->add_color_stop_rgba(0.0,red,green,blue,transparent);

				if(prop_invert_rotate)
					k = (threshold_angle - threshold_start_angle)!=0 ? ((i - threshold_start_angle)/(threshold_angle - threshold_start_angle)) : 1;
				else
					k = (threshold_start_angle-threshold_angle)!=0 ? ((threshold_start_angle-i)/(threshold_start_angle-threshold_angle)) : 1;
                red = get_property_hi_warning_color_2().get_red_p() + (get_property_hi_warning_color_1().get_red_p()-get_property_hi_warning_color_2().get_red_p())*k;
                green = get_property_hi_warning_color_2().get_green_p() + (get_property_hi_warning_color_1().get_green_p()-get_property_hi_warning_color_2().get_green_p())*k;
                blue = get_property_hi_warning_color_2().get_blue_p() + (get_property_hi_warning_color_1().get_blue_p()-get_property_hi_warning_color_2().get_blue_p())*k;
                transparent = get_property_hi_warning_color_2_transparent() + (get_property_hi_warning_color_1_transparent() - get_property_hi_warning_color_2_transparent())*k;
                gradient->add_color_stop_rgba(1.0,red,green,blue,transparent);

//				cout<<get_name()<<"::draw_threshold::hi_warning i_first="<<i_first<<" i="<<i<<" threshold_start_angle="<<threshold_start_angle<<" threshold_angle="<<threshold_angle<<" minDeg="<<minDeg<<" maxAngle="<<maxAngle<<endl;
                cr->set_source(gradient);
				if(prop_invert_rotate)
					cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2,i_first-M_PI, i-M_PI);
				else
					cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2, i-M_PI,i_first-M_PI);
				cr->stroke();
            }
            cr->restore();
        }
        if(property_lo_alarm_on_)
        {
			double threshold_angle;
			if(prop_invert_rotate)
			{
				threshold_angle = maxDeg - (fabs(prop_Min.get_value()) + property_lo_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_lo_alarm_.get_value() < prop_Min.get_value())
					threshold_angle = maxDeg;
				else if(property_lo_alarm_.get_value() > prop_Max.get_value())
					threshold_angle = minDeg;
			}
			else
			{
				threshold_angle = minDeg + (fabs(prop_Min.get_value()) + property_lo_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_lo_alarm_.get_value() < prop_Min.get_value())
					threshold_angle = minDeg;
				else if(property_lo_alarm_.get_value() > prop_Max.get_value())
					threshold_angle = maxDeg;
			}

            cr->save();
            cr->set_line_width(abs(prop_scaleMinor_height.get_value()));
			for(double i=prop_invert_rotate? maxDeg:minDeg;prop_invert_rotate? i>threshold_angle:i<threshold_angle;)
            {
                double k;
                double red;
                double green;
                double blue;
                double transparent;
                double i_first = i;
				if(prop_invert_rotate)
				{
					if(i_first<maxDeg && i_first>threshold_angle)
						i_first += get_prop_threshold_offset_gradient()/R;//2*M_PI/720;
					i -= minor_angle_step;
					if(i<threshold_angle)
						i = threshold_angle;
				}
				else
				{
					if(i_first>minDeg && i_first<threshold_angle)
						i_first -= get_prop_threshold_offset_gradient()/R;//2*M_PI/720;
					i += minor_angle_step;
					if(i>threshold_angle)
						i = threshold_angle;
				}

                double grad_x1 = pos_x - (R-(double)prop_scaleMinor_height.get_value()/2)*cos(i_first);
                double grad_y1 = pos_y - (R-(double)prop_scaleMinor_height.get_value()/2)*sin(i_first);
                double grad_x2 = pos_x - (R-(double)prop_scaleMinor_height.get_value()/2)*cos(i);
                double grad_y2 = pos_y - (R-(double)prop_scaleMinor_height.get_value()/2)*sin(i);
                Cairo::RefPtr<Cairo::LinearGradient> gradient = Cairo::LinearGradient::create(grad_x1, grad_y1, grad_x2, grad_y2);

				if(prop_invert_rotate)
					k = (maxDeg - threshold_angle)!=0 ? ((maxDeg - i_first)/(maxDeg - threshold_angle)) : 1;
				else
					k = (threshold_angle-minDeg)!=0 ? ((i_first-minDeg)/(threshold_angle-minDeg)) : 1;
				red = get_property_lo_alarm_color_2().get_red_p() + (get_property_lo_alarm_color_1().get_red_p()-get_property_lo_alarm_color_2().get_red_p())*k;
                green = get_property_lo_alarm_color_2().get_green_p() + (get_property_lo_alarm_color_1().get_green_p()-get_property_lo_alarm_color_2().get_green_p())*k;
                blue = get_property_lo_alarm_color_2().get_blue_p() + (get_property_lo_alarm_color_1().get_blue_p()-get_property_lo_alarm_color_2().get_blue_p())*k;
                transparent = get_property_lo_alarm_color_2_transparent() + (get_property_lo_alarm_color_1_transparent() - get_property_lo_alarm_color_2_transparent())*k;
                gradient->add_color_stop_rgba(0.0,red,green,blue,transparent);

				if(prop_invert_rotate)
					k = (maxDeg - threshold_angle)!=0 ? ((maxDeg - i)/(maxDeg - threshold_angle)) : 1;
				else
					k = (threshold_angle-minDeg)!=0 ? ((i-minDeg)/(threshold_angle-minDeg)) : 1;
				red = get_property_lo_alarm_color_2().get_red_p() + (get_property_lo_alarm_color_1().get_red_p()-get_property_lo_alarm_color_2().get_red_p())*k;
                green = get_property_lo_alarm_color_2().get_green_p() + (get_property_lo_alarm_color_1().get_green_p()-get_property_lo_alarm_color_2().get_green_p())*k;
                blue = get_property_lo_alarm_color_2().get_blue_p() + (get_property_lo_alarm_color_1().get_blue_p()-get_property_lo_alarm_color_2().get_blue_p())*k;
                transparent = get_property_lo_alarm_color_2_transparent() + (get_property_lo_alarm_color_1_transparent() - get_property_lo_alarm_color_2_transparent())*k;
                gradient->add_color_stop_rgba(1.0,red,green,blue,transparent);

//				cout<<get_name()<<"::draw_threshold::lo_alarm i_first="<<i_first<<" i="<<i<<" threshold_angle="<<threshold_angle<<" minDeg="<<minDeg<<" maxDeg="<<maxDeg<<endl;
                cr->set_source(gradient);
				if(prop_invert_rotate)
					cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2,i-M_PI, i_first-M_PI);
				else
					cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2, i_first-M_PI,i-M_PI);
				cr->stroke();
            }
            cr->restore();
        }
        if(property_hi_alarm_on_)
        {
			double threshold_angle;
			double maxAngle;
			if(prop_invert_rotate)
			{
				threshold_angle = maxDeg - (fabs(prop_Min.get_value()) + property_hi_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				maxAngle = (maxDeg>minDeg? minDeg:(minDeg-2*M_PI));
				if(property_hi_alarm_.get_value() < prop_Min.get_value())
					threshold_angle = maxDeg;
				else if(property_hi_alarm_.get_value() > prop_Max.get_value())
					threshold_angle = maxAngle;
			}
			else
			{
				threshold_angle = minDeg + (fabs(prop_Min.get_value()) + property_hi_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				maxAngle = (maxDeg>minDeg? maxDeg:(maxDeg+2*M_PI));
				if(property_hi_alarm_.get_value() < prop_Min.get_value())
					threshold_angle = minDeg;
				else if(property_hi_alarm_.get_value() > prop_Max.get_value())
					threshold_angle = maxAngle;
			}

            cr->save();
            cr->set_line_width(abs(prop_scaleMinor_height.get_value()));
			for(double i=maxAngle;prop_invert_rotate? i<threshold_angle:i>threshold_angle;)
            {
                double k;
                double red;
                double green;
                double blue;
                double transparent;
                double i_first = i;
				if(prop_invert_rotate)
				{
					if(i_first<threshold_angle && i_first>maxAngle)
						i_first -= get_prop_threshold_offset_gradient()/R;//2*M_PI/720;
					i += minor_angle_step;
					if(i>threshold_angle)
						i = threshold_angle;
				}
				else
				{
					if(i_first>threshold_angle && i_first<maxAngle)
						i_first += get_prop_threshold_offset_gradient()/R;//2*M_PI/720;
					i-=minor_angle_step;
					if(i<threshold_angle)
						i = threshold_angle;
				}

                double grad_x1 = pos_x - (R-(double)prop_scaleMinor_height.get_value()/2)*cos(i_first);
                double grad_y1 = pos_y - (R-(double)prop_scaleMinor_height.get_value()/2)*sin(i_first);
                double grad_x2 = pos_x - (R-(double)prop_scaleMinor_height.get_value()/2)*cos(i);
                double grad_y2 = pos_y - (R-(double)prop_scaleMinor_height.get_value()/2)*sin(i);
                Cairo::RefPtr<Cairo::LinearGradient> gradient = Cairo::LinearGradient::create(grad_x1, grad_y1, grad_x2, grad_y2);

				if(prop_invert_rotate)
					k = (threshold_angle - maxAngle)!=0 ? ((i_first - maxAngle)/(threshold_angle - maxAngle)) : 1;
				else
					k = (maxAngle - threshold_angle)!=0 ? ((maxAngle - i_first)/(maxAngle - threshold_angle)) : 1;
				red = get_property_hi_alarm_color_2().get_red_p() + (get_property_hi_alarm_color_1().get_red_p()-get_property_hi_alarm_color_2().get_red_p())*k;
                green = get_property_hi_alarm_color_2().get_green_p() + (get_property_hi_alarm_color_1().get_green_p()-get_property_hi_alarm_color_2().get_green_p())*k;
                blue = get_property_hi_alarm_color_2().get_blue_p() + (get_property_hi_alarm_color_1().get_blue_p()-get_property_hi_alarm_color_2().get_blue_p())*k;
                transparent = get_property_hi_alarm_color_2_transparent() + (get_property_hi_alarm_color_1_transparent() - get_property_hi_alarm_color_2_transparent())*k;
                gradient->add_color_stop_rgba(0.0,red,green,blue,transparent);

				if(prop_invert_rotate)
					k = (threshold_angle - maxAngle)!=0 ? ((i - maxAngle)/(threshold_angle - maxAngle)) : 1;
				else
					k = (maxAngle - threshold_angle)!=0 ? ((maxAngle - i)/(maxAngle - threshold_angle)) : 1;
                red = get_property_hi_alarm_color_2().get_red_p() + (get_property_hi_alarm_color_1().get_red_p()-get_property_hi_alarm_color_2().get_red_p())*k;
                green = get_property_hi_alarm_color_2().get_green_p() + (get_property_hi_alarm_color_1().get_green_p()-get_property_hi_alarm_color_2().get_green_p())*k;
                blue = get_property_hi_alarm_color_2().get_blue_p() + (get_property_hi_alarm_color_1().get_blue_p()-get_property_hi_alarm_color_2().get_blue_p())*k;
                transparent = get_property_hi_alarm_color_2_transparent() + (get_property_hi_alarm_color_1_transparent() - get_property_hi_alarm_color_2_transparent())*k;
                gradient->add_color_stop_rgba(1.0,red,green,blue,transparent);

//				cout<<get_name()<<"::draw_threshold::hi_alarm i_first="<<i_first<<" i="<<i<<" threshold_angle="<<threshold_angle<<" minDeg="<<minDeg<<" maxAngle="<<maxAngle<<endl;
                cr->set_source(gradient);
				if(prop_invert_rotate)
					cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2,i_first-M_PI, i-M_PI);
				else
					cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2, i-M_PI,i_first-M_PI);
				cr->stroke();
            }
            cr->restore();
        }
    }
    else
    {
		if(property_lo_warning_on_)
        {
			double threshold_angle;
			if(prop_invert_rotate)
			{
				threshold_angle = maxDeg - (fabs(prop_Min.get_value()) + property_lo_warning_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_lo_warning_.get_value() < prop_Min.get_value())
					threshold_angle = maxDeg;
				else if(property_lo_warning_.get_value() > prop_Max.get_value())
					threshold_angle = minDeg;
			}
			else
			{
				threshold_angle = minDeg + (fabs(prop_Min.get_value()) + property_lo_warning_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_lo_warning_.get_value() < prop_Min.get_value())
					threshold_angle = minDeg;
				else if(property_lo_warning_.get_value() > prop_Max.get_value())
					threshold_angle = maxDeg;
			}

            cr->save();
            Gdk::Cairo::set_source_color(cr, property_lo_warning_color_1.get_value());
            cr->set_line_width(abs(prop_scaleMinor_height.get_value()));
			if(prop_invert_rotate)
				cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2,threshold_angle-M_PI, maxDeg-M_PI);
			else
				cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2, minDeg-M_PI,threshold_angle-M_PI);
			cr->stroke();
            cr->restore();
        }
        if(property_hi_warning_on_)
        {
			double threshold_angle;
			if(prop_invert_rotate)
			{
				threshold_angle = maxDeg - (fabs(prop_Min.get_value()) + property_hi_warning_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_hi_warning_.get_value() < prop_Min.get_value())
					threshold_angle = maxDeg;
				else if(property_hi_warning_.get_value() > prop_Max.get_value())
					threshold_angle = minDeg;
			}
			else
			{
				threshold_angle = minDeg + (fabs(prop_Min.get_value()) + property_hi_warning_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_hi_warning_.get_value() < prop_Min.get_value())
					threshold_angle = minDeg;
				else if(property_hi_warning_.get_value() > prop_Max.get_value())
					threshold_angle = maxDeg;
			}

            cr->save();
            Gdk::Cairo::set_source_color(cr, property_hi_warning_color_1.get_value());
            cr->set_line_width(abs(prop_scaleMinor_height.get_value()));
			if(prop_invert_rotate)
				cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2,minDeg-M_PI, threshold_angle-M_PI);
			else
				cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2, threshold_angle-M_PI,maxDeg-M_PI);
			cr->stroke();
            cr->restore();
        }
        if(property_lo_alarm_on_)
        {
			double threshold_angle;
			if(prop_invert_rotate)
			{
				threshold_angle = maxDeg - (fabs(prop_Min.get_value()) + property_lo_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_lo_alarm_.get_value() < prop_Min.get_value())
					threshold_angle = maxDeg;
				else if(property_lo_alarm_.get_value() > prop_Max.get_value())
					threshold_angle = minDeg;
			}
			else
			{
				threshold_angle = minDeg + (fabs(prop_Min.get_value()) + property_lo_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_lo_alarm_.get_value() < prop_Min.get_value())
					threshold_angle = minDeg;
				else if(property_lo_alarm_.get_value() > prop_Max.get_value())
					threshold_angle = maxDeg;
			}

            cr->save();
            Gdk::Cairo::set_source_color(cr, property_lo_alarm_color_1.get_value());
            cr->set_line_width(abs(prop_scaleMinor_height.get_value()));
			if(prop_invert_rotate)
				cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2,threshold_angle-M_PI, maxDeg-M_PI);
			else
				cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2, minDeg-M_PI,threshold_angle-M_PI);
			cr->stroke();
            cr->restore();
        }
        if(property_hi_alarm_on_)
        {
			double threshold_angle;
			if(prop_invert_rotate)
			{
				threshold_angle = maxDeg - (fabs(prop_Min.get_value()) + property_hi_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_hi_alarm_.get_value() < prop_Min.get_value())
					threshold_angle = maxDeg;
				else if(property_hi_alarm_.get_value() > prop_Max.get_value())
					threshold_angle = minDeg;
			}
			else
			{
				threshold_angle = minDeg + (fabs(prop_Min.get_value()) + property_hi_alarm_.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
				if(property_hi_alarm_.get_value() < prop_Min.get_value())
					threshold_angle = minDeg;
				else if(property_hi_alarm_.get_value() > prop_Max.get_value())
					threshold_angle = maxDeg;
			}

            cr->save();
            Gdk::Cairo::set_source_color(cr, property_hi_alarm_color_1.get_value());
            cr->set_line_width(abs(prop_scaleMinor_height.get_value()));
			if(prop_invert_rotate)
				cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2,minDeg-M_PI, threshold_angle-M_PI);
			else
				cr->arc( pos_x, pos_y, R-(double)prop_scaleMinor_height.get_value()/2, threshold_angle-M_PI,maxDeg-M_PI);
			cr->stroke();
            cr->restore();
        }
    }
}
// -------------------------------------------------------------------------
void URadialValueIndicator::set_sensor_ai(const ObjectId sens_id, const ObjectId node_id)
{
    /* Set new sens_id */
    sensor_ai_.set_sens_id(sens_id);

    /* Set new node_id */
    sensor_ai_.set_node_id(node_id);
}
// -------------------------------------------------------------------------
void URadialValueIndicator::set_precisions(const int precisions)
{
    set_property_precision(precisions);
}
// -------------------------------------------------------------------------
void
URadialValueIndicator::set_connector(const ConnectorRef& connector) throw()
{
    if (connector == get_connector())
        return;

    UVoid::set_connector(connector);

    sensor_ai_.set_connector(connector);

    sensor_connection_.disconnect();

    if (get_connector() == true) {
        sigc::slot<void, ObjectId, ObjectId, long> process_sensor_slot =
                sigc::mem_fun(this, &URadialValueIndicator::process_sensor);
        sensor_connection_ = get_connector()->signals().connect_value_changed(
                process_sensor_slot,
                sensor_ai_.get_sens_id(),
                sensor_ai_.get_node_id());
        on_connect();
    }
}
// -------------------------------------------------------------------------
void
URadialValueIndicator::on_connect() throw()
{
    UVoid::on_connect();

    float new_value = get_connector()->
        get_analog_value(sensor_ai_.get_sens_id(), sensor_ai_.get_node_id());

    process_sensor(sensor_ai_.get_sens_id(), sensor_ai_.get_node_id(), new_value);

    if(get_property_disconnect_effect() > 0)
        queue_draw();
}
// -------------------------------------------------------------------------
void URadialValueIndicator::on_realize()
{
//	cout<<get_name()<<"::on_realize..."<<endl;
    UEventBox::on_realize();
    {
        calculate_circle_param();
        double prec_value = get_property_value()/pow10(get_property_precision());
        last_value_angle = value_angle;
		if(prop_invert_rotate)
		{
			value_angle = maxDeg - (prec_value - prop_Min.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
			if(prec_value < prop_Min.get_value())
				value_angle = maxDeg;
			else if(prec_value > prop_Max.get_value())
				value_angle = minDeg<maxDeg ? minDeg:minDeg+2*M_PI;
		}
		else
		{
			value_angle = minDeg + (prec_value - prop_Min.get_value())/(prop_Max.get_value()-prop_Min.get_value())*angle*(M_PI/180);
			if(prec_value < prop_Min.get_value())
				value_angle = minDeg;
			else if(prec_value > prop_Max.get_value())
				value_angle = minDeg<maxDeg ? maxDeg:maxDeg+2*M_PI;
		}
        temp_value_angle = value_angle;
		inert.set_value( value_angle, value_angle );
		
		queue_draw();
    }

//	on_value_changed();
//	auto conf = uniset_conf();
//	if(conf)
//		process_sensor(sensor_ai_.get_sens_id(),sensor_ai_.get_node_id(),sensor_ai_.get_value());
}
// -------------------------------------------------------------------------
void
URadialValueIndicator::on_disconnect() throw()
{
    UVoid::on_connect();
    UVoid::on_disconnect();

    if(get_property_disconnect_effect() > 0)
        queue_draw();
}
// -------------------------------------------------------------------------
void
URadialValueIndicator::process_sensor(ObjectId id, ObjectId node, float value)
{
//	cout<<get_name()<<"::process_sensor id="<<id<<" node="<<node<<" value="<<value<<endl;
    set_property_value(value);
}
// -------------------------------------------------------------------------
