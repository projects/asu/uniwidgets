#ifndef _URADIALVALUEINDICATOR_H
#define _URADIALVALUEINDICATOR_H
// -------------------------------------------------------------------------
#include <uwidgets/UEventBox.h>
#include "UInertia.h"
#include "UQuantization.h"
#include "UCalibrate.h"
#include <global_macros.h>
#include <types.h>
// -------------------------------------------------------------------------
/* Color masks */
#define COLOR_RED_MASK		0xFF0000
#define COLOR_GREEN_MASK	0xFF00
#define COLOR_BLUE_MASK		0xFF
/* Bits offset*/
#define BITS_OFFSET		8
// -------------------------------------------------------------------------
/*!
 * \brief Класс цифрового индикатора.
 * \par
 * Класс предназначен для реализации виджета "цифровой индикатор".
*/
class URadialValueIndicator : public UEventBox
{
public:
	URadialValueIndicator();
	explicit URadialValueIndicator(GtkmmBaseType::BaseObjectType* gobject);
	~URadialValueIndicator() {}

	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void set_connector(const ConnectorRef& connector) throw();

	/* Set methods for widget properties */
	/*! задать аналоговый датчик для виджета.
	    \param sens_id id датчика.
	    \param node_id id узла датчика.
	*/
	void set_sensor_ai(const UniWidgetsTypes::ObjectId sens_id, const UniWidgetsTypes::ObjectId node_id);
	/*! задать точность после запятой отображаемого значения.
	    \param precisions заданная точность значения.
	*/
	void set_precisions(const int precisions);
	/*! задать количество цифр используемых как ширина поля отображения.
	    \param digits заданная ширина поля отображения.
	*/
	void set_digits(const int digits);
	/*! задать символ для заполнения неиспользуемых разрядов.
	    \param digits заданный заполнитель.
	*/
	void set_fill_digits(const int digits);
	/*! задать цвет текста.
	    \param font_color заданный цвет текста.
	*/
	void set_font_color(const long font_color);

    inline void set_value(double value) { set_property_value(value); }

    typedef sigc::slot<void, double> radialValueChangedSlot; // double - радиальное значение
    typedef sigc::signal<void, double> radialValueChangedSignal;
	
	virtual inline radialValueChangedSignal& signal_radial_value_change(){ return signal; }	/* */
	
protected:
	virtual void on_realize();
	virtual bool on_expose_event(GdkEventExpose*);
	virtual void calculate_circle_param();
	virtual void calculate_circle_param_scale_offset();
	virtual void calculate_circle_param_label_offset();
	virtual void calculate_circle_param_arrow_offset();
	virtual void draw_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_threshold(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_arrow(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void allocate_changed(Gtk::Allocation& alloc);
	virtual bool on_indicator_event(GdkEvent* ev);
	virtual float calculation_value_in_push_point();
	
	virtual void on_value_changed();

	radialValueChangedSignal signal;
	radialValueChangedSlot slot;
	
	sigc::connection sensor_connection_;
	SensorProp sensor_ai_;
	UCalibrate calibrate;
	
	double minDeg;
	double maxDeg;
	double H;
	double W;
	double R;
	double pos_x;
	double pos_y;
	double cosA;
	double cosB;
	double sinA;
	double sinB;
	int angle;

	bool is_show;
	bool action_start;
	
	double last_value_angle;
	double value_angle;
	double temp_value_angle;
	sigc::connection inert_signal;
	virtual void on_inert_value_changed(double val, bool inert_is_stoped);
	
	ADD_PROPERTY(property_value, double)
	ADD_PROPERTY(property_precision, int)
	UQuantization uquant;
	
	ADD_PROPERTY( prop_passiveMode, bool )		/*!< свойство: пассивность индмкатора (только отображает текущее значение)*/
	ADD_PROPERTY( prop_setValuePermanent, bool )/*!< свойство: выставлять значение датчика перманентно)*/
	
	ADD_PROPERTY( prop_Min, float )				/*!< свойство: значение соответствующее началу шкалы*/
	ADD_PROPERTY( prop_Max, float )				/*!< свойство: значение соответствующее концу шкалы*/
	ADD_PROPERTY( prop_maxMinor, int )			/*!< свойство: количество малых делений внутри больших*/
	ADD_PROPERTY( prop_maxMajor, int )			/*!< свойство: количество больших делений*/
	ADD_PROPERTY( prop_MinDeg, int )			/*!< свойство: начальный угол отсчета шкалы*/
	ADD_PROPERTY( prop_MaxDeg, int )			/*!< свойство: конечный угол отсчета шкалы*/
	ADD_PROPERTY( prop_enableScale, bool )			/*!< свойство: включить шкалу*/
	ADD_PROPERTY( prop_scalemark_width, int )     		/*!< свойство: толщина линий делений шкалы */
	ADD_PROPERTY( prop_scaleMinor_height, int )     	/*!< свойство: длинна малых делений*/
	ADD_PROPERTY( prop_scaleMajor_height, int )     	/*!< свойство: длинна больших делений*/
	ADD_PROPERTY( prop_scaleMinorColor, Gdk::Color )	/*!< свойство: цвет малых делений*/
	ADD_PROPERTY( prop_scaleMajorColor, Gdk::Color )	/*!< свойство: цвет больших делений*/
	
	ADD_PROPERTY( prop_enableScaleLabel, bool )			/*!< свойство: включить подписи значений шкалы*/
	ADD_PROPERTY( property_digits, int )				/*!< свойство: */
	ADD_PROPERTY( property_fill_digits, int )			/*!< свойство: */
	ADD_PROPERTY( prop_scaleLabelColor, Gdk::Color )	/*!< свойство: цвет подписей значений шкалы*/
	ADD_PROPERTY( prop_scaleLabelFont, Glib::ustring )	/*!< свойство: шрифт подписей значений шкалы*/
	ADD_PROPERTY( prop_scaleLabelFontSize, int )		/*!< свойство: размер шрифта подписей значений шкалы*/
	ADD_PROPERTY( prop_scaleLabelOffset, int )			/*!< свойство: отступ положения значений шкалы от делений*/
	
	ADD_PROPERTY( prop_enableArrow, bool )				/*!< свойство: включить стрелку*/
	ADD_PROPERTY( prop_enableArrowThreshold, bool )		/*!< свойство: отрисовывать пороги на стрелке*/
	ADD_PROPERTY( prop_arrow_useThresholdGradientColor, bool )	/*!< свойство: использовать градиент порогов для заливки цвета стрелки*/
	ADD_PROPERTY( prop_arrowOnTop, bool )				/*!< свойство: рисовать стрелку поверх шкалы и порогов*/
	ADD_PROPERTY( prop_ratio_arrowWidth_and_arrowHeight, double )	/*!< свойство: коэфициент отношения радиуса центрального круга стрелки к длинне стрелки (от 0.0 до 1.0)*/
	ADD_PROPERTY( prop_ratio_arrowHeight_and_arrowPeakHeight, double )/*!< свойство: коэфициент отношения высоты пика стрелки к высоте стрелки (от 0.0 до 1.0)*/
	ADD_PROPERTY( prop_ratio_arrowWidth_and_arrowPeakWidht, double )/*!< свойство: коэфициент отношения пика стрелки к длинне стрелки (от 0.0 до 1.0)*/
	ADD_PROPERTY( prop_ratio_arrowBaseHeight_and_arrowHeight, double )/*!< свойство: коэфициент отношения радиуса центрального круга стрелки к ширене основания стрелки (от 0.0 до 1.0)*/
	ADD_PROPERTY( prop_arrow_stroke_color, Gdk::Color )	/*!< свойство: цвет обводки стрелки*/
	ADD_PROPERTY( prop_arrow_stroke_width, int )		/*!< свойство: ширина обводки стрелки*/
	ADD_PROPERTY( prop_arrow_fill_color1, Gdk::Color )	/*!< свойство: цвет заливки стрелки*/
	ADD_PROPERTY( prop_arrow_fill_color2, Gdk::Color )	/*!< свойство: цвет2 заливки стрелки для градиента*/
	ADD_PROPERTY( prop_arrow_useGradientColor, bool )	/*!< свойство: использовать градиент для заливки цвета стрелки*/
	ADD_PROPERTY( prop_arrow_gradient_color1_pos, double )	/*!< свойство: коэфициент отношения положения начала градиента к длинне стрелки (от 0.0 до 1.0)*/
	ADD_PROPERTY( prop_arrow_gradient_color2_pos, double )	/*!< свойство: коэфициент отношения положения конца градиента к длинне стрелки (от 0.0 до 1.0)*/
	
	ADD_PROPERTY( prop_use_arrow_svgfile, bool )		/*!< свойство: использовать SVG картинку для стрелки*/
	ADD_PROPERTY( prop_arrow_svgfile, Glib::ustring )	/*!< свойство: путь к файлу SVG картинки стрелки*/
	ADD_PROPERTY( prop_hi_warning_svgfile, Glib::ustring )	/*!< свойство: путь к файлу SVG картинки стрелки для верхнего предупредительного уровня*/
	ADD_PROPERTY( prop_hi_alarm_svgfile, Glib::ustring )	/*!< свойство: путь к файлу SVG картинки стрелки для верхнего аварийного уровня*/
	ADD_PROPERTY( prop_lo_warning_svgfile, Glib::ustring )	/*!< свойство: путь к файлу SVG картинки стрелки для нижнего предупредительного уровня*/
	ADD_PROPERTY( prop_lo_alarm_svgfile, Glib::ustring )	/*!< свойство: путь к файлу SVG картинки стрелки для нижнего аварийного уровня*/
	ADD_PROPERTY( prop_svg_auto_scale, bool )		/*!< свойство: автоматически масштабировать SVG картинку для стрелки*/
	ADD_PROPERTY( prop_arrow_width, int )				/*!< свойство: ширина стрелки*/
	ADD_PROPERTY( prop_arrow_height, int )				/*!< свойство: высота стрелки*/
	ADD_PROPERTY( prop_arrow_rotate_x, double )			/*!< свойство: позиция по X точки вращения стрелки*/
	ADD_PROPERTY( prop_arrow_rotate_y, double )			/*!< свойство: позиция по Y точки вращения стрелки*/

	ADD_PROPERTY( prop_invert_rotate, bool )			/*!< свойство: вращение против/по часовой стрелки*/
	ADD_PROPERTY( prop_use_arrow_inert, bool )			/*!< свойство: использовать инерцию для стрелки*/
	UInertia inert;
	
	ADD_PROPERTY( prop_enableScaleThreshold, bool )			/*!< свойство: отрисовывать пороги на щкале*/
	ADD_PROPERTY( prop_threshold_useGradientColor, bool )	/*!< свойство: использовать градиент для заливки цвета порогов*/
	ADD_PROPERTY( prop_threshold_offset_gradient, double )	/*!< свойство: отступ градиента для заливки цвета порогов*/
	ADD_PROPERTY( property_hi_warning_on_, bool)
	ADD_PROPERTY( property_hi_warning_, float)
	ADD_PROPERTY( property_hi_warning_color_1, Gdk::Color)
	ADD_PROPERTY( property_hi_warning_color_1_transparent, double)
	ADD_PROPERTY( property_hi_warning_color_2, Gdk::Color)
	ADD_PROPERTY( property_hi_warning_color_2_transparent, double)
	ADD_PROPERTY( property_hi_alarm_on_, bool)
	ADD_PROPERTY( property_hi_alarm_, float)
	ADD_PROPERTY( property_hi_alarm_color_1, Gdk::Color)
	ADD_PROPERTY( property_hi_alarm_color_1_transparent, double)
	ADD_PROPERTY( property_hi_alarm_color_2, Gdk::Color)
	ADD_PROPERTY( property_hi_alarm_color_2_transparent, double)
	ADD_PROPERTY( property_lo_warning_on_, bool)
	ADD_PROPERTY( property_lo_warning_, float)
	ADD_PROPERTY( property_lo_warning_color_1, Gdk::Color)
	ADD_PROPERTY( property_lo_warning_color_1_transparent, double)
	ADD_PROPERTY( property_lo_warning_color_2, Gdk::Color)
	ADD_PROPERTY( property_lo_warning_color_2_transparent, double)
	ADD_PROPERTY( property_lo_alarm_on_, bool)
	ADD_PROPERTY( property_lo_alarm_, float)
	ADD_PROPERTY( property_lo_alarm_color_1, Gdk::Color)
	ADD_PROPERTY( property_lo_alarm_color_1_transparent, double)
	ADD_PROPERTY( property_lo_alarm_color_2, Gdk::Color)
	ADD_PROPERTY( property_lo_alarm_color_2_transparent, double)

private:

	void ctor();
	void process_sensor(UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, float);
};

#endif
