#include <pangomm/layout.h>

#include "UScale.h"
#include "uwidgets/UniOscillograph.h"

#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;
using namespace UScale;

// -------------------------------------------------------------------------
ScaleLabels::ScaleLabels(Gtk::PositionType p) :
position_(p),
is_show(false),
enabled_(true)
{
	set_visible_window(false);
	signal_event().connect(sigc::mem_fun(*this, &ScaleLabels::on_map_event));
	layout_= create_pango_layout("");
}
// -------------------------------------------------------------------------
ScaleLabels::~ScaleLabels()
{
}
// -------------------------------------------------------------------------
bool ScaleLabels::on_map_event(GdkEvent* ev)
{
/*	if(ev->type!=GDK_EXPOSE)
		std::cout<<get_name()<<"::on_map_event event->type="<<ev->type<<" GDK_MAP="<<GDK_MAP<< std::endl;*/
	if(ev->type==GDK_MAP)
		is_show = true;
	else if(ev->type==GDK_UNMAP)
		is_show = false;
	return false;
}
// -------------------------------------------------------------------------
/*! Set the labels to be drawn
 *
 *  The given map holds double values associated with int positions on
 *  a scale.  The given offs can be taken as a hint to calculate the
 *  right positions for the labels.  Normally, the leftmost or upmost
 *  border of the scale's Gdk::Window is taken for this offset.
 *
 *  \sa newsize_ (called automatically)
 */
void ScaleLabels::set_labels(int offs, const std::map<int,double> &labels)
{
	offset_= offs;
	labels_= labels;
	newsize_();
//	update_();
}
// -------------------------------------------------------------------------
void ScaleLabels::set_enabled(bool b)
{
	if (b==enabled_) return;
	enabled_= b;
	newsize_();
}
// -------------------------------------------------------------------------
void ScaleLabels::set_labels_color(Gdk::Color color)
{
	label_color = color;
}
// -------------------------------------------------------------------------
void ScaleLabels::set_labels_font(Pango::FontDescription fd)
{
	font_ = fd;
}

// -------------------------------------------------------------------------
Glib::ustring ScaleLabels::format(double d) const
{
	std::ostringstream tmp;
	tmp<<d;
	return tmp.str();
}
// -------------------------------------------------------------------------
int ScaleLabels::text_width(const Glib::ustring &str) const
{
	int layw,layh;
	layout_->set_font_description(font_);
	layout_->set_text(str);
	layout_->context_changed();
	layout_->get_pixel_size(layw,layh);
	return layw;
}
// -------------------------------------------------------------------------
int ScaleLabels::text_height(const Glib::ustring &str) const
{
	int layw,layh;
	layout_->set_font_description(font_);
	layout_->set_text(str);
	layout_->context_changed();
	layout_->get_pixel_size(layw,layh);
	return layh;
}
// -------------------------------------------------------------------------
VScaleLabels::VScaleLabels(Gtk::PositionType p) : ScaleLabels(p)
{
}
// -------------------------------------------------------------------------
VScaleLabels::~VScaleLabels()
{
}
// -------------------------------------------------------------------------
bool VScaleLabels::on_expose_event(GdkEventExpose* event)
{
	update_();
}
// -------------------------------------------------------------------------
HScaleLabels::HScaleLabels(Gtk::PositionType p) : ScaleLabels(p)
{
	use_real_time_flag = false;
	cur_pos = 0;
	last_time = time(0);
}
// -------------------------------------------------------------------------
HScaleLabels::~HScaleLabels()
{
}
// -------------------------------------------------------------------------
bool HScaleLabels::on_expose_event(GdkEventExpose* event)
{
	update_();
}
// -------------------------------------------------------------------------
void ScaleLabels::newsize_()
{
	if (!labels_.size()) return;
	if (!enabled()) {
		set_size_request(0,0);
		return;
	} else {
		layh_= text_height("8,8");
		layw_= 0;
		int min, max;
		min= labels_.begin()->first;
		max= labels_.begin()->first;
		std::map<int,double>::iterator daPos;
		for (daPos= labels_.begin(); daPos!=labels_.end(); daPos++)
		{
			layw_= MAX(layw_,text_width(format(daPos->second)));
			min= MIN(min,daPos->first);
			max= MAX(max,daPos->first);
		}
		range_= max-min;
	}
	requestsize_();
}
// -------------------------------------------------------------------------
void VScaleLabels::requestsize_()
{
	set_size_request(layw_,-1);
}
// -------------------------------------------------------------------------
void HScaleLabels::requestsize_()
{
	set_size_request(-1,layh_);
}
// -------------------------------------------------------------------------
void VScaleLabels::update_()
{
	if (!enabled()) return;
#if GTK_VERSION_GE(2,20)
	if(!get_mapped()) return;
#else
	if(!is_show) return;
#endif
	if (!labels_.size()) return;

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
	cr->translate(alloc.get_x(), alloc.get_y());
	int layw,layh;
	int winw=alloc.get_width();
	int winh=alloc.get_height();

	scaleMap_.set_int_range(winh-1,0);
	int i, iy;
	double y;
	std::map<int,double> lblpos;
	for (i=0;i<scaleDiv_.maj_count();i++)
	{
		y= scaleDiv_.maj_mark(i);
		iy= scaleMap_.transform(y);
		lblpos[iy]= y;
	}
	set_labels(0,lblpos);

	std::map<int,double>::iterator daPos;
	layout_->set_font_description(font_);
	for (daPos= labels_.begin(); daPos!=labels_.end(); daPos++)
	{
		if(daPos->second==0)
			layout_->set_text("0");
		else
			layout_->set_text(format(daPos->second));
		layout_->context_changed();
		layout_->get_pixel_size(layw,layh);
		int lx=0,ly=offset_-layh/2+daPos->first;
		if (position()==Gtk::POS_LEFT)
		{
			lx= winw-layw;
		}
		if (ly<0) ly=0;
		else if (ly+layh>winh) ly=winh-layh;

		Gdk::Cairo::set_source_color(cr, label_color);
		cr->move_to(lx, ly);
		layout_->add_to_cairo_context(cr);
		cr->fill();
	}
}
// -------------------------------------------------------------------------
void HScaleLabels::update_()
{
	if (!enabled()) return;
#if GTK_VERSION_GE(2,20)
	if(!get_mapped()) return;
#else
	if(!is_show) return;
#endif
	if (!labels_.size()) return;

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
	cr->translate(alloc.get_x(), alloc.get_y());
	int layw,layh;
	int winw=alloc.get_width();
	int winh=alloc.get_height();

	scaleMap_.set_int_range(0,winw-1);
	int i, ix;
	double x;
	std::map<int,double> lblpos;
	for (i=0;i<scaleDiv_.maj_count();i++)
	{
		x= scaleDiv_.maj_mark(i);
		ix= scaleMap_.transform(x);
		lblpos[ix]= x;
	}
	set_labels(0,lblpos);

	std::map<int,double>::iterator daPos;
	layout_->set_font_description(font_);
	for (daPos= labels_.begin(); daPos!=labels_.end(); daPos++)
	{
		if(use_real_time_flag)
		{

			int hour,min,sec;
			UniOscillograph::timeToInt(daPos->second - cur_pos,hour,min,sec,last_time);
			std::ostringstream time;
			time << std::setw(2) << std::setfill('0') << hour << ":";
			time << std::setw(2) << std::setfill('0') << min << ":";
			time << std::setw(2) << std::setfill('0') << sec;
			layout_->set_text(time.str());
		}
		else
		{
			if(daPos->second==0)
				layout_->set_text("0");
			else
				layout_->set_text(format(daPos->second));
		}
		layout_->context_changed();
		layout_->get_pixel_size(layw,layh);
		int lx=offset_-layw/2+daPos->first,ly=0;
		if (position()==Gtk::POS_TOP)
		{
			ly= winh-layh;
		}
		if (lx<0) lx=0;
		else if (lx+layw>winw) lx=winw-layw;

		Gdk::Cairo::set_source_color(cr, label_color);
		cr->move_to(lx, ly);
		layout_->add_to_cairo_context(cr);
		cr->fill();
	}
}
// -------------------------------------------------------------------------
/*! Constructor
 *
 *  Upon construction, the position for the Scale must be declared.
 *  Optionally, a reference to a ScaleLabels instance can be provided
 *  to allow for automatic label generation.  If the labels are actually
 *  shown can be set with ScaleLabels::set_enabled()
 */
Scale::Scale(Gtk::PositionType p, ScaleLabels *l) :
labels_(l),
position_(p),
enabled_(false),
is_show(false),
prop_scaleMinor_height(*this,"scale-minor-height",5),
prop_scaleMajor_height(*this, "scale-major-height" , 10),
maxMaj(10),
maxMin(10)
{
	set_visible_window(false);
	signal_event().connect(sigc::mem_fun(*this, &Scale::on_map_event));
	set_range(-1,1,false);
}

bool Scale::on_map_event(GdkEvent* ev)
{
	if(ev->type==GDK_MAP)
		is_show = true;
	else if(ev->type==GDK_UNMAP)
		is_show = false;
	return false;
}
// -------------------------------------------------------------------------
Scale::~Scale()
{
}// -------------------------------------------------------------------------
void Scale::set_position(Gtk::PositionType p)
{
	position_= p;
}
// -------------------------------------------------------------------------
void Scale::set_scale_color(Gdk::Color color)
{
	scale_color = color;
}
// -------------------------------------------------------------------------
void Scale::set_enabled(bool b)
{
	if (labels_) labels_->set_enabled(b);
	if (b==enabled_) return;
	enabled_= b;
	on_tick_change();
	signal_enabled(enabled_);
}
// -------------------------------------------------------------------------
void Scale::on_realize()
{
	Gtk::EventBox::on_realize();
	window_= get_window();
#if 0
	if (labels_) {
	labwin_= labels_->get_window();
}
#endif
	on_tick_change();
}
// -------------------------------------------------------------------------
void Scale::set_range(double l, double r)
{
	set_range(l,r,scaleMap_.logarithmic());
}
// -------------------------------------------------------------------------
void Scale::set_range(double l, double r, bool lg)
{
	scaleMap_.set_dbl_range(l,r,lg);
	scaleDiv_.rebuild(l, r, maxMaj, maxMin, scaleMap_.logarithmic(), 0.0);
	if(labels_)
	{
		labels_->set_scaleMap(scaleMap_);
		labels_->set_scaleDiv(scaleDiv_);
	}

//	redraw();
}
// -------------------------------------------------------------------------
void Scale::set_autoscale(bool b)
{
	autoscale_= b;
}
// -------------------------------------------------------------------------
void Scale::begin_autoscale()
{
	if (!autoscale_) return;
	asMin_= 1; asMax_= -1;
}
// -------------------------------------------------------------------------
void Scale::autoscale(double min, double max)
{
	if (!autoscale_) return;
	if (asMin_ > asMax_) {
		asMin_= MIN(min,max);
		asMax_= MAX(min,max);
	} else {
		asMin_= MIN(asMin_,MIN(min,max));
		asMax_= MAX(asMax_,MAX(min,max));
	}
}
// -------------------------------------------------------------------------
void Scale::end_autoscale()
{
	if (!autoscale_) return;
	if (asMin_ != asMax_) {
		set_range(asMin_,asMax_,scaleMap_.logarithmic());
	}
}
// -------------------------------------------------------------------------
bool Scale::on_expose_event(GdkEventExpose* event)
{
	redraw();
}
// -------------------------------------------------------------------------
VScale::VScale(Gtk::PositionType p, ScaleLabels *l) : Scale(p,l)
{
}
// -------------------------------------------------------------------------
VScale::~VScale()
{
}
// -------------------------------------------------------------------------
void VScale::on_tick_change()
{
	if (enabled())
		set_size_request(get_prop_scaleMajor_height(),-1);
	else
		set_size_request(1,-1);
}
// -------------------------------------------------------------------------
void VScale::redraw()
{
	if (!window_) return;
	if (!enabled()) return;
#if GTK_VERSION_GE(2,20)
	if(!get_mapped()) return;
#else
	if(!is_show) return;
#endif

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
	cr->translate(alloc.get_x(), alloc.get_y());
	int winw=alloc.get_width();
	int winh=alloc.get_height();

	scaleMap_.set_int_range(winh-1,0);
	int i, iy;
	double y;
	int l0= 0;
	int l1= get_prop_scaleMinor_height();
	int l2= get_prop_scaleMajor_height();
	if (position()==Gtk::POS_LEFT) {
		l0= winw-1;
		l1= l0-l1;
		l2= l0-l2;
	}
	std::map<int,double> lblpos;
	for (i=0;i<scaleDiv_.maj_count();i++)
	{
		y= scaleDiv_.maj_mark(i);
		iy= scaleMap_.transform(y);
		if (labels_) lblpos[iy]= y;
		cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
		cr->set_line_width(1);
		Gdk::Cairo::set_source_color(cr, scale_color);
		cr->move_to(l0, iy);
		cr->line_to(l2, iy);
		cr->stroke();
	}
	for (i=0;i<scaleDiv_.min_count();i++) {
		y= scaleDiv_.min_mark(i);
		iy= scaleMap_.transform(y);
		cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
		cr->set_line_width(1);
		Gdk::Cairo::set_source_color(cr, scale_color);
		cr->move_to(l0, iy);
		cr->line_to(l1, iy);
		cr->stroke();
	}
	cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
	cr->set_line_width(1);
	Gdk::Cairo::set_source_color(cr, scale_color);
	cr->move_to(l0, 0);
	cr->line_to(l0, winh-1);
	cr->stroke();

	if (labels_) labels_->set_labels(0,lblpos);
}
// -------------------------------------------------------------------------
HScale::HScale(Gtk::PositionType p, ScaleLabels *l) : Scale(p,l)
{
}
// -------------------------------------------------------------------------
HScale::~HScale()
{
}
// -------------------------------------------------------------------------
void HScale::on_tick_change()
{
	if (enabled())
		set_size_request(-1,get_prop_scaleMajor_height());
	else
		set_size_request(-1,1);
}
// -------------------------------------------------------------------------
void HScale::redraw()
{
	if (!window_) return;
	if (!enabled()) return;
#if GTK_VERSION_GE(2,20)
	if(!get_mapped()) return;
#else
	if(!is_show) return;
#endif

	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();

	Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
	cr->translate(alloc.get_x(), alloc.get_y());
	int winw=alloc.get_width();
	int winh=alloc.get_height();

	scaleMap_.set_int_range(0,winw-1);
	int i, ix;
	double x;
	int l0= 0;
	int l1= get_prop_scaleMinor_height();
	int l2= get_prop_scaleMajor_height();
	if (position()==Gtk::POS_TOP) {
		l0= winh-1;
		l1= l0-l1;
		l2= l0-l2;
	}

	std::map<int,double> lblpos;
	for (i=0;i<scaleDiv_.maj_count();i++)
	{
		x= scaleDiv_.maj_mark(i);
		ix= scaleMap_.transform(x);
		if (labels_) lblpos[ix]= x;
		cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
		cr->set_line_width(1);
		Gdk::Cairo::set_source_color(cr, scale_color);
		cr->move_to(ix, l0);
		cr->line_to(ix, l2);
		cr->stroke();
	}
	for (i=0;i<scaleDiv_.min_count();i++) {
		x= scaleDiv_.min_mark(i);
		ix= scaleMap_.transform(x);
		cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
		cr->set_line_width(1);
		Gdk::Cairo::set_source_color(cr, scale_color);
		cr->move_to(ix, l0);
		cr->line_to(ix, l1);
		cr->stroke();
	}
	cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
	cr->set_line_width(1);
	Gdk::Cairo::set_source_color(cr, scale_color);
	cr->move_to(0, l0);
	cr->line_to(winw-1, l0);
	cr->stroke();

	if (labels_) labels_->set_labels(0,lblpos);
}
// -------------------------------------------------------------------------
Web::Web() :
enabled_(false),
enabled_minor(false),
is_show(false),
weblinetype(LINE_LINE),
maxMajH(10),
maxMinH(10),
maxMajV(10),
maxMinV(10)
{
	set_visible_window(false);
	signal_event().connect(sigc::mem_fun(*this, &Web::on_map_event));
	set_h_range(-1,1,false);
	set_v_range(-1,1,false);
}
// -------------------------------------------------------------------------
Web::~Web()
{
}
// -------------------------------------------------------------------------
GType weblinetype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { Web::LINE, "LINE", "Линия" },{ Web::LINE_LINE, "LINE_LINE", "Пунктир" },{ Web::LINE_DOT_LINE, "LINE_DOT_LINE", "Тире-точка-тире" },{ Web::POINT, "POINT", "Точки" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("WebLineType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<Web::WebLineType>::value_type()
{
	return weblinetype_get_type();
}
// -------------------------------------------------------------------------
bool Web::on_map_event(GdkEvent* ev)
{
	if(ev->type==GDK_MAP)
		is_show = true;
	else if(ev->type==GDK_UNMAP)
		is_show = false;
	return false;
}
// -------------------------------------------------------------------------
void Web::set_web_major_color(Gdk::Color color)
{
	web_major_color = color;
}
// -------------------------------------------------------------------------
void Web::set_web_minor_color(Gdk::Color color)
{
	web_minor_color = color;
}
// -------------------------------------------------------------------------
void Web::set_enabled(bool b)
{
	if (b==enabled_) return;
	enabled_= b;
	signal_enabled(enabled_);
}
// -------------------------------------------------------------------------
void Web::set_enabled_minor(bool b)
{
	if (b==enabled_minor) return;
	enabled_minor= b;
}
// -------------------------------------------------------------------------
void Web::on_realize()
{
	Gtk::EventBox::on_realize();
	window_= get_window();
}
// -------------------------------------------------------------------------
void Web::set_h_range(double l, double r)
{
	set_h_range(l,r,HMap_.logarithmic());
}
// -------------------------------------------------------------------------
void Web::set_h_range(double l, double r, bool lg)
{
	HMap_.set_dbl_range(l,r,lg);
	HDiv_.rebuild(l, r, maxMajH, maxMinH, HMap_.logarithmic(), 0.0);

//	redraw();
}
// -------------------------------------------------------------------------
void Web::set_v_range(double l, double r)
{
	set_v_range(l,r,VMap_.logarithmic());
}
// -------------------------------------------------------------------------
void Web::set_v_range(double l, double r, bool lg)
{
	VMap_.set_dbl_range(l,r,lg);
	VDiv_.rebuild(l, r, maxMajV, maxMinV, VMap_.logarithmic(), 0.0);

//	redraw();
}
// -------------------------------------------------------------------------
bool Web::on_expose_event(GdkEventExpose* event)
{
	bool rv = EventBox::on_expose_event(event);
	redraw();
	return false;
}
// -------------------------------------------------------------------------
void Web::redraw()
{
	if (!window_) return;
	if (!enabled_) return;
#if GTK_VERSION_GE(2,20)
	if(!get_mapped()) return;
#else
	if(!is_show) return;
#endif
	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Gtk::Allocation alloc = get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
	cr->translate(alloc.get_x(), alloc.get_y());
	int winw=alloc.get_width();
	int winh=alloc.get_height();
//	std::cout<<"Web::redraw() winw="<<alloc.get_width()<<" winh="<<alloc.get_height()<<std::endl;
//	std::cout<<"Web::redraw() HDiv_.maj_count()="<<HDiv_.maj_count()<<" HDiv_.min_count()="<<HDiv_.min_count()<<" VDiv_.maj_count()="<<VDiv_.maj_count()<<" VDiv_.min_count()="<<VDiv_.min_count()<<std::endl;
	
	HMap_.set_int_range(0,winw-1);
	VMap_.set_int_range(winh-1,0);

	std::vector<double> HdashesMinor(6);
	std::vector<double> VdashesMinor(6);
	double HdashMinor=0;
	double firstHLine;
	double lastHLine;
	double VdashMinor=0;
	double firstVLine;
	double lastVLine;

	if(enabled_minor)
	{
		
	}
	if(weblinetype == LINE_LINE || weblinetype == LINE_DOT_LINE || weblinetype == POINT)
	{
// 		if(VDiv_.min_count()>1)
// 		{
// 			HdashMinor = MIN(abs(VMap_.transform(VDiv_.maj_mark(0)) - VMap_.transform(VDiv_.min_mark(0))),abs(VMap_.transform(VDiv_.min_mark(0)) - VMap_.transform(VDiv_.min_mark(1))));
// 		}
// 		if(HDiv_.min_count()>1)
// 		{
// 			VdashMinor = MIN(abs(HMap_.transform(HDiv_.maj_mark(0)) - HMap_.transform(HDiv_.min_mark(0))),abs(HMap_.transform(HDiv_.min_mark(0)) - HMap_.transform(HDiv_.min_mark(1))));
// 		}
		firstHLine = MIN(VMap_.transform(VDiv_.maj_mark(VDiv_.maj_count()-1)),VMap_.transform(VDiv_.min_mark(VDiv_.min_count()-1)));
		lastHLine = MAX(VMap_.transform(VDiv_.maj_mark(0)),VMap_.transform(VDiv_.min_mark(0)));
		HdashMinor = fabs(firstHLine - lastHLine)/(VDiv_.maj_count() + VDiv_.min_count() - 1);
//		std::cout<<"Web::redraw(H) HdashMinor="<<HdashMinor<<" 1="<<VMap_.transform(VDiv_.min_mark(1))<<" 0="<<VMap_.transform(VDiv_.min_mark(0))<<" 0maj="<<VMap_.transform(VDiv_.maj_mark(0))<<" winh="<<winh<<" maj="<<VDiv_.maj_count()<<" min="<<VDiv_.min_count()<<" firstHLine="<<firstHLine<<" lastHLine="<<lastHLine<<std::endl;
		if(weblinetype == POINT)
			HdashMinor = HdashMinor>1? HdashMinor:1;
		else
			HdashMinor = HdashMinor>4? HdashMinor:4;
		
		firstVLine = MIN(HMap_.transform(HDiv_.maj_mark(0)),HMap_.transform(HDiv_.min_mark(0)));
		lastVLine = MAX(HMap_.transform(HDiv_.maj_mark(HDiv_.maj_count()-1)),HMap_.transform(HDiv_.min_mark(HDiv_.min_count()-1)));
		VdashMinor = fabs(firstVLine - lastVLine)/(HDiv_.maj_count() + HDiv_.min_count() - 1);
//		std::cout<<"Web::redraw(V) VdashMinor="<<VdashMinor<<" 1="<<HMap_.transform(HDiv_.min_mark(1))<<" 0="<<HMap_.transform(HDiv_.min_mark(0))<<" 0maj="<<HMap_.transform(HDiv_.maj_mark(0))<<" winw="<<winw<<" maj="<<HDiv_.maj_count()<<" min="<<HDiv_.min_count()<<" firstVLine="<<firstVLine<<" lastVLine="<<lastVLine<<std::endl;
		VdashMinor = VdashMinor>4? VdashMinor:4;
		
		if(weblinetype == LINE_LINE || weblinetype == LINE_DOT_LINE)
		{
			if(HdashMinor > VdashMinor)
			{
				float k = HdashMinor/VdashMinor;
				if(lround(k)>=2)
					HdashMinor = HdashMinor/lround(k);
//				cout<<"Web::redraw(H) k="<<k<<" HdashMinor="<<HdashMinor<<endl;
			}
			else if(VdashMinor > HdashMinor)
			{
				float k = VdashMinor/HdashMinor;
				if(lround(k)>=2)
					VdashMinor = VdashMinor/lround(k);
//				cout<<"Web::redraw(V) k="<<k<<" VdashMinor="<<VdashMinor<<endl;
			}
		}
		
		if(weblinetype == LINE_LINE)
		{
			HdashesMinor[0] = HdashMinor/4;
			HdashesMinor[1] = HdashMinor/2;
			HdashesMinor[2] = HdashMinor/4;
			HdashesMinor[3] = 0;
			HdashesMinor[4] = 0;
			HdashesMinor[5] = 0;
			
			VdashesMinor[0] = VdashMinor/4;
			VdashesMinor[1] = VdashMinor/2;
			VdashesMinor[2] = VdashMinor/4;
			VdashesMinor[3] = 0;
			VdashesMinor[4] = 0;
			VdashesMinor[5] = 0;
		}
		else if(weblinetype == LINE_DOT_LINE)
		{
			HdashesMinor[0] = HdashMinor/4;
			HdashesMinor[1] = HdashMinor/4-0,5;
			HdashesMinor[2] = 1;
			HdashesMinor[3] = HdashMinor/4-0,5;
			HdashesMinor[4] = HdashMinor/4;
			HdashesMinor[5] = 0;
			
			VdashesMinor[0] = VdashMinor/4;
			VdashesMinor[1] = VdashMinor/4 - 0.5;
			VdashesMinor[2] = 1;
			VdashesMinor[3] = VdashMinor/4 - 0.5;
			VdashesMinor[4] = VdashMinor/4;
			VdashesMinor[5] = 0;
		}
		else if(weblinetype == POINT)
		{
			HdashesMinor[0] = 1;
			HdashesMinor[1] = HdashMinor-1;
			HdashesMinor[2] = 0;
			HdashesMinor[3] = 0;
			HdashesMinor[4] = 0;
			HdashesMinor[5] = 0;
		}
	}
	
	int i, ix;
	double x;
	cr->save();
	if(enabled_minor)
	{
		for (i=0;i<HDiv_.min_count();i++) {
			x= HDiv_.min_mark(i);
			ix= HMap_.transform(x);
			cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			cr->set_line_width(1);
			Gdk::Cairo::set_source_color(cr, web_minor_color);
			if(weblinetype == LINE_LINE || weblinetype == LINE_DOT_LINE || weblinetype == POINT)
				cr->set_dash(HdashesMinor, -firstHLine);
			cr->move_to(ix, 0);
			cr->line_to(ix, winh);
			cr->stroke();
		}
		if(weblinetype == LINE_LINE || weblinetype == LINE_DOT_LINE)
			cr->unset_dash();
	}
	cr->restore();
	cr->save();
	for (i=0;i<HDiv_.maj_count();i++) {
		x= HDiv_.maj_mark(i);
		ix= HMap_.transform(x);
		cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
		cr->set_line_width(1);
		Gdk::Cairo::set_source_color(cr, web_major_color);
		cr->move_to(ix, 0);
		cr->line_to(ix, winh);
		cr->stroke();
	}
	cr->restore();
	
	int j, jy;
	double y;
	cr->save();
	if(enabled_minor && weblinetype != POINT)
	{
		for (j=0;j<VDiv_.min_count();j++) {
			y= VDiv_.min_mark(j);
			jy= VMap_.transform(y);
			cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			cr->set_line_width(1);
			Gdk::Cairo::set_source_color(cr, web_minor_color);
			if(weblinetype == LINE_LINE || weblinetype == LINE_DOT_LINE)
				cr->set_dash(VdashesMinor, -firstVLine);
			cr->move_to(0, jy);
			cr->line_to(winw, jy);
			cr->stroke();
		}
		if(weblinetype == LINE_LINE || weblinetype == LINE_DOT_LINE)
			cr->unset_dash();
	}
	cr->restore();
	cr->save();
	for (j=0;j<VDiv_.maj_count();j++) {
		y= VDiv_.maj_mark(j);
		jy= VMap_.transform(y);
		cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
		cr->set_line_width(1);
		Gdk::Cairo::set_source_color(cr, web_major_color);
		cr->move_to(0, jy);
		cr->line_to(winw, jy);
		cr->stroke();
	}
	cr->restore();
}

namespace UScale {
	/*! 
	 *  \brief Find the smallest value in an array
	 *  \param array Pointer to an array
	 *  \param size Array size
	 */
	double array_min(double *array, int size)
	{
		if (size <= 0)
			return 0.0;
		
		double rv = array[0];
		for (int i = 1; i < size; i++)
			rv = MIN(rv, array[i]);
		
		return rv;
	}
	
	
	/*!
	 *  \brief Find the largest value in an array
	 *  \param array Pointer to an array
	 *  \param size Array size
	 */
	double array_max(double *array, int size)
	{
		if (size <= 0)
			return 0.0;
		
		double rv = array[0];
		for (int i = 1; i < size; i++)
			rv = MAX(rv, array[i]);
		
		return rv;
	}
	
	
	/*!
	 *  \brief Find the smallest value out of {1,2,5}*10^n with an integer number n
	 *  which is greater than or equal to x
	 * 
	 *  \param x Input value
	 */
	double ceil_125( double x)
	{
		if (x == 0.0) 
			return 0.0;
		
		const double sign = (x > 0) ? 1.0 : -1.0;
		const double lx = log10(fabs(x));
		const double p10 = floor(lx);
		
		double fr = pow(10.0, lx - p10);
		if (fr <=1.0)
			fr = 1.0;
		else if (fr <= 2.0)
			fr = 2.0;
		else if (fr <= 5.0)
			fr = 5.0;
		else
			fr = 10.0;
		
		return sign * fr * pow(10.0, p10);
	} 
	
	
	/*!
	 *  \brief Find the largest value out of {1,2,5}*10^n with an integer number n
	 *  which is smaller than or equal to x
	 *  
	 *  \param x Input value
	 */
	double floor_125( double x)
	{
		if (x == 0.0) 
			return 0.0;
		
		double sign = (x > 0) ? 1.0 : -1.0;
		const double lx = log10(fabs(x));
		const double p10 = floor(lx);
		
		double fr = pow(10.0, lx - p10);
		if (fr >= 10.0)
			fr = 10.0;
		else if (fr >= 5.0)
			fr = 5.0;
		else if (fr >= 2.0)
			fr = 2.0;
		else
			fr = 1.0;
		
		return sign * fr * pow(10.0, p10);
	} 
	
	
	/*!
	 *  \brief  Checks if an array is a strictly monotonic sequence
	 *  \param array Pointer to an array 
	 *  \param size Size of the array
	 *  \return
	 *  <dl>
	 *  <dt>0<dd>sequence is not strictly monotonic
	 *  <dt>1<dd>sequence is strictly monotonically increasing
	 *  <dt>-1<dd>sequence is strictly monotonically decreasing
	 *  </dl>
	 */
	int check_mono(double *array, int size)
	{
		if (size < 2) 
			return 0;
		
		int rv = SIGN(array[1] - array[0]);
		for (int i = 1; i < size - 1; i++)
		{
			if ( SIGN(array[i+1] - array[i]) != rv )
			{
				rv = 0;
				break;
			}
		}
		return rv;
	}
	
	/*!
	 *  \brief Invert the order of array elements
	 *  \param array Pointer to an array 
	 *  \param size Size of the array
	 */
	void twist_array(double *array, int size)
	{
		const int s2 = size / 2;
		
		for (int i=0; i < s2; i++)
		{
			const int itmp = size - 1 - i;
			const double dtmp = array[i];
			array[i] = array[itmp];
			array[itmp] = dtmp;
		}
	}
	
	/*!
	 *  \brief Invert the order of array elements
	 *  \param array Pointer to an array 
	 *  \param size Size of the array
	 */
	void twist_array(std::vector<double> &array)
	{
		int size= array.size();
		const int s2 = size/ 2;
		
		for (int i=0; i < s2; i++)
		{
			const int itmp = size - 1 - i;
			const double dtmp = array[i];
			array[i] = array[itmp];
			array[itmp] = dtmp;
		}
	}
	
	/*!
	 *  \brief Create an array of equally spaced values
	 *  \param array Where to put the values
	 *  \param size Size of the array
	 *  \param xmin Value associated with index 0
	 *  \param xmax Value associated with index (size-1)
	 */
	void lin_space(double *array, int size, double xmin, double xmax)
	{
		if (size <= 0)
			return;
		
		const int imax = size -1;
		
		array[0] = xmin;
		array[imax] = xmax;
		
		const double step = (xmax - xmin) / double(imax);
		const double tiny = 1e-6;
		
		for (int i = 1; i < imax; i++)
		{
			array[i] = xmin + double(i) * step;
			if (fabs(array[i]) < tiny*fabs(step))
				array[i] = step*floor(array[i]/step + tiny/2);
		}
	}
	
	/*!
	 *  \brief Create an array of logarithmically equally spaced values
	 *  \param array  Where to put the values
	 *  \param size   Size of the array
	 *  \param xmin Value associated with index 0
	 *  \param xmax   Value associated with index (size-1)
	 */
	void log_space(double *array, int size, double xmin, double xmax)
	{
		if ((xmin <= 0.0) || (xmax <= 0.0) || (size <= 0))
			return;
		
		const int imax = size -1;
		
		array[0] = xmin;
		array[imax] = xmax;
		
		const double lxmin = log(xmin);
		const double lxmax = log(xmax);
		const double lstep = (lxmax - lxmin) / double(imax);
		
		for (int i = 1; i < imax; i++)
			array[i] = exp(lxmin + double(i) * lstep);
	}
	
	
	/*!
	 *  \brief Create an array of equally spaced values
	 *  \param array Where to put the values
	 *  \param size Size of the array
	 *  \param xmin Value associated with index 0
	 *  \param xmax Value associated with index (size-1)
	 */
	void lin_space(std::vector<double> &array_, int size, 
				   double xmin, double xmax)
	{
		if (size<1) return;
		double array[size];
		lin_space(array,size,xmin,xmax);
		vector_from_c(array_,array,size);
	}
	
	/*!
	 *  \brief Create an array of logarithmically equally spaced values
	 *  \param array  Where to put the values
	 *  \param size   Size of the array
	 *  \param xmin Value associated with index 0
	 *  \param xmax   Value associated with index (size-1)
	 */
	void log_space(std::vector<double> &array_, int size, 
				   double xmin, double xmax)
	{
		if (size<1) return;
		double array[size];
		log_space(array,size,xmin,xmax);
		vector_from_c(array_,array,size);
	}
	
	/*!
	 *  \brief Create an array of logarithmically equally spaced values
	 *  \param array  Where to put the values
	 *  \param size   Size of the array
	 *  \param xmin Value associated with index 0
	 *  \param xmax   Value associated with index (size-1)
	 */
	void vector_from_c(std::vector<double> &array,const double *c, int size)
	{
		array.clear();
		if (size<1) return;
		for (int i=0; i<size; ++i)
			array.push_back(c[i]);
	}
	
	static const double step_eps = 1.0e-3;
	static const double border_eps = 1.0e-10;
	
	static bool range_limes(double &val, double v1, double v2, 
							double eps_rel = 0.0, double eps_abs = 0.0)
	{
		bool rv = true;
		double vmin = MIN(v1, v2);
		double vmax = MAX(v1, v2);
		double delta_min = MAX(ABS(eps_rel * vmin), ABS(eps_abs));
		double delta_max = MAX(ABS(eps_rel * vmax), ABS(eps_abs));
		
		if (val < vmin) 
		{
			if (val < vmin - delta_min) 
				rv = false;
			val = vmin;
		}
		else if (val > vmax)
		{
			if (val > vmax + delta_max) 
				rv = false;
			val = vmax;
		}
		return rv;
	}
	
	
	//! Construct a ScaleDiv instance.
	ScaleDiv::ScaleDiv()
	{
		d_lBound = 0.0;
		d_hBound = 0.0;
		d_majStep = 0.0;
		d_log = false;
	}
	
	/*!
	 *  \brief Build a scale width major and minor divisions
	 *  If no fixed step width is specified or if it is set to 0, the
	 *  major step width will be calculated automatically according to the
	 *  the value of maxMajSteps. The maxMajSteps parameter has no effect
	 *  if a fixed step size is specified. The minor step width is always
	 *  calculated automatically.
	 * 
	 *  If the step width is to be calculated automatically, the algorithm
	 *  tries to find reasonable values fitting into the scheme {1,2,5}*10^n
	 *  with an integer number n for linear scales.
	 * 
	 *  For logarithmic scales, there are three different cases
	 *  <ol>
	 *  <li>If the major step width is one decade, the minor marks
	 *      will fit into one of the schemes {1,2,...9}, {2,4,6,8}, {2,5} or {5},
	 *      depending on the maxMinSteps parameter.
	 *  <li>If the major step size spans
	 *      more than one decade, the minor step size will be {1,2,5}*10^n decades
	 *      with a natural number n.
	 *  <li>If the whole range is less than one decade, a linear scale
	 *      division will be built.
	 *  </ol>
	 * 
	 *  \param x1 first boundary value
	 *  \param x2 second boundary value
	 *  \param maxMajSteps max. number of major step intervals
	 *  \param maxMinSteps max. number of minor step intervals
	 *  \param log logarithmic division (true/false)
	 *  \param step  fixed major step width. Defaults to 0.0.
	 *  \param ascend if true, sort in ascending order from min(x1, x2)
	 *                to max(x1, x2). If false, sort in the direction
	 *        from x1 to x2. Defaults to true.
	 *  \return True if the arrays have been allocated successfully.
	 *  \warning For logarithmic scales, the step width is measured in decades.
	 */
	bool ScaleDiv::rebuild(double x1, double x2, 
						   int maxMajSteps, int maxMinSteps, bool log, double step, bool ascend)
	{
		int rv;
		
		d_lBound = MIN(x1, x2);
		d_hBound = MAX(x1, x2);
		d_log = log;
		
		if (d_log)
			rv = build_log_div_(maxMajSteps,maxMinSteps,step);
		else
			rv = build_lin_div_(maxMajSteps, maxMinSteps, step);
		
		if ((!ascend) && (x2 < x1))
		{
			d_lBound = x1;
			d_hBound = x2;
			twist_array(majMarks_);
			twist_array(minMarks_);
		}
		
		return rv;
	}
	
	/*!
	 *  \brief Build a linear scale division in ascending order
	 * 
	 *  If the 'step' parameter is set to 0.0, this function
	 *  cal[culates the step width automatically according to
	 *  the value of 'maxSteps'. MaxSteps must be greater than or
	 *  equal to 2. It will be guessed if an invalid value is specified.
	 * 
	 *  The maximum possible number of steps is   limited to 10000.
	 *  The maxSteps parameter has no effect if a fixed step width is
	 *  specified.
	 * 
	 *  \param maxSteps max. number of step intervals
	 *  \param step -- fixed step width
	 *  \return true if array has been successfully resized
	 *  \warning This function uses the data members d_lBound and d_hBound
	 *  and assumes that d_hBound > d_lBound.
	 */
	bool ScaleDiv::build_lin_div_(int maxMajSteps, int maxMinSteps, double step)
	{
		int nMaj, nMin, minSize, i0,i,k;
		double val, mval;
		double firstTick, lastTick;
		double minStep;
		
		// parameter range check
		maxMajSteps = MAX(1, maxMajSteps);
		maxMinSteps = MAX(0, maxMinSteps);
		step = ABS(step);
		
		// detach arrays
		if (d_lBound == d_hBound) {
			majMarks_.clear();
			minMarks_.clear();
			return true;
		}
		//
		// Set up major divisions
		//
		if (step == 0.0)
			d_majStep = ceil_125(ABS(d_hBound - d_lBound) * 0.999999
			/ double(maxMajSteps));
		else
			d_majStep = step;
		
		if (d_majStep == 0.0) return true;
		
		firstTick = ceil( (d_lBound - step_eps * d_majStep) / d_majStep) * d_majStep;
		lastTick = floor( (d_hBound + step_eps * d_majStep) / d_majStep) * d_majStep;
		
		nMaj = MIN(10000, int(floor ((lastTick - firstTick) / d_majStep + 0.5)) + 1);
		
		lin_space(majMarks_, nMaj, firstTick, lastTick);
		
		//
		// Set up minor divisions
		//
		if (maxMinSteps < 1) // no minor divs
			return true;
		
		minStep = ceil_125( d_majStep  /  double(maxMinSteps) );
		
		if (minStep == 0.0) return true;
		
		nMin = ABS(int(floor(d_majStep / minStep + 0.5))) - 1; // # minor steps per interval
		
		// Do the minor steps fit into the interval?
		if ( ABS(double(nMin +  1) * minStep - d_majStep) 
			>  step_eps * d_majStep)
		{
			nMin = 1;
			minStep = d_majStep * 0.5;
		}
		
		// Are there minor ticks below the first major tick?
		if (majMarks_[0] > d_lBound )
			i0 = -1; 
		else
			i0 = 0;
		
		double buffer[nMin * (nMaj + 1)];
		// resize buffer to the maximum possible number of minor ticks
		
		// calculate minor ticks
		minSize = 0;
		for (i = i0; i < (int)majMarks_.size(); i++)
		{
			if (i >= 0)
				val = majMarks_[i];
			else
				val = majMarks_[0] - d_majStep;
			
			for (k=0; k< nMin; k++)
			{
				mval = (val += minStep);
				if (range_limes(mval, d_lBound, d_hBound, border_eps))
				{
					buffer[minSize] = mval;
					minSize++;
				}
			}
		}
		vector_from_c(minMarks_, buffer, minSize);
		
		return true;
	}
	
	/*!
	 *  \brief Build a logarithmic scale division
	 *  \return True if memory has been successfully allocated
	 *  \warning This function uses the data members d_lBound and d_hBound
	 *  and assumes that d_hBound > d_lBound.
	 */
	bool ScaleDiv::build_log_div_(int maxMajSteps, int maxMinSteps, double majStep)
	{
		double firstTick, lastTick;
		double lFirst, lLast;
		double val, sval, minStep, minFactor;
		int nMaj, nMin, minSize, i, k, k0, kstep, kmax, i0;
		int rv = true;
		double width;
		
		std::vector<double> buffer;
		
		
		// Parameter range check
		maxMajSteps = MAX(1, ABS(maxMajSteps));
		maxMinSteps = MAX(0, ABS(maxMinSteps));
		majStep = ABS(majStep);
		
		// boundary check
		range_limes(d_hBound, LOG_MIN, LOG_MAX);
		range_limes(d_lBound, LOG_MIN, LOG_MAX);
		
		// detach arrays
		majMarks_.clear();
		minMarks_.clear();
		
		if (d_lBound == d_hBound) return true;
		
		// scale width in decades
		width = log10(d_hBound) - log10(d_lBound);
		
		// scale width is less than one decade -> build linear scale
		if (width < 1.0) 
		{
			rv = build_lin_div_(maxMajSteps, maxMinSteps, 0.0);
			// convert step width to decades
			if (d_majStep > 0)
				d_majStep = log10(d_majStep);
			
			return rv;
		}
		
		//
		//  Set up major scale divisions
		//
		if (majStep == 0.0)
			d_majStep = ceil_125( width * 0.999999 / double(maxMajSteps));
		else
			d_majStep = majStep;
		
		// major step must be >= 1 decade
		d_majStep = MAX(d_majStep, 1.0);
		
		
		lFirst = ceil((log10(d_lBound) - step_eps * d_majStep) / d_majStep) * d_majStep;
		lLast = floor((log10(d_hBound) + step_eps * d_majStep) / d_majStep) * d_majStep;
		
		firstTick = pow(10.0, lFirst);
		lastTick = pow(10.0, lLast);
		
		nMaj = MIN(10000, int(floor (ABS(lLast - lFirst) / d_majStep + 0.5)) + 1);
		
		log_space(majMarks_, nMaj, firstTick, lastTick);
		
		//
		// Set up minor scale divisions
		//
		
		if ((majMarks_.size() < 1) || (maxMinSteps < 1)) return true; // no minor marks
		
		if (d_majStep < 1.1)            // major step width is one decade
		{
			if (maxMinSteps >= 8)
			{
				k0 = 2;
				kmax = 9;
				kstep = 1;
				minSize = (majMarks_.size() + 1) * 8;
			}
			else if (maxMinSteps >= 4)
			{
				k0 = 2;
				kmax = 8;
				kstep = 2;
				minSize = (majMarks_.size() + 1) * 4;
			}
			else if (maxMinSteps >= 2)
			{
				k0 = 2;
				kmax = 5;
				kstep = 3;
				minSize = (majMarks_.size() + 1) * 2;
			}
			else
			{
				k0 = 5;
				kmax = 5;
				kstep = 1;
				minSize = (majMarks_.size() + 1);
			}
			
			// resize buffer to the max. possible number of minor marks
			buffer.clear();
			
			// Are there minor ticks below the first major tick?
			if ( d_lBound < firstTick )
				i0 = -1;
			else
				i0 = 0;
			
			minSize = 0;
			for (i = i0; i< (int)majMarks_.size(); i++)
			{
				if (i >= 0)
					val = majMarks_[i];
				else
					val = majMarks_[0] / pow(10.0, d_majStep);
				
				for (k=k0; k<= kmax; k+=kstep)
				{
					sval = val * double(k);
					if (range_limes(sval, d_lBound, d_hBound, border_eps))
					{
						buffer.push_back(sval);
						minSize++;
					}
				}
			}
			
			// copy values into the min_marks array
			minMarks_= buffer;
			
		}
		else                // major step > one decade
		{
			
			// substep width in decades, at least one decade
			minStep = ceil_125( (d_majStep - step_eps * (d_majStep / double(maxMinSteps)))
			/  double(maxMinSteps) );
			minStep = MAX(1.0, minStep);
			
			// # subticks per interval
			nMin = int(floor (d_majStep / minStep + 0.5)) - 1;
			
			// Do the minor steps fit into the interval?
			if ( ABS( double(nMin + 1) * minStep - d_majStep)  >  step_eps * d_majStep)
				nMin = 0;
			
			if (nMin < 1) return true;      // no subticks
			
			// resize buffer to max. possible number of subticks
			buffer.clear();
			
			// substep factor = 10^substeps
			minFactor = MAX(pow(10.0, minStep), 10.0);
			
			// Are there minor ticks below the first major tick?
			if ( d_lBound < firstTick )
				i0 = -1;
			else
				i0 = 0;
			
			minSize = 0;
			for (i = i0; i< (int)majMarks_.size(); i++)
			{
				if (i >= 0)
					val = majMarks_[i];
				else
					val = firstTick / pow(10.0, d_majStep);
				
				for (k=0; k< nMin; k++)
				{
					sval = (val *= minFactor);
					if (range_limes(sval, d_lBound, d_hBound, border_eps))
					{
						buffer.push_back(sval);
						minSize++;
					}
				}
			}
			minMarks_= buffer;
		}
		
		return rv;
	}
	
	/*!
	 *  \brief Equality operator
	 *  \return true if this instance is equal to s
	 */
	int ScaleDiv::operator==(const ScaleDiv &s) const
	{
		if (d_lBound != s.d_lBound) return 0;
		if (d_hBound != s.d_hBound) return 0;
		if (d_log != s.d_log) return 0;
		if (d_majStep != s.d_majStep) return 0;
		if (majMarks_ != s.majMarks_) return 0;
		return (minMarks_ == s.minMarks_);
	}
	
	/*!
	 *  \brief Inequality
	 *  \return true if this instance is not equal to s
	 */
	int ScaleDiv::operator!=(const ScaleDiv &s) const
	{
		return (!(*this == s));
	}
	
	//! Detach the shared data and set everything to zero. 
	void ScaleDiv::reset()
	{
		// detach arrays
		majMarks_.clear();
		minMarks_.clear();
		
		d_lBound = 0.0;
		d_hBound = 0.0;
		d_majStep = 0.0;
		d_log = false;
	}
	
	const double DoubleIntMap::LogMin = 1.0e-150;
	const double DoubleIntMap::LogMax = 1.0e150;
	
	/*!
	 *  \brief Constructor
	 * 
	 *  The double and integer intervals are both set to [0,1].
	 */
	DoubleIntMap::DoubleIntMap()
	{
		d_x1 = 0.0;
		d_x2 = 1.0;
		d_y1 = 0;
		d_y2 = 1;
		d_cnv = 1.0;
	}
	
	
	/*!
	 *  \brief Constructor
	 * 
	 *  Constructs a DoubleIntMap instance with initial integer
	 *  and double intervals
	 * 
	 *  \param i1 first border of integer interval
	 *  \param i2 second border of integer interval
	 *  \param d1 first border of double interval
	 *  \param d2 second border of double interval
	 *  \param logarithmic logarithmic mapping, true or false.
	 */ 
	DoubleIntMap::DoubleIntMap(int i1, int i2, 
							   double d1, double d2, bool logarithmic)
	{
		d_log = logarithmic;
		set_int_range(i1,i2);
		set_dbl_range(d1, d2);
	}
	
	/*!
	 *  \brief Destructor
	 */
	DoubleIntMap::~DoubleIntMap()
	{
	}
	
	/*!
	 *  \return true if a value x lies inside or at the border of the
	 *  map's double range.
	 *  \param x value
	 */
	bool DoubleIntMap::contains(double x) const
	{
		return ( (x >= MIN(d_x1, d_x2)) && (x <= MAX(d_x1, d_x2)));
	}
	
	/*!
	 *  \return true if a value x lies inside or at the border of the
	 *    map's integer range
	 *  \param x value
	 */
	bool DoubleIntMap::contains(int x) const
	{
		return ( (x >= MIN(d_y1, d_y2)) && (x <= MAX(d_y1, d_y2)));
	}
	
	/*!
	 *  \brief Specify the borders of the double interval
	 *  \param d1 first border
	 *  \param d2 second border 
	 *  \param lg logarithmic (true) or linear (false) scaling
	 */
	void DoubleIntMap::set_dbl_range(double d1, double d2, bool lg)
	{
		if (lg)
		{
			d_log = true;
			if (d1 < LogMin) 
				d1 = LogMin;
			else if (d1 > LogMax) 
				d1 = LogMax;
			
			if (d2 < LogMin) 
				d2 = LogMin;
			else if (d2 > LogMax) 
				d2 = LogMax;
			
			d_x1 = log(d1);
			d_x2 = log(d2);
		}
		else
		{
			d_log = false;
			d_x1 = d1;
			d_x2 = d2;
		}
		newFactor();
	}
	
	/*!
	 *  \brief Specify the borders of the integer interval
	 *  \param i1 first border
	 *  \param i2 second border
	 */
	void DoubleIntMap::set_int_range(int i1, int i2)
	{
		d_y1 = i1;
		d_y2 = i2;
		newFactor();
	}
	
	/*!
	 *  \brief Transform an integer value into a double value
	 *  \param y integer value to be transformed
	 *  \return
	 *  <dl>
	 *  <dt>linear mapping:<dd>d1 + (d2 - d1) / (i2 - i1) * (y - i1)
	 *  <dt>logarithmic mapping:<dd>d1 + (d2 - d1) / log(i2 / i1) * log(y / i1)
	 *  </dl>
	 */
	double DoubleIntMap::inv_transform(int y) const 
	{
		if (d_cnv == 0.0)
			return 0.0;
		else
		{
			if(d_log) 
				return exp(d_x1 + double(y - d_y1) / d_cnv );
			else
				return ( d_x1 + double(y - d_y1) / d_cnv );  
		}
	}
	
	/*! 
	 *  \brief  Transform and limit
	 * 
	 *  The function is similar to DoubleIntMap::transform, but limits the input value
	 *  to the nearest border of the map's double interval if it lies outside
	 *  that interval.
	 * 
	 *  \param x value to be transformed
	 *  \return transformed value
	 */
	int DoubleIntMap::lim_transform(double x) const
	{
		if ( x > MAX(d_x1, d_x2) )
			x = MAX(d_x1, d_x2);
		else if ( x < MIN(d_x1, d_x2))
			x = MIN(d_x1, d_x2);
		return transform(x);
	}
	
	/*!
	 *  \brief Exact transformation
	 * 
	 *  This function is similar to DoubleIntMap::transform, but
	 *  makes the integer interval appear to be double. 
	 *  \param x value to be transformed
	 *  \return 
	 *  <dl>
	 *  <dt>linear mapping:<dd>i1 + (i2 - i1) / (d2 - d1) * (x - d1)
	 *  <dt>logarithmic mapping:<dd>i1 + (i2 - i1) / log(d2 / d1) * log(x / d1)
	 *  </dl>
	 */
	double DoubleIntMap::x_transform(double x) const
	{
		double rv;
		
		if (d_log)
			rv = double(d_y1) + (log(x) - d_x1) * d_cnv;    
		else
			rv = double(d_y1) + (x - d_x1) * d_cnv;
		
		return rv;
	}
	
	
	/*!
	 *  \brief Re-calculate the conversion factor.
	 */
	void DoubleIntMap::newFactor()
	{
		if (d_x2 != d_x1)
			d_cnv = double(d_y2 - d_y1) / (d_x2 - d_x1); 
		else 
			d_cnv = 0.0;
	}
} // namespace UScale
