/* -*- mode: C++ ; c-file-style: "stroustrup" -*- *****************************
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the LGPL
 *****************************************************************************/

#ifndef USCALE_H
#define USCALE_H

#include <vector>
#include <map>

#include <sigc++/sigc++.h>

#include <gtkmm.h>

#include <global_macros.h>

namespace Pango {
    class Layout;
    class FontDescription;
}

namespace UScale
{
	#ifndef LOG10_2
	#define LOG10_2     0.30102999566398119802  /* log10(2) */
	#endif
	
	#ifndef LOG10_3
	#define LOG10_3     0.47712125471966243540  /* log10(3) */
	#endif
	
	#ifndef LOG10_5
	#define LOG10_5     0.69897000433601885749  /* log10(5) */
	#endif
	
	#ifndef M_2PI
	#define M_2PI       6.28318530717958623200  /* 2 pi */
	#endif
	
	#ifndef LOG_MIN
	#define LOG_MIN 1.0e-100
	#endif
	
	#ifndef LOG_MAX
	#define LOG_MAX 1.0e100
	#endif
	
	#ifndef M_E
	#define M_E            2.7182818284590452354   /* e */
	#endif
	
	#ifndef M_LOG2E
	#define M_LOG2E 1.4426950408889634074   /* log_2 e */
	#endif
	
	#ifndef M_LOG2E
	#define M_LOG10E    0.43429448190325182765  /* log_10 e */
	#endif
	
	#ifndef M_LN2
	#define M_LN2       0.69314718055994530942  /* log_e 2 */
	#endif
	
	#ifndef M_LN10
	#define M_LN10         2.30258509299404568402  /* log_e 10 */
	#endif
	
	#ifndef M_PI
	#define M_PI        3.14159265358979323846  /* pi */
	#endif
	
	#ifndef M_PI_2
	#define M_PI_2      1.57079632679489661923  /* pi/2 */
	#endif
	
	#ifndef M_PI_4
	#define M_PI_4      0.78539816339744830962  /* pi/4 */
	#endif
	
	#ifndef M_1_PI
	#define M_1_PI      0.31830988618379067154  /* 1/pi */
	#endif
	
	#ifndef M_2_PI
	#define M_2_PI      0.63661977236758134308  /* 2/pi */
	#endif
	
	#ifndef M_2_SQRTPI
	#define M_2_SQRTPI  1.12837916709551257390  /* 2/sqrt(pi) */
	#endif
	
	#ifndef M_SQRT2
	#define M_SQRT2 1.41421356237309504880  /* sqrt(2) */
	#endif
	
	#ifndef M_SQRT1_2
	#define M_SQRT1_2   0.70710678118654752440  /* 1/sqrt(2) */
	#endif
	
	double ceil_125(double x);
	double floor_125(double x);
	double array_min (double *array, int size);
	double array_max( double *array, int size);
	void twist_array(double *array, int size);
	void twist_array(std::vector<double> &);
	int check_mono(double *array, int size);
	void lin_space(double *array, int size, double xmin, double xmax);
	void log_space(double *array, int size, double xmin, double xmax);
	void lin_space(std::vector<double>&,int size,double xmin,double xmax);
	void log_space(std::vector<double>&,int size,double xmin,double xmax);
	void vector_from_c(std::vector<double> &array,const double *c, int size);
	
	#ifndef MAX
	#define MAX(a,b) ((b) < (a) ? (a) : (b))
	#endif
	#ifndef MIN
	#define MIN(a,b) ((a) < (b) ? (a) : (b))
	#endif
	#ifndef ABS
	#define ABS(a) ((a) >= 0 ? (a) : -(a))
	#endif
	
	inline int ROUND( double d )
	{
		return d >= 0.0 ? 
		int(d + 0.5) : 
		int( d - ((int)d-1) + 0.5 ) + ((int)d-1);
	}
	
	
	
	//! Return the sign 
	template <class T>
	inline int SIGN(const T& x)
	{
		if (x > T(0))
			return 1;
		else if (x < T(0))
			return (-1);
		else
			return 0;
	}            
	
	/*! Copy an array into another
	 * 
	 *  \param dest Destination
	 *  \param src Source
	 *  \param n Number of elements  
	 */
	template <class T>
	void copy_array(T *dest, T *src, int n) 
	{
		int i;
		for (i=0; i<n;i++ )
			dest[i] = src[i];
	}
	
	/*! Sort two values in ascending order
	 * 
	 *  \param x1 First input value
	 *  \param x2 Second input value
	 *  \param xmax Greater value
	 *  \param xmin Smaller value
	 */
	template <class T>
	void sort_values(const T& x1, const T& x2, T& xmin, T& xmax)
	{
		T buffer;
		
		if (x2 < x1)
		{
			buffer = x1;
			xmin = x2;
			xmax = buffer;
		}
		else
		{
			xmin = x1;
			xmax = x2;
		}
	}
	
	//! Sort two values in ascending order 
	template <class T>
	void sort_values(T& x1, T& x2)
	{
		T buffer;
		
		if (x2 < x1)
		{
			buffer = x1;
			x1 = x2;
			x2 = buffer;
		}
	}
	
	/*! Limit a value to fit into a specified interval
	 * 
	 *  \param x Input value
	 *  \param x1 First interval boundary
	 *  \param x2 Second interval boundary  
	 */
	template <class T>
	T value_limes(const T& x, const T& x1, const T& x2)
	{
		T rv;
		T xmin, xmax;
		
		xmin = MIN(x1, x2);
		xmax = MAX(x1, x2);
		
		if ( x < xmin )
			rv = xmin;
		else if ( x > xmax )
			rv = xmax;
		else
			rv = x;
		
		return rv;
	}
	/*
	 *      inline Gdk::Point polar_to_position(const Gdk::Point &center,
	 *      double radius, double angle)
	 *      {
	 *      const double x = center.get_x() + radius * cos(angle);
	 *      const double y = center.get_y() - radius * sin(angle);
	 * 
	 *      return Gdk::Point(ROUND(x), ROUND(y));
}

inline Gdk::Point degree_to_position(const Gdk::Point &center,
double radius, double angle)
{
return polar_to_position(center, radius, angle / 180.0 * M_PI);
}
*/
	
	class ScaleDiv
    {
		public:
			ScaleDiv ();

		int operator== (const ScaleDiv &s) const;
		int operator!= (const ScaleDiv &s) const;
	
		/*! 
		\return left bound
		\sa ScaleDiv::rebuild
		*/
		double low_bound() const { return d_lBound; }
		/*! 
		\return right bound
		\sa ScaleDiv::rebuild
		*/
		double high_bound() const { return d_hBound; }
		/*! 
		\return minor mark count
		\sa ScaleDiv::rebuild
		*/
		unsigned int min_count() const { return minMarks_.size(); }
		/*! 
		\return major mark count
		\sa ScaleDiv::rebuild
		*/
		unsigned int maj_count() const { return majMarks_.size(); }

		/*! 
		\return TRUE id ths scale div is logarithmic
		\sa ScaleDiv::rebuild
		*/
		bool log_scale() const { return d_log; }

		//! Return major mark at position i
		double maj_mark(int i) const { return majMarks_[i]; }
		//! Return minor mark at position i
		double min_mark(int i) const { return minMarks_[i]; }

		/*! 
		\return major step size
		\sa ScaleDiv::rebuild
		*/
		double maj_step() const { return d_majStep; }
		void reset();

		bool rebuild(double lBound, double hBound, int maxMaj, int maxMin,bool log, double step = 0.0, bool ascend = true);
	
		private:
		bool build_lin_div_(int maxMajMark, int maxMinMark, double step = 0.0);
		bool build_log_div_(int maxMajMark, int maxMinMark, double step = 0.0);

		double d_lBound;
		double d_hBound;
		double d_majStep;
		bool d_log;

		std::vector<double> majMarks_;
		std::vector<double> minMarks_;
    };
	
	/*! @brief Map a double interval into an integer interval
	 * 
	 *  The DoubleIntMap class maps an interval of type double into an
	 *  interval of type int. It consists of two intervals D = [d1,
	 *  d2] (double) and I = [i1, i2] (int), which are specified with
	 *  the DoubleIntMap::set_dbl_range and DoubleIntMap::set_int_range
	 *  members. The point d1 is mapped to the point i1, and d2 is
	 *  mapped to i2. Any point inside or outside D can be mapped to a
	 *  point inside or outside I using DoubleIntMap::transform or
	 *  DoubleIntMap::lim_transform or vice versa using
	 *  Plot::inv_transform. D can be scaled linearly or
	 *  logarithmically, as specified with DoubleIntMap::set_dbl_range.
	 *   
	 *  <b>Usage</b>
	 *  \verbatim
	 *  #include <plotmm/doubleintmap.h>
	 *  
	 *  DoubleIntMap map;
	 *  int ival;
	 *  double dval;
	 *
	 *  // Assign an interval of type double with linear mapping
	 *  map.set_dbl_range(0.0, 3.1415); 
	 *  
	 *  // Assign an integer interval
	 *  map.set_int_range(0,100); 
	 *
	 *  // obtain integer value corresponding to 1.0
	 *  ival = map.transform(1.0);    
	 *  
	 *  // obtain double value corresponding to 77
	 *  dval = map.inv_transform(77);    
	 *  \endverbatim
	 */
	
	class DoubleIntMap
	{
	public:
		DoubleIntMap();
		DoubleIntMap(int i1, int i2, double d1, double d2, bool lg = false);
		~DoubleIntMap();
		
		bool contains(double x) const;
		bool contains(int x) const;
		
		void set_int_range(int i1, int i2);
		void set_dbl_range(double d1, double d2, bool lg = false);
		
		int transform(double x) const;
		double inv_transform(int i) const;
		
		int lim_transform(double x) const;
		double x_transform(double x) const;
		
		inline double d1() const;
		inline double d2() const;
		inline int i1() const;
		inline int i2() const;
		inline bool logarithmic() const;
		
		static const double LogMin;
		static const double LogMax;
		
	private:
		void newFactor();   
		
		double d_x1, d_x2;  // double interval boundaries
		int d_y1, d_y2;     // integer interval boundaries
		double d_cnv;       // conversion factor
		bool d_log;     // logarithmic scale?
	};
	
	/*!
	 *  \return the first border of the double interval
	 */
	inline double DoubleIntMap::d1() const 
	{
		return d_x1;
	}
	
	/*!
	 *  \return the second border of the double interval
	 */
	inline double DoubleIntMap::d2() const 
	{
		return d_x2;
	}
	
	/*!
	 *  \return the second border of the integer interval
	 */
	inline int DoubleIntMap::i1() const 
	{
		return d_y1;
	}
	
	/*!
	 *  \return the second border of the integer interval
	 */
	inline int DoubleIntMap::i2() const 
	{
		return d_y2;
	}
	
	/*!
	 *  \return TRUE if the double interval is scaled logarithmically
	 */
	inline bool DoubleIntMap::logarithmic() const 
	{
		return d_log;
	}
	
	/*! Transform a point in double interval into an point in the
	 *  integer interval
	 *
	 *  \param x value
	 *  \return
	 *  <dl>
	 *  <dt>linear mapping:
	 *      <dd>rint(i1 + (i2 - i1) / (d2 - d1) * (x - d1))
	 *  <dt>logarithmic mapping:
	 *      <dd>rint(i1 + (i2 - i1) / log(d2 / d1) * log(x / d1))
	 *  </dl>
	 *  \warning The specified point is allowed to lie outside the
	 *  intervals. If you want to limit the returned value, use
	 *  DoubleIntMap::lim_transform.
	 */
	inline int DoubleIntMap::transform(double x) const
	{
		if (d_log)
			return d_y1 + ROUND((log(x) - d_x1) * d_cnv);
		else
			return d_y1 + ROUND((x - d_x1) * d_cnv);
	}
} // namespace UScale
	
    /*! @brief Class to draw scale labels
	 *
     */
    class ScaleLabels : public Gtk::EventBox {
	    public:
		    ScaleLabels(Gtk::PositionType p);
		    virtual ~ScaleLabels();

	//! Return a pointer to the labels' font
		    Pango::FontDescription *font() { return &font_; }
		    void set_labels(int offs, const std::map<int,double> &labels);

		    void set_labels_color(Gdk::Color color);
		    void set_labels_font(Pango::FontDescription fd);
		    void set_enabled(bool b);
	//! Return if the labels are enabled
		    bool enabled() const { return enabled_; }
		    Glib::ustring format(double) const;
		    int text_width(const Glib::ustring &) const;
		    int text_height(const Glib::ustring &) const;
	//! Return where the labels are positioned
		    inline Gtk::PositionType position(){ return position_; };

		    inline void set_scaleDiv(const UScale::ScaleDiv &Div){ scaleDiv_ = Div;};
		    inline void set_scaleMap(const UScale::DoubleIntMap &Map){ scaleMap_ = Map;};

	    protected:
		    virtual void newsize_();
		    virtual void requestsize_() {}
		    virtual bool on_map_event(GdkEvent *event);

		    int offset_;
		    int layw_, layh_;
		    int range_;
		    std::map<int,double> labels_;
		    Glib::RefPtr<Pango::Layout> layout_;
		    Pango::FontDescription font_;
		    Gdk::Color label_color;
		    bool is_show;

		    UScale::ScaleDiv scaleDiv_;
		    UScale::DoubleIntMap scaleMap_;

	    private:
		    Gtk::PositionType position_;
		    bool enabled_;
    };

    /*! @brief Class to draw vertical scale labels
     *
     */
    class VScaleLabels : public ScaleLabels {
	    public:
		    VScaleLabels(Gtk::PositionType p);
		    virtual ~VScaleLabels();

	    protected:
		    virtual bool on_expose_event(GdkEventExpose* event);
		    virtual void update_();
		    virtual void requestsize_();
    };

    /*! @brief Class to draw horizintal scale labels
     *
     */
    class HScaleLabels : public ScaleLabels {
	    public:
		    HScaleLabels(Gtk::PositionType p);
		    virtual ~HScaleLabels();

		    inline void set_curPos(double pos)
		    {
			    cur_pos = pos;
				last_time = time(0);
		    };
			inline double get_curPos(){ return cur_pos;};
			inline time_t get_lastTime(){ return last_time;};
			inline void use_real_time(bool state){ use_real_time_flag = state;};

	    protected:
		    virtual bool on_expose_event(GdkEventExpose* event);
		    virtual void update_();
		    virtual void requestsize_();
		    double cur_pos;
		    time_t last_time;
		    bool use_real_time_flag;
    };



    /*! @brief Class to draw a scale.
     *
     */
    class Scale : public Gtk::EventBox
    {
	    public:
		    Scale(Gtk::PositionType p, ScaleLabels *labels= 0);
		    virtual ~Scale();

		    void set_position(Gtk::PositionType p);
	//! Return where the scale is positioned
		    Gtk::PositionType position() const { return position_; }

		    void set_scale_color(Gdk::Color color);

	//! Return the labels of scale
		    inline ScaleLabels* get_labels(){return labels_;};

		    void set_enabled(bool b);
	//! Query if the scale is enabled (i.e. visible)
		    bool enabled() const { return enabled_; }
		    inline void set_max_major(int max){maxMaj = max;};
		    inline void set_max_minor(int max){maxMin = max;};

	//! Query the ScaleMap used by this scale
		    const UScale::DoubleIntMap &scale_map() const { return scaleMap_; }

		    void set_range(double l, double r);
		    void set_range(double l, double r, bool lg);

		    void set_autoscale(bool b);
	//! Query if autoscaling is enabled
		    bool autoscale() const { return autoscale_; }
		    void begin_autoscale();
		    void autoscale(double min, double max);
		    void end_autoscale();

	/*! This signal is thrown whenever the scale is enabled or disabled
		     *  \sa set_enabled, enabled
	 */
		    sigc::signal1<void,bool> signal_enabled;

	    protected:
		    virtual void on_realize();
		    virtual bool on_expose_event(GdkEventExpose* event);
		    virtual bool on_map_event(GdkEvent *event);

		    /*! implement this to react on changes of tick lengths */
		    virtual void on_tick_change() {}
		    virtual void redraw() = 0;

		    Glib::RefPtr<Gdk::Window> window_;
		    ScaleLabels *labels_;
		    Gdk::Color scale_color;
		    int maxMaj;
		    int maxMin;
		    bool is_show;

		    UScale::ScaleDiv scaleDiv_;
		    UScale::DoubleIntMap scaleMap_;

			ADD_PROPERTY( prop_scaleMinor_height, int )     	/*!< свойство: длинна малых делений*/
			ADD_PROPERTY( prop_scaleMajor_height, int )     	/*!< свойство: длинна больших делений*/
			
	    private:
		    Gtk::PositionType position_;
		    bool autoscale_;
		    bool enabled_;
		    double asMin_, asMax_;
    };

    /*! @brief Class to draw a vertical scale.
     *
     */
    class VScale : public Scale
    {
	    public:
		    VScale(Gtk::PositionType p, ScaleLabels *l= 0);
		    virtual ~VScale();

	    protected:
		    virtual void redraw();
		    virtual void on_tick_change();
    };

    /*! @brief Class to draw a horizontal scale.
     *
     */
    class HScale : public Scale
    {
	    public:
		    HScale(Gtk::PositionType p, ScaleLabels *l= 0);
		    virtual ~HScale();

	    protected:
		    virtual void redraw();
		    virtual void on_tick_change();
    };

        /*! @brief Class to draw a scale.
     	*
	 */
    class Web : public Gtk::EventBox
    {
	    public:
		    Web();
		    virtual ~Web();

			enum WebLineType
			{
				LINE=0
				,LINE_LINE
				,LINE_DOT_LINE
				,POINT
			};
			
			void set_web_major_color(Gdk::Color color);
			void set_web_minor_color(Gdk::Color color);
			
			void set_enabled(bool b);
			void set_enabled_minor(bool b);
			//! Query if the scale is enabled (i.e. visible)
		    bool enabled() const { return enabled_; }
		    inline void set_web_line_type(int type){weblinetype = type;};
			inline void set_max_h_major(int max){maxMajH = max;};
		    inline void set_max_h_minor(int max){maxMinH = max;};
		    inline void set_max_v_major(int max){maxMajV = max;};
		    inline void set_max_v_minor(int max){maxMinV = max;};

	//! Query the ScaleMap used by this scale
		    const UScale::DoubleIntMap &h_map() const { return HMap_; }
		    const UScale::DoubleIntMap &v_map() const { return VMap_; }

		    void set_h_range(double l, double r);
		    void set_h_range(double l, double r, bool lg);
		    void set_v_range(double l, double r);
		    void set_v_range(double l, double r, bool lg);

	/*! This signal is thrown whenever the scale is enabled or disabled
		     *  \sa set_enabled, enabled
	 */
		    sigc::signal1<void,bool> signal_enabled;

	    protected:
		    virtual void on_realize();
		    virtual bool on_expose_event(GdkEventExpose* event);
		    virtual bool on_map_event(GdkEvent *event);

		    virtual void redraw();

			Gdk::Color web_major_color;
			Gdk::Color web_minor_color;
			int weblinetype;
		    int maxMajH;
		    int maxMinH;
		    int maxMajV;
		    int maxMinV;
		    bool is_show;

		    Glib::RefPtr<Gdk::Window> window_;

		    UScale::ScaleDiv HDiv_;
		    UScale::DoubleIntMap HMap_;
		    UScale::ScaleDiv VDiv_;
		    UScale::DoubleIntMap VMap_;

	    private:
		    bool enabled_;
			bool enabled_minor;
	};

	namespace Glib
	{
		template <>
		class Value<Web::WebLineType> : public Value_Enum<Web::WebLineType>
		{
		public:
			static GType value_type() G_GNUC_CONST;
		};
	} // namespace Glib
	
#endif
