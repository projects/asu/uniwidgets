#include "types.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <gdkmm.h>
#include <librsvg/rsvg.h>
//#include <librsvg/rsvg-cairo.h>
#include "UScrollScale.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using Glib::ustring;
// -------------------------------------------------------------------------
#define UScrollScale_INIT_PROPERTIES() \
    sensor_ai_(this,"ai", get_connector()) \
    ,calibrate(this) \
    ,property_value(*this,"value",0) \
    ,property_precision(*this,"precision",0) \
    \
    ,prop_use_inert(*this, "use-inertia",false) \
    ,inert(this) \
    \
    ,prop_Position(*this, "scale-position", Gtk::POS_RIGHT ) \
    \
    ,prop_loopScale(*this, "loop-scale",false) \
    \
    ,prop_minLimited(*this, "min-scale-limited", false) \
    ,prop_Min(*this,"min-value",0) \
    ,prop_maxLimited(*this, "max-scale-limited", false) \
    ,prop_Max(*this,"max-value",100) \
    ,prop_visibleRange(*this, "scale-visible-range" , 10) \
    ,prop_maxMinor(*this, "max-scale-minor" , 10) \
    ,prop_maxMajor(*this, "max-scale-major" , 10) \
    ,prop_scalemarkWidth(*this,"scale-width",1) \
    ,prop_scaleMinorHeight(*this,"scale-minor-height",2) \
    ,prop_scaleMajorHeight(*this, "scale-major-height" , 3) \
    ,prop_scaleMinorColor(*this, "scale-minor-color" , Gdk::Color("black")) \
    ,prop_scaleMajorColor(*this, "scale-major-color" , Gdk::Color("red")) \
    \
    ,prop_useImage(*this,"use-image",false) \
    ,prop_image(*this,"image","") \
    ,prop_imageMin(*this,"min-value-for-image",0) \
    ,prop_imageMax(*this,"max-value-for-image",100) \
    \
    ,property_digits(*this,"scale-label-digits",0) \
    ,property_fill_digits(*this,"scale-label-fill-digits",0) \
    ,prop_scaleLabelColor(*this, "scale-label-color", Gdk::Color("black") ) \
    ,prop_scaleLabelFont(*this, "scale-label-font", "Liberation Sans Bold" ) \
    ,prop_scaleLabelFontSize(*this, "scale-label-font-size", 12 ) \
    ,prop_Alignment(*this, "scale-label-alignment", CENTER ) \
    ,prop_alignmentEdge(*this, "scale-label-alignment-edge", false) \
    ,prop_scaleLabelOffset(*this, "scale-label-offset", 0 ) \
    \
    ,prop_useExceptions(*this,"use-exceptions", false) \
    ,prop_scaleExceptions(*this, "scale-exceptions" , "") \
    ,prop_exceptionsColor(*this, "exceptions-color", Gdk::Color("black") ) \
    ,prop_exceptionsFont(*this, "exceptions-font", "Liberation Sans Bold" ) \
    ,prop_exceptionsFontSize(*this, "exceptions-font-size", 12 ) \
    \
    ,prop_enableValueLabel(*this,"enable-value-label", false) \
    ,prop_valueBGMinSize(*this,"value-bg-min-size", 30) \
    ,prop_useValueBGImage(*this,"use-value-bg-image",false) \
    ,prop_valueBGImagePath(*this,"value-bg-image","") \
    ,prop_valueBGUseGradientColor(*this, "value-bg-use-gradient-color", true) \
    ,prop_valueBGStrokeUseGradientColor(*this, "value-bg-stroke-use-gradient-color", true) \
    ,prop_valueBGFillColor1(*this, "value-bg-fill-color1", Gdk::Color("white") ) \
    ,prop_valueBGFillColor2(*this, "value-bg-fill-color2", Gdk::Color("gray") ) \
    ,prop_valueBGRounding(*this,"value-bg-rounding", 0.5) \
    ,prop_valueBGStrokeWidth(*this,"value-bg-stroke-width", 2) \
    ,prop_valueLabelUnits(*this, "value-label-units", "" ) \
    ,prop_valueLabelColor(*this, "value-label-color", Gdk::Color("black") ) \
    ,prop_valueLabelFont(*this, "value-label-font", "Liberation Sans Bold" ) \
    ,prop_valueLabelFontSize(*this, "value-label-font-size", 12 ) \
    \
    ,is_show(false) \
    ,action_start(false) \
    \
    ,last_value(0) \
    ,value(0) \
    ,temp_value(0) \

// -------------------------------------------------------------------------
void UScrollScale::ctor()
{
    set_visible_window(false);

	value_layout= create_pango_layout(" ");
	value_layout->set_alignment(Pango::ALIGN_CENTER);
	
    connect_property_changed("value", sigc::mem_fun(*this, &UScrollScale::on_value_changed) );
	connect_property_changed("enable-calibrate", sigc::mem_fun(*this, &UScrollScale::on_value_changed) );
	connect_property_changed("enable-calibrate-limit", sigc::mem_fun(*this, &UScrollScale::on_value_changed) );
	connect_property_changed("calibrate-rmin", sigc::mem_fun(*this, &UScrollScale::on_value_changed) );
	connect_property_changed("calibrate-rmax", sigc::mem_fun(*this, &UScrollScale::on_value_changed) );
	connect_property_changed("calibrate-cmin", sigc::mem_fun(*this, &UScrollScale::on_value_changed) );
	connect_property_changed("calibrate-cmax", sigc::mem_fun(*this, &UScrollScale::on_value_changed) );
	connect_property_changed("precision", sigc::mem_fun(*this, &UScrollScale::on_value_changed) );

	connect_property_changed("scale-position", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("min-scale-limited", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("min-value", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("max-scale-limited", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("max-value", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("scale-visible-range", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("scale-exceptions", sigc::mem_fun(*this, &UScrollScale::on_exceptions_changed) );
	connect_property_changed("max-scale-minor", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("max-scale-major", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("scale-width", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("scale-minor-height", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("scale-major-height", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("scale-minor-color", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	connect_property_changed("scale-major-color", sigc::mem_fun(*this, &UScrollScale::queue_draw) );
	
    signal_size_allocate().connect(sigc::mem_fun(*this, &UScrollScale::allocate_changed));
    set_events(~Gdk::ALL_EVENTS_MASK);
//    signal_event().connect(sigc::mem_fun(*this, &UScrollScale::on_scale_event));
}
// -------------------------------------------------------------------------
UScrollScale::UScrollScale() :
    Glib::ObjectBase("uscrollscale")
    ,UScrollScale_INIT_PROPERTIES()
{
    ctor();
}
// -------------------------------------------------------------------------
UScrollScale::UScrollScale(GtkmmBaseType::BaseObjectType* gobject) :
    UEventBox(gobject)
	,UScrollScale_INIT_PROPERTIES()
{
    ctor();
}
// -------------------------------------------------------------------------
GType aligntype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { UScrollScale::CENTER, "CENTER", "центр" },{ UScrollScale::TOP, "TOP", "сверху" },{ UScrollScale::BOTTOM, "BOTTOM", "снизу" },{ UScrollScale::LEFT, "LEFT", "слева" },{ UScrollScale::RIGHT, "RIGHT", "справа" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("AlignType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UScrollScale::AlignType>::value_type()
{
	return aligntype_get_type();
}
// -------------------------------------------------------------------------
void UScrollScale::on_realize()
{
	//	cout<<get_name()<<"::on_realize..."<<endl;
	UEventBox::on_realize();
	
	on_exceptions_changed();

	double prec_value = get_property_value()/pow10(get_property_precision());
	last_value = value;
	value = prec_value;
	if(prop_useImage)
	{
		if(prec_value < prop_imageMin.get_value())
			value = prop_imageMin.get_value();
		else if(prec_value > prop_imageMax.get_value())
			value = prop_imageMax.get_value();
	}
	else
	{
		if(prec_value < prop_Min.get_value() && prop_minLimited)
			value = prop_Min.get_value();
		else if(prec_value > prop_Max.get_value() && prop_maxLimited)
			value = prop_Max.get_value();
	}
	temp_value = value;
	inert.set_value( value, value );
	
	queue_draw();
}
// -------------------------------------------------------------------------
void UScrollScale::on_exceptions_changed()
{
	exceptions.clear();
	std::list<Glib::ustring> exceptionsList = explodestr(get_prop_scaleExceptions(),'\n');
	for(std::list<Glib::ustring>::iterator it = exceptionsList.begin();it != exceptionsList.end();++it)
	{
		std::list<Glib::ustring> pare = explodestr(*it,'=');
		if( !pare.empty() )
		{
			std::list<Glib::ustring>::iterator pit = pare.begin();
			
			float val = atof((*pit).c_str());
			
			pit++;
			if(pit!=pare.end())
				exceptions[val] = (*pit);
			else
				exceptions[val] = "";
		}
	}
}
// -------------------------------------------------------------------------
void UScrollScale::on_value_changed()
{
	//	cout<<get_name()<<"::on_value_changed value="<<property_value.get_value()<< endl;
	double prec_value = calibrate.get_cvalue(property_value.get_value())/pow10(get_property_precision());
	if(prop_use_inert)
	{
		last_value = temp_value;
		value = prec_value;
		if(prop_useImage)
		{
			if(prec_value < prop_imageMin.get_value())
				value = prop_imageMin.get_value();
			else if(prec_value > prop_imageMax.get_value())
				value = prop_imageMax.get_value();
		}
		else
		{
			if(prec_value < prop_Min.get_value() && (prop_minLimited || prop_loopScale))
				value = prop_Min.get_value();
			else if(prec_value > prop_Max.get_value() && (prop_maxLimited || prop_loopScale))
				value = prop_Max.get_value();
		}
		
		if (!inert_signal.connected())
		{
			inert_signal= inert.signal_inert_value_change().connect(sigc::mem_fun(*this, &UScrollScale::on_inert_value_changed));
		}
		if(prop_loopScale && !prop_useImage && (((prop_Max.get_value() - prop_Min.get_value())/2) < fabs(value - last_value)))
		{
			if(fabs(last_value - prop_Min.get_value()) < fabs(last_value - prop_Max.get_value()))
				inert.set_value( last_value, prop_Min.get_value() - (prop_Max.get_value() - value) );
			else
				inert.set_value( last_value, prop_Max.get_value() + (value - prop_Min.get_value()) );
		}
		else
			inert.set_value( last_value, value );
	}
	else
	{
		if (inert_signal.connected())
			inert_signal.disconnect();
		last_value = value;
		value = prec_value;
		if(prop_useImage)
		{
			if(prec_value < prop_imageMin.get_value())
				value = prop_imageMin.get_value();
			else if(prec_value > prop_imageMax.get_value())
				value = prop_imageMax.get_value();
		}
		else
		{
			if(prec_value < prop_Min.get_value() && (prop_minLimited || prop_loopScale))
				value = prop_Min.get_value();
			else if(prec_value > prop_Max.get_value() && (prop_maxLimited || prop_loopScale))
				value = prop_Max.get_value();
		}
		temp_value = value;
	}
	//	cout<<get_name()<<"::on_value_changed value="<<value<<" temp_value="<<temp_value<< endl;
	
	queue_draw();
}
// -------------------------------------------------------------------------
void UScrollScale::on_inert_value_changed(double val, bool inert_is_stoped)
{
	//	cout<<get_name()<<"::on_inert_value_changed temp_value="<<val<< endl;
	if(prop_loopScale)
	{
		if(val<prop_Min.get_value())
			temp_value = val + (prop_Max.get_value() - prop_Min.get_value());
		else if(val>prop_Max.get_value())
			temp_value = val - (prop_Max.get_value() - prop_Min.get_value());
		else
			temp_value = val;
	}
	else
		temp_value = val;
	queue_draw();
}
// -------------------------------------------------------------------------
void UScrollScale::allocate_changed(Gtk::Allocation& alloc)
{
	width = get_allocation().get_width();
	height = get_allocation().get_height();
	
	queue_draw();
}
// -------------------------------------------------------------------------
bool UScrollScale::on_scale_event(GdkEvent* ev)
{
//	if(ev->type!=GDK_EXPOSE)
//		cout<<get_name()<<"::on_scale_event event->type="<<ev->type<< endl;

    if(ev->type==GDK_MAP)
    {
//		cout<<get_name()<<"::on_scale_event event->type="<<ev->type<< endl;
        is_show = true;
//		on_value_changed();
    }
    else if(ev->type==GDK_UNMAP)
        is_show = false;
    return false;
}
// -------------------------------------------------------------------------
bool UScrollScale::on_expose_event(GdkEventExpose* event)
{
	bool rv = UEventBox::on_expose_event(event);
	#if GTK_VERSION_GE(2,20)
    if(!get_mapped())
#else
    if(!is_show)
#endif
        return false;

    Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
	Gtk::Allocation alloc = get_allocation();
    Gdk::Cairo::rectangle(cr,alloc);
    cr->clip();
    cr->translate(alloc.get_x(), alloc.get_y());

	//рисуем шкалу
	if(prop_useImage)
		draw_image_scale(cr,alloc);
	else
		draw_scale(cr,alloc);
	//рисуем подпись текущего значения
	draw_value(cr,alloc);
	
	if ( !connected_ && get_property_disconnect_effect() == 1)
    {
		set_property_disconnect_effect(1);
        alloc.set_x(0);
        alloc.set_y(0);
        UVoid::draw_disconnect_effect_1(cr, alloc);
    }

    return false;
}
// -------------------------------------------------------------------------
void UScrollScale::draw_image_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	double cur_value;
	if(prop_use_inert)
		cur_value = temp_value;
	else
		cur_value = value;

	int W,H;
	if(prop_Position.get_value() == Gtk::POS_RIGHT || prop_Position.get_value() == Gtk::POS_LEFT)
	{
		W = width;
		H = height * ((prop_imageMax.get_value() - prop_imageMin.get_value())/prop_visibleRange.get_value() + 1);
	}
	else if(prop_Position.get_value() == Gtk::POS_TOP || prop_Position.get_value() == Gtk::POS_BOTTOM)
	{
		W = width * ((prop_imageMax.get_value() - prop_imageMin.get_value())/prop_visibleRange.get_value() + 1);
		H = height;
	}

	Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(prop_image.get_value(), W, H);
	if(prop_Position.get_value() == Gtk::POS_RIGHT || prop_Position.get_value() == Gtk::POS_LEFT)
	{
		float y = (cur_value/(prop_imageMax.get_value() - prop_imageMin.get_value()) * (H - height)) - (H - height);
		Gdk::Cairo::set_source_pixbuf(cr,image, 0, y);
	}
	else if(prop_Position.get_value() == Gtk::POS_TOP || prop_Position.get_value() == Gtk::POS_BOTTOM)
	{
		float x = (cur_value/(prop_imageMax.get_value() - prop_imageMin.get_value()) * (W - width)) - (W - width);
		Gdk::Cairo::set_source_pixbuf(cr,image, x, 0);
	}
	cr->paint();
}
// -------------------------------------------------------------------------
void UScrollScale::draw_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	double cur_value;
	if(prop_use_inert)
		cur_value = temp_value;
	else
		cur_value = value;
	
	double dashLength;
	double null_pos_y, null_pos_x;
	double loop_null_pos_y, loop_null_pos_x;
	double min_pos_x, max_pos_x;
	double min_pos_y, max_pos_y;
	if(prop_Position.get_value() == Gtk::POS_RIGHT || prop_Position.get_value() == Gtk::POS_LEFT)
	{
		dashLength = height - prop_scalemarkWidth.get_value();
	}
	else if(prop_Position.get_value() == Gtk::POS_TOP || prop_Position.get_value() == Gtk::POS_BOTTOM)
	{
		dashLength = width - prop_scalemarkWidth.get_value();
	}

	//отрисовка малых делений
	int numDashMinor = (prop_maxMinor.get_value()>0? prop_maxMinor.get_value():1)*(prop_maxMajor.get_value()>0? prop_maxMajor.get_value():1);
	std::vector<double> dashesMinor(2);
	dashesMinor[0] = prop_scalemarkWidth.get_value()<0? 0:prop_scalemarkWidth.get_value();
	dashesMinor[1] = dashLength/numDashMinor - prop_scalemarkWidth.get_value();
	dashesMinor[1] = dashesMinor[1]<0? 0:dashesMinor[1];
	
	cr->save();
	Gdk::Cairo::set_source_color(cr, prop_scaleMinorColor.get_value());
	cr->set_dash(dashesMinor,0);
	cr->set_line_width(abs(prop_scaleMinorHeight.get_value()));
	if(prop_Position.get_value() == Gtk::POS_RIGHT || prop_Position.get_value() == Gtk::POS_LEFT)
	{
		null_pos_y = (float)height/2 + cur_value/prop_visibleRange.get_value()*dashLength;
		if(prop_Position.get_value() == Gtk::POS_RIGHT)
			null_pos_x = width - ((float)prop_scaleMinorHeight.get_value())/2;
		else if(prop_Position.get_value() == Gtk::POS_LEFT)
			null_pos_x = ((float)prop_scaleMinorHeight.get_value())/2;

		min_pos_y = (float)height/2 + (cur_value - prop_Min.get_value())/prop_visibleRange.get_value()*dashLength;
		max_pos_y = (float)height/2 + (cur_value - prop_Max.get_value())/prop_visibleRange.get_value()*dashLength;
		
		cr->save();
		if(null_pos_y<0)
		{
			cr->move_to(null_pos_x, null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_y>height)
					cr->line_to(null_pos_x, height);
				else
					cr->line_to(null_pos_x, min_pos_y + (float)prop_scalemarkWidth.get_value()/2);
			}
			else
				cr->line_to(null_pos_x, height);
		}
		else if(null_pos_y>height)
		{
			cr->move_to(null_pos_x, null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_y<0)
					cr->line_to(null_pos_x, 0);
				else
					cr->line_to(null_pos_x, max_pos_y - (float)prop_scalemarkWidth.get_value()/2);
			}
			else
				cr->line_to(null_pos_x, 0);
		}
		else
		{
			cr->move_to(null_pos_x, null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_y>height)
					cr->line_to(null_pos_x, height);
				else
					cr->line_to(null_pos_x, min_pos_y + (float)prop_scalemarkWidth.get_value()/2);
			}
			else
				cr->line_to(null_pos_x, height);
			cr->move_to(null_pos_x, null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_y<0)
					cr->line_to(null_pos_x, 0);
				else
					cr->line_to(null_pos_x, max_pos_y - (float)prop_scalemarkWidth.get_value()/2);
			}
			else
				cr->line_to(null_pos_x, 0);
		}
		cr->stroke();
		cr->restore();
		
		//отрисовка малых делений зациклиной шкалы
		if(prop_loopScale)
		{
			if(min_pos_y<height && min_pos_y>0)
			{
				loop_null_pos_x = null_pos_x;
				loop_null_pos_y = null_pos_y + (min_pos_y - max_pos_y);
				double loop_min_pos_y = min_pos_y + (min_pos_y - max_pos_y);
				if(loop_min_pos_y>height)
					loop_min_pos_y = height;
				
				if(loop_null_pos_y <= min_pos_y)
				{
					cr->save();
					cr->rectangle(0, min_pos_y - (float)prop_scalemarkWidth.get_value()/2, width, (loop_min_pos_y - min_pos_y) + prop_scalemarkWidth.get_value());
					cr->clip_preserve();
					cr->begin_new_path();
					cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, loop_min_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->stroke();
					cr->restore();
				}
				else if(loop_null_pos_y >= loop_min_pos_y)
				{
					if(loop_min_pos_y<height)
					{
						cr->save();
						cr->rectangle(0,min_pos_y - (float)prop_scalemarkWidth.get_value()/2, width, (loop_min_pos_y - min_pos_y) + prop_scalemarkWidth.get_value());
						cr->clip_preserve();
						cr->begin_new_path();
						cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
						cr->line_to(loop_null_pos_x, min_pos_y - (float)prop_scalemarkWidth.get_value()/2);
						cr->stroke();
						cr->restore();
					}
					else
					{
						cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
						cr->line_to(loop_null_pos_x, min_pos_y - (float)prop_scalemarkWidth.get_value()/2);
						cr->stroke();
					}
				}
				else
				{
					cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, loop_min_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, min_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->stroke();
				}
			}
			
			if(max_pos_y<height && max_pos_y>0)
			{
				cr->save();
				loop_null_pos_x = null_pos_x;
				loop_null_pos_y = null_pos_y - (min_pos_y - max_pos_y);
				double loop_max_pos_y = max_pos_y - (min_pos_y - max_pos_y);
				if(loop_max_pos_y<0)
					loop_max_pos_y = 0;
				
				if(loop_null_pos_y >= max_pos_y)
				{
					cr->save();
					cr->rectangle(0, loop_max_pos_y - (float)prop_scalemarkWidth.get_value()/2, width, (max_pos_y - loop_max_pos_y) + prop_scalemarkWidth.get_value());
					cr->clip_preserve();
					cr->begin_new_path();
					cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, loop_max_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->stroke();
					cr->restore();
				}
				else if(loop_null_pos_y <= loop_max_pos_y)
				{
					if(loop_max_pos_y > 0)
					{
						cr->save();
						cr->rectangle(0, loop_max_pos_y - (float)prop_scalemarkWidth.get_value()/2, width, (max_pos_y - loop_max_pos_y) + prop_scalemarkWidth.get_value());
						cr->clip_preserve();
						cr->begin_new_path();
						cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
						cr->line_to(loop_null_pos_x, max_pos_y + (float)prop_scalemarkWidth.get_value()/2);
						cr->stroke();
						cr->restore();
					}
					else
					{
						cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
						cr->line_to(loop_null_pos_x, max_pos_y + (float)prop_scalemarkWidth.get_value()/2);
						cr->stroke();
					}
				}
				else
				{
					cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, max_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, loop_max_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->stroke();
				}
				cr->restore();
			}
		}
	}
	else if(prop_Position.get_value() == Gtk::POS_TOP || prop_Position.get_value() == Gtk::POS_BOTTOM)
	{
		null_pos_x = (float)width/2 - cur_value/prop_visibleRange.get_value()*dashLength;
		if(prop_Position.get_value() == Gtk::POS_TOP)
			null_pos_y = ((float)prop_scaleMinorHeight.get_value())/2;
		else if(prop_Position.get_value() == Gtk::POS_BOTTOM)
			null_pos_y = height - ((float)prop_scaleMinorHeight.get_value())/2;

		min_pos_x = (float)width/2 - (cur_value - prop_Min.get_value())/prop_visibleRange.get_value()*dashLength;
		max_pos_x = (float)width/2 - (cur_value - prop_Max.get_value())/prop_visibleRange.get_value()*dashLength;
		
		cr->save();
		if(null_pos_x<0)
		{
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_x>0)
				{
					cr->rectangle(min_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, width - min_pos_x + (float)prop_scalemarkWidth.get_value()/2, height);
					cr->clip_preserve();
					cr->begin_new_path();
				}
			}
			
			cr->move_to(null_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_x>width)
					cr->line_to(width, null_pos_y);
				else
					cr->line_to(max_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			}
			else
				cr->line_to(width, null_pos_y);
		}
		else if(null_pos_x>width)
		{
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_x<width)
				{
					cr->rectangle(0, 0, max_pos_x + (float)prop_scalemarkWidth.get_value()/2, height);
					cr->clip_preserve();
					cr->begin_new_path();
				}
			}
			
			cr->move_to(null_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_x<0)
					cr->line_to(0, null_pos_y);
				else
					cr->line_to(min_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			}
			else
				cr->line_to(0, null_pos_y);
		}
		else
		{
			if(prop_minLimited || prop_maxLimited || prop_loopScale)
			{
				if(max_pos_x<width || min_pos_x>0)
				{
					cr->rectangle(min_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, max_pos_x - min_pos_x + prop_scalemarkWidth.get_value(), height);
					cr->clip_preserve();
					cr->begin_new_path();
				}
			}
			
			cr->move_to(null_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_x>width)
					cr->line_to(width, null_pos_y);
				else
					cr->line_to(max_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			}
			else
				cr->line_to(width, null_pos_y);
			cr->move_to(null_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_x<0)
					cr->line_to(0, null_pos_y);
				else
					cr->line_to(min_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			}
			else
				cr->line_to(0, null_pos_y);
		}
		cr->stroke();
		cr->restore();
		//отрисовка малых делений зациклиной шкалы
		if(prop_loopScale)
		{
			if(min_pos_x<width && min_pos_x>0)
			{
				loop_null_pos_x = null_pos_x - (max_pos_x - min_pos_x);
				loop_null_pos_y = null_pos_y;
				double loop_min_pos_x = min_pos_x - (max_pos_x - min_pos_x);
				if(loop_min_pos_x<0)
					loop_min_pos_x = 0;
				
				if(loop_null_pos_x >= min_pos_x)
				{
					cr->save();
					cr->rectangle(loop_min_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, (min_pos_x - loop_min_pos_x) + prop_scalemarkWidth.get_value(), height);
					cr->clip_preserve();
					cr->begin_new_path();
					cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(loop_min_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->stroke();
					cr->restore();
				}
				else if(loop_null_pos_x <= loop_min_pos_x)
				{
					if(loop_min_pos_x>0)
					{
						cr->save();
						cr->rectangle(loop_min_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, (min_pos_x - loop_min_pos_x) + prop_scalemarkWidth.get_value(), height);
						cr->clip_preserve();
						cr->begin_new_path();
						cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
						cr->line_to(min_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
						cr->stroke();
						cr->restore();
					}
					else
					{
						cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
						cr->line_to(min_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
						cr->stroke();
					}
				}
				else
				{
					cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(loop_min_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(min_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->stroke();
				}
			}
			
			if(max_pos_x<width && max_pos_x>0)
			{
				cr->save();
				loop_null_pos_x = null_pos_x + (max_pos_x - min_pos_x);
				loop_null_pos_y = null_pos_y;
				double loop_max_pos_x = max_pos_x + (max_pos_x - min_pos_x);
				if(loop_max_pos_x>width)
					loop_max_pos_x = width;
				
				if(loop_null_pos_x <= max_pos_x)
				{
					cr->save();
					cr->rectangle(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, (loop_max_pos_x - max_pos_x) + prop_scalemarkWidth.get_value(), height);
					cr->clip_preserve();
					cr->begin_new_path();
					cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(loop_max_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->stroke();
					cr->restore();
				}
				else if(loop_null_pos_x >= loop_max_pos_x)
				{
					if(loop_max_pos_x < width)
					{
						cr->save();
						cr->rectangle(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, (loop_max_pos_x - max_pos_x) + prop_scalemarkWidth.get_value(), height);
						cr->clip_preserve();
						cr->begin_new_path();
						cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
						cr->line_to(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
						cr->stroke();
						cr->restore();
					}
					else
					{
						cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
						cr->line_to(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
						cr->stroke();
					}
				}
				else
				{
					cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(loop_max_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->stroke();
				}
				cr->restore();
			}
		}
	}
	cr->restore();
	
// 	cout<<endl<<get_name()<<"::draw_scale dashesMinor[0]="<<dashesMinor[0]<<" dashesMinor[1]="<<dashesMinor[1]<<endl;
// 	cout<<get_name()<<"::draw_scale null_pos_y="<<null_pos_y<<" null_pos_x="<<null_pos_x<<endl;
	
	//отрисовка больших делений
	int numDashMajor = prop_maxMajor.get_value()>0? prop_maxMajor.get_value():1;
	std::vector<double> dashesMajor(2);
	dashesMajor[0] = dashesMinor[0];
	dashesMajor[1] = dashLength/numDashMajor - prop_scalemarkWidth.get_value();
	dashesMajor[1] = dashesMajor[1]<0? 0:dashesMajor[1];
	
	cr->save();
	Gdk::Cairo::set_source_color(cr, prop_scaleMajorColor.get_value());
	cr->set_dash(dashesMajor,0);
	cr->set_line_width(abs(prop_scaleMajorHeight.get_value()));
	if(prop_Position.get_value() == Gtk::POS_RIGHT || prop_Position.get_value() == Gtk::POS_LEFT)
	{
		cr->save();
		if(prop_Position.get_value() == Gtk::POS_RIGHT)
			null_pos_x = width - ((float)prop_scaleMajorHeight.get_value())/2;
		else if(prop_Position.get_value() == Gtk::POS_LEFT)
			null_pos_x = ((float)prop_scaleMajorHeight.get_value())/2;
		
		if(null_pos_y<0)
		{
			cr->move_to(null_pos_x, null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_y>height)
					cr->line_to(null_pos_x, height);
				else
					cr->line_to(null_pos_x, min_pos_y + (float)prop_scalemarkWidth.get_value()/2);
			}
			else
				cr->line_to(null_pos_x, height);
		}
		else if(null_pos_y>height)
		{
			cr->move_to(null_pos_x, null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_y<0)
					cr->line_to(null_pos_x, 0);
				else
					cr->line_to(null_pos_x, max_pos_y - (float)prop_scalemarkWidth.get_value()/2);
			}
			else
				cr->line_to(null_pos_x, 0);
		}
		else
		{
			cr->move_to(null_pos_x, null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_y>height)
					cr->line_to(null_pos_x, height);
				else
					cr->line_to(null_pos_x, min_pos_y + (float)prop_scalemarkWidth.get_value()/2);
			}
			else
				cr->line_to(null_pos_x, height);
			cr->move_to(null_pos_x, null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_y<0)
					cr->line_to(null_pos_x, 0);
				else
					cr->line_to(null_pos_x, max_pos_y - (float)prop_scalemarkWidth.get_value()/2);
			}
			else
				cr->line_to(null_pos_x, 0);
		}
		cr->stroke();
		cr->restore();
		
		//отрисовка больших делений зациклиной шкалы
		if(prop_loopScale)
		{
			if(min_pos_y<height && min_pos_y>0)
			{
				loop_null_pos_x = null_pos_x;
				loop_null_pos_y = null_pos_y + (min_pos_y - max_pos_y);
				double loop_min_pos_y = min_pos_y + (min_pos_y - max_pos_y);
				if(loop_min_pos_y>height)
					loop_min_pos_y = height;
				
				if(loop_null_pos_y <= min_pos_y)
				{
					cr->save();
					cr->rectangle(0, min_pos_y - (float)prop_scalemarkWidth.get_value()/2, width, (loop_min_pos_y - min_pos_y) + prop_scalemarkWidth.get_value());
					cr->clip_preserve();
					cr->begin_new_path();
					cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, loop_min_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->stroke();
					cr->restore();
				}
				else if(loop_null_pos_y >= loop_min_pos_y)
				{
					if(loop_min_pos_y<height)
					{
						cr->save();
						cr->rectangle(0,min_pos_y - (float)prop_scalemarkWidth.get_value()/2, width, (loop_min_pos_y - min_pos_y) + prop_scalemarkWidth.get_value());
						cr->clip_preserve();
						cr->begin_new_path();
						cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
						cr->line_to(loop_null_pos_x, min_pos_y - (float)prop_scalemarkWidth.get_value()/2);
						cr->stroke();
						cr->restore();
					}
					else
					{
						cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
						cr->line_to(loop_null_pos_x, min_pos_y - (float)prop_scalemarkWidth.get_value()/2);
						cr->stroke();
					}
				}
				else
				{
					cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, loop_min_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, min_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->stroke();
				}
				
				//отрисовка подписей значений около больших делений зациклиной шкалы
				float val = 0;
				double label_pos_x = null_pos_x;
				double label_pos_y = loop_null_pos_y;
				double label_min_pos_y = null_pos_y;
				if(prop_Min.get_value()>0)
				{
					while(val<prop_Min.get_value())
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_min_pos_y = label_min_pos_y - dashLength/numDashMajor;
					};
				}
				else
				{
					while(val>prop_Min.get_value())
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_min_pos_y = label_min_pos_y + dashLength/numDashMajor;
					};
				}
				val = 0;
				
				if(loop_null_pos_y>height)
				{
					//пропуск значений за границей видимости
					while(label_pos_y>height || val<prop_Min.get_value())
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y - dashLength/numDashMajor;
					};
					while(label_pos_y>0)
					{
						if(val>prop_Max.get_value() || (long)label_pos_y==(long)label_min_pos_y)
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y - dashLength/numDashMajor;
					};
				}
				else if(loop_null_pos_y<0)
				{
					//пропуск значений за границей видимости
					while(label_pos_y<0 || (val>prop_Max.get_value() || (long)label_pos_y==(long)label_min_pos_y))
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y + dashLength/numDashMajor;
					};
					while(label_pos_y<height)
					{
						if(val<prop_Min.get_value())
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y + dashLength/numDashMajor;
					};
				}
				else
				{
					while(val<prop_Min.get_value())
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y - dashLength/numDashMajor;
					};
					while(label_pos_y>0)
					{
						if(val>prop_Max.get_value() || (long)label_pos_y==(long)label_min_pos_y)
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y - dashLength/numDashMajor;
					};
					
					val = -prop_visibleRange.get_value()/numDashMajor;
					label_pos_y = loop_null_pos_y + dashLength/numDashMajor;
					while(val>prop_Max.get_value() || (long)label_pos_y==(long)label_min_pos_y)
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y + dashLength/numDashMajor;
					};
					while(label_pos_y<height)
					{
						if(val<prop_Min.get_value())
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y + dashLength/numDashMajor;
					};
				}
			}
			
			if(max_pos_y<height && max_pos_y>0)
			{
				cr->save();
				loop_null_pos_x = null_pos_x;
				loop_null_pos_y = null_pos_y - (min_pos_y - max_pos_y);
				double loop_max_pos_y = max_pos_y - (min_pos_y - max_pos_y);
				if(loop_max_pos_y<0)
					loop_max_pos_y = 0;
				
				if(loop_null_pos_y >= max_pos_y)
				{
					cr->save();
					cr->rectangle(0, loop_max_pos_y - (float)prop_scalemarkWidth.get_value()/2, width, (max_pos_y - loop_max_pos_y) + prop_scalemarkWidth.get_value());
					cr->clip_preserve();
					cr->begin_new_path();
					cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, loop_max_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->stroke();
					cr->restore();
				}
				else if(loop_null_pos_y <= loop_max_pos_y)
				{
					if(loop_max_pos_y > 0)
					{
						cr->save();
						cr->rectangle(0, loop_max_pos_y - (float)prop_scalemarkWidth.get_value()/2, width, (max_pos_y - loop_max_pos_y) + prop_scalemarkWidth.get_value());
						cr->clip_preserve();
						cr->begin_new_path();
						cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
						cr->line_to(loop_null_pos_x, max_pos_y + (float)prop_scalemarkWidth.get_value()/2);
						cr->stroke();
						cr->restore();
					}
					else
					{
						cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
						cr->line_to(loop_null_pos_x, max_pos_y + (float)prop_scalemarkWidth.get_value()/2);
						cr->stroke();
					}
				}
				else
				{
					cr->move_to(loop_null_pos_x, loop_null_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, max_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->move_to(loop_null_pos_x, loop_null_pos_y + (float)prop_scalemarkWidth.get_value()/2);
					cr->line_to(loop_null_pos_x, loop_max_pos_y - (float)prop_scalemarkWidth.get_value()/2);
					cr->stroke();
				}
				cr->restore();
				
				//отрисовка подписей значений около больших делений зациклиной шкалы
				float val = 0;
				double label_pos_x = null_pos_x;
				double label_pos_y = loop_null_pos_y;
				double label_max_pos_y = null_pos_y;
				if(prop_Max.get_value()>0)
				{
					while(val<prop_Max.get_value())
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_max_pos_y = label_max_pos_y - dashLength/numDashMajor;
					};
				}
				else
				{
					while(val>prop_Max.get_value())
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_max_pos_y = label_max_pos_y + dashLength/numDashMajor;
					};
				}
				val = 0;
				
				if(loop_null_pos_y>height)
				{
					//пропуск значений за границей видимости
					while(label_pos_y>height || (val<prop_Min.get_value() || (long)label_pos_y==(long)label_max_pos_y))
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y - dashLength/numDashMajor;
					};
					while(label_pos_y>0)
					{
						if(val>prop_Max.get_value())
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y - dashLength/numDashMajor;
					};
				}
				else if(loop_null_pos_y<0)
				{
					//пропуск значений за границей видимости
					while(label_pos_y<0 || val>prop_Max.get_value())
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y + dashLength/numDashMajor;
					};
					while(label_pos_y<height)
					{
						if(val<prop_Min.get_value() || (long)label_pos_y==(long)label_max_pos_y)
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y + dashLength/numDashMajor;
					};
				}
				else
				{
					while(val<prop_Min.get_value() || (long)label_pos_y==(long)label_max_pos_y)
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y - dashLength/numDashMajor;
					};
					while(label_pos_y>0)
					{
						if(val>prop_Max.get_value())
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y - dashLength/numDashMajor;
					};
					
					val = -prop_visibleRange.get_value()/numDashMajor;
					label_pos_y = loop_null_pos_y + dashLength/numDashMajor;
					while(val>prop_Max.get_value())
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y + dashLength/numDashMajor;
					};
					while(label_pos_y<height)
					{
						if(val<prop_Min.get_value() || (long)label_pos_y==(long)label_max_pos_y)
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_y = label_pos_y + dashLength/numDashMajor;
					};
				}
			}
		}
		cr->restore();
		
		//отрисовка подписей значений около больших делений
		float val = 0;
		double label_pos_x = null_pos_x;
		double label_pos_y = null_pos_y;
		if(null_pos_y<0)
		{
			//пропуск значений за границей видимости
			while(label_pos_y<0)
			{
				val = val - prop_visibleRange.get_value()/numDashMajor;
				label_pos_y = label_pos_y + dashLength/numDashMajor;
			};
			while(label_pos_y<height)
			{
				if((prop_minLimited || prop_loopScale) && prop_Min.get_value()>val)
					break;
				
				draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
				val = val - prop_visibleRange.get_value()/numDashMajor;
				label_pos_y = label_pos_y + dashLength/numDashMajor;
			};
		}
		else if(null_pos_y>height)
		{
			//пропуск значений за границей видимости
			while(label_pos_y>height)
			{
				val = val + prop_visibleRange.get_value()/numDashMajor;
				label_pos_y = label_pos_y - dashLength/numDashMajor;
			};
			while(label_pos_y>0)
			{
				if((prop_maxLimited || prop_loopScale) && prop_Max.get_value()<val)
					break;
				
				draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
				val = val + prop_visibleRange.get_value()/numDashMajor;
				label_pos_y = label_pos_y - dashLength/numDashMajor;
			};
		}
		else
		{
			while(label_pos_y<height)
			{
				if((prop_minLimited || prop_loopScale) && prop_Min.get_value()>val)
					break;
				
				draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
				val = val - prop_visibleRange.get_value()/numDashMajor;
				label_pos_y = label_pos_y + dashLength/numDashMajor;
			};

			val = prop_visibleRange.get_value()/numDashMajor;
			label_pos_y = null_pos_y - dashLength/numDashMajor;
			while(label_pos_y>0)
			{
				if((prop_maxLimited || prop_loopScale) && prop_Max.get_value()<val)
					break;
				
				draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
				val = val + prop_visibleRange.get_value()/numDashMajor;
				label_pos_y = label_pos_y - dashLength/numDashMajor;
			};
		}
	}
	else if(prop_Position.get_value() == Gtk::POS_TOP || prop_Position.get_value() == Gtk::POS_BOTTOM)
	{
		if(prop_Position.get_value() == Gtk::POS_TOP)
			null_pos_y = ((float)prop_scaleMajorHeight.get_value())/2;
		else if(prop_Position.get_value() == Gtk::POS_BOTTOM)
			null_pos_y = height - ((float)prop_scaleMajorHeight.get_value())/2;
		
		cr->save();
		if(null_pos_x<0)
		{
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_x>0)
				{
					cr->rectangle(min_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, width - min_pos_x + (float)prop_scalemarkWidth.get_value()/2, height);
					cr->clip_preserve();
					cr->begin_new_path();
				}
			}
			
			cr->move_to(null_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_x>width)
					cr->line_to(width, null_pos_y);
				else
					cr->line_to(max_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			}
			else
				cr->line_to(width, null_pos_y);
		}
		else if(null_pos_x>width)
		{
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_x<width)
				{
					cr->rectangle(0, 0, max_pos_x + (float)prop_scalemarkWidth.get_value()/2, height);
					cr->clip_preserve();
					cr->begin_new_path();
				}
			}
			
			cr->move_to(null_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_x<0)
					cr->line_to(0, null_pos_y);
				else
					cr->line_to(min_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			}
			else
				cr->line_to(0, null_pos_y);
		}
		else
		{
			if(prop_minLimited || prop_maxLimited || prop_loopScale)
			{
				if(max_pos_x<width || min_pos_x>0)
				{
					cr->rectangle(min_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, max_pos_x - min_pos_x + prop_scalemarkWidth.get_value(), height);
					cr->clip_preserve();
					cr->begin_new_path();
				}
			}
			
			cr->move_to(null_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			if(prop_maxLimited || prop_loopScale)
			{
				if(max_pos_x>width)
					cr->line_to(width, null_pos_y);
				else
					cr->line_to(max_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			}
			else
				cr->line_to(width, null_pos_y);
			cr->move_to(null_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			if(prop_minLimited || prop_loopScale)
			{
				if(min_pos_x<0)
					cr->line_to(0, null_pos_y);
				else
					cr->line_to(min_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
			}
			else
				cr->line_to(0, null_pos_y);
		}
		cr->stroke();
		cr->restore();
		
		//отрисовка больших делений зациклиной шкалы
		if(prop_loopScale)
		{
			if(min_pos_x<width && min_pos_x>0)
			{
				loop_null_pos_x = null_pos_x - (max_pos_x - min_pos_x);
				loop_null_pos_y = null_pos_y;
				double loop_min_pos_x = min_pos_x - (max_pos_x - min_pos_x);
				if(loop_min_pos_x<0)
					loop_min_pos_x = 0;
				
				if(loop_null_pos_x >= min_pos_x)
				{
					cr->save();
					cr->rectangle(loop_min_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, (min_pos_x - loop_min_pos_x) + prop_scalemarkWidth.get_value(), height);
					cr->clip_preserve();
					cr->begin_new_path();
					cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(loop_min_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->stroke();
					cr->restore();
				}
				else if(loop_null_pos_x <= loop_min_pos_x)
				{
					if(loop_min_pos_x>0)
					{
						cr->save();
						cr->rectangle(loop_min_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, (min_pos_x - loop_min_pos_x) + prop_scalemarkWidth.get_value(), height);
						cr->clip_preserve();
						cr->begin_new_path();
						cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
						cr->line_to(min_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
						cr->stroke();
						cr->restore();
					}
					else
					{
						cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
						cr->line_to(min_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
						cr->stroke();
					}
				}
				else
				{
					cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(loop_min_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(min_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->stroke();
				}

				//отрисовка подписей значений около больших делений зациклиной шкалы
				float val = 0;
				double label_pos_x = loop_null_pos_x;
				double label_pos_y = null_pos_y;
				double label_min_pos_x = null_pos_x;
				if(prop_Min.get_value()>0)
				{
					while(val<prop_Min.get_value())
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_min_pos_x = label_min_pos_x + dashLength/numDashMajor;
					};
				}
				else
				{
					while(val>prop_Min.get_value())
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_min_pos_x = label_min_pos_x - dashLength/numDashMajor;
					};
				}
				val = 0;
				
				if(loop_null_pos_x<0)
				{
					//пропуск значений за границей видимости
					while(label_pos_x<0 || val<prop_Min.get_value())
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x + dashLength/numDashMajor;
					};
					while(label_pos_x<width)
					{
						if(val>prop_Max.get_value() || (long)label_pos_x==(long)label_min_pos_x)
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x + dashLength/numDashMajor;
					};
				}
				else if(loop_null_pos_x>width)
				{
					//пропуск значений за границей видимости
					while(label_pos_x>width || (val>prop_Max.get_value() || (long)label_pos_x==(long)label_min_pos_x))
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x - dashLength/numDashMajor;
					};
					while(label_pos_x>0)
					{
						if(val<prop_Min.get_value())
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x - dashLength/numDashMajor;
					};
				}
				else
				{
					while(val<prop_Min.get_value())
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x + dashLength/numDashMajor;
					};
					while(label_pos_x<width)
					{
						if(val>prop_Max.get_value() || (long)label_pos_x==(long)label_min_pos_x)
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x + dashLength/numDashMajor;
					};
					
					val = -prop_visibleRange.get_value()/numDashMajor;
					label_pos_x = loop_null_pos_x - dashLength/numDashMajor;
					while(val>prop_Max.get_value() || (long)label_pos_x==(long)label_min_pos_x)
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x - dashLength/numDashMajor;
					};
					while(label_pos_x>0)
					{
						if(val<prop_Min.get_value())
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x - dashLength/numDashMajor;
					};
				}
			}
			
			if(max_pos_x<width && max_pos_x>0)
			{
				cr->save();
				loop_null_pos_x = null_pos_x + (max_pos_x - min_pos_x);
				loop_null_pos_y = null_pos_y;
				double loop_max_pos_x = max_pos_x + (max_pos_x - min_pos_x);
				if(loop_max_pos_x>width)
					loop_max_pos_x = width;
				
				if(loop_null_pos_x <= max_pos_x)
				{
					cr->save();
					cr->rectangle(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, (loop_max_pos_x - max_pos_x) + prop_scalemarkWidth.get_value(), height);
					cr->clip_preserve();
					cr->begin_new_path();
					cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(loop_max_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->stroke();
					cr->restore();
				}
				else if(loop_null_pos_x >= loop_max_pos_x)
				{
					if(loop_max_pos_x < width)
					{
						cr->save();
						cr->rectangle(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, 0, (loop_max_pos_x - max_pos_x) + prop_scalemarkWidth.get_value(), height);
						cr->clip_preserve();
						cr->begin_new_path();
						cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
						cr->line_to(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
						cr->stroke();
						cr->restore();
					}
					else
					{
						cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
						cr->line_to(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
						cr->stroke();
					}
				}
				else
				{
					cr->move_to(loop_null_pos_x + (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(max_pos_x - (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->move_to(loop_null_pos_x - (float)prop_scalemarkWidth.get_value()/2, loop_null_pos_y);
					cr->line_to(loop_max_pos_x + (float)prop_scalemarkWidth.get_value()/2, null_pos_y);
					cr->stroke();
				}
				cr->restore();

				//отрисовка подписей значений около больших делений зациклиной шкалы
				float val = 0;
				double label_pos_x = loop_null_pos_x;
				double label_pos_y = null_pos_y;
				double label_max_pos_x = null_pos_x;
				if(prop_Max.get_value()>0)
				{
					while(val<prop_Max.get_value())
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_max_pos_x = label_max_pos_x + dashLength/numDashMajor;
					};
				}
				else
				{
					while(val>prop_Max.get_value())
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_max_pos_x = label_max_pos_x - dashLength/numDashMajor;
					};
				}
				val = 0;
				
				if(loop_null_pos_x<0)
				{
					//пропуск значений за границей видимости
					while(label_pos_x<0 || (val<prop_Min.get_value() || (long)label_pos_x==(long)label_max_pos_x))
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x + dashLength/numDashMajor;
					};
					while(label_pos_x<width)
					{
						if(val>prop_Max.get_value())
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x + dashLength/numDashMajor;
					};
				}
				else if(loop_null_pos_x>width)
				{
					//пропуск значений за границей видимости
					while(label_pos_x>width || val>prop_Max.get_value())
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x - dashLength/numDashMajor;
					};
					while(label_pos_x>0)
					{
						if(val<prop_Min.get_value() || (long)label_pos_x==(long)label_max_pos_x)
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x - dashLength/numDashMajor;
					};
				}
				else
				{
					while(val<prop_Min.get_value() || (long)label_pos_x==(long)label_max_pos_x)
					{
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x + dashLength/numDashMajor;
					};
					while(label_pos_x<width)
					{
						if(val>prop_Max.get_value())
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val + prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x + dashLength/numDashMajor;
					};
					
					val = -prop_visibleRange.get_value()/numDashMajor;
					label_pos_x = loop_null_pos_x - dashLength/numDashMajor;
					while(val>prop_Max.get_value())
					{
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x - dashLength/numDashMajor;
					};
					while(label_pos_x>0)
					{
						if(val<prop_Min.get_value() || (long)label_pos_x==(long)label_max_pos_x)
							break;
						
						draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
						val = val - prop_visibleRange.get_value()/numDashMajor;
						label_pos_x = label_pos_x - dashLength/numDashMajor;
					};
				}
			}
		}
		cr->restore();
		
		//отрисовка подписей значений около больших делений
		float val = 0;
		double label_pos_x = null_pos_x;
		double label_pos_y = null_pos_y;
		if(null_pos_x<0)
		{
			//пропуск значений за границей видимости
			while(label_pos_x<0)
			{
				val = val + prop_visibleRange.get_value()/numDashMajor;
				label_pos_x = label_pos_x + dashLength/numDashMajor;
			};
			if(prop_minLimited || prop_loopScale)
			{
				while(val<prop_Min.get_value())
				{
					val = val + prop_visibleRange.get_value()/numDashMajor;
					label_pos_x = label_pos_x + dashLength/numDashMajor;
				};
			}
			while(label_pos_x<width)
			{
				if((prop_maxLimited || prop_loopScale) && val>prop_Max.get_value())
					break;
				
				draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
				val = val + prop_visibleRange.get_value()/numDashMajor;
				label_pos_x = label_pos_x + dashLength/numDashMajor;
			};
		}
		else if(null_pos_x>width)
		{
			//пропуск значений за границей видимости
			while(label_pos_x>width)
			{
				val = val - prop_visibleRange.get_value()/numDashMajor;
				label_pos_x = label_pos_x - dashLength/numDashMajor;
			};
			if(prop_maxLimited || prop_loopScale)
			{
				while(val>prop_Max.get_value())
				{
					val = val - prop_visibleRange.get_value()/numDashMajor;
					label_pos_x = label_pos_x - dashLength/numDashMajor;
				};
			}
			while(label_pos_x>0)
			{
				if((prop_minLimited || prop_loopScale) && val<prop_Min.get_value())
					break;
				
				draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
				val = val - prop_visibleRange.get_value()/numDashMajor;
				label_pos_x = label_pos_x - dashLength/numDashMajor;
			};
		}
		else
		{
			if(prop_minLimited || prop_loopScale)
			{
				while(val<prop_Min.get_value())
				{
					val = val + prop_visibleRange.get_value()/numDashMajor;
					label_pos_x = label_pos_x + dashLength/numDashMajor;
				};
			}
			while(label_pos_x<width)
			{
				if((prop_maxLimited || prop_loopScale) && val>prop_Max.get_value())
					break;
				
				draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
				val = val + prop_visibleRange.get_value()/numDashMajor;
				label_pos_x = label_pos_x + dashLength/numDashMajor;
			};
			
			val = -prop_visibleRange.get_value()/numDashMajor;
			label_pos_x = null_pos_x - dashLength/numDashMajor;
			if(prop_maxLimited || prop_loopScale)
			{
				while(val>prop_Max.get_value())
				{
					val = val - prop_visibleRange.get_value()/numDashMajor;
					label_pos_x = label_pos_x - dashLength/numDashMajor;
				};
			}
			while(label_pos_x>0)
			{
				if((prop_minLimited || prop_loopScale) && val<prop_Min.get_value())
					break;
				
				draw_scale_text(cr, alloc, label_pos_x, label_pos_y, val);
				val = val - prop_visibleRange.get_value()/numDashMajor;
				label_pos_x = label_pos_x - dashLength/numDashMajor;
			};
		}
	}
	
	//отрисовка линии
	cr->save();
	Gdk::Cairo::set_source_color(cr, prop_scaleMinorColor.get_value());
	cr->set_line_width(prop_scalemarkWidth.get_value());
	if(prop_Position.get_value() == Gtk::POS_RIGHT)
	{
		cr->move_to(width - ((float)prop_scalemarkWidth.get_value())/2, height);
		cr->line_to(width - ((float)prop_scalemarkWidth.get_value())/2, 0);
	}
	else if(prop_Position.get_value() == Gtk::POS_LEFT)
	{
		cr->move_to(((float)prop_scalemarkWidth.get_value())/2, height);
		cr->line_to(((float)prop_scalemarkWidth.get_value())/2, 0);
	}
	else if(prop_Position.get_value() == Gtk::POS_TOP)
	{
		cr->move_to(width, ((float)prop_scalemarkWidth.get_value())/2);
		cr->line_to(0, ((float)prop_scalemarkWidth.get_value())/2);
	}
	else if(prop_Position.get_value() == Gtk::POS_BOTTOM)
	{
		cr->move_to(width, height - ((float)prop_scalemarkWidth.get_value())/2);
		cr->line_to(0, height - ((float)prop_scalemarkWidth.get_value())/2);
	}
	cr->stroke();
	cr->restore();
	
}
// -------------------------------------------------------------------------
void UScrollScale::draw_scale_text(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc, double label_pos_x, double label_pos_y, float val)
{
	Glib::RefPtr<Pango::Layout> layout_value_label;
	layout_value_label = create_pango_layout("0");
	layout_value_label->set_alignment(Pango::ALIGN_CENTER);

	cr->save();
	std::ostringstream title;
	if(prop_useExceptions)
	{
		scaleExceptions::iterator it = exceptions.find(val);
		if(it!=exceptions.end())
		{
			Gdk::Cairo::set_source_color(cr, prop_exceptionsColor.get_value());
			Pango::FontDescription fd(prop_exceptionsFont);
			fd.set_absolute_size(prop_exceptionsFontSize.get_value() * Pango::SCALE);
			layout_value_label->set_font_description(fd);

			title<<" "<<it->second<<" ";
		}
		else
		{
			Gdk::Cairo::set_source_color(cr, prop_scaleLabelColor.get_value());
			Pango::FontDescription fd(prop_scaleLabelFont);
			fd.set_absolute_size(prop_scaleLabelFontSize.get_value() * Pango::SCALE);
			layout_value_label->set_font_description(fd);

			title<<" ";
			if(property_fill_digits>0)
				title<<std::setw(property_fill_digits)<<setfill('0')<<(long)val;
			else
				title<<(long)val;
			if(property_digits>0)
				title<<","<<std::setw(property_digits)<<setfill('0')<<(long)(fabs(((float)val-(long)val )*pow10(property_digits)));
			title<<" ";
		}
	}
	else
	{
		Gdk::Cairo::set_source_color(cr, prop_scaleLabelColor.get_value());
		Pango::FontDescription fd(prop_scaleLabelFont);
		fd.set_absolute_size(prop_scaleLabelFontSize.get_value() * Pango::SCALE);
		layout_value_label->set_font_description(fd);

		title<<" ";
		if(property_fill_digits>0)
			title<<std::setw(property_fill_digits)<<setfill('0')<<(long)val;
		else
			title<<(long)val;
		if(property_digits>0)
			title<<","<<std::setw(property_digits)<<setfill('0')<<(long)(fabs(((float)val-(long)val )*pow10(property_digits)));
		title<<" ";
	}
	layout_value_label->set_text(title.str());
	
	int tx, ty;
	layout_value_label->get_pixel_size(tx,ty);
	
	if(prop_Position.get_value() == Gtk::POS_TOP || prop_Position.get_value() == Gtk::POS_BOTTOM)
	{
		if(prop_alignmentEdge)
		{
			if(prop_Position.get_value() == Gtk::POS_TOP)
				label_pos_y = height - ty - prop_scaleLabelOffset.get_value();
			else if(prop_Position.get_value() == Gtk::POS_BOTTOM)
				label_pos_y = prop_scaleLabelOffset.get_value();
		}
		else
		{
			if(prop_Position.get_value() == Gtk::POS_TOP)
				label_pos_y = prop_scaleMajorHeight.get_value() + prop_scaleLabelOffset.get_value();
			else if(prop_Position.get_value() == Gtk::POS_BOTTOM)
				label_pos_y = height - prop_scaleMajorHeight.get_value() - ty - prop_scaleLabelOffset.get_value();
		}
		float x;
		if(prop_Alignment == LEFT)
			x = label_pos_x - tx;
		else if(prop_Alignment == RIGHT)
			x = label_pos_x;
		else
			x = label_pos_x - (float)tx/2;

		cr->move_to(x<0?0:(x>width-tx?width-tx:x), label_pos_y<0?0:(label_pos_y>height-ty?height-ty:label_pos_y));
	}
	else if(prop_Position.get_value() == Gtk::POS_RIGHT || prop_Position.get_value() == Gtk::POS_LEFT)
	{
		if(prop_alignmentEdge)
		{
			if(prop_Position.get_value() == Gtk::POS_RIGHT)
				label_pos_x = prop_scaleLabelOffset.get_value();
			else if(prop_Position.get_value() == Gtk::POS_LEFT)
				label_pos_x = width - tx - prop_scaleLabelOffset.get_value();
		}
		else
		{
			if(prop_Position.get_value() == Gtk::POS_RIGHT)
				label_pos_x = width - prop_scaleMajorHeight.get_value() - tx - prop_scaleLabelOffset.get_value();
			else if(prop_Position.get_value() == Gtk::POS_LEFT)
				label_pos_x = prop_scaleMajorHeight.get_value() + prop_scaleLabelOffset.get_value();
		}
		
		float y;
		if(prop_Alignment == TOP)
			y = label_pos_y - ty;
		else if(prop_Alignment == BOTTOM)
			y = label_pos_y;
		else
			y = label_pos_y - (float)ty/2;
		
		cr->move_to(label_pos_x<0?0:(label_pos_x>width-tx?width-tx:label_pos_x), y<0?0:(y>height-ty?height-ty:y));
	}
		
	layout_value_label->add_to_cairo_context(cr);
	cr->fill();
	cr->restore();
}
// -------------------------------------------------------------------------
void UScrollScale::draw_loop_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	double cur_value;
	if(prop_use_inert)
		cur_value = temp_value;
	else
		cur_value = value;
}
// -------------------------------------------------------------------------
void UScrollScale::draw_value(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	if(!prop_enableValueLabel)
		return;
	
	double cur_value;
	if(prop_use_inert)
		cur_value = temp_value;
	else
		cur_value = value;
	
	std::ostringstream title;
	if(property_fill_digits>0)
		title<<std::setw(property_fill_digits)<<setfill('0')<<(long)cur_value;
	else
		title<<(long)cur_value;
	if(property_digits>0)
		title<<","<<std::setw(property_digits)<<setfill('0')<<(long)(fabs(((float)cur_value-(long)cur_value )*pow10(property_digits)));
	title<<get_prop_valueLabelUnits();
	
	Glib::RefPtr<Pango::Layout> layout_value;
	layout_value = create_pango_layout("0");
	layout_value->set_alignment(Pango::ALIGN_CENTER);
	Pango::FontDescription fd(prop_valueLabelFont);
	fd.set_absolute_size(prop_valueLabelFontSize.get_value() * Pango::SCALE);
	layout_value->set_font_description(fd);
	layout_value->set_text(title.str());

	int tx, ty;
	layout_value->get_pixel_size(tx,ty);
	
	double k_rounding = prop_valueBGRounding.get_value();
	k_rounding = k_rounding>1? 1:(k_rounding<0? 0:k_rounding);
	float stroke_offset = 0.5*get_prop_valueBGStrokeWidth();
//	cout<<get_name()<<"::draw_value() fvalue="<<cur_value<<" cur_value="<<get_property_value()<< endl;
	float half_w = ((float)width)/2;
	float half_h = ((float)height)/2;
	float size_w, size_h;

	cr->save();
	
	if(prop_useValueBGImage)
	{
		if(prop_Position.get_value() == Gtk::POS_RIGHT || prop_Position.get_value() == Gtk::POS_LEFT)
		{
			size_h = MAX(ty,prop_valueBGMinSize.get_value());
			size_w = width - get_prop_scaleMinorHeight();
			Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(get_prop_valueBGImagePath(), width, size_h);
			Gdk::Cairo::set_source_pixbuf(cr,image, 0, half_h - ((float)size_h)/2);
		}
		else if(prop_Position.get_value() == Gtk::POS_TOP || prop_Position.get_value() == Gtk::POS_BOTTOM)
		{
			size_w = MAX(tx,prop_valueBGMinSize.get_value());
			size_h = height - get_prop_scaleMinorHeight();
			Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(get_prop_valueBGImagePath(), size_w, height);
			Gdk::Cairo::set_source_pixbuf(cr,image, half_w - ((float)size_w)/2, 0);
		}
		cr->paint();
	}
	else
	{
		cr->begin_new_sub_path();
		cr->set_line_width(get_prop_valueBGStrokeWidth());
		cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		
		if(prop_Position.get_value() == Gtk::POS_RIGHT)
		{
			size_w = width - get_prop_scaleMinorHeight();
			size_h = MAX(ty,prop_valueBGMinSize.get_value());
			float w = 0.5*size_h * k_rounding;
			int x_start = stroke_offset + w;
			int x_end = size_w - stroke_offset - w;
			float curve_w_right = 0.8068*(w - stroke_offset);
			float curve_w_left = 1.3334*(w - stroke_offset);
			float x_w = 0.9587*w;
			float y_hi = half_h - 0.15*size_h;
			float y_lo = half_h + 0.15*size_h;
			float y_top = half_h - 0.5*size_h + stroke_offset;
			float y_bottom = half_h + 0.5*size_h - stroke_offset;

			cr->move_to(x_end + x_w, y_lo);
			cr->curve_to(x_end + x_w, y_lo, x_end + curve_w_right, y_bottom, x_end, y_bottom);
			cr->line_to(x_start, y_bottom);
			cr->curve_to(x_start - curve_w_left, y_bottom, x_start - curve_w_left, y_top, x_start, y_top);
			cr->line_to(x_end, y_top);
			cr->curve_to(x_end + curve_w_right, y_top, x_end + x_w, y_hi, x_end + x_w, y_hi);
			cr->line_to(width - stroke_offset, half_h);
		}
		if(prop_Position.get_value() == Gtk::POS_LEFT)
		{
			size_w = width - get_prop_scaleMinorHeight();
			size_h = MAX(ty,prop_valueBGMinSize.get_value());
			float w = 0.5*size_h * k_rounding;
			int x_start = get_prop_scaleMinorHeight() + stroke_offset + w;
			int x_end = width - stroke_offset - w;
			float curve_w_right = 1.3334*(w - stroke_offset);
			float curve_w_left = 0.8068*(w - stroke_offset);
			float x_w = 0.9587*w;
			float y_hi = half_h - 0.15*size_h;
			float y_lo = half_h + 0.15*size_h;
			float y_top = half_h - 0.5*size_h + stroke_offset;
			float y_bottom = half_h + 0.5*size_h - stroke_offset;
			
			cr->move_to(x_start - x_w, y_lo);
			cr->curve_to(x_start - x_w, y_lo, x_start - curve_w_left, y_bottom, x_start, y_bottom);
			cr->line_to(x_end, y_bottom);
			cr->curve_to(x_end + curve_w_right, y_bottom, x_end + curve_w_right, y_top, x_end, y_top);
			cr->line_to(x_start, y_top);
			cr->curve_to(x_start - curve_w_left, y_top, x_start - x_w, y_hi, x_start - x_w, y_hi);
			cr->line_to(stroke_offset, half_h);
		}
		if(prop_Position.get_value() == Gtk::POS_TOP)
		{
			size_w = MAX(tx,prop_valueBGMinSize.get_value());
			size_h = height - get_prop_scaleMinorHeight();
			float w = 0.5*size_h * k_rounding;
			int x_start = half_w - 0.5*size_w + w;
			int x_end = half_w + 0.5*size_w - w;
			float curve_w = 1.3334*(w - stroke_offset);
			
			if(k_rounding==1.0)
			{
				float R = 0.5*size_h - stroke_offset;
				float x1 = half_w - (0.5*size_w - R - stroke_offset);
				float x2 = half_w + (0.5*size_w - R - stroke_offset);
				cr->arc( x2, get_prop_scaleMinorHeight() + R + 0.5*stroke_offset, R, -0.5*M_PI, 0.5*M_PI );
				cr->arc( x1, get_prop_scaleMinorHeight() + R + 0.5*stroke_offset, R, 0.5*M_PI, 1.5*M_PI );
			}
			else
			{
				cr->move_to(x_end, get_prop_scaleMinorHeight() + stroke_offset);
				cr->curve_to(x_end + curve_w, get_prop_scaleMinorHeight() + stroke_offset, x_end + curve_w, height - stroke_offset, x_end, height - stroke_offset);
				cr->line_to(x_start, height - stroke_offset);
				cr->curve_to(x_start - curve_w, height - stroke_offset, x_start - curve_w, get_prop_scaleMinorHeight() + stroke_offset, x_start, get_prop_scaleMinorHeight() + stroke_offset);
			}
			cr->line_to(half_w - 0.05*size_w, get_prop_scaleMinorHeight() + stroke_offset);
			cr->line_to(half_w, stroke_offset);
			cr->line_to(half_w + 0.05*size_w, get_prop_scaleMinorHeight() + stroke_offset);
		}
		if(prop_Position.get_value() == Gtk::POS_BOTTOM)
		{
			size_w = MAX(tx,prop_valueBGMinSize.get_value());
			size_h = height - get_prop_scaleMinorHeight();
			float w = 0.5*size_h * k_rounding;
			int x_start = half_w - 0.5*size_w + w;
			int x_end = half_w + 0.5*size_w - w;
			float curve_w = 1.3334*(w - stroke_offset);
			
			if(k_rounding==1.0)
			{
				float R = 0.5*size_h - stroke_offset;
				float x1 = half_w - (0.5*size_w - R - stroke_offset);
				float x2 = half_w + (0.5*size_w - R - stroke_offset);
				cr->arc( x1, R + 0.5*stroke_offset, R, 0.5*M_PI, 1.5*M_PI );
				cr->arc( x2, R + 0.5*stroke_offset, R, -0.5*M_PI, 0.5*M_PI );
			}
			else
			{
				cr->move_to(x_start, size_h - stroke_offset);
				cr->curve_to(x_start - curve_w, size_h - stroke_offset, x_start - curve_w, stroke_offset, x_start, stroke_offset);
				cr->line_to(x_end, stroke_offset);
				cr->curve_to(x_end + curve_w, stroke_offset, x_end + curve_w, size_h - stroke_offset, x_end, size_h - stroke_offset);
			}
			cr->line_to(half_w + 0.05*size_w, size_h - stroke_offset);
			cr->line_to(half_w, height - stroke_offset);
			cr->line_to(half_w - 0.05*size_w, size_h - stroke_offset);
		}
		
		cr->close_path();
		Cairo::Path *bg = cr->copy_path();
		
		if(prop_valueBGUseGradientColor)
		{
			Cairo::RefPtr<Cairo::LinearGradient> gradient;
			
			if(prop_Position.get_value() == Gtk::POS_RIGHT || prop_Position.get_value() == Gtk::POS_LEFT)
				gradient = Cairo::LinearGradient::create(0, half_h - 0.5*size_h + get_prop_valueBGStrokeWidth(), 0, half_h + 0.5*size_h - get_prop_valueBGStrokeWidth());
			else if(prop_Position.get_value() == Gtk::POS_TOP || prop_Position.get_value() == Gtk::POS_BOTTOM)
				gradient = Cairo::LinearGradient::create(0, get_prop_valueBGStrokeWidth(), 0, height - get_prop_scaleMinorHeight() - get_prop_valueBGStrokeWidth());
			
			gradient->add_color_stop_rgba(0, get_prop_valueBGFillColor2().get_red_p(), get_prop_valueBGFillColor2().get_green_p(), get_prop_valueBGFillColor2().get_blue_p(),1.0);
			gradient->add_color_stop_rgba(1.0, get_prop_valueBGFillColor1().get_red_p(), get_prop_valueBGFillColor1().get_green_p(), get_prop_valueBGFillColor1().get_blue_p(),1.0);
			cr->set_source(gradient);
		}
		else
			Gdk::Cairo::set_source_color(cr, get_prop_valueBGFillColor1());
		cr->fill();
		
		cr->append_path(*bg);
		if(prop_valueBGStrokeUseGradientColor)
		{
			Cairo::RefPtr<Cairo::LinearGradient> gradient;
			
			if(prop_Position.get_value() == Gtk::POS_RIGHT || prop_Position.get_value() == Gtk::POS_LEFT)
				gradient = Cairo::LinearGradient::create(0, half_h - 0.5*size_h + get_prop_valueBGStrokeWidth(), 0, half_h + 0.5*size_h - get_prop_valueBGStrokeWidth());
			else if(prop_Position.get_value() == Gtk::POS_TOP || prop_Position.get_value() == Gtk::POS_BOTTOM)
				gradient = Cairo::LinearGradient::create(0, get_prop_valueBGStrokeWidth(), 0, height - get_prop_scaleMinorHeight() - get_prop_valueBGStrokeWidth());
			
			gradient->add_color_stop_rgba(0, get_prop_valueBGFillColor1().get_red_p(), get_prop_valueBGFillColor1().get_green_p(), get_prop_valueBGFillColor1().get_blue_p(),1.0);
			gradient->add_color_stop_rgba(1.0, get_prop_valueBGFillColor2().get_red_p(), get_prop_valueBGFillColor2().get_green_p(), get_prop_valueBGFillColor2().get_blue_p(),1.0);
			cr->set_source(gradient);
		}
		else
			Gdk::Cairo::set_source_color(cr, get_prop_valueBGFillColor2());
		cr->stroke();
	}
	
	float label_pos_x, label_pos_y;
	Gdk::Cairo::set_source_color(cr, get_prop_valueLabelColor());
	if(prop_Position.get_value() == Gtk::POS_RIGHT)
	{
		label_pos_x = 0.5*(size_w - tx);
		label_pos_y = half_h - 0.5*ty;
	}
	else if(prop_Position.get_value() == Gtk::POS_LEFT)
	{
		label_pos_x = get_prop_scaleMinorHeight() + 0.5*(size_w - tx);
		label_pos_y = half_h - 0.5*ty;
	}
	else if(prop_Position.get_value() == Gtk::POS_TOP)
	{
		label_pos_x = half_w - 0.5*tx;
		label_pos_y = get_prop_scaleMinorHeight() + 0.5*(size_h - ty);
	}
	else if(prop_Position.get_value() == Gtk::POS_BOTTOM)
	{
		label_pos_x = half_w - 0.5*tx;
		label_pos_y = 0.5*(size_h - ty);
	}
	cr->move_to(label_pos_x<0?0:(label_pos_x>width-tx?width-tx:label_pos_x), label_pos_y<0?0:(label_pos_y>height-ty?height-ty:label_pos_y));
	layout_value->add_to_cairo_context(cr);
	cr->fill();
	
	cr->restore();
	
}
// -------------------------------------------------------------------------
void UScrollScale::set_sensor_ai(const ObjectId sens_id, const ObjectId node_id)
{
    /* Set new sens_id */
    sensor_ai_.set_sens_id(sens_id);
}
// -------------------------------------------------------------------------
void UScrollScale::set_precisions(const int precisions)
{
    set_property_precision(precisions);
}
// -------------------------------------------------------------------------
void
UScrollScale::set_connector(const ConnectorRef& connector) throw()
{
    if (connector == get_connector())
        return;

    UVoid::set_connector(connector);

    sensor_ai_.set_connector(connector);

    sensor_connection_.disconnect();

    if (get_connector() == true) {
        sigc::slot<void, ObjectId, ObjectId, long> process_sensor_slot =
                sigc::mem_fun(this, &UScrollScale::process_sensor);
        sensor_connection_ = get_connector()->signals().connect_value_changed(
                process_sensor_slot,
                sensor_ai_.get_sens_id(),
                sensor_ai_.get_node_id());
        on_connect();
    }
}
// -------------------------------------------------------------------------
void
UScrollScale::on_connect() throw()
{
    UVoid::on_connect();

    float new_value = get_connector()->
        get_analog_value(sensor_ai_.get_sens_id(), sensor_ai_.get_node_id());

    process_sensor(sensor_ai_.get_sens_id(), sensor_ai_.get_node_id(), new_value);

    if(get_property_disconnect_effect() > 0)
        queue_draw();
}
// -------------------------------------------------------------------------
void
UScrollScale::on_disconnect() throw()
{
    UVoid::on_connect();
    UVoid::on_disconnect();

    if(get_property_disconnect_effect() > 0)
        queue_draw();
}
// -------------------------------------------------------------------------
void
UScrollScale::process_sensor(ObjectId id, ObjectId node, float val)
{
//	cout<<get_name()<<"::process_sensor id="<<id<<" node="<<node<<" val="<<val<<endl;
    set_property_value(val);
}
// -------------------------------------------------------------------------
std::list<Glib::ustring> UScrollScale::explodestr( const Glib::ustring str, char sep )
{
	list<Glib::ustring> l;
	
	string::size_type prev = 0;
	string::size_type pos = 0;
	do
	{
		pos = str.find(sep,prev);
		string s(str.substr(prev,pos-prev));
		if( !s.empty() )
		{
			l.push_back(s);
			prev=pos+1;
		}
	}
	while( pos!=string::npos );
	
	return l;
}
// -------------------------------------------------------------------------
