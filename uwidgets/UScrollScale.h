#ifndef _USCROLLSCALE_H
#define _USCROLLSCALE_H
// -------------------------------------------------------------------------
#include <uwidgets/UEventBox.h>
#include <unordered_map>
#include <map>
#include <gtkmm.h>
#include "UInertia.h"
#include "UCalibrate.h"
#include "plugins.h"
#include "global_macros.h"
#include <unordered_map>
// -------------------------------------------------------------------------
/*!
 * \brief Класс цифрового индикатора.
 * \par
 * Класс предназначен для реализации виджета "прокручивающаяся шкала ".
*/
class UScrollScale : public UEventBox
{
public:
	UScrollScale();
	explicit UScrollScale(GtkmmBaseType::BaseObjectType* gobject);
	~UScrollScale() {}

	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void set_connector(const ConnectorRef& connector) throw();

	/* Set methods for widget properties */
	/*! задать аналоговый датчик для виджета.
	    \param sens_id id датчика.
	    \param node_id id узла датчика.
	*/
	void set_sensor_ai(const UniWidgetsTypes::ObjectId sens_id, const UniWidgetsTypes::ObjectId node_id);
	/*! задать точность после запятой отображаемого значения.
	    \param precisions заданная точность значения.
	*/
	void set_precisions(const int precisions);
	/*! задать количество цифр используемых как ширина поля отображения.
	    \param digits заданная ширина поля отображения.
	*/
	void set_digits(const int digits);
	/*! задать символ для заполнения неиспользуемых разрядов.
	    \param digits заданный заполнитель.
	*/
	void set_fill_digits(const int digits);
	/*! задать цвет текста.
	    \param font_color заданный цвет текста.
	*/
	void set_font_color(const long font_color);

    inline void set_value(double value) { set_property_value(value); }

    enum AlignType
    {
		CENTER=0
		,TOP
		,BOTTOM
		,LEFT
		,RIGHT
	};

	typedef std::unordered_map<float,Glib::ustring> scaleExceptions;

	static std::list<Glib::ustring> explodestr( const Glib::ustring str, char sep );
	
protected:
	virtual void on_realize();
	virtual bool on_expose_event(GdkEventExpose*);
	virtual void allocate_changed(Gtk::Allocation& alloc);
	virtual bool on_scale_event(GdkEvent* ev);
	
	virtual void draw_image_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_loop_scale(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_value(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_scale_text(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc, double label_pos_x, double label_pos_y, float val);
	
	bool is_show;
	bool action_start;
	int width;
	int height;

	double last_value;
	double value;
	double temp_value;
	sigc::connection inert_signal;
	virtual void on_inert_value_changed(double val, bool inert_is_stoped);
	
	Glib::RefPtr<Pango::Layout> value_layout;

	scaleExceptions exceptions;

	virtual void on_value_changed();
	virtual void on_exceptions_changed();
	
private:
	void ctor();
	void process_sensor(UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, float);

	sigc::connection sensor_connection_;

	SensorProp sensor_ai_;
	UCalibrate calibrate;
	ADD_PROPERTY(property_value, double)
	ADD_PROPERTY(property_precision, int)

	ADD_PROPERTY( prop_use_inert, bool )						/*!< свойство: использовать инерцию для ползунка*/
	UInertia inert;

	ADD_PROPERTY( prop_Position, Gtk::PositionType )	/*!< свойство: положение шкалы */
	ADD_PROPERTY( prop_loopScale, bool )				/*!< свойство: циклически повторять шкалу (после максимального идет снова минимальное значение)*/
	ADD_PROPERTY( prop_minLimited, bool )				/*!< свойство: ограничивать шкалу Min значением*/
	ADD_PROPERTY( prop_Min, float )						/*!< свойство: значение соответствующее началу шкалы*/
	ADD_PROPERTY( prop_maxLimited, bool )				/*!< свойство: ограничивать шкалу Max значением*/
	ADD_PROPERTY( prop_Max, float )						/*!< свойство: значение соответствующее концу шкалы*/
	ADD_PROPERTY( prop_visibleRange, float )			/*!< свойство: видимый диапазон шкалы шкалы*/
	ADD_PROPERTY( prop_maxMinor, int )					/*!< свойство: количество малых делений внутри больших*/
	ADD_PROPERTY( prop_maxMajor, int )					/*!< свойство: количество больших делений*/
	ADD_PROPERTY( prop_scalemarkWidth, int )     		/*!< свойство: толщина линий делений шкалы */
	ADD_PROPERTY( prop_scaleMinorHeight, int )     		/*!< свойство: длинна малых делений*/
	ADD_PROPERTY( prop_scaleMajorHeight, int )     		/*!< свойство: длинна больших делений*/
	ADD_PROPERTY( prop_scaleMinorColor, Gdk::Color )	/*!< свойство: цвет малых делений*/
	ADD_PROPERTY( prop_scaleMajorColor, Gdk::Color )	/*!< свойство: цвет больших делений*/

	ADD_PROPERTY( prop_useImage, bool )					/*!< свойство: использовать картинку*/
	ADD_PROPERTY( prop_image, Glib::ustring )			/*!< свойство: путь к файлу картинки*/
	ADD_PROPERTY( prop_imageMin, float )				/*!< свойство: значение соответствующее началу шкалы картинки*/
	ADD_PROPERTY( prop_imageMax, float )				/*!< свойство: значение соответствующее концу шкалы картинки*/
	
	ADD_PROPERTY( property_digits, int )				/*!< свойство: */
	ADD_PROPERTY( property_fill_digits, int )			/*!< свойство: */
	ADD_PROPERTY( prop_scaleLabelColor, Gdk::Color )	/*!< свойство: цвет подписей значений шкалы*/
	ADD_PROPERTY( prop_scaleLabelFont, Glib::ustring )	/*!< свойство: шрифт подписей значений шкалы*/
	ADD_PROPERTY( prop_scaleLabelFontSize, int )		/*!< свойство: размер шрифта подписей значений шкалы*/
	ADD_PROPERTY( prop_Alignment, AlignType )			/*!< свойство: положение подписей относительно шкалы */
	ADD_PROPERTY( prop_alignmentEdge, bool )			/*!< свойство: выравнивание подписей значений шкалы по краю виджета*/
	ADD_PROPERTY( prop_scaleLabelOffset, int )			/*!< свойство: отступ положения значений шкалы от делений*/

	ADD_PROPERTY( prop_useExceptions, bool )			/*!< свойство: использовать исключения для замены подписей шкалы*/
	ADD_PROPERTY( prop_scaleExceptions, Glib::ustring )	/*!< свойство: исключения для замены подписей шкалы*/
	ADD_PROPERTY( prop_exceptionsColor, Gdk::Color )	/*!< свойство: цвет исключений для замены подписей шкалы*/
	ADD_PROPERTY( prop_exceptionsFont, Glib::ustring )	/*!< свойство: шрифт исключений для замены подписей шкалы*/
	ADD_PROPERTY( prop_exceptionsFontSize, int )		/*!< свойство: размер шрифта исключений для замены подписей шкалы*/
	
	ADD_PROPERTY( prop_enableValueLabel, bool )					/*!< свойство: включить центральную подпись значения шкалы*/
	ADD_PROPERTY( prop_valueBGMinSize, int )					/*!< свойство: минимальный размер задника подписи значения шкалы*/
	ADD_PROPERTY( prop_useValueBGImage, bool )					/*!< свойство: использовать картинку для задника подписи значения шкалы*/
	ADD_PROPERTY( prop_valueBGImagePath, Glib::ustring)			/*!< свойство: путь к файлу картинки задника подписи значения шкалы*/
	ADD_PROPERTY( prop_valueBGUseGradientColor, bool )			/*!< свойство: вкл/выкл градиента для цвета задника подписи значения шкалы*/
	ADD_PROPERTY( prop_valueBGStrokeUseGradientColor, bool )	/*!< свойство: вкл/выкл градиента для цвета обводки задника подписи значения шкалы*/
	ADD_PROPERTY( prop_valueBGFillColor1, Gdk::Color )			/*!< свойство: 1й цвет заливки задника подписи значения шкалы*/
	ADD_PROPERTY( prop_valueBGFillColor2, Gdk::Color )			/*!< свойство: 2й цвет заливки задника подписи значения шкалы*/
	ADD_PROPERTY( prop_valueBGRounding, double )				/*!< свойство: коэфициениет скругления углов задника подписи значения шкалы*/
	ADD_PROPERTY( prop_valueBGStrokeWidth, int )				/*!< свойство: ширина обводки задника подписи значения шкалы*/
	ADD_PROPERTY( prop_valueLabelUnits, Glib::ustring )			/*!< свойство: единицы измерения центральной подписи значения шкалы*/
	ADD_PROPERTY( prop_valueLabelColor, Gdk::Color )			/*!< свойство: цвет центральной подписи значения шкалы*/
	ADD_PROPERTY( prop_valueLabelFont, Glib::ustring )			/*!< свойство: шрифт центральной подписи значения шкалы*/
	ADD_PROPERTY( prop_valueLabelFontSize, int )				/*!< свойство: размер шрифта центральной подписи значения шкалы*/
};

namespace Glib
{
	template <>
	class Value<UScrollScale::AlignType> : public Value_Enum<UScrollScale::AlignType>
	{
	public:
		static GType value_type() G_GNUC_CONST;
	};
} // namespace Glib
#endif
