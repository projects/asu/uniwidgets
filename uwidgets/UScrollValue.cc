#include "types.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <gdkmm.h>
#include <librsvg/rsvg.h>
//#include <librsvg/rsvg-cairo.h>
#include "UScrollValue.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using Glib::ustring;
// -------------------------------------------------------------------------
#define UScrollValue_INIT_PROPERTIES() \
    sensor_ai_(this,"ai", get_connector()) \
    ,calibrate(this) \
    ,property_value(*this,"value",0) \
    ,property_precision(*this,"precision",0) \
    \
    ,uquant(this) \
    \
    ,prop_use_inert(*this, "use-inertia",false) \
    ,inert(this) \
    \
    ,prop_passiveMode(*this, "passive-mode",false) \
    \
    ,prop_scaleColor(*this, "scale-color" , Gdk::Color("white")) \
    ,prop_scaleLabelColor(*this, "scale-label-color", Gdk::Color("white") ) \
    ,prop_scaleLabelFont(*this, "scale-label-font", "Liberation Sans Bold" ) \
    ,prop_scaleLabelFontSize(*this, "scale-label-font-size", 12 ) \
    ,prop_orientation(*this, "orientation", Gtk::ORIENTATION_VERTICAL ) \
    ,prop_topScale(*this, "top-scale", true) \
    ,prop_bottomScale(*this, "bottom-scale", true) \
    ,prop_leftScale(*this, "left-scale", true) \
    ,prop_rightScale(*this, "right-scale", true) \
    \
    ,prop_Min(*this,"min-value",0) \
    ,prop_Max(*this,"max-value",100) \
    ,prop_maxMinor(*this, "max-scale-minor" , 10) \
    ,prop_maxMajor(*this, "max-scale-major" , 10) \
    \
    ,prop_useBGImage(*this, "use-bg-image", false) \
    ,prop_BGImagePath(*this, "bg-image-path", "") \
    ,prop_useScrollImage(*this, "use-scroll-image", false) \
    ,prop_ScrollImagePath(*this, "scroll-image-path", "") \
    \
    ,prop_bgUseGradientColor(*this, "bg-use-gradient-color", true) \
    ,prop_bgStrokeUseGradientColor(*this, "bg-stroke-use-gradient-color", true) \
    ,prop_bgFillColor1(*this, "bg-fill-color1", Gdk::Color("white") ) \
    ,prop_bgFillColor2(*this, "bg-fill-color2", Gdk::Color("gray") ) \
    ,prop_scrollUseGradientColor(*this, "scroll-use-gradient-color", true) \
    ,prop_scrollStrokeUseGradientColor(*this, "scroll-stroke-use-gradient-color", true) \
    ,prop_scrollFillColor1(*this, "scroll-fill-color1", Gdk::Color("white") ) \
    ,prop_scrollFillColor2(*this, "scroll-fill-color2", Gdk::Color("gray") ) \
    \
    ,prop_rounding(*this,"rounding", 0.5) \
    ,prop_bgStrokeWidth(*this,"bg-stroke-width", 2) \
    ,prop_scrollStrokeWidth(*this,"scroll-stroke-width", 2) \
    ,prop_useScaleLine(*this,"use-scale-line-on-scroll", false) \
    \
    ,prop_showValueLabel(*this,"show-value-label", false) \
    ,prop_valueJustify(*this, "value-label-justify", UScrollValue::CENTER ) \
    ,prop_valueLabelColor(*this, "value-label-color", Gdk::Color("white") ) \
    ,prop_valueLabelFont(*this, "value-label-font", "Liberation Sans Bold" ) \
    ,prop_valueLabelFontSize(*this, "value-label-font-size", 12 ) \
    ,prop_valueLabelUnits(*this, "value-label-units", "" ) \
    \
    ,is_show(false) \
    ,action_start(false) \
    \
    ,last_value(0) \
    ,value(0) \
    ,temp_value(0) \

// -------------------------------------------------------------------------
void UScrollValue::ctor()
{
    set_visible_window(false);

	value_layout= create_pango_layout(" ");
	value_layout->set_alignment(Pango::ALIGN_CENTER);
	
	push_composite_child();
	{
		table = manage(new Gtk::Table(3,3));
		add(*table);
		
		action_area = manage(new UEventBox());
		action_area->set_visible_window(false);
		action_area->signal_size_allocate().connect(sigc::mem_fun(*this, &UScrollValue::allocate_changed));
		action_area->signal_event().connect(sigc::mem_fun(*this, &UScrollValue::action_area_event));
		table->attach(*action_area,1,2,1,2);
		
		Gtk::HBox *topBox = manage(new Gtk::HBox());
		topRightPlaceholder = manage(new Gtk::EventBox());
		topRightPlaceholder->set_visible_window(false);
		topLeftPlaceholder = manage(new Gtk::EventBox());
		topLeftPlaceholder->set_visible_window(false);
		topScaleBox = manage(new Gtk::VBox());
		topBox->pack_start(*topRightPlaceholder,Gtk::PACK_SHRINK);
//		topBox->pack_start(*topScaleBox,Gtk::PACK_EXPAND_PADDING);
		topBox->add(*topScaleBox);
		topBox->pack_start(*topLeftPlaceholder,Gtk::PACK_SHRINK);
		
		table->attach(*topBox,1,2,0,1,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);
		
		HScaleLabels *topScaleLabel = manage(new HScaleLabels(Gtk::POS_TOP));
		topScaleLabel->set_name("topScaleLabel");
		topScale = manage(new HScale(Gtk::POS_TOP,topScaleLabel));
		topScale->set_enabled(true);
		topScale->set_range(0,100);
		topScaleBox->pack_start(*topScaleLabel,Gtk::PACK_SHRINK);
		topScaleBox->pack_start(*topScale,Gtk::PACK_SHRINK);
		topLabel = static_cast<HScaleLabels*>(topScale->get_labels());
		
		Gtk::HBox *bottomBox = manage(new Gtk::HBox());
		bottomRightPlaceholder = manage(new Gtk::EventBox());
		bottomRightPlaceholder->set_visible_window(false);
		bottomLeftPlaceholder = manage(new Gtk::EventBox());
		bottomLeftPlaceholder->set_visible_window(false);
		bottomScaleBox = manage(new Gtk::VBox());
		bottomBox->pack_start(*bottomRightPlaceholder,Gtk::PACK_SHRINK);
//		bottomBox->pack_start(*bottomScaleBox,Gtk::PACK_EXPAND_PADDING);
		bottomBox->add(*bottomScaleBox);
		bottomBox->pack_start(*bottomLeftPlaceholder,Gtk::PACK_SHRINK);
		
		table->attach(*bottomBox,1,2,2,3,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);
		
		HScaleLabels *bottomScaleLabel = manage(new HScaleLabels(Gtk::POS_BOTTOM));
		bottomScaleLabel->set_name("bottomScaleLabel");
		bottomScale = manage(new HScale(Gtk::POS_BOTTOM,bottomScaleLabel));
		bottomScale->set_enabled(true);
		bottomScale->set_range(0,100);
		bottomScaleBox->pack_start(*bottomScale,Gtk::PACK_SHRINK);
		bottomScaleBox->pack_start(*bottomScaleLabel,Gtk::PACK_SHRINK);
		bottomLabel = static_cast<HScaleLabels*>(bottomScale->get_labels());
		
		Gtk::VBox *leftBox = manage(new Gtk::VBox());
		leftTopPlaceholder = manage(new Gtk::EventBox());
		leftTopPlaceholder->set_visible_window(false);
		leftBottomPlaceholder = manage(new Gtk::EventBox());
		leftBottomPlaceholder->set_visible_window(false);
		leftScaleBox = manage(new Gtk::HBox());
		leftBox->pack_start(*leftTopPlaceholder,Gtk::PACK_SHRINK);
//		leftBox->pack_start(*leftScaleBox,Gtk::PACK_EXPAND_PADDING);
		leftBox->add(*leftScaleBox);
		leftBox->pack_start(*leftBottomPlaceholder,Gtk::PACK_SHRINK);
		VScaleLabels *leftScaleLabel = manage(new VScaleLabels(Gtk::POS_LEFT));
		leftScale = manage(new VScale(Gtk::POS_LEFT,leftScaleLabel));
		leftScale->set_enabled(true);
		leftScale->set_range(0,100);
		leftScaleBox->pack_start(*leftScaleLabel,Gtk::PACK_SHRINK);
		leftScaleBox->pack_start(*leftScale,Gtk::PACK_SHRINK);
		table->attach(*leftBox,0,1,1,2,Gtk::SHRINK,Gtk::FILL|Gtk::EXPAND);
		
		Gtk::VBox *rightBox = manage(new Gtk::VBox());
		rightTopPlaceholder = manage(new Gtk::EventBox());
		rightTopPlaceholder->set_visible_window(false);
		rightBottomPlaceholder = manage(new Gtk::EventBox());
		rightBottomPlaceholder->set_visible_window(false);
		rightScaleBox = manage(new Gtk::HBox());
		rightBox->pack_start(*rightTopPlaceholder,Gtk::PACK_SHRINK);
//		rightBox->pack_start(*rightScaleBox,Gtk::PACK_EXPAND_PADDING);
		rightBox->add(*rightScaleBox);
		rightBox->pack_start(*rightBottomPlaceholder,Gtk::PACK_SHRINK);
		VScaleLabels *rightScaleLabel = manage(new VScaleLabels(Gtk::POS_RIGHT));
		rightScale = manage(new VScale(Gtk::POS_RIGHT,rightScaleLabel));
		rightScale->set_enabled(true);
		rightScale->set_range(0,100);
		rightScaleBox->pack_start(*rightScale,Gtk::PACK_SHRINK);
		rightScaleBox->pack_start(*rightScaleLabel,Gtk::PACK_SHRINK);
		table->attach(*rightBox,2,3,1,2,Gtk::SHRINK,Gtk::FILL|Gtk::EXPAND);
		
		show_all();
	}
	pop_composite_child();
	
    connect_property_changed("value", sigc::mem_fun(*this, &UScrollValue::on_value_changed) );
	connect_property_changed("enable-calibrate", sigc::mem_fun(*this, &UScrollValue::on_value_changed) );
	connect_property_changed("enable-calibrate-limit", sigc::mem_fun(*this, &UScrollValue::on_value_changed) );
	connect_property_changed("calibrate-rmin", sigc::mem_fun(*this, &UScrollValue::on_value_changed) );
	connect_property_changed("calibrate-rmax", sigc::mem_fun(*this, &UScrollValue::on_value_changed) );
	connect_property_changed("calibrate-cmin", sigc::mem_fun(*this, &UScrollValue::on_value_changed) );
	connect_property_changed("calibrate-cmax", sigc::mem_fun(*this, &UScrollValue::on_value_changed) );
	connect_property_changed("precision", sigc::mem_fun(*this, &UScrollValue::on_value_changed) );
	connect_property_changed("precision", sigc::mem_fun(*this, &UScrollValue::on_value_changed) );

	connect_property_changed("min-value", sigc::mem_fun(*this, &UScrollValue::on_scales_range_changed) );
	connect_property_changed("max-value", sigc::mem_fun(*this, &UScrollValue::on_scales_range_changed) );
	connect_property_changed("max-scale-minor", sigc::mem_fun(*this, &UScrollValue::on_scales_range_changed) );
	connect_property_changed("max-scale-major", sigc::mem_fun(*this, &UScrollValue::on_scales_range_changed) );
	connect_property_changed("scale-color", sigc::mem_fun(*this, &UScrollValue::on_scale_color_changed) );
	connect_property_changed("scale-label-color", sigc::mem_fun(*this, &UScrollValue::on_font_color_changed) );
	connect_property_changed("scale-label-font", sigc::mem_fun(*this, &UScrollValue::on_font_changed) );
	connect_property_changed("scale-label-font-size", sigc::mem_fun(*this, &UScrollValue::on_font_changed) );
	connect_property_changed("orientation", sigc::mem_fun(*this, &UScrollValue::on_scales_enable_changed) );
	connect_property_changed("top-scale", sigc::mem_fun(*this, &UScrollValue::on_scales_enable_changed) );
	connect_property_changed("bottom-scale", sigc::mem_fun(*this, &UScrollValue::on_scales_enable_changed) );
	connect_property_changed("left-scale", sigc::mem_fun(*this, &UScrollValue::on_scales_enable_changed) );
	connect_property_changed("right-scale", sigc::mem_fun(*this, &UScrollValue::on_scales_enable_changed) );
	connect_property_changed("rounding", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
//	connect_property_changed("bg-stroke-width", bind(sigc::mem_fun(*this, &UScrollValue::allocate_changed), action_area->get_allocation()) );
	connect_property_changed("bg-stroke-width", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("scroll-stroke-width", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("use-scale-line-on-scroll", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("use-bg-image", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("bg-image-path", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("use-scroll-image", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("scroll-image-path", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("show-value-label", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("value-label-justify", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("value-label-color", sigc::mem_fun(*this, &UScrollValue::queue_draw) );
	connect_property_changed("value-label-font", sigc::mem_fun(*this, &UScrollValue::on_value_font_changed) );
	connect_property_changed("value-label-font-size", sigc::mem_fun(*this, &UScrollValue::on_value_font_changed) );
	
//    signal_size_allocate().connect(sigc::mem_fun(*this, &UScrollValue::allocate_changed));
    set_events(~Gdk::ALL_EVENTS_MASK);
    signal_event().connect(sigc::mem_fun(*this, &UScrollValue::on_indicator_event));
}
// -------------------------------------------------------------------------
UScrollValue::UScrollValue() :
    Glib::ObjectBase("uscrollvalue")
    ,UScrollValue_INIT_PROPERTIES()
{
    ctor();
}
// -------------------------------------------------------------------------
UScrollValue::UScrollValue(GtkmmBaseType::BaseObjectType* gobject) :
    UEventBox(gobject)
    ,UScrollValue_INIT_PROPERTIES()
{
    ctor();
}
// -------------------------------------------------------------------------
GType justifytype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { UScrollValue::CENTER, "CENTER", "центр" },{ UScrollValue::TOP, "TOP", "сверху" },{ UScrollValue::BOTTOM, "BOTTOM", "снизу" },{ UScrollValue::LEFT, "LEFT", "слева" },{ UScrollValue::RIGHT, "RIGHT", "справа" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("JusifyType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UScrollValue::JusifyType>::value_type()
{
	return justifytype_get_type();
}
// -------------------------------------------------------------------------
void UScrollValue::on_realize()
{
	//	cout<<get_name()<<"::on_realize..."<<endl;
	UEventBox::on_realize();
	on_scales_enable_changed();
	on_scale_color_changed();
	on_font_color_changed();
	on_font_changed();
	on_scales_range_changed();

	if(get_connector())
		process_sensor(sensor_ai_.get_sens_id(),sensor_ai_.get_node_id(),sensor_ai_.get_value());
	
	queue_draw();
}
// -------------------------------------------------------------------------
void UScrollValue::on_scales_enable_changed()
{
	if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
	{
		bottomScale->set_enabled(false);
		topScale->set_enabled(false);
		leftScale->set_enabled(prop_leftScale);
		rightScale->set_enabled(prop_rightScale);
	}
	else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
	{
		bottomScale->set_enabled(prop_bottomScale);
		topScale->set_enabled(prop_topScale);
		leftScale->set_enabled(false);
		rightScale->set_enabled(false);
	}
}
// -------------------------------------------------------------------------
void UScrollValue::on_scale_color_changed()
{
	topScale->set_scale_color(prop_scaleColor);
	bottomScale->set_scale_color(prop_scaleColor);
	leftScale->set_scale_color(prop_scaleColor);
	rightScale->set_scale_color(prop_scaleColor);
}
// -------------------------------------------------------------------------
void UScrollValue::on_font_color_changed()
{
	topScale->get_labels()->set_labels_color(prop_scaleLabelColor);
	bottomScale->get_labels()->set_labels_color(prop_scaleLabelColor);
	leftScale->get_labels()->set_labels_color(prop_scaleLabelColor);
	rightScale->get_labels()->set_labels_color(prop_scaleLabelColor);
}
// -------------------------------------------------------------------------
void UScrollValue::on_font_changed()
{
	Pango::FontDescription fd(prop_scaleLabelFont);
	fd.set_absolute_size(prop_scaleLabelFontSize.get_value() * Pango::SCALE);
	topScale->get_labels()->set_labels_font(fd);
	bottomScale->get_labels()->set_labels_font(fd);
	leftScale->get_labels()->set_labels_font(fd);
	rightScale->get_labels()->set_labels_font(fd);
}
// -------------------------------------------------------------------------
void UScrollValue::on_scales_range_changed()
{
//	cout<<get_name()<<"::on_scales_range_changed() get_prop_Min()="<<get_prop_Min()<<" get_prop_Max()="<<get_prop_Max()<<" get_prop_maxMajor()="<<get_prop_maxMajor()<<" get_prop_Min()="<<get_prop_Min()<<endl;
	leftScale->set_range(get_prop_Min(),get_prop_Max());
	leftScale->set_max_minor(get_prop_maxMinor());
	leftScale->set_max_major(get_prop_maxMajor());
	
	rightScale->set_range(get_prop_Min(),get_prop_Max());
	rightScale->set_max_minor(get_prop_maxMinor());
	rightScale->set_max_major(get_prop_maxMajor());
	
	
	topScale->set_range(get_prop_Min(),get_prop_Max());
	topScale->set_max_minor(get_prop_maxMinor());
	topScale->set_max_major(get_prop_maxMajor());

	bottomScale->set_range(get_prop_Min(),get_prop_Max());
	bottomScale->set_max_minor(get_prop_maxMinor());
	bottomScale->set_max_major(get_prop_maxMajor());
	
	queue_draw();
}
// -------------------------------------------------------------------------
void UScrollValue::on_value_font_changed()
{
	Pango::FontDescription font(prop_valueLabelFont.get_value());
	font.set_absolute_size(prop_valueLabelFontSize.get_value() * Pango::SCALE);
	value_layout->set_font_description(font);
}
// -------------------------------------------------------------------------
void UScrollValue::on_value_changed()
{
	//	cout<<get_name()<<"::on_value_changed value="<<property_value.get_value()<< endl;
	double prec_value = calibrate.get_cvalue(property_value.get_value())/pow10(get_property_precision());
	if(prop_use_inert)
	{
		last_value = temp_value;
		value = prec_value;
		if(prec_value < prop_Min.get_value())
			value = prop_Min.get_value();
		else if(prec_value > prop_Max.get_value())
			value = prop_Max.get_value();
		
		if (!inert_signal.connected())
		{
			inert_signal= inert.signal_inert_value_change().connect(sigc::mem_fun(*this, &UScrollValue::on_inert_value_changed));
		}
		inert.set_value( last_value, value );
	}
	else
	{
		if (inert_signal.connected())
			inert_signal.disconnect();
		last_value = value;
		value = prec_value;
		if(prec_value < prop_Min.get_value())
			value = prop_Min.get_value();
		else if(prec_value > prop_Max.get_value())
			value = prop_Max.get_value();
		temp_value = value;
	}
	//	cout<<get_name()<<"::on_value_changed value="<<value<<" temp_value="<<temp_value<< endl;
	
	queue_draw();
}
// -------------------------------------------------------------------------
void UScrollValue::on_inert_value_changed(double val, bool inert_is_stoped)
{
	//	cout<<get_name()<<"::on_inert_value_changed temp_value="<<val<< endl;
	temp_value = val;
	queue_draw();
}
// -------------------------------------------------------------------------
void UScrollValue::allocate_changed(Gtk::Allocation& alloc)
{
	width = action_area->get_allocation().get_width();
	height = action_area->get_allocation().get_height();
	
	if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
	{
		long h = lround(((float)width)/2);
//		cout<<get_name()<<"::allocate_changed width="<<width<<" height="<<height<<" h="<<h<<endl;
		rightTopPlaceholder->set_size_request(-1,h);
		rightBottomPlaceholder->set_size_request(-1,h);
		leftTopPlaceholder->set_size_request(-1,h);
		leftBottomPlaceholder->set_size_request(-1,h);
		topRightPlaceholder->set_size_request(0,0);
		topLeftPlaceholder->set_size_request(0,0);
		bottomRightPlaceholder->set_size_request(0,0);
		bottomLeftPlaceholder->set_size_request(0,0);
	}
	else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
	{
		long w = lround(((float)height)/2);
//		cout<<get_name()<<"::allocate_changed width="<<width<<" height="<<height<<" w="<<w<<endl;
		rightTopPlaceholder->set_size_request(0,0);
		rightBottomPlaceholder->set_size_request(0,0);
		leftTopPlaceholder->set_size_request(0,0);
		leftBottomPlaceholder->set_size_request(0,0);
		topRightPlaceholder->set_size_request(w,-1);
		topLeftPlaceholder->set_size_request(w,-1);
		bottomRightPlaceholder->set_size_request(w,-1);
		bottomLeftPlaceholder->set_size_request(w,-1);
	}
	queue_draw();
}
// -------------------------------------------------------------------------
bool UScrollValue::on_indicator_event(GdkEvent* ev)
{
//	if(ev->type!=GDK_EXPOSE)
//		cout<<get_name()<<"::on_indicator_event event->type="<<ev->type<< endl;

    if(ev->type==GDK_MAP)
    {
//		cout<<get_name()<<"::on_indicator_event event->type="<<ev->type<< endl;
        is_show = true;
//		on_value_changed();
    }
    else if(ev->type==GDK_UNMAP)
        is_show = false;
    return false;
}
// -------------------------------------------------------------------------
bool UScrollValue::action_area_event(GdkEvent* ev)
{
	if(prop_passiveMode)
		return false;
	
	GdkEventButton event_btn = ev->button;
	
	if(event_btn.type == GDK_BUTTON_PRESS)
	{
		action_start=true;
		calculation_value_in_push_point();
		queue_draw();
	}
	else if( ev->type == GDK_MOTION_NOTIFY && action_start)
	{
		calculation_value_in_push_point();
		queue_draw();
	}
	else if( ev->type == GDK_BUTTON_RELEASE && action_start)
	{
		action_start=false;
		float val = calculation_value_in_push_point();
// 		cout<<get_name()<<"::action_area_event() val="<<val<< endl;
		signal.emit(val);
		sensor_ai_.save_value(val);
	}
	return false;
}
// -------------------------------------------------------------------------
float UScrollValue::calculation_value_in_push_point()
{
	Glib::RefPtr<Gdk::Window> win = action_area->get_window();
	if(!win)
		return 0;
	
	int p_x,p_y;
	Gdk::ModifierType mtype;
	win->get_pointer(p_x,p_y,mtype);
	
	Gtk::Allocation alloc = action_area->get_allocation();
	//calculation value X & Y in push point
	long h = lround(((float)width)/2);
	long w = lround(((float)height)/2);
	float x_value = p_x - alloc.get_x() - w;
	float y_value = p_y - alloc.get_y() - h;

	if(x_value < 0)
		x_value = 0;
	else if(x_value > width - 2*w)
		x_value = width - 2*w;
	if(y_value < 0)
		y_value = 0;
	else if(y_value > height - 2*h)
		y_value = height - 2*h;

	double prec_value = 0;
	if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
	{
		float scale_h = rightScaleBox->get_height();
		float value_h = (float)(scale_h - y_value)/scale_h * fabs(get_prop_Max() - get_prop_Min()) + get_prop_Min();
//		cout<<get_name()<<"::calculation_value_in_push_point scale_h="<<scale_h<<" y_value="<<scale_h - y_value<<" value_h="<<value_h<<" fvalue_h="<<value_h*pow10(get_property_precision())<< endl;
		prec_value = value_h;//*pow10(get_property_precision());
	}
	else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
	{
		float scale_w = topScaleBox->get_width();
		float value_w = x_value/scale_w *  fabs(get_prop_Max() - get_prop_Min()) + get_prop_Min();
//		cout<<get_name()<<"::calculation_value_in_push_point scale_w="<<scale_w<<" x_value="<<x_value<<" value_w="<<value_w<<" fvalue_w="<<value_w*pow10(get_property_precision())<< endl;
		prec_value = value_w;//*pow10(get_property_precision());
	}
//	set_property_value(prec_value);
	if(prop_use_inert)
		temp_value = prec_value;
	else
		value = prec_value;
	return calibrate.get_rvalue(prec_value) * pow10(get_property_precision());
}
// -------------------------------------------------------------------------
bool UScrollValue::on_expose_event(GdkEventExpose* event)
{
	bool rv = UEventBox::on_expose_event(event);
	#if GTK_VERSION_GE(2,20)
    if(!get_mapped())
#else
    if(!is_show)
#endif
        return false;

    Cairo::RefPtr<Cairo::Context> cr = action_area->get_window()->create_cairo_context();
	Gtk::Allocation alloc = action_area->get_allocation();
    Gdk::Cairo::rectangle(cr,alloc);
    cr->clip();
    cr->translate(alloc.get_x(), alloc.get_y());

	//рисуем задник
	draw_bg(cr,alloc);
	//рисуем ползунок
	draw_scroll(cr,alloc);
	//рисуем подпись текущего значения
	if(prop_showValueLabel)
		draw_value(cr,alloc);
	
	if ( !connected_ && get_property_disconnect_effect() == 1)
    {
		action_area->set_property_disconnect_effect(1);
        alloc.set_x(0);
        alloc.set_y(0);
        UVoid::draw_disconnect_effect_1(cr, alloc);
    }

    return false;
}
// -------------------------------------------------------------------------
void UScrollValue::draw_bg(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	double k_rounding = prop_rounding.get_value();
	k_rounding = k_rounding>1? 1:(k_rounding<0? 0:k_rounding);
	
	cr->save();
	if(prop_useBGImage)
	{
/*
		if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
		{
			long h = lround(((float)width)/2);
			
			Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(get_prop_BGImagePath(), width, rightScaleBox->get_height());
			Gdk::Cairo::set_source_pixbuf(cr,image, 0, h);
			Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(get_prop_BGImagePath(), width, rightScaleBox->get_height());
			Gdk::Cairo::set_source_pixbuf(cr,image, 0, h);
		}
		else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
		{
			long w = lround(((float)height)/2);
			
			Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(get_prop_BGImagePath(), topScaleBox->get_width(), height);
			Gdk::Cairo::set_source_pixbuf(cr,image, w, 0);
		}
*/
		Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(get_prop_BGImagePath(), width, height);
		Gdk::Cairo::set_source_pixbuf(cr,image, 0, 0);
		
		cr->paint();
	}
	else
	{
		cr->begin_new_sub_path();
		cr->set_line_width(get_prop_bgStrokeWidth());
		cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		float stroke_offset = ((float)get_prop_bgStrokeWidth())/2;
		
		if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
		{
			float h = ((float)width)/2 * k_rounding;
			float curve_h = 1.3334*(h - stroke_offset);
			
			if(k_rounding==1.0)
			{
				cr->arc( h, h, h - stroke_offset, M_PI, 0 );
				cr->arc( h, height - h, h - stroke_offset, 0, M_PI );
			}
			else
			{
				cr->move_to(stroke_offset, h);
				cr->curve_to(stroke_offset, h - curve_h, width - stroke_offset, h - curve_h, width - stroke_offset, h);
				cr->line_to(width - stroke_offset,height - h);
				cr->curve_to(width - stroke_offset, height - h + curve_h, stroke_offset, height - h + curve_h, stroke_offset, height - h);
			}
		}
		else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
		{
			float w = ((float)height)/2 * k_rounding;
			float curve_w = 1.3334*(w - stroke_offset);
			
			if(k_rounding==1.0)
			{
				cr->arc( w, w, w - stroke_offset, M_PI/2, -M_PI/2 );
				cr->arc( width - w, w, w - stroke_offset, -M_PI/2, M_PI/2 );
			}
			else
			{
				cr->move_to(w, height - stroke_offset);
				cr->curve_to(w - curve_w, height - stroke_offset, w - curve_w, stroke_offset, w, stroke_offset);
				cr->line_to(width - w,stroke_offset);
				cr->curve_to(width - w + curve_w, stroke_offset, width - w + curve_w, height - stroke_offset, width - w, height - stroke_offset);
			}
		}
		
		cr->close_path();
		Cairo::Path *bg = cr->copy_path();
		
		if(prop_bgUseGradientColor)
		{
			Cairo::RefPtr<Cairo::LinearGradient> gradient;
			
			if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
				gradient = Cairo::LinearGradient::create(0, 0, width, 0);
			else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
				gradient = Cairo::LinearGradient::create(0, 0, 0, height);
			
			gradient->add_color_stop_rgba(0, get_prop_bgFillColor2().get_red_p(), get_prop_bgFillColor2().get_green_p(), get_prop_bgFillColor2().get_blue_p(),1.0);
			gradient->add_color_stop_rgba(1.0, get_prop_bgFillColor1().get_red_p(), get_prop_bgFillColor1().get_green_p(), get_prop_bgFillColor1().get_blue_p(),1.0);
			cr->set_source(gradient);
		}
		else
			Gdk::Cairo::set_source_color(cr, get_prop_bgFillColor1());
		cr->fill();
		
		cr->append_path(*bg);
		if(prop_bgStrokeUseGradientColor)
		{
			Cairo::RefPtr<Cairo::LinearGradient> gradient;
			
			if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
				gradient = Cairo::LinearGradient::create(0, 0, width, 0);
			else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
				gradient = Cairo::LinearGradient::create(0, 0, 0, height);
			
			gradient->add_color_stop_rgba(0, get_prop_bgFillColor1().get_red_p(), get_prop_bgFillColor1().get_green_p(), get_prop_bgFillColor1().get_blue_p(),1.0);
			gradient->add_color_stop_rgba(1.0, get_prop_bgFillColor2().get_red_p(), get_prop_bgFillColor2().get_green_p(), get_prop_bgFillColor2().get_blue_p(),1.0);
			cr->set_source(gradient);
		}
		else
			Gdk::Cairo::set_source_color(cr, get_prop_bgFillColor2());
		cr->stroke();
	}
	
	cr->restore();
}
// -------------------------------------------------------------------------
void UScrollValue::draw_scroll(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	double cur_value;
	if(prop_use_inert)
		cur_value = temp_value;
	else
		cur_value = value;
	
	if(cur_value>get_prop_Max())
		cur_value=get_prop_Max();
	if(cur_value<get_prop_Min())
		cur_value=get_prop_Min();
	cur_value = uquant.get_value(get_prop_Min(),get_prop_Max(),cur_value);
	
	double k_rounding = prop_rounding.get_value();
	k_rounding = k_rounding>1? 1:(k_rounding<0? 0:k_rounding);
	float stroke_offset = 0.5*get_prop_scrollStrokeWidth() + get_prop_bgStrokeWidth();
//	cout<<get_name()<<"::draw_scroll() fvalue="<<cur_value<<" cur_value="<<get_property_value()<< endl;
	float half_w, half_h;
	float scale_w, scale_h;
	float value_w, value_h;
	
	cr->save();

	if(prop_useScrollImage)
	{
		if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
		{
			scale_h = rightScaleBox->get_height();
			value_h = (scale_h/fabs(get_prop_Max() - get_prop_Min()) * fabs(get_prop_Max() - cur_value));

			Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(get_prop_ScrollImagePath(), width, width);
			Gdk::Cairo::set_source_pixbuf(cr,image, 0, value_h);
		}
		else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
		{
			scale_w = topScaleBox->get_width();
			value_w = (scale_w/fabs(get_prop_Max() - get_prop_Min()) * fabs(cur_value - get_prop_Min()));
			
			Glib::RefPtr<Gdk::Pixbuf> image = get_pixbuf_from_cache(get_prop_ScrollImagePath(), height, height);
			Gdk::Cairo::set_source_pixbuf(cr,image, value_w, 0);
		}
		cr->paint();
	}
	else
	{
		cr->begin_new_sub_path();
		cr->set_line_width(get_prop_scrollStrokeWidth());
		cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		
		if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
		{
			half_w = ((float)width)/2;
			scale_h = rightScaleBox->get_height();
			value_h = half_w + (scale_h/fabs(get_prop_Max() - get_prop_Min()) * fabs(get_prop_Max() - cur_value));
			float h = half_w * k_rounding;
			float curve_h = 1.3334*(h - 0.5*get_prop_scrollStrokeWidth());
			float offset_h = half_w - h - get_prop_bgStrokeWidth() - 0.5*get_prop_scrollStrokeWidth();
//			cout<<get_name()<<"::draw_scroll scale_h="<<scale_h<<" value_h="<<value_h<<" fvalue_h="<<value_h*pow10(get_property_precision())<< endl;
			
			if(k_rounding==1.0)
			{
				cr->arc( ((float)width)/2, value_h,((float)width)/2 - stroke_offset, 0, 2*M_PI );
			}
			else
			{
				cr->move_to(stroke_offset, value_h - offset_h);
				cr->curve_to(stroke_offset, value_h - offset_h - curve_h, width - stroke_offset, value_h - offset_h - curve_h, width - stroke_offset, value_h - offset_h);
				cr->line_to(width - stroke_offset,value_h + offset_h);
				cr->curve_to(width - stroke_offset, value_h + offset_h + curve_h, stroke_offset, value_h + offset_h + curve_h, stroke_offset,  value_h + offset_h);
			}
		}
		else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
		{
			half_h = ((float)height)/2;
			scale_w = topScaleBox->get_width();
			value_w = half_h + (scale_w/fabs(get_prop_Max() - get_prop_Min()) * fabs(cur_value - get_prop_Min()));
			float w = half_h * k_rounding;
			float curve_w = 1.3334*(w - 0.5*get_prop_scrollStrokeWidth());
			float offset_w = half_h - w - get_prop_bgStrokeWidth() - 0.5*get_prop_scrollStrokeWidth();
			
			if(k_rounding==1.0)
			{
				cr->arc( value_w, ((float)height)/2,((float)height)/2 - stroke_offset, 0, 2*M_PI );
			}
			else
			{
				cr->move_to(value_w - offset_w, height - stroke_offset);
				cr->curve_to(value_w - offset_w - curve_w, height - stroke_offset, value_w - offset_w - curve_w, stroke_offset, value_w - offset_w,  stroke_offset);
				cr->line_to(value_w + offset_w, stroke_offset);
				cr->curve_to(value_w + offset_w + curve_w, stroke_offset, value_w + offset_w + curve_w, height - stroke_offset, value_w + offset_w,  height - stroke_offset);
			}
		}
		
		cr->close_path();
		Cairo::Path *scroll = cr->copy_path();
		
		if(prop_scrollUseGradientColor)
		{
			Cairo::RefPtr<Cairo::LinearGradient> gradient;
			
			if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
				gradient = Cairo::LinearGradient::create(get_prop_bgStrokeWidth(), 0, width - get_prop_bgStrokeWidth(), 0);
			else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
				gradient = Cairo::LinearGradient::create(0, get_prop_bgStrokeWidth(), 0, height - get_prop_bgStrokeWidth());
			
			gradient->add_color_stop_rgba(0, get_prop_scrollFillColor2().get_red_p(), get_prop_scrollFillColor2().get_green_p(), get_prop_scrollFillColor2().get_blue_p(),1.0);
			gradient->add_color_stop_rgba(1.0, get_prop_scrollFillColor1().get_red_p(), get_prop_scrollFillColor1().get_green_p(), get_prop_scrollFillColor1().get_blue_p(),1.0);
			cr->set_source(gradient);
		}
		else
			Gdk::Cairo::set_source_color(cr, get_prop_scrollFillColor1());
		cr->fill();
		
		cr->append_path(*scroll);
		if(prop_scrollStrokeUseGradientColor)
		{
			Cairo::RefPtr<Cairo::LinearGradient> gradient;
			
			if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
				gradient = Cairo::LinearGradient::create(get_prop_bgStrokeWidth(), 0, width - get_prop_bgStrokeWidth(), 0);
			else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
				gradient = Cairo::LinearGradient::create(0, get_prop_bgStrokeWidth(), 0, height - get_prop_bgStrokeWidth());
			
			gradient->add_color_stop_rgba(0, get_prop_scrollFillColor1().get_red_p(), get_prop_scrollFillColor1().get_green_p(), get_prop_scrollFillColor1().get_blue_p(),1.0);
			gradient->add_color_stop_rgba(1.0, get_prop_scrollFillColor2().get_red_p(), get_prop_scrollFillColor2().get_green_p(), get_prop_scrollFillColor2().get_blue_p(),1.0);
			cr->set_source(gradient);
		}
		else
			Gdk::Cairo::set_source_color(cr, get_prop_scrollFillColor2());
		cr->stroke();
		
		if(prop_useScaleLine)
		{
			Gdk::Cairo::set_source_color(cr, get_prop_scaleColor());
			if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
			{
				cr->move_to(3*stroke_offset, value_h);
				cr->line_to(width - 3*stroke_offset, value_h);
			}
			else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
			{
				cr->move_to(value_w, 3*stroke_offset);
				cr->line_to(value_w, height - 3*stroke_offset);
			}
			cr->stroke();
		}
	}
	
	cr->restore();
}
// -------------------------------------------------------------------------
void UScrollValue::draw_value(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc)
{
	double cur_value;
	if(prop_use_inert)
		cur_value = temp_value;
	else
		cur_value = value;
	cur_value = uquant.get_value(get_prop_Min(),get_prop_Max(),cur_value);
	
	int prec = property_precision.get_value();
	std::ostringstream label_text;
	if(prec>0)
		label_text<<(long)cur_value<<","<<setw(prec)<<setfill('0')<<(long)(fabs((cur_value-(long)cur_value)*pow10(prec)));
	else
		label_text<<(long)cur_value;
	label_text<<get_prop_valueLabelUnits();
	
	int layw,layh;
	value_layout->set_text(label_text.str());
	value_layout->context_changed();
	value_layout->get_pixel_size(layw,layh);

	float half_w = ((float)width)/2;
	float half_h = ((float)height)/2;
	float scale_w, scale_h;
	float value_w, value_h;

	cr->save();
	if(get_prop_orientation()==Gtk::ORIENTATION_VERTICAL)
	{
		scale_h = rightScaleBox->get_height();
		value_h = half_w + (scale_h/fabs(get_prop_Max() - get_prop_Min()) * fabs(get_prop_Max() - cur_value));

		if(get_prop_valueJustify() == CENTER)
			cr->move_to(half_w - ((float)layw)/2, value_h - ((float)layh)/2);
		else if(get_prop_valueJustify() == TOP)
			cr->move_to(half_w - ((float)layw)/2, value_h - half_w);
		else if(get_prop_valueJustify() == BOTTOM)
			cr->move_to(half_w - ((float)layw)/2, value_h + half_w - layh);
		else if(get_prop_valueJustify() == LEFT)
			cr->move_to(0, value_h - ((float)layh)/2);
		else if(get_prop_valueJustify() == RIGHT)
			cr->move_to(width - layw, value_h - ((float)layh)/2);
	}
	else if(get_prop_orientation()==Gtk::ORIENTATION_HORIZONTAL)
	{
		scale_w = topScaleBox->get_width();
		value_w = half_h + (scale_w/fabs(get_prop_Max() - get_prop_Min()) * fabs(cur_value - get_prop_Min()));

		if(get_prop_valueJustify() == CENTER)
			cr->move_to(value_w - ((float)layw)/2, half_h - ((float)layh)/2);
		else if(get_prop_valueJustify() == TOP)
			cr->move_to(value_w - ((float)layw)/2, 0);
		else if(get_prop_valueJustify() == BOTTOM)
			cr->move_to(value_w - ((float)layw)/2, height - layh);
		else if(get_prop_valueJustify() == LEFT)
			cr->move_to(value_w - half_h, half_h - ((float)layh)/2);
		else if(get_prop_valueJustify() == RIGHT)
			cr->move_to(value_w + half_h - layw, half_h - ((float)layh)/2);
	}
	Gdk::Cairo::set_source_color(cr, prop_valueLabelColor.get_value());
	value_layout->add_to_cairo_context(cr);
	cr->fill();
	cr->restore();
}
// -------------------------------------------------------------------------
void UScrollValue::set_sensor_ai(const ObjectId sens_id, const ObjectId node_id)
{
    /* Set new sens_id */
    sensor_ai_.set_sens_id(sens_id);
}
// -------------------------------------------------------------------------
void UScrollValue::set_precisions(const int precisions)
{
    set_property_precision(precisions);
}
// -------------------------------------------------------------------------
void
UScrollValue::set_connector(const ConnectorRef& connector) throw()
{
    if (connector == get_connector())
        return;

    UVoid::set_connector(connector);

    sensor_ai_.set_connector(connector);

    sensor_connection_.disconnect();

    if (get_connector() == true) {
        sigc::slot<void, ObjectId, ObjectId, long> process_sensor_slot =
                sigc::mem_fun(this, &UScrollValue::process_sensor);
        sensor_connection_ = get_connector()->signals().connect_value_changed(
                process_sensor_slot,
                sensor_ai_.get_sens_id(),
                sensor_ai_.get_node_id());
        on_connect();
    }
}
// -------------------------------------------------------------------------
void
UScrollValue::on_connect() throw()
{
    UVoid::on_connect();

    float new_value = get_connector()->
        get_analog_value(sensor_ai_.get_sens_id(), sensor_ai_.get_node_id());

    process_sensor(sensor_ai_.get_sens_id(), sensor_ai_.get_node_id(), new_value);

    if(get_property_disconnect_effect() > 0)
        queue_draw();
}
// -------------------------------------------------------------------------
void
UScrollValue::on_disconnect() throw()
{
    UVoid::on_connect();
    UVoid::on_disconnect();

    if(get_property_disconnect_effect() > 0)
        queue_draw();
}
// -------------------------------------------------------------------------
void
UScrollValue::process_sensor(ObjectId id, ObjectId node, float val)
{
//	cout<<get_name()<<"::process_sensor id="<<id<<" node="<<node<<" val="<<val<<endl;
    set_property_value(val);
}
// -------------------------------------------------------------------------
