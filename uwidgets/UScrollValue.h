#ifndef _USCROLLVALUE_H
#define _USCROLLVALUE_H
// -------------------------------------------------------------------------
#include <uwidgets/UEventBox.h>
#include <unordered_map>
#include <map>
#include <gtkmm.h>
#include <TransProperty.h>
#include "UDefaultFunctions.h"
#include "USignals.h"
#include "UScale.h"
#include "UInertia.h"
#include "UQuantization.h"
#include "UCalibrate.h"
#include "plugins.h"
#include "global_macros.h"
// -------------------------------------------------------------------------
/*!
 * \brief Класс цифрового индикатора.
 * \par
 * Класс предназначен для реализации виджета "цифровой индикатор".
*/
class UScrollValue : public UEventBox
{
public:
	UScrollValue();
	explicit UScrollValue(GtkmmBaseType::BaseObjectType* gobject);
	~UScrollValue() {}

	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void set_connector(const ConnectorRef& connector) throw();

	/* Set methods for widget properties */
	/*! задать аналоговый датчик для виджета.
	    \param sens_id id датчика.
	    \param node_id id узла датчика.
	*/
	void set_sensor_ai(const UniWidgetsTypes::ObjectId sens_id, const UniWidgetsTypes::ObjectId node_id);
	/*! задать точность после запятой отображаемого значения.
	    \param precisions заданная точность значения.
	*/
	void set_precisions(const int precisions);
	/*! задать количество цифр используемых как ширина поля отображения.
	    \param digits заданная ширина поля отображения.
	*/
	void set_digits(const int digits);
	/*! задать символ для заполнения неиспользуемых разрядов.
	    \param digits заданный заполнитель.
	*/
	void set_fill_digits(const int digits);
	/*! задать цвет текста.
	    \param font_color заданный цвет текста.
	*/
	void set_font_color(const long font_color);

    inline void set_value(double value) { set_property_value(value); }

    enum JusifyType
    {
		CENTER=0
		,TOP
		,BOTTOM
		,LEFT
		,RIGHT
	};

	typedef sigc::slot<void, double> scrollValueChangedSlot; // double - радиальное значение
	typedef sigc::signal<void, double> scrollValueChangedSignal;
	
	virtual inline scrollValueChangedSignal& signal_scroll_value_change(){ return signal; }	/* */
	
protected:
	virtual void on_realize();
	virtual bool on_expose_event(GdkEventExpose*);
	virtual void allocate_changed(Gtk::Allocation& alloc);
	virtual bool on_indicator_event(GdkEvent* ev);
	virtual bool action_area_event(GdkEvent* ev);
	
	virtual void draw_bg(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_scroll(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	virtual void draw_value(Cairo::RefPtr<Cairo::Context> &cr,Gtk::Allocation& alloc);
	
	scrollValueChangedSignal signal;
	scrollValueChangedSlot slot;
	
	bool is_show;
	bool action_start;
	int width;
	int height;

	double last_value;
	double value;
	double temp_value;
	sigc::connection inert_signal;
	virtual void on_inert_value_changed(double val, bool inert_is_stoped);
	
	Gtk::VBox *topScaleBox;
	Gtk::VBox *bottomScaleBox;
	Gtk::HBox *leftScaleBox;
	Gtk::HBox *rightScaleBox;
	Gtk::EventBox *rightTopPlaceholder;
	Gtk::EventBox *rightBottomPlaceholder;
	Gtk::EventBox *leftTopPlaceholder;
	Gtk::EventBox *leftBottomPlaceholder;
	Gtk::EventBox *topRightPlaceholder;
	Gtk::EventBox *topLeftPlaceholder;
	Gtk::EventBox *bottomRightPlaceholder;
	Gtk::EventBox *bottomLeftPlaceholder;
	HScale *topScale;
	HScale *bottomScale;
	VScale *leftScale;
	VScale *rightScale;
	HScaleLabels* topLabel;
	HScaleLabels* bottomLabel;
	
	UEventBox *action_area;
	Gtk::Table *table;

	Glib::RefPtr<Pango::Layout> value_layout;
	
private:
	void ctor();
	void process_sensor(UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, float);

	float calculation_value_in_push_point();
	void on_value_changed();

	void on_scales_enable_changed();
	void on_font_changed();
	void on_font_color_changed();
	void on_scale_color_changed();
	void on_scales_range_changed();
	void on_value_font_changed();
	
	sigc::connection sensor_connection_;

	SensorProp sensor_ai_;
	UCalibrate calibrate;
	ADD_PROPERTY(property_value, double)
	ADD_PROPERTY(property_precision, int)

	UQuantization uquant;

	ADD_PROPERTY( prop_use_inert, bool )						/*!< свойство: использовать инерцию для ползунка*/
	UInertia inert;

	ADD_PROPERTY( prop_passiveMode, bool )						/*!< свойство: пассивность ползунка (только отображает текущее значение)*/
	
	ADD_PROPERTY( prop_scaleColor, Gdk::Color )					/*!< свойство: цвет шкалы*/
	ADD_PROPERTY( prop_scaleLabelColor, Gdk::Color )			/*!< свойство: цвет подписей шкалы*/
	ADD_PROPERTY( prop_scaleLabelFont, Glib::ustring )			/*!< свойство: шрифт подписей шкалы*/
	ADD_PROPERTY( prop_scaleLabelFontSize, int )				/*!< свойство: размер шрифта подписей шкалы*/
	ADD_PROPERTY( prop_orientation, Gtk::Orientation )			/*!< свойство: ориинтация виджета горизонтальная/вертикальная */
	ADD_PROPERTY( prop_topScale, bool )							/*!< свойство: вкл/выкл верхнюю шкалу, если ориентация горизонтальная*/
	ADD_PROPERTY( prop_bottomScale, bool )						/*!< свойство: вкл/выкл нижнюю шкалу, если ориентация горизонтальная*/
	ADD_PROPERTY( prop_leftScale, bool )						/*!< свойство: вкл/выкл левую шкалу, если ориентация вертикальная*/
	ADD_PROPERTY( prop_rightScale, bool )						/*!< свойство: вкл/выкл правую шкалу, если ориентация вертикальная*/
	ADD_PROPERTY( prop_Min, float )								/*!< свойство: значение соответствующее началу шкалы*/
	ADD_PROPERTY( prop_Max, float )								/*!< свойство: значение соответствующее концу шкалы*/
	ADD_PROPERTY( prop_maxMinor, int )							/*!< свойство: */
	ADD_PROPERTY( prop_maxMajor, int )							/*!< свойство: */

	ADD_PROPERTY( prop_useBGImage, bool )						/*!< свойство: использовать картинку для задника*/
	ADD_PROPERTY( prop_BGImagePath, Glib::ustring)				/*!< свойство: путь к файлу картинки задника*/
	ADD_PROPERTY( prop_useScrollImage, bool )					/*!< свойство: использовать картинку для ползунка*/
	ADD_PROPERTY( prop_ScrollImagePath, Glib::ustring)			/*!< свойство: путь к файлу картинки ползунка*/

	ADD_PROPERTY( prop_bgUseGradientColor, bool )				/*!< свойство: вкл/выкл градиента для цвета задника*/
	ADD_PROPERTY( prop_bgStrokeUseGradientColor, bool )			/*!< свойство: вкл/выкл градиента для цвета обводки задника*/
	ADD_PROPERTY( prop_bgFillColor1, Gdk::Color )				/*!< свойство: 1й цвет заливки задника ползунка*/
	ADD_PROPERTY( prop_bgFillColor2, Gdk::Color )				/*!< свойство: 2й цвет заливки задника ползунка*/
	ADD_PROPERTY( prop_scrollUseGradientColor, bool )			/*!< свойство: вкл/выкл градиента для цвета ползунка*/
	ADD_PROPERTY( prop_scrollStrokeUseGradientColor, bool )		/*!< свойство: вкл/выкл градиента для цвета обводки ползунка*/
	ADD_PROPERTY( prop_scrollFillColor1, Gdk::Color )			/*!< свойство: 1й цвет заливки ползунка*/
	ADD_PROPERTY( prop_scrollFillColor2, Gdk::Color )			/*!< свойство: 2й цвет заливки ползунка*/
	ADD_PROPERTY( prop_rounding, double )						/*!< свойство: коэфициениет скругления углов задника и ползунка*/
	ADD_PROPERTY( prop_bgStrokeWidth, int )						/*!< свойство: ширина обводки задника*/
	ADD_PROPERTY( prop_scrollStrokeWidth, int )					/*!< свойство: ширина обводки ползунка*/

	ADD_PROPERTY( prop_useScaleLine, bool )						/*!< свойство: вкл/выкл центральную линию шкалы на ползунке*/
	ADD_PROPERTY( prop_showValueLabel, bool )					/*!< свойство: показывать подпись текущего значения*/
	ADD_PROPERTY( prop_valueJustify, JusifyType )				/*!< свойство: выравнивание подписи текущего значения */
	ADD_PROPERTY( prop_valueLabelColor, Gdk::Color )			/*!< свойство: цвет подписи текущего значения*/
	ADD_PROPERTY( prop_valueLabelFont, Glib::ustring )			/*!< свойство: шрифт подписи текущего значения*/
	ADD_PROPERTY( prop_valueLabelFontSize, int )				/*!< свойство: размер шрифта подписи текущего значения*/
	ADD_PROPERTY( prop_valueLabelUnits, Glib::ustring )			/*!< свойство: еденицы измерения подписи текущего значения*/
};

namespace Glib
{
	template <>
	class Value<UScrollValue::JusifyType> : public Value_Enum<UScrollValue::JusifyType>
	{
	public:
		static GType value_type() G_GNUC_CONST;
	};
} // namespace Glib

#endif
