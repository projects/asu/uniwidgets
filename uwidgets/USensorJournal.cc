#include <iostream>
#include <sstream>
#include <gdkmm.h>
#include <types.h>
#include "ports.h"
#include "USensorJournal.h"
#include "USignals.h"
#include "ConfirmSignal.h"
#define DEBUG
// -------------------------------------------------------------------------
// using namespace std;
using namespace std;
using namespace UniWidgetsTypes;
using namespace UniWidgetsTypes;
using UMessages::MessageId;
using Glib::ustring;
// -------------------------------------------------------------------------
struct msgItem
{
  ObjectId node_id;
  Glib::ustring msg;
  int character;
};
// -------------------------------------------------------------------------
#define USENSORJOURNAL_INIT_PROPERTIES() \
   property_id_title          (*this, "id-title"   ,  "Номер"   ) \
  ,property_name_title        (*this, "name-title"   ,  "Название"   ) \
  ,property_value_title       (*this, "value-title",  "Значение") \
  ,property_time_title        (*this, "time-title" ,  "Время"   ) \
  ,property_text_title        (*this, "text-title" ,  "Текст"   ) \
  ,property_aps_title        (*this, "aps-title" ,  "АПС"   ) \
  ,property_info_color        (*this, "info-color",     Gdk::Color("white")  ) \
  ,property_warn_color        (*this, "warn-color",     Gdk::Color("yellow") ) \
  ,property_alarm_color       (*this, "alarm-color",    Gdk::Color("red") ) \
  ,property_attention_color   (*this, "attention-color",Gdk::Color("yellow") ) \
  ,property_id_width          (*this, "id-width"   ,  10        ) \
  ,property_name_width        (*this, "name-width"   ,  10        ) \
  ,property_value_width       (*this, "value-width",  5        ) \
  ,property_time_width        (*this, "time-width" ,  15        ) \
  ,property_aps_width       (*this, "aps-width",  10        ) \
  ,property_text_width        (*this, "text-width" ,  40       ) \
  ,property_local_mode   (*this, "local-mode", false )
// -------------------------------------------------------------------------
void USensorJournal::constructor()
{
  GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
  goclass->set_property = &custom_set_property_callback<USensorJournal>;

  scrolled_window_.add(tree_view_);
  scrolled_window_.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

  vbox.pack_start(browse,Gtk::PACK_SHRINK);
  vbox.pack_start(parameters,Gtk::PACK_SHRINK);

  chk_id.add_label("Поиск по id");
  chk_name.add_label("Поиск по name");
  chk_text.add_label("Поиск по text");
  chk_aps.add_label("Поиск по АПС");
  chk_reglet.add_label("Учитывать регистр");
  parameters.attach(chk_id,0,1,1,2);
  parameters.attach(chk_name,1,2,1,2);
  parameters.attach(chk_text,2,3,1,2);
  parameters.attach(chk_aps,3,4,1,2);
  parameters.attach(chk_reglet,0,2,0,1);
  
  chk_di.add_label("DI");
  chk_do.add_label("DO");
  chk_ai.add_label("AI");
  chk_ao.add_label("AO");
  parameters.attach(chk_di,0,1,2,3);
  parameters.attach(chk_do,1,2,2,3);
  parameters.attach(chk_ai,2,3,2,3);
  parameters.attach(chk_ao,3,4,2,3);
  parameters.attach(progress,0,4,3,4);
  
  progress.set_size_request( -1,5 );
  progress.show();
  chk_di.set_active(true);
  chk_do.set_active(true);
  chk_ai.set_active(true);
  chk_ao.set_active(true);
  chk_di.signal_toggled().connect(sigc::mem_fun(*this,
                                  &USensorJournal::on_change_browse) );
  chk_do.signal_toggled().connect(sigc::mem_fun(*this,
                        &USensorJournal::on_change_browse) );
  chk_ai.signal_toggled().connect(sigc::mem_fun(*this,
                        &USensorJournal::on_change_browse) );
  chk_ao.signal_toggled().connect(sigc::mem_fun(*this,
                        &USensorJournal::on_change_browse) );
  
  chk_id.set_active(true);
  chk_name.set_active(true);
  chk_text.set_active(true);
  chk_aps.set_active(true);
//   vbox.pack_start(scrolled_window_,Gtk::PACK_EXPAND_WIDGET);
  
  scrolled_window_log_.add(tree_view_log_);
  scrolled_window_log_.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  
  /* Добавляем оба окна со списками к VPaneld, чтобы можно было изменять размеры относительно друг друга*/
  vbox.pack_start(vpan,Gtk::PACK_EXPAND_WIDGET);
  vpan.add1(scrolled_window_);
  vpan.add2(scrolled_window_log_);
//   vbox.pack_start(scrolled_window_log_,Gtk::PACK_EXPAND_WIDGET);
  
  add(vbox);

  tree_model_ref_ = Gtk::TreeStore::create(columns_);
  tree_model_filter_ = Gtk::TreeModelFilter::create(tree_model_ref_);
//   tree_model_filter_->set_modify_func(columns_,sigc::mem_fun(*this,&USensorJournal::on_modify));
  tree_model_filter_->set_visible_func(sigc::mem_fun(*this,&USensorJournal::on_rows_visible));
  tree_view_.set_model(tree_model_filter_);
  tree_view_.get_selection()->set_mode(Gtk::SELECTION_SINGLE);
//   tree_model_ref_->signal_row_changed().connect(sigc::mem_fun(this,&USensorJournal::on_row_changed));
//   tree_model_filter_->signal_row_changed().connect(sigc::mem_fun(this,&USensorJournal::on_row_filtered_changed));
    //Значение
  {
    /*TODO*/
    Gtk::Adjustment adj(0.0, -10000.0, 10000.0, 1.0,  2.0, 0);
    
    Gtk::CellRendererSpin* pRenderer = Gtk::manage( new Gtk::CellRendererSpin() );
    pRenderer->property_adjustment() = &adj;
    pRenderer->property_editable() = true;
    tree_view_.append_column("Value", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_.get_column(0);
//     pRenderer->property_font() = "Liberation Sans 14";
    pColumn->add_attribute(pRenderer->property_text(), columns_.value_string);
    pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_FIXED);
    pColumn->set_resizable(true);

    pRenderer->signal_edited().connect( sigc::mem_fun(*this,
                                 &USensorJournal::on_my_edited) );

    pRenderer->signal_editing_started().connect(
        sigc::mem_fun(*this,
                       &USensorJournal::on_editing_started) );

//     pColumn->set_alignment(Gtk::ALIGN_LEFT);

//     pColumn->set_cell_data_func(*pRenderer,
//         sigc::mem_fun(*this,
//                        &USensorJournal::treeviewcolumn_validated_on_cell_data) );

    pRenderer->signal_editing_canceled().connect( sigc::mem_fun(*this,
                             &USensorJournal::on_editing_canceled) );
  }

  //Номер
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
    tree_view_.append_column("ID", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_.get_column(1);
    pColumn->set_resizable(true);
    pColumn->add_attribute(pRenderer->property_text(), columns_.id_string);
    pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
//     pRenderer->property_font() = "Liberation Sans 14";
  }

  //Имя
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
    tree_view_.append_column("Name", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_.get_column(2);
    pColumn->set_resizable(true);
    pColumn->add_attribute(pRenderer->property_text(), columns_.name_string);
    pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
//     pRenderer->property_font() = "Liberation Sans 14";
  }

  //Имя
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
    tree_view_.append_column("Node", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_.get_column(3);
    pColumn->set_resizable(true);
    pColumn->add_attribute(pRenderer->property_text(), columns_.node_string);
    pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
//     pRenderer->property_font() = "Liberation Sans 14";
    pColumn->set_title( "Узел" );
  }
  //АПС группа
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
    tree_view_.append_column("APS", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_.get_column(4);
    pColumn->set_resizable(true);
    pColumn->add_attribute(pRenderer->property_text(), columns_.aps_group);
    pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
//     pColumn->set_expand(true);
//     pRenderer->property_font() = "Liberation Sans 14";
//     pRenderer->property_ellipsize() = Pango::ELLIPSIZE_END;
//     pRenderer->property_ellipsize_set() = true;
  }

  //Время
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
    tree_view_.append_column("Time", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_.get_column(5);
    pColumn->set_resizable(true);
    pColumn->add_attribute(pRenderer->property_text(), columns_.time_string);
    pColumn->add_attribute(pRenderer->property_background_gdk(), columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(), columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
//     pRenderer->property_font() = "Liberation Sans 14";
  }

  //Название
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
    tree_view_.append_column("Text", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_.get_column(6);
    pColumn->set_resizable(false);
    pColumn->add_attribute(pRenderer->property_text(), columns_.text_message);
    pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
    pColumn->set_expand(true);
//     pRenderer->property_font() = "Liberation Sans 14";
    pRenderer->property_ellipsize() = Pango::ELLIPSIZE_END;
    pRenderer->property_ellipsize_set() = true;
  }

  /* Создание модели для окна логов */
  tree_model_log_ref_ = Gtk::ListStore::create(columns_);
  tree_view_log_.set_model(tree_model_log_ref_);
  tree_view_log_.get_selection()->set_mode(Gtk::SELECTION_SINGLE);
  tree_view_log_.property_enable_tree_lines()=true;
  tree_view_log_.property_enable_grid_lines()=true;
#if 0
    //Значение
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
    tree_view_log_.append_column("Value", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_log_.get_column(0);
//     pRenderer->property_font() = "Liberation Sans 14";
    pColumn->add_attribute(pRenderer->property_text(), columns_.value_string);
    pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
    pColumn->set_resizable(true);
    
//     pColumn->set_title( "Значение" );
//     pColumn->set_fixed_width( 25 );
  }

  //Время
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
    tree_view_log_.append_column("Time", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_log_.get_column(1);
    pColumn->set_resizable(true);
    pColumn->set_sort_column_id(columns_.time_string);
    pColumn->add_attribute(pRenderer->property_text(), columns_.time_string);
    pColumn->add_attribute(pRenderer->property_background_gdk(), columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(), columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
//     pRenderer->property_font() = "Liberation Sans 14";
//     pColumn->set_title( "Время изменения" );
//     pColumn->set_fixed_width( 25 );
  }

  //Имя
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );
    tree_view_log_.append_column("Name", *pRenderer);
    Gtk::TreeViewColumn* pColumn = tree_view_log_.get_column(2);
    pColumn->set_resizable(true);
    pColumn->add_attribute(pRenderer->property_text(), columns_.name_string);
    pColumn->add_attribute(pRenderer->property_background_gdk(),columns_.bgcolor);
    pColumn->add_attribute(pRenderer->property_foreground_gdk(),columns_.fgcolor);
    pColumn->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
//     pRenderer->property_font() = "Liberation Sans 14";
//     pColumn->set_title( "Название датчика" );
//     pColumn->set_fixed_width( 200 );
    pRenderer->property_ellipsize() = Pango::ELLIPSIZE_END;
    pRenderer->property_ellipsize_set() = true;
  }
#endif
  /* End. Создание окна для модели окна логов */
  on_id_title_changed();
  on_name_title_changed();
  on_value_title_changed();
  on_time_title_changed();
  on_text_title_changed();
  on_aps_title_changed();


  connect_property_changed("id-title", sigc::mem_fun(*this, &USensorJournal::on_id_title_changed) );
  connect_property_changed("name-title", sigc::mem_fun(*this, &USensorJournal::on_name_title_changed) ); 
  connect_property_changed("value-title", sigc::mem_fun(*this, &USensorJournal::on_value_title_changed) );
  connect_property_changed("time-title", sigc::mem_fun(*this, &USensorJournal::on_time_title_changed) );
  connect_property_changed("text-title", sigc::mem_fun(*this, &USensorJournal::on_text_title_changed) );
  connect_property_changed("aps-title", sigc::mem_fun(*this, &USensorJournal::on_aps_title_changed) );
  
  connect_property_changed("id-width", sigc::mem_fun(*this, &USensorJournal::on_id_width_changed) );
  connect_property_changed("name-width", sigc::mem_fun(*this, &USensorJournal::on_name_width_changed) ); 
  connect_property_changed("value-width", sigc::mem_fun(*this, &USensorJournal::on_value_width_changed) );
  connect_property_changed("time-width", sigc::mem_fun(*this, &USensorJournal::on_time_width_changed) );
  connect_property_changed("aps-width", sigc::mem_fun(*this, &USensorJournal::on_aps_width_changed) );

  /* Формирование меню */
  {
    Gtk::MenuItem* item = Gtk::manage(new Gtk::MenuItem("_Watch", true));
    item->signal_activate().connect(
                          sigc::mem_fun(*this, &USensorJournal::on_menu_watch_generic) );
    m_Menu_Popup.append(*item);
    
    item = Gtk::manage(new Gtk::MenuItem("_Mark", true));
    item->signal_activate().connect(
                          sigc::bind(sigc::mem_fun(*this, &USensorJournal::on_menu_mark_generic), true) );
    m_Menu_Popup.append(*item);
    
    item = Gtk::manage(new Gtk::MenuItem("_Unmark", true));
    item->signal_activate().connect(
                          sigc::bind(sigc::mem_fun(*this, &USensorJournal::on_menu_mark_generic),false) );
    m_Menu_Popup.append(*item);
    
    item = Gtk::manage(new Gtk::MenuItem("_UnmarkAll", true));
    item->signal_activate().connect(
                          sigc::mem_fun(*this, &USensorJournal::on_menu_unmarkall_generic) );
    m_Menu_Popup.append(*item);
    
    m_Menu_Popup.accelerate(*this);
    m_Menu_Popup.show_all(); //Show all menu items when the menu pops up
  }
  /* Формирование меню для списка логов */
  {
    Gtk::MenuItem* item = Gtk::manage(new Gtk::MenuItem("_UnWatch", true));
    item->signal_activate().connect(
                          sigc::mem_fun(*this, &USensorJournal::on_menu_unwatch_generic) );
    m_Menu_Popup_Logs.append(*item);
    
    item = Gtk::manage(new Gtk::MenuItem("_Clear", true));
    item->signal_activate().connect(
                          sigc::mem_fun(*this, &USensorJournal::on_menu_clear_all_generic) );
    m_Menu_Popup_Logs.append(*item);
    
    m_Menu_Popup_Logs.accelerate(*this);
    m_Menu_Popup_Logs.show_all(); //Show all menu items when the menu pops up
  }

  tree_view_.set_size_request( -1,400 );
  tree_view_log_.set_size_request( -1,300 );

  tree_view_.set_enable_search(true);
  tree_view_.set_search_column(columns_.name_string);
  /* Сигналы взаимодействия с окном */
  tree_view_.add_events(Gdk::BUTTON_PRESS_MASK);
  tree_view_.signal_row_activated().connect(sigc::mem_fun(*this,
                                  &USensorJournal::on_treeview_row_activated) );
  tree_view_.signal_button_press_event().connect(sigc::mem_fun(*this,
                                  &USensorJournal::on_button_press_event) );
  tree_view_.signal_event().connect(sigc::mem_fun(*this,
                                  &USensorJournal::on_my_event) );
  tree_view_log_.signal_event().connect(sigc::mem_fun(*this,
                          &USensorJournal::on_my_log_event) );

  browse.signal_changed().connect(sigc::mem_fun(*this,&USensorJournal::on_change_browse));
  
//   g_signal_stop_emission_by_name (tree_model_ref_->gobj(), "row_changed");
  if (get_property_disconnect_effect() == 2) {
    scrolled_window_.set_sensitive(false);
    tree_view_.set_sensitive(false);
  }

  show_all_children();
}
// -------------------------------------------------------------------------
USensorJournal::USensorJournal() :
  Glib::ObjectBase("usensorjournal")
  ,USENSORJOURNAL_INIT_PROPERTIES()
  ,parameters(3,4)
  ,block_id(UniWidgetsTypes::DefaultObjectId)
  ,value_changed_(false)
{
  constructor();
}
// -------------------------------------------------------------------------
USensorJournal::USensorJournal(GtkmmBaseType::BaseObjectType* gobject) :
  UEventBox(gobject)
  ,USENSORJOURNAL_INIT_PROPERTIES()
  ,parameters(3,4)
  ,block_id(UniWidgetsTypes::DefaultObjectId)
  ,value_changed_(false)
{
  constructor();
}
// -------------------------------------------------------------------------
USensorJournal::~USensorJournal()
{
}
// -------------------------------------------------------------------------
std::string USensorJournal::get_time()
{
  time_t raw_time;
  struct tm * timeinfo;
  gchar timestr[64];
  
  time(&raw_time);
  timeinfo = localtime(&raw_time);
  strftime(timestr, 64, "%H:%M:%S", timeinfo);
  return std::string(timestr);
//   stringstream sstr;
//   tm *tms = localtime(&sec);
//   sstr << std::setfill('0');
//   sstr << std::setw(2) << tms->tm_hour << ":"
//     << std::setw(2) << tms->tm_min << ":"
//     << std::setw(2) << tms->tm_sec << endl;
}
// -------------------------------------------------------------------------
void USensorJournal::set_connector(const ConnectorRef& connector) throw()
{
  if (connector == get_connector())
  return;

  UEventBox::set_connector(connector);

//   get_connector()->signals().connect_on_any_message_full(
//                 sigc::mem_fun(this, &USensorJournal::recieve_message));
}
// -------------------------------------------------------------------------
void USensorJournal::on_connect() throw()
{
  cout<<"on_connect"<<endl;
  UEventBox::on_connect();
//   IOController_i::ShortMapSeq_var sseq;
//   get_connector()->signal_sensor_info().connect(sigc::mem_fun(this, &USensorJournal::sensorInfo));
  typedef Gtk::TreeModel::Children Children;
  Children children = tree_model_ref_->children();
  for(Children::iterator it = children.begin(); it!=children.end();
      it = tree_model_ref_->erase(it));
  
  children = tree_model_log_ref_->children();
  for(Children::iterator it = children.begin(); it!=children.end();
      it = tree_model_ref_->erase(it));

  try{
    cout<<"SMAP begining"<<endl;
    IOController_i::ShortMapSeq_var sseq = get_connector()->get_uin()->getSensors(get_connector()->getControllerID("SharedMemory1"),get_connector()->getLocalNode());//Uniset - Extensions.cc
// #ifdef DEBUG
    cout<<"SMAP size="<<sseq->length()<<endl;
// #endif
    /* Родительская строка для группировки датчиков по io */
    /*TODO В дальнейшем сделать групировку по io, предварительно считав nodes из configure*/
    Gtk::TreeModel::Row localrow = *(tree_model_ref_->append());
    localrow[columns_.text_message] = Glib::ustring("Sensors without io");
    localrow[columns_.name_string] = Glib::ustring("Local");
    
    progress.set_fraction(0);
    long map_size = sseq->length();
    for(int i = 0 ; i < map_size ; i++ )
    {
      cout<<"Here1"<<endl;
      const double new_fraction = (float)i/map_size;
      progress.set_fraction(new_fraction);
//       while(Gtk::Main::events_pending())
//         Gtk::Main::iteration();
      cout<<"Here2"<<endl;
      IOController_i::ShortMap* m = &(sseq[i]);
      xmlNode* node = get_connector()->getXMLObjectNode(m->id);
      if(!node)
        continue;//Если не нашли этот node в conf,то значит что-то не то с id
      
      string textname = UniWidgetsTypes::xmlGetProp((xmlNode*)node, string("textname"));
      string io = UniWidgetsTypes::xmlGetProp((xmlNode*)node, string("io"));
      string name = UniWidgetsTypes::xmlGetProp((xmlNode*)node, string("name"));
      string aps = UniWidgetsTypes::xmlGetProp((xmlNode*)node, string("aps"));
      string unet = UniWidgetsTypes::xmlGetProp((xmlNode*)node, string("unet2"));

#ifdef _DEBUG

      cout<<"sensor id="<<m->id<<
            ",name="<<name<<
            ",value="<<m->value<<
            ",type="<<m->type<<
            ",io="<<io<<"////"<<endl;

#endif
      UniWidgetsTypes::ObjectId node_from_io = UniWidgetsTypes::DefaultObjectId;
      if(!io.empty() )//&& io.find("cpu") != string::npos )
      {
        node_from_io = get_connector()->getNodeID(io);
//         cout<<"IO="<<io<<endl;
//         cout<<"Node HERE1="<<node_from_io<<endl;
      }
//       Если использовать ListStore, то получается простой список
//       Gtk::TreeModel::iterator iter = tree_model_ref_->append();
      Gtk::TreeModel::iterator iter;
      if( io == "" )
        iter = tree_model_ref_->append(localrow.children());
      else
      {
        //TODO сделать обработку наличия поля io
        iter = tree_model_ref_->append(localrow.children());
      }

      Gtk::TreeRow row = *iter;
      long mtype = atoi(UniWidgetsTypes::xmlGetProp((xmlNode*)node, string("mtype")).c_str());
      if(mtype == msgINFO)
      {
        row[columns_.bgcolor] = Gdk::Color("#585858");
        row[columns_.fgcolor] = property_info_color.get_value();
      }
      else if(mtype == msgALARM)
      {
        row[columns_.bgcolor] = Gdk::Color("#585858");
        row[columns_.fgcolor] = property_alarm_color.get_value();
      }
      else if(mtype == msgWARNING)
      {
        row[columns_.bgcolor] = Gdk::Color("#585858");
        row[columns_.fgcolor] = property_warn_color.get_value();
      }
      else if(mtype == msgATTENTION)
      {
        row[columns_.bgcolor] = Gdk::Color("#585858");
        row[columns_.fgcolor] = property_attention_color.get_value();
      }
      else
      {
        row[columns_.bgcolor] = Gdk::Color("#585858");
        row[columns_.fgcolor] = Gdk::Color("#585858");
      }

      row[columns_.id_string] = m->id;
      row[columns_.name_string] = Glib::ustring::compose("%1",Glib::ustring::format(std::right,name));
      row[columns_.value_string] = m->value;//Glib::ustring::compose("%1",Glib::ustring::format(std::right,m->value));
      row[columns_.text_message] = conf->oind->getTextName(m->id);
      row[columns_.time_string] = get_time();
      row[columns_.stype] = m->type;
      row[columns_.node_string] = io;
      row[columns_.unet_string] = unet;
      if( aps != "" )
        row[columns_.aps_group] = aps;

//       get_connector()->get_uin()->askRemoteSensor(m->id, UniversalIO::UIONotify,conf->getLocalNode(), get_connector()->get_id());//get_id - нужно будет поменять
      SensorStore.insert ( std::pair<UniWidgetsTypes::ObjectId,Gtk::TreeModel::iterator>(m->id,iter) );
//       if (m->type == UniversalIO::DI || m->type == UniversalIO::DO )
//         get_connector()->signals().connect_value_changed(
//                       sigc::mem_fun(this, &USensorJournal::update_row),
//                                     m->id,
//                                     node_from_io);
//       else if(m->type == UniversalIO::AnalogInput || m->type == UniversalIO::AnalogOutput )
//         get_connector()->signals().connect_analog_value_changed(
//                       sigc::mem_fun(this, &USensorJournal::update_row),
//                                     m->id,
//                                     node_from_io);
    }
  }
  catch(...){cout<<"CATCH SMTH!"<<endl;};
  
  Glib::signal_timeout().
      connect(sigc::mem_fun(this, &USensorJournal::update_rows), 500);

//   if (get_property_disconnect_effect() == 2) {
//     scrolled_window_.set_sensitive();
//     tree_view_.set_sensitive();
//   }
}
// -------------------------------------------------------------------------
// void USensorJournal::update_row(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value)
bool USensorJournal::update_rows()

{
    cout<<"Update rows"<<endl;
#if 0
  if(SensorStore.find(id) == SensorStore.end())
    return;
  Gtk::TreeRow row = *SensorStore[id];
  
  if(row.get_value(columns_.watch))
  {
    Gtk::TreeRow log_row = *(tree_model_log_ref_->append());
    log_row[columns_.id_string] = row.get_value(columns_.id_string);
    log_row[columns_.value_string] = value;
    log_row[columns_.time_string] = get_time();
    log_row[columns_.name_string] = row.get_value(columns_.name_string);
  }
  if(id != block_id)
  {
    row[columns_.value_string] = value;
  }
#endif
    try{
      IOController_i::ShortMapSeq_var sseq = get_connector()->get_uin()->getSensors(get_connector()->getControllerID("SharedMemory1"),get_connector()->getLocalNode());//Uniset - Extensions.cc//FOR GUI SharedMemoryDB
      cout<<"LENGTH = "<<sseq->length()<<endl;
      for(int i = 0 ; i < sseq->length() ; i++ )
      {
        IOController_i::ShortMap* m = &(sseq[i]);
        xmlNode* node = get_connector()->getXMLObjectNode(m->id);
        if(!node)
          continue;//Если не нашли этот node в conf,то значит что-то не то с id
        SensorStoreMap::iterator it = SensorStore.find(m->id);
        if( it == SensorStore.end() || (*it->second)[columns_.value_string] == m->value)
          continue;

        (*it->second)[columns_.value_string] = m->value;
      }
    }
    catch(...){ return false;}
    return true;
}
// -------------------------------------------------------------------------
void USensorJournal::on_disconnect() throw()
{
  UEventBox::on_disconnect();
  SensorStore.clear();
//   tree_model_ref_.clear();
//   if (get_property_disconnect_effect() == 2) {
//     scrolled_window_.set_sensitive(false);
//     tree_view_.set_sensitive(false);
//   }
}
// -------------------------------------------------------------------------
void USensorJournal::on_id_title_changed()
{
  tree_view_.get_column(1)->set_title( property_id_title );
}
// -------------------------------------------------------------------------
void USensorJournal::on_name_title_changed()
{
  tree_view_.get_column(2)->set_title( property_name_title );
}
// -------------------------------------------------------------------------
void USensorJournal::on_value_title_changed()
{
  tree_view_.get_column(0)->set_title( property_value_title );
}
// -------------------------------------------------------------------------
void USensorJournal::on_time_title_changed()
{
  tree_view_.get_column(5)->set_title( property_time_title );
}
// -------------------------------------------------------------------------
void USensorJournal::on_text_title_changed()
{
  tree_view_.get_column(6)->set_title( property_text_title );
}
// -------------------------------------------------------------------------
void USensorJournal::on_aps_title_changed()
{
  tree_view_.get_column(4)->set_title( property_aps_title );
}
// -------------------------------------------------------------------------
void USensorJournal::on_id_width_changed()
{
  long tree_width = tree_view_.get_width();
  long field_width = (tree_width * property_id_width)/100;
  cout<<"on_id_width_changed 1 tr "<<tree_width<<endl;
  cout<<"on_id_width_changed 2 fi "<<field_width<<endl;
  tree_view_.get_column(1)->set_fixed_width( field_width );
}
// -------------------------------------------------------------------------
void USensorJournal::on_name_width_changed()
{
  long tree_width = tree_view_.get_width();
  long field_width = (tree_width * property_name_width)/100;
  cout<<"on_name_width_changed 1 tr "<<tree_width<<endl;
  cout<<"on_name_width_changed 2 fi "<<field_width<<endl; 
  tree_view_.get_column(2)->set_fixed_width( field_width);
}
// -------------------------------------------------------------------------
void USensorJournal::on_value_width_changed()
{
  long tree_width = tree_view_.get_width();
  long field_width = (tree_width * property_value_width)/100;
  cout<<"on_value_width_changed 1 tr "<<tree_width<<endl;
  cout<<"on_value_width_changed 2 fi "<<field_width<<endl; 
  tree_view_.get_column(0)->set_fixed_width( field_width);
}
// -------------------------------------------------------------------------
void USensorJournal::on_time_width_changed()
{
  long tree_width = tree_view_.get_width();
  long field_width = (tree_width * property_time_width)/100;
  cout<<"on_time_width_changed 1 tr "<<tree_width<<endl;
  cout<<"on_time_width_changed 2 fi "<<field_width<<endl; 
  tree_view_.get_column(5)->set_fixed_width( field_width);
}
// -------------------------------------------------------------------------
void USensorJournal::on_aps_width_changed()
{
  long tree_width = tree_view_.get_width();
  long field_width = (tree_width * get_property_aps_width())/100;
  cout<<"on_aps_width_changed 1 tr "<<tree_width<<endl;
  cout<<"on_aps_width_changed 2 fi "<<field_width<<endl; 
  tree_view_.get_column(4)->set_fixed_width( field_width);
}
// -------------------------------------------------------------------------
void USensorJournal::on_realize()
{
  UEventBox::on_realize();
  
  on_id_width_changed();
  on_name_width_changed();
  on_value_width_changed();
  on_time_width_changed();
  on_aps_width_changed();
  
  tree_view_.columns_autosize();
}
// -------------------------------------------------------------------------
void USensorJournal::on_treeview_row_activated(const Gtk::TreeModel::Path& path,
                                              Gtk::TreeViewColumn* column)
{
  cout<<"on_treeview_row_activated"<<endl; 
//   cout<<"TreePath1="<<path.to_string()<<endl;
  Gtk::TreeModel::iterator iter = tree_model_filter_->get_iter(path);
  Gtk::TreeModel::iterator parent_iter = (*iter).parent();
  if(!parent_iter)/* Если это строка группы,то отображаем её в любом случае */
    return;

  if(iter)
  {
    Gtk::TreeModel::Row row = *iter;
//     std::cout << "Row activated: ID=" << row[columns_.stype] << std::endl;
    if ( row[columns_.stype] == UniversalIO::DI || row[columns_.stype] == UniversalIO::DO )
    {
      long value = !row[columns_.value_string];
      UniWidgetsTypes::ObjectId sensor = row[columns_.id_string];
      UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId;
      try
      {
        if (get_connector())
        {
            if(!(row.get_value(columns_.node_string).empty()))
                node = get_connector()->getNodeID(row.get_value(columns_.node_string));
            else if(!(row.get_value(columns_.unet_string).empty()))
                node = get_connector()->getNodeID(row.get_value(columns_.unet_string));
//             cout<<"save sensor="<<sensor<<",node="<<node<<endl;
            get_connector()->save_value(value, sensor, node);
        }
      }
      catch(UniWidgetsTypes::Exception& ex)
      {
        cerr << ex << endl;
      }
    }
//     else /*TODO сделать обработку для других типов, для AI например можно вызвать отдельной окно с полем ввода*/
  }
}
// -------------------------------------------------------------------------
// void USensorJournal::treeviewcolumn_validated_on_cell_data( Gtk::CellRenderer* renderer, const Gtk::TreeModel::iterator& iter)
// {
// }
// -------------------------------------------------------------------------
void USensorJournal::on_editing_canceled()
{
// #ifdef DEBUG
  cout<<"on_editing_canceled chg="<<value_changed_<<endl;
// #endif
  Glib::RefPtr<Gtk::TreeSelection> treeselection = tree_view_.get_selection();
  Gtk::TreeModel::iterator iter = treeselection->get_selected();
  if(iter && value_changed_)
  {
    Gtk::TreeModel::Row row = *iter;
    on_set_sensor( tree_model_filter_->get_path(iter).to_string(), row[columns_.value_string]);
    value_changed_ = false;
  
    Gtk::TreeRow child_row = *SensorStore[row.get_value(columns_.id_string)];
    child_row[columns_.time_string] = get_time();
  }
}
// -------------------------------------------------------------------------
void USensorJournal::on_editing_started(Gtk::CellEditable* editable, const Glib::ustring& path_string)
{
// #ifdef DEBUG
  cout<<"on_editing_started"<<endl;
// #endif 
  Gtk::TreePath path(path_string);
  Gtk::TreeModel::iterator iter = tree_model_filter_->get_iter(path);
  Gtk::TreeModel::Row row = *iter;
  Gtk::CellEditable* celleditable_validated = editable;
  celleditable_validated->signal_editing_done().connect(sigc::mem_fun(this,&USensorJournal::on_my_editing_done));

  Gtk::SpinButton* pSpin = dynamic_cast<Gtk::SpinButton*>(celleditable_validated);
  if(pSpin)
  {
    pSpin->set_numeric(true);
    pSpin->modify_font(Pango::FontDescription("Sans 10"));
    
    if(row[columns_.stype] == UniversalIO::DI || row[columns_.stype] == UniversalIO::DO )
      pSpin->set_range(0,1);
    else
      pSpin->set_range(0,100000);//Так как задается диапазон,то мин = 0,а макс = какое-то большое(предельное число)
    
    value_change_connection_ = pSpin->signal_value_changed().connect(sigc::bind(sigc::mem_fun(this,&USensorJournal::on_value_changed),pSpin,path_string));
    block_id = row[columns_.id_string];
  }
}
// -------------------------------------------------------------------------
void USensorJournal::on_my_editing_done()
{
  block_id = UniWidgetsTypes::DefaultObjectId;
  value_change_connection_.disconnect();
#ifdef DEBUG
  cout<<"on_my_editing_done"<<endl;
#endif 
}
// -------------------------------------------------------------------------
void USensorJournal::on_value_changed(const Gtk::SpinButton* pSpin,const Glib::ustring &path_string)
{
#ifdef DEBUG
  cout<<"on_value changed "<<",path="<<path_string<<endl;
#endif
  long value = (long)pSpin->get_value();
  on_set_sensor(path_string, value);
  value_changed_ = true;
}
// -------------------------------------------------------------------------
void USensorJournal::on_my_edited(const Glib::ustring& path_string, const Glib::ustring& new_text)
{
#ifdef DEBUG
  cout<<"on_edited text="<<new_text<<",path="<<path_string<<endl;
#endif
  
  istringstream buffer(new_text.c_str());
  long value=0;
  buffer >> value;
  /* При изменении значения с использованием, кнопок больше/меньше не обновляем значение поля
     пока не завершится редактирование поля. Иначе если обновлять каждый раз при изменении,то 
     поле будет завершать редактирование автоматически*/
  if(value_changed_)
  {
    
    Gtk::TreePath path(path_string);
    Gtk::TreeModel::iterator iter = tree_model_filter_->get_iter(path);
    
    if(iter)
    {
      Gtk::TreeModel::Row row = *iter;
//       update_row(row[columns_.id_string], UniWidgetsTypes::DefaultObjectId, value);
      if(SensorStore.find(row.get_value(columns_.id_string)) == SensorStore.end())
        return;
      Gtk::TreeRow child_row = *SensorStore[row.get_value(columns_.id_string)];

      child_row[columns_.value_string] = value;
      child_row[columns_.time_string] = get_time();
      value_changed_ = false;
    }
  }
  
  on_set_sensor(path_string, value);
}
// -------------------------------------------------------------------------
void USensorJournal::on_set_sensor(const Glib::ustring& path_string, long value )
{
#ifdef DEBUG
  cout<<"on_set_sensor "<<",path="<<path_string<<endl;
#endif
  Gtk::TreePath path(path_string);
  Gtk::TreeModel::iterator iter = tree_model_filter_->get_iter(path);

  Gtk::TreeModel::iterator parent_iter = (*iter).parent();
  if(!parent_iter)/* Если это строка группы,то отображаем её в любом случае.*/
    return;

  if(iter)
  {
    Gtk::TreeModel::Row row = *iter;
    
    /* Если датчик дискретный,то может быть только два значения 0 или 1 */
    if (row[columns_.stype] == UniversalIO::DI || row[columns_.stype] == UniversalIO::DO )
      value = value > 0 ? 1 : 0;
    
#ifdef DEBUG
    cout<<"name="<<row[columns_.name_string]<<",VALUE1="<<value<<endl;
#endif
    UniWidgetsTypes::ObjectId sensor = row[columns_.id_string];
    UniWidgetsTypes::ObjectId node = UniWidgetsTypes::DefaultObjectId;

    try
    {
      if (get_connector())
      {
        if(!(row.get_value(columns_.node_string).empty()))
            node = get_connector()->getNodeID(row.get_value(columns_.node_string));
        else if(!(row.get_value(columns_.unet_string).empty()))
              node = get_connector()->getNodeID(row.get_value(columns_.unet_string));
        cout<<"save sensor="<<sensor<<",node="<<node<<endl;
        get_connector()->save_value(value, sensor, node);
      }
    }
    catch(UniWidgetsTypes::Exception& ex)
    {
      cerr << ex << endl;
    }
  }
}
//-------------------------------------------------------------------------
void USensorJournal::on_modify(const Gtk::TreeModel::iterator& iter,Glib::ValueBase& value,int column )
{
  cout<<"on_filtred"<<endl;
}
//-------------------------------------------------------------------------
bool USensorJournal::on_rows_visible(const Gtk::TreeModel::iterator& iter )
{
//   cout<<"on_rows_visible"<<endl;
  Gtk::TreeModel::Row row = *iter;
  Gtk::TreeModel::iterator parent_iter = row.parent();
  if(!parent_iter)/* Если это строка группы,то отображаем её в любом случае.Иначе ничего не отобразиться */
    return true;

  if(row[columns_.mark])
    return true;

  if( !on_check_row_type(iter))/* Проверка типа датчика для отображения только задынных типов */
    return false;

  if(chk_id.get_active() && find_in_string(Glib::ustring::compose("%1",row[columns_.id_string])) )
    return true;
  if(chk_name.get_active() && find_in_string(row[columns_.name_string]) )
     return true;
  if(chk_text.get_active() && find_in_string(row[columns_.text_message]) )
     return true;
  if(chk_aps.get_active() && find_in_string(row[columns_.aps_group]) )
    return true; 

  return false;
}
// -------------------------------------------------------------------------
bool USensorJournal::on_check_row_type(const Gtk::TreeModel::iterator& iter )
{
  Gtk::TreeModel::Row row = *iter;

  if(chk_di.get_active() && row[columns_.stype] == UniversalIO::DI )
    return true;
  if(chk_do.get_active() && row[columns_.stype] == UniversalIO::DO )
    return true;
  if(chk_ai.get_active() && row[columns_.stype] == UniversalIO::AI )
    return true;
  if(chk_ao.get_active() && row[columns_.stype] == UniversalIO::AO )
    return true;

  return false;
}
// -------------------------------------------------------------------------
bool USensorJournal::find_in_string(const Glib::ustring& str1)
{
  Glib::ustring::size_type res = std::string::npos;
  if(!chk_reglet.get_active())
    res = str1.casefold().find(browse.get_text().casefold());
  else
    res = str1.find(browse.get_text());

  if( res == std::string::npos)
    return false;
  else
    return true;
}
// -------------------------------------------------------------------------
void USensorJournal::on_change_browse()
{
//   cout<<"on_change_browse"<<endl;
  tree_model_filter_->refilter();
}
// -------------------------------------------------------------------------
// void USensorJournal::on_row_changed(const Gtk::TreeModel::Path& path, const Gtk::TreeModel::iterator& iter)
// {
//   if((*iter)[columns_.id_string] != 10)
//     cout<<"on_row_changed() path="<<path<<",name="<<(*iter)[columns_.name_string]<<endl;
// }
// -------------------------------------------------------------------------
// void USensorJournal::on_row_filtered_changed(const Gtk::TreeModel::Path& path, const Gtk::TreeModel::iterator& iter)
// {
//   if((*iter)[columns_.id_string] != 10)
//     cout<<"on_row_filtred_changed() path="<<path<<",name="<<(*iter)[columns_.name_string]<<endl;
// }
// -------------------------------------------------------------------------
bool USensorJournal::on_button_press_event(GdkEventButton* event)
{
  if( (event->type == GDK_BUTTON_PRESS) && (event->button == 3) )
  {
    m_Menu_Popup.popup(event->button, event->time);
  }

  return false;
}
// -------------------------------------------------------------------------
void USensorJournal::on_menu_watch_generic()
{
#ifdef DEBUG
  std::cout << "Add watching...";
#endif
  Glib::RefPtr<Gtk::TreeView::Selection> refSelection = tree_view_.get_selection();
  if(refSelection)
  {
    Gtk::TreeModel::iterator iter = refSelection->get_selected();
    if(iter)
    {
#ifdef DEBUG
      std::cout << " id=" << (*iter)[columns_.id_string] << std::endl;
#endif
      add_watching_for((*iter).get_value(columns_.id_string));
      
    }
  }
}
// -------------------------------------------------------------------------
void USensorJournal::on_menu_mark_generic(bool state)
{
#ifdef DEBUG
  std::cout << "Unmark...";
#endif

  Glib::RefPtr<Gtk::TreeView::Selection> refSelection = tree_view_.get_selection();
  if(refSelection)
  {
    Gtk::TreeModel::iterator iter = refSelection->get_selected();
    if(iter)
    {
#ifdef DEBUG
      std::cout << " id=" << (*iter)[columns_.id_string] << std::endl;
#endif
      UniWidgetsTypes::ObjectId id = ((*iter).get_value(columns_.id_string));
      if(SensorStore.find(id) == SensorStore.end())
        return;
  
      Gtk::TreeRow row = *SensorStore[id];
      row[columns_.mark] = state;
    }
  }
}
// -------------------------------------------------------------------------
void USensorJournal::on_menu_unmarkall_generic()
{
#ifdef DEBUG
  std::cout << "Unmark all...";
#endif
  tree_model_ref_->foreach_iter(sigc::mem_fun(this,&USensorJournal::on_foreach));
}
// -------------------------------------------------------------------------
void USensorJournal::on_menu_unwatch_generic()
{
#ifdef DEBUG
  std::cout << "Del watching...";
#endif

  Glib::RefPtr<Gtk::TreeView::Selection> refSelection = tree_view_log_.get_selection();
  if(refSelection)
  {
    Gtk::TreeModel::iterator iter = refSelection->get_selected();
    if(iter)
    {
#ifdef DEBUG
      std::cout << "on_menu_unwatch_generic id=" << (*iter)[columns_.id_string] << std::endl;
#endif
      del_watching_for((*iter).get_value(columns_.id_string));
    }
  }
}
// -------------------------------------------------------------------------
void USensorJournal::on_menu_clear_all_generic()
{
#ifdef DEBUG
  std::cout << "clear all logs...";
#endif
  clear_logs();
}
// -------------------------------------------------------------------------
bool USensorJournal::on_my_event(GdkEvent* event)
{
  if( ((event->button).type == GDK_BUTTON_PRESS) && ((event->button).button == 3) )
  {
    m_Menu_Popup.popup((event->button).button, (event->button).time);
  }

  return false;
}
// -------------------------------------------------------------------------
void USensorJournal::add_watching_for(UniWidgetsTypes::ObjectId id)
{
  if(SensorStore.find(id) == SensorStore.end())
    return;
  
  Gtk::TreeRow row = *SensorStore[id];
  if(row.get_value(columns_.watch))
    return; // Уже отслеживаетсяъ
  
  Gtk::TreeRow log_row = *(tree_model_log_ref_->append());

  log_row[columns_.id_string] = row.get_value(columns_.id_string);
  log_row[columns_.value_string] = row.get_value(columns_.value_string);
  log_row[columns_.time_string] = get_time();
  log_row[columns_.name_string] = row.get_value(columns_.name_string);
//   row[columns_.value_string] = value;
  row[columns_.watch] = true;
}
// -------------------------------------------------------------------------
bool USensorJournal::on_my_log_event(GdkEvent* event)
{
  if( ((event->button).type == GDK_BUTTON_PRESS) && ((event->button).button == 3) )
  {
    m_Menu_Popup_Logs.popup((event->button).button, (event->button).time);
  }

  return false;
}
// -------------------------------------------------------------------------
void USensorJournal::del_watching_for(UniWidgetsTypes::ObjectId id)
{
  if(SensorStore.find(id) == SensorStore.end())
    return;

  Gtk::TreeRow row = *SensorStore[id];
  row[columns_.watch] = false;
#ifdef DEBUG
  cout<<"del_watching_for id="<<id<<endl;
#endif
  /* Очистить логи от записей для данног датчика */
  clear_logs(id);
}
// -------------------------------------------------------------------------
void USensorJournal::clear_logs(UniWidgetsTypes::ObjectId id)
{
  typedef Gtk::TreeModel::Children Children;
  Children children = tree_model_log_ref_->children();

  Children::iterator it = children.begin();
  while (it != children.end()) {
    Gtk::TreeModel::Row row = *it;
    if ( row.get_value(columns_.id_string) == id || id == UniWidgetsTypes::DefaultObjectId )
    {
      it = tree_model_log_ref_->erase(it);
    }
    else
      it++;
  }
}
// -------------------------------------------------------------------------
bool USensorJournal::on_foreach(const Gtk::TreeIter& it)
{
#ifdef DEBUG
  cout<<"on_foreach"<<endl;
#endif
  (*it)[columns_.mark] = false;
  return false;
}
// ----------------------------------------------------------------------------