#ifndef _USENSORJOURNAL_H
#define _USENSORJOURNAL_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <gtkmm.h>
#include <time.h>
#include <uwidgets/UEventBox.h>
// #include <USignals.h>
// #include <uwidgets/KineticScroll.h>
#include <plugins.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
class msgItem;
/*! структура, описывающая строку в журнале(точнее колонки) */
class SensorColumnRecord : public Gtk::TreeModel::ColumnRecord
{
  public:

    SensorColumnRecord()
    {
      add(value_string);
      add(time_string);
      add(text_message);
      add(id_string);
      add(name_string);
      add(node_string);
      add(unet_string);
      add(aps_group);
      add(time);
      add(bgcolor);
      add(fgcolor);
      add(stype);
      add(mark);
      add(watch);
    }

    Gtk::TreeModelColumn<long> value_string;
    Gtk::TreeModelColumn<Glib::ustring> time_string;
    Gtk::TreeModelColumn<Glib::ustring> text_message;
    Gtk::TreeModelColumn<long> id_string;
    Gtk::TreeModelColumn<Glib::ustring> name_string;
    Gtk::TreeModelColumn<Glib::ustring> node_string;
    Gtk::TreeModelColumn<Glib::ustring> unet_string;
    Gtk::TreeModelColumn<Glib::ustring> aps_group;
    Gtk::TreeModelColumn<time_t> time;
    Gtk::TreeModelColumn<Gdk::Color> bgcolor;
    Gtk::TreeModelColumn<Gdk::Color> fgcolor;
    Gtk::TreeModelColumn<UniversalIO::IOType> stype;
    Gtk::TreeModelColumn<bool> mark;
    Gtk::TreeModelColumn<bool> watch;
};
// -------------------------------------------------------------------------
/*!
 * \brief Журнал для датчиков.
 * \par
 * Графический smviewer для отслеживания значений датчиков.
*/
class USensorJournal : public UEventBox
{
public:
  USensorJournal();
  explicit USensorJournal(GtkmmBaseType::BaseObjectType* gobject);
  virtual ~USensorJournal();

  virtual void set_connector(const ConnectorRef& connector) throw();
  virtual void on_connect() throw();
  virtual void on_disconnect() throw();

protected:

  /* Handlers */
  virtual void on_realize();
  void on_treeview_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);
  void on_change_browse();
  bool on_button_press_event(GdkEventButton* event);
  bool on_my_event(GdkEvent* event);
  bool on_my_log_event(GdkEvent* event);
  
  void on_menu_watch_generic();
  void on_menu_unwatch_generic();
  void on_menu_clear_all_generic();
  void on_menu_mark_generic(bool state = true);
  void on_menu_unmarkall_generic();
  void add_watching_for(UniWidgetsTypes::ObjectId id);
  void del_watching_for(UniWidgetsTypes::ObjectId id);
  void clear_logs(UniWidgetsTypes::ObjectId id = UniWidgetsTypes::DefaultObjectId);
  bool on_foreach(const Gtk::TreeIter& it);
  std::string get_time();

  typedef std::unordered_map<UniWidgetsTypes::ObjectId,Gtk::TreeModel::iterator > SensorStoreMap;
  SensorStoreMap SensorStore;

  Gtk::TreeView tree_view_;        /*!< виджет отображающий модель (Gtk::TreeModel) данных и позволяющий пользователю взаимодействовать с ними */
  Glib::RefPtr<Gtk::TreeStore> tree_model_ref_;    /*!< модель списка для использования с виджетом Gtk::TreeView */
  Glib::RefPtr<Gtk::TreeModelFilter> tree_model_filter_;
  Gtk::Entry browse;
  Gtk::VBox vbox;
  Gtk::Table parameters;
  Gtk::CheckButton chk_id;
  Gtk::CheckButton chk_name;
  Gtk::CheckButton chk_text;
  Gtk::CheckButton chk_aps;
  Gtk::CheckButton chk_reglet;
  Gtk::CheckButton chk_di;
  Gtk::CheckButton chk_do;
  Gtk::CheckButton chk_ai;
  Gtk::CheckButton chk_ao;
  Gtk::VPaned vpan;
  Gtk::TreeView tree_view_log_;
  Glib::RefPtr<Gtk::ListStore> tree_model_log_ref_;
// Glib::Property<Glib::ustring> property_shmid;    /*!< свойство: идентификатор SM */
  Gtk::ProgressBar progress;

  SensorColumnRecord columns_;        /*!< описание строки в журнале */

  Gtk::ScrolledWindow scrolled_window_;      /*!< главное прокручиваемое окно журнала */
  Gtk::ScrolledWindow scrolled_window_log_;     /*!< главное прокручиваемое окно журнала логов */

  Gtk::Menu m_Menu_Popup;
  Gtk::Menu m_Menu_Popup_Logs;
  
private:
  
  void constructor();
//   void update_row(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value);
    bool update_rows();

//   void treeviewcolumn_validated_on_cell_data(Gtk::CellRenderer* renderer, const Gtk::TreeModel::iterator& iter);
  void on_editing_started(Gtk::CellEditable* editable, const Glib::ustring& path_string);
  void on_editing_canceled();
  void on_my_editing_done();
  void on_my_edited(const Glib::ustring& path_string, const Glib::ustring& new_text);
  void on_modify(const Gtk::TreeModel::iterator& iter,Glib::ValueBase& value,int column );
  bool on_rows_visible(const Gtk::TreeModel::iterator& iter );
  bool on_check_row_type(const Gtk::TreeModel::iterator& iter );
  bool find_in_string(const Glib::ustring& str1);
//   void on_row_changed(const Gtk::TreeModel::Path& path, const Gtk::TreeModel::iterator& iter);
//   void on_row_filtered_changed(const Gtk::TreeModel::Path& path, const Gtk::TreeModel::iterator& iter);
  void on_value_changed(const Gtk::SpinButton* pSpin, const Glib::ustring &path_strin);
  void on_set_sensor(const Glib::ustring& path_string, long value );

  void on_id_title_changed();
  void on_name_title_changed();
  void on_value_title_changed();
  void on_time_title_changed();
  void on_text_title_changed();
  void on_aps_title_changed();

  void on_id_width_changed();
  void on_name_width_changed();
  void on_value_width_changed();
  void on_time_width_changed();
  void on_aps_width_changed();

  UniWidgetsTypes::ObjectId block_id; /*! id-датчика для блокировки обновления списка. Нужно при выставлении значения при помощи кнопок(больше, меньше) в Gtk::SpinButton */
  sigc::connection value_change_connection_;
  bool value_changed_;

  DISALLOW_COPY_AND_ASSIGN(USensorJournal);
  /*Properties*/
  ADD_PROPERTY( property_id_title, Glib::ustring);  /*!< свойство: заголовок шапки для поля ID датчика */
  ADD_PROPERTY( property_name_title, Glib::ustring); /*!< свойство: заголовок шапки для поля название датчика */
  ADD_PROPERTY( property_value_title, Glib::ustring); /*!< свойство: заголовок шапки для поля значения датчика */
  ADD_PROPERTY( property_time_title, Glib::ustring);  /*!< свойство: заголовок шапки для поля времени поступления сообщения */
  ADD_PROPERTY( property_text_title, Glib::ustring);  /*!< свойство: заголовок шапки для поля текста сообщения */
  ADD_PROPERTY( property_aps_title, Glib::ustring);  /*!< свойство: заголовок шапки для поля группы апс */
  ADD_PROPERTY( property_info_color, Gdk::Color);   /*!< свойство: цвет информационного сообщения */
  ADD_PROPERTY( property_warn_color, Gdk::Color);   /*!< свойство: цвет предупредительного сообщения */
  ADD_PROPERTY( property_alarm_color, Gdk::Color);  /*!< свойство: цвет аварийного сообщения */
  ADD_PROPERTY( property_attention_color, Gdk::Color);  /*!< свойство: цвет сообщения типа "внимание" */

  ADD_PROPERTY( property_id_width, long);    /*!< свойство:  ширина поля в процентах */
  ADD_PROPERTY( property_name_width, long)
  ADD_PROPERTY( property_value_width, long);
  ADD_PROPERTY( property_time_width, long);
  ADD_PROPERTY( property_aps_width, long);
  ADD_PROPERTY( property_text_width, long);
  ADD_PROPERTY( property_local_mode, bool);

};
#endif
