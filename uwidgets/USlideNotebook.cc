#include "USlideNotebook.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
#define USlideNotebook_INIT_PROPERTIES() \
	property_enable_lock(*this, "enable-lock", true) \
	,property_page(*this, "page", 0 ) \
	,property_orientation(*this, "page-slide-orientation", Gtk::ORIENTATION_HORIZONTAL ) \
	,current_page(0) \
// -------------------------------------------------------------------------
void USlideNotebook::ctor()
{
	GObjectClass *goclass = G_OBJECT_GET_CLASS(gobj());
	goclass->set_property = &custom_set_property_callback<USlideNotebook>;

	scrollwin = manage(new Gtk::ScrolledWindow());
	hbox = manage(new Gtk::HBox());
	vbox = manage(new Gtk::VBox());
	scrollwin->add(*hbox);
	Gtk::Fixed::on_add(scrollwin);
	show_all();

	signal_size_allocate().connect(sigc::mem_fun(*this, &USlideNotebook::notebook_allocate_changed));

	connect_property_changed("enable-lock", sigc::mem_fun(this,&USlideNotebook::on_property_enable_lock_changed));
	connect_property_changed("page", sigc::mem_fun(this,&USlideNotebook::on_property_page_changed));
	connect_property_changed("page-slide-orientation", sigc::mem_fun(this,&USlideNotebook::on_property_orientation_changed));
}
// -------------------------------------------------------------------------
USlideNotebook::USlideNotebook() :
	Glib::ObjectBase("USlideNotebook")
	,USlideNotebook_INIT_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
USlideNotebook::USlideNotebook(GtkmmBaseType::BaseObjectType* gobject) :
	BaseType(gobject)
	,USlideNotebook_INIT_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
USlideNotebook::~USlideNotebook()
{
}
// -------------------------------------------------------------------------
void USlideNotebook::on_realize()
{
	Gtk::Fixed::on_realize();

	Gtk::Allocation alloc = get_allocation();
	notebook_allocate_changed(alloc);
}
// -------------------------------------------------------------------------
void USlideNotebook::add_child(Gtk::Widget* w)
{
	Gtk::Fixed* fixed = manage(new Gtk::Fixed());
	fixed->set_size_request(width,height);
	cout<< get_name() << "::on_add() width="<<width<<" height="<<height<< endl;
	fixed->put(*w,0,0);
	fixed->show();

	if(property_orientation.get_value()==Gtk::ORIENTATION_HORIZONTAL)
	{
		hbox->add(*fixed);
	}
	else
	{
		vbox->add(*fixed);
	}
}
// -------------------------------------------------------------------------
void USlideNotebook::on_add(Gtk::Widget* w)
{
	add_child(w);
}
// -------------------------------------------------------------------------
void USlideNotebook::remove_child(Gtk::Widget* w)
{
	if(property_orientation.get_value()==Gtk::ORIENTATION_HORIZONTAL)
	{
		std::vector<Gtk::Widget*> children = hbox->get_children();
		for(std::vector<Gtk::Widget*>::iterator it = children.begin();it != children.end();it++)
			if(*it == w)
				hbox->remove(*w);
	}
	else
	{
		std::vector<Gtk::Widget*> children = vbox->get_children();
		for(std::vector<Gtk::Widget*>::iterator it = children.begin();it != children.end();it++)
			if(*it == w)
				vbox->remove(*w);
	}
}
// -------------------------------------------------------------------------
void USlideNotebook::on_remove(Gtk::Widget* w)
{
	remove_child(w);
}
// -------------------------------------------------------------------------
void USlideNotebook::notebook_allocate_changed(Gtk::Allocation& alloc)
{
	width = get_allocation().get_width();
	height = get_allocation().get_height();
	cout<< get_name() << "::notebook_allocate_changed() width="<<width<<" height="<<height<< endl;
	scrollwin->set_size_request(width,height);

	if(property_orientation.get_value()==Gtk::ORIENTATION_HORIZONTAL)
	{
		hbox->set_size_request(-1,height);

		std::vector<Gtk::Widget*> children = hbox->get_children();
		for(std::vector<Gtk::Widget*>::iterator it = children.begin();it != children.end();it++)
			(*it)->set_size_request(width,height);
	}
	else
	{
		vbox->set_size_request(width,-1);
		std::vector<Gtk::Widget*> children = vbox->get_children();
		for(std::vector<Gtk::Widget*>::iterator it = children.begin();it != children.end();it++)
			(*it)->set_size_request(width,height);
	}
}
// -------------------------------------------------------------------------
void USlideNotebook::add_lock(const Gtk::Widget& w)
{
	locks_list.push_back(&w);
	on_property_enable_lock_changed();
	/*нужен ли следующий вызов*/
//	UDefaultFunctions<Gtk::Notebook>::add_lock(w);
}
// -------------------------------------------------------------------------
void USlideNotebook::unlock_current()
{
	locks_list.pop_front();
	on_property_enable_lock_changed();

//	UDefaultFunctions<Gtk::Notebook>::unlock_current();
}
// -------------------------------------------------------------------------
void USlideNotebook::enable_lock(bool enable)
{
	property_enable_lock = enable;
}
// -------------------------------------------------------------------------
void USlideNotebook::on_property_enable_lock_changed()
{
	if (property_enable_lock && !locks_list.empty())
	{
//		const int page = page_num(*locks_list.front());
//		Gtk::Notebook::set_current_page(page);
		return;
	}
}
// -------------------------------------------------------------------------
void USlideNotebook::on_property_page_changed()
{
}
// -------------------------------------------------------------------------
void USlideNotebook::on_property_orientation_changed()
{
}
// -------------------------------------------------------------------------
