#ifndef _USlideNotebook_H
#define _USlideNotebook_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <deque>
#include "UDefaultFunctions.h"
#include <plugins.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
/*!
 * \brief Класс блокнота с закладками.
 * \par
 * Класс предназначен для реализации виджета блокнота с блокирующимися вкладками.
 * Вкладки блокируются и автоматически переключаются при срабатывании АПС сигнала.
 * Блокированную вкладку нельзя переключить пока сигнал не заквитирован или
 * не выставлен блокировка автопереключения вкладок. Если срабатывает несколько
 * АПС сигналов, то экран блокируется первым пришедшим сигналом, а все последующие
 * сигналы(точнее виджеты соответствующие этим сигналам) помещаются в очередь
 * блокировки. Когда будет заквитирован первый сигнал он будет удален из очереди и
 * блокнот автоматически переключиться на следующий в очереди сигнал и так пока
 * не закончится очередь.
*/
class USlideNotebook : public UDefaultFunctions<Gtk::Fixed>
{
public:
	USlideNotebook();

	explicit USlideNotebook(GtkmmBaseType::BaseObjectType* gobject);
	~USlideNotebook();

// 	virtual bool set_current_page(int page);
	void lock_nbook(const Gtk::Widget *w);				/*!< не используется */

	virtual void add_lock(const Gtk::Widget& w);			/*!< добавить виджет в очередь блокировки */
	virtual void unlock_current();					/*!< разблокировать текущую вкладку и переключиться на следующую, если необходимо */

	void enable_lock(bool enable=true);				/*!< включить автопереключение вкладок */
	virtual void add_child(Gtk::Widget* w);
	virtual void remove_child(Gtk::Widget* w);

protected:

	Gtk::ScrolledWindow *scrollwin;
	Gtk::VBox *vbox;
	Gtk::HBox *hbox;

	virtual void on_add(Gtk::Widget* w);
	virtual void on_remove(Gtk::Widget* w);
	virtual void notebook_allocate_changed(Gtk::Allocation& alloc);
	virtual void on_realize();

	/*! обработчик переключения на заданную закладку.
	    \param page страница, на которую нужно переключиться.
	    \param page_num номер закладки, на которую нужно переключиться.
	*/

private:
	void ctor();
	sigc::connection lock_connection;

	ADD_PROPERTY( property_enable_lock, bool )			/*!< свойство: */
	ADD_PROPERTY( property_page, int )				/*!< свойство: */
	ADD_PROPERTY( property_orientation, Gtk::Orientation )		/*!< свойство: */

	typedef std::deque<const Gtk::Widget*> LockList;
	LockList locks_list;

	int width;
	int height;
	int current_page;

	void on_property_enable_lock_changed();
	void on_property_page_changed();
	void on_property_orientation_changed();

};
#endif
