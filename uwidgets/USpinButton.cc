#include <iostream>
#include "USpinButton.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void USpinButton::ctor()
{
	/* Widget value doesn't initialise by real sensor value */
	is_value_init = false;
}
// -------------------------------------------------------------------------
USpinButton::USpinButton() :
	Glib::ObjectBase("uspinbutton")
	,aout(this, "aout", get_connector())
{
	ctor();
}
// -------------------------------------------------------------------------
USpinButton::USpinButton(GtkmmBaseType::BaseObjectType* gobject) :
	BaseType(gobject)
	,aout(this, "aout", get_connector())
{
	ctor();
}
// -------------------------------------------------------------------------
void
USpinButton::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	BaseType::set_connector(connector);
	aout.set_connector(get_connector());
}
// -------------------------------------------------------------------------
void USpinButton::fix_value()
{
	long sensor_value;

	/* Get current sensor value */
	sensor_value = aout.get_value();

	/* Set sensor value to widget value*/
	set_value(((float)sensor_value)/pow10(get_digits()));
}
// -------------------------------------------------------------------------
void USpinButton::on_value_changed()
{
	if (get_connector() && get_connector()->connected())
		aout.save_value(lround(get_value()*pow10(get_digits())));
}
// -------------------------------------------------------------------------
void USpinButton::set_sensor_ai(const ObjectId sens_id, const ObjectId node_id)
{
	/* Set new sens_id */
	aout.set_sens_id(sens_id);

	/* Set new node_id */
	aout.set_node_id(node_id);
}
// -------------------------------------------------------------------------
bool USpinButton::on_expose_event(GdkEventExpose* event)
{
	/* TODO: Initialise widget value here because
	 * there is unable to get sensor value
	 * in the constructor and set_connector method.
	 */
	if (! is_value_init)
	{
		fix_value();
		is_value_init = true;
	}

	return Gtk::SpinButton::on_expose_event(event);
}
// -------------------------------------------------------------------------
