#ifndef _USPINBUTTON_H
#define _USPINBUTTON_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include "UDefaultFunctions.h"
#include "SensorProp.h"
// -------------------------------------------------------------------------
/*!
 * \brief Класс цифрового поля вода с кнопками увеличения(вверх/вниз).
 * \par
 * Класс предназначен для реализации виджета "цифровое поле ввода" с кнопками
 * увеличнения значения и с привязанным к виджету аналоговым датчиком, позволяя
 * задавать значение датчика увеличивая или уменьшая его значение.
*/
class USpinButton : public UDefaultFunctions<Gtk::SpinButton>
{

public:
	USpinButton();
	explicit USpinButton(GtkmmBaseType::BaseObjectType* gobject);
	~USpinButton() {};

	virtual void set_connector(const ConnectorRef& connector) throw();

	/* Set methods for widget properties */
	/*! задать аналоговый датчик для виджета.
	    \param sens_id id датчика.
	    \param node_id id узла датчика.
	*/
	void set_sensor_ai(const UniWidgetsTypes::ObjectId sens_id, const UniWidgetsTypes::ObjectId node_id);

protected:
	virtual bool on_expose_event(GdkEventExpose* event);

private:
	bool is_value_init;
	SensorProp aout;

	void ctor();
	void fix_value();

	virtual void on_value_changed();
};
#endif
