#include "types.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <gdkmm.h>
#include "UValueIndicator.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using Glib::ustring;
// -------------------------------------------------------------------------
#define UVALUEINDICATOR_INIT_PROPERTIES() \
	sensor_ai_(this,"ai", get_connector()) \
	,calibrate(this) \
	,property_value_(*this,"value",0) \
	,property_font_name_(*this,"font-name","Liberation Sans 14") \
	\
	,property_bg_transparent_(*this,"bg-transparent",false) \
	,property_font_color_(*this,"font-color",Gdk::Color("green")) \
	,property_bg_color_(*this,"bg-color",Gdk::Color("black")) \
	\
	,property_precision_(*this,"precision",0) \
	,property_digits_(*this,"digits",0) \
	,property_fill_digits_(*this,"fill-digits",0) \
	,property_hide_sign_(*this,"hide-sign",false) \
	\
	,property_hi_warning_on_(*this,"hi_warning_on",false) \
	,property_hi_warning_(*this,"hi_warning",0) \
	,property_hi_warning_color_(*this,"hi_warning_color") \
	\
	,property_hi_alarm_on_(*this,"hi_alarm_on",false) \
	,property_hi_alarm_(*this,"hi_alarm",0) \
	,property_hi_alarm_color_(*this,"hi_alarm_color") \
	\
	,property_lo_warning_on_(*this,"lo_warning_on",false) \
	,property_lo_warning_(*this,"lo_warning",0) \
	,property_lo_warning_color_(*this,"lo_warning_color") \
	\
	,property_lo_alarm_on_(*this,"lo_alarm_on",false) \
	,property_lo_alarm_(*this,"lo_alarm",0) \
	,property_lo_alarm_color_(*this,"lo_alarm_color")
// -------------------------------------------------------------------------
void UValueIndicator::ctor()
{
	set_visible_window(false);

	layout_value_ = create_pango_layout("0.0");
	Pango::FontDescription fd("Liberation Serif Bold");
	fd.set_absolute_size(13 * Pango::SCALE);
	layout_value_->set_font_description(fd);

	connect_property_changed("font-name", sigc::mem_fun(*this, &UValueIndicator::on_font_changed) );
	connect_property_changed("value", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	connect_property_changed("enable-calibrate", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	connect_property_changed("enable-calibrate-limit", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	connect_property_changed("calibrate-rmin", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	connect_property_changed("calibrate-rmax", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	connect_property_changed("calibrate-cmin", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	connect_property_changed("calibrate-cmax", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	// 	connect_property_changed("pango", sigc::mem_fun(*this, &UValueIndicator::queue_draw) );

	connect_property_changed("bg-color", sigc::mem_fun(*this, &UValueIndicator::queue_draw) );
	connect_property_changed("bg-transparent", sigc::mem_fun(*this, &UValueIndicator::queue_draw) );
	connect_property_changed("font-color", sigc::mem_fun(*this, &UValueIndicator::queue_draw) );

	connect_property_changed("precision", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	connect_property_changed("digits", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	connect_property_changed("fill-digits", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );
	connect_property_changed("hide-sign", sigc::mem_fun(*this, &UValueIndicator::on_value_changed) );

	on_value_changed();
	show_all();
}
// -------------------------------------------------------------------------
UValueIndicator::UValueIndicator() :
	Glib::ObjectBase("uvalueindicator")
	,UVALUEINDICATOR_INIT_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UValueIndicator::UValueIndicator(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,UVALUEINDICATOR_INIT_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
void UValueIndicator::on_font_changed()
{
	Pango::FontDescription fd(property_font_name_);
	layout_value_->set_font_description(fd);
}
// -------------------------------------------------------------------------
void UValueIndicator::on_value_changed()
{
	std::ostringstream title;
	float value = calibrate.get_cvalue(property_value_)/pow10(property_precision_);
	if(!get_property_hide_sign_())
		title<<(value>=0? "":"-");
	value = fabs(value);
	if(property_fill_digits_>0)
		title<<std::setw(property_fill_digits_)<<setfill('0')<<(long)value;
	else
		title<<(long)value;
	if(property_digits_>0)
		title<<","<<std::setw(property_digits_)<<setfill('0')<<(long)(fabs(((float)value-(long)value )*pow10(property_digits_)));
//	ustring value = ustring::format(std::fixed,
//			std::setprecision(property_precision_),
//			std::setw(property_digits_),
//			std::setfill(L'0'),
//			property_value_/pow10(property_precision_));
//	cout<<"!!!!!!!!!! new_value = "<<title.str()<<endl;
	layout_value_->set_text(title.str());

	queue_draw();
}
// -------------------------------------------------------------------------
bool UValueIndicator::on_expose_event(GdkEventExpose* event)
{
	Gtk::Allocation alloc = get_allocation();
	Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();

	Gdk::Cairo::rectangle(cr, alloc);
	cr->clip();

	if (!property_bg_transparent_)
	{
		Gdk::Cairo::set_source_color(cr, property_bg_color_);
		cr->paint();
	}

	Gdk::Color fc;
	     if (property_hi_alarm_on_   && property_value_ >= property_hi_alarm_)
		fc = property_hi_alarm_color_;
	else if (property_hi_warning_on_ && property_value_ >= property_hi_warning_)
		fc = property_hi_warning_color_;
	else if (property_lo_alarm_on_   && property_value_ <= property_lo_alarm_)
		fc = property_lo_alarm_color_;
	else if (property_lo_warning_on_ && property_value_ <= property_lo_warning_)
		fc = property_lo_warning_color_;
	else
		fc = property_font_color_;

	Gdk::Cairo::set_source_color(cr, fc);

	int tx, ty;
	layout_value_->get_pixel_size(tx,ty);
	const int xpos = alloc.get_x() + (alloc.get_width() -tx) / 2;
	const int ypos = alloc.get_y() + (alloc.get_height()-ty) / 2;
	cr->move_to(xpos, ypos);

	layout_value_->add_to_cairo_context(cr);
	cr->fill();

	return true;
}
// -------------------------------------------------------------------------
void UValueIndicator::set_sensor_ai(const ObjectId sens_id, const ObjectId node_id)
{
	/* Set new sens_id */
	sensor_ai_.set_sens_id(sens_id);

	/* Set new node_id */
	sensor_ai_.set_node_id(node_id);
}
// -------------------------------------------------------------------------
void UValueIndicator::set_precisions(const int precisions)
{
	property_precision_ = precisions;
}
// -------------------------------------------------------------------------
void UValueIndicator::set_digits(const int digits)
{
	property_digits_ = digits;
}
// -------------------------------------------------------------------------
void UValueIndicator::set_fill_digits(const int digits)
{
	property_fill_digits_ = digits;
}
// -------------------------------------------------------------------------
void UValueIndicator::set_font_color(const long font_color)
{
	long red;
	long green;
	long blue;

	/* Calculate red color */
	red = COLOR_RED_MASK & font_color;
	if (red > 0)
	{
		/* Decrease red color to 2 byte size - 0xFFFF */
		red = red >> BITS_OFFSET;
	}

	/* Calculate green color */
	green = COLOR_GREEN_MASK & font_color;

	/* Calculate blue color */
	blue = COLOR_BLUE_MASK & font_color;
	if (blue > 0)
	{
		/* Increase blue color to 2 byte size - 0xFFFF */
		blue = blue << BITS_OFFSET;
	}

	/* Set font color property to new value*/
	Gdk::Color color;

	color.set_rgb(red, green, blue);
	property_font_color_.set_value(color);
}
// -------------------------------------------------------------------------
void
UValueIndicator::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	UVoid::set_connector(connector);

	sensor_ai_.set_connector(connector);

	sensor_connection_.disconnect();

	if (get_connector() == true) {
		sigc::slot<void, ObjectId, ObjectId, float> process_sensor_slot =
				sigc::mem_fun(this, &UValueIndicator::process_sensor);
		sensor_connection_ = get_connector()->signals().connect_analog_value_changed(
				process_sensor_slot,
				sensor_ai_.get_sens_id(),
				sensor_ai_.get_node_id());
		on_connect();
	}
}
// -------------------------------------------------------------------------
void
UValueIndicator::on_connect() throw()
{
	UVoid::on_connect();

	process_sensor(sensor_ai_.get_sens_id(), sensor_ai_.get_node_id(), sensor_ai_.get_value() / pow10(sensor_ai_.get_info().precision));

	if(get_property_disconnect_effect() > 0)
		queue_draw();
}
// -------------------------------------------------------------------------
void UValueIndicator::on_realize()
{
	UEventBox::on_realize();
	if( get_connector() )
		process_sensor(sensor_ai_.get_sens_id(),sensor_ai_.get_node_id(),sensor_ai_.get_value() / pow10(sensor_ai_.get_info().precision));

}
// -------------------------------------------------------------------------
void
UValueIndicator::on_disconnect() throw()
{
	UVoid::on_connect();
	UVoid::on_disconnect();

	if(get_property_disconnect_effect() > 0)
		queue_draw();
}
// -------------------------------------------------------------------------
void
UValueIndicator::process_sensor(ObjectId id, ObjectId node, float value)
{
	property_value_ = value;
}
// -------------------------------------------------------------------------
