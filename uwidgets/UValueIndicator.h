#ifndef _UVALUEINDICATOR_H
#define _UVALUEINDICATOR_H
// -------------------------------------------------------------------------
#include "UEventBox.h"
#include "UCalibrate.h"
#include "global_macros.h"
// -------------------------------------------------------------------------
/* Color masks */
#define COLOR_RED_MASK		0xFF0000
#define COLOR_GREEN_MASK	0xFF00
#define COLOR_BLUE_MASK		0xFF
/* Bits offset*/
#define BITS_OFFSET		8
// -------------------------------------------------------------------------
/*!
 * \brief Класс цифрового индикатора.
 * \par
 * Класс предназначен для реализации виджета "цифровой индикатор".
*/
class UValueIndicator : public UEventBox
{
public:
	UValueIndicator();
	explicit UValueIndicator(GtkmmBaseType::BaseObjectType* gobject);
	~UValueIndicator() {}

	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void set_connector(const ConnectorRef& connector) throw();

	/* Set methods for widget properties */
	/*! задать аналоговый датчик для виджета.
	    \param sens_id id датчика.
	    \param node_id id узла датчика.
	*/
	void set_sensor_ai(const UniWidgetsTypes::ObjectId sens_id, const UniWidgetsTypes::ObjectId node_id);
	/*! задать точность после запятой отображаемого значения.
	    \param precisions заданная точность значения.
	*/
	void set_precisions(const int precisions);
	/*! задать количество цифр используемых как ширина поля отображения.
	    \param digits заданная ширина поля отображения.
	*/
	void set_digits(const int digits);
	/*! задать символ для заполнения неиспользуемых разрядов.
	    \param digits заданный заполнитель.
	*/
	void set_fill_digits(const int digits);
	/*! задать цвет текста.
	    \param font_color заданный цвет текста.
	*/
	void set_font_color(const long font_color);

protected:
	virtual void on_realize();
	virtual bool on_expose_event(GdkEventExpose*);

private:
	void ctor();
	virtual void process_sensor(UniWidgetsTypes::ObjectId, UniWidgetsTypes::ObjectId, float);

	void on_value_changed();
	void on_bg_color_changed();
	void on_text_color_changed();
	void on_align_changed();
	void on_font_changed();

	Glib::RefPtr<Pango::Layout> layout_value_;
	sigc::connection sensor_connection_;

	SensorProp sensor_ai_;
	UCalibrate calibrate;

	ADD_PROPERTY(property_value_, double);
	ADD_PROPERTY(property_font_name_, Glib::ustring);

	ADD_PROPERTY(property_precision_, int);
	ADD_PROPERTY(property_digits_, int);
	ADD_PROPERTY(property_fill_digits_, int);
	ADD_PROPERTY(property_hide_sign_, bool);


	ADD_PROPERTY(property_bg_transparent_, bool);
	ADD_PROPERTY(property_font_color_, Gdk::Color);
	ADD_PROPERTY(property_bg_color_, Gdk::Color);


	ADD_PROPERTY(property_hi_warning_on_, bool);
	ADD_PROPERTY(property_hi_warning_, float);
	ADD_PROPERTY(property_hi_warning_color_, Gdk::Color);

	ADD_PROPERTY(property_hi_alarm_on_, bool);
	ADD_PROPERTY(property_hi_alarm_, float);
	ADD_PROPERTY(property_hi_alarm_color_, Gdk::Color);

	ADD_PROPERTY(property_lo_warning_on_, bool);
	ADD_PROPERTY(property_lo_warning_, float);
	ADD_PROPERTY(property_lo_warning_color_, Gdk::Color);

	ADD_PROPERTY(property_lo_alarm_on_, bool);
	ADD_PROPERTY(property_lo_alarm_, float);
	ADD_PROPERTY(property_lo_alarm_color_, Gdk::Color);

};
#endif
