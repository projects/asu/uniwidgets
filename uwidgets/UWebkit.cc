#include <iostream>
#include "UWebkit.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
// -------------------------------------------------------------------------
void UWebkit::on_url_prop_changed()
{
	webkit_web_view_open(WEBKIT_WEB_VIEW(webview), prop_url_text.get_value().c_str());
}
// -------------------------------------------------------------------------
void UWebkit::on_delay_menu_prop_changed()
{
}
// -------------------------------------------------------------------------
void UWebkit::on_enbscrollbar_prop_changed()
{
	if(prop_enbscrollbar)
	{
		Gtk::Widget* browser = Glib::wrap(GTK_WIDGET(webview));
		Gtk::Container::remove(*browser);
		scrollwin->add(*browser);
		add(*scrollwin);
	}
	else
	{
		Gtk::Widget* browser = Glib::wrap(GTK_WIDGET(webview));
		Gtk::Container::remove(*scrollwin);
		scrollwin->Gtk::Container::remove(*browser);
		add(*browser);
	}
}
// -------------------------------------------------------------------------
void UWebkit::on_slide_px_size_prop_changed()
{
//	if(prop_slide_px_size.get_value()>0)
//		prop_slide_px_size = 0;
}
// -------------------------------------------------------------------------
void UWebkit::ctor()
{
	start_soft_move = false;
	start_history_move = false;

	scrollwin = manage(new class Gtk::ScrolledWindow());//new Gtk::ScrolledWindow;
	scrollwin->set_policy(Gtk::POLICY_NEVER ,Gtk::POLICY_NEVER);
//	scrollwin->set_border_width(1);

	webview = WEBKIT_WEB_VIEW(webkit_web_view_new());
	webkit_web_view_load_uri(WEBKIT_WEB_VIEW(webview), prop_url_text.get_value().c_str());
//	webkit_web_view_set_editable(webview,false);
//	webkit_web_view_set_highlight_text_matches(webview,false);
	Gtk::Widget* browser = Glib::wrap(GTK_WIDGET(webview));
	scrollwin->show();
	browser->show();

	//popup menu:
	{
		Gtk::Menu::MenuList menulist = web_popup_menu.items();

		menu_go_back_item = manage( new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::GO_BACK,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Назад")) );
		menu_go_back_item->signal_activate().connect(sigc::mem_fun(*this, &UWebkit::go_back));
		menu_go_back_item->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*menu_go_back_item) );

		menu_go_forward_item = manage( new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::GO_FORWARD,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Вперёд")) );
		menu_go_forward_item->signal_activate().connect(sigc::mem_fun(*this, &UWebkit::go_forward));
		menu_go_forward_item->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*menu_go_forward_item) );

		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(Glib::ustring("_Обновить"),*manage( new Gtk::Image(Gtk::Stock::REFRESH,Gtk::ICON_SIZE_SMALL_TOOLBAR) ),sigc::mem_fun(*this, &UWebkit::reload) ) );

		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(Glib::ustring("_Найти"),*manage( new Gtk::Image(Gtk::Stock::FIND,Gtk::ICON_SIZE_SMALL_TOOLBAR) ),sigc::mem_fun(*this, &UWebkit::finde_text) ) );
	}
	web_popup_menu.accelerate(*this);
	check_menu_item();

	browser->signal_event().connect(sigc::mem_fun(*this, &UWebkit::v_cross_event),true);
	browser->signal_event().connect(sigc::mem_fun(*this, &UWebkit::h_cross_event),true);
	browser->signal_event().connect(sigc::mem_fun(*this, &UWebkit::menu_button_press_event),true);
//	browser->set_events(~Gdk::ALL_EVENTS_MASK);

	connect_property_changed("url", sigc::mem_fun(*this, &UWebkit::on_url_prop_changed) );
	connect_property_changed("slide-size-in-pixel", sigc::mem_fun(*this, &UWebkit::on_slide_px_size_prop_changed) );
	connect_property_changed("enable-scrollbar", sigc::mem_fun(*this, &UWebkit::on_enbscrollbar_prop_changed) );
	connect_property_changed("menu-delay-time", sigc::mem_fun(*this, &UWebkit::on_delay_menu_prop_changed) );
	
	g_signal_connect(webview, "download-requested", G_CALLBACK(&UWebkit::download_requested), this);
}
// -------------------------------------------------------------------------
void UWebkit::download_notify_status(GObject* object, GParamSpec* pspec, UWebkit* webkit)
{
	using Glib::ustring;
	WebKitDownload* download = WEBKIT_DOWNLOAD(object);
	switch (webkit_download_get_status(download)) {
		case WEBKIT_DOWNLOAD_STATUS_FINISHED:
			cout << "UWebkit::download_notify_status() Отчет успешно сохранён"<< endl;
			break;
		case WEBKIT_DOWNLOAD_STATUS_ERROR:
			cout << "UWebkit::download_notify_status() Ошибка сохранения отчета"<< endl;
			break;
		case WEBKIT_DOWNLOAD_STATUS_CANCELLED:
// 			g_assert_not_reached();
			break;
		default:
		break;
	}
	if(webkit->post_download)
		webkit->post_download(webkit_download_get_status(download));
}
// -------------------------------------------------------------------------
gboolean UWebkit::download_requested(WebKitWebView  *webview, WebKitDownload *download, UWebkit* webkit)
{
	//webkit_download_set_destination_uri(download,g_filename_to_uri("/tmp/flash",NULL, NULL));
	string uri;
	if(webkit->get_uri)
		uri = webkit->get_uri(webkit->prop_download_path.get_value(),webkit_download_get_suggested_filename (download));
	else
		uri = webkit->prop_download_path.get_value()+"/"+webkit_download_get_suggested_filename (download);
	gchar *destination = NULL;
	destination = g_filename_to_uri(uri.c_str(),NULL, NULL);
	if(destination)
	{
		webkit_download_set_destination_uri(download,destination);
		//cout << "UWebkit::download_requested() "  << webkit_download_get_destination_uri(download) << endl;
		g_signal_connect(download, "notify::status", G_CALLBACK(download_notify_status), webkit);
		return true;
	}
	return false;
}
// -------------------------------------------------------------------------
void UWebkit::on_realize()
{
	UEventBox::on_realize();
	on_enbscrollbar_prop_changed();
	on_url_prop_changed();
	on_delay_menu_prop_changed();
	on_slide_px_size_prop_changed();
	check_menu_item();
}
// -------------------------------------------------------------------------
UWebkit::UWebkit() :
	Glib::ObjectBase("uwebkit")
	,get_uri(0)
	,post_download(0)
	,prop_url_text(*this,"url","localhost")
	,prop_enbslide(*this,"horisontal-slide",false)
	,prop_slide_px_size(*this,"slide-size-in-pixel",0)
	,prop_enbscrollbar(*this,"enable-scrollbar",false)
	,prop_delay_menu(*this,"menu-delay-time",2000)
	,prop_download_path(*this,"download-path","/tmp")
{
	ctor();
}
// -------------------------------------------------------------------------
UWebkit::UWebkit(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,get_uri(0)
	,post_download(0)
	,prop_url_text(*this,"url","localhost")
	,prop_enbslide(*this,"horisontal-slide",false)
	,prop_slide_px_size(*this,"slide-size-in-pixel",0)
	,prop_enbscrollbar(*this,"enable-scrollbar",false)
	,prop_delay_menu(*this,"menu-delay-time",2000)
	,prop_download_path(*this,"download-path","/tmp")
{
	ctor();
}
// -------------------------------------------------------------------------
UWebkit::~UWebkit()
{
}
// -------------------------------------------------------------------------
bool UWebkit::v_cross_event(GdkEvent *event)
{
	GdkEventButton event_btn = event->button;
//	cout<<"v_cross_event: event->type="<<event->type<< std::endl;

	if((event->type==GDK_MOTION_NOTIFY) && prop_enbscrollbar && start_soft_move)
	{
		double value = scrollwin->get_vadjustment()->get_value();

		int p_x,p_y;
		Gdk::ModifierType mtype;
		get_window()->get_pointer(p_x,p_y,mtype);
//		cout<<": softmove... p_x="<<p_x<<" p_y="<<p_y<<" mtype="<<mtype<<endl;
		double softmove_step = abs(p_y - current_y_position);
//		cout<<": softmove... softmove_step="<<softmove_step<<endl;

		if((double)p_y < current_y_position)
		{
			long_button_press_timer.disconnect();
			double upper_lim = scrollwin->get_vadjustment()->get_upper() - scrollwin->get_height();
			if((value + softmove_step) < upper_lim)
				scrollwin->get_vadjustment()->set_value(value + softmove_step);
			else
				scrollwin->get_vadjustment()->set_value(upper_lim);

			current_y_position = (double)p_y;
		}
		else if((double)p_y > current_y_position)
		{
			long_button_press_timer.disconnect();
			double lower_lim = scrollwin->get_vadjustment()->get_lower();
			if((value - softmove_step) > lower_lim)
				scrollwin->get_vadjustment()->set_value(value - softmove_step);
			else
				scrollwin->get_vadjustment()->set_value(lower_lim);

			current_y_position = (double)p_y;
		}
	}
	else if((event_btn.type == GDK_BUTTON_PRESS && event_btn.button == 1))
	{
		int p_x,p_y;
		Gdk::ModifierType mtype;
		get_window()->get_pointer(p_x,p_y,mtype);
		current_y_position = (double)p_y;
		start_soft_move = true;
	}
	else if((event->type==GDK_BUTTON_RELEASE))
	{
		start_soft_move = false;
	}
	return false;
}
// -------------------------------------------------------------------------
bool UWebkit::h_cross_event(GdkEvent *event)
{
	GdkEventButton event_btn = event->button;
//	cout<<"h_cross_event: event->type="<<event->type<< std::endl;

	if((event->type==GDK_MOTION_NOTIFY) && prop_enbslide && prop_slide_px_size.get_value()>0 && start_history_move)
	{
		int p_x,p_y;
		Gdk::ModifierType mtype;
		get_window()->get_pointer(p_x,p_y,mtype);
//		cout<<": GDK_CROSSING_GTK_GRAB... p_x="<<p_x<<" p_y="<<p_y<<" mtype="<<mtype<<endl;
		int diff = abs(p_x - current_x_position);
//		cout<<": GDK_CROSSING_GTK_GRAB... diff="<<diff<<endl;

		if(p_x < current_x_position && prop_slide_px_size.get_value() < diff)
		{
			long_button_press_timer.disconnect();
			go_forward();
			start_history_move = false;
		}
		else if(p_x > current_x_position && prop_slide_px_size.get_value() < diff)
		{
			long_button_press_timer.disconnect();
			go_back();
			start_history_move = false;
		}
	}
	else if((event_btn.type == GDK_BUTTON_PRESS && event_btn.button == 1))
	{
		int p_x,p_y;
		Gdk::ModifierType mtype;
		get_window()->get_pointer(p_x,p_y,mtype);
		current_x_position = p_x;
		start_history_move = true;
	}
	else if((event->type==GDK_BUTTON_RELEASE))
	{
		start_history_move = false;
	}
	return false;
}
// -------------------------------------------------------------------------
bool UWebkit::menu_button_press_event(GdkEvent *event)
{
	GdkEventButton event_btn = event->button;
//	cout<<"v_cross_event: event->type="<<event->type<< std::endl;

	if((event_btn.type == Gdk::BUTTON_PRESS && event_btn.button == 1))
	{
		sigc::slot<bool> time_slot = sigc::bind(sigc::mem_fun(*this, &UWebkit::show_popup_menu),event_btn);
		if(prop_delay_menu>0)
			long_button_press_timer = Glib::signal_timeout().connect(time_slot,prop_delay_menu);
//		cout<<"long_button_press_event: start timer at time="<<prop_delay_menu<< std::endl;
	}
	else if(event->type==Gdk::DOUBLE_BUTTON_PRESS && event_btn.button == 1)
	{
		long_button_press_timer.disconnect();
		show_popup_menu(event_btn);
	}
	else if((event->type==Gdk::BUTTON_RELEASE))
	{
		long_button_press_timer.disconnect();
	}
	return false;
}
// -------------------------------------------------------------------------
void UWebkit::check_menu_item()
{
	if(webkit_web_view_can_go_back(webview))
	{
		menu_go_back_item->set_sensitive(true);
	}
	else
	{
		menu_go_back_item->set_sensitive(false);
	}
	if(webkit_web_view_can_go_forward(webview))
	{
		menu_go_forward_item->set_sensitive(true);
	}
	else
	{
		menu_go_forward_item->set_sensitive(false);
	}
}
// -------------------------------------------------------------------------
bool UWebkit::show_popup_menu(GdkEventButton event_btn)
{
	start_soft_move = false;
	start_history_move = false;
	check_menu_item();
	web_popup_menu.popup(event_btn.button, event_btn.time);
	return false;
}
// -------------------------------------------------------------------------
void UWebkit::go_forward()
{
	if(webkit_web_view_can_go_forward(webview))
		webkit_web_view_go_forward(webview);
}
// -------------------------------------------------------------------------
void UWebkit::go_back()
{
	if(webkit_web_view_can_go_back(webview))
		webkit_web_view_go_back(webview);
}
// -------------------------------------------------------------------------
void UWebkit::reload()
{
	webkit_web_view_reload(webview);
}
// -------------------------------------------------------------------------
void UWebkit::finde_text()
{
}
// -------------------------------------------------------------------------
void UWebkit::init_widget()
{
//	on_scrollbar_prop_changed();
}
// -------------------------------------------------------------------------
