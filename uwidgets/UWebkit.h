#ifndef _UWEBKIT_H
#define _UWEBKIT_H
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <gdkmm.h>
#include <webkit/webkit.h>
#include <uwidgets/UEventBox.h>
#include <global_macros.h>
// -------------------------------------------------------------------------
/*!
 * \brief Класс Веб-браузера.
 * \par
 * Класс представляет собой простое окно Веб-браузера. URL задается через свойство
 * виджета "UWebkit::prop_url_text".
*/
class UWebkit : public UEventBox{
public:	
	typedef std::string(*GetUriFunction)(const std::string path,const std::string file);
	inline void connect_get_uri(GetUriFunction func){get_uri = func;};
	typedef void(*PostDownloadFunction)(WebKitDownloadStatus status);
	inline void connect_post_download(PostDownloadFunction func){post_download = func;};
private:
	GetUriFunction get_uri;
	PostDownloadFunction post_download;
	WebKitWebView* webview;
	Gtk::ScrolledWindow *scrollwin;
	void ctor();
	static gboolean download_requested(WebKitWebView  *web_view, WebKitDownload *download, UWebkit* webkit);
	static void download_notify_status(GObject* object, GParamSpec* pspec, UWebkit* webkit);
	
public:
	UWebkit();
	explicit UWebkit(GtkmmBaseType::BaseObjectType* gobject);
	~UWebkit();
	virtual void init_widget();
	/*! обработчик сигнала signal_realize(), выполняется в конце создания виджета перед отображением */
	virtual void on_realize();

protected:
	/*! обработчик события изменения URL-адреса */
	virtual void on_url_prop_changed();
//	virtual void on_enbslide_prop_changed();
	/*! обработчик события изменения prop_slide_px_size */
	virtual void on_slide_px_size_prop_changed();
	/*! обработчик события изменения prop_enbscrollbar */
	virtual void on_enbscrollbar_prop_changed();
	/*! обработчик события изменения prop_delay_menu */
	virtual void on_delay_menu_prop_changed();
	/*! обработчик события горизонтальной прокрутки */
	virtual bool h_cross_event(GdkEvent *event);
	/*! обработчик события вертикальной прокрутки */
	virtual bool v_cross_event(GdkEvent *event);
	/*! обработчик нажатия кнопок мыши в области виджета */
	virtual bool menu_button_press_event(GdkEvent *event);

	bool start_soft_move;				/*!< начало прокрутки вертикальной */
	bool start_history_move;			/*!< начало прокрутки горизонтальной */
	int current_x_position;				/*!< текущая x-позиция курсора */
	double current_y_position;			/*!< текущая y-позиция курсора */

	Gtk::Menu web_popup_menu;			/*!< контекстное меню */
	Gtk::ImageMenuItem* menu_go_back_item;		/*!< строка контекстного меню "назад" */
	Gtk::ImageMenuItem* menu_go_forward_item;	/*!< строка контекстного меню "вперед" */
	sigc::connection long_button_press_timer;	/*!< коннектор к сигналу долгого нажатия кнопки мыши */
	bool show_popup_menu(GdkEventButton event_btn);	/*!< обработчик вызова контекстного меню */

	void check_menu_item();				/*!< обработчик чувствительности пунктов контекстного меню к нажатиям */
	void go_forward();				/*!< прокрутка вперед(контекстное меню) */
	void go_back();					/*!< прокрутка назад(контекстное меню) */
	void reload();					/*!< обновить страницу(контекстное меню) */
	void finde_text();				/*!< найти текст(контекстное меню) */
private:

	ADD_PROPERTY( prop_url_text, Glib::ustring )	/*!< свойство: url адрес */
	ADD_PROPERTY( prop_enbslide, bool )		/*!< свойство: показывать и обрабатывать горизонтальный скроллбар */
	ADD_PROPERTY( prop_slide_px_size, int )		/*!< свойство: минимальное дистанция перемещения курсора для прокрутки(если курсор переместился на меньшее или равное растояние, то прокрутка не обработается) */
	ADD_PROPERTY( prop_enbscrollbar, bool )		/*!< свойство: показывать и обрабатывать вертикальный скроллбар */
	ADD_PROPERTY( prop_delay_menu, int )		/*!< свойство: время задержки перед появлением меню по клику */
	ADD_PROPERTY( prop_download_path, Glib::ustring )	/*!< свойство: путь сохранения */
};
#endif
