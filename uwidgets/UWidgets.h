#ifndef _UWIDGETS_H
#define _UWIDGETS_H

#include <uwidgets/UAPSJournal.h>
#include <uwidgets/UButton.h>
#include <uwidgets/UContainer.h>
#include <uwidgets/UEventBox.h>
#include <uwidgets/UPopupBox.h>
#ifdef HAVE_WEBKIT
#include <uwidgets/UWebkit.h>
#endif
#include <uwidgets/UDiagram.h>
#include <uwidgets/UniOscillograph.h>
#include <uwidgets/UniOscilChannel.h>
#include <uwidgets/UniOscilConfig.h>
#include <uwidgets/UIndicator.h>
#include <uwidgets/UIndicatorContainer.h>
#include <uwidgets/UJournal.h>
//#include <uwidgets/USensorJournal.h>
#include <uwidgets/ULockNotebook.h>
#include <uwidgets/USlideNotebook.h>
#include <uwidgets/UParamPopup.h>
#include <uwidgets/UProxyWidget.h>
#include <uwidgets/USpinButton.h>
#include <uwidgets/UValueIndicator.h>
#include <uwidgets/URadialValueIndicator.h>
#include <uwidgets/URadialBar.h>
#include <uwidgets/UScrollValue.h>
#include <uwidgets/UScrollScale.h>
#include <uwidgets/UPrinterInterface.h>
#include <uwidgets/UImitatorLamp.h>
#include <uwidgets/UDKeyboard.h>

#endif
