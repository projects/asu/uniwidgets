#include <iostream>
#include <gtkmm.h>
#include "UniOscillograph.h"
#include "ConfirmSignal.h"

using namespace std;
using namespace UniWidgetsTypes;

#define USVGINDICATORPLUS_PROPERTY_INIT() \
	prop_visible(*this, "visible-draw", true) \
	,sensor_prop_ai_(this,"ai",get_connector()) \
	,prop_precision(*this, "precision", 0) \
	,prop_value(*this, "value", 0.0) \
	,prop_channelname(*this, "chanel-name", "Channel") \
	,prop_channelcolor(*this, "chanel-color", Gdk::Color("black") ) \
	,prop_fill(*this, "use-fill" , false) \
	,prop_gradient(*this, "use-gradient-color" , false) \
	,prop_gradientcolor(*this, "gradient-color", Gdk::Color("black") ) \
	,prop_gradientTransparent(*this, "gradient-transparent", 0.0 ) \
	,prop_randcolor(*this, "random-color" , true) \
	,prop_enableGroupOfOscils(*this, "enable-group-of-oscils" , true) \
	,prop_groupOfOscils(*this, "group-of-oscils" , "") \
	,prop_channelGroupName(*this, "chanel-group-name" , "") \
	,current_value(0) \
	,last_value(0)

void UniOscilChannel::ctor()
{
	++gen_id;
	own_number = gen_id;
	cout<<get_name()<<"::ctor() cannel id="<<own_number<<endl;
	set_visible_window(false);
	connect_property_changed("value", sigc::mem_fun(*this, &UniOscilChannel::on_value_changed) );
	connect_property_changed("gradient-transparent", sigc::mem_fun(*this, &UniOscilChannel::on_gradient_transparent_changed) );
	connect_property_changed("group-of-oscils", sigc::mem_fun(*this, &UniOscilChannel::on_group_of_oscils_changed) );
	
	//popup menu:
	menu_popup = manage( new Gtk::Menu() );
	menulist = menu_popup->items();
	menu_popup->accelerate(*this);
	signal_event().connect(sigc::mem_fun(*this, &UniOscilChannel::on_area_event));
}
// -------------------------------------------------------------------------
UniOscilChannel::UniOscilChannel() :
	Glib::ObjectBase("unioscilchannel")
	,USVGINDICATORPLUS_PROPERTY_INIT()
{
	ctor();
}
// -------------------------------------------------------------------------
UniOscilChannel::UniOscilChannel(GtkmmBaseType::BaseObjectType* gobject) :
	UEventBox(gobject)
	,USVGINDICATORPLUS_PROPERTY_INIT()
{
	ctor();
}
// -------------------------------------------------------------------------
bool UniOscilChannel::on_expose_event(GdkEventExpose* event)
{
	bool rv = UEventBox::on_expose_event(event);
	return rv;
}
// -------------------------------------------------------------------------
UniOscilChannel::~UniOscilChannel()
{
}
// -------------------------------------------------------------------------
void UniOscilChannel::init_widget()
{
}
// -------------------------------------------------------------------------
void UniOscilChannel::on_realize()
{
	UEventBox::on_realize();
	if(get_connector())
		process_sensor(sensor_prop_ai_.get_sens_id(),sensor_prop_ai_.get_node_id(),sensor_prop_ai_.get_value());
}
// -------------------------------------------------------------------------
void UniOscilChannel::to_lo_erarchy(Gtk::Widget* w)
{
	Glib::ListHandle<Gtk::Widget*> childlist = (dynamic_cast<Gtk::Container*>(w))->get_children();
	for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); ++it)
	{
		if (UniOscillograph* oscil = dynamic_cast<UniOscillograph*>(*it))
		{
//			cout<<get_name()<<"::to_lo_erarchy  add oscil="<<oscil->get_name()<<endl;
			if( prop_enableGroupOfOscils && !get_prop_groupOfOscils().empty() && oscils_group.find(oscil->get_prop_oscilGroupName())==oscils_group.end() )
				continue;
			Gtk::ImageMenuItem* mi_add_to_oscil;
			UniOscillograph::ChannelsList::iterator ch_it = oscil->get_channels()->find(own_number);
			if(ch_it == oscil->get_channels()->end())
			{
				mi_add_to_oscil = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::ADD,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("добавить на '"+oscil->get_name()+"'")));
				mi_add_to_oscil->signal_activate().connect(bind(sigc::mem_fun(*oscil, &UniOscillograph::add_channel),this));
			}
			else
			{
				mi_add_to_oscil = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::DELETE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("удалить из '"+oscil->get_name()+"'")));
				mi_add_to_oscil->signal_activate().connect(bind(sigc::mem_fun(*oscil, &UniOscillograph::remove_channel),this));
			}
			mi_add_to_oscil->show();
			menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_add_to_oscil) );
			
			oscil_list[oscil] = mi_add_to_oscil;
		}
		else if(Gtk::Container* cont = dynamic_cast<Gtk::Container*>(*it))
			to_lo_erarchy(cont);
	}
}
// -------------------------------------------------------------------------
bool UniOscilChannel::on_area_event(GdkEvent* ev)
{
	//	if(ev->type!=GDK_EXPOSE)
	//		cout<<get_name()<<"::on_area_event event->type="<<ev->type<< endl;
	GdkEventButton event_btn = ev->button;
	
	if(event_btn.type == GDK_2BUTTON_PRESS)
	{
		if(!oscil_list.empty())
			oscil_list.clear();
		menulist = menu_popup->items();
		menulist.clear();
		to_lo_erarchy( get_toplevel() );
		
		for(OscillographsList::iterator oscil_it = oscil_list.begin(); oscil_it != oscil_list.end(); ++oscil_it)
		{
			UniOscillograph::ChannelsList::iterator ch_it = oscil_it->first->get_channels()->find(own_number);
			if(ch_it == oscil_it->first->get_channels()->end())
			{
				oscil_it->second->set_image(*manage( new Gtk::Image(Gtk::Stock::ADD,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
				oscil_it->second->set_label(Glib::ustring("добавить на '"+oscil_it->first->get_name()+"'"));
				oscil_it->second->signal_activate().connect(bind(sigc::mem_fun(*(oscil_it->first), &UniOscillograph::add_channel),this));
			}
			else
			{
				oscil_it->second->set_image(*manage( new Gtk::Image(Gtk::Stock::DELETE,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
				oscil_it->second->set_label(Glib::ustring("удалить из '"+oscil_it->first->get_name()+"'"));
				oscil_it->second->signal_activate().connect(bind(sigc::mem_fun(*(oscil_it->first), &UniOscillograph::remove_channel),this));
			}
		}
		menu_popup->popup(event_btn.button, event_btn.time);
	}

	return false;
}
// -------------------------------------------------------------------------
void UniOscilChannel::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	if (connector == true)
	{
		UEventBox::set_connector(connector);
		sensor_connection_.disconnect();
		sigc::slot<void, ObjectId, ObjectId, long> process_sensor_slot =
				sigc::mem_fun(this, &UniOscilChannel::process_sensor);
		sensor_connection_ = get_connector()->signals().connect_value_changed(
				process_sensor_slot,
				sensor_prop_ai_.get_sens_id(),
				sensor_prop_ai_.get_node_id());
		on_connect();
	}
}
// -------------------------------------------------------------------------
void UniOscilChannel::on_connect() throw()
{
	UEventBox::on_connect();

	long value = get_connector()->get_value(sensor_prop_ai_.get_sens_id(), sensor_prop_ai_.get_node_id());

	process_sensor(sensor_prop_ai_.get_sens_id(), sensor_prop_ai_.get_node_id(), value);
}
// -------------------------------------------------------------------------
void UniOscilChannel::on_disconnect() throw()
{
	UEventBox::on_disconnect();
}
// -------------------------------------------------------------------------
void UniOscilChannel::process_sensor(ObjectId id, ObjectId node, long value)
{
	if(id == sensor_prop_ai_.get_sens_id() && node == sensor_prop_ai_.get_node_id())
	{
		prop_value.set_value(value);
	}
}
// -------------------------------------------------------------------------
void UniOscilChannel::on_value_changed()
{
	last_value=current_value;
	current_value=prop_value.get_value();
//	cout<<get_name()<<"::process_sensor current_value="<<current_value<<" fvalue="<<get_fvalue()<<" last_value="<<last_value<<" last_fvalue="<<get_last_fvalue()<<endl;
}
// -------------------------------------------------------------------------
void UniOscilChannel::on_gradient_transparent_changed()
{
	if(prop_gradientTransparent.get_value()>1.0)
		prop_gradientTransparent.set_value(1.0);
	else if(prop_gradientTransparent.get_value()<0.0)
		prop_gradientTransparent.set_value(0.0);
}
// -------------------------------------------------------------------------
void UniOscilChannel::on_group_of_oscils_changed()
{
	oscils_group.clear();
	std::list<Glib::ustring> oscils_group_list = explodestr(get_prop_groupOfOscils(),',');
	for(std::list<Glib::ustring>::iterator oscil_it = oscils_group_list.begin(); oscil_it != oscils_group_list.end(); ++oscil_it)
		add_oscils_group(*oscil_it);
}
// -------------------------------------------------------------------------
