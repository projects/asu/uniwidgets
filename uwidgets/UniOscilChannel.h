#ifndef _UNIOSCILCHANNEL_H
#define _UNIOSCILCHANNEL_H

#include <unordered_map>
#include <gdkmm.h>
#include <sigc++/sigc++.h>
#include "ConfirmSignal.h"
#include "USignals.h"
#include "UEventBox.h"
#include "global_macros.h"

class UniOscillograph;

static long gen_id=0;

class UniOscilChannel : public UEventBox{
public:
	UniOscilChannel();
	explicit UniOscilChannel(GtkmmBaseType::BaseObjectType* gobject);
	~UniOscilChannel();

	typedef std::unordered_map<std::string,bool> OscilsGroupMap;

	inline OscilsGroupMap* get_oscils_group(){ return &oscils_group;};
	inline void add_oscils_group(std::string group){ oscils_group[group] = true;};
	
	typedef std::map<UniOscillograph*,Gtk::ImageMenuItem*> OscillographsList;
	
	SensorProp* get_ai() { return &sensor_prop_ai_; }
	inline void set_value(double value) { set_prop_value(value); }
	inline long get_value() { return current_value; }
	inline long get_last_value() { return last_value; }
	inline float get_fvalue() { return ((float)current_value)/pow10(prop_precision); }
	inline float get_last_fvalue() { return ((float)last_value)/pow10(prop_precision); }
	inline void equate_value() { last_value = current_value; }
	inline Glib::ustring get_channelname() { return prop_channelname.get_value(); }
	inline Gdk::Color get_channelcolor() { return prop_channelcolor.get_value(); }
	inline void set_channelcolor(Gdk::Color color) { prop_channelcolor.set_value(color); }
	inline long get_channel_id() { return own_number; }
	inline void set_channel_id(long id) { own_number=id; }
	inline OscillographsList* get_oscil_list() { return &oscil_list; }
	
	Glib::PropertyProxy<Glib::ustring> property_channelname();
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_channelname() const;
	Glib::PropertyProxy<Glib::ustring> property_channelcolor();
	Glib::PropertyProxy_ReadOnly<Glib::ustring> property_channelcolor() const;
	Glib::PropertyProxy<bool> property_randcolor();
	Glib::PropertyProxy_ReadOnly<bool> property_randcolor() const;

	virtual void init_widget();

	virtual void set_connector(const ConnectorRef& connector) throw();
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void on_realize();

protected:
	virtual bool on_expose_event(GdkEventExpose*);
	void process_sensor(UniWidgetsTypes::ObjectId,UniWidgetsTypes::ObjectId,long);
	
	Gtk::Menu* menu_popup;	/*!< контекстное меню */
	Gtk::Menu::MenuList menulist;
	virtual bool on_area_event(GdkEvent* ev);
	
	void to_lo_erarchy(Gtk::Widget* w);
	
	OscilsGroupMap oscils_group;
	
private:
	void ctor();
	void on_value_changed();
	void on_gradient_transparent_changed();
	void on_group_of_oscils_changed();
	
	USignals::Connection sensor_connection_;

	long current_value;
	long last_value;

	long own_number;
	
	SensorProp sensor_prop_ai_;
	ADD_PROPERTY( prop_visible, bool )					/*!< свойство: */
	ADD_PROPERTY( prop_precision, int )					/*!< свойство: */
	ADD_PROPERTY( prop_value, double )					/*!< свойство: */
	ADD_PROPERTY( prop_channelname, Glib::ustring )		/*!< свойство: */
	ADD_PROPERTY( prop_channelcolor, Gdk::Color )		/*!< свойство: */
	ADD_PROPERTY( prop_fill, bool )						/*!< свойство: */
	ADD_PROPERTY( prop_gradient, bool )					/*!< свойство: */
	ADD_PROPERTY( prop_gradientcolor, Gdk::Color )		/*!< свойство: */
	ADD_PROPERTY( prop_gradientTransparent, double )	/*!< свойство: */
	ADD_PROPERTY( prop_randcolor, bool )				/*!< свойство: */
	ADD_PROPERTY( prop_enableGroupOfOscils, bool )		/*!< свойство: */
	ADD_PROPERTY( prop_groupOfOscils, Glib::ustring)	/*!< свойство: */
	ADD_PROPERTY( prop_channelGroupName, Glib::ustring)	/*!< свойство: */
	
	DISALLOW_COPY_AND_ASSIGN(UniOscilChannel);
	
	OscillographsList oscil_list;
};
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<Glib::ustring> UniOscilChannel::property_channelname()
{
	return prop_channelname.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<Glib::ustring> UniOscilChannel::property_channelname() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"chanel-name");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<Glib::ustring> UniOscilChannel::property_channelcolor()
{
	return prop_channelname.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<Glib::ustring> UniOscilChannel::property_channelcolor() const
{
	return Glib::PropertyProxy_ReadOnly<Glib::ustring>(this,"chanel-color");
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy<bool> UniOscilChannel::property_randcolor()
{
	return prop_randcolor.get_proxy();
}
// -------------------------------------------------------------------------
inline Glib::PropertyProxy_ReadOnly<bool> UniOscilChannel::property_randcolor() const
{
	return Glib::PropertyProxy_ReadOnly<bool>(this,"random-color");
}
// -------------------------------------------------------------------------
#endif
