#include <iostream>
#include <gtkmm.h>
#include "UniOscilConfig.h"
#include "UniOscillograph.h"
#include "UniOscilChannel.h"
#include <libxml/xinclude.h>

using namespace std;
using namespace UniWidgetsTypes;

#define UNIOSCILCONFIG_PROPERTY_INIT() \
prop_readonly(*this, "readonly", true) \
,prop_confFileName(*this, "conf-file", "") \
,prop_confSectionName(*this, "conf-section-name", "") \
,prop_filterField(*this, "filter-field", "oscil_ch") \
,prop_filterValue(*this, "filter-value", "oscil_ch") \
,prop_enableGroupOfOscils(*this, "enable-group-of-oscils" , true) \
,prop_groupOfOscils(*this, "group-of-oscils" , "") \
,prop_channelGroupName(*this, "chanel-group-name" , "") \
,parent(nullptr) \

void UniOscilConfig::ctor()
{
	set_visible_window(false);
	connect_property_changed("group-of-oscils", sigc::mem_fun(*this, &UniOscilConfig::on_group_of_oscils_changed) );

	connect_property_changed("conf-file", sigc::mem_fun(*this, &UniOscilConfig::changed_conf) );
	connect_property_changed("conf-section-name", sigc::mem_fun(*this, &UniOscilConfig::changed_conf) );
	connect_property_changed("filter-field", sigc::mem_fun(*this, &UniOscilConfig::changed_conf) );
	connect_property_changed("filter-value", sigc::mem_fun(*this, &UniOscilConfig::changed_conf) );
	
	//popup menu:
	menu_popup = manage( new Gtk::Menu() );
	menulist = menu_popup->items();
	menu_popup->accelerate(*this);
	signal_event().connect(sigc::mem_fun(*this, &UniOscilConfig::on_area_event));

	signal_parent_changed().connect(sigc::mem_fun(*this, &UniOscilConfig::on_add_to_new_parent));
}
// -------------------------------------------------------------------------
UniOscilConfig::UniOscilConfig() :
Glib::ObjectBase("unioscilconfig")
,UNIOSCILCONFIG_PROPERTY_INIT()
{
	ctor();
}
// -------------------------------------------------------------------------
UniOscilConfig::UniOscilConfig(GtkmmBaseType::BaseObjectType* gobject) :
UEventBox(gobject)
,UNIOSCILCONFIG_PROPERTY_INIT()
{
	ctor();
}
// -------------------------------------------------------------------------
bool UniOscilConfig::on_expose_event(GdkEventExpose* event)
{
	bool rv = UEventBox::on_expose_event(event);
	return rv;
}
// -------------------------------------------------------------------------
UniOscilConfig::~UniOscilConfig()
{
}
// -------------------------------------------------------------------------
void UniOscilConfig::init_widget()
{
}
// -------------------------------------------------------------------------
void UniOscilConfig::on_realize()
{
	UEventBox::on_realize();
//	changed_conf();
}
// -------------------------------------------------------------------------
void UniOscilConfig::to_lo_erarchy(Gtk::Widget* w)
{
	Glib::ListHandle<Gtk::Widget*> childlist = (dynamic_cast<Gtk::Container*>(w))->get_children();
	for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); ++it)
	{
		if (UniOscillograph* oscil = dynamic_cast<UniOscillograph*>(*it))
		{
			//			cout<<get_name()<<"::to_lo_erarchy  add oscil="<<oscil->get_name()<<endl;
			if( prop_enableGroupOfOscils && !get_prop_groupOfOscils().empty() && oscils_group.find(oscil->get_prop_oscilGroupName())==oscils_group.end() )
				continue;
/*
			Gtk::ImageMenuItem* mi_add_to_oscil;
			UniOscillograph::ChannelsList::iterator ch_it = oscil->get_channels()->find(own_number);
			if(ch_it == oscil->get_channels()->end())
			{
				mi_add_to_oscil = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::ADD,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("добавить на '"+oscil->get_name()+"'")));
				mi_add_to_oscil->signal_activate().connect(bind(sigc::mem_fun(*oscil, &UniOscillograph::add_channel),this));
			}
			else
			{
				mi_add_to_oscil = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::DELETE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("удалить из '"+oscil->get_name()+"'")));
				mi_add_to_oscil->signal_activate().connect(bind(sigc::mem_fun(*oscil, &UniOscillograph::remove_channel),this));
			}
			mi_add_to_oscil->show();
			menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_add_to_oscil) );
			
			oscil_list[oscil] = mi_add_to_oscil;
*/
		}
		else if(Gtk::Container* cont = dynamic_cast<Gtk::Container*>(*it))
			to_lo_erarchy(cont);
	}
}
// -------------------------------------------------------------------------
bool UniOscilConfig::on_area_event(GdkEvent* ev)
{
	//	if(ev->type!=GDK_EXPOSE)
	//		cout<<get_name()<<"::on_area_event event->type="<<ev->type<< endl;
	GdkEventButton event_btn = ev->button;
	
	if(event_btn.type == GDK_2BUTTON_PRESS)
	{
		if(!oscil_list.empty())
			oscil_list.clear();
		menulist = menu_popup->items();
		menulist.clear();
		to_lo_erarchy( get_toplevel() );
		
		for(OscillographsList::iterator oscil_it = oscil_list.begin(); oscil_it != oscil_list.end(); ++oscil_it)
		{
/*
			UniOscillograph::ChannelsList::iterator ch_it = oscil_it->first->get_channels()->find(own_number);
			if(ch_it == oscil_it->first->get_channels()->end())
			{
				oscil_it->second->set_image(*manage( new Gtk::Image(Gtk::Stock::ADD,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
				oscil_it->second->set_label(Glib::ustring("добавить на '"+oscil_it->first->get_name()+"'"));
				oscil_it->second->signal_activate().connect(bind(sigc::mem_fun(*(oscil_it->first), &UniOscillograph::add_channel),this));
			}
			else
			{
				oscil_it->second->set_image(*manage( new Gtk::Image(Gtk::Stock::DELETE,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
				oscil_it->second->set_label(Glib::ustring("удалить из '"+oscil_it->first->get_name()+"'"));
				oscil_it->second->signal_activate().connect(bind(sigc::mem_fun(*(oscil_it->first), &UniOscillograph::remove_channel),this));
			}
*/
		}
		menu_popup->popup(event_btn.button, event_btn.time);
	}
	
	return false;
}
// -------------------------------------------------------------------------
void UniOscilConfig::on_add_to_new_parent(Gtk::Widget* prev_parent)
{
	cout<<get_name()<<"::on_add_to_new_parent get_parent()->name="<<get_parent()->get_name()<< endl;
	on_remove_from_parent();

	if(dynamic_cast<UniOscillograph*>(get_parent()))
	{
		parent = dynamic_cast<UniOscillograph*>(get_parent());
		load_xml();
		on_add_to_parent();
	}
}
// -------------------------------------------------------------------------
void UniOscilConfig::on_add_to_parent()
{
	if(parent)
	{
		cout<<get_name()<<"::on_add_to_parent parent->name="<<parent->get_name()<< endl;
		for(UniOscilConfig::ChannelsList::iterator ch_it = channel_list.begin(); ch_it != channel_list.end(); ++ch_it)
		{
			parent->add_channel(*ch_it);
		}
	}
}
// -------------------------------------------------------------------------
void UniOscilConfig::on_remove_from_parent()
{
	if(parent)
	{
		cout<<get_name()<<"::on_remove_from_parent parent->name="<<parent->get_name()<< endl;
		for(UniOscilConfig::ChannelsList::iterator ch_it = channel_list.begin(); ch_it != channel_list.end(); ++ch_it)
		{
			parent->remove_channel(*ch_it);
		}
		channel_list.clear();
	}
}
// -------------------------------------------------------------------------
void UniOscilConfig::changed_conf()
{
	cout<<get_name()<<"::changed_conf()"<< endl;
	on_remove_from_parent();
	load_xml();
	on_add_to_parent();
}
// -------------------------------------------------------------------------
UniOscilConfig::ChannelsList* UniOscilConfig::load_xml()
{
	channel_list.clear();
	
	xmlNode* it = nullptr;
	xmlDoc* doc = nullptr;
	if(get_prop_confFileName().empty() && !is_glade_editor)
	{
		assert( get_connector() );
		
		if(get_prop_confSectionName().empty())
			it = get_connector()->get_xml_node("sensors");
		else
			it = get_connector()->get_xml_node(get_prop_confSectionName());
	}
	else
	{
		try
		{
			Glib::Dir subdir(get_prop_confFileName());
		}
		catch( Glib::FileError& ex )
		{
			if(get_prop_confFileName().empty() || !UniWidgetsTypes::file_exist(get_prop_confFileName()))
			{
				cout<<get_name()<<"::load_xml  not find file '"<<get_prop_confFileName()<<"'"<<endl;
			}
			else
			{
				xmlKeepBlanksDefault(0);
				doc = xmlParseFile(get_prop_confFileName().c_str());
				if(doc)
				{
					xmlXIncludeProcess(doc);
					it = UniWidgetsTypes::xmlFindNode(xmlDocGetRootElement(doc), get_prop_confSectionName());
				}
			}
		}
	}

	if ( it && (it = it->children) )
	{
		for ( ; it; it = it->next )
		{
			if( UniWidgetsTypes::xmlGetProp(it, get_prop_filterField()) != get_prop_filterValue() )
				continue;
			
			UniOscilChannel* ch = manage(new UniOscilChannel());
			channel_list.push_back(ch);
			/*
			guint n=0;
			GParamSpec** list = g_object_class_list_properties(G_OBJECT_GET_CLASS(G_OBJECT(ch->gobj())),&n);
			for(guint i = 0; i < n; i++)
			{
				cout<<get_name()<<"::load_xml  add oscil="<<ch->get_name()<<" "<<list[i]->name<<endl;
			}
			g_free(list);
		*/
			cout<<get_name()<<"::load_xml  add '"<<UniWidgetsTypes::xmlGetProp(it, "chanel_name")<<"'"<<endl;
			if(!UniWidgetsTypes::xmlGetProp(it, "visible_draw").empty())
				ch->set_prop_visible((bool)atoi(UniWidgetsTypes::xmlGetProp(it, "visible_draw").c_str()));
			if(!UniWidgetsTypes::xmlGetProp(it, "ai_sensor_name").empty())
				ch->get_ai()->set_sens_name(UniWidgetsTypes::xmlGetProp(it, "ai_sensor_name"));
			if(!UniWidgetsTypes::xmlGetProp(it, "ai_node_name").empty())
				ch->get_ai()->set_node_name(UniWidgetsTypes::xmlGetProp(it, "ai_node_name"));
			if(!UniWidgetsTypes::xmlGetProp(it, "ai_sensor_id").empty())
				ch->get_ai()->set_sens_id(UniWidgetsTypes::xmlGetProp(it, "ai_sensor_id"));
			if(!UniWidgetsTypes::xmlGetProp(it, "ai_node_id").empty())
				ch->get_ai()->set_node_id(UniWidgetsTypes::xmlGetProp(it, "ai_node_id"));
			if(!UniWidgetsTypes::xmlGetProp(it, "ai_stype").empty())
				ch->get_ai()->set_stype(UniWidgetsTypes::xmlGetProp(it, "ai_stype"));
			if(!UniWidgetsTypes::xmlGetProp(it, "precision").empty())
				ch->set_prop_precision(atoi(UniWidgetsTypes::xmlGetProp(it, "precision").c_str()));
			if(!UniWidgetsTypes::xmlGetProp(it, "value").empty())
				ch->set_prop_value(atoi(UniWidgetsTypes::xmlGetProp(it, "value").c_str()));
			if(!UniWidgetsTypes::xmlGetProp(it, "chanel_name").empty())
				ch->set_prop_channelname(UniWidgetsTypes::xmlGetProp(it, "chanel_name"));
			if(!UniWidgetsTypes::xmlGetProp(it, "chanel_color").empty())
				ch->set_prop_channelcolor(Gdk::Color(UniWidgetsTypes::xmlGetProp(it, "chanel_color")));
			if(!UniWidgetsTypes::xmlGetProp(it, "use_gradient_color").empty())
				ch->set_prop_gradient((bool)atoi(UniWidgetsTypes::xmlGetProp(it, "use_gradient_color").c_str()));
			if(!UniWidgetsTypes::xmlGetProp(it, "gradient_color").empty())
				ch->set_prop_gradientcolor(Gdk::Color(UniWidgetsTypes::xmlGetProp(it, "gradient_color")));
			if(!UniWidgetsTypes::xmlGetProp(it, "gradient_transparent").empty())
				ch->set_prop_gradientTransparent(atof(UniWidgetsTypes::xmlGetProp(it, "gradient_transparent").c_str()));
			if(!UniWidgetsTypes::xmlGetProp(it, "random_color").empty())
				ch->set_prop_randcolor((bool)atoi(UniWidgetsTypes::xmlGetProp(it, "random_color").c_str()));
			if(!UniWidgetsTypes::xmlGetProp(it, "enable_group_of_oscils").empty())
				ch->set_prop_enableGroupOfOscils((bool)atoi(UniWidgetsTypes::xmlGetProp(it, "enable_group_of_oscils").c_str()));
			if(!UniWidgetsTypes::xmlGetProp(it, "group_of_oscils").empty())
				ch->set_prop_groupOfOscils(UniWidgetsTypes::xmlGetProp(it, "group_of_oscils"));
			if(!UniWidgetsTypes::xmlGetProp(it, "chanel_group_name").empty())
				ch->set_prop_channelGroupName(UniWidgetsTypes::xmlGetProp(it, "chanel_group_name"));
		}
	}
	
	return &channel_list;
}
// -------------------------------------------------------------------------
void UniOscilConfig::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;
	
	if (connector == true)
	{
		UEventBox::set_connector(connector);

		for(UniOscilConfig::ChannelsList::iterator ch_it = channel_list.begin(); ch_it != channel_list.end(); ++ch_it)
		{
			(*ch_it)->set_connector(connector);
		}
		on_connect();
	}
}
// -------------------------------------------------------------------------
void UniOscilConfig::on_connect() throw()
{
	UEventBox::on_connect();
	
	for(UniOscilConfig::ChannelsList::iterator ch_it = channel_list.begin(); ch_it != channel_list.end(); ++ch_it)
	{
		(*ch_it)->on_connect();
	}
}
// -------------------------------------------------------------------------
void UniOscilConfig::on_disconnect() throw()
{
	UEventBox::on_disconnect();

	for(UniOscilConfig::ChannelsList::iterator ch_it = channel_list.begin(); ch_it != channel_list.end(); ++ch_it)
	{
		(*ch_it)->on_disconnect();
	}
}
// -------------------------------------------------------------------------
void UniOscilConfig::on_group_of_oscils_changed()
{
	oscils_group.clear();
	std::list<Glib::ustring> oscils_group_list = explodestr(get_prop_groupOfOscils(),',');
	for(std::list<Glib::ustring>::iterator oscil_it = oscils_group_list.begin(); oscil_it != oscils_group_list.end(); ++oscil_it)
		add_oscils_group(*oscil_it);
}
// -------------------------------------------------------------------------
