#ifndef _UNIOSCILCONFIGL_H
#define _UNIOSCILCONFIGL_H

#include <unordered_map>
#include <gdkmm.h>
#include "UEventBox.h"
#include "global_macros.h"

class UniOscillograph;
class UniOscilChannel;

class UniOscilConfig : public UEventBox{
public:
	UniOscilConfig();
	explicit UniOscilConfig(GtkmmBaseType::BaseObjectType* gobject);
	~UniOscilConfig();
	
	typedef std::unordered_map<std::string,bool> OscilsGroupMap;
	inline OscilsGroupMap* get_oscils_group(){ return &oscils_group;};
	inline void add_oscils_group(std::string group){ oscils_group[group] = true;};
	
	typedef std::map<UniOscillograph*,Gtk::ImageMenuItem*> OscillographsList;
	inline OscillographsList* get_oscil_list() { return &oscil_list; }
	
	typedef std::list<UniOscilChannel*> ChannelsList;
	inline ChannelsList* get_channels_list() { return &channel_list; }
	ChannelsList* load_xml();
	
	virtual void init_widget();
	
	virtual void set_connector(const ConnectorRef& connector) throw();
	virtual void on_connect() throw();
	virtual void on_disconnect() throw();
	virtual void on_realize();
	
protected:
	virtual bool on_expose_event(GdkEventExpose*);
	
	Gtk::Menu* menu_popup;	/*!< контекстное меню */
	Gtk::Menu::MenuList menulist;
	virtual bool on_area_event(GdkEvent* ev);
	
	void to_lo_erarchy(Gtk::Widget* w);
	
	OscilsGroupMap oscils_group;

	UniOscillograph* parent;
	virtual void on_add_to_new_parent(Gtk::Widget* prev_parent);
	virtual void on_add_to_parent();
	virtual void on_remove_from_parent();
	virtual void changed_conf();
	
private:
	void ctor();
	void on_group_of_oscils_changed();
	
	ADD_PROPERTY( prop_readonly, bool )					/*!< свойство: */
	ADD_PROPERTY( prop_confFileName, Glib::ustring )	/*!< свойство: */
	ADD_PROPERTY( prop_confSectionName, Glib::ustring )	/*!< свойство: */
	ADD_PROPERTY( prop_filterField, Glib::ustring)		/*!< свойство: */
	ADD_PROPERTY( prop_filterValue, Glib::ustring)		/*!< свойство: */
	ADD_PROPERTY( prop_enableGroupOfOscils, bool )		/*!< свойство: */
	ADD_PROPERTY( prop_groupOfOscils, Glib::ustring)	/*!< свойство: */
	ADD_PROPERTY( prop_channelGroupName, Glib::ustring)	/*!< свойство: */
	
	DISALLOW_COPY_AND_ASSIGN(UniOscilConfig);
	
	OscillographsList oscil_list;
	ChannelsList channel_list;
};
// -------------------------------------------------------------------------
#endif
