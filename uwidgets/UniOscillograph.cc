#include <gdkmm.h>
#include <iostream>
#include <fstream>
#include <errno.h>
#include <iomanip>
#include "UniOscillograph.h"
#include "ConfirmSignal.h"
#include "UniOscilChannel.h"
#include "UniOscilConfig.h"

// -------------------------------------------------------------------------
using namespace std;
using namespace UniWidgetsTypes;
using UMessages::MessageId;
// -------------------------------------------------------------------------
#define INIT_UniOscillograph_PROPERTIES() \
	 prop_refresh_timeout(*this, "refresh-timeout" , 0) \
	 ,prop_enableTimeLine(*this, "enable-time-line" , true) \
	 ,prop_timeLineColor(*this, "time-line-color" , Gdk::Color("black")) \
	 ,prop_enableWeb(*this, "enable-web" , true) \
	 ,prop_webMajorColor(*this, "web-major-color" , Gdk::Color("grey")) \
	 ,prop_enableWebMinor(*this, "enable-web-minor" , true) \
	 ,prop_webMinorColor(*this, "web-minor-color" , Gdk::Color("grey")) \
	 ,prop_WebLineType(*this, "web-line-type" , Web::LINE_LINE) \
	 ,prop_scaleColor(*this, "scale-color" , Gdk::Color("black")) \
	 ,prop_scaleLabelColor(*this, "scale-label-color", Gdk::Color("black") ) \
	 ,prop_scaleLabelFont(*this, "scale-label-font", "Liberation Sans Bold" ) \
	 ,prop_scaleLabelFontSize(*this, "scale-label-font-size", 12 ) \
	 ,prop_topScale(*this, "top-scale", true) \
	 ,prop_bottomScale(*this, "bottom-scale", true) \
	 ,prop_leftScale(*this, "left-scale", true) \
	 ,prop_rightScale(*this, "right-scale", true) \
	 ,prop_enableScaleRealTime(*this, "scale-label-real-time", true) \
	 ,prop_XRange(*this, "scale-x-range" , 100) \
	 ,prop_XStepsInMemory(*this, "scale-x-steps-in-memory" , 100) \
	 ,prop_XStepsNum(*this, "scale-x-timeout-steps-num" , 5) \
	 ,prop_YMin(*this, "scale-y-min" , 0) \
	 ,prop_YMax(*this, "scale-y-max" , 100) \
	 ,prop_YDynamicRange(*this, "scale-y-dynamic-range" , false) \
	 ,prop_plot_type(*this, "plot-type" , LINEAR) \
	 ,prop_diagram_column_w(*this, "diagram-column-w" , 0.5) \
	 ,prop_line_width(*this, "line-width" , 1) \
	 ,prop_smoothCurve(*this, "smooth-curve" , true) \
	 ,prop_enableDynamicScaleResolution(*this, "dynamic-scale-resolution" , true) \
	 ,prop_XmaxMin(*this, "max-x-scale-minor" , 10) \
	 ,prop_XmaxMaj(*this, "max-x-scale-major" , 10) \
	 ,prop_YmaxMin(*this, "max-y-scale-minor" , 10) \
	 ,prop_YmaxMaj(*this, "max-y-scale-major" , 10) \
	 ,prop_BGImagePath(*this, "bg-image") \
	 ,prop_expand_type(*this, "expand-type" , PERENT_FIXED) \
	 ,prop_expand_parent(*this, "expand-widget" , nullptr) \
	 ,prop_expandBGImagePath(*this, "expand-bg-image") \
	 ,prop_bgBorderWidth(*this, "bg-border-width", 0 ) \
	 ,prop_enableTooltip(*this, "enable-tooltip", false) \
	 ,prop_enableTooltipGravity(*this, "tooltip-gravity-to-channels", true) \
	 ,prop_tooltipImagePath(*this, "tooltip-image") \
	 ,prop_tooltipImageWidth(*this, "tooltip-image-width", 10 ) \
	 ,prop_tooltipImageHeight(*this, "tooltip-image-height", 10) \
	 ,prop_enableTooltipRealTime(*this, "tooltip-label-real-time", true) \
	 ,prop_tooltipLabelPrecision(*this, "tooltip-label-precision", 0) \
	 ,prop_tooltipLabelColor(*this, "tooltip-label-color", Gdk::Color("black") ) \
	 ,prop_tooltipLabelFont(*this, "tooltip-label-font", "Liberation Sans Bold" ) \
	 ,prop_tooltipLabelFontSize(*this, "tooltip-label-font-size", 12 ) \
	 ,prop_enableHisory(*this, "enable-history", false) \
	 ,prop_enablePastHisory(*this, "enable-past-history", false) \
	 ,prop_dir(*this, "history-dir", "/tmp/" ) \
	 ,prop_historyPointInMem(*this, "history-point-in-memory", 100 ) \
	 ,prop_enableLabelList(*this, "enable-channels-list", false) \
	 ,prop_addInMenu(*this, "show-add-ch-in-popmenu", true) \
	 ,prop_removeInMenu(*this, "show-remove-ch-in-popmenu", true) \
	 ,prop_enableGroupOfChannels(*this, "enable-group-of-channels", true) \
	 ,prop_groupOfAddChannels(*this, "group-of-channels", "") \
	 ,prop_groupByGroupName(*this, "group-by-group-name", true) \
	 ,prop_oscilGroupName(*this, "oscil-group-name", "") \
	 ,prop_LabelListInMenu(*this, "show-channels-list-in-popmenu", true) \
	 ,prop_LabelListBGImagePath(*this, channelsListBGImage.property_image0_path(), "channels-list-bg-image", "") \
	 ,prop_labelListWidth(*this, "channels-list-width", 100 ) \
	 ,prop_labelListBorderWidth(*this, "channels-list-border-width", 1 ) \
	 ,prop_labelListFrame(*this, "channels-list-frame-type", Gtk::SHADOW_IN ) \
	 ,prop_labelListFont(*this, "channels-list-label-font", "Liberation Sans Bold" ) \
	 ,prop_labelListFontSize(*this, "channels-list-label-font-size", 12 ) \
	 ,point_x(0) \
	 ,step_x(0) \
	 ,min_x(0) \
	 ,max_x(100) \
	 ,min_y(0) \
	 ,max_y(100) \
	 ,XmaxMin(0) \
	 ,XmaxMaj(0) \
	 ,YmaxMin(0) \
	 ,YmaxMaj(0) \
	 ,range_y(100) \
	 ,ch_num(0) \
	 ,current_plot_width(0) \
	 ,current_plot_height(0) \
	 ,plot_width(1) \
	 ,plot_height(1) \
	 ,widget_width(0) \
	 ,widget_height(0) \
	 ,current_width(0) \
	 ,current_height(0) \
	 ,show_tooltip(false) \
	 ,softmove_ch_list_pos_x(0) \
	 ,softmove_ch_list_pos_y(0) \
	 ,softmove_ch_list_step_x(0) \
	 ,softmove_ch_list_step_y(0) \
	 ,softmove_ch_list_start_x(0) \
	 ,softmove_ch_list_start_y(0) \
	 ,softmove_ch_list_start(false) \
	 ,scale_change_start_x(0) \
	 ,scale_change_start_y(0) \
	 ,scale_change_start(false) \
	 ,view_range_change_start(false) \
	 ,view_range_lock(false) \
	 ,history_file_name("") \
	 ,history_file_fullname("") \
	 ,save_in_progress(false) \
	 ,force_history_flag(false) \
	 ,save_channel_num(0) \
	 ,is_show(false) \
	 ,is_expand(false) \
	 ,is_realized(false) \
	 ,WindowChild(0) \
	 ,mainwindow(0) \
	 ,parent(0) \
	 ,expand_parent_widget_name("") \
	 ,block_hide_dialog_by_out_focus(false) \
	 
// -------------------------------------------------------------------------
void UniOscillograph::ctor()
{
	tooltip_layout= create_pango_layout(" ");
	tooltip_layout->set_alignment(Pango::ALIGN_CENTER);
	channels_layout= create_pango_layout(" ");
	channels_layout->set_alignment(Pango::ALIGN_LEFT);
	
	BGImage = manage(new Gtk::Image(get_pixbuf_from_cache("", 1, 1)));
	BGImage->show();
	Gtk::Fixed::on_add(BGImage);

	expandBGImage = manage(new Gtk::Image(get_pixbuf_from_cache("", 1, 1)));
	expandBGImage->hide();
	Gtk::Fixed::on_add(expandBGImage);

	push_composite_child();
	{
		mainbox = manage(new Gtk::HBox());
		
		hpaned = manage(new Gtk::HPaned());
		mainbox->pack_start(*hpaned,Gtk::PACK_SHRINK);

		Gtk::Fixed::on_add(mainbox);

		channelsListScrollwin = manage(new Gtk::EventBox());
		channelsListScrollwin->set_visible_window(false);
		channelsListScrollwin->set_size_request(-1,-1);
		channelsListScrollwin->set_events(~Gdk::ALL_EVENTS_MASK);
		channelsListScrollwin->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::scroll_channels_list_event));

		channelsListBox = manage(new Gtk::Frame());
		channelsListBox->set_border_width(1);
		channelsListBox->add(*channelsListScrollwin);
		channelsListBGImage.set_property_disconnect_effect(0);
		channelsListBGImage.add(*channelsListBox);

		hpaned->add1(channelsListBGImage);

		table = manage(new Gtk::Table(3,3));
		hpaned->add2(*table);

		plot_area = manage(new Web());
		plot_area->set_enabled(true);
		plot_area->set_h_range(0,100);
		plot_area->set_v_range(0,100);
		table->attach(*plot_area,1,2,1,2);

		topScaleBox = manage(new Gtk::VBox());
		table->attach(*topScaleBox,1,2,0,1,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);

		HScaleLabels *topScaleLabel = manage(new HScaleLabels(Gtk::POS_TOP));
		topScaleLabel->set_name("topScaleLabel");
		topScale = manage(new HScale(Gtk::POS_TOP,topScaleLabel));
		topScale->set_enabled(true);
		topScale->set_range(0,100);
		topScaleBox->pack_start(*topScaleLabel,Gtk::PACK_SHRINK);
		topScaleBox->pack_start(*topScale,Gtk::PACK_SHRINK);
		topScaleLabel->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_h_scale_event));
		topScale->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_h_scale_event));
		topLabel = static_cast<HScaleLabels*>(topScale->get_labels());

		bottomScaleBox = manage(new Gtk::VBox());
		table->attach(*bottomScaleBox,1,2,2,3,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);

		HScaleLabels *bottomScaleLabel = manage(new HScaleLabels(Gtk::POS_BOTTOM));
		bottomScaleLabel->set_name("bottomScaleLabel");
		bottomScale = manage(new HScale(Gtk::POS_BOTTOM,bottomScaleLabel));
		bottomScale->set_enabled(true);
		bottomScale->set_range(0,100);
		bottomScaleBox->pack_start(*bottomScale,Gtk::PACK_SHRINK);
		bottomScaleBox->pack_start(*bottomScaleLabel,Gtk::PACK_SHRINK);
		bottomScaleLabel->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_h_scale_event));
		bottomScale->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_h_scale_event));
		bottomLabel = static_cast<HScaleLabels*>(bottomScale->get_labels());

		leftScaleBox = manage(new Gtk::HBox());
		VScaleLabels *leftScaleLabel = manage(new VScaleLabels(Gtk::POS_LEFT));
		leftScale = manage(new VScale(Gtk::POS_LEFT,leftScaleLabel));
		leftScale->set_enabled(true);
		leftScale->set_range(0,100);
		leftScaleBox->pack_start(*leftScaleLabel,Gtk::PACK_SHRINK);
		leftScaleBox->pack_start(*leftScale,Gtk::PACK_SHRINK);
		leftScaleLabel->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_v_scale_event));
		leftScale->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_v_scale_event));
		table->attach(*leftScaleBox,0,1,1,2,Gtk::SHRINK,Gtk::FILL|Gtk::EXPAND);

		rightScaleBox = manage(new Gtk::HBox());
		VScaleLabels *rightScaleLabel = manage(new VScaleLabels(Gtk::POS_RIGHT));
		rightScale = manage(new VScale(Gtk::POS_RIGHT,rightScaleLabel));
		rightScale->set_enabled(true);
		rightScale->set_range(0,100);
		rightScaleBox->pack_start(*rightScale,Gtk::PACK_SHRINK);
		rightScaleBox->pack_start(*rightScaleLabel,Gtk::PACK_SHRINK);
		rightScale->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_v_scale_event));
		rightScaleLabel->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_v_scale_event));
		table->attach(*rightScaleBox,2,3,1,2,Gtk::SHRINK,Gtk::FILL|Gtk::EXPAND);
		
		Gtk::Alignment *alignment = manage(new Gtk::Alignment(0.5,0.5,0,0));
//		alignment->set_padding(0.5,0.5,0,0);
		saveProgress = manage(new Gtk::ProgressBar());
		saveProgress->set_fraction(0);
		alignment->add(*saveProgress);
		plot_area->add(*alignment);
		
		show_all();
	}
	pop_composite_child();
	saveProgress->hide();
	
	bufferWindow = manage(new Gtk::Window());
	
	cs_dialog = manage(new Gtk::ColorSelectionDialog("Выбор цвета канала..."));
//	cs_dialog->set_position(Gtk::WIN_POS_MOUSE);
//	cs_dialog->set_position(Gtk::WIN_POS_CENTER);
//	cs_dialog->set_position(Gtk::WIN_POS_NONE);
	cs_dialog->set_decorated(false);
//	cs_dialog->set_modal(true);
	cs_dialog->signal_focus_out_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_dialog_focus_out_event));
	
	set_value_dialog = manage(new Gtk::Dialog("Выбор диапазона шкалы по Y..."));
//	set_value_dialog->set_position(Gtk::WIN_POS_MOUSE);
//	set_value_dialog->set_position(Gtk::WIN_POS_CENTER);
//	set_value_dialog->set_position(Gtk::WIN_POS_NONE);
	set_value_dialog->set_decorated(false);
//	set_value_dialog->set_modal(true);
	set_value_dialog->signal_focus_out_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_dialog_focus_out_event));
	set_value_dialog->add_button(Gtk::Stock::OK,Gtk::RESPONSE_OK);
	set_value_dialog->add_button(Gtk::Stock::CANCEL,Gtk::RESPONSE_CANCEL);
	Gtk::VBox *set_value_dialog_vbox = set_value_dialog->get_vbox();
	val1_label = manage(new Gtk::Label("шкала по Y от"));
	val1_label->show();
	val2_label = manage(new Gtk::Label("шкала по Y до"));
	val2_label->show();
	val1_spinbutton = manage(new Gtk::SpinButton(1.0));
	val1_spinbutton->set_range(-DBL_MAX,DBL_MAX);
	val1_spinbutton->show();
	val2_spinbutton = manage(new Gtk::SpinButton(1.0));
	val2_spinbutton->set_range(-DBL_MAX,DBL_MAX);
	val2_spinbutton->show();
	Gtk::Table *set_value_table = manage(new Gtk::Table(2,2));
	set_value_table->attach(*val1_label,0,1,0,1,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);
	set_value_table->attach(*val2_label,0,1,1,2,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);
	set_value_table->attach(*val1_spinbutton,1,2,0,1,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);
	set_value_table->attach(*val2_spinbutton,1,2,1,2,Gtk::FILL|Gtk::EXPAND,Gtk::SHRINK);
	set_value_table->show();
	set_value_dialog_vbox->pack_start(*set_value_table);
	
	//popup menu:
	menu_popup = manage( new Gtk::Menu() );
	{
		Gtk::Menu::MenuList& menulist = menu_popup->items();

		mi_set_lock_view = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::MEDIA_PAUSE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Зафиксировать просмотр")));
		mi_set_lock_view->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::switch_lock_view));
		mi_set_lock_view->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_lock_view) );
		
		mi_set_expand = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::FULLSCREEN,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Развернуть")));
		mi_set_expand->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::oscil_expand));
		mi_set_expand->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_expand) );

		mi_set_dynamic_range = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::ZOOM_100,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Динамическая шкала по Y")));
		mi_set_dynamic_range->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::set_dynamic_range));
		mi_set_dynamic_range->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_dynamic_range) );

		mi_set_Y_range = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::PAGE_SETUP,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Поменять диапазон шкалы по Y")));
		mi_set_Y_range->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::set_Y_range));
		mi_set_Y_range->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_Y_range) );
		
		mi_set_X_range = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::PAGE_SETUP,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Поменять диапазон шкалы по X")));
		mi_set_X_range->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::set_X_range));
		mi_set_X_range->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_X_range) );
		
		mi_set_web = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::PAGE_SETUP,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Показать сетку")));
		mi_set_web->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::switch_oscil_web));
		mi_set_web->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_web) );

		mi_set_time_on_scale = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::PAGE_SETUP,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Отображение времени по X")));
		mi_set_time_on_scale->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::switch_time_on_scale));
		mi_set_time_on_scale->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_time_on_scale) );

		mi_set_time_on_tooltip = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::PREFERENCES,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Отображения времени на всплывающей подсказке")));
		mi_set_time_on_tooltip->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::switch_time_on_tooltip));
		mi_set_time_on_tooltip->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_time_on_tooltip) );
		
		mi_set_tooltip_gravity = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::PREFERENCES,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Притяжение всплывающей подсказки к графикам")));
		mi_set_tooltip_gravity->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::switch_tooltip_gravity));
		mi_set_tooltip_gravity->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_tooltip_gravity) );
		
		mi_set_refresh_timeout = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::PREFERENCES,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Изменить время опроса каналов")));
		mi_set_refresh_timeout->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::set_refresh_timeout));
		mi_set_refresh_timeout->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_refresh_timeout) );
		
		mi_set_ch_list = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::APPLY,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Показать список каналов")));
		mi_set_ch_list->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::switch_ch_list));
		mi_set_ch_list->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_ch_list) );
		
		mi_set_line_width = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::PREFERENCES,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Изменить толщину линии графиков")));
		mi_set_line_width->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::set_line_width));
		mi_set_line_width->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_line_width) );
		
		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );
		
		mi_set_past_hisory = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::DND_MULTIPLE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Сохранение из файлов прошлой истории")));
		mi_set_past_hisory->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::switch_past_hisory));
		mi_set_past_hisory->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_set_past_hisory) );
		
		mi_save_all_channels = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::SAVE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Сохранить все каналы в отдельные файлы CSV")));
		mi_save_all_channels->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::save_all_channels));
		mi_save_all_channels->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_save_all_channels) );

		mi_save_all_in_one = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::SAVE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Сохранить все каналы в один файл CSV")));
		mi_save_all_in_one->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::save_all_channels_in_one_file));
		mi_save_all_in_one->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*mi_save_all_in_one) );
	}
	menu_popup->accelerate(*plot_area);

	//popup channel menu:
	ch_menu_popup = manage( new Gtk::Menu() );
	{
		Gtk::Menu::MenuList& menulist = ch_menu_popup->items();

		ch_mi_visible_channel = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::MEDIA_PLAY,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Показать канал")));
		ch_mi_visible_channel->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::set_channel_visible));
		ch_mi_visible_channel->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_visible_channel) );

		ch_mi_visible_only_one_channel = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::ABOUT,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Показать канал, а остальные скрыть")));
		ch_mi_visible_only_one_channel->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::set_channel_visible_only));
		ch_mi_visible_only_one_channel->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_visible_only_one_channel) );

		ch_mi_visible_all_channel = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::APPLY,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Показать все каналы")));
		ch_mi_visible_all_channel->signal_activate().connect(bind(sigc::mem_fun(*this, &UniOscillograph::set_all_channel_visible),true));
		ch_mi_visible_all_channel->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_visible_all_channel) );

		ch_mi_hide_all_channel = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::CANCEL,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Скрыть все каналы")));
		ch_mi_hide_all_channel->signal_activate().connect(bind(sigc::mem_fun(*this, &UniOscillograph::set_all_channel_visible),false));
		ch_mi_hide_all_channel->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_hide_all_channel) );

		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );

		ch_mi_save_channel = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::SAVE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Сохранить в файл CSV канал")));
		ch_mi_save_channel->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::save_channel));
		ch_mi_save_channel->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_save_channel) );

		ch_mi_save_all_channels = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::SAVE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Сохранить все каналы в отдельные файлы CSV")));
		ch_mi_save_all_channels->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::save_all_channels));
		ch_mi_save_all_channels->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_save_all_channels) );

		ch_mi_save_all_in_one = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::SAVE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Сохранить все каналы в один файл CSV")));
		ch_mi_save_all_in_one->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::save_all_channels_in_one_file));
		ch_mi_save_all_in_one->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_save_all_in_one) );

		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );

		ch_mi_set_channel_color = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::COLOR_PICKER,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Выбрать цвет канала")));
		ch_mi_set_channel_color->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::set_channel_color));
		ch_mi_set_channel_color->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_set_channel_color) );

		ch_mi_set_channel_fill_color = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::SELECT_COLOR,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Включить заливку канала цветом")));
		ch_mi_set_channel_fill_color->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::set_channel_fill_color));
		ch_mi_set_channel_fill_color->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_set_channel_fill_color) );
		
		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );
		
		//popup add channel menu:
		ch_add_menu_popup = manage( new Gtk::Menu() );
		ch_mi_add_channels = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::ADD,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Добавить канал")));
		ch_mi_add_channels->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::chek_add_channel_list));
		ch_mi_add_channels->show();
		ch_mi_add_channels->set_submenu(*ch_add_menu_popup);
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_add_channels) );
		ch_add_menu_popup->accelerate(*ch_menu_popup);
		
		ch_mi_del_channel = manage(new Gtk::ImageMenuItem(*manage( new Gtk::Image(Gtk::Stock::DELETE,Gtk::ICON_SIZE_SMALL_TOOLBAR)), Glib::ustring("Удалить канал")));
		ch_mi_del_channel->signal_activate().connect(sigc::mem_fun(*this, &UniOscillograph::del_channel));
		ch_mi_del_channel->show();
		menulist.push_back( Gtk::Menu_Helpers::ImageMenuElem(*ch_mi_del_channel) );
	}
	ch_menu_popup->accelerate(*channelsListScrollwin);

	signal_size_allocate().connect(sigc::mem_fun(*this, &UniOscillograph::oscil_allocate_changed));

	connect_property_changed("enable-web", sigc::mem_fun(*this, &UniOscillograph::on_web_enable_changed) );
	connect_property_changed("enable-web-minor", sigc::mem_fun(*this, &UniOscillograph::on_web_minor_enable_changed) );
	connect_property_changed("web-major-color", sigc::mem_fun(*this, &UniOscillograph::on_web_major_color_changed) );
	connect_property_changed("web-minor-color", sigc::mem_fun(*this, &UniOscillograph::on_web_minor_color_changed) );
	connect_property_changed("web-line-type", sigc::mem_fun(*this, &UniOscillograph::on_web_line_type_changed) );
	connect_property_changed("top-scale", sigc::mem_fun(*this, &UniOscillograph::on_scales_enable_changed) );
	connect_property_changed("bottom-scale", sigc::mem_fun(*this, &UniOscillograph::on_scales_enable_changed) );
	connect_property_changed("left-scale", sigc::mem_fun(*this, &UniOscillograph::on_scales_enable_changed) );
	connect_property_changed("right-scale", sigc::mem_fun(*this, &UniOscillograph::on_scales_enable_changed) );
	connect_property_changed("scale-label-real-time", sigc::mem_fun(*this, &UniOscillograph::on_scales_enable_real_time_changed) );
	connect_property_changed("scale-x-range", sigc::mem_fun(*this, &UniOscillograph::on_scales_x_range_changed) );
	connect_property_changed("scale-x-timeout-steps-num", sigc::mem_fun(*this, &UniOscillograph::on_scales_x_step_changed) );
	connect_property_changed("scale-y-min", sigc::mem_fun(*this, &UniOscillograph::on_scales_y_range_changed) );
	connect_property_changed("scale-y-max", sigc::mem_fun(*this, &UniOscillograph::on_scales_y_range_changed) );
	connect_property_changed("scale-y-dynamic-range", sigc::mem_fun(*this, &UniOscillograph::on_scales_y_range_changed) );
	connect_property_changed("scale-color", sigc::mem_fun(*this, &UniOscillograph::on_scale_color_changed) );
	connect_property_changed("scale-label-color", sigc::mem_fun(*this, &UniOscillograph::on_font_color_changed) );
	connect_property_changed("scale-label-font", sigc::mem_fun(*this, &UniOscillograph::on_font_changed) );
	connect_property_changed("scale-label-font-size", sigc::mem_fun(*this, &UniOscillograph::on_font_changed) );
	connect_property_changed("dynamic-scale-resolution", sigc::mem_fun(*this, &UniOscillograph::on_dynamic_scale_resolution_changed) );
	connect_property_changed("max-y-scale-minor", sigc::mem_fun(*this, &UniOscillograph::on_v_max_minor_changed) );
	connect_property_changed("max-y-scale-major", sigc::mem_fun(*this, &UniOscillograph::on_v_max_major_changed) );
	connect_property_changed("max-x-scale-minor", sigc::mem_fun(*this, &UniOscillograph::on_h_max_minor_changed) );
	connect_property_changed("max-x-scale-major", sigc::mem_fun(*this, &UniOscillograph::on_h_max_major_changed) );
	connect_property_changed("refresh-timeout", sigc::mem_fun(*this, &UniOscillograph::on_refresh_timeout_changed) );
	connect_property_changed("bg-image", sigc::mem_fun(*this, &UniOscillograph::on_bg_image_changed) );
	connect_property_changed("expand-bg-image", sigc::mem_fun(*this, &UniOscillograph::on_expand_bg_image_changed) );
	connect_property_changed("bg-border-width", sigc::mem_fun(*this, &UniOscillograph::on_bg_border_width_changed) );
	connect_property_changed("tooltip-image", sigc::mem_fun(*this, &UniOscillograph::on_tooltip_image_changed) );
	connect_property_changed("tooltip-image-width", sigc::mem_fun(*this, &UniOscillograph::on_tooltip_image_changed) );
	connect_property_changed("tooltip-image-height", sigc::mem_fun(*this, &UniOscillograph::on_tooltip_image_changed) );
	connect_property_changed("tooltip-label-font", sigc::mem_fun(*this, &UniOscillograph::on_tooltip_font_changed) );
	connect_property_changed("tooltip-label-font-size", sigc::mem_fun(*this, &UniOscillograph::on_tooltip_font_changed) );
	connect_property_changed("enable-history", sigc::mem_fun(*this, &UniOscillograph::on_history_enable_changed) );
	connect_property_changed("history-dir", sigc::mem_fun(*this, &UniOscillograph::on_history_dir_changed) );
	connect_property_changed("enable-channels-list", sigc::mem_fun(*this, &UniOscillograph::on_channels_list_width_changed) );
	connect_property_changed("channels-list-bg-image", sigc::mem_fun(*this, &UniOscillograph::on_list_bg_image_changed) );
	connect_property_changed("channels-list-width", sigc::mem_fun(*this, &UniOscillograph::on_channels_list_width_changed) );
	connect_property_changed("channels-list-border-width", sigc::mem_fun(*this, &UniOscillograph::on_channels_list_border_width_changed) );
	connect_property_changed("channels-list-frame-type", sigc::mem_fun(*this, &UniOscillograph::on_channels_list_frame_changed) );
	connect_property_changed("channels-list-label-font", sigc::mem_fun(*this, &UniOscillograph::on_channels_list_font_changed) );
	connect_property_changed("channels-list-label-font-size", sigc::mem_fun(*this, &UniOscillograph::on_channels_list_font_changed) );
	connect_property_changed("group-of-channels", sigc::mem_fun(*this, &UniOscillograph::on_group_of_channels_changed) );
	
	plot_area->set_events(~Gdk::ALL_EVENTS_MASK);
	plot_area->signal_event().connect(sigc::mem_fun(*this, &UniOscillograph::on_plot_area_event));
}
// -------------------------------------------------------------------------
UniOscillograph::UniOscillograph() :
		Glib::ObjectBase("unioscillograph")
		,INIT_UniOscillograph_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UniOscillograph::UniOscillograph(GtkmmBaseType::BaseObjectType* gobject) :
		BaseType(gobject)
		,INIT_UniOscillograph_PROPERTIES()
{
	ctor();
}
// -------------------------------------------------------------------------
UniOscillograph::~UniOscillograph()
{
}
// -------------------------------------------------------------------------
GType plottype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { UniOscillograph::LINEAR, "LINEAR", "Линейный график" },{ UniOscillograph::DIAGRAM, "DIAGRAM", "Диграммы" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("PlotType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UniOscillograph::PlotType>::value_type()
{
	return plottype_get_type();
}
// -------------------------------------------------------------------------
GType expandtype_get_type()
{
	static GType etype = 0;
	if (G_UNLIKELY(etype == 0))
	{
		static const GEnumValue values[] = { { UniOscillograph::PERENT_FIXED, "PERENT_FIXED", "В родительском Gtk::Fixed" },{ UniOscillograph::MAIN_WINDOW, "MAIN_WINDOW", "В главном окне" },{ UniOscillograph::SELECTED_PERENT, "SELECTED_PERENT", "Выбрать Widget" },{ 0, NULL, NULL }};
		etype = g_enum_register_static (g_intern_static_string ("ExpandType"), values);
	}
	return etype;
}
// -------------------------------------------------------------------------
// static
GType Glib::Value<UniOscillograph::ExpandType>::value_type()
{
	return expandtype_get_type();
}
// -------------------------------------------------------------------------
void UniOscillograph::init_expand_parent_widget(Gtk::Widget* widget)
{
//	cout<<get_name()<<"::init_expand_parent_widget widget_name="<<widget->get_name()<<endl;
//	cout<< get_name() << "::init_expand_parent_widget() widget="<<widget<<endl;
	if(widget)
	{
		cout<<get_name()<<"::init_expand_parent_widget widget_name="<<widget->get_name()<<" expand_parent_widget_name="<<expand_parent_widget_name<<endl;
		Gtk::Container* conteiner = dynamic_cast<Gtk::Container*>(widget);
		if(conteiner)
		{
			widget_connection_list.push_back(conteiner->signal_add().connect(sigc::mem_fun(*this, &UniOscillograph::init_expand_parent_widget)));
			if(!get_prop_expand_parent() && !expand_parent_widget_name.empty() && expand_parent_widget_name == widget->get_name())
				set_prop_expand_parent(conteiner);
		}
	}
}
// -------------------------------------------------------------------------
bool UniOscillograph::on_expose_event(GdkEventExpose* ev)
{
	bool rv = Gtk::Fixed::on_expose_event(ev);
//	cout<<get_name()<<"::on_expose_event event->type="<<ev->type<< endl;
#if GTK_VERSION_GE(2,20)
	if(!get_mapped())
#else
	if(!is_show)
#endif
		return false;

	Glib::RefPtr<Gdk::Window> win = plot_area->get_window();
	if(!win)
		return false;
	Cairo::RefPtr<Cairo::Context> cr = win->create_cairo_context();
	Gtk::Allocation alloc = plot_area->get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
	cr->translate(alloc.get_x(), alloc.get_y());

	//draw channels
	for(DataMap::iterator it = data.begin();it != data.end();it++)
	{
		if(!it->first->get_prop_visible())
			continue;

		if(get_prop_plot_type() == LINEAR)
		{
			Gdk::Cairo::set_source_color(cr, it->first->get_channelcolor());
			cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			cr->set_line_width(get_prop_line_width()>1?get_prop_line_width():1);

			double x,prev_x;
			double y,prev_y;
			ChannelsDataMap::iterator pointIt = it->second.begin();
			ChannelsDataMap::iterator last_pointIt = pointIt;
			for(;pointIt != it->second.end();++pointIt)
			{
				if(pointIt->first <= min_x)
				{
					last_pointIt = pointIt;
					continue;
				}
				if(last_pointIt!=it->second.end())
				{
					x = (last_pointIt->first - min_x)/prop_XRange.get_value()*current_plot_width;
					y = current_plot_height - (last_pointIt->second - min_y)/range_y*current_plot_height;
					prev_x = x;
					prev_y = y;
					
					cr->move_to(x, y);

					if(last_pointIt==pointIt)
					{
						last_pointIt = it->second.end();
						continue;
					}
					last_pointIt = it->second.end();
				}
				
				x = (pointIt->first - min_x)/prop_XRange.get_value()*current_plot_width;
				y = current_plot_height - (pointIt->second - min_y)/range_y*current_plot_height;

				if(prop_smoothCurve)
				{
					cr->curve_to( prev_x+(x-prev_x)/2, prev_y, prev_x+(x-prev_x)/2, y, x, y);
					prev_x = x;
					prev_y = y;
				}
				else
					cr->line_to(x, y);
			}
			if(it->first->get_prop_fill())
			{
				cr->line_to(x, current_plot_height);
				cr->line_to(0, current_plot_height);
				cr->close_path();
				if(it->first->get_prop_gradient())
				{
					Cairo::RefPtr<Cairo::LinearGradient> gradient = Cairo::LinearGradient::create(0, y, 0, current_plot_height);
					gradient->add_color_stop_rgba(0.0, it->first->get_channelcolor().get_red_p(), it->first->get_channelcolor().get_green_p(), it->first->get_channelcolor().get_blue_p(),1.0);
					gradient->add_color_stop_rgba(1.0, it->first->get_prop_gradientcolor().get_red_p(), it->first->get_prop_gradientcolor().get_green_p(), it->first->get_prop_gradientcolor().get_blue_p(),it->first->get_prop_gradientTransparent());
					cr->set_source(gradient);
				}
				cr->fill();
			}
			else
				cr->stroke();
		}
		else if(get_prop_plot_type() == DIAGRAM)
		{
			if(it->first->get_prop_gradient())
			{
				if(!it->second.empty())
				{
					Cairo::RefPtr<Cairo::LinearGradient> gradient = Cairo::LinearGradient::create(0, current_plot_height - (it->second[point_x] - min_y)/range_y*current_plot_height, 0, current_plot_height);
					gradient->add_color_stop_rgba(0.0, it->first->get_channelcolor().get_red_p(), it->first->get_channelcolor().get_green_p(), it->first->get_channelcolor().get_blue_p(),1.0);
					gradient->add_color_stop_rgba(1.0, it->first->get_prop_gradientcolor().get_red_p(), it->first->get_prop_gradientcolor().get_green_p(), it->first->get_prop_gradientcolor().get_blue_p(),it->first->get_prop_gradientTransparent());
					cr->set_source(gradient);
				}
				else
					Gdk::Cairo::set_source_color(cr, it->first->get_channelcolor());
			}
			else
				Gdk::Cairo::set_source_color(cr, it->first->get_channelcolor());

			double x,prev_x;
			double y,prev_y;
			double k = get_prop_diagram_column_w();
			if(k>1)
				k=1;
			else if(k<=0)
				k=0.01;
			for(ChannelsDataMap::iterator pointIt = it->second.begin();pointIt != it->second.end();pointIt++)
			{
				x = (pointIt->first - min_x)/prop_XRange.get_value()*current_plot_width;
				y = current_plot_height - (pointIt->second - min_y)/range_y*current_plot_height;

				double step = ((double)prop_refresh_timeout.get_value()/1000)/prop_XRange.get_value()*current_plot_width;

				cr->rectangle( x, y, step*k, current_plot_height-y);
				cr->fill();
			}
		}
	}

	//draw TimeLine
	if(prop_enableTimeLine)
	{
		Gdk::Cairo::set_source_color(cr, prop_timeLineColor.get_value());
		double line_pos = abs((point_x - min_x)/prop_XRange.get_value()*current_plot_width);
		cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
		cr->set_line_width(1);
		cr->move_to(line_pos, 0);
		cr->line_to(line_pos, current_plot_height);
		cr->stroke();
	}

	//draw channelsname list
	if(prop_enableLabelList && channelsListBox->get_width()>1)
		draw_channels_list();

	//draw tooltip with x & y poins
	if(prop_enableTooltip && show_tooltip)
		draw_tooltip();
	
	if(!connector_)
	{
		if(get_property_disconnect_effect() == 1)
		{
			alloc.set_x(0);
			alloc.set_y(0);
			UVoid::draw_disconnect_effect_1(cr, alloc);
		}
	}
	else if ( !connector_->connected() && get_property_disconnect_effect() == 1)
	{
		alloc.set_x(0);
		alloc.set_y(0);
		UVoid::draw_disconnect_effect_1(cr, alloc);
	}
	return false;
}
// -------------------------------------------------------------------------
void UniOscillograph::draw_tooltip()
{
	UniOscilChannel* gravity_channel = 0;
	
	Glib::RefPtr<Gdk::Window> win = plot_area->get_window();
	if(!win)
		return;
	Cairo::RefPtr<Cairo::Context> cr = win->create_cairo_context();
	Gtk::Allocation alloc = plot_area->get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
	cr->translate(alloc.get_x(), alloc.get_y());

	Glib::RefPtr<Gdk::Pixbuf> tmp_image = tooltip_image->copy();
	int x,y;
	Gdk::ModifierType mtype;
	plot_area->get_window()->get_pointer(x,y,mtype);

	//calculation value X & Y in push point
	double x_value = x - alloc.get_x();
	double y_value = y - alloc.get_y();
	if(x_value<0)
		x_value=0;
	else if(x_value>current_plot_width)
		x_value=current_plot_width;
	if(y_value<0)
		y_value=0;
	else if(y_value>current_plot_height)
		y_value=current_plot_height;
	
	x_value = x_value/(double)current_plot_width*prop_XRange.get_value() + min_x;
	y_value = (double)(current_plot_height -y_value)/(double)current_plot_height*range_y + min_y;

	if(prop_enableTooltipGravity)
	{
		double min_delta_y_value = range_y;
		double new_x_value = x_value;
		double new_y_value = y_value;
		for(DataMap::iterator it = data.begin();it != data.end();++it)
		{
			if(!it->first->get_prop_visible())
				continue;
			
			ChannelsDataMap::iterator last_pointIt = it->second.begin();
			for(ChannelsDataMap::iterator pointIt = it->second.begin();pointIt != it->second.end();++pointIt)
			{
				if(pointIt->first >= x_value && last_pointIt->first <= x_value)
				{
					if( (x_value-last_pointIt->first) >= (pointIt->first-x_value) )
					{
						if(fabs(pointIt->second-y_value)<min_delta_y_value)
						{
							min_delta_y_value = fabs(pointIt->second-y_value);
							gravity_channel = it->first;
							new_x_value = pointIt->first;
							new_y_value = pointIt->second;
						}
					}
					else
					{
						if(fabs(last_pointIt->second-y_value)<min_delta_y_value)
						{
							min_delta_y_value = fabs(last_pointIt->second-y_value);
							gravity_channel = it->first;
							new_x_value = last_pointIt->first;
							new_y_value = last_pointIt->second;
						}
					} 
					break;
				}
				last_pointIt = pointIt;
			}
		}
		x_value = new_x_value;
		y_value = new_y_value;
		x = (x_value - min_x)/prop_XRange.get_value()*current_plot_width + alloc.get_x();
		y = current_plot_height - (y_value - min_y)/range_y*current_plot_height + alloc.get_y();
	}
	
	//draw bg image
	int x_pos = x - alloc.get_x();
	int y_pos = y - alloc.get_y() - prop_tooltipImageHeight.get_value();

	if(x_pos > current_plot_width - prop_tooltipImageWidth.get_value())
	{
		tmp_image = tmp_image->flip(true);
		x_pos = x_pos > current_plot_width ? current_plot_width - prop_tooltipImageWidth.get_value():x_pos - prop_tooltipImageWidth.get_value();
	}
	else if(x < alloc.get_x())
	{
		x_pos = 0;
	}
	if(y > current_plot_height + alloc.get_y())
	{
		y_pos = current_plot_height - prop_tooltipImageHeight.get_value();
	}
	else if(y < alloc.get_y() + prop_tooltipImageHeight.get_value())
	{
		tmp_image = tmp_image->flip(false);
		y_pos = y - alloc.get_y() > 0 ? y - alloc.get_y():0;
	}

	Gdk::Cairo::set_source_pixbuf(cr,tmp_image, x_pos, y_pos);
	cr->paint();

	//draw gravity point
	if(prop_enableTooltipGravity && gravity_channel)
	{
		int gravity_point_pos_x = x - alloc.get_x();
		int gravity_point_pos_y = y - alloc.get_y();
		if( gravity_point_pos_x > current_plot_width )
			gravity_point_pos_x = current_plot_width;
		else if( gravity_point_pos_x < 0 )
			gravity_point_pos_x = 0;
		if( gravity_point_pos_y > current_plot_height )
			gravity_point_pos_y = current_plot_height;
		else if( gravity_point_pos_y < 0 )
			gravity_point_pos_y = 0;
		
		cr->save();
		Gdk::Cairo::set_source_color(cr, gravity_channel->get_channelcolor());
		cr->set_line_width(get_prop_line_width()>1?get_prop_line_width()*2:2);
		cr->arc( gravity_point_pos_x, gravity_point_pos_y, (get_prop_line_width()>1?get_prop_line_width()+3:4), 0, 2*M_PI );
//		cr->stroke();
		cr->fill();
		cr->restore();
	}
	
	//draw text
	int prec = prop_tooltipLabelPrecision.get_value();
	if(prop_enableTooltipGravity && gravity_channel)
		prec = gravity_channel->get_prop_precision();
	if(prec<0)
		prec = 0;

	int layw,layh;
	std::ostringstream label_text;
	if(prop_enableTooltipRealTime)
	{
		int hour,min,sec;
		if(prop_enableScaleRealTime)
			timeToInt(lround(x_value - topLabel->get_curPos()),hour,min,sec,topLabel->get_lastTime());
		else
			timeToInt(lround(x_value-(point_x - (double)prop_refresh_timeout.get_value()/1000)),hour,min,sec);
		ostringstream time;
		time << std::setw(2) << std::setfill('0') << hour << ":";
		time << std::setw(2) << std::setfill('0') << min << ":";
		time << std::setw(2) << std::setfill('0') << sec;
		label_text<<"T: "<<time.str()<<endl;
	}
	else
		label_text<<"X: "<<(long)x_value<<","<<setw(prec)<<setfill('0')<<(long)((x_value-(long)x_value)*pow10(prec))<<endl;
	if(prec>0)
		label_text<<"Y: "<<(long)y_value<<","<<setw(prec)<<setfill('0')<<(long)((y_value-(long)y_value)*pow10(prec));
	else
		label_text<<"Y: "<<(long)y_value;
	tooltip_layout->set_text(label_text.str());
	tooltip_layout->context_changed();
	tooltip_layout->get_pixel_size(layw,layh);
	double lay_xpos = x_pos + (double(prop_tooltipImageWidth.get_value()) -layw) / 2;
	double lay_ypos = y_pos + (double(prop_tooltipImageHeight.get_value())-layh) / 2;
	cr->move_to(lay_xpos, lay_ypos);
	cr->save();
	if(prop_enableTooltipGravity && gravity_channel)
		Gdk::Cairo::set_source_color(cr, gravity_channel->get_channelcolor());
	else
		Gdk::Cairo::set_source_color(cr, prop_tooltipLabelColor.get_value());
	tooltip_layout->add_to_cairo_context(cr);
	cr->fill();
	cr->restore();
}
// -------------------------------------------------------------------------
void UniOscillograph::draw_channels_list()
{
	Glib::RefPtr<Gdk::Window> win = channelsListScrollwin->get_window();
	if(!win)
		return;
	Cairo::RefPtr<Cairo::Context> cr = win->create_cairo_context();
	Gtk::Allocation alloc = channelsListScrollwin->get_allocation();
	Gdk::Cairo::rectangle(cr,alloc);
	cr->clip();
	cr->translate(alloc.get_x(), alloc.get_y());

	int layw,layh,max_layw=0;
	channels_layout->get_pixel_size(layw,layh);
	int list_height = alloc.get_height();
	int list_width = alloc.get_width();

	if(softmove_ch_list_pos_y>0 || list_height >= ch_num*layh)
		softmove_ch_list_pos_y = 0;
	else if(softmove_ch_list_pos_y < list_height - ch_num*layh)
		softmove_ch_list_pos_y = list_height - ch_num*layh;

	for(ChannelsList::iterator it = channels.begin();it != channels.end();++it)
	{
		string channel_name = it->second->get_channelname();
		float ch_value = it->second->get_fvalue();
		DataMap::iterator dit = data.find(it->second);
		if(dit != data.end())
		{
			ChannelsDataMap::reverse_iterator last_pointIt = dit->second.rbegin();
			if(last_pointIt != dit->second.rend())
				ch_value = last_pointIt->second;
		}
		int prec = it->second->get_prop_precision();
		if(prec<0)
			prec = 0;
		std::ostringstream label_text;
		label_text <<" [";
		if(prec>0)
			label_text<<(long)ch_value<<","<<setw(prec)<<setfill('0')<<(long)((ch_value-(long)ch_value)*pow10(prec));
		else
			label_text<<(long)ch_value;
		label_text <<"] "<<channel_name;

		channels_layout->set_text(label_text.str());
		channels_layout->context_changed();
		channels_layout->get_pixel_size(layw,layh);

		max_layw = MAX(max_layw,layw);
	}
	if(softmove_ch_list_pos_x>0 || list_width >= max_layw + layh)
		softmove_ch_list_pos_x = 0;
	else if(softmove_ch_list_pos_x < list_width - max_layw - layh)
		softmove_ch_list_pos_x = list_width - max_layw - layh;

	int num=0;
	for(ChannelsList::iterator it = channels.begin();it != channels.end();++it)
	{
		string channel_name = it->second->get_channelname();
		float ch_value = it->second->get_fvalue();
		DataMap::iterator dit = data.find(it->second);
		if(dit != data.end())
		{
			ChannelsDataMap::reverse_iterator last_pointIt = dit->second.rbegin();
			if(last_pointIt != dit->second.rend())
				ch_value = last_pointIt->second;
		}
		int prec = it->second->get_prop_precision();
		if(prec<0)
			prec = 0;
		std::ostringstream label_text;
		label_text <<" [";
		if(prec>0)
			label_text<<(long)ch_value<<","<<setw(prec)<<setfill('0')<<(long)((ch_value-(long)ch_value)*pow10(prec));
		else
			label_text<<(long)ch_value;
		label_text <<"] "<<channel_name;

		channels_layout->set_text(label_text.str());
		channels_layout->context_changed();
		channels_layout->get_pixel_size(layw,layh);

		Gdk::Cairo::set_source_color(cr, it->second->get_channelcolor());

		//draw round button of visible
		cr->set_line_width(2);
		cr->arc( softmove_ch_list_pos_x + (double)layh/2, softmove_ch_list_pos_y + num*layh + (double)layh/2, (double)layh/2 - 2, 0, 2*M_PI );
		cr->stroke();
		if(it->second->get_prop_visible())
		{
			cr->arc( softmove_ch_list_pos_x + (double)layh/2, softmove_ch_list_pos_y + num*layh + (double)layh/2, (double)layh/6, 0, 2*M_PI );
			cr->fill();
		}

		//draw text
		cr->move_to(softmove_ch_list_pos_x + layh, softmove_ch_list_pos_y + num*layh);
		channels_layout->add_to_cairo_context(cr);
		cr->fill();
		++num;
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::set_refresh_timeout()
{
	if(!set_value_dialog)
		return;
	
	set_value_dialog->set_title("Выбор времени опроса каналов (ms)...");
	val1_label->set_label("время опроса каналов (ms)");
	val2_label->set_label("кол. шагов опроса для\nотступа от края шкалы X");
	val2_label->show();
	
	val1_spinbutton->set_increments(10,100);
	val1_spinbutton->set_range(0,DBL_MAX);
	val1_spinbutton->set_digits(0);
	val1_spinbutton->set_value(prop_refresh_timeout.get_value());
	val2_spinbutton->set_increments(1,10);
	val2_spinbutton->set_range( 1, get_prop_XRange()/((double)prop_refresh_timeout.get_value()/1000) );
	val2_spinbutton->set_digits(0);
	val2_spinbutton->set_value(prop_XStepsNum.get_value());
	val2_spinbutton->show();
	
	set_value_dialog_change_position();
	int res = set_value_dialog->run();
	switch(res)
	{
		case Gtk::RESPONSE_OK:
			set_prop_refresh_timeout(val1_spinbutton->get_value());
			set_prop_XStepsNum(val2_spinbutton->get_value());
			set_value_dialog->hide();
			break;
		case Gtk::RESPONSE_CANCEL:
			set_value_dialog->hide();
			break;
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::on_refresh_timeout_changed()
{
	if (tmr.connected())
		tmr.disconnect();
	if(prop_refresh_timeout.get_value()>0)
		tmr= Glib::signal_timeout().connect(sigc::mem_fun(*this, &UniOscillograph::on_timer_tick),prop_refresh_timeout.get_value());
}
// -------------------------------------------------------------------------
bool UniOscillograph::on_timer_tick()
{
	point_x = point_x + (double)prop_refresh_timeout.get_value()/1000;

	if(point_x>max_x && !view_range_lock)
	{
		min_x = point_x + step_x - get_prop_XRange() ;
		on_scales_x_range_changed();
//		cout<<"\n"<< get_name() << "::on_timer_tick() step_x="<<step_x<< endl;
	}

	// очистка данных
	double min_x_for_delete = point_x - get_prop_XRange() - get_prop_XStepsInMemory() * ((double)prop_refresh_timeout.get_value()/1000);
	for(DataMap::iterator it = data.begin();it != data.end();++it)
	{
		while(it->second.begin()!=it->second.end()? it->second.begin()->first < min_x_for_delete : false)
		{
			it->second.erase(it->second.begin());
		};
	}

	if(min_x < min_x_for_delete - step_x)
	{
		min_x = min_x_for_delete;
		on_scales_x_range_changed();
	}
	
	time_t tm = time(0);

	bool newline_flag = true;
	for(ChannelsList::iterator it = channels.begin();it != channels.end();++it)
	{
		float ch_value = it->second->get_fvalue();

		data[it->second][point_x] = ch_value;

		if((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag))
		{
			ChannelHistoryData channel_history_data;
			channel_history_data.tm = tm;
			channel_history_data.newline_flag = newline_flag;
			channel_history_data.value = ch_value;
			channel_history_data.point_x = point_x;
			channel_history_data.ch_id = it->second->get_channel_id();
			history[point_x][it->second->get_channel_id()] = channel_history_data;
			newline_flag = false;
		}
	}

	if(prop_YDynamicRange)
	{
		double new_min_y = prop_YMin.get_value();
		double new_max_y = prop_YMax.get_value();
		for(DataMap::iterator it = data.begin();it != data.end();++it)
		{
			ChannelsDataMap::iterator pointIt = it->second.begin();
			for(pointIt;pointIt != it->second.end();++pointIt)
			{
				if(!it->first->get_prop_visible())
					continue;
				new_min_y = MIN(new_min_y,pointIt->second);
//				min_y = MIN(min_y,prop_YMin.get_value());
				new_max_y = MAX(new_max_y,pointIt->second);
//				max_y = MAX(max_y,prop_YMax.get_value());
//				cout<< get_name() << "::on_timer_tick() min_y="<<min_y<<" max_y="<<max_y<< endl;
			}
		}
		if(new_min_y!=min_y || new_max_y!=max_y)
		{
			min_y = new_min_y;
			max_y = new_max_y;
			on_scales_y_range_changed();
		}
	}

	if((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag))
	{
		if(prop_historyPointInMem.get_value()>0)
		{
			if( history.size() >= prop_historyPointInMem.get_value() )
			{
				FILE* outfile = 0;
				bool outfileIsOpen;

				if(history_file_fullname.empty())
					on_history_dir_changed();
				cout<< get_name() << " history_file_fullname="<<history_file_fullname<<endl;

				fstream fout( history_file_fullname.c_str(), ios::out | ios::app | ios::binary );
				if(!fout)
				{
					cout<< get_name() << " can't open file="<<history_file_fullname<<" for write!"<< endl;
					outfileIsOpen = false;
				}
				else
				{
					fout.close();
					outfile = fopen(history_file_fullname.c_str(),"a");
					outfileIsOpen = true;
				}

				if(outfileIsOpen)
				{
					for( auto it = history.begin(); it!=history.end(); ++it )
					{
						for( auto chIt = it->second.begin(); chIt!=it->second.end(); ++chIt )
						{
							if(!outfile)
								cout<< get_name() << " can't open file="<<history_file_fullname<<" for write!"<< endl;
							else if(fwrite(&(chIt->second),sizeof(chIt->second),1,outfile) < 1)
								cout<< get_name() << " can't write to file="<<history_file_fullname<< endl;
						}
					}
					fclose(outfile);
					history.clear();
				}
			}
		}
	}

	queue_draw();
	return true;
}// -------------------------------------------------------------------------
void UniOscillograph::on_realize()
{
	if(!is_realized)
	{
		min_y = prop_YMin.get_value();
		max_y = prop_YMax.get_value();
	}

	Gtk::Fixed::on_realize();
	on_web_minor_enable_changed();
	on_web_major_color_changed();
	on_web_minor_color_changed();
	on_scales_enable_real_time_changed();
	on_tooltip_image_changed();
	on_tooltip_font_changed();
	on_font_changed();
	on_channels_list_font_changed();
	on_channels_list_width_changed();
	on_history_enable_changed();
	on_bg_image_changed();
//	on_expand_bg_image_changed();
	on_list_bg_image_changed();
	on_h_max_minor_changed();
	on_h_max_major_changed();
	on_v_max_minor_changed();
	on_v_max_major_changed();
	saveProgress->hide();

	for(ConnectionsList::iterator it = widget_connection_list.begin(); it != widget_connection_list.end(); ++it)
		(*it).disconnect();
	widget_connection_list.clear();
	
//	cout<< get_name() << "::on_realize() current_plot_width="<<current_plot_width<<" plot_width="<<plot_width<<" current_plot_height="<<current_plot_height<<" plot_height="<<plot_height<<endl;
	is_realized = true;
}
// -------------------------------------------------------------------------
void UniOscillograph::oscil_allocate_changed(Gtk::Allocation& alloc)
{
	
	if(alloc.get_width() != current_width || alloc.get_height() != current_height)
	{
		current_width = alloc.get_width();
		current_height = alloc.get_height();

		if(!is_expand)
			BGImage->set(get_pixbuf_from_cache(prop_BGImagePath.get_value(), current_width, current_height));
		else
			expandBGImage->set(get_pixbuf_from_cache(prop_expandBGImagePath.get_value(), current_width, current_height));
	}

	mainbox->set_size_request(current_width,current_height);
	hpaned->set_size_request(current_width,current_height);

	current_plot_width = plot_area->get_allocation().get_width();
	current_plot_height = plot_area->get_allocation().get_height();
	
	if(!is_expand)
	{
		widget_width = alloc.get_width();
		widget_height = alloc.get_height();
		widget_x = alloc.get_x();
		widget_y = alloc.get_y();
	}

	if(is_realized && prop_enableDynamicScaleResolution && !is_glade_editor)
	{
//		cout<< get_name() << "::oscil_allocate_changed() current_plot_width="<<current_plot_width<<" plot_width="<<plot_width<<" current_plot_height="<<current_plot_height<<" plot_height="<<plot_height<<endl;
		int XMin, XMaj;
		int YMin, YMaj;
		if(is_expand)
		{
			float kx = ((float)current_plot_width)/plot_width;
			float ky = ((float)current_plot_height)/plot_height;
			XMin = lround(XmaxMin * kx);
			XMaj = lround(XmaxMaj * kx);
			YMin = lround(YmaxMin * ky);
			YMaj = lround(YmaxMaj * ky);
		}
		else
		{
			XMin = XmaxMin;
			XMaj = XmaxMaj;
			YMin = YmaxMin;
			YMaj = YmaxMaj;
		}
//		cout<< get_name() << "::oscil_allocate_changed() kx="<<kx<<" ky="<<ky<<" XMin="<<XMin<<" XMaj="<<XMaj<<" YMin="<<YMin<<" YMaj="<<YMaj<<endl;
		
		topScale->set_max_minor(XMin);
		topScale->set_max_major(XMaj);
		topScale->set_range(min_x,max_x);
		
		bottomScale->set_max_minor(XMin);
		bottomScale->set_max_major(XMaj);
		bottomScale->set_range(min_x,max_x);
		
		plot_area->set_max_h_minor(XMin);
		plot_area->set_max_h_major(XMaj);
		plot_area->set_h_range(min_x,max_x);
		
		leftScale->set_max_minor(YMin);
		leftScale->set_max_major(YMaj);
		leftScale->set_range(min_y,max_y);
		
		rightScale->set_max_minor(YMin);
		rightScale->set_max_major(YMaj);
		rightScale->set_range(min_y,max_y);
		
		plot_area->set_max_v_minor(YMin);
		plot_area->set_max_v_major(YMaj);
		plot_area->set_v_range(min_y,max_y);
	}
// 	cout<< get_name() << "::oscil_allocate_changed() is_expand="<<is_expand<<" widget_width="<<alloc.get_width()<<" widget_height="<<alloc.get_height()<<" alloc.get_x()="<<alloc.get_x()<<" alloc.get_y()="<<alloc.get_y()<<endl;
}
// -------------------------------------------------------------------------
void UniOscillograph::oscil_expand()
{
	if(!parent)
		parent=get_parent();
	
	if(get_prop_expand_type() == PERENT_FIXED)
	{
		if(!is_expand)
		{
			if (expand_parent_resize_connection.connected())
				expand_parent_resize_connection.disconnect();
			expand_parent_resize_connection= parent->signal_size_allocate().connect(sigc::mem_fun(*this, &UniOscillograph::on_oscil_expand_parent_resize));

			parent->Gtk::Container::remove(*this);
			parent->add(*this);
			reparent(*parent);
			is_expand = true;
			set_size_request(parent->get_allocation().get_width(),parent->get_allocation().get_height());

//			expandBGImage->set(get_pixbuf_from_cache(prop_expandBGImagePath.get_value(), parent->get_allocation().get_width(), parent->get_allocation().get_height()));
			expandBGImage->show();
			BGImage->hide();
		}
		else
		{
			if (expand_parent_resize_connection.connected())
				expand_parent_resize_connection.disconnect();
			
			if(dynamic_cast<Gtk::Fixed*>(parent))
				dynamic_cast<Gtk::Fixed*>(parent)->move(*this,widget_x - parent->get_allocation().get_x(),widget_y - parent->get_allocation().get_y());
			is_expand = false;
			set_size_request(widget_width,widget_height);

//			BGImage->set(get_pixbuf_from_cache(prop_BGImagePath.get_value(), widget_width, widget_height));
			BGImage->show();
			expandBGImage->hide();
		}
	}
	else if(get_prop_expand_type() == MAIN_WINDOW)
	{
		if(!mainwindow)
			mainwindow = dynamic_cast<Gtk::Window*>(get_toplevel());
		if(!is_expand)
		{
			if (win_hide_connection.connected())
				win_hide_connection.disconnect();
			win_hide_connection= mainwindow->signal_hide().connect(sigc::mem_fun(*this, &UniOscillograph::on_oscil_expand_window_hide));
			
			is_expand = true;
			
			WindowChild = mainwindow->get_child();
			WindowChild->reparent(*bufferWindow);
			reparent(*mainwindow);
			
//			expandBGImage->set(get_pixbuf_from_cache(prop_expandBGImagePath.get_value(),get_toplevel()->get_allocation().get_width(),get_toplevel()->get_allocation().get_height()));
			expandBGImage->show();
			BGImage->hide();
		}
		else
		{
			reparent(*parent);
			if(WindowChild)
				WindowChild->reparent(*mainwindow);
			if(dynamic_cast<Gtk::Fixed*>(parent))
				dynamic_cast<Gtk::Fixed*>(parent)->move(*this,widget_x - parent->get_allocation().get_x(),widget_y - parent->get_allocation().get_y());

			is_expand = false;

			set_size_request(widget_width,widget_height);
//			BGImage->set(get_pixbuf_from_cache(prop_BGImagePath.get_value(), widget_width, widget_height));
			BGImage->show();
			expandBGImage->hide();
		}
	}
	else if(get_prop_expand_type() == SELECTED_PERENT)
	{
		Gtk::Window *newPerentWindow = dynamic_cast<Gtk::Window*>(get_prop_expand_parent());
		Gtk::Fixed *expandFixed = dynamic_cast<Gtk::Fixed*>(get_prop_expand_parent());
		Gtk::Container *expandContainer = dynamic_cast<Gtk::Container*>(get_prop_expand_parent());

		if(newPerentWindow)
		{
			if(!mainwindow)
				mainwindow = dynamic_cast<Gtk::Window*>(get_toplevel());
			
			if(!is_expand)
			{
				if(newPerentWindow != mainwindow)
					newPerentWindow->show();

				if (win_hide_connection.connected())
					win_hide_connection.disconnect();
				win_hide_connection= newPerentWindow->signal_hide().connect(sigc::mem_fun(*this, &UniOscillograph::on_oscil_expand_window_hide));
				
				is_expand = true;
				
				WindowChild = newPerentWindow->get_child();
				if(WindowChild)
					WindowChild->reparent(*bufferWindow);
				reparent(*newPerentWindow);
				
//				expandBGImage->set(get_pixbuf_from_cache(prop_expandBGImagePath.get_value(),newPerentWindow->get_allocation().get_width(),newPerentWindow->get_allocation().get_height()));
				expandBGImage->show();
				BGImage->hide();
			}
			else
			{
				if (win_hide_connection.connected())
					win_hide_connection.disconnect();
				
				reparent(*parent);
				if(WindowChild)
					WindowChild->reparent(*newPerentWindow);
				if(dynamic_cast<Gtk::Fixed*>(parent))
				{
					reparent(*parent);
					dynamic_cast<Gtk::Fixed*>(parent)->move(*this,widget_x - parent->get_allocation().get_x(),widget_y - parent->get_allocation().get_y());
					set_size_request(widget_width,widget_height);
				}
				else if(dynamic_cast<Gtk::Container*>(parent))
				{
					is_expand = false;
					parent->add(*this);
				}

//				BGImage->set(get_pixbuf_from_cache(prop_BGImagePath.get_value(), widget_width, widget_height));
				BGImage->show();
				expandBGImage->hide();

				is_expand = false;
				
				if(newPerentWindow != mainwindow)
					newPerentWindow->hide();
			}
		}
		else if(expandFixed)
		{
			if(!mainwindow)
				mainwindow = dynamic_cast<Gtk::Window*>(get_toplevel());
			if(!newPerentWindow)
				newPerentWindow = dynamic_cast<Gtk::Window*>(expandFixed->get_toplevel());
			
			if(!is_expand)
			{
				if(newPerentWindow != mainwindow)
					newPerentWindow->show();
				
				if (win_hide_connection.connected())
					win_hide_connection.disconnect();
				win_hide_connection= newPerentWindow->signal_hide().connect(sigc::mem_fun(*this, &UniOscillograph::on_oscil_expand_window_hide));
				
				if (expand_parent_resize_connection.connected())
					expand_parent_resize_connection.disconnect();
				expand_parent_resize_connection= expandFixed->signal_size_allocate().connect(sigc::mem_fun(*this, &UniOscillograph::on_oscil_expand_parent_resize));
				
				is_expand = true;
				
				reparent(*expandFixed);
				set_size_request(expandFixed->get_allocation().get_width(),expandFixed->get_allocation().get_height());
//				expandBGImage->set(get_pixbuf_from_cache(prop_expandBGImagePath.get_value(), expandFixed->get_allocation().get_width(), expandFixed->get_allocation().get_height()));
				expandBGImage->show();
				BGImage->hide();
			}
			else
			{
				if (win_hide_connection.connected())
					win_hide_connection.disconnect();

				if (expand_parent_resize_connection.connected())
					expand_parent_resize_connection.disconnect();

				if(dynamic_cast<Gtk::Fixed*>(parent))
				{
					reparent(*parent);
					dynamic_cast<Gtk::Fixed*>(parent)->move(*this,widget_x - parent->get_allocation().get_x(),widget_y - parent->get_allocation().get_y());
					set_size_request(widget_width,widget_height);
				}
				else if(dynamic_cast<Gtk::Container*>(parent))
				{
					is_expand = false;
					parent->add(*this);
				}

//				BGImage->set(get_pixbuf_from_cache(prop_BGImagePath.get_value(), widget_width, widget_height));
				BGImage->show();
				expandBGImage->hide();

				is_expand = false;
				
				if(newPerentWindow != mainwindow)
					newPerentWindow->hide();
			}
		}
		else if(expandContainer)
		{
			if(!is_expand)
			{
				is_expand = true;
				
				reparent(*expandContainer);
				set_size_request(expandContainer->get_allocation().get_width(),expandContainer->get_allocation().get_height());
//				expandBGImage->set(get_pixbuf_from_cache(prop_expandBGImagePath.get_value(), expandContainer->get_allocation().get_width(), expandContainer->get_allocation().get_height()));
				expandBGImage->show();
				BGImage->hide();
			}
			else
			{
				if(dynamic_cast<Gtk::Fixed*>(parent))
				{
					reparent(*parent);
					dynamic_cast<Gtk::Fixed*>(parent)->move(*this,widget_x - parent->get_allocation().get_x(),widget_y - parent->get_allocation().get_y());
					is_expand = false;
					set_size_request(widget_width,widget_height);
				}
				else if(dynamic_cast<Gtk::Container*>(parent))
				{
					is_expand = false;
					parent->add(*this);
				}

//				BGImage->set(get_pixbuf_from_cache(prop_BGImagePath.get_value(), widget_width, widget_height));
				BGImage->show();
				expandBGImage->hide();
			}
		}
	}
	if(prop_enableLabelList)
	{
		switch_ch_list();
		switch_ch_list();
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::on_oscil_expand_window_hide()
{
	if(is_expand)
		oscil_expand();
}
// -------------------------------------------------------------------------
void UniOscillograph::on_oscil_expand_parent_resize(Gtk::Allocation& alloc)
{
//	cout<< get_name() << "::on_oscil_expand_parent_resize() is_expand="<<is_expand<<" widget_width="<<alloc.get_width()<<" widget_height="<<alloc.get_height()<<" alloc.get_x()="<<alloc.get_x()<<" alloc.get_y()="<<alloc.get_y()<<endl;
	if(is_expand)
		set_size_request(alloc.get_width(),alloc.get_height());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_channels_list_width_changed()
{
	if(prop_enableLabelList)
	{
		channelsListScrollwin->set_size_request(abs(prop_labelListWidth.get_value()),get_allocation().get_height());
		table->reparent(*hpaned);
		hpaned->show();
		hpaned->set_size_request(get_allocation().get_width(),get_allocation().get_height());
	}
	else
	{
//		hpaned->set_position(0);
//		hpaned->property_position_set().set_value(true);
//		channelsListScrollwin->set_size_request(0,get_allocation().get_height());
		hpaned->hide();
		table->reparent(*mainbox);
		table->set_size_request(get_allocation().get_width(),get_allocation().get_height());
	}
	current_plot_width = plot_area->get_allocation().get_width();
}
// -------------------------------------------------------------------------
void UniOscillograph::cs_dialog_change_position()
{
	if(cs_dialog && get_toplevel()->get_window())
	{
		int root_x,root_y;
//		get_window()->get_position(root_x,root_y);
//		get_window()->get_root_coords(plot_area->get_allocation().get_x(),plot_area->get_allocation().get_y(),root_x,root_y);
//		get_window()->get_root_origin(root_x,root_y);
//		get_root_window()->get_origin(root_x,root_y);
		
		get_toplevel()->get_window()->get_position(root_x,root_y);
//		cout<< get_name() << "::dialog_change_position() win_name["<<get_toplevel()->get_name()<<"] root_x="<<root_x<<" root_y="<<root_y<<endl;
		cs_dialog->move(root_x+plot_area->get_allocation().get_x(),root_y+plot_area->get_allocation().get_y());
		cs_dialog->set_size_request(current_plot_width,current_plot_height);
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::set_value_dialog_change_position()
{
	if(set_value_dialog && get_toplevel()->get_window())
	{
		int root_x,root_y;
		get_toplevel()->get_window()->get_position(root_x,root_y);
//		cout<< get_name() << "::dialog_change_position() win_name["<<get_toplevel()->get_name()<<"] root_x="<<root_x<<" root_y="<<root_y<<endl;
		int dialog_width = set_value_dialog->get_width();
		int dialog_height = set_value_dialog->get_height();
		if(dialog_width<=1 && dialog_height<=1)
		{
			set_value_dialog->move(get_window()->get_screen()->get_width(),get_window()->get_screen()->get_height());
			set_value_dialog->show();
		}
		dialog_width = set_value_dialog->get_width();
		dialog_height = set_value_dialog->get_height();
//		cout<< get_name() << "::set_value_dialog_change_position() win_name["<<get_toplevel()->get_name()<<"] dialog_width="<<dialog_width<<" dialog_height="<<dialog_height<<endl;
		set_value_dialog->move(root_x+plot_area->get_allocation().get_x()+(current_plot_width-dialog_width)/2,root_y+plot_area->get_allocation().get_y()+(current_plot_height-dialog_height)/2);
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::on_channels_list_border_width_changed()
{
//	cout<< get_name() << "::on_channels_list_width_changed() set_border_width ="<<prop_labelListBorderWidth.get_value()<<endl;
	channelsListBox->set_border_width(prop_labelListBorderWidth.get_value());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_bg_border_width_changed()
{
//	cout<< get_name() << "::on_bg_border_width_changed() set_border_width ="<<prop_bgBorderWidth.get_value()<<endl;
	hpaned->set_border_width(prop_bgBorderWidth.get_value());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_add(Gtk::Widget* w)
{
	if (UVoid* uv = dynamic_cast<UVoid*>(w)) {
		uv->set_property_disconnect_effect(0);
		uv->set_property_auto_connect(true);
	}

	if (UniOscilChannel* ch = dynamic_cast<UniOscilChannel*>(w))
	{
		Gtk::Fixed::on_add(w);
		w->set_child_visible(false);
		add_channel(ch);
	}
	else if(UniOscilConfig* conf = dynamic_cast<UniOscilConfig*>(w))
	{
		Gtk::Fixed::on_add(conf);
	}
	else
		Gtk::Fixed::on_remove(w);
}
// -------------------------------------------------------------------------
void UniOscillograph::add_channel(UniOscilChannel* ch)
{
	if(channels.find(ch->get_channel_id())!=channels.end())
	{
		cout<< get_name() << "::on_add channel id="<<ch->get_channel_id()<<" already added..."<< endl;
		return;
	}
	channels[ch->get_channel_id()] = ch;
	ch_num = channels.size();
	//		cout<< get_name() << "::on_add ch_id="<<ch->set_channel_id<< endl;
	
	if(ch)
		ch->set_connector(get_connector());
	
	ch->property_randcolor().signal_changed().connect(bind(sigc::mem_fun(*this, &UniOscillograph::set_channel_random_color),ch));
	if(ch->get_prop_randcolor())
	{
		set_channel_random_color(ch);
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::set_channel_random_color(UniOscilChannel *ch)
{
	if(ch->get_prop_randcolor())
	{
		Gdk::Color color;
		//Если с 30-й попытки не удалось сгенерировать подходящего цвета, так как
		//каналов слишком много, оставляем выбор цвета на совести пользователя.
		for(int i=0 ; i< 30 ; i++ )
		{
			color.set_rgb_p(((double)random())/RAND_MAX, ((double)random())/RAND_MAX, ((double)random())/RAND_MAX);
			ChannelsList::iterator it = channels.begin();
			for(; it != channels.end(); it++)
			{
				if(ch->get_channelcolor().to_string() != color.to_string())
					break;
			}
			if(it != channels.end())
			{
				ch->set_channelcolor(color);
				break;
			}

		}
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::on_remove(Gtk::Widget* w)
{
	remove_channel(dynamic_cast<UniOscilChannel*>(w));
	Gtk::Fixed::on_remove(w);
}
// -------------------------------------------------------------------------
void UniOscillograph::remove_channel(UniOscilChannel* ch)
{
	ChannelsList::iterator it = channels.find(ch->get_channel_id());
	if(channels.find(ch->get_channel_id()) != channels.end())
	{
		data.erase(ch);
		channels.erase(ch->get_channel_id());
		ch_num = channels.size();
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::del_channel()
{
	remove_channel(channels[save_channel_num]);
}
// -------------------------------------------------------------------------
#define check_child( W ) \
	if ( childinfomap_.count(W) == 0 ) { \
		std::cerr << "(UniOscillograph):Error:" \
			  << W->get_name() << \
			     " is not in container" <<std::endl; \
		return; \
}
// -------------------------------------------------------------------------
void UniOscillograph::set_child_connector(Gtk::Widget& child)
{
	UVoid* voidChild = dynamic_cast<UVoid*>(&child);
	if(voidChild && get_connector())
	{
		voidChild->set_connector(get_connector());
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::set_connector(const ConnectorRef& connector) throw()
{
	if (connector == get_connector())
		return;

	if(connector == true)
	{
		UVoid::set_connector(connector);
		
		sigc::slot<void, Gtk::Widget&> set_child_connector = sigc::mem_fun(this, &UniOscillograph::set_child_connector);
		
		foreach(set_child_connector);
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::on_connect() throw()
{
	UVoid::on_connect();

	queue_draw();
}
// -------------------------------------------------------------------------
void UniOscillograph::on_disconnect() throw()
{
	UVoid::on_disconnect();
	queue_draw();
}
// -------------------------------------------------------------------------
void UniOscillograph::process_sensor(ObjectId id, ObjectId node, long value)
{
}
// -------------------------------------------------------------------------
void UniOscillograph::on_group_of_channels_changed()
{
	channels_group.clear();
	std::list<Glib::ustring> channels_group_list = explodestr(get_prop_groupOfAddChannels(),',');
	for(std::list<Glib::ustring>::iterator ch_it = channels_group_list.begin(); ch_it != channels_group_list.end(); ++ch_it)
		add_channels_group(*ch_it);
}
// -------------------------------------------------------------------------
void UniOscillograph::on_scales_enable_changed()
{
	bottomScale->set_enabled(prop_bottomScale);
	topScale->set_enabled(prop_topScale);
	leftScale->set_enabled(prop_leftScale);
	rightScale->set_enabled(prop_rightScale);
}
// -------------------------------------------------------------------------
void UniOscillograph::on_scales_enable_real_time_changed()
{
	topLabel->use_real_time(prop_enableScaleRealTime.get_value());
	bottomLabel->use_real_time(prop_enableScaleRealTime.get_value());

//	queue_draw();
}
// -------------------------------------------------------------------------
void UniOscillograph::on_web_enable_changed()
{
	plot_area->set_enabled(prop_enableWeb);
}
// -------------------------------------------------------------------------
void UniOscillograph::on_web_major_color_changed()
{
	plot_area->set_web_major_color(get_prop_webMajorColor());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_web_minor_enable_changed()
{
	plot_area->set_enabled_minor(prop_enableWebMinor);
}
// -------------------------------------------------------------------------
void UniOscillograph::on_web_minor_color_changed()
{
	plot_area->set_web_minor_color(get_prop_webMinorColor());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_web_line_type_changed()
{
	plot_area->set_web_line_type(get_prop_WebLineType());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_scales_y_range_changed()
{
	if(!prop_YDynamicRange)
	{
		min_y = prop_YMin.get_value();
		max_y = prop_YMax.get_value();
	}

	leftScale->set_range(min_y,max_y);
	rightScale->set_range(min_y,max_y);
	plot_area->set_v_range(min_y,max_y);
	range_y = abs(max_y - min_y);

	queue_draw();
}
// -------------------------------------------------------------------------
void UniOscillograph::on_scales_x_range_changed()
{
	if(prop_enableScaleRealTime)
	{
		topLabel->set_curPos(point_x);
		bottomLabel->set_curPos(point_x);
	}
	
	max_x = min_x + prop_XRange.get_value();
	plot_area->set_h_range(min_x,max_x);
	topScale->set_range(min_x,max_x);
	bottomScale->set_range(min_x,max_x);
	on_scales_x_step_changed();

	queue_draw();
}
// -------------------------------------------------------------------------
void UniOscillograph::on_scales_x_step_changed()
{
	double XStepsNum = get_prop_XStepsNum()<1 ? 1:get_prop_XStepsNum();
	if(abs(XStepsNum *  (double)get_prop_refresh_timeout()/1000) > get_prop_XRange())
		step_x = (long)abs(get_prop_XRange()/((double)get_prop_refresh_timeout()/1000)) * ((double)get_prop_refresh_timeout()/1000);
	else
		step_x = abs(XStepsNum * (double)get_prop_refresh_timeout()/1000);

	queue_draw();
}
// -------------------------------------------------------------------------
void UniOscillograph::on_h_max_minor_changed()
{
	if(prop_enableScaleRealTime)
	{
		topLabel->set_curPos(point_x);
		bottomLabel->set_curPos(point_x);
	}
	XmaxMin = prop_XmaxMin.get_value();
	
	topScale->set_max_minor(XmaxMin);
	topScale->set_range(min_x,max_x);
	
	bottomScale->set_max_minor(XmaxMin);
	bottomScale->set_range(min_x,max_x);
	
	plot_area->set_max_h_minor(XmaxMin);
	plot_area->set_h_range(min_x,max_x);
	
	queue_draw();
	plot_width = plot_area->get_allocation().get_width();
	// 	cout<< get_name() << "::on_h_max_minor_changed() XmaxMin="<<XmaxMin<<" plot_width="<<plot_width<<endl;
}
// -------------------------------------------------------------------------
void UniOscillograph::on_h_max_major_changed()
{
	if(prop_enableScaleRealTime)
	{
		topLabel->set_curPos(point_x);
		bottomLabel->set_curPos(point_x);
	}
	XmaxMaj = prop_XmaxMaj.get_value();
	
	topScale->set_max_major(XmaxMaj);
	topScale->set_range(min_x,max_x);
	
	bottomScale->set_max_major(XmaxMaj);
	bottomScale->set_range(min_x,max_x);
	
	plot_area->set_max_h_major(XmaxMaj);
	plot_area->set_h_range(min_x,max_x);
	
	queue_draw();
	plot_width = plot_area->get_allocation().get_width();
	// 	cout<< get_name() << "::on_h_max_major_changed() XmaxMaj="<<XmaxMaj<<" plot_width="<<plot_width<<endl;
}
// -------------------------------------------------------------------------
void UniOscillograph::on_v_max_minor_changed()
{
	YmaxMin = prop_YmaxMin.get_value();
	
	leftScale->set_max_minor(YmaxMin);
	leftScale->set_range(min_y,max_y);
	
	rightScale->set_max_minor(YmaxMin);
	rightScale->set_range(min_y,max_y);
	
	plot_area->set_max_v_minor(YmaxMin);
	plot_area->set_v_range(min_y,max_y);
	
	queue_draw();
	plot_height = plot_area->get_allocation().get_height();
	// 	cout<< get_name() << "::on_v_max_minor_changed() YmaxMin="<<YmaxMin<<" plot_height="<<plot_height<<endl;
}
// -------------------------------------------------------------------------
void UniOscillograph::on_v_max_major_changed()
{
	YmaxMaj = prop_YmaxMaj.get_value();
	
	leftScale->set_max_major(YmaxMaj);
	leftScale->set_range(min_y,max_y);
	
	rightScale->set_max_major(YmaxMaj);
	rightScale->set_range(min_y,max_y);
	
	plot_area->set_max_v_major(YmaxMaj);
	plot_area->set_v_range(min_y,max_y);
	
	queue_draw();
	plot_height = plot_area->get_allocation().get_height();
	// 	cout<< get_name() << "::on_v_max_major_changed() YmaxMaj="<<YmaxMaj<<" plot_height="<<plot_height<<endl;
}
// -------------------------------------------------------------------------
void UniOscillograph::on_dynamic_scale_resolution_changed()
{
	if(prop_enableDynamicScaleResolution)
	{
		set_size_request(current_width, current_height);
	}
	else
	{
		int XMin = XmaxMin;
		int XMaj = XmaxMaj;
		int YMin = YmaxMin;
		int YMaj = YmaxMaj;
		
		topScale->set_max_minor(XMin);
		topScale->set_max_major(XMaj);
		topScale->set_range(min_x,max_x);
		
		bottomScale->set_max_minor(XMin);
		bottomScale->set_max_major(XMaj);
		bottomScale->set_range(min_x,max_x);
		
		plot_area->set_max_h_minor(XMin);
		plot_area->set_max_h_major(XMaj);
		plot_area->set_h_range(min_x,max_x);
		
		leftScale->set_max_minor(YMin);
		leftScale->set_max_major(YMaj);
		leftScale->set_range(min_y,max_y);
		
		rightScale->set_max_minor(YMin);
		rightScale->set_max_major(YMaj);
		rightScale->set_range(min_y,max_y);
		
		plot_area->set_max_v_minor(YMin);
		plot_area->set_max_v_major(YMaj);
		plot_area->set_v_range(min_y,max_y);
	}
}
// -------------------------------------------------------------------------
/*void UniOscillograph::on_scales_autoscale_changed()
{
	m_plot.scale(AXIS_LEFT)->set_autoscale(prop_dynamicAxe);
	m_plot.scale(AXIS_RIGHT)->set_autoscale(prop_dynamicAxe);
	m_plot.scale(AXIS_TOP)->set_autoscale(prop_dynamicAxe);
	m_plot.scale(AXIS_BOTTOM)->set_autoscale(prop_dynamicAxe);
}*/
// -------------------------------------------------------------------------
void UniOscillograph::on_font_changed()
{
	Pango::FontDescription fd(get_prop_scaleLabelFont());
	fd.set_absolute_size(prop_scaleLabelFontSize.get_value() * Pango::SCALE);
	topScale->get_labels()->set_labels_font(fd);
	bottomScale->get_labels()->set_labels_font(fd);
	leftScale->get_labels()->set_labels_font(fd);
	rightScale->get_labels()->set_labels_font(fd);
}
// -------------------------------------------------------------------------
void UniOscillograph::on_scale_color_changed()
{
	topScale->set_scale_color(get_prop_scaleColor());
	bottomScale->set_scale_color(get_prop_scaleColor());
	leftScale->set_scale_color(get_prop_scaleColor());
	rightScale->set_scale_color(get_prop_scaleColor());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_font_color_changed()
{
	topScale->get_labels()->set_labels_color(get_prop_scaleLabelColor());
	bottomScale->get_labels()->set_labels_color(get_prop_scaleLabelColor());
	leftScale->get_labels()->set_labels_color(get_prop_scaleLabelColor());
	rightScale->get_labels()->set_labels_color(get_prop_scaleLabelColor());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_plot_bg_color_changed()
{
//	plot_area->modify_bg(Gtk::STATE_NORMAL,prop_bgColor);
}
// -------------------------------------------------------------------------
void UniOscillograph::on_bg_image_changed()
{
	BGImage->set(get_pixbuf_from_cache(prop_BGImagePath.get_value(), get_allocation().get_width(), get_allocation().get_height()));
	if(is_expand)
		BGImage->hide();
}
// -------------------------------------------------------------------------
void UniOscillograph::on_expand_bg_image_changed()
{
}
// -------------------------------------------------------------------------
void UniOscillograph::on_list_bg_image_changed()
{
	channelsListBGImage.property_image0_path().set_value(prop_LabelListBGImagePath.get_value());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_tooltip_image_changed()
{
	tooltip_image = get_pixbuf_from_cache(prop_tooltipImagePath.get_value(), prop_tooltipImageWidth.get_value(), prop_tooltipImageHeight.get_value());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_tooltip_font_changed()
{
	Pango::FontDescription font(prop_tooltipLabelFont);
	font.set_absolute_size(prop_tooltipLabelFontSize.get_value() * Pango::SCALE);
	tooltip_layout->set_font_description(font);
}
// -------------------------------------------------------------------------
void UniOscillograph::on_channels_list_frame_changed()
{
	channelsListBox->set_shadow_type(prop_labelListFrame.get_value());
}
// -------------------------------------------------------------------------
void UniOscillograph::on_channels_list_font_changed()
{
	Pango::FontDescription font(prop_labelListFont);
	font.set_absolute_size(prop_labelListFontSize.get_value() * Pango::SCALE);
	channels_layout->set_font_description(font);
}
// -------------------------------------------------------------------------
bool UniOscillograph::scroll_channels_list_event(GdkEvent* ev)
{
//	if(ev->type!=GDK_EXPOSE)
//		cout<<get_name()<<"::scroll_channels_list_event event->type="<<ev->type<< endl;
	GdkEventButton event_btn = ev->button;

	if(event_btn.type == GDK_2BUTTON_PRESS)
	{
		int x,y;
		Gdk::ModifierType mtype;
		channelsListScrollwin->get_window()->get_pointer(x,y,mtype);
		int layw,layh;
		channels_layout->get_pixel_size(layw,layh);

		ch_mi_visible_channel->set_sensitive( ch_num > 0 );
		ch_mi_visible_only_one_channel->set_sensitive( ch_num > 0 );
		ch_mi_visible_all_channel->set_sensitive( ch_num > 0 );
		ch_mi_hide_all_channel->set_sensitive( ch_num > 0 );
		ch_mi_save_all_in_one->set_sensitive( (ch_num > 0) && ((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag)) );
		ch_mi_save_channel->set_sensitive( (ch_num > 0) && ((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag)) );
		ch_mi_save_all_channels->set_sensitive( (ch_num > 0) && ((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag)) );
		ch_mi_set_channel_color->set_sensitive( ch_num > 0 );
		ch_mi_set_channel_fill_color->set_sensitive( ch_num > 0 );
		ch_mi_del_channel->set_sensitive( ch_num > 0 );

		if(prop_addInMenu)
			ch_mi_add_channels->show();
		else
			ch_mi_add_channels->hide();
		if(prop_removeInMenu)
			ch_mi_del_channel->show();
		else
			ch_mi_del_channel->hide();
		
		if(ch_num > 0)
		{
			
			save_channel_num = (y - channelsListScrollwin->get_allocation().get_y() + abs(softmove_ch_list_pos_y))/layh;
			if(save_channel_num>=ch_num-1)
				save_channel_num = channels.rbegin()->second->get_channel_id();
			else if(save_channel_num<=0)
				save_channel_num = channels.begin()->second->get_channel_id();
			else
			{
				int num = 0;
				ChannelsList::iterator it = channels.begin();
				while(num<save_channel_num)
				{
					++num;
					++it;
				};
				save_channel_num = it->second->get_channel_id();
			}
			
			ch_mi_visible_channel->show();
			ch_mi_visible_only_one_channel->show();
			ch_mi_visible_all_channel->show();
			ch_mi_hide_all_channel->show();
			ch_mi_save_all_in_one->show();
			ch_mi_save_channel->show();
			ch_mi_save_all_channels->show();
			ch_mi_set_channel_color->show();
			ch_mi_set_channel_fill_color->show();

			string name = channels[save_channel_num]->get_channelname();
			ch_mi_save_channel->set_label(Glib::ustring("Сохранить канал '")+name+Glib::ustring("' в файл CSV"));
			
			ch_mi_visible_channel->set_image(*manage( new Gtk::Image(channels[save_channel_num]->get_prop_visible() ? Gtk::Stock::MEDIA_PAUSE : Gtk::Stock::MEDIA_PLAY,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
			ch_mi_visible_channel->set_label(channels[save_channel_num]->get_prop_visible() ? Glib::ustring("Скрыть канал '")+name+Glib::ustring("'"):Glib::ustring("Показать канал '")+name+Glib::ustring("'"));
			
			ch_mi_visible_only_one_channel->set_label(Glib::ustring("Показать канал '")+name+Glib::ustring("', а остальные скрыть"));
			
			ch_mi_set_channel_fill_color->set_label(channels[save_channel_num]->get_prop_fill() ? Glib::ustring("Отключить заливку цветом канала '")+name+Glib::ustring("'"):Glib::ustring("Включить заливку цветом канала '")+name+Glib::ustring("'"));
		}
		else
		{
			ch_mi_visible_channel->hide();
			ch_mi_visible_only_one_channel->hide();
			ch_mi_visible_all_channel->hide();
			ch_mi_hide_all_channel->hide();
			ch_mi_save_all_in_one->hide();
			ch_mi_save_channel->hide();
			ch_mi_save_all_channels->hide();
			ch_mi_set_channel_color->hide();
			ch_mi_set_channel_fill_color->hide();
		}

		if(prop_addInMenu)
			chek_add_channel_list();
		
		ch_menu_popup->popup(event_btn.button, event_btn.time);
	}
	else if(event_btn.type == GDK_BUTTON_PRESS)
	{
		softmove_ch_list_start=true;
		int p_x,p_y;
		Gdk::ModifierType mtype;
		channelsListScrollwin->get_window()->get_pointer(p_x,p_y,mtype);
		softmove_ch_list_start_x = p_x;
		softmove_ch_list_start_y = p_y;
	}
	else if( ev->type == GDK_MOTION_NOTIFY && softmove_ch_list_start)
	{
		int p_x,p_y;
		Gdk::ModifierType mtype;
		channelsListScrollwin->get_window()->get_pointer(p_x,p_y,mtype);
		softmove_ch_list_step_x = p_x - softmove_ch_list_start_x;
		softmove_ch_list_step_y = p_y - softmove_ch_list_start_y;
		softmove_ch_list_start_x = p_x;
		softmove_ch_list_start_y = p_y;
		softmove_ch_list_pos_x = softmove_ch_list_pos_x + softmove_ch_list_step_x;
		softmove_ch_list_pos_y = softmove_ch_list_pos_y + softmove_ch_list_step_y;
//		draw_channels_list();
		queue_draw();
	}
	else if( /*ev->type == GDK_LEAVE_NOTIFY ||*/ ev->type == GDK_BUTTON_RELEASE )
	{
		softmove_ch_list_start=false;
	}
	return false;
}
// -------------------------------------------------------------------------
void UniOscillograph::chek_add_channel_list()
{
	ch_add_menulist = ch_add_menu_popup->items();
	ch_add_menulist.clear();
	to_lo_erarchy( get_toplevel() );
	if(ch_add_menulist.empty())
		ch_mi_add_channels->hide();
	else
		ch_mi_add_channels->show();
}
// -------------------------------------------------------------------------
void UniOscillograph::to_hi_erarchy(Gtk::Widget* w)
{
	if(Gtk::Window* win = dynamic_cast<Gtk::Window*>(w))
		to_lo_erarchy(dynamic_cast<Gtk::Container*>(w));
	else if(Gtk::Container* cont = dynamic_cast<Gtk::Container*>(w))
		to_hi_erarchy(w->get_parent());
}
// -------------------------------------------------------------------------
void UniOscillograph::to_lo_erarchy(Gtk::Widget* w)
{
	Glib::ListHandle<Gtk::Widget*> childlist = (dynamic_cast<Gtk::Container*>(w))->get_children();
	for(Glib::ListHandle<Gtk::Widget*>::iterator it = childlist.begin(); it != childlist.end(); ++it)
	{
		if (UniOscilChannel* ch = dynamic_cast<UniOscilChannel*>(*it))
		{
			if(channels.find(ch->get_channel_id())!=channels.end())
				continue;
			
			if( prop_enableGroupOfChannels && !get_prop_groupOfAddChannels().empty() )
			{
				std::list<Glib::ustring> channels_group_list = explodestr(ch->get_prop_channelGroupName(),',');
				for(std::list<Glib::ustring>::iterator ch_it = channels_group_list.begin(); ch_it != channels_group_list.end(); ++ch_it)
				{
					ChannelsGroupMap::iterator ch_group_it = channels_group.find(*ch_it);
					if( ch_group_it==channels_group.end() )
						continue;
					//cout<<get_name()<<"::to_lo_erarchy  add channel="<<ch->get_name()<<" '"<<ch->get_channelname()<<"'"<<endl;
					if( prop_groupByGroupName )
					{
						//cout<<get_name()<<"::to_lo_erarchy  add channel="<<ch->get_name()<<" '"<<ch->get_channelname()<<"' from group="<<ch_group_it->first<<" ch->get_channel_id()="<<ch->get_channel_id()<<endl;
						std::vector<Gtk::Widget*> m_childlist = ch_add_menu_popup->get_children();
						std::vector<Gtk::Widget*>::iterator mit;
						for(mit = m_childlist.begin(); mit != m_childlist.end(); ++mit)
						{
							if( dynamic_cast<Gtk::MenuItem*>(*mit)->get_label()==Glib::ustring(ch_group_it->first) )
								break;
						}
						if( mit==m_childlist.end() )
						{
							//popup group menu:
							Gtk::Menu* ch_add_group_menu_popup = manage( new Gtk::Menu() );
							Gtk::MenuItem* mi_add_group_to_oscil = manage(new Gtk::MenuItem(Glib::ustring(ch_group_it->first)));
							mi_add_group_to_oscil->show();
							mi_add_group_to_oscil->set_submenu(*ch_add_group_menu_popup);
							ch_add_menu_popup->append(*mi_add_group_to_oscil);
							ch_add_group_menu_popup->accelerate(*ch_add_menu_popup);
							
							ch_group_it->second = ch_add_group_menu_popup;
						}
						//popup group item menu:
						Gtk::MenuItem* mi_add_to_oscil = manage(new Gtk::MenuItem(Glib::ustring(ch->get_channelname())));
						mi_add_to_oscil->signal_activate().connect(bind(sigc::mem_fun(*this, &UniOscillograph::add_channel),ch));
						mi_add_to_oscil->show();
						ch_group_it->second->append(*mi_add_to_oscil);
					}
					else
					{
						Gtk::MenuItem* mi_add_to_oscil = manage(new Gtk::MenuItem(Glib::ustring(ch->get_channelname())));
						mi_add_to_oscil->signal_activate().connect(bind(sigc::mem_fun(*this, &UniOscillograph::add_channel),ch));
						mi_add_to_oscil->show();
						ch_add_menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_add_to_oscil) );
					}
				}
			}
			else
			{
				Gtk::MenuItem* mi_add_to_oscil = manage(new Gtk::MenuItem(Glib::ustring(ch->get_channelname())));
				mi_add_to_oscil->signal_activate().connect(bind(sigc::mem_fun(*this, &UniOscillograph::add_channel),ch));
				mi_add_to_oscil->show();
				ch_add_menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_add_to_oscil) );
			}
		}
		else if(Gtk::Container* cont = dynamic_cast<Gtk::Container*>(*it))
			to_lo_erarchy(cont);
	}
}
// -------------------------------------------------------------------------
bool UniOscillograph::on_plot_area_event(GdkEvent* ev)
{
//	if(ev->type!=GDK_EXPOSE)
//		cout<<get_name()<<"::on_plot_area_event event->type="<<ev->type<< endl;
	GdkEventButton event_btn = ev->button;

	if(ev->type==GDK_MAP)
		is_show = true;
	else if(ev->type==GDK_UNMAP)
		is_show = false;

	if(event_btn.type == GDK_2BUTTON_PRESS)
	{
		mi_set_lock_view->set_image(*manage( new Gtk::Image(view_range_lock ? Gtk::Stock::MEDIA_PLAY : Gtk::Stock::MEDIA_PAUSE,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
		mi_set_lock_view->set_label(view_range_lock ? Glib::ustring("Не фиксировать окно просмотр"):Glib::ustring("Зафиксировать окно просмотра"));
		
		mi_set_expand->set_image(*manage( new Gtk::Image(is_expand ? Gtk::Stock::LEAVE_FULLSCREEN : Gtk::Stock::FULLSCREEN,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
		mi_set_expand->set_label(is_expand ? Glib::ustring("Свернуть"):Glib::ustring("Развернуть"));

		mi_set_dynamic_range->set_image(*manage( new Gtk::Image(prop_YDynamicRange ? Gtk::Stock::ZOOM_100 : Gtk::Stock::ZOOM_FIT,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
		mi_set_dynamic_range->set_label(prop_YDynamicRange ? Glib::ustring("Отключить динамическую шкалу по Y"):Glib::ustring("Включить динамическую шкалу по Y"));

		mi_set_web->set_label(prop_enableWeb ? Glib::ustring("Скрыть  сетку"):Glib::ustring("Показать сетку"));
		mi_set_time_on_scale->set_label(prop_enableScaleRealTime ? Glib::ustring("Не показывать время по оси X"):Glib::ustring("Отобразить время по оси X"));
		mi_set_time_on_tooltip->set_label(prop_enableTooltipRealTime ? Glib::ustring("Не показывать время на всплывающей подсказке"):Glib::ustring("Отобразить время на всплывающей подсказке"));
		mi_set_tooltip_gravity->set_label(prop_enableTooltipGravity ? Glib::ustring("Откл. притяжение всплывающей подсказки к графикам"):Glib::ustring("Вкл. притяжение всплывающей подсказки к графикам"));

		if(prop_LabelListInMenu)
		{
			mi_set_ch_list->set_image(*manage( new Gtk::Image(prop_enableLabelList ? Gtk::Stock::CANCEL : Gtk::Stock::APPLY,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
			mi_set_ch_list->set_label(prop_enableLabelList ? Glib::ustring("Скрыть список каналов"):Glib::ustring("Показать список каналов"));
			mi_set_ch_list->show();
		}
		else
			mi_set_ch_list->hide();
		
		mi_set_past_hisory->set_sensitive( (ch_num > 0) && ((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag)) );
		mi_set_past_hisory->set_image(*manage( new Gtk::Image(prop_enablePastHisory ? Gtk::Stock::DND : Gtk::Stock::DND_MULTIPLE,Gtk::ICON_SIZE_SMALL_TOOLBAR)));
		mi_set_past_hisory->set_label(prop_enablePastHisory ? Glib::ustring("Не сохранять из файлов прошлой истории"):Glib::ustring("Сохранять из файлов прошлой истории"));
		
		mi_save_all_in_one->set_sensitive( (ch_num > 0) && ((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag)) );
		mi_save_all_channels->set_sensitive( (ch_num > 0) && ((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag)) );
		
		menu_popup->popup(event_btn.button, event_btn.time);
	}
	else if( (ev->type == GDK_MOTION_NOTIFY || ev->type == GDK_BUTTON_PRESS ) && prop_enableTooltip )
	{
		show_tooltip = true;
		queue_draw();
	}
	else if( (ev->type == GDK_LEAVE_NOTIFY || ev->type == GDK_BUTTON_RELEASE) && prop_enableTooltip )
	{
		show_tooltip = false;
		queue_draw();
	}
	return false;
}
// -------------------------------------------------------------------------
bool UniOscillograph::on_h_scale_event(GdkEvent* ev)
{
	GdkEventButton event_btn = ev->button;
	GdkEventScroll event_scroll = ev->scroll;
//	cout<<get_name()<<"::on_h_scale_event event->type="<<ev->type<<" event_btn.type="<<event_btn.type<< endl;
	
	if(event_btn.type == GDK_2BUTTON_PRESS  && !data.empty())
	{
		double min_x_from_data=data.begin()->second.begin()->first;
		for(DataMap::iterator it = data.begin();it != data.end();++it)
			min_x_from_data = MIN(min_x_from_data,it->second.begin()->first);
		
		max_x = point_x + step_x;
		min_x = max_x - get_prop_XRange();
		if(min_x < min_x_from_data)
		{
			min_x = min_x_from_data;
			max_x = min_x_from_data + get_prop_XRange();
		}
		if(prop_enableScaleRealTime)
		{
			topLabel->set_curPos(point_x);
			bottomLabel->set_curPos(point_x);
		}
		
		topScale->set_range(min_x,max_x);
		bottomScale->set_range(min_x,max_x);
		plot_area->set_h_range(min_x,max_x);
		
		view_range_lock = false;
	}
	else if(event_btn.type == GDK_BUTTON_PRESS)
	{
		int p_x,p_y;
		Gdk::ModifierType mtype;
		plot_area->get_window()->get_pointer(p_x,p_y,mtype);
		scale_change_start_x = p_x;
		
		scale_change_start_x_min = min_x;
		scale_change_start_x_max = max_x;
		
		view_range_change_start=true;
	}
	else if( ev->type == GDK_MOTION_NOTIFY && view_range_change_start && !data.empty())
	{
		int p_x,p_y;
		Gdk::ModifierType mtype;
		plot_area->get_window()->get_pointer(p_x,p_y,mtype);
		int scale_change_step_x = scale_change_start_x - p_x;

		double min_x_from_data=data.begin()->second.begin()->first;
		for(DataMap::iterator it = data.begin();it != data.end();++it)
			min_x_from_data = MIN(min_x_from_data,it->second.begin()->first);
		
		min_x = scale_change_start_x_min + (get_prop_XRange()/current_plot_width)*scale_change_step_x;
		if( min_x < min_x_from_data )
			min_x = min_x_from_data;
		max_x = min_x + get_prop_XRange();
		if( max_x>(point_x + step_x) )
		{
			if( min_x_from_data > (point_x + step_x - get_prop_XRange()) )
			{
				min_x = min_x_from_data;
				max_x = min_x + get_prop_XRange();
			}
			else
			{
				max_x = point_x + step_x;
				min_x = max_x - get_prop_XRange();
			}
		}
		if(max_x > point_x )
			view_range_lock = false;
		else
			view_range_lock = true;
		
		if(prop_enableScaleRealTime)
		{
			topLabel->set_curPos(point_x);
			bottomLabel->set_curPos(point_x);
		}
		
		topScale->set_range(min_x,max_x);
		bottomScale->set_range(min_x,max_x);
		plot_area->set_h_range(min_x,max_x);
	}
	else if( /*ev->type == GDK_LEAVE_NOTIFY ||*/ ev->type == GDK_BUTTON_RELEASE )
	{
		view_range_change_start=false;
	}
	else if( event_scroll.type == GDK_SCROLL )
	{
		double min_x_from_data=data.begin()->second.begin()->first;
		for(DataMap::iterator it = data.begin();it != data.end();++it)
			min_x_from_data = MIN(min_x_from_data,it->second.begin()->first);
		
		int scale_change_step_x = 1;
		if(event_scroll.direction == GDK_SCROLL_UP)
			scale_change_step_x = current_plot_width/-10;
		else if(event_scroll.direction == GDK_SCROLL_DOWN)
			scale_change_step_x = current_plot_width/10;
		
		min_x = min_x + (get_prop_XRange()/current_plot_width)*scale_change_step_x;
		if( min_x < min_x_from_data )
			min_x = min_x_from_data;
		max_x = min_x + get_prop_XRange();
		if( max_x>(point_x + step_x) )
		{
			if( min_x_from_data > (point_x + step_x - get_prop_XRange()) )
			{
				min_x = min_x_from_data;
				max_x = min_x + get_prop_XRange();
			}
			else
			{
				max_x = point_x + step_x;
				min_x = max_x - get_prop_XRange();
			}
		}
		if(max_x > point_x )
			view_range_lock = false;
		else
			view_range_lock = true;

		if(prop_enableScaleRealTime)
		{
			topLabel->set_curPos(point_x);
			bottomLabel->set_curPos(point_x);
		}
		
		topScale->set_range(min_x,max_x);
		bottomScale->set_range(min_x,max_x);
		plot_area->set_h_range(min_x,max_x);
	}
	return false;
}
// -------------------------------------------------------------------------
bool UniOscillograph::on_v_scale_event(GdkEvent* ev)
{
	GdkEventButton event_btn = ev->button;
	GdkEventScroll event_scroll = ev->scroll;

	if(event_btn.type == GDK_2BUTTON_PRESS)
	{
		double new_max_y=0;
		double new_min_y=0;
		if(data.begin() != data.end())
		{
			new_max_y = data.begin()->second.begin()->second;
			new_min_y = new_max_y;
		}
		for(DataMap::iterator it = data.begin();it != data.end();++it)
		{
			for(ChannelsDataMap::iterator pointIt = it->second.begin();pointIt != it->second.end();++pointIt)
			{
				if(!it->first->get_prop_visible())
					continue;
				new_min_y = MIN(new_min_y,pointIt->second);
				new_max_y = MAX(new_max_y,pointIt->second);
//				cout<< get_name() << "::on_v_scale_event() min_y="<<min_y<<" max_y="<<max_y<< endl;
			}
		}
		set_prop_YMin(new_min_y);
		set_prop_YMax(new_max_y);
	}
	else if(event_btn.type == GDK_BUTTON_PRESS)
	{
		int p_x,p_y;
		Gdk::ModifierType mtype;
		plot_area->get_window()->get_pointer(p_x,p_y,mtype);
		scale_change_start_y = p_y;
		
		scale_change_start_y_min = min_y;//get_prop_YMin();
		scale_change_start_y_max = max_y;//get_prop_YMax();
		scale_change_start_range = range_y;
		
		scale_change_start=true;
	}
	else if( ev->type == GDK_MOTION_NOTIFY && scale_change_start)
	{
		Gtk::Allocation alloc = plot_area->get_allocation();
		int event_zone_height = current_plot_height/2;

		int p_x,p_y;
		Gdk::ModifierType mtype;
		plot_area->get_window()->get_pointer(p_x,p_y,mtype);
		int scale_change_step_y = scale_change_start_y - p_y;
		if(abs(scale_change_step_y)>event_zone_height)
			scale_change_step_y = scale_change_step_y>=0 ? event_zone_height : -event_zone_height;

		prop_YDynamicRange.set_value(false);
		double new_max_y;
		double new_min_y;
		
		//проверка в какую половину (верхнюю или нижнию) было нажатие
		if( (current_plot_height - (scale_change_start_y - alloc.get_y())) > event_zone_height )
		{
			new_max_y = scale_change_start_y_max + scale_change_start_range*(float)scale_change_step_y/(event_zone_height);
			if(new_max_y<=min_y)
				new_max_y = min_y+1;

			set_prop_YMax(new_max_y);
		}
		else
		{
			new_min_y = scale_change_start_y_min + scale_change_start_range*(float)scale_change_step_y/(event_zone_height);
			if(new_min_y>=max_y)
				new_min_y = max_y-1;

			set_prop_YMin(new_min_y);
		}
	}
	else if( /*ev->type == GDK_LEAVE_NOTIFY ||*/ ev->type == GDK_BUTTON_RELEASE )
	{
		scale_change_start=false;
	}
	else if( event_scroll.type == GDK_SCROLL )
	{
		Gtk::Allocation alloc = plot_area->get_allocation();
		int event_zone_height = current_plot_height/2;
		
		int p_x,p_y;
		Gdk::ModifierType mtype;
		plot_area->get_window()->get_pointer(p_x,p_y,mtype);

		int scale_change_step_y = 1;
		if(event_scroll.direction == GDK_SCROLL_UP)
			scale_change_step_y = current_plot_height/10;
		else if(event_scroll.direction == GDK_SCROLL_DOWN)
			scale_change_step_y = current_plot_height/-10;
		
		prop_YDynamicRange.set_value(false);
		double new_max_y;
		double new_min_y;
		
		//проверка в какую половину (верхнюю или нижнию) было нажатие
		if( (current_plot_height - (p_y - alloc.get_y())) > event_zone_height )
		{
			new_max_y = max_y + range_y*(float)scale_change_step_y/(current_plot_height);
			if(new_max_y<=min_y)
				new_max_y = min_y+1;
			
			set_prop_YMax(new_max_y);
		}
		else
		{
			new_min_y = min_y + range_y*(float)scale_change_step_y/(current_plot_height);
			if(new_min_y>=max_y)
				new_min_y = max_y-1;
			
			set_prop_YMin(new_min_y);
		}
	}
	return false;
}
// -------------------------------------------------------------------------
void UniOscillograph::on_history_dir_changed()
{
	if((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag))
	{
		if(createDir(prop_dir.get_value()))
		{
			if(history_file_name.empty())
				history_file_name = dateToString(time(0),"")+"_"+timeToString(time(0),"")+"_"+get_name()+".dat";
			history_file_fullname	= prop_dir.get_value()+"/"+history_file_name;
		}
		else
			cout<< get_name() << "::on_history_dir_changed() can't create Dir="<<prop_dir.get_value()<< endl;
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::on_history_enable_changed()
{
	ch_mi_save_all_in_one->set_sensitive((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag));
	ch_mi_save_channel->set_sensitive((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag));
	ch_mi_save_all_channels->set_sensitive((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag));

	mi_save_all_channels->set_sensitive((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag));
	mi_save_all_in_one->set_sensitive((prop_enableHisory && !is_glade_editor) || (prop_enableHisory && force_history_flag));
	on_history_dir_changed();
}
// -------------------------------------------------------------------------
void UniOscillograph::set_channel_fill_color()
{
	channels[save_channel_num]->set_prop_fill(!channels[save_channel_num]->get_prop_fill());
}
// -------------------------------------------------------------------------
void UniOscillograph::set_channel_visible()
{
	channels[save_channel_num]->set_prop_visible(!channels[save_channel_num]->get_prop_visible());
}
// -------------------------------------------------------------------------
void UniOscillograph::set_channel_visible_only()
{
	for(ChannelsList::iterator it = channels.begin();it != channels.end();++it)
	{
		it->second->set_prop_visible(false);
	}
	channels[save_channel_num]->set_prop_visible(true);
}
// -------------------------------------------------------------------------
void UniOscillograph::set_all_channel_visible(bool visible)
{
	for(ChannelsList::iterator it = channels.begin();it != channels.end();++it)
	{
		it->second->set_prop_visible(visible);
	}
}
//---------------------------------------------------------------------------------------
bool UniOscillograph::on_dialog_focus_out_event(GdkEventFocus *event)
{
	if(block_hide_dialog_by_out_focus)
		return false;

	if(cs_dialog)
		cs_dialog->hide();
	if(set_value_dialog)
		set_value_dialog->hide();
	return false;
}
// -------------------------------------------------------------------------
void UniOscillograph::set_channel_color()
{
	if(!cs_dialog)
		return;

	cs_dialog_change_position();
	cs_dialog->get_colorsel()->set_current_color(channels[save_channel_num]->get_channelcolor());

	int res = cs_dialog->run();
	switch(res)
	{
		case Gtk::RESPONSE_OK:
			channels[save_channel_num]->set_channelcolor(cs_dialog->get_colorsel()->get_current_color());
			cs_dialog->hide();
			break;
		case Gtk::RESPONSE_CANCEL:
			cs_dialog->hide();
			break;
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::set_dynamic_range()
{
	prop_YDynamicRange.set_value(!prop_YDynamicRange);
}
// -------------------------------------------------------------------------
void UniOscillograph::set_Y_range()
{
	if(!set_value_dialog)
		return;

	int prec = prop_tooltipLabelPrecision.get_value();
	if(prec<0)
		prec = 0;

	set_value_dialog->set_title("Выбор диапазона шкалы по Y...");
	val1_label->set_label("шкала по Y от");
	val2_label->set_label("шкала по Y до");
	val2_label->show();
	
	val1_spinbutton->set_increments(1/pow10(prec),10/pow10(prec));
	val2_spinbutton->set_increments(1/pow10(prec),10/pow10(prec));
	val1_spinbutton->set_range(-DBL_MAX,DBL_MAX);
	val2_spinbutton->set_range(-DBL_MAX,DBL_MAX);
	val1_spinbutton->set_digits(prec);
	val2_spinbutton->set_digits(prec);
	val1_spinbutton->set_value(prop_YMin.get_value());
	val2_spinbutton->set_value(prop_YMax.get_value());
	val2_spinbutton->show();
	
	set_value_dialog_change_position();
	int res = set_value_dialog->run();
	switch(res)
	{
		case Gtk::RESPONSE_OK:
			prop_YMin.set_value(val1_spinbutton->get_value());
			prop_YMax.set_value(val2_spinbutton->get_value());
			set_value_dialog->hide();
			break;
		case Gtk::RESPONSE_CANCEL:
			set_value_dialog->hide();
			break;
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::set_X_range()
{
	if(!set_value_dialog)
		return;
	
	int prec = 0;
	if(prop_refresh_timeout.get_value()<1000)
		prec = 1;
	
	set_value_dialog->set_title("Выбор диапазона шкалы по X...");
	if(prop_enableScaleRealTime)
	{
		val1_label->set_label("диапазон времени минуты");
		val2_label->set_label("диапазон времени секунды");
		
		val1_spinbutton->set_increments(1,10);
		val1_spinbutton->set_range(0,DBL_MAX);
		val1_spinbutton->set_digits(0);
		val1_spinbutton->set_value((long)prop_XRange.get_value()/60);
		val2_spinbutton->set_increments(1,10);
		val2_spinbutton->set_range(0,59);
		val2_spinbutton->set_digits(0);
		val2_spinbutton->set_value((long)prop_XRange.get_value()%60);
		val2_spinbutton->show();
		
		set_value_dialog_change_position();
		int res = set_value_dialog->run();
		double value = val1_spinbutton->get_value()*60 + val2_spinbutton->get_value();
		switch(res)
		{
			case Gtk::RESPONSE_OK:
				prop_XRange.set_value(value>step_x ? value:step_x);
				set_value_dialog->hide();
				break;
			case Gtk::RESPONSE_CANCEL:
				set_value_dialog->hide();
				break;
		}
	}
	else
	{
		val1_label->set_label("диапазон шкалы по X");
		val2_label->hide();
		
		val1_spinbutton->set_increments(1/pow10(prec),10/pow10(prec));
		val1_spinbutton->set_range(-DBL_MAX,DBL_MAX);
		val1_spinbutton->set_digits(prec);
		val1_spinbutton->set_value(prop_XRange.get_value());
		val2_spinbutton->hide();
		
		set_value_dialog_change_position();
		int res = set_value_dialog->run();
		switch(res)
		{
			case Gtk::RESPONSE_OK:
				prop_XRange.set_value(val1_spinbutton->get_value()>step_x ? val1_spinbutton->get_value():step_x);
				set_value_dialog->hide();
				break;
			case Gtk::RESPONSE_CANCEL:
				set_value_dialog->hide();
				break;
		}
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::set_line_width()
{
	if(!set_value_dialog)
		return;
	
	set_value_dialog->set_title("Выбор ширины линий графиков...");
	val2_label->hide();
	if(get_prop_plot_type()==DIAGRAM)
	{
		int prec = 2;
		
		val1_label->set_label("ширина столбцов графиков (0.0-1.0)");
		val1_spinbutton->set_increments(1/pow10(prec),10/pow10(prec));
		val1_spinbutton->set_range(0,1);
		val1_spinbutton->set_digits(prec);
		val1_spinbutton->set_value(prop_diagram_column_w.get_value());
		val2_spinbutton->hide();
	}
	else
	{
		int prec = 0;
		
		val1_label->set_label("ширина линий графиков");
		val1_spinbutton->set_increments(1/pow10(prec),10/pow10(prec));
		val1_spinbutton->set_range(0,INT_MAX);
		val1_spinbutton->set_digits(prec);
		val1_spinbutton->set_value(prop_line_width.get_value());
		val2_spinbutton->hide();
	}
	
	set_value_dialog_change_position();
	int res = set_value_dialog->run();
	switch(res)
	{
		case Gtk::RESPONSE_OK:
			if(get_prop_plot_type()==DIAGRAM)
				prop_diagram_column_w.set_value(val1_spinbutton->get_value()>0 ? val1_spinbutton->get_value():0);
			else
				prop_line_width.set_value(val1_spinbutton->get_value()>1 ? val1_spinbutton->get_value():1);
			set_value_dialog->hide();
			break;
		case Gtk::RESPONSE_CANCEL:
			set_value_dialog->hide();
			break;
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::save_channel()
{
	string filename;
	string channel_name = channels[save_channel_num]->get_channelname();
	if(!prop_dir.get_value().empty())
		filename = prop_dir.get_value() + "/" + dateToString(time(0),"") + "_" + timeToString(time(0),"") + "_" + channel_name + ".csv";
	else
		filename = dateToString(time(0),"") + "_" + timeToString(time(0),"") + "_" + channel_name + ".csv";

	save_channel_in_file(channels[save_channel_num]->get_channel_id(), filename);
}
// -------------------------------------------------------------------------
void UniOscillograph::save_all_channels()
{
	time_t tm = time(0);
	ChannelsList::iterator ch_it;
	for( ch_it=channels.begin(); ch_it!=channels.end(); ++ch_it )
	{
		string filename;
		string channel_name = ch_it->second->get_channelname();
		if(!prop_dir.get_value().empty())
			filename = prop_dir.get_value() + "/" + dateToString(tm,"") + "_" + timeToString(tm,"") + "_" + channel_name + ".csv";
		else
			filename = dateToString(tm,"") + "_" + timeToString(tm,"") + "_" + channel_name + ".csv";
		
		save_channel_in_file(ch_it->second->get_channel_id(), filename);
	}
}
// -------------------------------------------------------------------------
void UniOscillograph::save_channel_in_file(int channel_num, string filename)
{
	if(channels.empty() || save_in_progress)
		return;
	if(channel_num<0)
		return;
	ChannelsList::iterator ch_it = channels.find(channel_num);
	if(ch_it==channels.end())
		return;
	
	save_in_progress = true;
	saveProgress->set_fraction(0);
	saveProgress->set_text("Сохранение "+filename+"...");
	saveProgress->show();
	while(Gtk::Main::events_pending())
		Gtk::Main::iteration();
	
//	cout<< get_name() << "::save_channel_in_file ch_id="<<channel_num<< endl;
	filename = Glib::convert(filename,"CP1251","UTF8");

	cout<< get_name() <<"  try write to file: "<<filename<<endl;
	fstream fout( filename.c_str(), ios::out | ios::trunc );
	if(!fout)
	{
		cout<< get_name() << " can't open file="<<history_file_fullname<<" for write!"<< endl;
		return;
	}

	int prec = ch_it->second->get_prop_precision();
	stringstream s;
	string channel_name = ch_it->second->get_channelname();
	s<<"Время"<<"\tОтсчет [с]\t"<<channel_name;

	list<string> HistoryFileList;
	if(prop_enablePastHisory)
	{
		//наполняем список файлов истории
		Glib::Dir dir(get_prop_dir());
		Glib::DirIterator   dirit;
		for(dirit=dir.begin(); dirit!=dir.end(); ++dirit )
		{
			try
			{
				Glib::Dir subdir(get_prop_dir()+"/"+(*dirit));
			}
			catch( Glib::FileError& ex )
			{
				//NOT_DIRECTORY
				Glib::ustring fname = *dirit;
				//				fname = fname.uppercase();
				Glib::ustring name	=  get_name() + ".dat";
				//				name = name.uppercase();
				if( (fname.rfind(name)==(fname.size()-name.size())) && (history_file_fullname!=get_prop_dir()+"/"+fname) )
				{
					cout<< get_name() << " find past history file="<<fname<<"..."<< endl;
					HistoryFileList.push_back(get_prop_dir()+"/"+fname);
					inplace_merge(HistoryFileList.begin(), --(HistoryFileList.end()), HistoryFileList.end());
				}
			}
		}
		dir.close();
	}
	HistoryFileList.push_back(history_file_fullname);

	// узнаем размер истории в байтах
	long size_of_history = 0;
	for(list<string>::iterator hf_it = HistoryFileList.begin(); hf_it!=HistoryFileList.end(); ++hf_it )
	{
		string history_file = *hf_it;
		fstream fin( history_file.c_str(), ios::in);
		if(fin)
		{
			fin.seekg(0, ios::end);
			size_of_history+=fin.tellg();
//			cout<< get_name() << " history_file="<<history_file<<" size_of_history="<<size_of_history<< endl;
			fin.close();
		}
	}
	size_of_history+=history.size()*sizeof(ChannelHistoryData);
	
	long tick = 0;
	double common_point=0; //файл содержит point_x отсчитанный от 0, потому нужна эта обобщенная прибавка
	for(list<string>::iterator hf_it = HistoryFileList.begin(); hf_it!=HistoryFileList.end(); ++hf_it )
	{
		string history_file = *hf_it;
		fstream fin( history_file.c_str(), ios::in);
		if(!fin)
		{
			cout<< get_name() << " can't open file="<<history_file<<" for read!"<< endl;
		}
		else
		{
			cout<< get_name() << " open file="<<history_file<<" for read!"<< endl;
			fin.close();
			ChannelHistoryData tmpHistory;
			FILE* infile = fopen(history_file.c_str(),"r");
			if(infile)
			{
				double point;
				while(fread(&tmpHistory,sizeof(ChannelHistoryData),1,infile) > 0)
				{
					++tick;
					if(tick%(100*get_prop_refresh_timeout()) == 0)
					{
						double fraction = 0;
						if(size_of_history>0)
							fraction = (float)(tick*sizeof(ChannelHistoryData))/size_of_history;
						if(fraction<0)
							fraction = 0;
						else if(fraction>1.0)
							fraction = 1.0;
						saveProgress->set_fraction(fraction);
						
						while(Gtk::Main::events_pending())
							Gtk::Main::iteration();
					}

					if(tmpHistory.ch_id!=channel_num)
						continue;
					string time_str = timeToString(tmpHistory.tm,":");
					point = common_point + tmpHistory.point_x;
					float ch_value = tmpHistory.value;
					
					s<<"\n"<<time_str;
					s<<"\t"<<(long)point<<","<<setw(5)<<setfill('0')<<(long)((point-(long)point)*100000);
					s<<"\t"<< (ch_value>=0? "":"-") <<abs((long)ch_value)<<","<<abs((long)((ch_value-(long)ch_value)*pow10(prec)));
				};
				fclose(infile);
				common_point = point;
			}
		}
	}

	//если список файлов истории состоит только из файла истории текущей сессии то обнуляем обобщенную прибавку
	if(*(HistoryFileList.begin())==history_file_fullname)
		common_point = 0;
	
	for(HistoryMap::iterator it = history.begin(); it!=history.end(); it++ )
	{
		++tick;
		if(tick%(100*get_prop_refresh_timeout()) == 0)
		{
			double fraction = 0;
			if(size_of_history>0)
				fraction = (float)(tick*sizeof(ChannelHistoryData))/size_of_history;
			if(fraction<0)
				fraction = 0;
			else if(fraction>1.0)
				fraction = 1.0;
			saveProgress->set_fraction(fraction);
			
			while(Gtk::Main::events_pending())
				Gtk::Main::iteration();
		}
		
		if(it->second.find(channel_num)==it->second.end())
			continue;
		string time_str = timeToString((it->second.begin())->second.tm,":");
		double point = it->second[channel_num].point_x;
		float ch_value = it->second[channel_num].value;

		s<<"\n"<<time_str;
		s<<"\t"<<(long)point<<","<<setw(5)<<setfill('0')<<(long)((point-(long)point)*100000);
		s<<"\t"<< (ch_value>=0? "":"-") <<abs((long)ch_value)<<","<<abs(lround((ch_value-(long)ch_value)*pow10(prec)));
	}

	fout<<Glib::convert(s.str(),"UNICODE","UTF8");
//	fout<<s.str();
	fout.close();
	cout<< get_name() << "::save_channel '"<<channel_name<<"' save OK!"<< endl;
	saveProgress->hide();
	save_in_progress = false;
}
//--------------------------------------------------------------------------
void UniOscillograph::save_all_channels_in_one_file()
{
	if(channels.empty() || save_in_progress)
		return;

	string filename;
	if(!prop_dir.get_value().empty())
		filename = prop_dir.get_value() + "/" + dateToString(time(0),"") + "_" + timeToString(time(0),"") + "_" + get_name() + ".csv";
	else
		filename = dateToString(time(0),"") + "_" + timeToString(time(0),"") + "_" + get_name() + ".csv";
	
	save_in_progress = true;
	saveProgress->set_text("Сохранение "+filename+"...");
	saveProgress->set_fraction(0);
	saveProgress->show();
	while(Gtk::Main::events_pending())
		Gtk::Main::iteration();
	
//	cout<<"file: "<<filename<<endl;
	filename = Glib::convert(filename,"CP1251","UTF8");

	cout<< get_name() <<" try write to file: "<<filename<<endl;
	fstream fout( filename.c_str(), ios::out | ios::trunc );
	if(!fout)
	{
		cout<< get_name() << " can't open file="<<filename<<" for write!"<< endl;
		return;
	}

	stringstream s;
	s<<"Время"<<"\tОтсчет [с]";
	for( ChannelsList::iterator it=channels.begin(); it!=channels.end(); it++ )
	{
		string name = it->second->get_channelname();
		s<<"\t"<<name;
	}

	list<string> HistoryFileList;
	if(prop_enablePastHisory)
	{
		//наполняем список файлов истории
		Glib::Dir dir(get_prop_dir());
		Glib::DirIterator   dirit;
		for(dirit=dir.begin(); dirit!=dir.end(); ++dirit )
		{
			try
			{
				Glib::Dir subdir(get_prop_dir()+"/"+(*dirit));
			}
			catch( Glib::FileError& ex )
			{
				//NOT_DIRECTORY
				Glib::ustring fname = *dirit;
//				fname = fname.uppercase();
				Glib::ustring name	=  get_name() + ".dat";
//				name = name.uppercase();
				if( (fname.rfind(name)==(fname.size()-name.size())) && (history_file_fullname!=get_prop_dir()+"/"+fname) )
				{
					cout<< get_name() << " find past history file="<<fname<<"..."<< endl;
					HistoryFileList.push_back(get_prop_dir()+"/"+fname);
					inplace_merge(HistoryFileList.begin(), --(HistoryFileList.end()), HistoryFileList.end());
				}
			}
		}
		dir.close();
	}
	HistoryFileList.push_back(history_file_fullname);
	
	// узнаем размер истории в байтах
	long size_of_history = 0;
	for(list<string>::iterator hf_it = HistoryFileList.begin(); hf_it!=HistoryFileList.end(); ++hf_it )
	{
		string history_file = *hf_it;
		fstream fin( history_file.c_str(), ios::in);
		if(fin)
		{
			fin.seekg(0, ios::end);
			size_of_history+=fin.tellg();
//			cout<< get_name() << " history_file="<<history_file<<" size_of_history="<<size_of_history<< endl;
			fin.close();
		}
	}
	size_of_history+=history.size()*channels.size()*sizeof(ChannelHistoryData);
	
	int newline_id=channels.begin()->second->get_channel_id();
	long tick = 0;
	double common_point=0; //файл содержит point_x отсчитанный от 0, потому нужна эта обобщенная прибавка
	for(list<string>::iterator hf_it = HistoryFileList.begin(); hf_it!=HistoryFileList.end(); ++hf_it )
	{
		string history_file = *hf_it;
		fstream fin( history_file.c_str(), ios::in);
		if(!fin)
		{
			cout<< get_name() << " can't open file="<<history_file<<" for read!"<< endl;
		}
		else
		{
			cout<< get_name() << " open file="<<history_file<<" for read!"<< endl;
			fin.close();
			ChannelHistoryData tmpHistory;
			FILE* infile = fopen(history_file.c_str(),"r");
			if(infile)
			{
				string time_str;
				double point;
				ChannelsList::iterator last_ch_it = channels.begin();
				while(fread(&tmpHistory,sizeof(ChannelHistoryData),1,infile) > 0)
				{
					++tick;
					if(tick%(100*get_prop_refresh_timeout()) == 0)
					{
						double fraction = 0;
						if(size_of_history>0)
							fraction = (float)(tick*sizeof(ChannelHistoryData))/size_of_history;
						if(fraction<0)
							fraction = 0;
						else if(fraction>1.0)
							fraction = 1.0;
						saveProgress->set_fraction(fraction);
						
						while(Gtk::Main::events_pending())
							Gtk::Main::iteration();
					}
					
					ChannelsList::iterator ch_it = channels.find(tmpHistory.ch_id);
					if(ch_it==channels.end())
						continue;

					if( tmpHistory.ch_id==newline_id || tmpHistory.newline_flag ) // проверка что пора переходить на новую строчку
					{
						s<<"\n";
						time_str = timeToString(tmpHistory.tm,":");
						point = common_point + tmpHistory.point_x;
						s<<time_str<<"\t"<<(long)point<<","<<setw(5)<<setfill('0')<<(long)((point-(long)point)*100000);
					}
					if( tmpHistory.ch_id!=newline_id && tmpHistory.newline_flag )
					{
						for( ChannelsList::iterator it=channels.begin(); it!=ch_it; ++it )
							s<<"\t";
					}
					else if( tmpHistory.ch_id!=newline_id )
					{
						for( ChannelsList::iterator it=last_ch_it; (it!=ch_it && it!=channels.end()); ++it )
							s<<"\t";
					}
					
					float ch_value = tmpHistory.value;
					int prec = ch_it->second->get_prop_precision();
					s<<"\t"<< (ch_value>=0? "":"-") <<abs((long)ch_value)<<","<<abs(lround((ch_value-(long)ch_value)*pow10(prec)));
					
					last_ch_it = ch_it;
					++last_ch_it;
				};
				fclose(infile);
				common_point = point;
			}
		}
	}
	
	//если список файлов истории состоит только из файла истории текущей сессии то обнуляем обобщенную прибавку
	if(*(HistoryFileList.begin())==history_file_fullname)
		common_point = 0;

	for(HistoryMap::iterator it = history.begin(); it!=history.end(); ++it )
	{
		s<<"\n";
		double point = common_point + it->first;
		string time_str = timeToString((it->second.begin())->second.tm,":");
		s<<time_str<<"\t"<<(long)point<<","<<setw(5)<<setfill('0')<<(long)((point-(long)point)*100000);

		ChannelsList::iterator last_ch_it = channels.begin();
		for(ChannelHistoryDataMap::iterator ch_it = it->second.begin(); ch_it!=it->second.end(); ++ch_it )
		{
			++tick;
			if(tick%(100*get_prop_refresh_timeout()) == 0)
			{
				double fraction = 0;
				if(size_of_history>0)
					fraction = (float)(tick*sizeof(ChannelHistoryData))/size_of_history;
				if(fraction<0)
					fraction = 0;
				else if(fraction>1.0)
					fraction = 1.0;
				saveProgress->set_fraction(fraction);
				
				while(Gtk::Main::events_pending())
					Gtk::Main::iteration();
			}
			
			ChannelsList::iterator ch_list_it = channels.find(ch_it->second.ch_id);
			if(ch_list_it==channels.end())
				continue;
			
			ChannelsList::iterator num_it;
			if( ch_it->second.ch_id!=newline_id && ch_it->second.newline_flag )
			{
				for( num_it=channels.begin(); num_it!=ch_list_it; ++num_it )
					s<<"\t";
			}
			else if( ch_it->second.ch_id!=newline_id )
			{
				for( num_it=last_ch_it; (num_it!=ch_list_it && num_it!=channels.end()); ++num_it )
					s<<"\t";
			}
			
			float ch_value = ch_it->second.value;
			int prec = ch_list_it->second->get_prop_precision();
			s<<"\t"<< (ch_value>=0? "":"-") <<abs((long)ch_value)<<","<<abs(lround((ch_value-(long)ch_value)*pow10(prec)));
			
			last_ch_it = ch_list_it;
			++last_ch_it;
		}
	}

	fout<<Glib::convert(s.str(),"UNICODE","UTF8");
//	fout<<s.str();
	fout.close();
	cout<< get_name() << "::save_all_channels_in_one_file() save OK!"<< endl;
	saveProgress->hide();
	save_in_progress = false;
}
//--------------------------------------------------------------------------
string UniOscillograph::dateToString(time_t tm, string brk)
{
	struct tm *tms = localtime(&tm);
	ostringstream date;
	date << std::setw(4) << std::setfill('0') << tms->tm_year+1900 << brk;
	date << std::setw(2) << std::setfill('0') << tms->tm_mon+1 << brk;
	date << std::setw(2) << std::setfill('0') << tms->tm_mday;
	return date.str();
}
//--------------------------------------------------------------------------
string UniOscillograph::timeToString(time_t tm, string brk)
{
	struct tm *tms = localtime(&tm);
	ostringstream time;
	time << std::setw(2) << std::setfill('0') << tms->tm_hour << brk;
	time << std::setw(2) << std::setfill('0') << tms->tm_min << brk;
	time << std::setw(2) << std::setfill('0') << tms->tm_sec;
	return time.str();
}
//--------------------------------------------------------------------------
void UniOscillograph::timeToInt(long time,int &hour, int &min, int &sec, time_t tm)
{
 	struct tm *tms = localtime(&tm);
	hour = tms->tm_hour + time / 3600;
	min = tms->tm_min + (time % 3600) / 60;
	sec = tms->tm_sec + (time % 3600) % 60;
	if(sec<0)
	{
		sec = 60 + sec;
		min--;
	}
	if(sec>=60)
	{
		sec = sec - 60;
		min++;
	}
	if(min<0)
	{
		min = 60 + min;
		hour--;
	}
	if(min>=60)
	{
		min = min - 60;
		hour++;
	}
	if(hour<0)
		hour = 24 + (hour % 24);
	else if(hour>=24)
		hour = hour % 24;
}
//--------------------------------------------------------------------------
bool UniOscillograph::createDir( const std::string dir )
{
	if( mkdir(dir.c_str(), (S_IRWXO | S_IRWXU | S_IRWXG) ) == -1 )
	{
		if( errno != EEXIST )
		{
			cout<< " mkdir " << dir << " FAILED! " << strerror(errno) << endl;
			return false;
		}
	}

	return true;
}
//--------------------------------------------------------------------------
