#ifndef _UNIOSCILLOGRAPH_H
#define _UNIOSCILLOGRAPH_H
// -------------------------------------------------------------------------
#include <unordered_map>
#include <map>
#include <gtkmm.h>
#include <TransProperty.h>
#include <usvgwidgets/USVGImage.h>
#include "UniOscilChannel.h"
#include "UDefaultFunctions.h"
#include "USignals.h"
#include "UScale.h"
#include "plugins.h"
#include "global_macros.h"
// -------------------------------------------------------------------------
/*!\brief Класс контейнера.
 * \par
 * Класс предназначен для хранения виджетов UniOscilChannel отображающих состояние датчиков.
 * Класс основан на Gtk::Fixed
 */
class UniOscillograph : public UDefaultFunctions<Gtk::Fixed>
{
	public:
		UniOscillograph();
		explicit UniOscillograph(GtkmmBaseType::BaseObjectType* gobject);
		virtual ~UniOscillograph();

		typedef std::map<long,UniOscilChannel*> ChannelsList;
		typedef std::unordered_map<std::string,Gtk::Menu*> ChannelsGroupMap;
		
		virtual void process_sensor(UniWidgetsTypes::ObjectId,UniWidgetsTypes::ObjectId,long);
		void startup_init(void);

		static std::string timeToString(time_t tm=time(0), std::string brk=""); /*!< Preobrazovanie vremeni v stroku HH:MM:SS */
		static std::string dateToString(time_t tm=time(0), std::string brk=""); /*!< Preobrazovanie dati v stroku YYYY/MM/DD */
		static bool createDir( const std::string dir );
		static void timeToInt(long time,int &hour, int &min, int &sec, time_t tm=time(0)); /*!< Preobrazovanie vremeni v chisla*/

		inline void switch_lock_view(){ view_range_lock = !view_range_lock;};
		inline void switch_oscil_web(){ prop_enableWeb.set_value(!prop_enableWeb);};
		inline void switch_ch_list(){ prop_enableLabelList.set_value(!prop_enableLabelList);};
		inline void switch_past_hisory(){ set_prop_enablePastHisory(!prop_enablePastHisory);};
		inline void switch_time_on_scale(){ prop_enableScaleRealTime.set_value(!prop_enableScaleRealTime);};
		inline void switch_time_on_tooltip(){ prop_enableTooltipRealTime.set_value(!prop_enableTooltipRealTime);};
		inline void switch_tooltip_gravity(){ prop_enableTooltipGravity.set_value(!prop_enableTooltipGravity);};
		inline void set_force_history(bool flag){ force_history_flag = flag;};
		inline void set_enableHisory(bool flag){ set_prop_enableHisory(flag);};
		inline ChannelsList* get_channels(){ return &channels;};
		void add_channel(UniOscilChannel* ch);
		void remove_channel(UniOscilChannel* ch);
		inline ChannelsGroupMap* get_channels_group(){ return &channels_group;};
		inline void add_channels_group(std::string group){ channels_group[group] = nullptr;};
		
		inline Gtk::Dialog* get_set_dialog() { return set_value_dialog; }
		inline Gtk::Dialog* get_cs_dialog() { return cs_dialog; }
		inline void set_block_hide_dialog_by_out_focus(bool flag) { block_hide_dialog_by_out_focus = flag; }
		inline void set_expand_parent_widget_name(Glib::ustring name) { expand_parent_widget_name = name; }
		void init_expand_parent_widget(Gtk::Widget* widget);
		typedef std::list<sigc::connection> ConnectionsList;
		ConnectionsList widget_connection_list;
		
		enum PlotType
		{
			LINEAR=0,
			DIAGRAM
		};

		enum ExpandType
		{
			PERENT_FIXED=0
			,MAIN_WINDOW
			,SELECTED_PERENT
		};
		
	protected:
		enum ScalePosition
		{
			LEFT = 0,
			RIGHT,
			TOP,
			BOTTOM
		};

		virtual void on_add(Gtk::Widget* w);
		virtual void on_remove(Gtk::Widget* w);
		virtual void oscil_allocate_changed(Gtk::Allocation& alloc);
		virtual void on_realize();
		virtual bool on_expose_event(GdkEventExpose*);
		virtual void set_connector(const ConnectorRef& connector) throw();
		virtual void on_connect() throw();
		virtual void on_disconnect() throw();
		virtual void draw_channels_list();
		virtual void draw_tooltip();
		virtual bool on_dialog_focus_out_event(GdkEventFocus *event);
		virtual bool on_plot_area_event(GdkEvent *event);
		virtual bool on_h_scale_event(GdkEvent *event);
		virtual bool on_v_scale_event(GdkEvent *event);
		
		virtual bool on_timer_tick();

		Gtk::VBox *topScaleBox;
		Gtk::VBox *bottomScaleBox;
		Gtk::HBox *leftScaleBox;
		Gtk::HBox *rightScaleBox;
		HScale *topScale;
		HScale *bottomScale;
		VScale *leftScale;
		VScale *rightScale;
		HScaleLabels* topLabel;
		HScaleLabels* bottomLabel;

		Web *plot_area;
		Gtk::Table *table;
		Gtk::HPaned *hpaned;
		Gtk::HBox *mainbox;
		
		Gtk::Image *BGImage;
		Gtk::Image *expandBGImage;
		Gtk::Window *bufferWindow;
		Gtk::Window *mainwindow;
		Gtk::Container *parent;
		Gtk::Widget *WindowChild;
		Glib::ustring expand_parent_widget_name;
		
		Gtk::Frame *channelsListBox;
		USVGImage channelsListBGImage;
		Gtk::EventBox *channelsListScrollwin;
		Glib::RefPtr<Pango::Layout> channels_layout;

		Glib::RefPtr<Pango::Layout> tooltip_layout;
		Glib::RefPtr<Gdk::Pixbuf> tooltip_image;

		typedef std::map<double,double> ChannelsDataMap;
		typedef std::unordered_map<UniOscilChannel*,ChannelsDataMap> DataMap;
		DataMap data;
		ChannelsList channels;

		ChannelsGroupMap channels_group;

	private:
		void ctor();
		sigc::connection tmr;
		sigc::connection win_hide_connection;
		sigc::connection expand_parent_resize_connection;
		
		void connect_child(Gtk::Widget* child);
		void disconnect_child(Gtk::Widget& child);

		void set_child_connector(Gtk::Widget&);

		void on_web_enable_changed();
		void on_web_minor_enable_changed();
		void on_web_major_color_changed();
		void on_web_minor_color_changed();
		void on_web_line_type_changed();
		void on_scales_enable_changed();
		void on_scales_enable_real_time_changed();
		void on_scales_y_range_changed();
		void on_scales_x_range_changed();
		void on_scales_x_step_changed();
		void on_scales_autoscale_changed();
		void on_font_changed();
		void on_font_color_changed();
		void on_scale_color_changed();
		void on_dynamic_scale_resolution_changed();
		void on_h_max_minor_changed();
		void on_h_max_major_changed();
		void on_v_max_minor_changed();
		void on_v_max_major_changed();
		void on_plot_bg_color_changed();
		void on_refresh_timeout_changed();
		void on_bg_image_changed();
		void on_expand_bg_image_changed();
		void on_bg_border_width_changed();
		void on_tooltip_image_changed();
		void on_tooltip_font_changed();
		void on_history_enable_changed();
		void on_history_dir_changed();
		void on_list_bg_image_changed();
		void on_channels_list_width_changed();
		void on_channels_list_font_changed();
		void on_channels_list_frame_changed();
		void on_channels_list_border_width_changed();
		void set_channel_random_color(UniOscilChannel *ch);
		void cs_dialog_change_position();
		void set_value_dialog_change_position();
		void on_group_of_channels_changed();
		
		bool scroll_channels_list_event(GdkEvent *event);

		void set_channel_fill_color();
		void set_channel_visible();
		void set_all_channel_visible(bool show = true);
		void set_channel_visible_only();
		void save_channel();
		void save_all_channels();
		void save_channel_in_file(int channel_num, std::string filename);
		void save_all_channels_in_one_file();
		void set_channel_color();
		void del_channel();
		void chek_add_channel_list();
		void set_dynamic_range();
		void set_Y_range();
		void set_X_range();
		void set_refresh_timeout();
		void set_line_width();
		void oscil_expand();
		void on_oscil_expand_window_hide();
		void on_oscil_expand_parent_resize(Gtk::Allocation& alloc);
		
		struct ChannelHistoryData
		{
			ChannelHistoryData():
			value(0),
			point_x(0),
			ch_id(-1),
			newline_flag(1),
			tm()
			{
			};
			float value;
			float point_x;
			int ch_id;
			int newline_flag;
			time_t tm;
		}__attribute__((packed));
		typedef std::map<int,ChannelHistoryData> ChannelHistoryDataMap;
		typedef std::map<double,ChannelHistoryDataMap> HistoryMap;
		HistoryMap history;

		int current_plot_width;
		int current_plot_height;
		int plot_width;
		int plot_height;
		int ch_num;
		double min_x;
		double max_x;
		double min_y;
		double max_y;
		float XmaxMin;
		float XmaxMaj;
		float YmaxMin;
		float YmaxMaj;
		double point_x;
		double step_x;
		double range_y;
		bool show_tooltip;
		bool is_show;
		bool is_expand;
		bool is_realized;
		int widget_width;
		int widget_height;
		int current_width;
		int current_height;
		int widget_x;
		int widget_y;

		int softmove_ch_list_pos_x;
		int softmove_ch_list_pos_y;
		int softmove_ch_list_step_x;
		int softmove_ch_list_step_y;
		int softmove_ch_list_start_x;
		int softmove_ch_list_start_y;
		bool softmove_ch_list_start;

		int scale_change_start_x;
		int scale_change_start_y;
		double scale_change_start_x_min;
		double scale_change_start_x_max;
		double scale_change_start_y_min;
		double scale_change_start_y_max;
		double scale_change_start_range;
		bool scale_change_start;
		bool view_range_change_start;
		bool view_range_lock;
		
		std::string history_file_name;
		std::string history_file_fullname;
		bool save_in_progress;
		Gtk::ProgressBar *saveProgress;
		bool force_history_flag;

		Gtk::Menu* menu_popup;							/*!< контекстное меню */
		Gtk::ImageMenuItem* mi_set_lock_view;			/*!< вкл\откл заблокировать окно просмотра графиков*/
		Gtk::ImageMenuItem* mi_set_expand;				/*!< вкл\откл расширение на весь родительский виджет */
		Gtk::ImageMenuItem* mi_set_web;					/*!< вкл\откл отображения сетки */
		Gtk::ImageMenuItem* mi_set_time_on_scale;		/*!< вкл\откл отображения времени по X */
		Gtk::ImageMenuItem* mi_set_dynamic_range;		/*!< вкл\откл динамичискую шкалу по Y */
		Gtk::ImageMenuItem* mi_set_tooltip_gravity;		/*!< вкл\откл притяжение всплывающей подсказки к графикам */
		Gtk::ImageMenuItem* mi_set_time_on_tooltip;		/*!< вкл\откл отображения времени на всплывающей подсказке */
		Gtk::ImageMenuItem* mi_set_ch_list;				/*!< вкл\откл отображения списка каналов */
		Gtk::ImageMenuItem* mi_set_Y_range;				/*!< диапазон шкалы по Y */
		Gtk::ImageMenuItem* mi_set_X_range;				/*!< диапазон шкалы по X */
		Gtk::ImageMenuItem* mi_set_refresh_timeout;		/*!< время опроса каналов (ms)*/
		Gtk::ImageMenuItem* mi_set_line_width;			/*!< задать ширину линий графиков */
		Gtk::ImageMenuItem* mi_set_past_hisory;			/*!< вкл\откл сохранения из файлов прошлой истории */
		Gtk::ImageMenuItem* mi_save_all_in_one;			/*!< сохранение всех каналов в один файл */
		Gtk::ImageMenuItem* mi_save_all_channels;		/*!< сохранение всех каналов в отдельный файл */
		Gtk::Dialog* set_value_dialog;
		Gtk::Label *val1_label;
		Gtk::SpinButton *val1_spinbutton;
		Gtk::Label *val2_label;
		Gtk::SpinButton *val2_spinbutton;
		bool block_hide_dialog_by_out_focus;

		Gtk::Menu* ch_menu_popup;							/*!< контекстное меню канала*/
		Gtk::ImageMenuItem* ch_mi_visible_channel;			/*!< отобразить\скрыть канал на графике*/
		Gtk::ImageMenuItem* ch_mi_visible_only_one_channel;	/*!< отобразить канал на графике а остальные скрыть*/
		Gtk::ImageMenuItem* ch_mi_visible_all_channel;		/*!< отобразить канал на графике а остальные скрыть*/
		Gtk::ImageMenuItem* ch_mi_hide_all_channel;			/*!< отобразить канал на графике а остальные скрыть*/
		Gtk::ImageMenuItem* ch_mi_save_all_in_one;			/*!< сохранение всех каналов в один файл */
		Gtk::ImageMenuItem* ch_mi_save_channel;				/*!< сохранение канала в файл */
		Gtk::ImageMenuItem* ch_mi_save_all_channels;		/*!< сохранение всех каналов в отдельный файл */
		Gtk::ImageMenuItem* ch_mi_set_channel_color;		/*!< выбрать цвет канала */
		Gtk::ImageMenuItem* ch_mi_set_channel_fill_color;	/*!< выставить заливку цветом канала */
		Gtk::ImageMenuItem* ch_mi_add_channels;				/*!< добавить канал */
		Gtk::ImageMenuItem* ch_mi_del_channel;				/*!< удалить канал */
		
		Gtk::Menu* ch_add_menu_popup;							/*!< контекстное меню добавления каналов*/
		Gtk::Menu::MenuList ch_add_menulist;
		void to_hi_erarchy(Gtk::Widget* w);
		void to_lo_erarchy(Gtk::Widget* w);
		
		int save_channel_num;
		Gtk::ColorSelectionDialog* cs_dialog;
		
		ADD_PROPERTY( prop_refresh_timeout, int )				/*!< свойство: */
		ADD_PROPERTY( prop_enableTimeLine, bool )				/*!< свойство: */
		ADD_PROPERTY( prop_timeLineColor, Gdk::Color )			/*!< свойство: */
		ADD_PROPERTY( prop_enableWeb, bool )					/*!< свойство: */
		ADD_PROPERTY( prop_webMajorColor, Gdk::Color )			/*!< свойство: */
		ADD_PROPERTY( prop_enableWebMinor, bool )				/*!< свойство: */
		ADD_PROPERTY( prop_webMinorColor, Gdk::Color )			/*!< свойство: */
		ADD_PROPERTY( prop_WebLineType, Web::WebLineType )		/*!< свойство: */
		ADD_PROPERTY( prop_scaleColor, Gdk::Color )				/*!< свойство: */
		ADD_PROPERTY( prop_scaleLabelColor, Gdk::Color )		/*!< свойство: */
		ADD_PROPERTY( prop_scaleLabelFont, Glib::ustring )		/*!< свойство: */
		ADD_PROPERTY( prop_scaleLabelFontSize, int )			/*!< свойство: */
		ADD_PROPERTY( prop_topScale, bool )						/*!< свойство: */
		ADD_PROPERTY( prop_bottomScale, bool )					/*!< свойство: */
		ADD_PROPERTY( prop_leftScale, bool )					/*!< свойство: */
		ADD_PROPERTY( prop_rightScale, bool )					/*!< свойство: */
		ADD_PROPERTY( prop_enableScaleRealTime, bool )			/*!< свойство: */
		ADD_PROPERTY( prop_XRange, float )						/*!< свойство: */
		ADD_PROPERTY( prop_XStepsInMemory, int )				/*!< свойство: */
		ADD_PROPERTY( prop_XStepsNum, int )						/*!< свойство: */
		ADD_PROPERTY( prop_YMin, float )						/*!< свойство: */
		ADD_PROPERTY( prop_YMax, float )						/*!< свойство: */
		ADD_PROPERTY( prop_YDynamicRange, bool )				/*!< свойство: */
		ADD_PROPERTY( prop_plot_type, PlotType )				/*!< свойство: тип отрисовки графика*/
		ADD_PROPERTY( prop_diagram_column_w, double )			/*!< свойство: коэфициент ширины столбца диаграммы относительно шага таймера*/
		ADD_PROPERTY( prop_line_width, int )					/*!< свойство: ширина линний графика*/
		ADD_PROPERTY( prop_smoothCurve, bool )					/*!< свойство: */
		ADD_PROPERTY( prop_enableDynamicScaleResolution, bool )	/*!< свойство: */
		ADD_PROPERTY( prop_XmaxMin, int )						/*!< свойство: */
		ADD_PROPERTY( prop_XmaxMaj, int )						/*!< свойство: */
		ADD_PROPERTY( prop_YmaxMin, int )						/*!< свойство: */
		ADD_PROPERTY( prop_YmaxMaj, int )						/*!< свойство: */
		ADD_PROPERTY( prop_BGImagePath, Glib::ustring)			/*!< свойство: */
		ADD_PROPERTY( prop_expand_type, ExpandType )			/*!< свойство: тип разворота графиков*/
		ADD_PROPERTY( prop_expand_parent, Gtk::Container* )		/*!< свойство: родитель для разворота графиков*/
		ADD_PROPERTY( prop_expandBGImagePath, Glib::ustring)	/*!< свойство: */
		ADD_PROPERTY( prop_bgBorderWidth, int)					/*!< свойство: */
		ADD_PROPERTY( prop_enableTooltip, bool )				/*!< свойство: */
		ADD_PROPERTY( prop_enableTooltipGravity, bool )			/*!< свойство: */
		ADD_PROPERTY( prop_tooltipImagePath, Glib::ustring)		/*!< свойство: */
		ADD_PROPERTY( prop_tooltipImageWidth, int )				/*!< свойство: */
		ADD_PROPERTY( prop_tooltipImageHeight, int )			/*!< свойство: */
		ADD_PROPERTY( prop_enableTooltipRealTime, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_tooltipLabelPrecision, int )			/*!< свойство: */
		ADD_PROPERTY( prop_tooltipLabelColor, Gdk::Color )		/*!< свойство: */
		ADD_PROPERTY( prop_tooltipLabelFont, Glib::ustring )	/*!< свойство: */
		ADD_PROPERTY( prop_tooltipLabelFontSize, int )			/*!< свойство: */
		ADD_PROPERTY( prop_enableHisory, bool )					/*!< свойство: */
		ADD_PROPERTY( prop_enablePastHisory, bool )				/*!< свойство: */
		ADD_PROPERTY( prop_dir, Glib::ustring )					/*!< свойство: */
		ADD_PROPERTY( prop_historyPointInMem, long )			/*!< свойство: */
		ADD_PROPERTY( prop_addInMenu, bool )					/*!< свойство: */
		ADD_PROPERTY( prop_removeInMenu, bool )					/*!< свойство: */
		ADD_PROPERTY( prop_enableGroupOfChannels, bool )		/*!< свойство: */
		ADD_PROPERTY( prop_groupOfAddChannels, Glib::ustring)	/*!< свойство: */
		ADD_PROPERTY( prop_groupByGroupName, bool )				/*!< свойство: */
		ADD_PROPERTY( prop_oscilGroupName, Glib::ustring)		/*!< свойство: */
		ADD_PROPERTY( prop_enableLabelList, bool )				/*!< свойство: */
		ADD_PROPERTY( prop_LabelListInMenu, bool )				/*!< свойство: */
		TransProperty<Glib::ustring> prop_LabelListBGImagePath;
		ADD_PROPERTY( prop_labelListWidth, int )				/*!< свойство: */
		ADD_PROPERTY( prop_labelListBorderWidth, int )			/*!< свойство: */
		ADD_PROPERTY( prop_labelListFrame, Gtk::ShadowType )	/*!< свойство: */
		ADD_PROPERTY( prop_labelListFont, Glib::ustring )		/*!< свойство: */
		ADD_PROPERTY( prop_labelListFontSize, int )				/*!< свойство: */

		DISALLOW_COPY_AND_ASSIGN(UniOscillograph);
};
// -------------------------------------------------------------------------
template<>
inline GType UObj_Get_Type<UniOscillograph>()
{
	static GType gtype = 0;
	if (gtype)
		return gtype;

	UniOscillograph* dummy = new UniOscillograph();
	GtkContainerClass* container_klass = GTK_CONTAINER_GET_CLASS( dummy->gobj() );

	gtype = G_OBJECT_TYPE(dummy->gobj());
	delete( dummy );
	Glib::wrap_register(gtype, &UObj_Wrap_New<UniOscillograph>);

	return gtype;
}
namespace Glib
{
	template <>
	class Value<UniOscillograph::PlotType> : public Value_Enum<UniOscillograph::PlotType>
	{
	public:
		static GType value_type() G_GNUC_CONST;
	};
	template <>
	class Value<UniOscillograph::ExpandType> : public Value_Enum<UniOscillograph::ExpandType>
	{
	public:
		static GType value_type() G_GNUC_CONST;
	};
} // namespace Glib
#endif
